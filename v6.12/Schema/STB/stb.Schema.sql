/****** Object:  Schema [stb]    Script Date: 4/17/2017 11:32:54 AM ******/
IF  EXISTS (SELECT * FROM sys.schemas WHERE name = N'stb')
DROP SCHEMA [stb]
GO
/****** Object:  Schema [stb]    Script Date: 4/17/2017 11:33:01 AM ******/
IF NOT EXISTS (SELECT * FROM sys.schemas WHERE name = N'stb')
EXEC sys.sp_executesql N'CREATE SCHEMA [stb]'

GO
