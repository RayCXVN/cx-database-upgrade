IF NOT EXISTS (SELECT * FROM sys.schemas WHERE name = N'at')
EXEC sys.sp_executesql N'CREATE SCHEMA [at]'

GO
IF NOT EXISTS (SELECT * FROM sys.fn_listextendedproperty(N'MS_Description' , N'SCHEMA',N'at', NULL,NULL, NULL,NULL))
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'The At schema holdes tables related to the Questionnaire.' , @level0type=N'SCHEMA',@level0name=N'at'
GO
