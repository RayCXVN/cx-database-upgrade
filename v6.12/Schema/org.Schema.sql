IF NOT EXISTS (SELECT * FROM sys.schemas WHERE name = N'org')
EXEC sys.sp_executesql N'CREATE SCHEMA [org]'

GO
IF NOT EXISTS (SELECT * FROM sys.fn_listextendedproperty(N'MS_Description' , N'SCHEMA',N'org', NULL,NULL, NULL,NULL))
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'The org scheam holds organisational information like departments,users and hierarchies.' , @level0type=N'SCHEMA',@level0name=N'org'
GO
