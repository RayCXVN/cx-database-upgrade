IF NOT EXISTS (SELECT * FROM sys.schemas WHERE name = N'exp')
EXEC sys.sp_executesql N'CREATE SCHEMA [exp]'

GO
IF NOT EXISTS (SELECT * FROM sys.fn_listextendedproperty(N'MS_Description' , N'SCHEMA',N'exp', NULL,NULL, NULL,NULL))
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Tables that holdes informatiion about the export setup.' , @level0type=N'SCHEMA',@level0name=N'exp'
GO
