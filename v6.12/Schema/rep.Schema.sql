IF NOT EXISTS (SELECT * FROM sys.schemas WHERE name = N'rep')
EXEC sys.sp_executesql N'CREATE SCHEMA [rep]'

GO
IF NOT EXISTS (SELECT * FROM sys.fn_listextendedproperty(N'MS_Description' , N'SCHEMA',N'rep', NULL,NULL, NULL,NULL))
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Holdes table related to how reports are shown and selections grouped.' , @level0type=N'SCHEMA',@level0name=N'rep'
GO
