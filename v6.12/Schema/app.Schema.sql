IF NOT EXISTS (SELECT * FROM sys.schemas WHERE name = N'app')
EXEC sys.sp_executesql N'CREATE SCHEMA [app]'

GO
IF NOT EXISTS (SELECT * FROM sys.fn_listextendedproperty(N'MS_Description' , N'SCHEMA',N'app', NULL,NULL, NULL,NULL))
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Holdes data on how the site (solution) is organized. Holds menues and pages setup table.' , @level0type=N'SCHEMA',@level0name=N'app'
GO
