/*
ALTER LOGIN [ATAdmin] WITH PASSWORD=N'offline'
GO
IF EXISTS (SELECT 1 FROM sys.[syslogins] l WHERE l.[name] = 'IS_Vokal')
BEGIN
    ALTER LOGIN [IS_Vokal] DISABLE
END
GO
*/
PRINT 'Pre-Upgrade 6.12'
/*
Start of versioning database-level extended properties.
*/
BEGIN
    DECLARE @DB_VERSION varchar(32), @NewConceptName nvarchar(128), @NewMainVersion varchar(32) = '',
            @RequiredMainVersion varchar(32) = '', @RequiredConceptName nvarchar(128) = '',
            @CurrentMainVersion varchar(32) = '', @CurrentConceptName nvarchar(128) = '';
    SELECT @RequiredConceptName = 'Engage', @RequiredMainVersion = '6.12.5', @NewMainVersion = '6.12.6',
           @NewConceptName = 'ENGAGE Main',
           @DB_VERSION = '6.12.006.000';    -- Format: <MajorRelease>.<System>.<Solution>

    SELECT @CurrentConceptName = convert(nvarchar(max),value) FROM fn_listextendedproperty(N'Concept Name', NULL, NULL, NULL, NULL, NULL, NULL)
    SELECT @CurrentMainVersion = convert(nvarchar(max),value) FROM fn_listextendedproperty(N'Common Main Version', NULL, NULL, NULL, NULL, NULL, NULL)
    
    RAISERROR ('This script is generated for executing on %s version %s', 0, 1, @RequiredConceptName, @RequiredMainVersion) WITH NOWAIT;
    IF @CurrentConceptName NOT LIKE ('%' + @RequiredConceptName + '%') OR @CurrentMainVersion NOT LIKE @RequiredMainVersion
    BEGIN
        PRINT 'Incorrect solution or database version'
        SET NOEXEC ON
    END

    IF EXISTS (SELECT 1 FROM fn_listextendedproperty(N'Concept Name', NULL, NULL, NULL, NULL, NULL, NULL))
        EXEC sp_dropextendedproperty N'Concept Name', NULL, NULL, NULL, NULL, NULL, NULL
    EXEC sp_addextendedproperty N'Concept Name', @NewConceptName, NULL, NULL, NULL, NULL, NULL, NULL

    IF EXISTS (SELECT 1 FROM fn_listextendedproperty(N'Common Main Version', NULL, NULL, NULL, NULL, NULL, NULL))
        EXEC sp_dropextendedproperty N'Common Main Version', NULL, NULL, NULL, NULL, NULL, NULL
    EXEC sp_addextendedproperty N'Common Main Version', @NewMainVersion, NULL, NULL, NULL, NULL, NULL, NULL

    IF EXISTS (SELECT 1 FROM fn_listextendedproperty(N'cxStudio Database Version', NULL, NULL, NULL, NULL, NULL, NULL))
        EXEC sp_dropextendedproperty N'cxStudio Database Version', NULL, NULL, NULL, NULL, NULL, NULL
    EXEC sp_addextendedproperty N'cxStudio Database Version', @DB_VERSION, NULL, NULL, NULL, NULL, NULL, NULL
END
/*
End of versioning database-level extended properties.
*/
GO
SET NOEXEC OFF
GO