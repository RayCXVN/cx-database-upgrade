ALTER LOGIN [ATAdmin] WITH PASSWORD=N'morty'
GO
IF EXISTS (SELECT 1 FROM sys.[syslogins] l WHERE l.[name] = 'IS_Vokal')
BEGIN
    ALTER LOGIN [IS_Vokal] ENABLE
END
GO
PRINT 'Setting database branch'
/*
Start of versioning database-level extended properties.
*/
BEGIN
    DECLARE @DB_VERSION varchar(32), @ConceptName nvarchar(128)
    SELECT @ConceptName = 'ENGAGE Release', @DB_VERSION = '120.0';

    IF EXISTS (SELECT 1 FROM fn_listextendedproperty(N'Concept Name', NULL, NULL, NULL, NULL, NULL, NULL))
        EXEC sp_dropextendedproperty N'Concept Name', NULL, NULL, NULL, NULL, NULL, NULL
    EXEC sp_addextendedproperty N'Concept Name', @ConceptName, NULL, NULL, NULL, NULL, NULL, NULL

    IF EXISTS (SELECT 1 FROM fn_listextendedproperty(N'cxStudio Database Version', NULL, NULL, NULL, NULL, NULL, NULL))
        EXEC sp_dropextendedproperty N'cxStudio Database Version', NULL, NULL, NULL, NULL, NULL, NULL
    EXEC sp_addextendedproperty N'cxStudio Database Version', @DB_VERSION, NULL, NULL, NULL, NULL, NULL, NULL
END
/*
End of versioning database-level extended properties.
*/
GO