IF NOT EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[org].[FK_Owner_Hierarchy]') AND parent_object_id = OBJECT_ID(N'[org].[Owner]'))
ALTER TABLE [org].[Owner]  WITH CHECK ADD  CONSTRAINT [FK_Owner_Hierarchy] FOREIGN KEY([MainHierarchyID])
REFERENCES [org].[Hierarchy] ([HierarchyID])
GO
IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[org].[FK_Owner_Hierarchy]') AND parent_object_id = OBJECT_ID(N'[org].[Owner]'))
ALTER TABLE [org].[Owner] CHECK CONSTRAINT [FK_Owner_Hierarchy]
GO
IF NOT EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[org].[FK_Customer_MenuItem]') AND parent_object_id = OBJECT_ID(N'[org].[Customer]'))
ALTER TABLE [org].[Customer]  WITH CHECK ADD  CONSTRAINT [FK_Customer_MenuItem] FOREIGN KEY([RootMenuID])
REFERENCES [app].[MenuItem] ([MenuItemID])
GO
IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[org].[FK_Customer_MenuItem]') AND parent_object_id = OBJECT_ID(N'[org].[Customer]'))
ALTER TABLE [org].[Customer] CHECK CONSTRAINT [FK_Customer_MenuItem]
GO
IF NOT EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[org].[FK_Department_LastUpdatedBy]') AND parent_object_id = OBJECT_ID(N'[org].[Department]'))
ALTER TABLE [org].[Department]  WITH CHECK ADD  CONSTRAINT [FK_Department_LastUpdatedBy] FOREIGN KEY([LastUpdatedBy])
REFERENCES [org].[User] ([UserID])
GO
IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[org].[FK_Department_LastUpdatedBy]') AND parent_object_id = OBJECT_ID(N'[org].[Department]'))
ALTER TABLE [org].[Department] CHECK CONSTRAINT [FK_Department_LastUpdatedBy]
GO
IF NOT EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[org].[FK_UserGroup_Survey]') AND parent_object_id = OBJECT_ID(N'[org].[UserGroup]'))
ALTER TABLE [org].[UserGroup]  WITH CHECK ADD  CONSTRAINT [FK_UserGroup_Survey] FOREIGN KEY([SurveyId])
REFERENCES [at].[Survey] ([SurveyID])
GO
IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[org].[FK_UserGroup_Survey]') AND parent_object_id = OBJECT_ID(N'[org].[UserGroup]'))
ALTER TABLE [org].[UserGroup] CHECK CONSTRAINT [FK_UserGroup_Survey]
GO
IF NOT EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[at].[FK_StatusType_SiteID]') AND parent_object_id = OBJECT_ID(N'[at].[StatusType]'))
ALTER TABLE [at].[StatusType]  WITH CHECK ADD  CONSTRAINT [FK_StatusType_SiteID] FOREIGN KEY([SiteID])
REFERENCES [app].[Site] ([SiteID])
GO
IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[at].[FK_StatusType_SiteID]') AND parent_object_id = OBJECT_ID(N'[at].[StatusType]'))
ALTER TABLE [at].[StatusType] CHECK CONSTRAINT [FK_StatusType_SiteID]
GO
IF NOT EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_Bubble_Result]') AND parent_object_id = OBJECT_ID(N'[dbo].[Bubble]'))
ALTER TABLE [dbo].[Bubble]  WITH CHECK ADD  CONSTRAINT [FK_Bubble_Result] FOREIGN KEY([ResultID])
REFERENCES [dbo].[Result] ([ResultID])
ON DELETE CASCADE
GO
IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_Bubble_Result]') AND parent_object_id = OBJECT_ID(N'[dbo].[Bubble]'))
ALTER TABLE [dbo].[Bubble] CHECK CONSTRAINT [FK_Bubble_Result]
GO
