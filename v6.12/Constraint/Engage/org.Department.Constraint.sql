IF EXISTS (SELECT * FROM tempdb..sysobjects WHERE id=OBJECT_ID('tempdb..#tmpErrors')) DROP TABLE #tmpErrors
GO
CREATE TABLE #tmpErrors (Error int)
GO
SET XACT_ABORT ON
GO
SET TRANSACTION ISOLATION LEVEL SERIALIZABLE
GO
BEGIN TRANSACTION
GO
----------------------------------------------------------------------------------------------------------------
-- Do not allow CustomerID differences
SELECT 'Department without CustomerID, need manual fix' AS [Description],
       d.[DepartmentID], d.[Name], hd.[PathName], d.[EntityStatusID] AS [DepartmentEntityStatusID], d.[Deleted] AS [DepartmentDeleted], d.[CustomerID] AS [DepartmentCustomerID],
       hd.[Deleted] AS [HD_Deleted]
FROM [org].[Department] d
LEFT JOIN [org].[H_D] hd ON hd.[DepartmentID] = d.[DepartmentID]
WHERE d.[CustomerID] IS NULL;

IF (@@rowcount > 0)
BEGIN
    RAISERROR ('Please manually fixing all departments without CustomerID before proceeding to next step. Script terminated', 0, 1) WITH NOWAIT;
    SET NOEXEC ON;
END
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
IF EXISTS (SELECT * FROM sys.indexes WHERE object_id = OBJECT_ID(N'[org].[Department]') AND name = N'IX_Department_CustomerID')
BEGIN
    DROP INDEX [IX_Department_CustomerID] ON [org].[Department];
END
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
IF EXISTS (SELECT * FROM sys.indexes WHERE object_id = OBJECT_ID(N'[org].[Department]') AND name = N'IX_Department_ExtID_CustomerID_ArchetypeID')
BEGIN
    DROP INDEX [IX_Department_ExtID_CustomerID_ArchetypeID] ON [org].[Department];
END
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
ALTER TABLE [org].[Department] ALTER COLUMN [CustomerID] int NOT NULL;
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
IF NOT EXISTS (SELECT * FROM sys.indexes WHERE object_id = OBJECT_ID(N'[org].[Department]') AND name = N'IX_Department_CustomerID')
CREATE NONCLUSTERED INDEX [IX_Department_CustomerID] ON [org].[Department]
(
	[CustomerID] ASC
)
INCLUDE ( 	[DepartmentID]) WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
/*
IF NOT EXISTS (SELECT * FROM sys.indexes WHERE object_id = OBJECT_ID(N'[org].[Department]') AND name = N'IX_Department_ExtID_CustomerID_ArchetypeID')
CREATE UNIQUE NONCLUSTERED INDEX [IX_Department_ExtID_CustomerID_ArchetypeID] ON [org].[Department]
(
	[ExtID] ASC,
	[CustomerID] ASC,
	[ArchetypeID] ASC
)
WHERE ([ExtID] IS NOT NULL AND [ExtID]<>'' AND [EntityStatusID] = 1 AND [Deleted] IS NULL)
WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [Organization]
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
*/
----------------------------------------------------------------------------------------------------------------
IF EXISTS (SELECT * FROM #tmpErrors) ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT>0 BEGIN
PRINT 'The database update succeeded'
COMMIT TRANSACTION
END
ELSE PRINT 'The database update failed'
GO
DROP TABLE #tmpErrors
GO
SET NOEXEC OFF
GO