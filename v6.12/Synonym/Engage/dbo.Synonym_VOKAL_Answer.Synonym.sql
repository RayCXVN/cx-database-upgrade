IF NOT EXISTS (SELECT * FROM sys.synonyms WHERE name = N'Synonym_VOKAL_Answer' AND schema_id = SCHEMA_ID(N'dbo'))
BEGIN
    DECLARE @QDBname nvarchar(256) = DB_NAME(), @RDBname nvarchar(256) = '', @sqlCommand nvarchar(MAX) = ''

    SET @RDBname = REPLACE(@QDBname, 'at6q', 'at6r')
    SET @sqlCommand = N'CREATE SYNONYM [dbo].[Synonym_VOKAL_Answer] FOR [' + @RDBname + '].[dbo].[Answer]'
    EXEC (@sqlCommand)
END
GO
