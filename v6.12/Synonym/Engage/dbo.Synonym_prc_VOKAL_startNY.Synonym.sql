IF NOT EXISTS (SELECT * FROM sys.synonyms WHERE name = N'Synonym_prc_VOKAL_startNY' AND schema_id = SCHEMA_ID(N'dbo'))
BEGIN
    DECLARE @QDBname nvarchar(256) = DB_NAME(), @RDBname nvarchar(256) = '', @sqlCommand nvarchar(MAX) = ''

    SET @RDBname = REPLACE(@QDBname, 'at6q', 'at6r')
    SET @sqlCommand = N'CREATE SYNONYM [dbo].[Synonym_prc_VOKAL_startNY] FOR [' + @RDBname + '].[dbo].[prc_VOKAL_startNY_RDB]'
    EXEC (@sqlCommand)
END
GO
