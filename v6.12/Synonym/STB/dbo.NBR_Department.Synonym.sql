/****** Object:  Synonym [dbo].[NBR_Department]    Script Date: 4/17/2017 11:25:58 AM ******/
IF  EXISTS (SELECT * FROM sys.synonyms WHERE name = N'NBR_Department' AND schema_id = SCHEMA_ID(N'dbo'))
DROP SYNONYM [dbo].[NBR_Department]
GO
/****** Object:  Synonym [dbo].[NBR_Department]    Script Date: 4/17/2017 11:25:58 AM ******/
IF NOT EXISTS (SELECT * FROM sys.synonyms WHERE name = N'NBR_Department' AND schema_id = SCHEMA_ID(N'dbo'))
CREATE SYNONYM [dbo].[NBR_Department] FOR [NBR].[cx].[Department]
GO
