/****** Object:  Synonym [dbo].[NBR_H_D]    Script Date: 4/17/2017 11:25:58 AM ******/
IF  EXISTS (SELECT * FROM sys.synonyms WHERE name = N'NBR_H_D' AND schema_id = SCHEMA_ID(N'dbo'))
DROP SYNONYM [dbo].[NBR_H_D]
GO
/****** Object:  Synonym [dbo].[NBR_H_D]    Script Date: 4/17/2017 11:25:59 AM ******/
IF NOT EXISTS (SELECT * FROM sys.synonyms WHERE name = N'NBR_H_D' AND schema_id = SCHEMA_ID(N'dbo'))
CREATE SYNONYM [dbo].[NBR_H_D] FOR [NBR].[cx].[H_D]
GO
