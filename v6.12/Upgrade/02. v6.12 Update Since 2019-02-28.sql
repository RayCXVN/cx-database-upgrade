SET QUOTED_IDENTIFIER ON
GO
DECLARE @Current_DB_Version varchar(32), @CurrentConceptName nvarchar(128),
        @Required_DB_Version varchar(32), @RequiredConceptName nvarchar(128)

SET @Required_DB_Version = '6.12.007.%'
SET @RequiredConceptName = 'Engage Main'
PRINT 'This script is generated for executing on ' + @RequiredConceptName + ' version ' + @Required_DB_Version

SELECT @Current_DB_Version = convert(nvarchar(max),value) FROM fn_listextendedproperty(N'cxStudio Database Version', NULL, NULL, NULL, NULL, NULL, NULL)
SELECT @CurrentConceptName = convert(nvarchar(max),value) FROM fn_listextendedproperty(N'Concept Name', NULL, NULL, NULL, NULL, NULL, NULL)

IF @CurrentConceptName != @RequiredConceptName OR @Current_DB_Version NOT LIKE @Required_DB_Version
BEGIN
    PRINT 'Incorrect solution or cxStudio Database version'
    SET NOEXEC ON
END
GO
-------------------------------------------------------------------------------------------------------------------------
IF EXISTS (SELECT * FROM tempdb..sysobjects WHERE id=OBJECT_ID('tempdb..#tmpErrors')) DROP TABLE #tmpErrors
GO
CREATE TABLE #tmpErrors (Error int)
GO
SET XACT_ABORT ON
GO
SET TRANSACTION ISOLATION LEVEL SERIALIZABLE
GO
BEGIN TRANSACTION
GO
----------------------------------------------------------------------------------------------------------------
--#region 125. Restrict Archetype To TableType

--#region Constraints for tables
DECLARE c_Table CURSOR LOCAL FAST_FORWARD READ_ONLY FOR
SELECT SCHEMA_NAME(t.[schema_id]) AS [SchemaName], t.[name] AS [TableName], t.[object_id]
FROM [sys].[columns] [c]
JOIN [sys].[tables] t ON t.[object_id] = c.[object_id] AND [c].[name] = 'ArchetypeID'
 AND t.[name] NOT IN ('Archetype','AccessGroupMember','EntityStatusReason')
 AND t.[name] NOT LIKE 'tmp%';

DECLARE @SchemaName nvarchar(256) = '', @TableName nvarchar(256) = '', @ObjectID bigint,
        @sqlCommand nvarchar(max) = '';

OPEN c_Table
FETCH NEXT FROM c_Table INTO @SchemaName, @TableName, @ObjectID
WHILE @@FETCH_STATUS = 0
BEGIN
    SET @sqlCommand = 
N'IF NOT EXISTS (SELECT 1
                 FROM sys.[check_constraints] c
                 WHERE c.[parent_object_id] = @ObjectID AND c.[name] = ''Chk_@TableName_Archetype'')
BEGIN
    ALTER TABLE [@SchemaName].[@TableName] ADD CONSTRAINT [Chk_@TableName_Archetype] CHECK ([dbo].[Check_Archetype_TableType] (''@SchemaName'',''@TableName'',[ArchetypeID]) = 1);
END'
    SET @sqlCommand = REPLACE(@sqlCommand, '@SchemaName', @SchemaName);
    SET @sqlCommand = REPLACE(@sqlCommand, '@TableName', @TableName);
    SET @sqlCommand = REPLACE(@sqlCommand, '@ObjectID', @ObjectID);
    RAISERROR (@sqlCommand, 0, 1) WITH NOWAIT;
    EXEC sp_executesql @sqlCommand;

    FETCH NEXT FROM c_Table INTO @SchemaName, @TableName, @ObjectID
END
CLOSE c_Table
DEALLOCATE c_Table
--#endregion Constraints for tables

--#endregion 125. Restrict Archetype To TableType
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
-------------------------------------------------------------------------------------------------------------------
PRINT 'Setting database version'
/*
Start of versioning database-level extended properties.
*/
BEGIN
    DECLARE @DB_VERSION varchar(32), @ConceptName nvarchar(128), @ReleaseScriptName nvarchar(4000)
    SET @DB_VERSION = '6.12.008.000.00'
    SET @ConceptName = 'Engage Main'
    SET @ReleaseScriptName = N'v' + @DB_VERSION + N'-02. v6.12 Update Since 2019-02-28'

    IF EXISTS (SELECT 1 FROM fn_listextendedproperty(N'cxStudio Database Version', NULL, NULL, NULL, NULL, NULL, NULL))
        EXEC sp_dropextendedproperty N'cxStudio Database Version', NULL, NULL, NULL, NULL, NULL, NULL
    EXEC sp_addextendedproperty N'cxStudio Database Version', @DB_VERSION, NULL, NULL, NULL, NULL, NULL, NULL
		
    IF EXISTS (SELECT 1 FROM fn_listextendedproperty(N'Script Name', NULL, NULL, NULL, NULL, NULL, NULL))
        EXEC sp_dropextendedproperty N'Script Name', NULL, NULL, NULL, NULL, NULL, NULL
    EXEC sp_addextendedproperty N'Script Name',  @ReleaseScriptName , NULL, NULL, NULL, NULL, NULL, NULL
END
/*
End of versioning database-level extended properties.
*/
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
IF EXISTS (SELECT * FROM #tmpErrors) ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT>0 BEGIN
PRINT 'The database update succeeded'
COMMIT TRANSACTION
END
ELSE PRINT 'The database update failed'
GO
DROP TABLE #tmpErrors
GO
SET NOEXEC OFF
GO
-------------------------------------------------------------------------------------------------------------------------
DECLARE @Current_DB_Version varchar(32), @CurrentConceptName nvarchar(128),
        @Required_DB_Version varchar(32), @RequiredConceptName nvarchar(128)

SET @Required_DB_Version = '6.12.008.%'
SET @RequiredConceptName = 'Engage Main'
PRINT 'This script is generated for executing on ' + @RequiredConceptName + ' version ' + @Required_DB_Version

SELECT @Current_DB_Version = convert(nvarchar(max),value) FROM fn_listextendedproperty(N'cxStudio Database Version', NULL, NULL, NULL, NULL, NULL, NULL)
SELECT @CurrentConceptName = convert(nvarchar(max),value) FROM fn_listextendedproperty(N'Concept Name', NULL, NULL, NULL, NULL, NULL, NULL)

IF @CurrentConceptName != @RequiredConceptName OR @Current_DB_Version NOT LIKE @Required_DB_Version
BEGIN
    PRINT 'Incorrect solution or cxStudio Database version'
    SET NOEXEC ON
END
GO
-------------------------------------------------------------------------------------------------------------------------
IF EXISTS (SELECT * FROM tempdb..sysobjects WHERE id=OBJECT_ID('tempdb..#tmpErrors')) DROP TABLE #tmpErrors
GO
CREATE TABLE #tmpErrors (Error int)
GO
SET XACT_ABORT ON
GO
SET TRANSACTION ISOLATION LEVEL SERIALIZABLE
GO
BEGIN TRANSACTION
GO
----------------------------------------------------------------------------------------------------------------
--#region 126. Improve Performance UserGroup List
IF NOT EXISTS (SELECT 1 FROM sys.objects WHERE name = N'prc_UserGroup_get' AND type IN ( N'P', N'PC' )  AND SCHEMA_NAME(schema_id) = 'org')
	EXEC ('CREATE PROC [org].[prc_UserGroup_get] AS ')
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
/*
    Steve - Sept 22 2016 - EntityStatus for table UserGroup
    Sarah - 2018-07-05   - ATAdmin/CXPLAT-294/Extend UG_UCXPLAT-854
    2019-02-28 Ray:     Improve performance for ATAdmin
*/
ALTER PROCEDURE [org].[prc_UserGroup_get](
    @DepartmentID int = NULL,
    @SurveyID     int = NULL,
    @Userid       int = NULL
) AS
BEGIN
    SET NOCOUNT ON;
    DECLARE @Err int;
    DECLARE @ActiveEntityStatusID int= (SELECT [EntityStatusID] FROM [EntityStatus] WHERE [CodeName] = 'Active');

    SELECT [ug].[UserGroupID], [ug].[OwnerID], ISNULL([ug].[DepartmentID], 0) AS 'DepartmentID', ISNULL([ug].[SurveyId], 0) AS 'SurveyId', [ug].[Name],
           [ug].[Description], [ug].[Created], [ug].[ExtID], ISNULL([ug].[PeriodID], 0) AS 'PeriodID', ISNULL([ug].[UserID], 0) AS 'UserID',
           ISNULL([ug].[UsergroupTypeID], 0) AS 'UserGroupTypeID', [ug].[Tag], [ug].[LastUpdated], [ug].[ReferrerResource], [ug].[ReferrerToken],
           [ug].[ReferrerArchetypeID], [ug].[LastUpdatedBy], [ug].[LastSynchronized], [ug].[ArchetypeID], [ug].[Deleted], [ug].[EntityStatusID],
           [ug].[EntityStatusReasonID], [ug].[ReferrerToken], [ug].[ReferrerResource], [ug].[ReferrerArchetypeID],
           ISNULL(STUFF((SELECT ',' + CAST(dt.[DepartmentTypeID] AS nvarchar(32)) AS [text()]
                         FROM [org].[DT_UG] dt
                         WHERE dt.[UserGroupID] = ug.[UserGroupID]
                         FOR XML PATH (''))
                        , 1, 1, ''), '') AS [DepartmentTypeList]
    FROM [org].[UserGroup] [ug]
    LEFT JOIN [org].[Department] [d] ON [d].[DepartmentID] = [ug].[DepartmentID]
    WHERE ([ug].[DepartmentID] = @DepartmentID OR @DepartmentID IS NULL)
      AND ([ug].[SurveyId] = @SurveyID OR @SurveyID IS NULL)
      AND ([ug].[UserId] = @UserID OR @UserID IS NULL)
      AND ([ug].[DepartmentID] IS NULL OR ([d].[EntityStatusID] = @ActiveEntityStatusID AND [d].[Deleted] IS NULL)
          )
      AND [ug].[EntityStatusID] = @ActiveEntityStatusID
      AND [ug].[Deleted] IS NULL;
    SET @Err = @@Error;

    RETURN @Err;
END;
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
--#endregion 126. Improve Performance UserGroup List

--#region 129. New Archetype EducationProgram
-- Dev: Gary
-- Feature: Archetype
-- New archetype EducationProgram
IF NOT EXISTS (SELECT 1 FROM [dbo].[Archetype] a WHERE a.[MasterID] = '5FD79BA5-8260-40DE-BA92-B95E8900ACB8')
BEGIN
    INSERT INTO [dbo].[Archetype] ([ArchetypeID], [CodeName], [TableTypeID], [MasterID])
    SELECT 42 AS [ArchetypeID], 'EducationProgram' AS [CodeName], 16 AS [TableTypeID], '5FD79BA5-8260-40DE-BA92-B95E8900ACB8' AS [MasterID];
END

--#endregion 129. New Archetype EducationProgram
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
--#region 133. CE-2380 Import Org Status
-- Dev: Thomas
-- Feature: Import Organization
-- CE-2380 Import Org Status
IF NOT EXISTS (SELECT 1 FROM [log].[EventType] et WHERE et.[MasterID] = 'B304DE2D-C060-4772-9105-57EB97D307A0')
BEGIN
    DECLARE @EventType_ImportOrg int;

    INSERT INTO [log].[EventType] ([CodeName], [OwnerID], [No], [Created], [Active], [DisplayPattern], [RetentionPeriod], [MasterID])
    SELECT 'Import_Org' AS [CodeName], o.[OwnerID], 0 AS [No], GETDATE() AS [Created], 1 AS [Active], '' AS [DisplayPattern], 0 AS [RetentionPeriod],
           'B304DE2D-C060-4772-9105-57EB97D307A0' AS [MasterID]
    FROM [org].[Owner] o;
    SET @EventType_ImportOrg = SCOPE_IDENTITY();

    INSERT INTO [log].[LT_EventType] ([LanguageID], [EventTypeID], [Name], [Description])
    SELECT l.[LanguageID], @EventType_ImportOrg AS [EventTypeID], 'Import Organization' AS [Name], '' AS [Description]
    FROM [dbo].[Language] l
    WHERE l.[LanguageID] IN (1,2,7);
END
--#endregion 133. CE-2380 Import Org Status
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
-------------------------------------------------------------------------------------------------------------------
PRINT 'Setting database version'
/*
Start of versioning database-level extended properties.
*/
BEGIN
    DECLARE @DB_VERSION varchar(32), @ConceptName nvarchar(128), @ReleaseScriptName nvarchar(4000)
    SET @DB_VERSION = '6.12.009.000.00'
    SET @ConceptName = 'Engage Main'
    SET @ReleaseScriptName = N'v' + @DB_VERSION + N'-02. v6.12 Update Since 2019-02-28'

    IF EXISTS (SELECT 1 FROM fn_listextendedproperty(N'cxStudio Database Version', NULL, NULL, NULL, NULL, NULL, NULL))
        EXEC sp_dropextendedproperty N'cxStudio Database Version', NULL, NULL, NULL, NULL, NULL, NULL
    EXEC sp_addextendedproperty N'cxStudio Database Version', @DB_VERSION, NULL, NULL, NULL, NULL, NULL, NULL
		
    IF EXISTS (SELECT 1 FROM fn_listextendedproperty(N'Script Name', NULL, NULL, NULL, NULL, NULL, NULL))
        EXEC sp_dropextendedproperty N'Script Name', NULL, NULL, NULL, NULL, NULL, NULL
    EXEC sp_addextendedproperty N'Script Name',  @ReleaseScriptName , NULL, NULL, NULL, NULL, NULL, NULL
END
/*
End of versioning database-level extended properties.
*/
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
IF EXISTS (SELECT * FROM #tmpErrors) ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT>0 BEGIN
PRINT 'The database update succeeded'
COMMIT TRANSACTION
END
ELSE PRINT 'The database update failed'
GO
DROP TABLE #tmpErrors
GO
SET NOEXEC OFF
GO