SET QUOTED_IDENTIFIER ON
GO
DECLARE @Current_DB_Version varchar(32), @CurrentConceptName nvarchar(128),
        @Required_DB_Version varchar(32), @RequiredConceptName nvarchar(128)

SET @Required_DB_Version = '6.12.007.000.%'
SET @RequiredConceptName = 'Engage Main'
PRINT 'This script is generated for executing on ' + @RequiredConceptName + ' version ' + @Required_DB_Version

SELECT @Current_DB_Version = convert(nvarchar(max),value) FROM fn_listextendedproperty(N'cxStudio Database Version', NULL, NULL, NULL, NULL, NULL, NULL)
SELECT @CurrentConceptName = convert(nvarchar(max),value) FROM fn_listextendedproperty(N'Concept Name', NULL, NULL, NULL, NULL, NULL, NULL)

IF @CurrentConceptName != @RequiredConceptName OR @Current_DB_Version NOT LIKE @Required_DB_Version
BEGIN
    PRINT 'Incorrect solution or cxStudio Database version'
    SET NOEXEC ON
END
GO
-------------------------------------------------------------------------------------------------------------------------
IF EXISTS (SELECT * FROM tempdb..sysobjects WHERE id=OBJECT_ID('tempdb..#tmpErrors')) DROP TABLE #tmpErrors
GO
CREATE TABLE #tmpErrors (Error int)
GO
SET XACT_ABORT ON
GO
SET TRANSACTION ISOLATION LEVEL SERIALIZABLE
GO
BEGIN TRANSACTION
GO
----------------------------------------------------------------------------------------------------------------
--#region 119. CE-2307 Access Control On TeachingGroup

--#region 01. Member Role
-- Dev: Thomas
-- Feature: VGO
-- CE-2307 Access control on teaching groups
UPDATE ugm SET ugm.[MemberRoleID] = mr.[MemberRoleID]
FROM [org].[MemberRole] mr
JOIN [org].[UserGroup] ug ON mr.[MasterID] = 'C36C34A6-A1B4-4625-9470-F37BE94691B4' AND ug.[ArchetypeID] = 7
JOIN [org].[UGMember] ugm ON ugm.[UserGroupID] = ug.[UserGroupID];
--#endregion 01. Member Role

--#region 02. CE-2319 Access On MultiEdit
-- Dev: Thomas
-- Feature: Access control on teaching groups
-- CE-2319 Access on Multi Edit
DECLARE @ItemListCommandIDs_10251191939291 nvarchar(32)= '';
SELECT @ItemListCommandIDs_10251191939291 = STUFF( (
    SELECT ','+CAST([c].[ItemListCommandID] AS nvarchar(32))
    FROM [list].[ItemListCommand] [c]
    WHERE [c].[MasterID] IN ('B5FDF8F9-D3E0-4427-ABC1-C599BB9716C4')
    ORDER BY CHARINDEX(CAST([c].[MasterID] AS nvarchar(64)), N'B5FDF8F9-D3E0-4427-ABC1-C599BB9716C4')
    FOR XML PATH, TYPE
                                                   ) .value ('.[1]', 'nvarchar(max)'), 1, 1, '');
IF NOT EXISTS (SELECT 1 FROM [list].[ItemListDataParameter] [dp] WHERE [dp].[MasterID] = '13E024B6-9AE2-444D-BAD1-B33AEBC7D3EB')
BEGIN
    INSERT INTO [list].[ItemListDataParameter] ([ItemListDataSourceID], [DataType], [Type], [Mandatory], [DefaultValue], [ConstructionPattern], [Created], [MasterID])
    SELECT [ds].[ItemListDataSourceID] AS [ItemListDataSourceID], N'String' AS [DataType], N'PortalPartParam' AS [Type], N'0' AS [Mandatory], N''+@ItemListCommandIDs_10251191939291+'' AS [DefaultValue], N'' AS [ConstructionPattern], GETDATE() AS [Created], N'13E024B6-9AE2-444D-BAD1-B33AEBC7D3EB' AS [MasterID]
    FROM [list].[ItemListDataSource] [ds]
    WHERE [ds].[MasterID] = '27D8FDF2-664A-452F-AAAD-7E243998B415';
    DECLARE @ParamID_4807 nvarchar(32)= SCOPE_IDENTITY();

    INSERT INTO [list].[LT_ItemListDataParameter] ([LanguageID], [ItemListDataParameterID], [Name], [Description])
    SELECT l.[LanguageID], @ParamID_4807 AS [ItemListDataParameterID], N'deniedCommandIds' AS [Name], N'' AS [Description]
    FROM [dbo].[Language] l
    WHERE l.[LanguageID] IN (1,2,7);
END;
--#endregion 02. CE-2319 Access On MultiEdit

--#endregion 119. CE-2307 Access Control On TeachingGroup
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
--#region 120. CE-2246 Update Tooltips In Class List
-- Dev: David
-- Feature: Class list
-- CE-2246 CE-2321 Update Tooltips In Class List
UPDATE lt SET lt.[ItemTooltipText] = N'Inkluder i elevrapport', lt.[HeaderTooltipText] = 'Inkluder alle i elevrapport'
FROM [list].[ItemListField] f
JOIN [list].[LT_ItemListField] lt ON lt.[ItemListFieldID] = f.[ItemListFieldID] AND f.[MasterID] = '1FBD8EAE-F5BE-4173-B59E-C66BE7903799'
 AND lt.[LanguageID] = 1;

UPDATE lt SET lt.[ItemTooltipText] = N'Include in student report', lt.[HeaderTooltipText] = 'Include all in student report'
FROM [list].[ItemListField] f
JOIN [list].[LT_ItemListField] lt ON lt.[ItemListFieldID] = f.[ItemListFieldID] AND f.[MasterID] = '1FBD8EAE-F5BE-4173-B59E-C66BE7903799'
 AND lt.[LanguageID] IN (2,7);

UPDATE lt SET lt.[RowTooltipText] = N''
FROM [list].[ItemList] l
JOIN [list].[LT_ItemList] lt ON lt.[ItemListID] = l.[ItemListID] AND l.[MasterID] = 'F2656DD5-8789-4F32-9762-C4A2EC6F64EA' AND lt.[LanguageID] IN (1,2,7);

UPDATE lt SET lt.[ItemTooltipText] = '{FirstName} {LastName} - klikk for å se elevoversikt'
FROM [list].[ItemList] l
JOIN [list].[ItemListField] f ON f.[ItemListID] = l.[ItemListID] AND l.[MasterID] = 'F2656DD5-8789-4F32-9762-C4A2EC6F64EA' 
JOIN [list].[LT_ItemListField] lt ON lt.[ItemListFieldID] = f.[ItemListFieldID] AND lt.[LanguageID] = 1
 AND f.[MasterID] NOT IN ('1FBD8EAE-F5BE-4173-B59E-C66BE7903799','16C6DEFB-00E9-433A-AB5A-350ACD0BE807');

UPDATE lt SET lt.[ItemTooltipText] = '{FirstName} {LastName} - click to see student overview'
FROM [list].[ItemList] l
JOIN [list].[ItemListField] f ON f.[ItemListID] = l.[ItemListID] AND l.[MasterID] = 'F2656DD5-8789-4F32-9762-C4A2EC6F64EA' 
JOIN [list].[LT_ItemListField] lt ON lt.[ItemListFieldID] = f.[ItemListFieldID] AND lt.[LanguageID] IN (2,7)
 AND f.[MasterID] NOT IN ('1FBD8EAE-F5BE-4173-B59E-C66BE7903799','16C6DEFB-00E9-433A-AB5A-350ACD0BE807');
--#endregion 120. CE-2246 Update Tooltips In Class List
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
--#region 121. CE-2267 Companion Button
-- Dev: David
-- Feature: Companion integration
-- CE-2267 CE-2320 Update Companion buttons

--#region AccessGroup
UPDATE lt SET lt.[Name] = N'Stafettlogg', lt.[Description] = N'----->Legg inn kunder som skal ha tilgang'
FROM [dbo].[AccessGroup] ag
JOIN [dbo].[LT_AccessGroup] lt ON lt.[AccessGroupID] = ag.[AccessGroupID] AND ag.[MasterID] = 'BAFD48E5-361E-4CF9-ACBD-FA993EA254C0';

IF NOT EXISTS (SELECT 1 FROM [dbo].[AccessGroup] ag WHERE ag.[MasterID] = 'E998DB79-E040-4EC6-8011-A77A5AEFFC1B')
BEGIN
    DECLARE @AccessGroupID_225 int;

    INSERT INTO [dbo].[AccessGroup] ([OwnerID], [No], [Created], [MasterID])
    SELECT o.[OwnerID], 0 AS [No], GETDATE() AS [Created], 'E998DB79-E040-4EC6-8011-A77A5AEFFC1B' AS [MasterID]
    FROM [org].[Owner] o;
    SET @AccessGroupID_225 = SCOPE_IDENTITY();

    INSERT INTO [dbo].[LT_AccessGroup] ([LanguageID], [AccessGroupID], [Name], [Description])
    SELECT l.[LanguageID], @AccessGroupID_225 AS [AccessGroupID], N'Oppfølgingslogg' AS [Name], N'----->Legg inn kunder som skal ha tilgang' AS [Description]
    FROM [dbo].[Language] l
    WHERE l.[LanguageID] IN (1,2,7);
END
--#endregion AccessGroup

--#region Portal

--#region PortalPart 5633
IF NOT EXISTS (SELECT 1 FROM [app].[PortalPart] [pt] WHERE [pt].[MasterID] = '41FC8963-2C15-489F-9591-6B42D786697F')
BEGIN
    DECLARE @PreviousItemOrder int;
    SELECT @PreviousItemOrder = pt.[No] FROM [app].[PortalPart] pt WHERE pt.[MasterID] = '11B9F4DC-BB02-4291-8688-F0688E966F09';

    INSERT INTO [app].[PortalPart] ([PortalPageID], [PortalPartTypeID], [No], [CategoryID], [Settings], [BubbleID], [ReportPartID], [ReportID], [CssClass], [InlineStyle], [ActivityID], [SurveyID], [ExtID], [MasterID])
    SELECT [pp].[PortalPageID] AS [PortalPageID], N'1' AS [PortalPartTypeID], @PreviousItemOrder+1 AS [No], NULL AS [CategoryID], N'<params>IsEmptyHtml%3D'+'true'+'</params>' AS [Settings], NULL AS [BubbleID], NULL AS [ReportPartID], NULL AS [ReportID], N'container-command-info col-sm-4 col-xs-12 popup-default box-floating popup-fixed no-padding no-right-radius  style-violet' AS [CssClass], N'' AS [InlineStyle], NULL AS [ActivityID], NULL AS [SurveyID], N'' AS [ExtID], N'41FC8963-2C15-489F-9591-6B42D786697F' AS [MasterID]
    FROM [app].[PortalPage] [pp]
    WHERE [pp].[MasterID] = '0678F421-C8FE-4441-A0E1-655A4CEE3B68';
    DECLARE @PortalPartID_5633 nvarchar(32)= SCOPE_IDENTITY();

    INSERT INTO [app].[LT_PortalPart] ([LanguageID], [PortalPartID], [Name], [Description], [Text], [TextAbove], [TextBelow], [HeaderText], [FooterText], [BeforeContentText], [AfterContentText])
    SELECT l.[LanguageID], @PortalPartID_5633 AS [PortalPartID], N'Oppfølgingslogg info popup' AS [Name], N'' AS [Description], N'<div class="modal-header">
    <button data-dismiss="modal" class="close" type="button"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
</div>
<div class="modal-body">
   
    <p>Oppfølgingsloggen i IKO-verktøyet skal sikre en systematisk og tett oppfølging av den enkelte elev. Oppfølgingen er den viktigste delen av IKO-arbeidet. I oppfølgingsloggen beskrives elevens utfordringer, det settes opp en plan med tiltak, hvem som har ansvar for gjennomføring og tidsramme for tiltakene og effekten av tiltakene evalueres. Oppfølgingsloggen gir en samlet og oppdatert oversikt for alle som er involvert i oppfølgingen av eleven.</p>
</div>
<div class="col-xs-12">
    <button class="btn btn-violet pull-right" type="button" onclick="goCompanion($(this),''http://systemtest-relaylog-no-www.conexus.no/Account?{bracket(0)}extId={bracket(1)}'');">Start her...</button>
</div>' AS [Text], N'' AS [TextAbove], N'' AS [TextBelow], N'' AS [HeaderText], N'' AS [FooterText], N'' AS [BeforeContentText], N'' AS [AfterContentText]
    FROM [dbo].[Language] l
    WHERE l.[LanguageID] IN (1,2,7);
END;
--#endregion PortalPart 5633

--#region PortalPart 5606
UPDATE lt SET lt.[Name] = N'Stafettlogg info popup'
FROM [app].[PortalPart] pt
JOIN [app].[LT_PortalPart] lt ON lt.[PortalPartID] = pt.[PortalPartID] AND pt.[MasterID] = '4ADF301D-144D-4D12-AB24-D61DDF994AA8';
--#endregion PortalPart 5606

/* test
{if({IsMemberOfAccessGroup(228)},
"<td><button class="btn btn-primary btn-companion" type="button" onclick="renderPortalPartInfo($(this),5633);
" data-target="#portal-part-5606">Harry &nbsp; | <span class="icon-vokal-help"></span></button></td>","")}
*/
DECLARE @AccessGroup_223 nvarchar(32) = '', @AccessGroup_225 nvarchar(32) = '',
        @PortalPart_5606 nvarchar(32) = '', @PortalPart_5633 nvarchar(32) = '',
        @PortalPage_3840 nvarchar(32) = '', @Form_106 nvarchar(32) = '';
SELECT @AccessGroup_223 = ag.[AccessGroupID] FROM [dbo].[AccessGroup] ag WHERE ag.[MasterID] = 'BAFD48E5-361E-4CF9-ACBD-FA993EA254C0';
SELECT @AccessGroup_225 = ag.[AccessGroupID] FROM [dbo].[AccessGroup] ag WHERE ag.[MasterID] = 'E998DB79-E040-4EC6-8011-A77A5AEFFC1B';
SELECT @PortalPart_5606 = pt.[PortalPartID] FROM [app].[PortalPart] pt WHERE pt.[MasterID] = '4ADF301D-144D-4D12-AB24-D61DDF994AA8';
SELECT @PortalPart_5633 = pt.[PortalPartID] FROM [app].[PortalPart] pt WHERE pt.[MasterID] = '41FC8963-2C15-489F-9591-6B42D786697F';
SELECT @PortalPage_3840 = pp.[PortalPageID] FROM [app].[PortalPage] pp WHERE pp.[MasterID] = '4252A963-664C-4F2C-9DA5-EDE4279B0C51';
SELECT @Form_106 = f.[FormID] FROM [form].[Form] f WHERE f.[MasterID] = '3898FDDE-06BF-440A-BC94-D9024670F526';

--#region PortalPart 5607,5608
UPDATE lt SET lt.[Text] = N'<table class="table" data-nosort="true"><colgroup><col></colgroup><tbody><tr>{if({IsMemberOfAccessGroup('+@AccessGroup_223+')},"<td><button class="btn btn-primary btn-companion" type="button" onclick="renderPortalPartInfo($(this),'+@PortalPart_5606+');" data-target="#portal-part-'+@PortalPart_5606+'">Stafettlogg &nbsp; | <span class="icon-vokal-help"></span></button></td>","")}{if({IsMemberOfAccessGroup('+@AccessGroup_225+')},"<td><button class="btn btn-primary btn-companion" type="button" onclick="renderPortalPartInfo($(this),'+@PortalPart_5633+');" data-target="#portal-part-'+@PortalPart_5633+'">Oppfølgingslogg &nbsp; | <span class="icon-vokal-help"></span></button></td>","")}<td><button class="btn btn-primary icon right large" type="button" onclick="renderReportBox($(this), '+@PortalPage_3840+');"><span class="icon-vokal-print"></span>Lag elevrapport</button></td><td><button class="btn btn-primary icon right large" type="button" onclick="showEditStudentPopup('+@Form_106+');">Redigere elev<span class="icon-vokal-edit"></span></button></td></tr></tbody></table>'
FROM [app].[PortalPart] pt
JOIN [app].[LT_PortalPart] lt ON lt.[PortalPartID] = pt.[PortalPartID]
 AND pt.[MasterID] IN ('4B067F0F-80C6-4BCE-AB53-4A8A47857B0B','F1F73C47-3FB7-40E2-9C97-FAD4E1AA917E');
--#endregion PortalPart 5607,5608

--#region PortalPart 5609,5610
UPDATE lt SET lt.[Text] = N'<table class="table" data-nosort="true"><colgroup><col></colgroup><tbody><tr>{if({IsMemberOfAccessGroup('+@AccessGroup_223+')},"<td><button class="btn btn-primary btn-companion" type="button" onclick="renderPortalPartInfo($(this),'+@PortalPart_5606+');" data-target="#portal-part-'+@PortalPart_5606+'">Stafettlogg &nbsp; | <span class="icon-vokal-help"></span></button></td>","")}{if({IsMemberOfAccessGroup('+@AccessGroup_225+')},"<td><button class="btn btn-primary btn-companion" type="button" onclick="renderPortalPartInfo($(this),'+@PortalPart_5633+');" data-target="#portal-part-'+@PortalPart_5633+'">Oppfølgingslogg &nbsp; | <span class="icon-vokal-help"></span></button></td>","")}<td><button class="btn btn-primary icon right large" type="button" onclick="renderReportBox($(this), '+@PortalPage_3840+');"><span class="icon-vokal-print"></span>Lag elevrapport</button></td></tr></tbody></table>'
FROM [app].[PortalPart] pt
JOIN [app].[LT_PortalPart] lt ON lt.[PortalPartID] = pt.[PortalPartID]
 AND pt.[MasterID] IN ('51F23E8A-1F2A-4730-BC75-E2BB3EB32EC8','D614FB12-106A-4A83-BCFD-36675B1810AF');
--#endregion PortalPart 5609,5610

--#region Remove old PortalParts
DELETE acc
FROM [app].[PortalPart] pt
JOIN [dbo].[AccessGeneric] acc ON acc.[TableTypeID] = 28 AND acc.[Elementid] = pt.[PortalPartID]
 AND pt.[MasterID] IN ('C719804E-DA6B-4500-97DC-CA734D4BA618','AAC82A43-0723-48F7-A668-A9C3BC2BCC29',
                       'A55E85C9-DA52-40A1-A9F7-AE2B594A8A34','516B3CF0-5AD6-4321-B1C9-96F72A047A92');

DELETE FROM [app].[PortalPart] WHERE [MasterID] IN ('C719804E-DA6B-4500-97DC-CA734D4BA618','AAC82A43-0723-48F7-A668-A9C3BC2BCC29',
                                                    'A55E85C9-DA52-40A1-A9F7-AE2B594A8A34','516B3CF0-5AD6-4321-B1C9-96F72A047A92')
--#endregion Remove old PortalParts

--#region Remove accesses
DELETE acc
FROM [app].[PortalPart] pt
JOIN [dbo].[AccessGeneric] acc ON acc.[TableTypeID] = 28 AND acc.[Elementid] = pt.[PortalPartID]
 AND pt.[MasterID] IN ('4B067F0F-80C6-4BCE-AB53-4A8A47857B0B','F1F73C47-3FB7-40E2-9C97-FAD4E1AA917E',
                       '51F23E8A-1F2A-4730-BC75-E2BB3EB32EC8','D614FB12-106A-4A83-BCFD-36675B1810AF')
JOIN [dbo].[AccessGroup] ag ON ag.[MasterID] = 'BAFD48E5-361E-4CF9-ACBD-FA993EA254C0'
--#endregion Remove accesses

--#endregion Portal

--#region ListCommand

--#region Update current
UPDATE lt SET lt.[Name] = N'Stafettlogg', lt.[Text] = N'Stafettlogg &nbsp; | <span class="icon-vokal-help"></span>'
FROM [list].[ItemListCommand] c
JOIN [list].[LT_ItemListCommand] lt ON lt.[ItemListCommandID] = c.[ItemListCommandID]
 AND c.[MasterID] IN ('6DC32B80-BB6E-4E8E-99B1-6D42A53A5979','1C826108-5F56-4A8A-AE0C-83CFBAD845F0','3F253200-9B97-4121-9303-BF2F846D4A18',
                      '1AC8B4BD-64BC-4727-8B36-E2FC5955DA41','CA95FB60-9916-4172-AA03-22C7ED5DE120','82CD47FD-293D-41C1-A2AD-8C03F0079A12');
--#endregion Update current

--#region Add new
DECLARE @PortalPartID_5633_14303376019972 nvarchar(32)= '';
SELECT @PortalPartID_5633_14303376019972 = [pt].[PortalPartID]
FROM [app].[PortalPart] [pt]
WHERE [pt].[MasterID] = '41FC8963-2C15-489F-9591-6B42D786697F';
IF NOT EXISTS (SELECT 1 FROM [list].[ItemListCommand] [c] WHERE [c].[MasterID] = '512A4A2B-948A-4759-8E0F-09975DEF070A')
BEGIN
    DECLARE @ListCommand200_SortAfter int;
    SELECT @ListCommand200_SortAfter = c.[No] FROM [list].[ItemListCommand] c WHERE c.[MasterID] = '6DC32B80-BB6E-4E8E-99B1-6D42A53A5979';

    UPDATE c SET c.[No] = c.[No] + 1
    FROM [list].[ItemListCommand] c
    JOIN [list].[ItemList] l ON l.[ItemListID] = c.[ItemListID] AND l.[MasterID] = 'A6F5E879-610C-496E-A3B0-3CAB8331AE17' AND c.[No] > @ListCommand200_SortAfter;

    INSERT INTO [list].[ItemListCommand] ([ItemListID], [PlaceAbove], [PlaceBelow], [MultiCheckCommand], [ClientFunction], [DisabledInEmptyList], [CssClass], [Created], [No], [MasterID])
    SELECT [l].[ItemListID] AS [ItemListID], N'0' AS [PlaceAbove], N'1' AS [PlaceBelow], N'0' AS [MultiCheckCommand], N'renderPortalPartInfo('+'$(this)'+','+@PortalPartID_5633_14303376019972+');' AS [ClientFunction], N'0' AS [DisabledInEmptyList], N'btn btn-primary btn-companion' AS [CssClass], GETDATE() AS [Created], @ListCommand200_SortAfter + 1 AS [No], N'512A4A2B-948A-4759-8E0F-09975DEF070A' AS [MasterID]
    FROM [list].[ItemList] [l]
    WHERE [l].[MasterID] = 'A6F5E879-610C-496E-A3B0-3CAB8331AE17';
    DECLARE @ListCommandID_200 nvarchar(32)= SCOPE_IDENTITY();

    INSERT INTO [list].[LT_ItemListCommand] ([LanguageID], [ItemListCommandID], [Name], [Description], [Text], [TooltipText], [HotKey])
    SELECT l.[LanguageID], @ListCommandID_200 AS [ItemListCommandID], N'Oppfølgingslogg' AS [Name], N'' AS [Description], N'Oppfølgingslogg &nbsp; | <span class="icon-vokal-help"></span>' AS [Text], N'' AS [TooltipText], N'' AS [HotKey]
    FROM [dbo].[Language] l
    WHERE l.[LanguageID] IN (1,2,7);
END;

DECLARE @PortalPartID_5633_14411128815722 nvarchar(32)= '';
SELECT @PortalPartID_5633_14411128815722 = [pt].[PortalPartID]
FROM [app].[PortalPart] [pt]
WHERE [pt].[MasterID] = '41FC8963-2C15-489F-9591-6B42D786697F';
IF NOT EXISTS (SELECT 1 FROM [list].[ItemListCommand] [c] WHERE [c].[MasterID] = 'E568BCD9-D2B3-4515-9193-57A05D5119A2')
BEGIN
    DECLARE @ListCommand201_SortAfter int;
    SELECT @ListCommand201_SortAfter = c.[No] FROM [list].[ItemListCommand] c WHERE c.[MasterID] = '1C826108-5F56-4A8A-AE0C-83CFBAD845F0';

    UPDATE c SET c.[No] = c.[No] + 1
    FROM [list].[ItemListCommand] c
    JOIN [list].[ItemList] l ON l.[ItemListID] = c.[ItemListID] AND l.[MasterID] = 'FC3B97E9-55DD-47D8-9E34-F16A5E508E66' AND c.[No] > @ListCommand201_SortAfter;

    INSERT INTO [list].[ItemListCommand] ([ItemListID], [PlaceAbove], [PlaceBelow], [MultiCheckCommand], [ClientFunction], [DisabledInEmptyList], [CssClass], [Created], [No], [MasterID])
    SELECT [l].[ItemListID] AS [ItemListID], N'0' AS [PlaceAbove], N'1' AS [PlaceBelow], N'0' AS [MultiCheckCommand], N'renderPortalPartInfo('+'$(this)'+','+@PortalPartID_5633_14411128815722+');' AS [ClientFunction], N'0' AS [DisabledInEmptyList], N'btn btn-primary btn-companion' AS [CssClass], GETDATE() AS [Created], @ListCommand201_SortAfter + 1 AS [No], N'E568BCD9-D2B3-4515-9193-57A05D5119A2' AS [MasterID]
    FROM [list].[ItemList] [l]
    WHERE [l].[MasterID] = 'FC3B97E9-55DD-47D8-9E34-F16A5E508E66';
    DECLARE @ListCommandID_201 nvarchar(32)= SCOPE_IDENTITY();

    INSERT INTO [list].[LT_ItemListCommand] ([LanguageID], [ItemListCommandID], [Name], [Description], [Text], [TooltipText], [HotKey])
    SELECT l.[LanguageID], @ListCommandID_201 AS [ItemListCommandID], N'Oppfølgingslogg' AS [Name], N'' AS [Description], N'Oppfølgingslogg &nbsp; | <span class="icon-vokal-help"></span>' AS [Text], N'' AS [TooltipText], N'' AS [HotKey]
    FROM [dbo].[Language] l
    WHERE l.[LanguageID] IN (1,2,7);
END;

DECLARE @PortalPartID_5633_14441314774202 nvarchar(32)= '';
SELECT @PortalPartID_5633_14441314774202 = [pt].[PortalPartID]
FROM [app].[PortalPart] [pt]
WHERE [pt].[MasterID] = '41FC8963-2C15-489F-9591-6B42D786697F';
IF NOT EXISTS (SELECT 1 FROM [list].[ItemListCommand] [c] WHERE [c].[MasterID] = '72135FDA-39C7-4192-8413-E262A543F105')
BEGIN
    DECLARE @ListCommand202_SortAfter int;
    SELECT @ListCommand202_SortAfter = c.[No] FROM [list].[ItemListCommand] c WHERE c.[MasterID] = '3F253200-9B97-4121-9303-BF2F846D4A18';

    UPDATE c SET c.[No] = c.[No] + 1
    FROM [list].[ItemListCommand] c
    JOIN [list].[ItemList] l ON l.[ItemListID] = c.[ItemListID] AND l.[MasterID] = 'CD883953-CCDA-45B7-8A16-CED0C8D4904F' AND c.[No] > @ListCommand202_SortAfter;

    INSERT INTO [list].[ItemListCommand] ([ItemListID], [PlaceAbove], [PlaceBelow], [MultiCheckCommand], [ClientFunction], [DisabledInEmptyList], [CssClass], [Created], [No], [MasterID])
    SELECT [l].[ItemListID] AS [ItemListID], N'0' AS [PlaceAbove], N'1' AS [PlaceBelow], N'0' AS [MultiCheckCommand], N'renderPortalPartInfo('+'$(this)'+','+@PortalPartID_5633_14441314774202+');' AS [ClientFunction], N'0' AS [DisabledInEmptyList], N'btn btn-primary btn-companion' AS [CssClass], GETDATE() AS [Created], @ListCommand202_SortAfter + 1 AS [No], N'72135FDA-39C7-4192-8413-E262A543F105' AS [MasterID]
    FROM [list].[ItemList] [l]
    WHERE [l].[MasterID] = 'CD883953-CCDA-45B7-8A16-CED0C8D4904F';
    DECLARE @ListCommandID_202 nvarchar(32)= SCOPE_IDENTITY();

    INSERT INTO [list].[LT_ItemListCommand] ([LanguageID], [ItemListCommandID], [Name], [Description], [Text], [TooltipText], [HotKey])
    SELECT l.[LanguageID], @ListCommandID_202 AS [ItemListCommandID], N'Oppfølgingslogg' AS [Name], N'' AS [Description], N'Oppfølgingslogg &nbsp; | <span class="icon-vokal-help"></span>' AS [Text], N'' AS [TooltipText], N'' AS [HotKey]
    FROM [dbo].[Language] l
    WHERE l.[LanguageID] IN (1,2,7);
END;

DECLARE @PortalPartID_5633_14464266387572 nvarchar(32)= '';
SELECT @PortalPartID_5633_14464266387572 = [pt].[PortalPartID]
FROM [app].[PortalPart] [pt]
WHERE [pt].[MasterID] = '41FC8963-2C15-489F-9591-6B42D786697F';
IF NOT EXISTS (SELECT 1 FROM [list].[ItemListCommand] [c] WHERE [c].[MasterID] = '8CCBE306-CECF-4945-9C71-F084A709E4E8')
BEGIN
    DECLARE @ListCommand203_SortAfter int;
    SELECT @ListCommand203_SortAfter = c.[No] FROM [list].[ItemListCommand] c WHERE c.[MasterID] = '1AC8B4BD-64BC-4727-8B36-E2FC5955DA41';

    UPDATE c SET c.[No] = c.[No] + 1
    FROM [list].[ItemListCommand] c
    JOIN [list].[ItemList] l ON l.[ItemListID] = c.[ItemListID] AND l.[MasterID] = 'EA9FF8E5-BDAC-4E6F-A1FF-5D5A8E065E52' AND c.[No] > @ListCommand203_SortAfter;

    INSERT INTO [list].[ItemListCommand] ([ItemListID], [PlaceAbove], [PlaceBelow], [MultiCheckCommand], [ClientFunction], [DisabledInEmptyList], [CssClass], [Created], [No], [MasterID])
    SELECT [l].[ItemListID] AS [ItemListID], N'0' AS [PlaceAbove], N'1' AS [PlaceBelow], N'0' AS [MultiCheckCommand], N'renderPortalPartInfo('+'$(this)'+','+@PortalPartID_5633_14464266387572+');' AS [ClientFunction], N'0' AS [DisabledInEmptyList], N'btn btn-primary btn-companion' AS [CssClass], GETDATE() AS [Created], @ListCommand203_SortAfter + 1 AS [No], N'8CCBE306-CECF-4945-9C71-F084A709E4E8' AS [MasterID]
    FROM [list].[ItemList] [l]
    WHERE [l].[MasterID] = 'EA9FF8E5-BDAC-4E6F-A1FF-5D5A8E065E52';
    DECLARE @ListCommandID_203 nvarchar(32)= SCOPE_IDENTITY();

    INSERT INTO [list].[LT_ItemListCommand] ([LanguageID], [ItemListCommandID], [Name], [Description], [Text], [TooltipText], [HotKey])
    SELECT l.[LanguageID], @ListCommandID_203 AS [ItemListCommandID], N'Oppfølgingslogg' AS [Name], N'' AS [Description], N'Oppfølgingslogg &nbsp; | <span class="icon-vokal-help"></span>' AS [Text], N'' AS [TooltipText], N'' AS [HotKey]
    FROM [dbo].[Language] l
    WHERE l.[LanguageID] IN (1,2,7);
END;

DECLARE @PortalPartID_5633_14485291699752 nvarchar(32)= '';
SELECT @PortalPartID_5633_14485291699752 = [pt].[PortalPartID]
FROM [app].[PortalPart] [pt]
WHERE [pt].[MasterID] = '41FC8963-2C15-489F-9591-6B42D786697F';
IF NOT EXISTS (SELECT 1 FROM [list].[ItemListCommand] [c] WHERE [c].[MasterID] = 'B7D2930E-EE18-4C0D-9DF1-5EBB63DF8CD1')
BEGIN
    DECLARE @ListCommand204_SortAfter int;
    SELECT @ListCommand204_SortAfter = c.[No] FROM [list].[ItemListCommand] c WHERE c.[MasterID] = 'CA95FB60-9916-4172-AA03-22C7ED5DE120';

    UPDATE c SET c.[No] = c.[No] + 1
    FROM [list].[ItemListCommand] c
    JOIN [list].[ItemList] l ON l.[ItemListID] = c.[ItemListID] AND l.[MasterID] = 'AF4254CB-62A5-423B-ADC3-D568D1D34DB6' AND c.[No] > @ListCommand204_SortAfter;

    INSERT INTO [list].[ItemListCommand] ([ItemListID], [PlaceAbove], [PlaceBelow], [MultiCheckCommand], [ClientFunction], [DisabledInEmptyList], [CssClass], [Created], [No], [MasterID])
    SELECT [l].[ItemListID] AS [ItemListID], N'0' AS [PlaceAbove], N'1' AS [PlaceBelow], N'0' AS [MultiCheckCommand], N'renderPortalPartInfo('+'$(this)'+','+@PortalPartID_5633_14485291699752+');' AS [ClientFunction], N'0' AS [DisabledInEmptyList], N'btn btn-primary btn-companion' AS [CssClass], GETDATE() AS [Created], @ListCommand204_SortAfter + 1 AS [No], N'B7D2930E-EE18-4C0D-9DF1-5EBB63DF8CD1' AS [MasterID]
    FROM [list].[ItemList] [l]
    WHERE [l].[MasterID] = 'AF4254CB-62A5-423B-ADC3-D568D1D34DB6';
    DECLARE @ListCommandID_204 nvarchar(32)= SCOPE_IDENTITY();

    INSERT INTO [list].[LT_ItemListCommand] ([LanguageID], [ItemListCommandID], [Name], [Description], [Text], [TooltipText], [HotKey])
    SELECT l.[LanguageID], @ListCommandID_204 AS [ItemListCommandID], N'Oppfølgingslogg' AS [Name], N'' AS [Description], N'Oppfølgingslogg &nbsp; | <span class="icon-vokal-help"></span>' AS [Text], N'' AS [TooltipText], N'' AS [HotKey]
    FROM [dbo].[Language] l
    WHERE l.[LanguageID] IN (1,2,7);
END;

DECLARE @PortalPartID_5633_14505474229282 nvarchar(32)= '';
SELECT @PortalPartID_5633_14505474229282 = [pt].[PortalPartID]
FROM [app].[PortalPart] [pt]
WHERE [pt].[MasterID] = '41FC8963-2C15-489F-9591-6B42D786697F';
IF NOT EXISTS (SELECT 1 FROM [list].[ItemListCommand] [c] WHERE [c].[MasterID] = 'FE26AAAB-DB57-4F99-ACB9-5DF36B6E06E0')
BEGIN
    DECLARE @ListCommand205_SortAfter int;
    SELECT @ListCommand205_SortAfter = c.[No] FROM [list].[ItemListCommand] c WHERE c.[MasterID] = '82CD47FD-293D-41C1-A2AD-8C03F0079A12';

    UPDATE c SET c.[No] = c.[No] + 1
    FROM [list].[ItemListCommand] c
    JOIN [list].[ItemList] l ON l.[ItemListID] = c.[ItemListID] AND l.[MasterID] = 'ADB034E3-26E7-4332-A311-C0FCFA4262E4' AND c.[No] > @ListCommand205_SortAfter;

    INSERT INTO [list].[ItemListCommand] ([ItemListID], [PlaceAbove], [PlaceBelow], [MultiCheckCommand], [ClientFunction], [DisabledInEmptyList], [CssClass], [Created], [No], [MasterID])
    SELECT [l].[ItemListID] AS [ItemListID], N'0' AS [PlaceAbove], N'1' AS [PlaceBelow], N'0' AS [MultiCheckCommand], N'renderPortalPartInfo('+'$(this)'+','+@PortalPartID_5633_14505474229282+');' AS [ClientFunction], N'0' AS [DisabledInEmptyList], N'btn btn-primary btn-companion' AS [CssClass], GETDATE() AS [Created], @ListCommand205_SortAfter + 1 AS [No], N'FE26AAAB-DB57-4F99-ACB9-5DF36B6E06E0' AS [MasterID]
    FROM [list].[ItemList] [l]
    WHERE [l].[MasterID] = 'ADB034E3-26E7-4332-A311-C0FCFA4262E4';
    DECLARE @ListCommandID_205 nvarchar(32)= SCOPE_IDENTITY();

    INSERT INTO [list].[LT_ItemListCommand] ([LanguageID], [ItemListCommandID], [Name], [Description], [Text], [TooltipText], [HotKey])
    SELECT l.[LanguageID], @ListCommandID_205 AS [ItemListCommandID], N'Oppfølgingslogg' AS [Name], N'' AS [Description], N'Oppfølgingslogg &nbsp; | <span class="icon-vokal-help"></span>' AS [Text], N'' AS [TooltipText], N'' AS [HotKey]
    FROM [dbo].[Language] l
    WHERE l.[LanguageID] IN (1,2,7);
END;
--#endregion Add new

--#region Access for new commands
INSERT INTO [dbo].[AccessGeneric] ([TableTypeID], [Elementid], [Type], [AccessGroupID], [Created], [SiteID], [MasterID])
SELECT 36 AS [TableTypeID], c.[ItemListCommandID] AS [Elementid], 1 AS [Type], ag.[AccessGroupID], GETDATE() AS [Created], NULL AS [SiteID], NEWID() AS [MasterID]
FROM [dbo].[AccessGroup] ag
JOIN [list].[ItemListCommand] c ON ag.[MasterID] = 'E998DB79-E040-4EC6-8011-A77A5AEFFC1B'
 AND c.[MasterID] IN ('512A4A2B-948A-4759-8E0F-09975DEF070A','E568BCD9-D2B3-4515-9193-57A05D5119A2','72135FDA-39C7-4192-8413-E262A543F105',
                      '8CCBE306-CECF-4945-9C71-F084A709E4E8','B7D2930E-EE18-4C0D-9DF1-5EBB63DF8CD1','FE26AAAB-DB57-4F99-ACB9-5DF36B6E06E0')
WHERE NOT EXISTS (SELECT 1 FROM [dbo].[AccessGeneric] acc WHERE acc.[TableTypeID] = 36 AND acc.[AccessGroupID] = ag.[AccessGroupID]
                                                            AND acc.[Type] = 1 AND acc.[Elementid] = c.[ItemListCommandID]);
--#endregion Access for new commands

--#endregion ListCommand

------------------------------------------------------------------------------------------------------------------------------------
--#region Fix text PortalPart 5606
UPDATE lt SET lt.[Text] = N'<div class="modal-header">
    <button data-dismiss="modal" class="close" type="button"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
</div>
<div class="modal-body">
    <img class="bg-image" src="{getBaseUrl()}/Content/Styles/Themes/Magnum/Images/bg-companion.png" alt="companion" />
    <p>Tidlig innsats krever kartlegging og forebyggende innsatser tidlig i risikoforløpet. Ved bekymring eller undring for barn og unge iverksettes tiltak i og mellom tjenestene. Stafettloggen systematiserer arbeidet og involverer foreldrene på et tidlig stadium.</p>
</div>
<div class="col-xs-12">
    <button class="btn btn-violet pull-right" type="button" onclick="goCompanion($(this),''http://systemtest-relaylog-no-www.conexus.no/Account?{bracket(0)}extId={bracket(1)}'');">Start her...</button>
</div>'
FROM [app].[PortalPart] pt
JOIN [app].[LT_PortalPart] lt ON lt.[PortalPartID] = pt.[PortalPartID] AND pt.[MasterID] = '4ADF301D-144D-4D12-AB24-D61DDF994AA8';
--#endregion Fix text PortalPart 5606
--#endregion 121. CE-2267 Companion Button
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
--#region 122. CE-2327 Class List Tooltip Resize
-- Dev: David
-- Feature: Class list
-- CE-2327 [ClassList] Tooltip be resized when hovering mouse over after moving to other department
UPDATE lt SET lt.[HeaderTooltipText] = '', lt.[HeaderText] = N'<i class="sort-setting icon-vokal-setting blue no-push-right" onclick="showSortSettings($(this))" title="Sorteringsinnstillinger"></i>'
FROM [list].[ItemListField] f
JOIN [list].[LT_ItemListField] lt ON lt.[ItemListFieldID] = f.[ItemListFieldID] AND f.[MasterID] = '16C6DEFB-00E9-433A-AB5A-350ACD0BE807' AND lt.[LanguageID] = 1;

--#endregion 122. CE-2327 Class List Tooltip Resize
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
-------------------------------------------------------------------------------------------------------------------
PRINT 'Setting database version'
/*
Start of versioning database-level extended properties.
*/
BEGIN
    DECLARE @DB_VERSION varchar(32), @ConceptName nvarchar(128), @ReleaseScriptName nvarchar(4000)
    SET @DB_VERSION = '6.12.007.001.00'
    SET @ConceptName = 'Engage Main'
    SET @ReleaseScriptName = N'v' + @DB_VERSION + N'-03. Engage Data Update Since 2019-02-21'

    IF EXISTS (SELECT 1 FROM fn_listextendedproperty(N'cxStudio Database Version', NULL, NULL, NULL, NULL, NULL, NULL))
        EXEC sp_dropextendedproperty N'cxStudio Database Version', NULL, NULL, NULL, NULL, NULL, NULL
    EXEC sp_addextendedproperty N'cxStudio Database Version', @DB_VERSION, NULL, NULL, NULL, NULL, NULL, NULL
		
    IF EXISTS (SELECT 1 FROM fn_listextendedproperty(N'Script Name', NULL, NULL, NULL, NULL, NULL, NULL))
        EXEC sp_dropextendedproperty N'Script Name', NULL, NULL, NULL, NULL, NULL, NULL
    EXEC sp_addextendedproperty N'Script Name',  @ReleaseScriptName , NULL, NULL, NULL, NULL, NULL, NULL
END
/*
End of versioning database-level extended properties.
*/
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
IF EXISTS (SELECT * FROM #tmpErrors) ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT>0 BEGIN
PRINT 'The database update succeeded'
COMMIT TRANSACTION
END
ELSE PRINT 'The database update failed'
GO
DROP TABLE #tmpErrors
GO
SET NOEXEC OFF
GO
-------------------------------------------------------------------------------------------------------------------------
DECLARE @Current_DB_Version varchar(32), @CurrentConceptName nvarchar(128),
        @Required_DB_Version varchar(32), @RequiredConceptName nvarchar(128)

SET @Required_DB_Version = '6.12.007.001.00'
SET @RequiredConceptName = 'Engage Main'
PRINT 'This script is generated for executing on ' + @RequiredConceptName + ' version ' + @Required_DB_Version

SELECT @Current_DB_Version = convert(nvarchar(max),value) FROM fn_listextendedproperty(N'cxStudio Database Version', NULL, NULL, NULL, NULL, NULL, NULL)
SELECT @CurrentConceptName = convert(nvarchar(max),value) FROM fn_listextendedproperty(N'Concept Name', NULL, NULL, NULL, NULL, NULL, NULL)

IF @CurrentConceptName != @RequiredConceptName OR @Current_DB_Version NOT LIKE @Required_DB_Version
BEGIN
    PRINT 'Incorrect solution or cxStudio Database version'
    SET NOEXEC ON
END
GO
-------------------------------------------------------------------------------------------------------------------------
IF EXISTS (SELECT * FROM tempdb..sysobjects WHERE id=OBJECT_ID('tempdb..#tmpErrors')) DROP TABLE #tmpErrors
GO
CREATE TABLE #tmpErrors (Error int)
GO
SET XACT_ABORT ON
GO
SET TRANSACTION ISOLATION LEVEL SERIALIZABLE
GO
BEGIN TRANSACTION
GO
----------------------------------------------------------------------------------------------------------------
--#region 125. Restrict Archetype To TableType

--#region Fix bad data
UPDATE d SET d.[ArchetypeID] = arch.[ArchetypeID]
FROM [org].[Department] d
JOIN [dbo].[Archetype] ar ON ar.[ArchetypeID] = d.[ArchetypeID] AND ar.[TableTypeID] <> 5
JOIN [org].[DT_D] dtd ON dtd.[DepartmentID] = d.[DepartmentID] AND dtd.[DepartmentTypeID] = 36
JOIN [dbo].[Archetype] arch ON arch.[CodeName] = 'School' AND arch.[TableTypeID] = 5;
--#endregion Fix bad data

--#region NSY
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[prc_VOKAL_startNY]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [dbo].[prc_VOKAL_startNY] AS' 
END
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
/*
    2016-10-10 Sarah:   Remove column Status on User, Department and Result tables
    2016-10-31 Sarah:   Hot fix: Set limit length of string which is used to update UserName
    2017-05-24 Sarah:   Insert ArchetypeID, EntityStatusID, EntityStatusReasonID when insert a new UserGroup
    2018-04-24 Ray:     Switch to use archetype learner instead of usertype student
    2018-06-19 Ray:     Clear UserFavorite
    2018-07-06 Ray:     Replace UG_U by UGMember
    2018-07-19 Ray:     Improve performance for Clear UserFavorite
    2018-08-10 Ray:     Fix UserFavourite left over after NSY, fix for class and school
                        Fix DT_UG of classes with multiple levels
    2019-02-28 Ray:     Set archetype of UserGroups to Archived Class
*/
ALTER PROCEDURE [dbo].[prc_VOKAL_startNY]
(
    @PeriodID           int,
    @HDID               int,
    @DepartmentTypeID   int,
    @DT_RTID            int,
    @UT_RTID            int,
    @UT_AVG             int,
    @ErrorMessage       nvarchar(MAX) out,
    @Sync               bit = 0,
    @SiteID             int = 5,
    @ExecutedByUserID   int = NULL
)
AS
BEGIN TRAN
    SET NOCOUNT ON
    SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED
    DECLARE @DepartmentID int, @Name nvarchar(256), @UGID int, @ParentDepID int, @UserID int, @OName varchar(10), @NName varchar(10), @Trinn varchar(200), @Count int,
            @C1 int, @C2 int, @Result int= 0, -- no errors
            @PropFileID int, @PROP_ZipFile int= NULL, @PROP_SinglePDF int= NULL, @EntityStatusID_Active int= 0, @EntityStatusID_Deactive int= 0,
            @EntityStatusReasonID_RelatedObject int, @EntityStatusReasonID_Active int, @ArchetypeID_ArchivedClass int= 0, @ArchetypeID_Learner int = 0;
    DECLARE @EventType_ForCleanUp int;
    DECLARE @CleanupEventType TABLE ([EventTypeID] int);

    --DECLARE @StartTime datetime = getdate()
    SELECT @PROP_ZipFile    = [sp].[Value] FROM [app].[SiteParameter] sp WHERE sp.[SiteID] = @SiteID AND [sp].[Key] = 'Platform.NewSchoolYear.ZipProp'
    SELECT @PROP_SinglePDF  = [sp].[Value] FROM [app].[SiteParameter] sp WHERE sp.[SiteID] = @SiteID AND [sp].[Key] = 'Platform.NewSchoolYear.ZipSLGProp'

    SELECT @EntityStatusID_Active = EntityStatusID FROM dbo.EntityStatus WHERE CodeName = N'Active'
    SELECT @EntityStatusID_Deactive = EntityStatusID FROM dbo.EntityStatus WHERE CodeName = N'Deactive'
    SELECT @EntityStatusReasonID_RelatedObject = [EntityStatusReasonID] FROM [EntityStatusReason] WHERE [CodeName] = 'Deactive_SetByRelatedObject'
    SELECT @EntityStatusReasonID_Active = EntityStatusReasonID FROM dbo.EntityStatusReason WHERE CodeName = N'Active_None'
    SELECT @ArchetypeID_ArchivedClass = ArchetypeID FROM dbo.Archetype WHERE CodeName = N'ArchivedClass'
    SELECT @ArchetypeID_Learner = ArchetypeID FROM dbo.Archetype WHERE CodeName = N'Learner'
    SELECT @EventType_ForCleanUp = et.[EventTypeID] FROM [log].[EventType] et WHERE et.[MasterID] = '72150482-FDBC-4425-8317-E9EB472D07EE'; -- ForCleanUp
    
    INSERT INTO @CleanupEventType ([EventTypeID])
    SELECT et.[EventTypeID]
    FROM [log].[EventType] et
    WHERE et.[CodeName] IN ('VIEW_USER_INFORMATION','USER_CREATED','RESULT_CREATED','RESULT_STATUS_CHANGE','RESULT_IN_PROGRESS','RESULT_DELETED','RESULT_VIEWED','RESULT_UPDATED');

    BEGIN TRY
        -- temporary table with old and new type's Department and User Type's. Name fields are used to change the class name from, for example. 2A to 3A.
        CREATE TABLE #Map
        (
            OID        int,
            OName    varchar(10),
            NID        int,
            NName    varchar(10),
            EntityStatusID int,
            Deleted DATETIME
        )

        -- loading DepartmentId to school according to HDID.
        SELECT @ParentDepID = DepartmentID FROM org.H_D WHERE HDID = @HDID
        
        -- Delete previous schoolyear zip files
        DECLARE @PropFileTable TABLE ([PropFileID] int)
        DELETE pv OUTPUT [DELETED].[PropFileID] INTO @PropFileTable ([PropFileID])
        FROM [prop].[PropFile] pf JOIN [prop].[PropValue] pv ON pf.[PropFileId] = pv.[PropFileID] AND pv.[PropertyID] IN (@PROP_ZipFile, @PROP_SinglePDF) AND pv.[ItemID] = @ParentDepID

        DELETE pf FROM [prop].[PropFile] pf JOIN @PropFileTable t ON pf.[PropFileId] = t.[PropFileID]
        
        -- Remove graduated student from previous schoolyear
        UPDATE org.[User] SET EntityStatusReasonID = @EntityStatusReasonID_RelatedObject, EntityStatusID = @EntityStatusID_Deactive, [UserName] = SUBSTRING('DEL_' + cast([UserID] AS VARCHAR(32)) + '_'+ [UserName],1,128)
        FROM  org.[User] u
        WHERE DepartmentID = @ParentDepID 
          AND EXISTS (SELECT 1 FROM org.UT_U utu WHERE utu.UserTypeID = @UT_AVG AND utu.UserID = u.UserID)
        
        -- stock cursor that runs through all school classes
        DECLARE department_cursor CURSOR LOCAL FAST_FORWARD READ_ONLY FOR        
        -- get list of child active department id and name of current department and must have department type
        SELECT DISTINCT H.DepartmentID, Name FROM org.H_D H
        INNER JOIN org.Department D on H.DepartmentID = D.DepartmentID
        WHERE ParentID = @HDID AND H.DepartmentID IN (SELECT DepartmentID FROM org.DT_D WHERE DepartmentTypeID = @DepartmentTypeID)
        AND D.EntityStatusID = @EntityStatusID_Active AND D.Deleted IS NULL AND H.Deleted = 0
        
        OPEN department_cursor
        FETCH NEXT FROM department_cursor INTO @DepartmentID, @Name

        WHILE (@@FETCH_STATUS = 0)
        BEGIN
            -- clear the temporary table        
            DELETE FROM #Map  
            
            -- create a User Group with class id and current periodeID (created also for deleted branches)
            -- print 'create a User Group with class id and current periodeID (created also for deleted branches)'
            INSERT org.Usergroup(OwnerID, DepartmentID, Name, Description, ExtID, PeriodID, ArchetypeID, EntityStatusID, EntityStatusReasonID)
            SELECT OwnerID, @ParentDepID, Name, Description, CAST(DepartmentID As varchar(12)), @PeriodID, @ArchetypeID_ArchivedClass, @EntityStatusID_Active, @EntityStatusReasonID_Active From org.Department
            WHERE DepartmentID = @DepartmentID
            SET @UGID = scope_identity()
            
            -- add all users into created User Group for its class.
            -- Adds even users who are soft-deleted with status <0
            -- print 'legger alle brukerne i opprettet userGroup for sin klasse'
            INSERT INTO [org].[UGMember] ([UserGroupID], [UserID], [Created], [CreatedBy], [LastUpdated], [LastUpdatedBy],
                                          [EntityStatusID], [EntityStatusReasonID], [CustomerID], [PeriodID], [ExtID], [Deleted])
            SELECT @UGID AS [UserGroupID], u.[UserID], GETDATE() AS [Created], @ExecutedByUserID AS [CreatedBy], GETDATE() AS [LastUpdated], @ExecutedByUserID AS [LastUpdatedBy],
                   @EntityStatusID_Active AS [EntityStatusID], @EntityStatusReasonID_Active AS [EntityStatusReasonID], u.[CustomerID], @PeriodID, '' AS [ExtID], NULL AS [Deleted]
            FROM org.[User] u WHERE u.[DepartmentID] = @DepartmentID;
            
            -- create the link between class User Group and class Department type before class has new type Department's
            -- the type of relationship to object mapping table is for new school year for the Department Type (opprykksmappinger from eg.
            -- 2A to 3A.
            -- print 'create the link between class User Group and class Department type before class has new type Department's'
            INSERT org.DT_UG(DepartmentTypeID, UserGroupID)
            SELECT dtd.[DepartmentTypeID], @UGID
            FROM org.[DT_D] dtd
            WHERE dtd.[DepartmentID] = @DepartmentID
              AND dtd.[DepartmentTypeID] IN (SELECT om.[FromID] FROM [dbo].[ObjectMapping] om WHERE om.[RelationTypeID] = @DT_RTID
                                             UNION
                                             SELECT om.[ToID] FROM [dbo].[ObjectMapping] om WHERE om.[RelationTypeID] = @DT_RTID); 

            -- Clear UserFavorite
            DELETE uf
            FROM [org].[UserFavorite] uf
            WHERE uf.[DepartmentID] = @DepartmentID;
        
            IF @EventType_ForCleanUp > 0
            BEGIN
                UPDATE e SET e.[EventTypeID] = @EventType_ForCleanUp
                FROM [log].[Event] e
                JOIN @CleanupEventType et ON et.[EventTypeID] = e.[EventTypeID] AND e.[DepartmentID] = @DepartmentID;
            END
        
            -- If you do not want to block the move as putting one @ Sync = 0 
            IF @Sync = 0
            BEGIN                
                SELECT @C1 = COUNT(*) FROM org.DT_D DT
                WHERE DT.DepartmentID = @DepartmentID
                    AND (DepartmentTypeID IN (SELECT FromID FROM ObjectMapping WHERE RelationTypeID = @DT_RTID)
                    OR  DepartmentTypeID IN (SELECT ToID FROM ObjectMapping WHERE RelationTypeID = @DT_RTID))
                
                SELECT @C2 = COUNT(*) FROM org.DT_D DT
                WHERE DT.DepartmentID = @ParentDepID
                    AND (DepartmentTypeID IN (SELECT FromID FROM ObjectMapping WHERE RelationTypeID = @DT_RTID)
                    OR  DepartmentTypeID IN (SELECT ToID FROM ObjectMapping WHERE RelationTypeID = @DT_RTID))
                
                IF @C1 <> @C2
                BEGIN
                    -- put into the temp-table mappings from the current step to the next step promotion, ex. 2 to 3 steps,
                    -- and ExtID'ene the old and the new Department Type of class.
                    -- NB! Steps must be checked to school at all be picked out from the mapping table.
                    --     Classes that are being tried moved the school steps, losing Department Type of step,
                    --     and be soft-deleted.
                    -- print 'put into the temp-table mappings from the current step to the next step promotion, ex. 2 to 3 steps'
                    INSERT #Map (OID, OName, NID, NName)
                    SELECT FromID, DT1.ExtID, ToID, DT2.ExtID
                    FROM ObjectMapping O
                    INNER JOIN org.DT_D DT ON DT.DepartmentTypeID = O.FromID AND DT.DepartmentID = @DepartmentID
                    INNER JOIN org.DepartmentType DT1 ON O.FromID = DT1.DepartmentTypeID
                    INNER JOIN org.DepartmentType DT2 ON O.ToID = DT2.DepartmentTypeID
                    WHERE O.RelationTypeID = @DT_RTID AND 
                        O.ToID IN (SELECT DepartmentTypeID FROM org.DT_D WHERE DepartmentID = @ParentDepID)
                                        
                    -- Delete all the Department's type of class that is mapped as a step
                    -- print 'Delete all the Department's type of class that is mapped as a step'
                    DELETE org.DT_D WHERE DepartmentID = @DepartmentID 
                        AND (DepartmentTypeID IN (SELECT FromID FROM ObjectMapping WHERE RelationTypeID = @DT_RTID)
                        OR  DepartmentTypeID IN (SELECT ToID FROM ObjectMapping WHERE RelationTypeID = @DT_RTID))
                                                
                    -- there are mappings to the next level in the class at all?
                    SELECT @Count = Count(*) FROM #Map
                    -- If not
                    IF @Count = 0
                    BEGIN
                        -- no mappings, set-class soft-deleted
                        -- 10B -> new step does not exist when there is a departure class, so soft-delete class)
                        -- NB! should also H_D soft-delete?
                        -- print 'no mappings, set-class soft-deleted'
                        UPDATE org.Department SET EntityStatusReasonID = @EntityStatusReasonID_RelatedObject, EntityStatusID = @EntityStatusID_Deactive WHERE DepartmentID = @DepartmentID            
                        UPDATE org.H_D SET Deleted = 1 WHERE DepartmentID = @DepartmentID
                    END
                    ELSE
                    BEGIN
                        -- enter new stage in class from temporary table
                        -- print 'enter new stage in class from temporary table'
                        INSERT org.DT_D
                        SELECT NID, @DepartmentID FROM #Map
                    
                        /* Fix Name */
                        -- or old and new names as defined in the mapping table
                        SELECT TOP 1 @Nname = NName, @OName = OName FROM #Map
                        
                        -- if the old name from the mapping table for the current class
                        IF CHARINDEX(@OName, @Name) = 1 AND dbo.IsInteger(SUBSTRING(@Name,LEN(@OName) + 1,1)) <> 1
                        BEGIN
                            -- replacing old example 9 by 10 and add the rest of the old class name,
                            -- it is assumed that the class names starting with digits.
                            SET @Name = REPLACE(LEFT(@Name, LEN(@OName)), @OName, @Nname) + SUBSTRING(@Name, LEN(@OName) + 1, 1000)
                            
                            -- sets new class name of Department
                            -- print 'sets new class name of Department'
                            UPDATE org.Department
                            SET Name = @Name
                            WHERE DepartmentID = @DepartmentID
                            
                            -- ExtID also update with a new class name, very important for classes that have integration (Locked = 1)
                            UPDATE org.Department
                            SET ExtID = SUBSTRING(ExtID,1,CHARINDEX(':',ExtID,1)) + [Name]
                            WHERE DepartmentID = @DepartmentID AND Locked = 1 AND CHARINDEX(':',ExtID,0) > 0

                            UPDATE org.Department
                            SET ExtID = SUBSTRING([ExtID], 1, LEN([ExtID]) - CHARINDEX('_', REVERSE([ExtID])) + 1) + [Name]
                            WHERE DepartmentID = @DepartmentID AND Locked = 1 AND CHARINDEX('_',ExtID,0) > 0
                        END
                    END
                END
            END    
            
            FETCH NEXT FROM department_cursor INTO @DepartmentID, @Name            
        END        
        
        -- Clear UserFavorite
        DELETE uf
        FROM [org].[UserFavorite] uf
        WHERE uf.[DepartmentID] = @ParentDepID;
        
        IF @EventType_ForCleanUp > 0
        BEGIN
            UPDATE e SET e.[EventTypeID] = @EventType_ForCleanUp
            FROM [log].[Event] e
            JOIN @CleanupEventType et ON et.[EventTypeID] = e.[EventTypeID] AND e.[DepartmentID] = @ParentDepID;
        END

        SELECT @DepartmentID = DepartmentID FROM org.H_D WHERE HDID = @HDID
        
        DELETE FROM #Map  
        
        INSERT #Map (OID, OName, NID, NName, EntityStatusID, [Deleted])
        SELECT U.UserID, '', UT.UserTypeID, '', U.EntityStatusID, U.Deleted
        FROM org.[User] U
        INNER JOIN org.H_D HD ON U.DepartmentID = HD.DepartmentID AND U.[ArchetypeID] = @ArchetypeID_Learner
        INNER JOIN org.UT_U UT ON U.UserID = UT.UserID 
        WHERE HD.Path LIKE '%\' + CAST(@HDID as varchar(12)) + '\%' 
        AND (UT.UserTypeID IN (SELECT FromID FROM ObjectMapping WHERE RelationTypeID = @UT_RTID)
            OR  UT.UserTypeID IN (SELECT ToID FROM ObjectMapping WHERE RelationTypeID = @UT_RTID))
        AND NOT EXISTS (SELECT 1 FROM org.UT_U utu WHERE utu.UserID = u.UserID AND utu.UserTypeID = @UT_AVG)
        
        -- Assign graduated usertype to last grade users of the department
        INSERT org.UT_U    
        SELECT DISTINCT @UT_AVG, M.OID  
        FROM #Map M 
        INNER JOIN ObjectMapping O ON M.NID = O.FromID  AND RelationTypeID = @UT_RTID
        WHERE NOT O.ToID IN (SELECT ToID FROM ObjectMapping O
                             INNER JOIN org.DT_D DT ON O.FromID = DT.DepartmentTypeID AND DT.DepartmentID = @DepartmentID 
                             WHERE RelationTypeID = 1 AND O.FromTableTypeID = 27 AND O.ToTableTypeID = 26)
            AND NOT M.OID IN (SELECT UserID FROM org.UT_U WHERE UserTypeID = @UT_AVG)
                            
        -- Assign graduated usertype to last grade (10th grade) users
        INSERT org.UT_U    
        SELECT DISTINCT @UT_AVG, M.OID  
        FROM #Map M 
        WHERE NOT M.NID IN (SELECT FromID FROM ObjectMapping WHERE RelationTypeID = @UT_RTID)
        AND NOT EXISTS (SELECT 1 FROM org.UT_U UTU WHERE UTU.UserTypeID=@UT_AVG AND UTU.UserID=M.OID)
        
        -- Upgrade not deleted users to higher grade
        INSERT org.UT_U    
        SELECT DISTINCT O.ToID, U.UserID
        FROM org.[User] U
        INNER JOIN org.H_D HD ON U.DepartmentID = HD.DepartmentID AND u.EntityStatusID = @EntityStatusID_Active AND u.Deleted IS NULL AND U.[ArchetypeID] = @ArchetypeID_Learner
        INNER JOIN org.UT_U UT ON U.UserID = UT.UserID 
        INNER JOIN ObjectMapping O ON O.RelationTypeID = @UT_RTID AND UT.UserTypeID = O.FromID
        WHERE HD.Path LIKE '%\' + CAST(@HDID as varchar(12)) + '\%' 
        AND NOT U.UserID IN (SELECT UserID FROM org.UT_U WHERE UserTypeID = O.ToID)

        -- Move graduated users to parent department
        UPDATE org.[User] 
        SET DepartmentID = @DepartmentID 
        FROM org.[User] U
        INNER JOIN org.H_D HD ON U.DepartmentID = HD.DepartmentID
        INNER JOIN org.UT_U UTE ON U.UserID = UTE.UserID AND UTE.UserTypeID = @UT_AVG
        WHERE HD.Path LIKE '%\' + CAST(@HDID as varchar(12)) + '\%'
        
        DECLARE @GraduatedUser TABLE (UserID int)
        INSERT INTO @GraduatedUser(UserID) SELECT u.UserID FROM org.[User] u 
        WHERE u.DepartmentID = @DepartmentID AND EXISTS (SELECT 1 FROM org.UT_U utu WHERE utu.UserID = u.UserID AND utu.UserTypeID = @UT_AVG)
        
        -- Clean original school property of graduated students
        DECLARE @PROP_Move_School int = 17
        SELECT @PROP_Move_School = [sp].[Value] FROM [app].[SiteParameter] sp WHERE sp.[SiteID] = @SiteID AND [sp].[Key] = 'Platform.NewSchoolYear.ReceivingDepartmentProp'
        DELETE pv FROM [prop].[PropValue] pv WHERE [pv].[PropertyID] = @PROP_Move_School AND [pv].[ItemID] IN (SELECT UserID FROM @GraduatedUser)        

        SELECT * FROM @GraduatedUser
        
        -- Remove graduated usertype for those who were soft deleted
        DELETE org.UT_U WITH (ROWLOCK)
        FROM org.UT_U utu
        WHERE utu.UserID IN (SELECT m.OID FROM #Map m WHERE m.EntityStatusID = @EntityStatusID_Deactive OR m.Deleted IS NOT NULL)
          AND utu.UserTypeID = @UT_AVG
        
        -- Remove users' old grade except deleted ones
        DELETE org.UT_U WITH (ROWLOCK)
        FROM org.UT_U UT
        INNER JOIN #Map M ON UT.UserID = M.OID AND UT.UserTypeID = M.NID AND M.EntityStatusID = @EntityStatusID_Active AND M.Deleted IS NULL
        WHERE UT.UserID IN (SELECT M.OID FROM #Map M)
    END TRY
    BEGIN CATCH        
        Set @Result = @@Error
        SET @ErrorMessage = ERROR_MESSAGE()
        GOTO RollbackResult
    END CATCH

CommitResult:
    IF CURSOR_STATUS('global','department_cursor') = 1
    BEGIN
        CLOSE department_cursor
        DEALLOCATE department_cursor
    END
    
    IF Object_ID('tempdb..#Map') IS NOT NULL DROP TABLE #Map
    COMMIT TRAN    
    SET @ErrorMessage = ''
    RETURN @Result
    
RollbackResult:        
    IF CURSOR_STATUS('global','department_cursor') = 1
    BEGIN
        CLOSE department_cursor
        DEALLOCATE department_cursor
    END    
    --print 'Update child department : ' + rtrim(cast(datediff(ms,@StartTime,getdate()) as char(10))) + ' mili seconds!'            
    IF Object_ID('tempdb..#Map') IS NOT NULL DROP TABLE #Map
    ROLLBACK TRAN    
    RETURN @Result

GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
--#endregion NSY

--#endregion 125. Restrict Archetype To TableType
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
-------------------------------------------------------------------------------------------------------------------
PRINT 'Setting database version'
/*
Start of versioning database-level extended properties.
*/
BEGIN
    DECLARE @DB_VERSION varchar(32), @ConceptName nvarchar(128), @ReleaseScriptName nvarchar(4000)
    SET @DB_VERSION = '6.12.007.001.01'
    SET @ConceptName = 'Engage Main'
    SET @ReleaseScriptName = N'v' + @DB_VERSION + N'-03. Engage Data Update Since 2019-02-21'

    IF EXISTS (SELECT 1 FROM fn_listextendedproperty(N'cxStudio Database Version', NULL, NULL, NULL, NULL, NULL, NULL))
        EXEC sp_dropextendedproperty N'cxStudio Database Version', NULL, NULL, NULL, NULL, NULL, NULL
    EXEC sp_addextendedproperty N'cxStudio Database Version', @DB_VERSION, NULL, NULL, NULL, NULL, NULL, NULL
		
    IF EXISTS (SELECT 1 FROM fn_listextendedproperty(N'Script Name', NULL, NULL, NULL, NULL, NULL, NULL))
        EXEC sp_dropextendedproperty N'Script Name', NULL, NULL, NULL, NULL, NULL, NULL
    EXEC sp_addextendedproperty N'Script Name',  @ReleaseScriptName , NULL, NULL, NULL, NULL, NULL, NULL
END
/*
End of versioning database-level extended properties.
*/
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
IF EXISTS (SELECT * FROM #tmpErrors) ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT>0 BEGIN
PRINT 'The database update succeeded'
COMMIT TRANSACTION
END
ELSE PRINT 'The database update failed'
GO
DROP TABLE #tmpErrors
GO
SET NOEXEC OFF
GO