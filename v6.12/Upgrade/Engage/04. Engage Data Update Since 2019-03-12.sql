SET QUOTED_IDENTIFIER ON
GO
-------------------------------------------------------------------------------------------------------------------------
DECLARE @Current_DB_Version varchar(32), @CurrentConceptName nvarchar(128),
        @Required_DB_Version varchar(32), @RequiredConceptName nvarchar(128)

SET @Required_DB_Version = '6.12.009.000.%'
SET @RequiredConceptName = 'Engage Main'
PRINT 'This script is generated for executing on ' + @RequiredConceptName + ' version ' + @Required_DB_Version

SELECT @Current_DB_Version = convert(nvarchar(max),value) FROM fn_listextendedproperty(N'cxStudio Database Version', NULL, NULL, NULL, NULL, NULL, NULL)
SELECT @CurrentConceptName = convert(nvarchar(max),value) FROM fn_listextendedproperty(N'Concept Name', NULL, NULL, NULL, NULL, NULL, NULL)

IF @CurrentConceptName != @RequiredConceptName OR @Current_DB_Version NOT LIKE @Required_DB_Version
BEGIN
    PRINT 'Incorrect solution or cxStudio Database version'
    SET NOEXEC ON
END
GO
-------------------------------------------------------------------------------------------------------------------------
IF EXISTS (SELECT * FROM tempdb..sysobjects WHERE id=OBJECT_ID('tempdb..#tmpErrors')) DROP TABLE #tmpErrors
GO
CREATE TABLE #tmpErrors (Error int)
GO
SET XACT_ABORT ON
GO
SET TRANSACTION ISOLATION LEVEL SERIALIZABLE
GO
BEGIN TRANSACTION
GO
----------------------------------------------------------------------------------------------------------------
--#region 127. CE-2347 Hide Activity
-- Dev: Thomas
-- Feature: CE-2333 Hide activity
-- CE-2347 Hide an activity from slide-down popup

--#region Datasource 2164
IF NOT EXISTS (SELECT 1 FROM [list].[ItemListDataSource] [ds] WHERE [ds].[MasterID] = 'C20DF1DC-56CB-4DE6-9983-51C9D6089428')
BEGIN
    INSERT INTO [list].[ItemListDataSource] ([OwnerID], [FunctionName], [Created], [MasterID])
    SELECT [o].[OwnerID] AS [OwnerID], N'GetActivityListPopup' AS [FunctionName], GETDATE() AS [Created], N'C20DF1DC-56CB-4DE6-9983-51C9D6089428' AS [MasterID]
    FROM [org].[Owner] [o]
    WHERE [o].[MasterID] = '1C483E02-E808-42A7-8210-B11F4051D2E4';
    DECLARE @ItemListDataSourceID_2164 nvarchar(32)= SCOPE_IDENTITY();

    INSERT INTO [list].[LT_ItemListDataSource] ([LanguageID], [ItemListDataSourceID], [Name], [Description])
    SELECT l.[LanguageID], @ItemListDataSourceID_2164 AS [ItemListDataSourceID], N'Get activity list popup' AS [Name], N'' AS [Description]
    FROM [dbo].[Language] l
    WHERE l.[LanguageID] IN (1,2,7);

    IF NOT EXISTS (SELECT 1 FROM [list].[ItemListDataParameter] [dp] WHERE [dp].[MasterID] = 'A2AC603D-628B-4CD2-911C-DB4F05A514F1')
    BEGIN
        INSERT INTO [list].[ItemListDataParameter] ([ItemListDataSourceID], [DataType], [Type], [Mandatory], [DefaultValue], [ConstructionPattern], [Created], [MasterID])
        SELECT [ds].[ItemListDataSourceID] AS [ItemListDataSourceID], N'String' AS [DataType], N'PortalPartParam' AS [Type], N'0' AS [Mandatory], N'' AS [DefaultValue], N'' AS [ConstructionPattern], GETDATE() AS [Created], N'A2AC603D-628B-4CD2-911C-DB4F05A514F1' AS [MasterID]
        FROM [list].[ItemListDataSource] [ds]
        WHERE [ds].[MasterID] = 'C20DF1DC-56CB-4DE6-9983-51C9D6089428';
        DECLARE @ParamID_4808 nvarchar(32)= SCOPE_IDENTITY();

        INSERT INTO [list].[LT_ItemListDataParameter] ([LanguageID], [ItemListDataParameterID], [Name], [Description])
        SELECT l.[LanguageID], @ParamID_4808 AS [ItemListDataParameterID], N'XCategoryIds' AS [Name], N'' AS [Description]
        FROM [dbo].[Language] l
        WHERE l.[LanguageID] IN (1,2,7);
    END;

    IF NOT EXISTS (SELECT 1 FROM [list].[ItemListDataParameter] [dp] WHERE [dp].[MasterID] = 'ABF4E969-4E18-4145-8175-340FE56FA205')
    BEGIN
        INSERT INTO [list].[ItemListDataParameter] ([ItemListDataSourceID], [DataType], [Type], [Mandatory], [DefaultValue], [ConstructionPattern], [Created], [MasterID])
        SELECT [ds].[ItemListDataSourceID] AS [ItemListDataSourceID], N'String' AS [DataType], N'PortalPartParam' AS [Type], N'0' AS [Mandatory], N'' AS [DefaultValue], N'' AS [ConstructionPattern], GETDATE() AS [Created], N'ABF4E969-4E18-4145-8175-340FE56FA205' AS [MasterID]
        FROM [list].[ItemListDataSource] [ds]
        WHERE [ds].[MasterID] = 'C20DF1DC-56CB-4DE6-9983-51C9D6089428';
        DECLARE @ParamID_4810 nvarchar(32)= SCOPE_IDENTITY();

        INSERT INTO [list].[LT_ItemListDataParameter] ([LanguageID], [ItemListDataParameterID], [Name], [Description])
        SELECT l.[LanguageID], @ParamID_4810 AS [ItemListDataParameterID], N'XCategoryTrinnIds' AS [Name], N'' AS [Description]
        FROM [dbo].[Language] l
        WHERE l.[LanguageID] IN (1,2,7);
    END;

    IF NOT EXISTS (SELECT 1 FROM [list].[ItemListDataParameter] [dp] WHERE [dp].[MasterID] = '21E23CC2-489A-48D0-93FC-ADDC91A197BA')
    BEGIN
        INSERT INTO [list].[ItemListDataParameter] ([ItemListDataSourceID], [DataType], [Type], [Mandatory], [DefaultValue], [ConstructionPattern], [Created], [MasterID])
        SELECT [ds].[ItemListDataSourceID] AS [ItemListDataSourceID], N'Integer' AS [DataType], N'PortalPartParam' AS [Type], N'0' AS [Mandatory], N'' AS [DefaultValue], N'' AS [ConstructionPattern], GETDATE() AS [Created], N'21E23CC2-489A-48D0-93FC-ADDC91A197BA' AS [MasterID]
        FROM [list].[ItemListDataSource] [ds]
        WHERE [ds].[MasterID] = 'C20DF1DC-56CB-4DE6-9983-51C9D6089428';
        DECLARE @ParamID_4811 nvarchar(32)= SCOPE_IDENTITY();

        INSERT INTO [list].[LT_ItemListDataParameter] ([LanguageID], [ItemListDataParameterID], [Name], [Description])
        SELECT l.[LanguageID], @ParamID_4811 AS [ItemListDataParameterID], N'CurrentActivityId' AS [Name], N'' AS [Description]
        FROM [dbo].[Language] l
        WHERE l.[LanguageID] IN (1,2,7);
    END;

    IF NOT EXISTS (SELECT 1 FROM [list].[ItemListDataParameter] [dp] WHERE [dp].[MasterID] = 'BCFC2DFF-B177-48C8-B625-1E4D530D1668')
    BEGIN
        INSERT INTO [list].[ItemListDataParameter] ([ItemListDataSourceID], [DataType], [Type], [Mandatory], [DefaultValue], [ConstructionPattern], [Created], [MasterID])
        SELECT [ds].[ItemListDataSourceID] AS [ItemListDataSourceID], N'String' AS [DataType], N'PortalPartParam' AS [Type], N'0' AS [Mandatory], N'' AS [DefaultValue], N'' AS [ConstructionPattern], GETDATE() AS [Created], N'BCFC2DFF-B177-48C8-B625-1E4D530D1668' AS [MasterID]
        FROM [list].[ItemListDataSource] [ds]
        WHERE [ds].[MasterID] = 'C20DF1DC-56CB-4DE6-9983-51C9D6089428';
        DECLARE @ParamID_4812 nvarchar(32)= SCOPE_IDENTITY();

        INSERT INTO [list].[LT_ItemListDataParameter] ([LanguageID], [ItemListDataParameterID], [Name], [Description])
        SELECT l.[LanguageID], @ParamID_4812 AS [ItemListDataParameterID], N'AllActivityPartIds' AS [Name], N'' AS [Description]
        FROM [dbo].[Language] l
        WHERE l.[LanguageID] IN (1,2,7);
    END;

    IF NOT EXISTS (SELECT 1 FROM [list].[ItemListDataParameter] [dp] WHERE [dp].[MasterID] = '0C106AD3-4EFA-499D-A676-AE38B8B16E98')
    BEGIN
        INSERT INTO [list].[ItemListDataParameter] ([ItemListDataSourceID], [DataType], [Type], [Mandatory], [DefaultValue], [ConstructionPattern], [Created], [MasterID])
        SELECT [ds].[ItemListDataSourceID] AS [ItemListDataSourceID], N'Integer' AS [DataType], N'PortalPartParam' AS [Type], N'0' AS [Mandatory], N'' AS [DefaultValue], N'' AS [ConstructionPattern], GETDATE() AS [Created], N'0C106AD3-4EFA-499D-A676-AE38B8B16E98' AS [MasterID]
        FROM [list].[ItemListDataSource] [ds]
        WHERE [ds].[MasterID] = 'C20DF1DC-56CB-4DE6-9983-51C9D6089428';
        DECLARE @ParamID_4813 nvarchar(32)= SCOPE_IDENTITY();

        INSERT INTO [list].[LT_ItemListDataParameter] ([LanguageID], [ItemListDataParameterID], [Name], [Description])
        SELECT l.[LanguageID], @ParamID_4813 AS [ItemListDataParameterID], N'userId' AS [Name], N'' AS [Description]
        FROM [dbo].[Language] l
        WHERE l.[LanguageID] IN (1,2,7);
    END;

    IF NOT EXISTS (SELECT 1 FROM [list].[ItemListDataParameter] [dp] WHERE [dp].[MasterID] = '75A1717A-9BA4-4C87-AF62-19B1F9AEAFD2')
    BEGIN
        INSERT INTO [list].[ItemListDataParameter] ([ItemListDataSourceID], [DataType], [Type], [Mandatory], [DefaultValue], [ConstructionPattern], [Created], [MasterID])
        SELECT [ds].[ItemListDataSourceID] AS [ItemListDataSourceID], N'Boolean' AS [DataType], N'PortalPartParam' AS [Type], N'0' AS [Mandatory], N'' AS [DefaultValue], N'' AS [ConstructionPattern], GETDATE() AS [Created], N'75A1717A-9BA4-4C87-AF62-19B1F9AEAFD2' AS [MasterID]
        FROM [list].[ItemListDataSource] [ds]
        WHERE [ds].[MasterID] = 'C20DF1DC-56CB-4DE6-9983-51C9D6089428';
        DECLARE @ParamID_4814 nvarchar(32)= SCOPE_IDENTITY();

        INSERT INTO [list].[LT_ItemListDataParameter] ([LanguageID], [ItemListDataParameterID], [Name], [Description])
        SELECT l.[LanguageID], @ParamID_4814 AS [ItemListDataParameterID], N'IsOutsideReport' AS [Name], N'' AS [Description]
        FROM [dbo].[Language] l
        WHERE l.[LanguageID] IN (1,2,7);
    END;

    IF NOT EXISTS (SELECT 1 FROM [list].[ItemListDataParameter] [dp] WHERE [dp].[MasterID] = 'D7E8DF47-7190-44F8-9568-A2AB7640D914')
    BEGIN
        INSERT INTO [list].[ItemListDataParameter] ([ItemListDataSourceID], [DataType], [Type], [Mandatory], [DefaultValue], [ConstructionPattern], [Created], [MasterID])
        SELECT [ds].[ItemListDataSourceID] AS [ItemListDataSourceID], N'Integer' AS [DataType], N'PortalPartParam' AS [Type], N'0' AS [Mandatory], N'' AS [DefaultValue], N'' AS [ConstructionPattern], GETDATE() AS [Created], N'D7E8DF47-7190-44F8-9568-A2AB7640D914' AS [MasterID]
        FROM [list].[ItemListDataSource] [ds]
        WHERE [ds].[MasterID] = 'C20DF1DC-56CB-4DE6-9983-51C9D6089428';
        DECLARE @ParamID_4815 nvarchar(32)= SCOPE_IDENTITY();

        INSERT INTO [list].[LT_ItemListDataParameter] ([LanguageID], [ItemListDataParameterID], [Name], [Description])
        SELECT l.[LanguageID], @ParamID_4815 AS [ItemListDataParameterID], N'CurrentResultId' AS [Name], N'' AS [Description]
        FROM [dbo].[Language] l
        WHERE l.[LanguageID] IN (1,2,7);
    END;

    IF NOT EXISTS (SELECT 1 FROM [list].[ItemListDataParameter] [dp] WHERE [dp].[MasterID] = '2A5F72AD-E43A-485B-88FB-AB0AFF6B6532')
    BEGIN
        INSERT INTO [list].[ItemListDataParameter] ([ItemListDataSourceID], [DataType], [Type], [Mandatory], [DefaultValue], [ConstructionPattern], [Created], [MasterID])
        SELECT [ds].[ItemListDataSourceID] AS [ItemListDataSourceID], N'String' AS [DataType], N'PortalPartParam' AS [Type], N'0' AS [Mandatory], N'' AS [DefaultValue], N'' AS [ConstructionPattern], GETDATE() AS [Created], N'2A5F72AD-E43A-485B-88FB-AB0AFF6B6532' AS [MasterID]
        FROM [list].[ItemListDataSource] [ds]
        WHERE [ds].[MasterID] = 'C20DF1DC-56CB-4DE6-9983-51C9D6089428';
        DECLARE @ParamID_4816 nvarchar(32)= SCOPE_IDENTITY();

        INSERT INTO [list].[LT_ItemListDataParameter] ([LanguageID], [ItemListDataParameterID], [Name], [Description])
        SELECT l.[LanguageID], @ParamID_4816 AS [ItemListDataParameterID], N'HiddenActivityIds' AS [Name], N'' AS [Description]
        FROM [dbo].[Language] l
        WHERE l.[LanguageID] IN (1,2,7);
    END;

END;
--#endregion Datasource 2164

--#region PortalPage 3899
IF NOT EXISTS (SELECT 1 FROM [app].[PortalPage] [pp] WHERE [pp].[MasterID] = '532C0615-4BDB-448F-A16F-DAE54B56BE07')
BEGIN
    INSERT INTO [app].[PortalPage] ([OwnerID], [No], [Parameter], [ExtID], [Created], [MasterID])
    SELECT [o].[OwnerID] AS [OwnerID], N'135' AS [No], N'' AS [Parameter], N'' AS [ExtID], GETDATE() AS [Created], N'532C0615-4BDB-448F-A16F-DAE54B56BE07' AS [MasterID]
    FROM [org].[Owner] [o]
    WHERE [o].[MasterID] = '1C483E02-E808-42A7-8210-B11F4051D2E4';
    DECLARE @PortalPageID_3899 nvarchar(32)= SCOPE_IDENTITY();

    INSERT INTO [app].[LT_PortalPage] ([LanguageID], [PortalPageID], [Name], [Description])
    SELECT l.[LanguageID], @PortalPageID_3899 AS [PortalPageID], N'Independent portalparts' AS [Name], N'RenderToolbar=false' AS [Description]
    FROM [dbo].[Language] l
    WHERE l.[LanguageID] IN (1,2,7);

    DECLARE @DataSourceID_2164_10523722443061 nvarchar(32)= '';
    SELECT @DataSourceID_2164_10523722443061 = [ds].[ItemListDataSourceID]
    FROM [list].[ItemListDataSource] [ds]
    WHERE [ds].[MasterID] = 'C20DF1DC-56CB-4DE6-9983-51C9D6089428';
    IF NOT EXISTS (SELECT 1 FROM [app].[PortalPart] [pt] WHERE [pt].[MasterID] = 'C5CE6852-4881-41F6-B4D1-49B9B2465A26')
    BEGIN
        INSERT INTO [app].[PortalPart] ([PortalPageID], [PortalPartTypeID], [No], [CategoryID], [Settings], [BubbleID], [ReportPartID], [ReportID], [CssClass], [InlineStyle], [ActivityID], [SurveyID], [ExtID], [MasterID])
        SELECT [pp].[PortalPageID] AS [PortalPageID], N'43' AS [PortalPartTypeID], N'1' AS [No], NULL AS [CategoryID], N'<params>DataSourceId%3D'+@DataSourceID_2164_10523722443061+'%26HiddenActivityIds%3D'+'0'+'</params>' AS [Settings], NULL AS [BubbleID], NULL AS [ReportPartID], NULL AS [ReportID], N'col-lg-4 col-md-5 col-xs-6 bg-white box-floating popup-fixed no-border no-radius popup-header-all-activities' AS [CssClass], N'' AS [InlineStyle], NULL AS [ActivityID], NULL AS [SurveyID], N'' AS [ExtID], N'C5CE6852-4881-41F6-B4D1-49B9B2465A26' AS [MasterID]
        FROM [app].[PortalPage] [pp]
        WHERE [pp].[MasterID] = '532C0615-4BDB-448F-A16F-DAE54B56BE07';
        DECLARE @PortalPartID_5634 nvarchar(32)= SCOPE_IDENTITY();

        INSERT INTO [app].[LT_PortalPart] ([LanguageID], [PortalPartID], [Name], [Description], [Text], [TextAbove], [TextBelow], [HeaderText], [FooterText], [BeforeContentText], [AfterContentText])
        SELECT l.[LanguageID], @PortalPartID_5634 AS [PortalPartID], N'[PersonalPage] Activity list popup' AS [Name], N'' AS [Description], N'' AS [Text], N'' AS [TextAbove], N'' AS [TextBelow], N'' AS [HeaderText], N'' AS [FooterText], N'' AS [BeforeContentText], N'' AS [AfterContentText]
        FROM [dbo].[Language] l
        WHERE l.[LanguageID] IN (1,2,7);
    END;

END;
--#endregion PortalPage 3899

--#region Datasource 90
DECLARE @PortalPartID_5634_10451859986411 nvarchar(32)= '';
SELECT @PortalPartID_5634_10451859986411 = pt.[PortalPartID]
FROM [app].[PortalPart] pt
WHERE pt.[MasterID] = 'C5CE6852-4881-41F6-B4D1-49B9B2465A26'

IF NOT EXISTS (SELECT 1 FROM [list].[ItemListDataParameter] [dp] WHERE [dp].[MasterID] = '00B95BFC-794F-40C9-B1E6-21AEFC9981B3')
BEGIN
    INSERT INTO [list].[ItemListDataParameter] ([ItemListDataSourceID], [DataType], [Type], [Mandatory], [DefaultValue], [ConstructionPattern], [Created], [MasterID])
    SELECT [ds].[ItemListDataSourceID] AS [ItemListDataSourceID], N'Integer' AS [DataType], N'PortalPartParam' AS [Type], N'0' AS [Mandatory], N''+@PortalPartID_5634_10451859986411+'' AS [DefaultValue], N'' AS [ConstructionPattern], GETDATE() AS [Created], N'00B95BFC-794F-40C9-B1E6-21AEFC9981B3' AS [MasterID]
    FROM [list].[ItemListDataSource] [ds]
    WHERE [ds].[MasterID] = '4F99BE33-3793-403F-A061-2714A52E2B9F';
    DECLARE @ParamID_4817 nvarchar(32)= SCOPE_IDENTITY();

    INSERT INTO [list].[LT_ItemListDataParameter] ([LanguageID], [ItemListDataParameterID], [Name], [Description])
    SELECT l.[LanguageID], @ParamID_4817 AS [ItemListDataParameterID], N'ActivityListPopupPortalPartId' AS [Name], N'' AS [Description]
    FROM [dbo].[Language] l
    WHERE l.[LanguageID] IN (1,2,7);
END;
--#endregion Datasource 90

------------------------------------------------------ Update 1 -------------------------------------------------------------
--#region Update PortalPart 5632
DECLARE @DataSourceID_2159_13565078894911 nvarchar(32)= '';
SELECT @DataSourceID_2159_13565078894911 = [ds].[ItemListDataSourceID]
FROM [list].[ItemListDataSource] [ds]
WHERE [ds].[MasterID] = 'C20DF1DC-56CB-4DE6-9983-51C9D6089428';

UPDATE pt SET pt.[Settings] = N'<params>DataSourceId%3D'+@DataSourceID_2159_13565078894911+'</params>'
FROM [app].[PortalPart] pt
WHERE pt.[MasterID] = 'C5CE6852-4881-41F6-B4D1-49B9B2465A26';
--#endregion Update PortalPart 5632

--#region DataParameter 2804
IF NOT EXISTS (SELECT 1 FROM [list].[ItemListDataParameter] [dp] WHERE [dp].[MasterID] = '2A5F72AD-E43A-485B-88FB-AB0AFF6B6532')
BEGIN
    INSERT INTO [list].[ItemListDataParameter] ([ItemListDataSourceID], [DataType], [Type], [Mandatory], [DefaultValue], [ConstructionPattern], [Created], [MasterID])
    SELECT [ds].[ItemListDataSourceID] AS [ItemListDataSourceID], N'String' AS [DataType], N'PortalPartParam' AS [Type], N'0' AS [Mandatory], N'' AS [DefaultValue], N'' AS [ConstructionPattern], GETDATE() AS [Created], N'2A5F72AD-E43A-485B-88FB-AB0AFF6B6532' AS [MasterID]
    FROM [list].[ItemListDataSource] [ds]
    WHERE [ds].[MasterID] = 'C20DF1DC-56CB-4DE6-9983-51C9D6089428';
    DECLARE @ParamID_2804 nvarchar(32)= SCOPE_IDENTITY();

    INSERT INTO [list].[LT_ItemListDataParameter] ([LanguageID], [ItemListDataParameterID], [Name], [Description])
    SELECT l.[LanguageID], @ParamID_2804 AS [ItemListDataParameterID], N'HiddenActivityIds' AS [Name], N'' AS [Description]
    FROM [dbo].[Language] l
    WHERE l.[LanguageID] IN (1,2,7);
END;
--#endregion DataParameter 2804

------------------------------------------------------ Update 2 -------------------------------------------------------------
--#region DataParameter 2807
IF NOT EXISTS (SELECT 1 FROM [list].[ItemListDataParameter] [dp] WHERE [dp].[MasterID] = '5D707AF6-E0B3-4955-B0A2-C78B4F6043B6')
BEGIN
    INSERT INTO [list].[ItemListDataParameter] ([ItemListDataSourceID], [DataType], [Type], [Mandatory], [DefaultValue], [ConstructionPattern], [Created], [MasterID])
    SELECT [ds].[ItemListDataSourceID] AS [ItemListDataSourceID], N'String' AS [DataType], N'PortalPartParam' AS [Type], N'0' AS [Mandatory], N'' AS [DefaultValue], N'' AS [ConstructionPattern], GETDATE() AS [Created], N'5D707AF6-E0B3-4955-B0A2-C78B4F6043B6' AS [MasterID]
    FROM [list].[ItemListDataSource] [ds]
    WHERE [ds].[MasterID] = 'A039FAF6-2D36-41ED-8C1D-2B43433FF089';
    DECLARE @ParamID_2807 nvarchar(32)= SCOPE_IDENTITY();

    INSERT INTO [list].[LT_ItemListDataParameter] ([LanguageID], [ItemListDataParameterID], [Name], [Description])
    SELECT l.[LanguageID], @ParamID_2807 AS [ItemListDataParameterID], N'hiddenXCategoryIds' AS [Name], N'' AS [Description]
    FROM [dbo].[Language] l
    WHERE l.[LanguageID] IN (1,2,7);
END;
--#endregion DataParameter 2807

--#region Update name for DataParameter 2675
UPDATE lt SET lt.[Name] = N'hiddenActivityIds'
FROM [list].[ItemListDataParameter] dp
JOIN [list].[LT_ItemListDataParameter] lt ON lt.[ItemListDataParameterID] = dp.[ItemListDataParameterID] AND dp.[MasterID] = 'B14BB405-65CA-466D-BD16-228518321B26'

UPDATE dp SET dp.[DataType] = 'String'
FROM [list].[ItemListDataParameter] dp
JOIN [list].[LT_ItemListDataParameter] lt ON lt.[ItemListDataParameterID] = dp.[ItemListDataParameterID] AND dp.[MasterID] = 'B14BB405-65CA-466D-BD16-228518321B26'

--#endregion Update name for DataParameter 2675

--#endregion 127. CE-2347 Hide Activity
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
-------------------------------------------------------------------------------------------------------------------
PRINT 'Setting database version'
/*
Start of versioning database-level extended properties.
*/
BEGIN
    DECLARE @DB_VERSION varchar(32), @ConceptName nvarchar(128), @ReleaseScriptName nvarchar(4000)
    SET @DB_VERSION = '6.12.009.001.00'
    SET @ConceptName = 'Engage Main'
    SET @ReleaseScriptName = N'v' + @DB_VERSION + N'-04. Engage Data Update Since 2019-03-12'

    IF EXISTS (SELECT 1 FROM fn_listextendedproperty(N'cxStudio Database Version', NULL, NULL, NULL, NULL, NULL, NULL))
        EXEC sp_dropextendedproperty N'cxStudio Database Version', NULL, NULL, NULL, NULL, NULL, NULL
    EXEC sp_addextendedproperty N'cxStudio Database Version', @DB_VERSION, NULL, NULL, NULL, NULL, NULL, NULL
		
    IF EXISTS (SELECT 1 FROM fn_listextendedproperty(N'Script Name', NULL, NULL, NULL, NULL, NULL, NULL))
        EXEC sp_dropextendedproperty N'Script Name', NULL, NULL, NULL, NULL, NULL, NULL
    EXEC sp_addextendedproperty N'Script Name',  @ReleaseScriptName , NULL, NULL, NULL, NULL, NULL, NULL
END
/*
End of versioning database-level extended properties.
*/
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
IF EXISTS (SELECT * FROM #tmpErrors) ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT>0 BEGIN
PRINT 'The database update succeeded'
COMMIT TRANSACTION
END
ELSE PRINT 'The database update failed'
GO
DROP TABLE #tmpErrors
GO
SET NOEXEC OFF
GO
-------------------------------------------------------------------------------------------------------------------------
DECLARE @Current_DB_Version varchar(32), @CurrentConceptName nvarchar(128),
        @Required_DB_Version varchar(32), @RequiredConceptName nvarchar(128)

SET @Required_DB_Version = '6.12.009.001.00'
SET @RequiredConceptName = 'Engage Main'
PRINT 'This script is generated for executing on ' + @RequiredConceptName + ' version ' + @Required_DB_Version

SELECT @Current_DB_Version = convert(nvarchar(max),value) FROM fn_listextendedproperty(N'cxStudio Database Version', NULL, NULL, NULL, NULL, NULL, NULL)
SELECT @CurrentConceptName = convert(nvarchar(max),value) FROM fn_listextendedproperty(N'Concept Name', NULL, NULL, NULL, NULL, NULL, NULL)

IF @CurrentConceptName != @RequiredConceptName OR @Current_DB_Version NOT LIKE @Required_DB_Version
BEGIN
    PRINT 'Incorrect solution or cxStudio Database version'
    SET NOEXEC ON
END
GO
-------------------------------------------------------------------------------------------------------------------------
IF EXISTS (SELECT * FROM tempdb..sysobjects WHERE id=OBJECT_ID('tempdb..#tmpErrors')) DROP TABLE #tmpErrors
GO
CREATE TABLE #tmpErrors (Error int)
GO
SET XACT_ABORT ON
GO
SET TRANSACTION ISOLATION LEVEL SERIALIZABLE
GO
BEGIN TRANSACTION
GO
----------------------------------------------------------------------------------------------------------------
--#region 128. CE-2356 Learning Support Activity
-- Dev: David
-- Feature: CE-2315 Learning support activity
-- CE-2356 Support for L�ringst�ttende pr�ver

--#region New PortalPart 5637
IF NOT EXISTS (SELECT 1 FROM [app].[PortalPart] [pt] WHERE [pt].[MasterID] = '66EFA5BB-BDE5-4F8A-9C97-547495C8EAC1')
BEGIN
    DECLARE @PortalPart_5637_SortNo int;
    SELECT @PortalPart_5637_SortNo = MAX(pt.[No])+1
    FROM [app].[PortalPart] pt
    JOIN [app].[PortalPage] pp ON pp.[PortalPageID] = pt.[PortalPageID] AND pp.[MasterID] = '0678F421-C8FE-4441-A0E1-655A4CEE3B68';
    
    INSERT INTO [app].[PortalPart] ([PortalPageID], [PortalPartTypeID], [No], [CategoryID], [Settings], [BubbleID], [ReportPartID], [ReportID], [CssClass], [InlineStyle], [ActivityID], [SurveyID], [ExtID], [MasterID])
    SELECT [pp].[PortalPageID] AS [PortalPageID], N'1' AS [PortalPartTypeID], @PortalPart_5637_SortNo AS [No], NULL AS [CategoryID], N'<params>IsEmptyHtml%3D'+'true'+'</params>' AS [Settings], NULL AS [BubbleID], NULL AS [ReportPartID], NULL AS [ReportID], N'container-command-info col-sm-4 col-xs-12 popup-default box-floating popup-fixed no-padding no-right-radius ' AS [CssClass], N'' AS [InlineStyle], NULL AS [ActivityID], NULL AS [SurveyID], N'' AS [ExtID], N'66EFA5BB-BDE5-4F8A-9C97-547495C8EAC1' AS [MasterID]
    FROM [app].[PortalPage] [pp]
    WHERE [pp].[MasterID] = '0678F421-C8FE-4441-A0E1-655A4CEE3B68';
    DECLARE @PortalPartID_5637 nvarchar(32)= SCOPE_IDENTITY();

    INSERT INTO [app].[LT_PortalPart] ([LanguageID], [PortalPartID], [Name], [Description], [Text], [TextAbove], [TextBelow], [HeaderText], [FooterText], [BeforeContentText], [AfterContentText])
    SELECT l.[LanguageID], @PortalPartID_5637 AS [PortalPartID], N'Nasjonale pr�ver new - tekst' AS [Name], N'' AS [Description], N'Update later' AS [Text], N'' AS [TextAbove], N'' AS [TextBelow], N'' AS [HeaderText], N'' AS [FooterText], N'' AS [BeforeContentText], N'' AS [AfterContentText]
    FROM [dbo].[Language] l
    WHERE l.[LanguageID] IN (1,2,7);
END;
--#endregion New PortalPart 5637

--#region New PortalPage 3899
IF NOT EXISTS (SELECT 1 FROM [app].[PortalPage] [pp] WHERE [pp].[MasterID] = '5C4DA92C-27D6-4883-89C5-F46FD93B697D')
BEGIN
    INSERT INTO [app].[PortalPage] ([OwnerID], [No], [Parameter], [ExtID], [Created], [MasterID])
    SELECT [o].[OwnerID] AS [OwnerID], N'52' AS [No], N'RenderToolbar=false&rendername=false' AS [Parameter], N'' AS [ExtID], GETDATE() AS [Created], N'5C4DA92C-27D6-4883-89C5-F46FD93B697D' AS [MasterID]
    FROM [org].[Owner] [o]
    WHERE [o].[MasterID] = '1C483E02-E808-42A7-8210-B11F4051D2E4';
    DECLARE @PortalPageID_3899 nvarchar(32)= SCOPE_IDENTITY();

    INSERT INTO [app].[LT_PortalPage] ([LanguageID], [PortalPageID], [Name], [Description])
    SELECT l.[LanguageID], @PortalPageID_3899 AS [PortalPageID], N'L�ringsst�ttende pr�ve' AS [Name], N'L�ringsst�ttende pr�ve' AS [Description]
    FROM [dbo].[Language] l
    WHERE l.[LanguageID] IN (1,2,7);

    IF NOT EXISTS (SELECT 1 FROM [app].[PortalPart] [pt] WHERE [pt].[MasterID] = '5BFC3FC9-F255-4D4D-840A-921C3F5DDD00')
    BEGIN
        INSERT INTO [app].[PortalPart] ([PortalPageID], [PortalPartTypeID], [No], [CategoryID], [Settings], [BubbleID], [ReportPartID], [ReportID], [CssClass], [InlineStyle], [ActivityID], [SurveyID], [ExtID], [MasterID])
        SELECT [pp].[PortalPageID] AS [PortalPageID], N'1' AS [PortalPartTypeID], N'1' AS [No], NULL AS [CategoryID], N'<params>showintableview%3D'+'%7Bif(%7B%22%7B%7Bgetworkcontext().Site.SiteId%7D%3D5%7D%22%7D%2C%22false%22%2C%22true%22)%7D'+'</params>' AS [Settings], NULL AS [BubbleID], NULL AS [ReportPartID], NULL AS [ReportID], N'container-pas' AS [CssClass], N'' AS [InlineStyle], NULL AS [ActivityID], NULL AS [SurveyID], N'' AS [ExtID], N'5BFC3FC9-F255-4D4D-840A-921C3F5DDD00' AS [MasterID]
        FROM [app].[PortalPage] [pp]
        WHERE [pp].[MasterID] = '5C4DA92C-27D6-4883-89C5-F46FD93B697D';
        DECLARE @PortalPartID_5633 nvarchar(32)= SCOPE_IDENTITY();

        INSERT INTO [app].[LT_PortalPart] ([LanguageID], [PortalPartID], [Name], [Description], [Text], [TextAbove], [TextBelow], [HeaderText], [FooterText], [BeforeContentText], [AfterContentText])
        SELECT l.[LanguageID], @PortalPartID_5633 AS [PortalPartID], N'Nasjonale pr�ver - tekst' AS [Name], N'' AS [Description], N'Dere overf�rer resultatene fra PAS til {getsitename()} ved � laste ned en CSV-fil fra PAS og deretter importere den til {getsitename()}. Dette m� gj�res for hver pr�ve. Det vil si at en barneskole m� laste ned og importere 3 filer, og tilsvarende m� en ungdomsskole laste ned og importere 5 filer. Slik g�r du frem:<ul style="list-style-type: none;"><li>1. Logg inn i <a href="https://pas.udir.no/AuthenticationWeb/?RequestApplication=https%3a%2f%2fpas.udir.no%3a443%2fWeb%2flogin.aspx&returnURL=%2fWeb%2fDefault.aspx" target="_blank">PAS</a> og velg <b>"Analyserapport"</b>. (Ikke bland dette med "Grupperapport")</li><li>2. G� til fanen �Gruppe�.</li><li>3. Velg pr�ve i nedtrekkslisten �Pr�ve�</li><li>4. Velg �Alle� i nedtrekkslisten �Gruppe�</li><li>5. I �hamburgermenyen� i h�yre hj�rne <img src="{GetBaseURL()}/Content/styles/themes/Magnum/images/list-icon.png" width="16px" style="margin-right:2px;width:12px" alt="list"/> velger du �Eksporter til Excel�</li><li>6. Lagre filen p� skolens nettverk.</li><ul style="margin-top:0"><li><b>Viktig!</b> Ikke gj�r endringer i filnavn eller filens innhold</li></ul><li>7. Gj�r punkt 3 til 6 for alle pr�vene</li><li>8. Logg inn i {getsitename()}</li><li>9. G� til Administrasjon -> Hent eksterne resultater -> Nasjonale pr�ver</li><li>10. Klikk p� ikonet <img src="{GetBaseURL()}/Content/styles/themes/Magnum/images/upload-icon.png" alt="upload" style="width:18px;margin-bottom:-2px"/> ved siden av riktig pr�ve for � laste opp filen du eksporterte fra PAS</li><li>11. Gj�r punkt 10 for alle pr�vene</li><li>12. N�r filen(e) er lastet opp, klikk p� �Knytt resultater til elever� og f�lg instruksjonene i skjermbildet som dukker opp</li></ul><div id="divWarning"><ul class="importantAlignment"><li class="importantDiv"><br>Tallet i knappen indikerer hvor mange elever som ikke er tilknyttet resultatene.<li></ul>' AS [Text], N'' AS [TextAbove], N'' AS [TextBelow], N'' AS [HeaderText], N'' AS [FooterText], N'' AS [BeforeContentText], N'' AS [AfterContentText]
        FROM [dbo].[Language] l
        WHERE l.[LanguageID] IN (1,2,7);
    END;

    DECLARE @ItemListID_1351_15154345219961 nvarchar(32)= '';
    SELECT @ItemListID_1351_15154345219961 = [l].[ItemListID]
    FROM [list].[ItemList] [l]
    WHERE [l].[MasterID] = '09484279-DADE-4931-BA6A-EE3DF4CB443E';
    
    DECLARE @ActivityID_1768_15154345219962 nvarchar(32)= '';
    SELECT @ActivityID_1768_15154345219962 = [a].[ActivityID] FROM [at].[Activity] [a] WHERE [a].[MasterID] = 'B0FA472D-AF1A-4D60-BBA4-A46E39D0B7A9';
    
    DECLARE @PortalPartID_5517 nvarchar(32)= '', @PortalPartID_5637_1 nvarchar(32)= '';
    SELECT @PortalPartID_5517 = pt.[PortalPartID] FROM [app].[PortalPart] pt WHERE pt.[MasterID] = '658D31B6-2BDA-42BD-BC1A-91AC4BD25ACB';
    SELECT @PortalPartID_5637_1 = pt.[PortalPartID] FROM [app].[PortalPart] pt WHERE pt.[MasterID] = '66EFA5BB-BDE5-4F8A-9C97-547495C8EAC1';
    IF NOT EXISTS (SELECT 1 FROM [app].[PortalPart] [pt] WHERE [pt].[MasterID] = '6B84167B-D013-4142-A040-67BD548E15EA')
    BEGIN
        INSERT INTO [app].[PortalPart] ([PortalPageID], [PortalPartTypeID], [No], [CategoryID], [Settings], [BubbleID], [ReportPartID], [ReportID], [CssClass], [InlineStyle], [ActivityID], [SurveyID], [ExtID], [MasterID])
        SELECT [pp].[PortalPageID] AS [PortalPageID], N'21' AS [PortalPartTypeID], N'2' AS [No], NULL AS [CategoryID], N'<params>ListID%3D'+@ItemListID_1351_15154345219961+'%26activityid%3D'+@ActivityID_1768_15154345219962+'%26groupBox%3D'+'%7Bif(%7B%22%7B%7Bgetworkcontext().Site.SiteId%7D%3D5%7D%22%7D%2C%22false%22%2C%22true%22)%7D'+'</params>' AS [Settings], NULL AS [BubbleID], NULL AS [ReportPartID], NULL AS [ReportID], N'col-xs-12 container-employee-list bg-white table-responsive container-top-border container-pas' AS [CssClass], N'' AS [InlineStyle], NULL AS [ActivityID], NULL AS [SurveyID], N'' AS [ExtID], N'6B84167B-D013-4142-A040-67BD548E15EA' AS [MasterID]
        FROM [app].[PortalPage] [pp]
        WHERE [pp].[MasterID] = '5C4DA92C-27D6-4883-89C5-F46FD93B697D';
        DECLARE @PortalPartID_5634 nvarchar(32)= SCOPE_IDENTITY();

        INSERT INTO [app].[LT_PortalPart] ([LanguageID], [PortalPartID], [Name], [Description], [Text], [TextAbove], [TextBelow], [HeaderText], [FooterText], [BeforeContentText], [AfterContentText])
        SELECT l.[LanguageID], @PortalPartID_5634 AS [PortalPartID], N'L�ringsst�ttende pr�ve 6. trinn' AS [Name], N'' AS [Description], N'' AS [Text], N'<div class="container-table-command table-responsive">    <table class="table" data-nosort="true">        <colgroup>            <col>            <col>            <col>            <col>        </colgroup>        <tbody><tr> <td title="Informasjon" onclick="renderUserlistInfo($(this), '+@PortalPartID_5517+')"><div class="btn-slide-command text-left"><span class="icon-vokal-info"></span></div></td></tr>    </tbody></table></div>' AS [TextAbove], N'' AS [TextBelow], N'' AS [HeaderText], N'<input type="hidden" value="'+@PortalPartID_5637_1+'" id="hInfoPortalPartId">' AS [FooterText], N'' AS [BeforeContentText], N'<script type="text/javascript">window.gMessageFileSize = ''The file size cannot greater than 4 Mb'',    window.gMessageWrongFormat = ''<div style="text-align:left">Incorrect file format, the template format must be: *.csv</div>'';</script>' AS [AfterContentText]
        FROM [dbo].[Language] l
        WHERE l.[LanguageID] IN (1,2,7);
    END;
END;
--#endregion New PortalPage 3899

--#region New MenuItem 3521
DECLARE @MenuItemID_2439_16513994639133521 nvarchar(32)= '';
SELECT @MenuItemID_2439_16513994639133521 = [MI].[MenuItemID]
FROM [app].[MenuItem] [MI]
WHERE [MI].[MasterID] = 'A51044FA-AA58-43C5-8AB3-69E62C319064';

DECLARE @PortalPageID_3899_16513994639133521 nvarchar(32)= '';
SELECT @PortalPageID_3899_16513994639133521 = [pp].[PortalPageID]
FROM [app].[PortalPage] [pp]
WHERE [pp].[MasterID] = '5C4DA92C-27D6-4883-89C5-F46FD93B697D';
IF NOT EXISTS (SELECT 1 FROM [app].[MenuItem] [mi] WHERE [mi].[MasterID] = '92CB4924-7F54-49E3-98F9-E94693E278BF')
BEGIN
    INSERT INTO [app].[MenuItem] ([MenuID], [ParentID], [MenuItemTypeID], [PortalPageID], [No], [CssClass], [URL], [Target], [IsDefault], [Created], [Active], [ExtID], [NoRender], [MasterID])
    SELECT [m].[MenuID] AS [MenuID], @MenuItemID_2439_16513994639133521 AS [ParentID], N'2' AS [MenuItemTypeID], @PortalPageID_3899_16513994639133521 AS [PortalPageID], N'3' AS [No], N'' AS [CssClass], N'' AS [URL], N'' AS [Target], N'0' AS [IsDefault], GETDATE() AS [Created], N'1' AS [Active], N'' AS [ExtID], N'0' AS [NoRender], N'92CB4924-7F54-49E3-98F9-E94693E278BF' AS [MasterID]
    FROM [app].[Menu] [m]
    WHERE [m].[MasterID] = '13BCDBA6-29B7-4079-AF97-067AD541D27B';
    DECLARE @MenuItemID_3521 nvarchar(32)= SCOPE_IDENTITY();

    INSERT INTO [app].[LT_MenuItem] ([LanguageID], [MenuItemID], [Name], [Description], [ToolTip], [URL])
    SELECT l.[LanguageID], @MenuItemID_3521 AS [MenuItemID], N'L�ringsst�ttende pr�ve' AS [Name], N'' AS [Description], N'' AS [ToolTip], N'' AS [URL]
    FROM [dbo].[Language] l
    WHERE l.[LanguageID] IN (1,2,7);
END;
--#endregion New MenuItem 3521

--#region AccessGeneric
INSERT INTO [dbo].[AccessGeneric] ([TableTypeID], [Elementid], [Type], [AccessGroupID], [Created], [SiteID], [MasterID])
SELECT tt.[TableTypeID], pt.[PortalPartID] AS [Elementid], 1 AS [Type], ag.[AccessGroupID], GETDATE() AS [Created], NULL AS [SiteID],
       'B9BA5D82-0021-4EA1-8E9B-DDCAA555B1E8' AS [MasterID]
FROM [dbo].[TableType] tt
JOIN [dbo].[AccessGroup] ag ON ag.[MasterID] = '4AADFC3D-49B8-4A93-B22D-98E4F30D64FE' AND tt.[Name] = 'PortalPart'
JOIN [app].[PortalPart] pt ON pt.[MasterID] = '6B84167B-D013-4142-A040-67BD548E15EA'
WHERE NOT EXISTS (SELECT 1 FROM [dbo].[AccessGeneric] acc WHERE acc.[MasterID] = 'B9BA5D82-0021-4EA1-8E9B-DDCAA555B1E8');
--#endregion AccessGeneric
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
------------------------------------------------------ Upddate 1 ----------------------------------------------------------------
UPDATE lt SET lt.[Text] = N'<div class="modal-header"><button data-dismiss="modal" class="close" type="button"><span aria-hidden="true">&times;</span><span class="sr-only">Lukk</span></button> <h4 class="modal-title">L�ringsst�ttende pr�ver</h4></div><div class="modal-body"><p>Dere overf�rer resultatene fra PAS til {getsitename()} ved � laste ned en CSV-fil fra PAS og deretter importere den til {getsitename()}. Dette m� gj�res for hver pr�ve, for hver klasse. Slik g�r du frem:</p><ul style="list-style-type: none;"><li>1. Logg inn i PAS-pr�ver</li><li>2. Velg "Resultater og sk�ring" i menyen</li><li>3.	Velg pr�ven du �nsker, for eksempel l�ringsst�ttende pr�ve lesing 6.trinn.</li><li>4.	Ekspander menyen og velg pr�vegruppen du �nsker, og velg "Se resultater".</li><li>5. Fra fanen Gruppe kan man velge Eksporter til Excel </li><li>6. Lagre filen p� skolens nettverk.</li><ul style="margin-top:0"><li><b>Viktig!</b> Ikke gj�r endringer i filnavn eller filens innhold</li></ul><li>7. Gj�r punkt 3 til 6 for alle pr�vene for alle klasser</li><li>8. Logg inn i {getsitename()}</li><li>9. G� til Administrasjon -> Hent eksterne resultater -> L�ringsst�ttende pr�ver</li><li>10. Klikk p� ikonet <span class="icon-vokal-upload btn-upload blue"></span> ved siden av riktig pr�ve for � laste opp filen du eksporterte fra PAS</li><li>11. Gj�r punkt 10 for alle pr�vene for alle klassene</li><li>12. N�r filen(e) er lastet opp, klikk p� �Knytt resultater til elever� og f�lg instruksjonene i skjermbildet som dukker opp</li></ul><p><img src=''{GetBaseURL()}/Content/styles/themes/Magnum/images/important.png'' style=''padding-right:15px'' />Tallet i knappen indikerer hvor mange elever som ikke er tilknyttet resultatene.</p></div><div class="col-xs-12"> <button type="button" class="btn btn-primary pull-right" onclick="_setNotShowInstructionCookiePAS(this)">Ok, jeg forst�r!</button></div><div class="border"></div>'
FROM [app].[PortalPart] [pt]
JOIN [app].[LT_PortalPart] [lt] ON lt.[PortalPartID] = pt.[PortalPartID] AND [pt].[MasterID] = '66EFA5BB-BDE5-4F8A-9C97-547495C8EAC1';

DELETE lt
FROM [app].[PortalPart] [pt]
JOIN [app].[LT_PortalPart] [lt] ON lt.[PortalPartID] = pt.[PortalPartID] AND [pt].[MasterID] = '5BFC3FC9-F255-4D4D-840A-921C3F5DDD00';

DELETE pt FROM [app].[PortalPart] [pt] WHERE [pt].[MasterID] = '5BFC3FC9-F255-4D4D-840A-921C3F5DDD00';

DECLARE @PortalPartID_5637_1 nvarchar(32)= '';
SELECT @PortalPartID_5637_1 = pt.[PortalPartID] FROM [app].[PortalPart] pt WHERE pt.[MasterID] = '66EFA5BB-BDE5-4F8A-9C97-547495C8EAC1';

UPDATE lt SET lt.[TextAbove] = N'<div class="container-table-command table-responsive">    <table class="table" data-nosort="true">        <colgroup>            <col>            <col>            <col>            <col>        </colgroup>        <tbody><tr> <td title="Informasjon" onclick="renderUserlistInfo($(this), '+@PortalPartID_5637_1+')"><div class="btn-slide-command text-left"><span class="icon-vokal-info"></span></div></td></tr>    </tbody></table></div>'
FROM [app].[PortalPart] [pt]
JOIN [app].[LT_PortalPart] [lt] ON lt.[PortalPartID] = pt.[PortalPartID] AND [pt].[MasterID] = '6B84167B-D013-4142-A040-67BD548E15EA';

--#endregion 128. CE-2356 Learning Support Activity
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
-------------------------------------------------------------------------------------------------------------------
PRINT 'Setting database version'
/*
Start of versioning database-level extended properties.
*/
BEGIN
    DECLARE @DB_VERSION varchar(32), @ConceptName nvarchar(128), @ReleaseScriptName nvarchar(4000)
    SET @DB_VERSION = '6.12.009.002.00'
    SET @ConceptName = 'Engage Main'
    SET @ReleaseScriptName = N'v' + @DB_VERSION + N'-04. Engage Data Update Since 2019-03-12'

    IF EXISTS (SELECT 1 FROM fn_listextendedproperty(N'cxStudio Database Version', NULL, NULL, NULL, NULL, NULL, NULL))
        EXEC sp_dropextendedproperty N'cxStudio Database Version', NULL, NULL, NULL, NULL, NULL, NULL
    EXEC sp_addextendedproperty N'cxStudio Database Version', @DB_VERSION, NULL, NULL, NULL, NULL, NULL, NULL
		
    IF EXISTS (SELECT 1 FROM fn_listextendedproperty(N'Script Name', NULL, NULL, NULL, NULL, NULL, NULL))
        EXEC sp_dropextendedproperty N'Script Name', NULL, NULL, NULL, NULL, NULL, NULL
    EXEC sp_addextendedproperty N'Script Name',  @ReleaseScriptName , NULL, NULL, NULL, NULL, NULL, NULL
END
/*
End of versioning database-level extended properties.
*/
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
IF EXISTS (SELECT * FROM #tmpErrors) ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT>0 BEGIN
PRINT 'The database update succeeded'
COMMIT TRANSACTION
END
ELSE PRINT 'The database update failed'
GO
DROP TABLE #tmpErrors
GO
SET NOEXEC OFF
GO
-------------------------------------------------------------------------------------------------------------------------
DECLARE @Current_DB_Version varchar(32), @CurrentConceptName nvarchar(128),
        @Required_DB_Version varchar(32), @RequiredConceptName nvarchar(128)

SET @Required_DB_Version = '6.12.009.002.00'
SET @RequiredConceptName = 'Engage Main'
PRINT 'This script is generated for executing on ' + @RequiredConceptName + ' version ' + @Required_DB_Version

SELECT @Current_DB_Version = convert(nvarchar(max),value) FROM fn_listextendedproperty(N'cxStudio Database Version', NULL, NULL, NULL, NULL, NULL, NULL)
SELECT @CurrentConceptName = convert(nvarchar(max),value) FROM fn_listextendedproperty(N'Concept Name', NULL, NULL, NULL, NULL, NULL, NULL)

IF @CurrentConceptName != @RequiredConceptName OR @Current_DB_Version NOT LIKE @Required_DB_Version
BEGIN
    PRINT 'Incorrect solution or cxStudio Database version'
    SET NOEXEC ON
END
GO
-------------------------------------------------------------------------------------------------------------------------
IF EXISTS (SELECT * FROM tempdb..sysobjects WHERE id=OBJECT_ID('tempdb..#tmpErrors')) DROP TABLE #tmpErrors
GO
CREATE TABLE #tmpErrors (Error int)
GO
SET XACT_ABORT ON
GO
SET TRANSACTION ISOLATION LEVEL SERIALIZABLE
GO
BEGIN TRANSACTION
GO
----------------------------------------------------------------------------------------------------------------
--#region 130. Fix Missing CustomerID In XCategory Access
-- Dev: Thomas
-- Feature: Admin content
-- Fix Missing CustomerID In XCategory Access
DECLARE @XCategoryIDList nvarchar(max) = '', @CustomerID int = 51, @DepartmentID int = 18918
DECLARE @XCategoryAccess TABLE ([XCID] int, [AccessGroupID] int)

INSERT INTO @XCategoryAccess ([XCID], [AccessGroupID])
SELECT v.[XCID], ag.[Value] AS [AccessGroupID]
FROM (SELECT xc.[XCID], pv.[Value]
      FROM [at].[XCategory] xc
      JOIN [prop].[PropValue] pv ON pv.[PropertyID] = 1060 AND pv.[ItemID] = xc.[XCID]
       AND (@XCategoryIDList = ''
            OR xc.[XCID] IN (SELECT x.[Value] AS [XCID] FROM [dbo].[funcListToTableInt](@XCategoryIDList,',') AS x)
           )
     ) v
CROSS APPLY dbo.funcListToTableInt(v.[Value], ',') AS ag

UPDATE agm SET agm.[CustomerID] = d.[CustomerID]
FROM @XCategoryAccess xa
JOIN [dbo].[AccessGroupMember] agm ON agm.[AccessGroupID] = xa.[AccessGroupID]
JOIN [org].[Department] d ON d.[DepartmentID] = agm.[DepartmentID]
WHERE agm.[DepartmentID] > 0 AND agm.[CustomerID] IS NULL;

--#endregion 130. Fix Missing CustomerID In XCategory Access
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
--#region 131. Misc Update
-- Dev: Jennie
-- Feature: Landing Page - Banner Carousel
-- Responsive for image
UPDATE lt SET lt.[Text] = REPLACE(lt.[Text], N'<img src=', N'<img class="img-responsive" src=')
FROM [app].[PortalPart] pt
JOIN [app].[LT_PortalPart] lt ON lt.[PortalPartID] = pt.[PortalPartID]
 AND pt.[MasterID] IN ('F1FB3019-B166-4D5C-A204-805FD94E4E68',
                       '1E6A5166-79F8-4FEF-9051-8576598B8474',
                       '7160FE23-C1CB-49BF-95D7-E9AC78DA17B2')
 AND lt.[Text] NOT LIKE 'class="img-responsive"';

--#endregion 131. Misc Update
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
-------------------------------------------------------------------------------------------------------------------
PRINT 'Setting database version'
/*
Start of versioning database-level extended properties.
*/
BEGIN
    DECLARE @DB_VERSION varchar(32), @ConceptName nvarchar(128), @ReleaseScriptName nvarchar(4000)
    SET @DB_VERSION = '6.12.009.003.00'
    SET @ConceptName = 'Engage Main'
    SET @ReleaseScriptName = N'v' + @DB_VERSION + N'-04. Engage Data Update Since 2019-03-12'

    IF EXISTS (SELECT 1 FROM fn_listextendedproperty(N'cxStudio Database Version', NULL, NULL, NULL, NULL, NULL, NULL))
        EXEC sp_dropextendedproperty N'cxStudio Database Version', NULL, NULL, NULL, NULL, NULL, NULL
    EXEC sp_addextendedproperty N'cxStudio Database Version', @DB_VERSION, NULL, NULL, NULL, NULL, NULL, NULL
		
    IF EXISTS (SELECT 1 FROM fn_listextendedproperty(N'Script Name', NULL, NULL, NULL, NULL, NULL, NULL))
        EXEC sp_dropextendedproperty N'Script Name', NULL, NULL, NULL, NULL, NULL, NULL
    EXEC sp_addextendedproperty N'Script Name',  @ReleaseScriptName , NULL, NULL, NULL, NULL, NULL, NULL
END
/*
End of versioning database-level extended properties.
*/
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
IF EXISTS (SELECT * FROM #tmpErrors) ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT>0 BEGIN
PRINT 'The database update succeeded'
COMMIT TRANSACTION
END
ELSE PRINT 'The database update failed'
GO
DROP TABLE #tmpErrors
GO
SET NOEXEC OFF
GO