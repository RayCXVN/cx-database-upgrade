SET QUOTED_IDENTIFIER ON
GO
-------------------------------------------------------------------------------------------------------------------------
DECLARE @Current_DB_Version varchar(32), @CurrentConceptName nvarchar(128),
        @Required_DB_Version varchar(32), @RequiredConceptName nvarchar(128)

SET @Required_DB_Version = '6.12.009.003.%'
SET @RequiredConceptName = 'Engage Main'
PRINT 'This script is generated for executing on ' + @RequiredConceptName + ' version ' + @Required_DB_Version

SELECT @Current_DB_Version = convert(nvarchar(max),value) FROM fn_listextendedproperty(N'cxStudio Database Version', NULL, NULL, NULL, NULL, NULL, NULL)
SELECT @CurrentConceptName = convert(nvarchar(max),value) FROM fn_listextendedproperty(N'Concept Name', NULL, NULL, NULL, NULL, NULL, NULL)

IF @CurrentConceptName != @RequiredConceptName OR @Current_DB_Version NOT LIKE @Required_DB_Version
BEGIN
    PRINT 'Incorrect solution or cxStudio Database version'
    SET NOEXEC ON
END
GO
-------------------------------------------------------------------------------------------------------------------------
IF EXISTS (SELECT * FROM tempdb..sysobjects WHERE id=OBJECT_ID('tempdb..#tmpErrors')) DROP TABLE #tmpErrors
GO
CREATE TABLE #tmpErrors (Error int)
GO
SET XACT_ABORT ON
GO
SET TRANSACTION ISOLATION LEVEL SERIALIZABLE
GO
BEGIN TRANSACTION
GO
----------------------------------------------------------------------------------------------------------------
--#region 133. CE-2380 Import Org Status Update 1
UPDATE lt SET lt.[TextAbove] = REPLACE(lt.[TextAbove], N'<option value="3">To uke</option>', N'<option value="3">To uker</option>')
FROM [app].[PortalPart] pt
JOIN [app].[LT_PortalPart] lt ON lt.[PortalPartID] = pt.[PortalPartID] AND pt.[MasterID] = '28050334-4127-4179-8384-8556DFA4B883';

UPDATE lt SET lt.[ItemText] = N'{GetDateTimeString("{EndDate}","HH'':''mm'':''ss")}'
FROM [list].[ItemListField] f
JOIN [list].[LT_ItemListField] lt ON lt.[ItemListFieldID] = f.[ItemListFieldID] AND f.[MasterID] = 'BC330076-CAA2-41B4-88EA-E70EA07F174D';

UPDATE lt SET lt.[ItemText] = N'{GetDateTimeString("{StartDate}","HH'':''mm'':''ss")}'
FROM [list].[ItemListField] f
JOIN [list].[LT_ItemListField] lt ON lt.[ItemListFieldID] = f.[ItemListFieldID] AND f.[MasterID] = 'B42E1957-1976-4CC0-9C03-55E97DBFC2E9';
--#endregion 133. CE-2380 Import Org Status Update 1
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
--#region 134. CE-2322 Activity Dropdown In User List
-- Dev: Gary
-- Feature: User List
-- CE-2322 Activity dropdown in user list

--#region DataSource 2159
IF NOT EXISTS (SELECT 1 FROM [list].[ItemListDataParameter] [dp] WHERE [dp].[MasterID] = 'CB321331-BF35-4C90-A4EC-2CB4429F09E1')
BEGIN
    INSERT INTO [list].[ItemListDataParameter] ([ItemListDataSourceID], [DataType], [Type], [Mandatory], [DefaultValue], [ConstructionPattern], [Created], [MasterID])
    SELECT [ds].[ItemListDataSourceID] AS [ItemListDataSourceID], N'Boolean' AS [DataType], N'DynamicallyAddedPortalPartParam' AS [Type], N'0' AS [Mandatory], N'' AS [DefaultValue], N'' AS [ConstructionPattern], GETDATE() AS [Created], N'CB321331-BF35-4C90-A4EC-2CB4429F09E1' AS [MasterID]
    FROM [list].[ItemListDataSource] [ds]
    WHERE [ds].[MasterID] = 'C20DF1DC-56CB-4DE6-9983-51C9D6089428';
    DECLARE @ParamID_2811 nvarchar(32)= SCOPE_IDENTITY();

    INSERT INTO [list].[LT_ItemListDataParameter] ([LanguageID], [ItemListDataParameterID], [Name], [Description])
    SELECT l.[LanguageID], @ParamID_2811 AS [ItemListDataParameterID], N'isUserlist' AS [Name], N'' AS [Description]
    FROM [dbo].[Language] l
    WHERE l.[LanguageID] IN (1,2,7);
END;

DECLARE @PortalPageID_3894_13480576508211 nvarchar(32)= '';
SELECT @PortalPageID_3894_13480576508211 = [pp].[PortalPageID]
FROM [app].[PortalPage] [pp]
WHERE [pp].[MasterID] = '3F1F5E61-10A3-446B-98BC-8860215814CE';
IF NOT EXISTS (SELECT 1 FROM [list].[ItemListDataParameter] [dp] WHERE [dp].[MasterID] = '4E96B9C2-84E0-4E73-9195-A80BA3AF24FF')
BEGIN
    INSERT INTO [list].[ItemListDataParameter] ([ItemListDataSourceID], [DataType], [Type], [Mandatory], [DefaultValue], [ConstructionPattern], [Created], [MasterID])
    SELECT [ds].[ItemListDataSourceID] AS [ItemListDataSourceID], N'Integer' AS [DataType], N'PortalPartParam' AS [Type], N'0' AS [Mandatory], N''+@PortalPageID_3894_13480576508211+'' AS [DefaultValue], N'' AS [ConstructionPattern], GETDATE() AS [Created], N'4E96B9C2-84E0-4E73-9195-A80BA3AF24FF' AS [MasterID]
    FROM [list].[ItemListDataSource] [ds]
    WHERE [ds].[MasterID] = 'C20DF1DC-56CB-4DE6-9983-51C9D6089428';
    DECLARE @ParamID_2814 nvarchar(32)= SCOPE_IDENTITY();

    INSERT INTO [list].[LT_ItemListDataParameter] ([LanguageID], [ItemListDataParameterID], [Name], [Description])
    SELECT l.[LanguageID], @ParamID_2814 AS [ItemListDataParameterID], N'VgoTableListPortalPageId' AS [Name], N'' AS [Description]
    FROM [dbo].[Language] l
    WHERE l.[LanguageID] IN (1,2,7);
END;

DECLARE @PortalPageID_3807_13503325009811 nvarchar(32)= '';
SELECT @PortalPageID_3807_13503325009811 = [pp].[PortalPageID]
FROM [app].[PortalPage] [pp]
WHERE [pp].[MasterID] = '6A17C304-C8D1-4BAC-84F0-572E41DD5D98';
IF NOT EXISTS (SELECT 1 FROM [list].[ItemListDataParameter] [dp] WHERE [dp].[MasterID] = '5572E9C5-7259-4EF5-B88D-18F21A2D58A2')
BEGIN
    INSERT INTO [list].[ItemListDataParameter] ([ItemListDataSourceID], [DataType], [Type], [Mandatory], [DefaultValue], [ConstructionPattern], [Created], [MasterID])
    SELECT [ds].[ItemListDataSourceID] AS [ItemListDataSourceID], N'Integer' AS [DataType], N'PortalPartParam' AS [Type], N'0' AS [Mandatory], N''+@PortalPageID_3807_13503325009811+'' AS [DefaultValue], N'' AS [ConstructionPattern], GETDATE() AS [Created], N'5572E9C5-7259-4EF5-B88D-18F21A2D58A2' AS [MasterID]
    FROM [list].[ItemListDataSource] [ds]
    WHERE [ds].[MasterID] = 'C20DF1DC-56CB-4DE6-9983-51C9D6089428';
    DECLARE @ParamID_2815 nvarchar(32)= SCOPE_IDENTITY();

    INSERT INTO [list].[LT_ItemListDataParameter] ([LanguageID], [ItemListDataParameterID], [Name], [Description])
    SELECT l.[LanguageID], @ParamID_2815 AS [ItemListDataParameterID], N'MassRegPortalPageId' AS [Name], N'' AS [Description]
    FROM [dbo].[Language] l
    WHERE l.[LanguageID] IN (1,2,7);
END;
--#endregion DataSource 2159

--#region DataSource 91
DECLARE @PortalPartID_5632_16410785653561 nvarchar(32)= '';
SELECT @PortalPartID_5632_16410785653561 = [pt].[PortalPartID]
FROM [app].[PortalPart] [pt]
WHERE [pt].[MasterID] = 'C5CE6852-4881-41F6-B4D1-49B9B2465A26';
IF NOT EXISTS (SELECT 1 FROM [list].[ItemListDataParameter] [dp] WHERE [dp].[MasterID] = '26BCC7CB-6BCA-47BB-9B41-92A6A63E4F97')
BEGIN
    INSERT INTO [list].[ItemListDataParameter] ([ItemListDataSourceID], [DataType], [Type], [Mandatory], [DefaultValue], [ConstructionPattern], [Created], [MasterID])
    SELECT [ds].[ItemListDataSourceID] AS [ItemListDataSourceID], N'Integer' AS [DataType], N'PortalPartParam' AS [Type], N'0' AS [Mandatory], N''+@PortalPartID_5632_16410785653561+'' AS [DefaultValue], N'' AS [ConstructionPattern], GETDATE() AS [Created], N'26BCC7CB-6BCA-47BB-9B41-92A6A63E4F97' AS [MasterID]
    FROM [list].[ItemListDataSource] [ds]
    WHERE [ds].[MasterID] = 'BF5FA693-26E7-440C-820E-505C9AF349A6';
    DECLARE @ParamID_2810 nvarchar(32)= SCOPE_IDENTITY();

    INSERT INTO [list].[LT_ItemListDataParameter] ([LanguageID], [ItemListDataParameterID], [Name], [Description])
    SELECT l.[LanguageID], @ParamID_2810 AS [ItemListDataParameterID], N'activitiesPopupPortalPartId' AS [Name], N'' AS [Description]
    FROM [dbo].[Language] l
    WHERE l.[LanguageID] IN (1,2,7);
END;
--#endregion DataSource 91

--#region DataSource 2154
DECLARE @PortalPartID_5632_13441882766181 nvarchar(32)= '';
SELECT @PortalPartID_5632_13441882766181 = [pt].[PortalPartID]
FROM [app].[PortalPart] [pt]
WHERE [pt].[MasterID] = 'C5CE6852-4881-41F6-B4D1-49B9B2465A26';
IF NOT EXISTS (SELECT 1 FROM [list].[ItemListDataParameter] [dp] WHERE [dp].[MasterID] = 'C5CC0427-DE0E-4DA7-B44A-F5FC3814AA83')
BEGIN
    INSERT INTO [list].[ItemListDataParameter] ([ItemListDataSourceID], [DataType], [Type], [Mandatory], [DefaultValue], [ConstructionPattern], [Created], [MasterID])
    SELECT [ds].[ItemListDataSourceID] AS [ItemListDataSourceID], N'Integer' AS [DataType], N'PortalPartParam' AS [Type], N'0' AS [Mandatory], N''+@PortalPartID_5632_13441882766181+'' AS [DefaultValue], N'' AS [ConstructionPattern], GETDATE() AS [Created], N'C5CC0427-DE0E-4DA7-B44A-F5FC3814AA83' AS [MasterID]
    FROM [list].[ItemListDataSource] [ds]
    WHERE [ds].[MasterID] = '9DB9B94B-D050-44D5-AD3A-4913AF1442C9';
    DECLARE @ParamID_2813 nvarchar(32)= SCOPE_IDENTITY();

    INSERT INTO [list].[LT_ItemListDataParameter] ([LanguageID], [ItemListDataParameterID], [Name], [Description])
    SELECT l.[LanguageID], @ParamID_2813 AS [ItemListDataParameterID], N'activitiesPopupPortalPartId' AS [Name], N'' AS [Description]
    FROM [dbo].[Language] l
    WHERE l.[LanguageID] IN (1,2,7);
END;
--#endregion DataSource 2154

---------------------------------------- Update 1 ------------------------------------------
-- Dev: Jennie, Gary
-- Feature: User List
-- CE-2322 Activity dropdown in user list

--#region PortalPart
-- PortalPart 5632
UPDATE [app].[PortalPart] SET [CssClass] = 'col-lg-4 col-md-5 col-xs-6 bg-white box-floating popup-fixed no-border no-radius popup-header-all-activities'
WHERE [MasterID] = 'C5CE6852-4881-41F6-B4D1-49B9B2465A26';

-- PortalPart 4442
UPDATE [app].[PortalPart] SET [CssClass] = [CssClass] + ' clearfix'
WHERE [MasterID] = 'DEC30505-C912-474A-8133-08952C722E88' AND [CssClass] NOT LIKE '%clearfix%'

-- PortalPart 5640
DECLARE @DataSourceID_2159_10151979285041 nvarchar(32)= '';
        SELECT @DataSourceID_2159_10151979285041 = ds.[ItemListDataSourceID]
        FROM [list].[ItemListDataSource] ds
        WHERE ds.[MasterID] = 'C20DF1DC-56CB-4DE6-9983-51C9D6089428'
IF NOT EXISTS (SELECT 1 FROM [app].[PortalPart] [pt] WHERE [pt].[MasterID] = '67ACB50D-09FD-4973-9852-DA71D3E48BD1')
BEGIN
    INSERT INTO [app].[PortalPart] ([PortalPageID], [PortalPartTypeID], [No], [CategoryID], [Settings], [BubbleID], [ReportPartID], [ReportID], [CssClass], [InlineStyle], [ActivityID], [SurveyID], [ExtID], [MasterID])
    SELECT [pp].[PortalPageID] AS [PortalPageID], N'43' AS [PortalPartTypeID], N'2' AS [No], NULL AS [CategoryID], N'<params>DataSourceId%3D'+@DataSourceID_2159_10151979285041+'</params>' AS [Settings], NULL AS [BubbleID], NULL AS [ReportPartID], NULL AS [ReportID], N'col-lg-4 col-md-5 col-xs-6 popup-header-all-activities' AS [CssClass], N'' AS [InlineStyle], NULL AS [ActivityID], NULL AS [SurveyID], N'' AS [ExtID], N'67ACB50D-09FD-4973-9852-DA71D3E48BD1' AS [MasterID]
    FROM [app].[PortalPage] [pp]
    WHERE [pp].[MasterID] = '532C0615-4BDB-448F-A16F-DAE54B56BE07';
    DECLARE @PortalPartID_5640 nvarchar(32)= SCOPE_IDENTITY();

    INSERT INTO [app].[LT_PortalPart] ([LanguageID], [PortalPartID], [Name], [Description], [Text], [TextAbove], [TextBelow], [HeaderText], [FooterText], [BeforeContentText], [AfterContentText])
    SELECT l.[LanguageID], @PortalPartID_5640 AS [PortalPartID], N'[MassReg] Activity list popup' AS [Name], N'' AS [Description], N'' AS [Text], N'' AS [TextAbove], N'' AS [TextBelow], N'' AS [HeaderText], N'' AS [FooterText], N'' AS [BeforeContentText], N'' AS [AfterContentText]
    FROM [dbo].[Language] l
    WHERE l.[LanguageID] IN (1,2,7);

    UPDATE [list].[ItemListDataParameter] SET [DefaultValue] = @PortalPartID_5640 WHERE [MasterID] IN ('26BCC7CB-6BCA-47BB-9B41-92A6A63E4F97','C5CC0427-DE0E-4DA7-B44A-F5FC3814AA83');
END;
--#endregion PortalPart

--#endregion 134. CE-2322 Activity Dropdown In User List
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
-------------------------------------------------------------------------------------------------------------------
PRINT 'Setting database version'
/*
Start of versioning database-level extended properties.
*/
BEGIN
    DECLARE @DB_VERSION varchar(32), @ConceptName nvarchar(128), @ReleaseScriptName nvarchar(4000)
    SET @DB_VERSION = '6.12.009.004.00'
    SET @ConceptName = 'Engage Main'
    SET @ReleaseScriptName = N'v' + @DB_VERSION + N'-05. Engage Data Update Since 2019-03-26'

    IF EXISTS (SELECT 1 FROM fn_listextendedproperty(N'cxStudio Database Version', NULL, NULL, NULL, NULL, NULL, NULL))
        EXEC sp_dropextendedproperty N'cxStudio Database Version', NULL, NULL, NULL, NULL, NULL, NULL
    EXEC sp_addextendedproperty N'cxStudio Database Version', @DB_VERSION, NULL, NULL, NULL, NULL, NULL, NULL
		
    IF EXISTS (SELECT 1 FROM fn_listextendedproperty(N'Script Name', NULL, NULL, NULL, NULL, NULL, NULL))
        EXEC sp_dropextendedproperty N'Script Name', NULL, NULL, NULL, NULL, NULL, NULL
    EXEC sp_addextendedproperty N'Script Name',  @ReleaseScriptName , NULL, NULL, NULL, NULL, NULL, NULL
END
/*
End of versioning database-level extended properties.
*/
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
IF EXISTS (SELECT * FROM #tmpErrors) ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT>0 BEGIN
PRINT 'The database update succeeded'
COMMIT TRANSACTION
END
ELSE PRINT 'The database update failed'
GO
DROP TABLE #tmpErrors
GO
SET NOEXEC OFF
GO
