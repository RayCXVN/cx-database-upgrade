SET QUOTED_IDENTIFIER ON
GO
DECLARE @Current_DB_Version varchar(32), @CurrentConceptName nvarchar(128),
        @Required_DB_Version varchar(32), @RequiredConceptName nvarchar(128)

SET @Required_DB_Version = '6.12.006.002.%'
SET @RequiredConceptName = 'Engage Main'
PRINT 'This script is generated for executing on ' + @RequiredConceptName + ' version ' + @Required_DB_Version

SELECT @Current_DB_Version = convert(nvarchar(max),value) FROM fn_listextendedproperty(N'cxStudio Database Version', NULL, NULL, NULL, NULL, NULL, NULL)
SELECT @CurrentConceptName = convert(nvarchar(max),value) FROM fn_listextendedproperty(N'Concept Name', NULL, NULL, NULL, NULL, NULL, NULL)

IF @CurrentConceptName != @RequiredConceptName OR @Current_DB_Version NOT LIKE @Required_DB_Version
BEGIN
    PRINT 'Incorrect solution or cxStudio Database version'
    SET NOEXEC ON
END
GO
-------------------------------------------------------------------------------------------------------------------------
IF EXISTS (SELECT * FROM tempdb..sysobjects WHERE id=OBJECT_ID('tempdb..#tmpErrors')) DROP TABLE #tmpErrors
GO
CREATE TABLE #tmpErrors (Error int)
GO
SET XACT_ABORT ON
GO
SET TRANSACTION ISOLATION LEVEL SERIALIZABLE
GO
BEGIN TRANSACTION
GO
----------------------------------------------------------------------------------------------------------------
--#region 101. CXPLAT-1067 CalcType For Alternative
-- Dev: Jed
-- Feature: Average CalcType
-- CXPLAT-1067 CalcType For Alternative
IF NOT EXISTS (SELECT 1 FROM [prop].[PropPage] [PP] WHERE [PP].[MasterID] = 'D3D980D9-AEA1-49F6-9617-8047FC83ED17')
BEGIN
    INSERT INTO [prop].[PropPage] ([TableTypeID], [Type], [No], [Created], [MasterID])
    SELECT [T].[TableTypeID] AS [TableTypeID], N'0' AS [Type], N'1' AS [No], GETDATE() AS [Created], N'D3D980D9-AEA1-49F6-9617-8047FC83ED17' AS [MasterID]
    FROM [dbo].[TableType] [T]
    WHERE [T].[MasterID] = '49BA9CFC-7E08-4CBE-9BE2-48D496FB512C';
    DECLARE @PropPageID_190913402651140 nvarchar(32)= SCOPE_IDENTITY();

    INSERT INTO [Prop].[LT_PropPage] ([LanguageID], [PropPageID], [Name], [Description])
    SELECT l.[LanguageID], @PropPageID_190913402651140 AS [PropPageID], N'Alternative property' AS [Name], N'Alternative property' AS [Description]
    FROM [dbo].[Language] l
    WHERE l.[LanguageID] IN (1,2,7);
END;

IF NOT EXISTS (SELECT 1 FROM [prop].[Prop] [p] WHERE [p].[MasterID] = 'C8ADEC88-1BA5-4329-A76F-17EA9409D0BD')
BEGIN
    INSERT INTO [prop].[Prop] ([PropPageID], [No], [Type], [ValueType], [FormatString], [MultiValue], [MasterID])
    SELECT [pp].[PropPageID] AS [PropPageID], N'1' AS [No], N'0' AS [Type], N'1' AS [ValueType], N'' AS [FormatString], N'0' AS [MultiValue], N'C8ADEC88-1BA5-4329-A76F-17EA9409D0BD' AS [MasterID]
    FROM [Prop].[PropPage] [pp]
    WHERE [pp].[MasterID] = 'D3D980D9-AEA1-49F6-9617-8047FC83ED17';
    DECLARE @PropertyID_10980912002813208 nvarchar(32)= SCOPE_IDENTITY();

    INSERT INTO [Prop].[LT_Prop] ([LanguageID], [PropertyID], [Name], [Description])
    SELECT l.[LanguageID], @PropertyID_10980912002813208 AS [PropertyID], N'Calculate average value' AS [Name], N'' AS [Description]
    FROM [dbo].[Language] l
    WHERE l.[LanguageID] IN (1,2,7);
END;

INSERT INTO [app].[SiteParameter] ([SiteID], [Key], [Value], [Created], [GroupName])
SELECT s.[SiteID], N'Vokal.Common.CalculatingAverageValue' AS [Key], p.[PropertyID] AS [Value], GETDATE() AS [Created], N'Vokal.Common' AS [GroupName]
FROM [app].[Site] s
JOIN [prop].[Prop] p ON p.[MasterID] = 'C8ADEC88-1BA5-4329-A76F-17EA9409D0BD'
WHERE NOT EXISTS (SELECT 1 FROM [app].[SiteParameter] sp WHERE sp.[SiteID] = s.[SiteID] AND sp.[Key] = 'Vokal.Common.CalculatingAverageValue');

--#endregion 101. CXPLAT-1067 CalcType For Alternative
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
--#region 108. CE-2155 Import Status
-- Dev: Thomas
-- Feature: CE-2155 Import status
-- CE-2206 [ServerCode] Import status

--#region DataSource 2162
IF NOT EXISTS (SELECT 1 FROM [list].[ItemListDataSource] [ds] WHERE [ds].[MasterID] = '418D26D1-B232-4A3F-BBEA-B365FFA48820')
BEGIN
    INSERT INTO [list].[ItemListDataSource] ([OwnerID], [FunctionName], [Created], [MasterID])
    SELECT [o].[OwnerID] AS [OwnerID], N'GetImportStatus' AS [FunctionName], GETDATE() AS [Created], N'418D26D1-B232-4A3F-BBEA-B365FFA48820' AS [MasterID]
    FROM [org].[Owner] [o]
    WHERE [o].[MasterID] = '1C483E02-E808-42A7-8210-B11F4051D2E4';
    DECLARE @ItemListDataSourceID_2162 nvarchar(32)= SCOPE_IDENTITY();

    INSERT INTO [list].[LT_ItemListDataSource] ([LanguageID], [ItemListDataSourceID], [Name], [Description])
    SELECT l.[LanguageID], @ItemListDataSourceID_2162 AS [ItemListDataSourceID], N'Get import status' AS [Name], N'' AS [Description]
    FROM [dbo].[Language] l
    WHERE l.[LanguageID] IN (1,2,7);

    IF NOT EXISTS (SELECT 1 FROM [list].[ItemListDataParameter] [dp] WHERE [dp].[MasterID] = '39C46909-4B6E-42EB-8484-ADC63392FCB7')
    BEGIN
        INSERT INTO [list].[ItemListDataParameter] ([ItemListDataSourceID], [DataType], [Type], [Mandatory], [DefaultValue], [ConstructionPattern], [Created], [MasterID])
        SELECT [ds].[ItemListDataSourceID] AS [ItemListDataSourceID], N'Integer' AS [DataType], N'PortalPartParam' AS [Type], N'0' AS [Mandatory], N'' AS [DefaultValue], N'' AS [ConstructionPattern], GETDATE() AS [Created], N'39C46909-4B6E-42EB-8484-ADC63392FCB7' AS [MasterID]
        FROM [list].[ItemListDataSource] [ds]
        WHERE [ds].[MasterID] = '418D26D1-B232-4A3F-BBEA-B365FFA48820';
        DECLARE @ParamID_3793 nvarchar(32)= SCOPE_IDENTITY();

        INSERT INTO [list].[LT_ItemListDataParameter] ([LanguageID], [ItemListDataParameterID], [Name], [Description])
        SELECT l.[LanguageID], @ParamID_3793 AS [ItemListDataParameterID], N'PeriodStatus' AS [Name], N'' AS [Description]
        FROM [dbo].[Language] l
        WHERE l.[LanguageID] IN (1,2,7);
    END;
END;
--#endregion DataSource 2162

--#region PortalPart 5630
IF NOT EXISTS (SELECT 1 FROM [app].[PortalPart] [pt] WHERE [pt].[MasterID] = '11B9F4DC-BB02-4291-8688-F0688E966F09')
BEGIN
    INSERT INTO [app].[PortalPart] ([PortalPageID], [PortalPartTypeID], [No], [CategoryID], [Settings], [BubbleID], [ReportPartID], [ReportID], [CssClass], [InlineStyle], [ActivityID], [SurveyID], [ExtID], [MasterID])
    SELECT [pp].[PortalPageID] AS [PortalPageID], N'1' AS [PortalPartTypeID], N'17' AS [No], NULL AS [CategoryID], N'<params>IsEmptyHtml%3D'+'true'+'</params>' AS [Settings], NULL AS [BubbleID], NULL AS [ReportPartID], NULL AS [ReportID], N'container-command-info col-sm-4 col-xs-12 popup-default box-floating popup-fixed no-padding no-right-radius ' AS [CssClass], N'' AS [InlineStyle], NULL AS [ActivityID], NULL AS [SurveyID], N'' AS [ExtID], N'11B9F4DC-BB02-4291-8688-F0688E966F09' AS [MasterID]
    FROM [app].[PortalPage] [pp]
    WHERE [pp].[MasterID] = '0678F421-C8FE-4441-A0E1-655A4CEE3B68';
    DECLARE @PortalPartID_5630 nvarchar(32)= SCOPE_IDENTITY();

    INSERT INTO [app].[LT_PortalPart] ([LanguageID], [PortalPartID], [Name], [Description], [Text], [TextAbove], [TextBelow], [HeaderText], [FooterText], [BeforeContentText], [AfterContentText])
    SELECT l.[LanguageID], @PortalPartID_5630 AS [PortalPartID], N'Import status' AS [Name], N'' AS [Description], N'<div class="modal-header">    <button data-dismiss="modal" class="close" type="button"><span aria-hidden="true">&times;</span><span class="sr-only">Lukk</span></button>    <h4 class="modal-title">Import status  </h4></div><div class="modal-body">    <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aenean euismod bibendum laoreet. Proin gravida dolor sit amet lacus accumsan et viverra justo.</p>
<p>Proin sodales pulvinar sic tempor. Sociis natoque penatibus et magnis dis parturient montes</p>
</div><div class="col-xs-12">    <button class="btn btn-primary pull-right" type="button" data-dismiss="modal">Lukk</button></div><div class="border"></div>' AS [Text], N'' AS [TextAbove], N'' AS [TextBelow], N'' AS [HeaderText], N'' AS [FooterText], N'' AS [BeforeContentText], N'' AS [AfterContentText]
    FROM [dbo].[Language] l
    WHERE l.[LanguageID] IN (1,2,7);
END;
--#endregion PortalPart 5630

--#region ItemList 1410
DECLARE @DataSourceID_2162_143726353801914101 nvarchar(32)= '';
SELECT @DataSourceID_2162_143726353801914101 = [ds].[ItemListDataSourceID]
FROM [list].[ItemListDataSource] [ds]
WHERE [ds].[MasterID] = '418D26D1-B232-4A3F-BBEA-B365FFA48820';
IF NOT EXISTS (SELECT 1 FROM [list].[ItemList] [l] WHERE [l].[MasterID] = 'E2FE7E5F-D429-4BB2-A1FF-5C5E8B86380E')
BEGIN
    INSERT INTO [list].[ItemList] ([OwnerID], [ItemListDataSourceID], [ListTableCSSClass], [ListTableCSSCStyle], [ListHeaderRowCSSClass], [ListRowCSSClass], [ListAltRowCSSClass], [ExpandingItemListDatasourceID], [ExpandingListRowCSSClass], [ExpandingListAltRowCSSClass], [ExpandingExpression], [InsertMultiCheckColumn], [DisableCheckColumnExpression], [DisplayUpperCmdBarThreshold], [MaxRows], [ExtID], [Created], [MasterID])
    SELECT [o].[OwnerID] AS [OwnerID], @DataSourceID_2162_143726353801914101 AS [ItemListDataSourceID], N'table-user stripe no-border tablesorter tablesorter-default' AS [ListTableCSSClass], N'' AS [ListTableCSSCStyle], N'th' AS [ListHeaderRowCSSClass], N'' AS [ListRowCSSClass], N'trAlt' AS [ListAltRowCSSClass], NULL AS [ExpandingItemListDatasourceID], N'' AS [ExpandingListRowCSSClass], N'' AS [ExpandingListAltRowCSSClass], N'' AS [ExpandingExpression], N'0' AS [InsertMultiCheckColumn], N'' AS [DisableCheckColumnExpression], N'15' AS [DisplayUpperCmdBarThreshold], N'-1' AS [MaxRows], N'' AS [ExtID], GETDATE() AS [Created], N'E2FE7E5F-D429-4BB2-A1FF-5C5E8B86380E' AS [MasterID]
    FROM [org].[Owner] [o]
    WHERE [o].[MasterID] = '1C483E02-E808-42A7-8210-B11F4051D2E4';
    DECLARE @ItemListID_1410 nvarchar(32)= SCOPE_IDENTITY();

    INSERT INTO [list].[LT_ItemList] ([LanguageID], [ItemListID], [Name], [Description], [RowTooltipText], [EmptyListText])
    SELECT l.[LanguageID], @ItemListID_1410 AS [ItemListID], N'[Admin] Import status' AS [Name], N'Administration - Import status' AS [Description], N'' AS [RowTooltipText], N'' AS [EmptyListText]
    FROM [dbo].[Language] l
    WHERE l.[LanguageID] IN (1,2,7);

    IF NOT EXISTS (SELECT 1 FROM [list].[ItemListCommand] [c] WHERE [c].[MasterID] = '1C6B606F-2EB3-44C7-AF4F-9407E23F1BD1')
    BEGIN
        INSERT INTO [list].[ItemListCommand] ([ItemListID], [PlaceAbove], [PlaceBelow], [MultiCheckCommand], [ClientFunction], [DisabledInEmptyList], [CssClass], [Created], [No], [MasterID])
        SELECT [l].[ItemListID] AS [ItemListID], N'1' AS [PlaceAbove], N'1' AS [PlaceBelow], N'0' AS [MultiCheckCommand], N'exportGenericPage(2,0,0,''&excludePortalParts=@PortalPartID_5631_next'');' AS [ClientFunction], N'0' AS [DisabledInEmptyList], N'icon-vokal-excel button' AS [CssClass], GETDATE() AS [Created], N'0' AS [No], N'1C6B606F-2EB3-44C7-AF4F-9407E23F1BD1' AS [MasterID]
        FROM [list].[ItemList] [l]
        WHERE [l].[MasterID] = 'E2FE7E5F-D429-4BB2-A1FF-5C5E8B86380E';
        DECLARE @ListCommandID_194 nvarchar(32)= SCOPE_IDENTITY();

        INSERT INTO [list].[LT_ItemListCommand] ([LanguageID], [ItemListCommandID], [Name], [Description], [Text], [TooltipText], [HotKey])
        SELECT l.[LanguageID], @ListCommandID_194 AS [ItemListCommandID], N'Eksporter til regneark' AS [Name], N'' AS [Description], N'Eksport' AS [Text], N'' AS [TooltipText], N'' AS [HotKey]
        FROM [dbo].[Language] l
        WHERE l.[LanguageID] IN (1,2,7);
    END;
    
    IF NOT EXISTS (SELECT 1 FROM [list].[ItemListField] [f] WHERE [f].[MasterID] = '9368D26D-1032-4F38-8965-4CD6DA5D5252')
    BEGIN
        INSERT INTO [list].[ItemListField] ([ItemListID], [No], [TableTypeID], [FieldType], [ValueType], [FieldName], [PropID], [QuestionID], [OverrideLocked], [ShowInUserList], [ShowInTableView], [ShowInExport], [Mandatory], [Sortable], [SortNo], [SortDirection], [Width], [Align], [ItemPrefix], [ItemSuffix], [HeaderCssClass], [ItemCssClass], [Created], [ColumnCssClass], [HeaderGroupCSSClass], [HeaderGroupLastItem], [MasterID])
        SELECT [l].[ItemListID] AS [ItemListID], N'2' AS [No], NULL AS [TableTypeID], N'0' AS [FieldType], N'0' AS [ValueType], N'Status' AS [FieldName], N'0' AS [PropID], NULL AS [QuestionID], N'0' AS [OverrideLocked], N'0' AS [ShowInUserList], N'0' AS [ShowInTableView], N'1' AS [ShowInExport], N'0' AS [Mandatory], N'0' AS [Sortable], N'1' AS [SortNo], N'1' AS [SortDirection], N'250' AS [Width], N'7' AS [Align], N'' AS [ItemPrefix], N'' AS [ItemSuffix], N'tdSort parser-text sorter-text' AS [HeaderCssClass], N'td' AS [ItemCssClass], GETDATE() AS [Created], N'' AS [ColumnCssClass], N'tblListGroupTd' AS [HeaderGroupCSSClass], N'0' AS [HeaderGroupLastItem], N'9368D26D-1032-4F38-8965-4CD6DA5D5252' AS [MasterID]
        FROM [list].[ItemList] [l]
        WHERE [l].[MasterID] = 'E2FE7E5F-D429-4BB2-A1FF-5C5E8B86380E';
        DECLARE @ItemListFieldID_3758 nvarchar(32)= SCOPE_IDENTITY();

        INSERT INTO [list].[LT_ItemListField] ([LanguageID], [ItemListFieldID], [Name], [Description], [HeaderText], [HeaderGroupText], [HeaderTooltipText], [ItemTooltipText], [ItemText], [ExpandingItemText], [DefaultValues], [AvailableValues])
        SELECT l.[LanguageID], @ItemListFieldID_3758 AS [ItemListFieldID], N'Status' AS [Name], N'Status' AS [Description], N'Status' AS [HeaderText], N'' AS [HeaderGroupText], N'' AS [HeaderTooltipText], N'' AS [ItemTooltipText], N'{if({"{Status}"="True"},"<span class="icon-vokal-checked green"></span>Ok","<span class="icon-vokal-warning red"></span>Feil")}' AS [ItemText], N'' AS [ExpandingItemText], N'' AS [DefaultValues], N'' AS [AvailableValues]
        FROM [dbo].[Language] l
        WHERE l.[LanguageID] IN (1,2,7);
    END;

    IF NOT EXISTS (SELECT 1 FROM [list].[ItemListField] [f] WHERE [f].[MasterID] = 'BC330076-CAA2-41B4-88EA-E70EA07F174D')
    BEGIN
        INSERT INTO [list].[ItemListField] ([ItemListID], [No], [TableTypeID], [FieldType], [ValueType], [FieldName], [PropID], [QuestionID], [OverrideLocked], [ShowInUserList], [ShowInTableView], [ShowInExport], [Mandatory], [Sortable], [SortNo], [SortDirection], [Width], [Align], [ItemPrefix], [ItemSuffix], [HeaderCssClass], [ItemCssClass], [Created], [ColumnCssClass], [HeaderGroupCSSClass], [HeaderGroupLastItem], [MasterID])
        SELECT [l].[ItemListID] AS [ItemListID], N'4' AS [No], NULL AS [TableTypeID], N'0' AS [FieldType], N'0' AS [ValueType], N'EndDate' AS [FieldName], N'0' AS [PropID], NULL AS [QuestionID], N'0' AS [OverrideLocked], N'0' AS [ShowInUserList], N'0' AS [ShowInTableView], N'1' AS [ShowInExport], N'0' AS [Mandatory], N'0' AS [Sortable], N'-1' AS [SortNo], N'1' AS [SortDirection], N'150' AS [Width], N'7' AS [Align], N'' AS [ItemPrefix], N'' AS [ItemSuffix], N'tdSort parser-text' AS [HeaderCssClass], N'td' AS [ItemCssClass], GETDATE() AS [Created], N'' AS [ColumnCssClass], N'tblListGroupTd' AS [HeaderGroupCSSClass], N'0' AS [HeaderGroupLastItem], N'BC330076-CAA2-41B4-88EA-E70EA07F174D' AS [MasterID]
        FROM [list].[ItemList] [l]
        WHERE [l].[MasterID] = 'E2FE7E5F-D429-4BB2-A1FF-5C5E8B86380E';
        DECLARE @ItemListFieldID_3762 nvarchar(32)= SCOPE_IDENTITY();

        INSERT INTO [list].[LT_ItemListField] ([LanguageID], [ItemListFieldID], [Name], [Description], [HeaderText], [HeaderGroupText], [HeaderTooltipText], [ItemTooltipText], [ItemText], [ExpandingItemText], [DefaultValues], [AvailableValues])
        SELECT l.[LanguageID], @ItemListFieldID_3762 AS [ItemListFieldID], N'Sluttid' AS [Name], N'Sluttid' AS [Description], N'Sluttid' AS [HeaderText], N'' AS [HeaderGroupText], N'' AS [HeaderTooltipText], N'' AS [ItemTooltipText], N'{GetDateTimeString("{EndDate}","HH:mm:ss")}' AS [ItemText], N'' AS [ExpandingItemText], N'' AS [DefaultValues], N'' AS [AvailableValues]
        FROM [dbo].[Language] l
        WHERE l.[LanguageID] IN (1,2,7);
    END;

    IF NOT EXISTS (SELECT 1 FROM [list].[ItemListField] [f] WHERE [f].[MasterID] = 'B42E1957-1976-4CC0-9C03-55E97DBFC2E9')
    BEGIN
        INSERT INTO [list].[ItemListField] ([ItemListID], [No], [TableTypeID], [FieldType], [ValueType], [FieldName], [PropID], [QuestionID], [OverrideLocked], [ShowInUserList], [ShowInTableView], [ShowInExport], [Mandatory], [Sortable], [SortNo], [SortDirection], [Width], [Align], [ItemPrefix], [ItemSuffix], [HeaderCssClass], [ItemCssClass], [Created], [ColumnCssClass], [HeaderGroupCSSClass], [HeaderGroupLastItem], [MasterID])
        SELECT [l].[ItemListID] AS [ItemListID], N'3' AS [No], NULL AS [TableTypeID], N'0' AS [FieldType], N'0' AS [ValueType], N'StartDate' AS [FieldName], N'0' AS [PropID], NULL AS [QuestionID], N'0' AS [OverrideLocked], N'0' AS [ShowInUserList], N'0' AS [ShowInTableView], N'1' AS [ShowInExport], N'0' AS [Mandatory], N'0' AS [Sortable], N'-1' AS [SortNo], N'1' AS [SortDirection], N'250' AS [Width], N'7' AS [Align], N'' AS [ItemPrefix], N'' AS [ItemSuffix], N'tdSort parser-text' AS [HeaderCssClass], N'td' AS [ItemCssClass], GETDATE() AS [Created], N'' AS [ColumnCssClass], N'tblListGroupTd' AS [HeaderGroupCSSClass], N'0' AS [HeaderGroupLastItem], N'B42E1957-1976-4CC0-9C03-55E97DBFC2E9' AS [MasterID]
        FROM [list].[ItemList] [l]
        WHERE [l].[MasterID] = 'E2FE7E5F-D429-4BB2-A1FF-5C5E8B86380E';
        DECLARE @ItemListFieldID_3763 nvarchar(32)= SCOPE_IDENTITY();

        INSERT INTO [list].[LT_ItemListField] ([LanguageID], [ItemListFieldID], [Name], [Description], [HeaderText], [HeaderGroupText], [HeaderTooltipText], [ItemTooltipText], [ItemText], [ExpandingItemText], [DefaultValues], [AvailableValues])
        SELECT l.[LanguageID], @ItemListFieldID_3763 AS [ItemListFieldID], N'Starttid' AS [Name], N'Starttid' AS [Description], N'Starttid' AS [HeaderText], N'' AS [HeaderGroupText], N'' AS [HeaderTooltipText], N'' AS [ItemTooltipText], N'{GetDateTimeString("{StartDate}","HH:mm:ss")}' AS [ItemText], N'' AS [ExpandingItemText], N'' AS [DefaultValues], N'' AS [AvailableValues]
        FROM [dbo].[Language] l
        WHERE l.[LanguageID] IN (1,2,7);
    END;

    IF NOT EXISTS (SELECT 1 FROM [list].[ItemListField] [f] WHERE [f].[MasterID] = '1AB40B35-E5C9-4DC9-8D63-18E91A9D094C')
    BEGIN
        INSERT INTO [list].[ItemListField] ([ItemListID], [No], [TableTypeID], [FieldType], [ValueType], [FieldName], [PropID], [QuestionID], [OverrideLocked], [ShowInUserList], [ShowInTableView], [ShowInExport], [Mandatory], [Sortable], [SortNo], [SortDirection], [Width], [Align], [ItemPrefix], [ItemSuffix], [HeaderCssClass], [ItemCssClass], [Created], [ColumnCssClass], [HeaderGroupCSSClass], [HeaderGroupLastItem], [MasterID])
        SELECT [l].[ItemListID] AS [ItemListID], N'1' AS [No], NULL AS [TableTypeID], N'0' AS [FieldType], N'0' AS [ValueType], N'Created' AS [FieldName], N'0' AS [PropID], NULL AS [QuestionID], N'0' AS [OverrideLocked], N'0' AS [ShowInUserList], N'0' AS [ShowInTableView], N'1' AS [ShowInExport], N'0' AS [Mandatory], N'0' AS [Sortable], N'1' AS [SortNo], N'0' AS [SortDirection], N'250' AS [Width], N'7' AS [Align], N'' AS [ItemPrefix], N'' AS [ItemSuffix], N'tdSort parser-text' AS [HeaderCssClass], N'td' AS [ItemCssClass], GETDATE() AS [Created], N'' AS [ColumnCssClass], N'tblListGroupTd' AS [HeaderGroupCSSClass], N'0' AS [HeaderGroupLastItem], N'1AB40B35-E5C9-4DC9-8D63-18E91A9D094C' AS [MasterID]
        FROM [list].[ItemList] [l]
        WHERE [l].[MasterID] = 'E2FE7E5F-D429-4BB2-A1FF-5C5E8B86380E';
        DECLARE @ItemListFieldID_3764 nvarchar(32)= SCOPE_IDENTITY();

        INSERT INTO [list].[LT_ItemListField] ([LanguageID], [ItemListFieldID], [Name], [Description], [HeaderText], [HeaderGroupText], [HeaderTooltipText], [ItemTooltipText], [ItemText], [ExpandingItemText], [DefaultValues], [AvailableValues])
        SELECT l.[LanguageID], @ItemListFieldID_3764 AS [ItemListFieldID], N'Dato' AS [Name], N'Dato' AS [Description], N'Dato' AS [HeaderText], N'' AS [HeaderGroupText], N'' AS [HeaderTooltipText], N'' AS [ItemTooltipText], N'{GetDateTimeString("{Created}","ddd dd.MM.yyyy")}' AS [ItemText], N'' AS [ExpandingItemText], N'' AS [DefaultValues], N'' AS [AvailableValues]
        FROM [dbo].[Language] l
        WHERE l.[LanguageID] IN (1,2,7);
    END;
END;
--#endregion ItemList 1410

--#region PortalPage 3897
IF NOT EXISTS (SELECT 1 FROM [app].[PortalPage] [pp] WHERE [pp].[MasterID] = '6057F4F1-C6A1-4B99-8BCC-BF9A31A0C98A')
BEGIN
    INSERT INTO [app].[PortalPage] ([OwnerID], [No], [Parameter], [ExtID], [Created], [MasterID])
    SELECT [o].[OwnerID] AS [OwnerID], N'26' AS [No], N'RenderToolbar=false&rendername=false&cssClass=clearfix container-administration container-import-status' AS [Parameter], N'' AS [ExtID], GETDATE() AS [Created], N'6057F4F1-C6A1-4B99-8BCC-BF9A31A0C98A' AS [MasterID]
    FROM [org].[Owner] [o]
    WHERE [o].[MasterID] = '1C483E02-E808-42A7-8210-B11F4051D2E4';
    DECLARE @PortalPageID_3897 nvarchar(32)= SCOPE_IDENTITY();

    INSERT INTO [app].[LT_PortalPage] ([LanguageID], [PortalPageID], [Name], [Description])
    SELECT l.[LanguageID], @PortalPageID_3897 AS [PortalPageID], N'Import status' AS [Name], N'' AS [Description]
    FROM [dbo].[Language] l
    WHERE l.[LanguageID] IN (1,2,7);

    DECLARE @ItemListID_1410_14503085200381 nvarchar(32)= '';
    SELECT @ItemListID_1410_14503085200381 = [l].[ItemListID]
    FROM [list].[ItemList] [l]
    WHERE [l].[MasterID] = 'E2FE7E5F-D429-4BB2-A1FF-5C5E8B86380E';
    IF NOT EXISTS (SELECT 1 FROM [app].[PortalPart] [pt] WHERE [pt].[MasterID] = 'AA6DB8B3-6D49-4713-9CFF-18F9FB55BDA3')
    BEGIN
        INSERT INTO [app].[PortalPart] ([PortalPageID], [PortalPartTypeID], [No], [CategoryID], [Settings], [BubbleID], [ReportPartID], [ReportID], [CssClass], [InlineStyle], [ActivityID], [SurveyID], [ExtID], [MasterID])
        SELECT [pp].[PortalPageID] AS [PortalPageID], N'21' AS [PortalPartTypeID], N'2' AS [No], NULL AS [CategoryID], N'<params>listid%3D'+@ItemListID_1410_14503085200381+'%26groupbox%3D'+'false'+'</params>' AS [Settings], NULL AS [BubbleID], NULL AS [ReportPartID], NULL AS [ReportID], N'' AS [CssClass], N'' AS [InlineStyle], NULL AS [ActivityID], NULL AS [SurveyID], N'' AS [ExtID], N'AA6DB8B3-6D49-4713-9CFF-18F9FB55BDA3' AS [MasterID]
        FROM [app].[PortalPage] [pp]
        WHERE [pp].[MasterID] = '6057F4F1-C6A1-4B99-8BCC-BF9A31A0C98A';
        DECLARE @PortalPartID_5628 nvarchar(32)= SCOPE_IDENTITY();

        INSERT INTO [app].[LT_PortalPart] ([LanguageID], [PortalPartID], [Name], [Description], [Text], [TextAbove], [TextBelow], [HeaderText], [FooterText], [BeforeContentText], [AfterContentText])
        SELECT l.[LanguageID], @PortalPartID_5628 AS [PortalPartID], N'Import status' AS [Name], N'' AS [Description], N'' AS [Text], N'' AS [TextAbove], N'' AS [TextBelow], N'' AS [HeaderText], N'' AS [FooterText], N'' AS [BeforeContentText], N'' AS [AfterContentText]
        FROM [dbo].[Language] l
        WHERE l.[LanguageID] IN (1,2,7);
    END;

    IF NOT EXISTS (SELECT 1 FROM [app].[PortalPart] [pt] WHERE [pt].[MasterID] = '28050334-4127-4179-8384-8556DFA4B883')
    BEGIN
        INSERT INTO [app].[PortalPart] ([PortalPageID], [PortalPartTypeID], [No], [CategoryID], [Settings], [BubbleID], [ReportPartID], [ReportID], [CssClass], [InlineStyle], [ActivityID], [SurveyID], [ExtID], [MasterID])
        SELECT [pp].[PortalPageID] AS [PortalPageID], N'1' AS [PortalPartTypeID], N'1' AS [No], NULL AS [CategoryID], N'<params/>' AS [Settings], NULL AS [BubbleID], NULL AS [ReportPartID], NULL AS [ReportID], N'' AS [CssClass], N'' AS [InlineStyle], NULL AS [ActivityID], NULL AS [SurveyID], N'' AS [ExtID], N'28050334-4127-4179-8384-8556DFA4B883' AS [MasterID]
        FROM [app].[PortalPage] [pp]
        WHERE [pp].[MasterID] = '6057F4F1-C6A1-4B99-8BCC-BF9A31A0C98A';
        DECLARE @PortalPartID_5631 nvarchar(32)= SCOPE_IDENTITY();

        DECLARE @PortalPartID_5630_next nvarchar(32) = '';
        SELECT @PortalPartID_5630_next = pt.[PortalPartID]
        FROM [app].[PortalPart] pt
        WHERE pt.[MasterID] = '11B9F4DC-BB02-4291-8688-F0688E966F09';

        INSERT INTO [app].[LT_PortalPart] ([LanguageID], [PortalPartID], [Name], [Description], [Text], [TextAbove], [TextBelow], [HeaderText], [FooterText], [BeforeContentText], [AfterContentText])
        SELECT l.[LanguageID], @PortalPartID_5631 AS [PortalPartID], N'Warning message and period' AS [Name], N'' AS [Description], N'' AS [Text], N'<div class="container-table-command"><table class="table" data-nosort="true"><colgroup><col><col><col><col></colgroup><tbody><tr><td title="Informasjon" onclick="renderUserlistInfo($(this),'+@PortalPartID_5630_next+');"><div class="btn-slide-command text-left"><span class="icon-vokal-info"></span></div></td></tr></tbody></table></div>

<div class="command-filter">
    <label>Periode:</label>
    <div class="dropdown-default">
        <select id="selPeriod">
            <option value="1" selected=''selected''>Gjeldende status</option>
            <option value="2">En uke</option>
            <option value="3">To uke</option>
            <option value="4">En m�ned</option>
            <option value="5">Tre m�neder</option>
        </select>
        <span class="icon-vokal-down-arrow"></span>
    </div>
</div>

<div id="import-status-false" class="box-top-message" style="display:none">
    <span class="icon-vokal-warning red"></span>Siste integrering mislyktes
    Conexus Engage har ikke den nyeste informasjonen fra skolens administrative system
</div>

<div id="import-status-ok" class="box-top-message" style="display:none">
    <span class="icon-vokal-checked green"></span>Siste integrering var vellykket
    Conexus Engage har den nyeste informasjonen fra skolens administrative system
</div>' AS [TextAbove], N'' AS [TextBelow], N'' AS [HeaderText], N'' AS [FooterText], N'' AS [BeforeContentText], N'' AS [AfterContentText]
        FROM [dbo].[Language] l
        WHERE l.[LanguageID] IN (1,2,7);
    END;
END;
--#endregion PortalPage 3897

UPDATE c SET c.[ClientFunction] = REPLACE(c.[ClientFunction], '@PortalPartID_5631_next', @PortalPartID_5631) FROM [list].[ItemListCommand] [c] WHERE [c].[MasterID] = '1C6B606F-2EB3-44C7-AF4F-9407E23F1BD1';

--#region MenuItem 3520
DECLARE @MenuItemID_2345_15570764215833520 nvarchar(32)= '';
SELECT @MenuItemID_2345_15570764215833520 = [MI].[MenuItemID]
FROM [app].[MenuItem] [MI]
WHERE [MI].[MasterID] = 'E7449FCD-5A34-4186-AE2B-ECF40F558387';

DECLARE @PortalPageID_3897_15570764215833520 nvarchar(32)= '';
SELECT @PortalPageID_3897_15570764215833520 = [pp].[PortalPageID]
FROM [app].[PortalPage] [pp]
WHERE [pp].[MasterID] = '6057F4F1-C6A1-4B99-8BCC-BF9A31A0C98A';
IF NOT EXISTS (SELECT 1 FROM [app].[MenuItem] [mi] WHERE [mi].[MasterID] = 'EDCB8DB0-753B-43A6-BD80-D42CA1139604')
BEGIN
    DECLARE @ItemOrderNo int
    SELECT @ItemOrderNo = mi.[No]
    FROM [app].[MenuItem] mi
    WHERE mi.[MasterID] = 'DF070EA7-CD80-4FE1-A66D-5AC09936B6AF';

    UPDATE mi SET mi.[No] = mi.[No] + 1 FROM [app].[MenuItem] mi WHERE mi.[ParentID] = @MenuItemID_2345_15570764215833520 AND mi.[No] >= @ItemOrderNo;

    INSERT INTO [app].[MenuItem] ([MenuID], [ParentID], [MenuItemTypeID], [PortalPageID], [No], [CssClass], [URL], [Target], [IsDefault], [Created], [Active], [ExtID], [NoRender], [MasterID])
    SELECT [m].[MenuID] AS [MenuID], @MenuItemID_2345_15570764215833520 AS [ParentID], N'4' AS [MenuItemTypeID], @PortalPageID_3897_15570764215833520 AS [PortalPageID], @ItemOrderNo AS [No], N' right' AS [CssClass], N'' AS [URL], N'' AS [Target], N'0' AS [IsDefault], GETDATE() AS [Created], N'1' AS [Active], N'' AS [ExtID], N'0' AS [NoRender], N'EDCB8DB0-753B-43A6-BD80-D42CA1139604' AS [MasterID]
    FROM [app].[Menu] [m]
    WHERE [m].[MasterID] = '13BCDBA6-29B7-4079-AF97-067AD541D27B';
    DECLARE @MenuItemID_3520 nvarchar(32)= SCOPE_IDENTITY();

    INSERT INTO [app].[LT_MenuItem] ([LanguageID], [MenuItemID], [Name], [Description], [ToolTip], [URL])
    SELECT l.[LanguageID], @MenuItemID_3520 AS [MenuItemID], N'Import status' AS [Name], N'' AS [Description], N'' AS [ToolTip], N'' AS [URL]
    FROM [dbo].[Language] l
    WHERE l.[LanguageID] IN (1,2,7);
END;
--#endregion MenuItem 3520

--#endregion 108. CE-2155 Import Status
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
-------------------------------------------------------------------------------------------------------------------
PRINT 'Setting database version'
/*
Start of versioning database-level extended properties.
*/
BEGIN
    DECLARE @DB_VERSION varchar(32), @ConceptName nvarchar(128), @ReleaseScriptName nvarchar(4000)
    SET @DB_VERSION = '6.12.006.003.00'
    SET @ConceptName = 'Engage Main'
    SET @ReleaseScriptName = N'v' + @DB_VERSION + N'-02. Engage Data Update Since 2018-12-21'

    IF EXISTS (SELECT 1 FROM fn_listextendedproperty(N'cxStudio Database Version', NULL, NULL, NULL, NULL, NULL, NULL))
        EXEC sp_dropextendedproperty N'cxStudio Database Version', NULL, NULL, NULL, NULL, NULL, NULL
    EXEC sp_addextendedproperty N'cxStudio Database Version', @DB_VERSION, NULL, NULL, NULL, NULL, NULL, NULL
		
    IF EXISTS (SELECT 1 FROM fn_listextendedproperty(N'Script Name', NULL, NULL, NULL, NULL, NULL, NULL))
        EXEC sp_dropextendedproperty N'Script Name', NULL, NULL, NULL, NULL, NULL, NULL
    EXEC sp_addextendedproperty N'Script Name',  @ReleaseScriptName , NULL, NULL, NULL, NULL, NULL, NULL
END
/*
End of versioning database-level extended properties.
*/
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
IF EXISTS (SELECT * FROM #tmpErrors) ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT>0 BEGIN
PRINT 'The database update succeeded'
COMMIT TRANSACTION
END
ELSE PRINT 'The database update failed'
GO
DROP TABLE #tmpErrors
GO
SET NOEXEC OFF
GO
-------------------------------------------------------------------------------------------------------------------------
DECLARE @Current_DB_Version varchar(32), @CurrentConceptName nvarchar(128),
        @Required_DB_Version varchar(32), @RequiredConceptName nvarchar(128)

SET @Required_DB_Version = '6.12.006.003.00'
SET @RequiredConceptName = 'Engage Main'
PRINT 'This script is generated for executing on ' + @RequiredConceptName + ' version ' + @Required_DB_Version

SELECT @Current_DB_Version = convert(nvarchar(max),value) FROM fn_listextendedproperty(N'cxStudio Database Version', NULL, NULL, NULL, NULL, NULL, NULL)
SELECT @CurrentConceptName = convert(nvarchar(max),value) FROM fn_listextendedproperty(N'Concept Name', NULL, NULL, NULL, NULL, NULL, NULL)

IF @CurrentConceptName != @RequiredConceptName OR @Current_DB_Version NOT LIKE @Required_DB_Version
BEGIN
    PRINT 'Incorrect solution or cxStudio Database version'
    SET NOEXEC ON
END
GO
-------------------------------------------------------------------------------------------------------------------------
IF EXISTS (SELECT * FROM tempdb..sysobjects WHERE id=OBJECT_ID('tempdb..#tmpErrors')) DROP TABLE #tmpErrors
GO
CREATE TABLE #tmpErrors (Error int)
GO
SET XACT_ABORT ON
GO
SET TRANSACTION ISOLATION LEVEL SERIALIZABLE
GO
BEGIN TRANSACTION
GO
----------------------------------------------------------------------------------------------------------------
--#region 109. CE-2189 Hide XCategory From PortalPart
-- Dev: Gary
-- CE-2189 Hide a xcategory from portal part
DECLARE @XCIDs_13505120973071 nvarchar(32)= '';
SELECT @XCIDs_13505120973071 = STUFF( (SELECT ','+CAST([xc].[XCID] AS nvarchar(32))
                                       FROM [at].[XCategory] [xc]
                                       WHERE [xc].[MasterID] IN ('CBB5C458-D4C2-4F33-8D85-A204F1B44FC8', '86F73266-7EF5-4D79-B28A-57B50780DCB3')
                                       ORDER BY CHARINDEX(CAST([xc].[MasterID] AS nvarchar(64)), N'CBB5C458-D4C2-4F33-8D85-A204F1B44FC8,86F73266-7EF5-4D79-B28A-57B50780DCB3')
                                       FOR XML PATH, TYPE
                                      ) .value ('.[1]', 'nvarchar(max)'), 1, 1, '');
IF NOT EXISTS (SELECT 1 FROM [list].[ItemListDataParameter] [dp] WHERE [dp].[MasterID] = '1A0C59DC-03D9-47BD-AA5A-807D7DC6D667')
BEGIN
    INSERT INTO [list].[ItemListDataParameter] ([ItemListDataSourceID], [DataType], [Type], [Mandatory], [DefaultValue], [ConstructionPattern], [Created], [MasterID])
    SELECT [ds].[ItemListDataSourceID] AS [ItemListDataSourceID], N'String' AS [DataType], N'PortalPartParam' AS [Type], N'0' AS [Mandatory], N''+@XCIDs_13505120973071+'' AS [DefaultValue], N'' AS [ConstructionPattern], GETDATE() AS [Created], N'1A0C59DC-03D9-47BD-AA5A-807D7DC6D667' AS [MasterID]
    FROM [list].[ItemListDataSource] [ds]
    WHERE [ds].[MasterID] = '87ECCE6F-561A-4D32-9A8E-EF37BD2BE256';
    DECLARE @ParamID_3801 nvarchar(32)= SCOPE_IDENTITY();

    INSERT INTO [list].[LT_ItemListDataParameter] ([LanguageID], [ItemListDataParameterID], [Name], [Description])
    SELECT l.[LanguageID], @ParamID_3801 AS [ItemListDataParameterID], N'hiddenXcategoryIds' AS [Name], N'' AS [Description]
    FROM [dbo].[Language] l
    WHERE l.[LanguageID] IN (1,2,7);
END;

DECLARE @XCIDs_13513803783201 nvarchar(32)= '';
SELECT @XCIDs_13513803783201 = STUFF( (SELECT ','+CAST([xc].[XCID] AS nvarchar(32))
                                       FROM [at].[XCategory] [xc]
                                       WHERE [xc].[MasterID] IN ('CBB5C458-D4C2-4F33-8D85-A204F1B44FC8')
                                       ORDER BY CHARINDEX(CAST([xc].[MasterID] AS nvarchar(64)), N'CBB5C458-D4C2-4F33-8D85-A204F1B44FC8')
                                       FOR XML PATH, TYPE
                                      ) .value ('.[1]', 'nvarchar(max)'), 1, 1, '');
IF NOT EXISTS (SELECT 1 FROM [list].[ItemListDataParameter] [dp] WHERE [dp].[MasterID] = 'F344065D-8FC3-4271-A0C9-B2F87310AF33')
BEGIN
    INSERT INTO [list].[ItemListDataParameter] ([ItemListDataSourceID], [DataType], [Type], [Mandatory], [DefaultValue], [ConstructionPattern], [Created], [MasterID])
    SELECT [ds].[ItemListDataSourceID] AS [ItemListDataSourceID], N'String' AS [DataType], N'PortalPartParam' AS [Type], N'0' AS [Mandatory], N''+@XCIDs_13513803783201+'' AS [DefaultValue], N'' AS [ConstructionPattern], GETDATE() AS [Created], N'F344065D-8FC3-4271-A0C9-B2F87310AF33' AS [MasterID]
    FROM [list].[ItemListDataSource] [ds]
    WHERE [ds].[MasterID] = 'DF09E3DE-7239-4F5A-A4E0-7993EF88C46D';
    DECLARE @ParamID_3802 nvarchar(32)= SCOPE_IDENTITY();

    INSERT INTO [list].[LT_ItemListDataParameter] ([LanguageID], [ItemListDataParameterID], [Name], [Description])
    SELECT l.[LanguageID], @ParamID_3802 AS [ItemListDataParameterID], N'hiddenXcategoryIds' AS [Name], N'' AS [Description]
    FROM [dbo].[Language] l
    WHERE l.[LanguageID] IN (1,2,7);
END;
--#endregion 109. CE-2189 Hide XCategory From PortalPart
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
-------------------------------------------------------------------------------------------------------------------
PRINT 'Setting database version'
/*
Start of versioning database-level extended properties.
*/
BEGIN
    DECLARE @DB_VERSION varchar(32), @ConceptName nvarchar(128), @ReleaseScriptName nvarchar(4000)
    SET @DB_VERSION = '6.12.006.004.00'
    SET @ConceptName = 'Engage Main'
    SET @ReleaseScriptName = N'v' + @DB_VERSION + N'-02. Engage Data Update Since 2018-12-21'

    IF EXISTS (SELECT 1 FROM fn_listextendedproperty(N'cxStudio Database Version', NULL, NULL, NULL, NULL, NULL, NULL))
        EXEC sp_dropextendedproperty N'cxStudio Database Version', NULL, NULL, NULL, NULL, NULL, NULL
    EXEC sp_addextendedproperty N'cxStudio Database Version', @DB_VERSION, NULL, NULL, NULL, NULL, NULL, NULL
		
    IF EXISTS (SELECT 1 FROM fn_listextendedproperty(N'Script Name', NULL, NULL, NULL, NULL, NULL, NULL))
        EXEC sp_dropextendedproperty N'Script Name', NULL, NULL, NULL, NULL, NULL, NULL
    EXEC sp_addextendedproperty N'Script Name',  @ReleaseScriptName , NULL, NULL, NULL, NULL, NULL, NULL
END
/*
End of versioning database-level extended properties.
*/
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
IF EXISTS (SELECT * FROM #tmpErrors) ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT>0 BEGIN
PRINT 'The database update succeeded'
COMMIT TRANSACTION
END
ELSE PRINT 'The database update failed'
GO
DROP TABLE #tmpErrors
GO
SET NOEXEC OFF
GO