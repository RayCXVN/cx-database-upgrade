SET QUOTED_IDENTIFIER ON
GO
DECLARE @Current_DB_Version varchar(32), @CurrentConceptName nvarchar(128),
        @Required_DB_Version varchar(32), @RequiredConceptName nvarchar(128)

SET @Required_DB_Version = '6.12.006.002.%'
SET @RequiredConceptName = 'Engage Main'
PRINT 'This script is generated for executing on ' + @RequiredConceptName + ' version ' + @Required_DB_Version

SELECT @Current_DB_Version = convert(nvarchar(max),value) FROM fn_listextendedproperty(N'cxStudio Database Version', NULL, NULL, NULL, NULL, NULL, NULL)
SELECT @CurrentConceptName = convert(nvarchar(max),value) FROM fn_listextendedproperty(N'Concept Name', NULL, NULL, NULL, NULL, NULL, NULL)

IF @CurrentConceptName != @RequiredConceptName OR @Current_DB_Version NOT LIKE @Required_DB_Version
BEGIN
    PRINT 'Incorrect solution or cxStudio Database version'
    SET NOEXEC ON
END
GO
-------------------------------------------------------------------------------------------------------------------------
IF EXISTS (SELECT * FROM tempdb..sysobjects WHERE id=OBJECT_ID('tempdb..#tmpErrors')) DROP TABLE #tmpErrors
GO
CREATE TABLE #tmpErrors (Error int)
GO
SET XACT_ABORT ON
GO
SET TRANSACTION ISOLATION LEVEL SERIALIZABLE
GO
BEGIN TRANSACTION
GO
----------------------------------------------------------------------------------------------------------------
--#region 100. CE-200 Individual View Of Activity 600
-- Dev: Gary
-- Feature: VGO activity
-- CE-200 Individual view of activity 600 (Oppfølging i fag)

--#region DataSource 2151
IF NOT EXISTS (SELECT 1 FROM [list].[ItemListDataSource] [ds] WHERE [ds].[MasterID] = '3120964C-13AD-45F9-8E1C-DFE3E1C7EC50')
BEGIN
    INSERT INTO [list].[ItemListDataSource] ([OwnerID], [FunctionName], [Created], [MasterID])
    SELECT [o].[OwnerID] AS [OwnerID], N'getvgoviewtypesummary' AS [FunctionName], GETDATE() AS [Created], N'3120964C-13AD-45F9-8E1C-DFE3E1C7EC50' AS [MasterID]
    FROM [org].[Owner] [o]
    WHERE [o].[MasterID] = '1C483E02-E808-42A7-8210-B11F4051D2E4';
    DECLARE @ItemListDataSourceID_2151 nvarchar(32)= SCOPE_IDENTITY();

    INSERT INTO [list].[LT_ItemListDataSource] ([LanguageID], [ItemListDataSourceID], [Name], [Description])
    SELECT l.[LanguageID], @ItemListDataSourceID_2151 AS [ItemListDataSourceID], N'VGO Viewtype Summary' AS [Name], N'' AS [Description]
    FROM [dbo].[Language] l
    WHERE l.[LanguageID] IN (1,2,7);

    IF NOT EXISTS (SELECT 1 FROM [list].[ItemListDataParameter] [dp] WHERE [dp].[MasterID] = 'DA245A32-9CC0-4BB5-8E87-1EEE92CFD0F3')
    BEGIN
        INSERT INTO [list].[ItemListDataParameter] ([ItemListDataSourceID], [DataType], [Type], [Mandatory], [DefaultValue], [ConstructionPattern], [Created], [MasterID])
        SELECT [ds].[ItemListDataSourceID] AS [ItemListDataSourceID], N'String' AS [DataType], N'PortalPartParam' AS [Type], N'0' AS [Mandatory], N''+'GetVGOViewtypeSummary'+'' AS [DefaultValue], N'' AS [ConstructionPattern], GETDATE() AS [Created], N'DA245A32-9CC0-4BB5-8E87-1EEE92CFD0F3' AS [MasterID]
        FROM [list].[ItemListDataSource] [ds]
        WHERE [ds].[MasterID] = '3120964C-13AD-45F9-8E1C-DFE3E1C7EC50';
        DECLARE @ParamID_2732 nvarchar(32)= SCOPE_IDENTITY();

        INSERT INTO [list].[LT_ItemListDataParameter] ([LanguageID], [ItemListDataParameterID], [Name], [Description])
        SELECT l.[LanguageID], @ParamID_2732 AS [ItemListDataParameterID], N'ViewName' AS [Name], N'' AS [Description]
        FROM [dbo].[Language] l
        WHERE l.[LanguageID] IN (1,2,7);
    END;

    IF NOT EXISTS (SELECT 1 FROM [list].[ItemListDataParameter] [dp] WHERE [dp].[MasterID] = '8DD94414-109D-41E2-8803-F65D4B067486')
    BEGIN
        INSERT INTO [list].[ItemListDataParameter] ([ItemListDataSourceID], [DataType], [Type], [Mandatory], [DefaultValue], [ConstructionPattern], [Created], [MasterID])
        SELECT [ds].[ItemListDataSourceID] AS [ItemListDataSourceID], N'String' AS [DataType], N'PortalPartParam' AS [Type], N'0' AS [Mandatory], N'' AS [DefaultValue], N'' AS [ConstructionPattern], GETDATE() AS [Created], N'8DD94414-109D-41E2-8803-F65D4B067486' AS [MasterID]
        FROM [list].[ItemListDataSource] [ds]
        WHERE [ds].[MasterID] = '3120964C-13AD-45F9-8E1C-DFE3E1C7EC50';
        DECLARE @ParamID_2733 nvarchar(32)= SCOPE_IDENTITY();

        INSERT INTO [list].[LT_ItemListDataParameter] ([LanguageID], [ItemListDataParameterID], [Name], [Description])
        SELECT l.[LanguageID], @ParamID_2733 AS [ItemListDataParameterID], N'AbsenceQuestionIds' AS [Name], N'' AS [Description]
        FROM [dbo].[Language] l
        WHERE l.[LanguageID] IN (1,2,7);
    END;

    IF NOT EXISTS (SELECT 1 FROM [list].[ItemListDataParameter] [dp] WHERE [dp].[MasterID] = '8BABF3C5-AF9D-437B-8D05-283869C9851B')
    BEGIN
        INSERT INTO [list].[ItemListDataParameter] ([ItemListDataSourceID], [DataType], [Type], [Mandatory], [DefaultValue], [ConstructionPattern], [Created], [MasterID])
        SELECT [ds].[ItemListDataSourceID] AS [ItemListDataSourceID], N'String' AS [DataType], N'PortalPartParam' AS [Type], N'0' AS [Mandatory], N'' AS [DefaultValue], N'' AS [ConstructionPattern], GETDATE() AS [Created], N'8BABF3C5-AF9D-437B-8D05-283869C9851B' AS [MasterID]
        FROM [list].[ItemListDataSource] [ds]
        WHERE [ds].[MasterID] = '3120964C-13AD-45F9-8E1C-DFE3E1C7EC50';
        DECLARE @ParamID_2734 nvarchar(32)= SCOPE_IDENTITY();

        INSERT INTO [list].[LT_ItemListDataParameter] ([LanguageID], [ItemListDataParameterID], [Name], [Description])
        SELECT l.[LanguageID], @ParamID_2734 AS [ItemListDataParameterID], N'RepeatQuestionIds' AS [Name], N'' AS [Description]
        FROM [dbo].[Language] l
        WHERE l.[LanguageID] IN (1,2,7);
    END;

    IF NOT EXISTS (SELECT 1 FROM [list].[ItemListDataParameter] [dp] WHERE [dp].[MasterID] = '4A890685-C757-47EC-899E-677791D25FAC')
    BEGIN
        INSERT INTO [list].[ItemListDataParameter] ([ItemListDataSourceID], [DataType], [Type], [Mandatory], [DefaultValue], [ConstructionPattern], [Created], [MasterID])
        SELECT [ds].[ItemListDataSourceID] AS [ItemListDataSourceID], N'Integer' AS [DataType], N'PortalPartParam' AS [Type], N'0' AS [Mandatory], N'' AS [DefaultValue], N'' AS [ConstructionPattern], GETDATE() AS [Created], N'4A890685-C757-47EC-899E-677791D25FAC' AS [MasterID]
        FROM [list].[ItemListDataSource] [ds]
        WHERE [ds].[MasterID] = '3120964C-13AD-45F9-8E1C-DFE3E1C7EC50';
        DECLARE @ParamID_2737 nvarchar(32)= SCOPE_IDENTITY();

        INSERT INTO [list].[LT_ItemListDataParameter] ([LanguageID], [ItemListDataParameterID], [Name], [Description])
        SELECT l.[LanguageID], @ParamID_2737 AS [ItemListDataParameterID], N'batchid' AS [Name], N'' AS [Description]
        FROM [dbo].[Language] l
        WHERE l.[LanguageID] IN (1,2,7);
    END;

    IF NOT EXISTS (SELECT 1 FROM [list].[ItemListDataParameter] [dp] WHERE [dp].[MasterID] = '9D58702C-D43F-4B25-9D46-18AB61BF631F')
    BEGIN
        INSERT INTO [list].[ItemListDataParameter] ([ItemListDataSourceID], [DataType], [Type], [Mandatory], [DefaultValue], [ConstructionPattern], [Created], [MasterID])
        SELECT [ds].[ItemListDataSourceID] AS [ItemListDataSourceID], N'Integer' AS [DataType], N'PortalPartParam' AS [Type], N'0' AS [Mandatory], N'' AS [DefaultValue], N'' AS [ConstructionPattern], GETDATE() AS [Created], N'9D58702C-D43F-4B25-9D46-18AB61BF631F' AS [MasterID]
        FROM [list].[ItemListDataSource] [ds]
        WHERE [ds].[MasterID] = '3120964C-13AD-45F9-8E1C-DFE3E1C7EC50';
        DECLARE @ParamID_2738 nvarchar(32)= SCOPE_IDENTITY();

        INSERT INTO [list].[LT_ItemListDataParameter] ([LanguageID], [ItemListDataParameterID], [Name], [Description])
        SELECT l.[LanguageID], @ParamID_2738 AS [ItemListDataParameterID], N'activityid' AS [Name], N'' AS [Description]
        FROM [dbo].[Language] l
        WHERE l.[LanguageID] IN (1,2,7);
    END;

    IF NOT EXISTS (SELECT 1 FROM [list].[ItemListDataParameter] [dp] WHERE [dp].[MasterID] = 'C22A6011-21F6-4D80-9497-6B41F9A48E9C')
    BEGIN
        INSERT INTO [list].[ItemListDataParameter] ([ItemListDataSourceID], [DataType], [Type], [Mandatory], [DefaultValue], [ConstructionPattern], [Created], [MasterID])
        SELECT [ds].[ItemListDataSourceID] AS [ItemListDataSourceID], N'Integer' AS [DataType], N'PortalPartParam' AS [Type], N'0' AS [Mandatory], N'' AS [DefaultValue], N'' AS [ConstructionPattern], GETDATE() AS [Created], N'C22A6011-21F6-4D80-9497-6B41F9A48E9C' AS [MasterID]
        FROM [list].[ItemListDataSource] [ds]
        WHERE [ds].[MasterID] = '3120964C-13AD-45F9-8E1C-DFE3E1C7EC50';
        DECLARE @ParamID_2739 nvarchar(32)= SCOPE_IDENTITY();

        INSERT INTO [list].[LT_ItemListDataParameter] ([LanguageID], [ItemListDataParameterID], [Name], [Description])
        SELECT l.[LanguageID], @ParamID_2739 AS [ItemListDataParameterID], N'userid' AS [Name], N'' AS [Description]
        FROM [dbo].[Language] l
        WHERE l.[LanguageID] IN (1,2,7);
    END;

    IF NOT EXISTS (SELECT 1 FROM [list].[ItemListDataParameter] [dp] WHERE [dp].[MasterID] = '8F825F2B-0AFF-4AA1-B5CD-156701DFBF2C')
    BEGIN
        INSERT INTO [list].[ItemListDataParameter] ([ItemListDataSourceID], [DataType], [Type], [Mandatory], [DefaultValue], [ConstructionPattern], [Created], [MasterID])
        SELECT [ds].[ItemListDataSourceID] AS [ItemListDataSourceID], N'Integer' AS [DataType], N'PortalPartParam' AS [Type], N'0' AS [Mandatory], N'' AS [DefaultValue], N'' AS [ConstructionPattern], GETDATE() AS [Created], N'8F825F2B-0AFF-4AA1-B5CD-156701DFBF2C' AS [MasterID]
        FROM [list].[ItemListDataSource] [ds]
        WHERE [ds].[MasterID] = '3120964C-13AD-45F9-8E1C-DFE3E1C7EC50';
        DECLARE @ParamID_2740 nvarchar(32)= SCOPE_IDENTITY();

        INSERT INTO [list].[LT_ItemListDataParameter] ([LanguageID], [ItemListDataParameterID], [Name], [Description])
        SELECT l.[LanguageID], @ParamID_2740 AS [ItemListDataParameterID], N'resultid' AS [Name], N'' AS [Description]
        FROM [dbo].[Language] l
        WHERE l.[LanguageID] IN (1,2,7);
    END;

    IF NOT EXISTS (SELECT 1 FROM [list].[ItemListDataParameter] [dp] WHERE [dp].[MasterID] = 'E0271E23-8B8E-49F2-B855-17EEDF063FFB')
    BEGIN
        INSERT INTO [list].[ItemListDataParameter] ([ItemListDataSourceID], [DataType], [Type], [Mandatory], [DefaultValue], [ConstructionPattern], [Created], [MasterID])
        SELECT [ds].[ItemListDataSourceID] AS [ItemListDataSourceID], N'String' AS [DataType], N'PortalPartParam' AS [Type], N'0' AS [Mandatory], N'' AS [DefaultValue], N'' AS [ConstructionPattern], GETDATE() AS [Created], N'E0271E23-8B8E-49F2-B855-17EEDF063FFB' AS [MasterID]
        FROM [list].[ItemListDataSource] [ds]
        WHERE [ds].[MasterID] = '3120964C-13AD-45F9-8E1C-DFE3E1C7EC50';
        DECLARE @ParamID_2750 nvarchar(32)= SCOPE_IDENTITY();

        INSERT INTO [list].[LT_ItemListDataParameter] ([LanguageID], [ItemListDataParameterID], [Name], [Description])
        SELECT l.[LanguageID], @ParamID_2750 AS [ItemListDataParameterID], N'RankSummaryQuestionIds' AS [Name], N'' AS [Description]
        FROM [dbo].[Language] l
        WHERE l.[LanguageID] IN (1,2,7);
    END;

    IF NOT EXISTS (SELECT 1 FROM [list].[ItemListDataParameter] [dp] WHERE [dp].[MasterID] = 'FB0CECB6-1A85-496D-8E32-4DF23623F68E')
    BEGIN
        INSERT INTO [list].[ItemListDataParameter] ([ItemListDataSourceID], [DataType], [Type], [Mandatory], [DefaultValue], [ConstructionPattern], [Created], [MasterID])
        SELECT [ds].[ItemListDataSourceID] AS [ItemListDataSourceID], N'Boolean' AS [DataType], N'PortalPartParam' AS [Type], N'0' AS [Mandatory], N'' AS [DefaultValue], N'' AS [ConstructionPattern], GETDATE() AS [Created], N'FB0CECB6-1A85-496D-8E32-4DF23623F68E' AS [MasterID]
        FROM [list].[ItemListDataSource] [ds]
        WHERE [ds].[MasterID] = '3120964C-13AD-45F9-8E1C-DFE3E1C7EC50';
        DECLARE @ParamID_2751 nvarchar(32)= SCOPE_IDENTITY();

        INSERT INTO [list].[LT_ItemListDataParameter] ([LanguageID], [ItemListDataParameterID], [Name], [Description])
        SELECT l.[LanguageID], @ParamID_2751 AS [ItemListDataParameterID], N'isExport' AS [Name], N'' AS [Description]
        FROM [dbo].[Language] l
        WHERE l.[LanguageID] IN (1,2,7);
    END;
END;
--#endregion DataSource 2151

--#region DataSource 2152
IF NOT EXISTS (SELECT 1 FROM [list].[ItemListDataSource] [ds] WHERE [ds].[MasterID] = '25E1D4AF-622A-4B11-AC20-4A2452DBD696')
BEGIN
    INSERT INTO [list].[ItemListDataSource] ([OwnerID], [FunctionName], [Created], [MasterID])
    SELECT [o].[OwnerID] AS [OwnerID], N'getvgoviewtypedetail' AS [FunctionName], GETDATE() AS [Created], N'25E1D4AF-622A-4B11-AC20-4A2452DBD696' AS [MasterID]
    FROM [org].[Owner] [o]
    WHERE [o].[MasterID] = '1C483E02-E808-42A7-8210-B11F4051D2E4';
    DECLARE @ItemListDataSourceID_2152 nvarchar(32)= SCOPE_IDENTITY();

    INSERT INTO [list].[LT_ItemListDataSource] ([LanguageID], [ItemListDataSourceID], [Name], [Description])
    SELECT l.[LanguageID], @ItemListDataSourceID_2152 AS [ItemListDataSourceID], N'VGO Viewtype Detail' AS [Name], N'' AS [Description]
    FROM [dbo].[Language] l
    WHERE l.[LanguageID] IN (1,2,7);

    IF NOT EXISTS (SELECT 1 FROM [list].[ItemListDataParameter] [dp] WHERE [dp].[MasterID] = '4D37B3F8-56DB-420C-A0A9-5E31C6829179')
    BEGIN
        INSERT INTO [list].[ItemListDataParameter] ([ItemListDataSourceID], [DataType], [Type], [Mandatory], [DefaultValue], [ConstructionPattern], [Created], [MasterID])
        SELECT [ds].[ItemListDataSourceID] AS [ItemListDataSourceID], N'String' AS [DataType], N'PortalPartParam' AS [Type], N'0' AS [Mandatory], N'' AS [DefaultValue], N'' AS [ConstructionPattern], GETDATE() AS [Created], N'4D37B3F8-56DB-420C-A0A9-5E31C6829179' AS [MasterID]
        FROM [list].[ItemListDataSource] [ds]
        WHERE [ds].[MasterID] = '25E1D4AF-622A-4B11-AC20-4A2452DBD696';
        DECLARE @ParamID_2741 nvarchar(32)= SCOPE_IDENTITY();

        INSERT INTO [list].[LT_ItemListDataParameter] ([LanguageID], [ItemListDataParameterID], [Name], [Description])
        SELECT l.[LanguageID], @ParamID_2741 AS [ItemListDataParameterID], N'GeneralQuestionIds' AS [Name], N'' AS [Description]
        FROM [dbo].[Language] l
        WHERE l.[LanguageID] IN (1,2,7);
    END;

    IF NOT EXISTS (SELECT 1 FROM [list].[ItemListDataParameter] [dp] WHERE [dp].[MasterID] = '0633C4E3-EAF5-459E-A65C-CAC98052BF06')
    BEGIN
        INSERT INTO [list].[ItemListDataParameter] ([ItemListDataSourceID], [DataType], [Type], [Mandatory], [DefaultValue], [ConstructionPattern], [Created], [MasterID])
        SELECT [ds].[ItemListDataSourceID] AS [ItemListDataSourceID], N'String' AS [DataType], N'PortalPartParam' AS [Type], N'0' AS [Mandatory], N'' AS [DefaultValue], N'' AS [ConstructionPattern], GETDATE() AS [Created], N'0633C4E3-EAF5-459E-A65C-CAC98052BF06' AS [MasterID]
        FROM [list].[ItemListDataSource] [ds]
        WHERE [ds].[MasterID] = '25E1D4AF-622A-4B11-AC20-4A2452DBD696';
        DECLARE @ParamID_2742 nvarchar(32)= SCOPE_IDENTITY();

        INSERT INTO [list].[LT_ItemListDataParameter] ([LanguageID], [ItemListDataParameterID], [Name], [Description])
        SELECT l.[LanguageID], @ParamID_2742 AS [ItemListDataParameterID], N'MarkQuestionIds' AS [Name], N'' AS [Description]
        FROM [dbo].[Language] l
        WHERE l.[LanguageID] IN (1,2,7);
    END;

    IF NOT EXISTS (SELECT 1 FROM [list].[ItemListDataParameter] [dp] WHERE [dp].[MasterID] = 'F14C3119-C175-4FDE-B826-2E2B0C4EBE78')
    BEGIN
        INSERT INTO [list].[ItemListDataParameter] ([ItemListDataSourceID], [DataType], [Type], [Mandatory], [DefaultValue], [ConstructionPattern], [Created], [MasterID])
        SELECT [ds].[ItemListDataSourceID] AS [ItemListDataSourceID], N'Integer' AS [DataType], N'PortalPartParam' AS [Type], N'0' AS [Mandatory], N'' AS [DefaultValue], N'' AS [ConstructionPattern], GETDATE() AS [Created], N'F14C3119-C175-4FDE-B826-2E2B0C4EBE78' AS [MasterID]
        FROM [list].[ItemListDataSource] [ds]
        WHERE [ds].[MasterID] = '25E1D4AF-622A-4B11-AC20-4A2452DBD696';
        DECLARE @ParamID_2743 nvarchar(32)= SCOPE_IDENTITY();

        INSERT INTO [list].[LT_ItemListDataParameter] ([LanguageID], [ItemListDataParameterID], [Name], [Description])
        SELECT l.[LanguageID], @ParamID_2743 AS [ItemListDataParameterID], N'activityid' AS [Name], N'' AS [Description]
        FROM [dbo].[Language] l
        WHERE l.[LanguageID] IN (1,2,7);
    END;

    IF NOT EXISTS (SELECT 1 FROM [list].[ItemListDataParameter] [dp] WHERE [dp].[MasterID] = '9F765C7D-ADC1-4653-BAC9-072480565217')
    BEGIN
        INSERT INTO [list].[ItemListDataParameter] ([ItemListDataSourceID], [DataType], [Type], [Mandatory], [DefaultValue], [ConstructionPattern], [Created], [MasterID])
        SELECT [ds].[ItemListDataSourceID] AS [ItemListDataSourceID], N'Integer' AS [DataType], N'PortalPartParam' AS [Type], N'0' AS [Mandatory], N'' AS [DefaultValue], N'' AS [ConstructionPattern], GETDATE() AS [Created], N'9F765C7D-ADC1-4653-BAC9-072480565217' AS [MasterID]
        FROM [list].[ItemListDataSource] [ds]
        WHERE [ds].[MasterID] = '25E1D4AF-622A-4B11-AC20-4A2452DBD696';
        DECLARE @ParamID_2744 nvarchar(32)= SCOPE_IDENTITY();

        INSERT INTO [list].[LT_ItemListDataParameter] ([LanguageID], [ItemListDataParameterID], [Name], [Description])
        SELECT l.[LanguageID], @ParamID_2744 AS [ItemListDataParameterID], N'batchid' AS [Name], N'' AS [Description]
        FROM [dbo].[Language] l
        WHERE l.[LanguageID] IN (1,2,7);
    END;

    IF NOT EXISTS (SELECT 1 FROM [list].[ItemListDataParameter] [dp] WHERE [dp].[MasterID] = '2D3EADA9-DEDA-460D-A811-2DF3DBCEB1CC')
    BEGIN
        INSERT INTO [list].[ItemListDataParameter] ([ItemListDataSourceID], [DataType], [Type], [Mandatory], [DefaultValue], [ConstructionPattern], [Created], [MasterID])
        SELECT [ds].[ItemListDataSourceID] AS [ItemListDataSourceID], N'Integer' AS [DataType], N'PortalPartParam' AS [Type], N'0' AS [Mandatory], N'' AS [DefaultValue], N'' AS [ConstructionPattern], GETDATE() AS [Created], N'2D3EADA9-DEDA-460D-A811-2DF3DBCEB1CC' AS [MasterID]
        FROM [list].[ItemListDataSource] [ds]
        WHERE [ds].[MasterID] = '25E1D4AF-622A-4B11-AC20-4A2452DBD696';
        DECLARE @ParamID_2745 nvarchar(32)= SCOPE_IDENTITY();

        INSERT INTO [list].[LT_ItemListDataParameter] ([LanguageID], [ItemListDataParameterID], [Name], [Description])
        SELECT l.[LanguageID], @ParamID_2745 AS [ItemListDataParameterID], N'userid' AS [Name], N'' AS [Description]
        FROM [dbo].[Language] l
        WHERE l.[LanguageID] IN (1,2,7);
    END;

    IF NOT EXISTS (SELECT 1 FROM [list].[ItemListDataParameter] [dp] WHERE [dp].[MasterID] = '51EF35F9-4425-46E8-B8BC-9C2DB38DC0FC')
    BEGIN
        INSERT INTO [list].[ItemListDataParameter] ([ItemListDataSourceID], [DataType], [Type], [Mandatory], [DefaultValue], [ConstructionPattern], [Created], [MasterID])
        SELECT [ds].[ItemListDataSourceID] AS [ItemListDataSourceID], N'Integer' AS [DataType], N'PortalPartParam' AS [Type], N'0' AS [Mandatory], N'' AS [DefaultValue], N'' AS [ConstructionPattern], GETDATE() AS [Created], N'51EF35F9-4425-46E8-B8BC-9C2DB38DC0FC' AS [MasterID]
        FROM [list].[ItemListDataSource] [ds]
        WHERE [ds].[MasterID] = '25E1D4AF-622A-4B11-AC20-4A2452DBD696';
        DECLARE @ParamID_2746 nvarchar(32)= SCOPE_IDENTITY();

        INSERT INTO [list].[LT_ItemListDataParameter] ([LanguageID], [ItemListDataParameterID], [Name], [Description])
        SELECT l.[LanguageID], @ParamID_2746 AS [ItemListDataParameterID], N'resultid' AS [Name], N'' AS [Description]
        FROM [dbo].[Language] l
        WHERE l.[LanguageID] IN (1,2,7);
    END;

    IF NOT EXISTS (SELECT 1 FROM [list].[ItemListDataParameter] [dp] WHERE [dp].[MasterID] = '16F4E6C9-2E72-417D-8312-8E90CEEE6A02')
    BEGIN
        INSERT INTO [list].[ItemListDataParameter] ([ItemListDataSourceID], [DataType], [Type], [Mandatory], [DefaultValue], [ConstructionPattern], [Created], [MasterID])
        SELECT [ds].[ItemListDataSourceID] AS [ItemListDataSourceID], N'String' AS [DataType], N'PortalPartParam' AS [Type], N'0' AS [Mandatory], N''+'GetVGOViewtypeSummary'+'' AS [DefaultValue], N'' AS [ConstructionPattern], GETDATE() AS [Created], N'16F4E6C9-2E72-417D-8312-8E90CEEE6A02' AS [MasterID]
        FROM [list].[ItemListDataSource] [ds]
        WHERE [ds].[MasterID] = '25E1D4AF-622A-4B11-AC20-4A2452DBD696';
        DECLARE @ParamID_2747 nvarchar(32)= SCOPE_IDENTITY();

        INSERT INTO [list].[LT_ItemListDataParameter] ([LanguageID], [ItemListDataParameterID], [Name], [Description])
        SELECT l.[LanguageID], @ParamID_2747 AS [ItemListDataParameterID], N'ViewName' AS [Name], N'' AS [Description]
        FROM [dbo].[Language] l
        WHERE l.[LanguageID] IN (1,2,7);
    END;

    IF NOT EXISTS (SELECT 1 FROM [list].[ItemListDataParameter] [dp] WHERE [dp].[MasterID] = '47CDEF37-9410-4F3B-A2BC-3F2CA9E76DB7')
    BEGIN
        INSERT INTO [list].[ItemListDataParameter] ([ItemListDataSourceID], [DataType], [Type], [Mandatory], [DefaultValue], [ConstructionPattern], [Created], [MasterID])
        SELECT [ds].[ItemListDataSourceID] AS [ItemListDataSourceID], N'String' AS [DataType], N'PortalPartParam' AS [Type], N'0' AS [Mandatory], N'' AS [DefaultValue], N'' AS [ConstructionPattern], GETDATE() AS [Created], N'47CDEF37-9410-4F3B-A2BC-3F2CA9E76DB7' AS [MasterID]
        FROM [list].[ItemListDataSource] [ds]
        WHERE [ds].[MasterID] = '25E1D4AF-622A-4B11-AC20-4A2452DBD696';
        DECLARE @ParamID_2748 nvarchar(32)= SCOPE_IDENTITY();

        INSERT INTO [list].[LT_ItemListDataParameter] ([LanguageID], [ItemListDataParameterID], [Name], [Description])
        SELECT l.[LanguageID], @ParamID_2748 AS [ItemListDataParameterID], N'AbsenceQuestionIds' AS [Name], N'' AS [Description]
        FROM [dbo].[Language] l
        WHERE l.[LanguageID] IN (1,2,7);
    END;

    IF NOT EXISTS (SELECT 1 FROM [list].[ItemListDataParameter] [dp] WHERE [dp].[MasterID] = '64F058F2-621B-48DF-A283-D67820B2D35E')
    BEGIN
        INSERT INTO [list].[ItemListDataParameter] ([ItemListDataSourceID], [DataType], [Type], [Mandatory], [DefaultValue], [ConstructionPattern], [Created], [MasterID])
        SELECT [ds].[ItemListDataSourceID] AS [ItemListDataSourceID], N'String' AS [DataType], N'PortalPartParam' AS [Type], N'0' AS [Mandatory], N'' AS [DefaultValue], N'' AS [ConstructionPattern], GETDATE() AS [Created], N'64F058F2-621B-48DF-A283-D67820B2D35E' AS [MasterID]
        FROM [list].[ItemListDataSource] [ds]
        WHERE [ds].[MasterID] = '25E1D4AF-622A-4B11-AC20-4A2452DBD696';
        DECLARE @ParamID_2749 nvarchar(32)= SCOPE_IDENTITY();

        INSERT INTO [list].[LT_ItemListDataParameter] ([LanguageID], [ItemListDataParameterID], [Name], [Description])
        SELECT l.[LanguageID], @ParamID_2749 AS [ItemListDataParameterID], N'RankSummaryQuestionIds' AS [Name], N'' AS [Description]
        FROM [dbo].[Language] l
        WHERE l.[LanguageID] IN (1,2,7);
    END;

    IF NOT EXISTS (SELECT 1 FROM [list].[ItemListDataParameter] [dp] WHERE [dp].[MasterID] = '84165336-1B5A-4C4D-A819-1A778C44E264')
    BEGIN
        INSERT INTO [list].[ItemListDataParameter] ([ItemListDataSourceID], [DataType], [Type], [Mandatory], [DefaultValue], [ConstructionPattern], [Created], [MasterID])
        SELECT [ds].[ItemListDataSourceID] AS [ItemListDataSourceID], N'Boolean' AS [DataType], N'PortalPartParam' AS [Type], N'0' AS [Mandatory], N'' AS [DefaultValue], N'' AS [ConstructionPattern], GETDATE() AS [Created], N'84165336-1B5A-4C4D-A819-1A778C44E264' AS [MasterID]
        FROM [list].[ItemListDataSource] [ds]
        WHERE [ds].[MasterID] = '25E1D4AF-622A-4B11-AC20-4A2452DBD696';
        DECLARE @ParamID_2752 nvarchar(32)= SCOPE_IDENTITY();

        INSERT INTO [list].[LT_ItemListDataParameter] ([LanguageID], [ItemListDataParameterID], [Name], [Description])
        SELECT l.[LanguageID], @ParamID_2752 AS [ItemListDataParameterID], N'isExport' AS [Name], N'' AS [Description]
        FROM [dbo].[Language] l
        WHERE l.[LanguageID] IN (1,2,7);
    END;

    IF NOT EXISTS (SELECT 1 FROM [list].[ItemListDataParameter] [dp] WHERE [dp].[MasterID] = 'E90ACBCD-2A33-4A80-AEEE-6C2146CDC1F5')
    BEGIN
        INSERT INTO [list].[ItemListDataParameter] ([ItemListDataSourceID], [DataType], [Type], [Mandatory], [DefaultValue], [ConstructionPattern], [Created], [MasterID])
        SELECT [ds].[ItemListDataSourceID] AS [ItemListDataSourceID], N'String' AS [DataType], N'PortalPartParam' AS [Type], N'0' AS [Mandatory], N'' AS [DefaultValue], N'' AS [ConstructionPattern], GETDATE() AS [Created], N'E90ACBCD-2A33-4A80-AEEE-6C2146CDC1F5' AS [MasterID]
        FROM [list].[ItemListDataSource] [ds]
        WHERE [ds].[MasterID] = '25E1D4AF-622A-4B11-AC20-4A2452DBD696';
        DECLARE @ParamID_2753 nvarchar(32)= SCOPE_IDENTITY();

        INSERT INTO [list].[LT_ItemListDataParameter] ([LanguageID], [ItemListDataParameterID], [Name], [Description])
        SELECT l.[LanguageID], @ParamID_2753 AS [ItemListDataParameterID], N'RankSummaryAlternativeIds' AS [Name], N'' AS [Description]
        FROM [dbo].[Language] l
        WHERE l.[LanguageID] IN (1,2,7);
    END;
END;
--#endregion DataSource 2152

--#region PortalPage 3887
IF NOT EXISTS (SELECT 1 FROM [app].[PortalPage] [pp] WHERE [pp].[MasterID] = '2A7684DB-A110-4111-954F-42EA70B8B0BC')
BEGIN
    INSERT INTO [app].[PortalPage] ([OwnerID], [No], [Parameter], [ExtID], [Created], [MasterID])
    SELECT [o].[OwnerID] AS [OwnerID], N'127' AS [No], N'' AS [Parameter], N'' AS [ExtID], GETDATE() AS [Created], N'2A7684DB-A110-4111-954F-42EA70B8B0BC' AS [MasterID]
    FROM [org].[Owner] [o]
    WHERE [o].[MasterID] = '1C483E02-E808-42A7-8210-B11F4051D2E4';
    DECLARE @PortalPageID_3887 nvarchar(32)= SCOPE_IDENTITY();

    INSERT INTO [app].[LT_PortalPage] ([LanguageID], [PortalPageID], [Name], [Description])
    SELECT l.[LanguageID], @PortalPageID_3887 AS [PortalPageID], N'VGO ViewType Summary' AS [Name], N'' AS [Description]
    FROM [dbo].[Language] l
    WHERE l.[LanguageID] IN (1,2,7);

    DECLARE @DataSourceID_2151_10482500211132 nvarchar(32)= '';
    SELECT @DataSourceID_2151_10482500211132 = [ds].[ItemListDataSourceID]
    FROM [list].[ItemListDataSource] [ds]
    WHERE [ds].[MasterID] = '3120964C-13AD-45F9-8E1C-DFE3E1C7EC50';

    DECLARE @QuestionIds_10482500211133 nvarchar(32)= '';
    SELECT @QuestionIds_10482500211133 = STUFF( (SELECT ','+CAST([q].[QuestionID] AS nvarchar(32))
                                                 FROM [at].[Question] [q]
                                                 WHERE [q].[MasterID] IN ('D6AD7642-8F3B-49F4-977B-0B124B7563A1', 'F9508ECD-F240-4408-A32E-74EFB8A411E9')
                                                 ORDER BY CHARINDEX(CAST([q].[MasterID] AS nvarchar(64)), N'D6AD7642-8F3B-49F4-977B-0B124B7563A1,F9508ECD-F240-4408-A32E-74EFB8A411E9')
                                                 FOR XML PATH, TYPE
                                                ) .value ('.[1]', 'nvarchar(max)'), 1, 1, '');
    DECLARE @QuestionIds_10482500211135 nvarchar(32)= '';
    SELECT @QuestionIds_10482500211135 = STUFF( (SELECT ','+CAST([q].[QuestionID] AS nvarchar(32))
                                                 FROM [at].[Question] [q]
                                                 WHERE [q].[MasterID] IN ('C4CF1481-18F8-4447-BB8E-0FC02E18C9B1', 'DF914DD2-941F-45A0-924A-DCFB67281FC3', '7160E01A-6E37-4BBA-931B-5C782416A515', 'CF4E288E-16E9-4B64-BBCE-6CC13E7C3CD4')
                                                 ORDER BY CHARINDEX(CAST([q].[MasterID] AS nvarchar(64)), N'C4CF1481-18F8-4447-BB8E-0FC02E18C9B1,DF914DD2-941F-45A0-924A-DCFB67281FC3,7160E01A-6E37-4BBA-931B-5C782416A515,CF4E288E-16E9-4B64-BBCE-6CC13E7C3CD4')
                                                 FOR XML PATH, TYPE
                                                ) .value ('.[1]', 'nvarchar(max)'), 1, 1, '');
    DECLARE @QuestionIds_10482500211136 nvarchar(32)= '';
    SELECT @QuestionIds_10482500211136 = STUFF( (SELECT ','+CAST([q].[QuestionID] AS nvarchar(32))
                                                 FROM [at].[Question] [q]
                                                 WHERE [q].[MasterID] IN ('095EF475-30FF-4009-9376-8C3CCE33E372', '641DE5C9-15C9-4ECA-862F-7BC28C9EA763')
                                                 ORDER BY CHARINDEX(CAST([q].[MasterID] AS nvarchar(64)), N'095EF475-30FF-4009-9376-8C3CCE33E372,641DE5C9-15C9-4ECA-862F-7BC28C9EA763')
                                                 FOR XML PATH, TYPE
                                                ) .value ('.[1]', 'nvarchar(max)'), 1, 1, '');
    IF NOT EXISTS (SELECT 1 FROM [app].[PortalPart] [pt] WHERE [pt].[MasterID] = '91D8092A-3963-414B-A190-711474AB3DCF')
    BEGIN
        INSERT INTO [app].[PortalPart] ([PortalPageID], [PortalPartTypeID], [No], [CategoryID], [Settings], [BubbleID], [ReportPartID], [ReportID], [CssClass], [InlineStyle], [ActivityID], [SurveyID], [ExtID], [MasterID])
        SELECT [pp].[PortalPageID] AS [PortalPageID], N'43' AS [PortalPartTypeID], N'1' AS [No], NULL AS [CategoryID], N'<params>isemptyhtml%3D'+'true'+'%26DataSourceID%3D'+@DataSourceID_2151_10482500211132+'%26AbsenceQuestionIds%3D'+@QuestionIds_10482500211133+'%26SummaryQuestionIds%3D'+'0'+'%26RepeatQuestionIds%3D'+@QuestionIds_10482500211135+'%26RankSummaryQuestionIds%3D'+@QuestionIds_10482500211136+'</params>' AS [Settings], NULL AS [BubbleID], NULL AS [ReportPartID], NULL AS [ReportID], N'' AS [CssClass], N'' AS [InlineStyle], NULL AS [ActivityID], NULL AS [SurveyID], N'' AS [ExtID], N'91D8092A-3963-414B-A190-711474AB3DCF' AS [MasterID]
        FROM [app].[PortalPage] [pp]
        WHERE [pp].[MasterID] = '2A7684DB-A110-4111-954F-42EA70B8B0BC';
        DECLARE @PortalPartID_5615 nvarchar(32)= SCOPE_IDENTITY();

        INSERT INTO [app].[LT_PortalPart] ([LanguageID], [PortalPartID], [Name], [Description], [Text], [TextAbove], [TextBelow], [HeaderText], [FooterText], [BeforeContentText], [AfterContentText])
        SELECT l.[LanguageID], @PortalPartID_5615 AS [PortalPartID], N'Summary ' AS [Name], N'' AS [Description], N'' AS [Text], N'' AS [TextAbove], N'' AS [TextBelow], N'' AS [HeaderText], N'' AS [FooterText], N'' AS [BeforeContentText], N'' AS [AfterContentText]
        FROM [dbo].[Language] l
        WHERE l.[LanguageID] IN (1,2,7);
    END;
END;
--#endregion PortalPage 3887

--#region PortalPage 3888
IF NOT EXISTS (SELECT 1 FROM [app].[PortalPage] [pp] WHERE [pp].[MasterID] = '467CFFB5-434B-477A-8EA8-C7DD8797EFB0')
BEGIN
    INSERT INTO [app].[PortalPage] ([OwnerID], [No], [Parameter], [ExtID], [Created], [MasterID])
    SELECT [o].[OwnerID] AS [OwnerID], N'128' AS [No], N'' AS [Parameter], N'' AS [ExtID], GETDATE() AS [Created], N'467CFFB5-434B-477A-8EA8-C7DD8797EFB0' AS [MasterID]
    FROM [org].[Owner] [o]
    WHERE [o].[MasterID] = '1C483E02-E808-42A7-8210-B11F4051D2E4';
    DECLARE @PortalPageID_3888 nvarchar(32)= SCOPE_IDENTITY();

    INSERT INTO [app].[LT_PortalPage] ([LanguageID], [PortalPageID], [Name], [Description])
    SELECT l.[LanguageID], @PortalPageID_3888 AS [PortalPageID], N'VGO ViewType Detail' AS [Name], N'' AS [Description]
    FROM [dbo].[Language] l
    WHERE l.[LanguageID] IN (1,2,7);

    DECLARE @DataSourceID_2152_10534964353782 nvarchar(32)= '';
    SELECT @DataSourceID_2152_10534964353782 = [ds].[ItemListDataSourceID]
    FROM [list].[ItemListDataSource] [ds]
    WHERE [ds].[MasterID] = '25E1D4AF-622A-4B11-AC20-4A2452DBD696';
    DECLARE @QuestionIds_10534964353783 nvarchar(32)= '';
    SELECT @QuestionIds_10534964353783 = STUFF( (SELECT ','+CAST([q].[QuestionID] AS nvarchar(32))
                                                 FROM [at].[Question] [q]
                                                 WHERE [q].[MasterID] IN ('E7BA7871-5A4C-487E-85D2-4386655EC00F', '8A5890C7-8E79-4B68-9330-CBF8BBA8BD1E')
                                                 ORDER BY CHARINDEX(CAST([q].[MasterID] AS nvarchar(64)), N'E7BA7871-5A4C-487E-85D2-4386655EC00F,8A5890C7-8E79-4B68-9330-CBF8BBA8BD1E')
                                                 FOR XML PATH, TYPE
                                                ) .value ('.[1]', 'nvarchar(max)'), 1, 1, '');
    DECLARE @QuestionIds_10534964353784 nvarchar(32)= '';
    SELECT @QuestionIds_10534964353784 = STUFF( (SELECT ','+CAST([q].[QuestionID] AS nvarchar(32))
                                                 FROM [at].[Question] [q]
                                                 WHERE [q].[MasterID] IN ('C4CF1481-18F8-4447-BB8E-0FC02E18C9B1', 'DF914DD2-941F-45A0-924A-DCFB67281FC3', '7160E01A-6E37-4BBA-931B-5C782416A515', 'CF4E288E-16E9-4B64-BBCE-6CC13E7C3CD4')
                                                 ORDER BY CHARINDEX(CAST([q].[MasterID] AS nvarchar(64)), N'C4CF1481-18F8-4447-BB8E-0FC02E18C9B1,DF914DD2-941F-45A0-924A-DCFB67281FC3,7160E01A-6E37-4BBA-931B-5C782416A515,CF4E288E-16E9-4B64-BBCE-6CC13E7C3CD4')
                                                 FOR XML PATH, TYPE
                                                ) .value ('.[1]', 'nvarchar(max)'), 1, 1, '');
    DECLARE @QuestionIds_10534964353785 nvarchar(32)= '';
    SELECT @QuestionIds_10534964353785 = STUFF( (SELECT ','+CAST([q].[QuestionID] AS nvarchar(32))
                                                 FROM [at].[Question] [q]
                                                 WHERE [q].[MasterID] IN ('D6AD7642-8F3B-49F4-977B-0B124B7563A1', 'F9508ECD-F240-4408-A32E-74EFB8A411E9')
                                                 ORDER BY CHARINDEX(CAST([q].[MasterID] AS nvarchar(64)), N'D6AD7642-8F3B-49F4-977B-0B124B7563A1,F9508ECD-F240-4408-A32E-74EFB8A411E9')
                                                 FOR XML PATH, TYPE
                                                ) .value ('.[1]', 'nvarchar(max)'), 1, 1, '');
    DECLARE @QuestionIds_10534964353786 nvarchar(32)= '';
    SELECT @QuestionIds_10534964353786 = STUFF( (SELECT ','+CAST([q].[QuestionID] AS nvarchar(32))
                                                 FROM [at].[Question] [q]
                                                 WHERE [q].[MasterID] IN ('095EF475-30FF-4009-9376-8C3CCE33E372', '641DE5C9-15C9-4ECA-862F-7BC28C9EA763')
                                                 ORDER BY CHARINDEX(CAST([q].[MasterID] AS nvarchar(64)), N'095EF475-30FF-4009-9376-8C3CCE33E372,641DE5C9-15C9-4ECA-862F-7BC28C9EA763')
                                                 FOR XML PATH, TYPE
                                                ) .value ('.[1]', 'nvarchar(max)'), 1, 1, '');
    IF NOT EXISTS (SELECT 1 FROM [app].[PortalPart] [pt] WHERE [pt].[MasterID] = 'E87EFBDC-AB19-4289-9CD0-E0088AA5E5C8')
    BEGIN
        INSERT INTO [app].[PortalPart] ([PortalPageID], [PortalPartTypeID], [No], [CategoryID], [Settings], [BubbleID], [ReportPartID], [ReportID], [CssClass], [InlineStyle], [ActivityID], [SurveyID], [ExtID], [MasterID])
        SELECT [pp].[PortalPageID] AS [PortalPageID], N'43' AS [PortalPartTypeID], N'1' AS [No], NULL AS [CategoryID], N'<params>isemptyhtml%3D'+'true'+'%26DataSourceID%3D'+@DataSourceID_2152_10534964353782+'%26GeneralQuestionIds%3D'+@QuestionIds_10534964353783+'%26MarkQuestionIds%3D'+@QuestionIds_10534964353784+'%26AbsenceQuestionIds%3D'+@QuestionIds_10534964353785+'%26RankSummaryQuestionIds%3D'+@QuestionIds_10534964353786+'</params>' AS [Settings], NULL AS [BubbleID], NULL AS [ReportPartID], NULL AS [ReportID], N'' AS [CssClass], N'' AS [InlineStyle], NULL AS [ActivityID], NULL AS [SurveyID], N'' AS [ExtID], N'E87EFBDC-AB19-4289-9CD0-E0088AA5E5C8' AS [MasterID]
        FROM [app].[PortalPage] [pp]
        WHERE [pp].[MasterID] = '467CFFB5-434B-477A-8EA8-C7DD8797EFB0';
        DECLARE @PortalPartID_5616 nvarchar(32)= SCOPE_IDENTITY();

        INSERT INTO [app].[LT_PortalPart] ([LanguageID], [PortalPartID], [Name], [Description], [Text], [TextAbove], [TextBelow], [HeaderText], [FooterText], [BeforeContentText], [AfterContentText])
        SELECT l.[LanguageID], @PortalPartID_5616 AS [PortalPartID], N'Detail' AS [Name], N'' AS [Description], N'' AS [Text], N'' AS [TextAbove], N'' AS [TextBelow], N'' AS [HeaderText], N'' AS [FooterText], N'' AS [BeforeContentText], N'' AS [AfterContentText]
        FROM [dbo].[Language] l
        WHERE l.[LanguageID] IN (1,2,7);
    END;
END;
--#endregion PortalPage 3888

--#region PortalPage 3889
IF NOT EXISTS (SELECT 1 FROM [app].[PortalPage] [pp] WHERE [pp].[MasterID] = '8779CE5E-CF5E-48E9-A33F-13C25B115D06')
BEGIN
    INSERT INTO [app].[PortalPage] ([OwnerID], [No], [Parameter], [ExtID], [Created], [MasterID])
    SELECT [o].[OwnerID] AS [OwnerID], N'129' AS [No], N'' AS [Parameter], N'' AS [ExtID], GETDATE() AS [Created], N'8779CE5E-CF5E-48E9-A33F-13C25B115D06' AS [MasterID]
    FROM [org].[Owner] [o]
    WHERE [o].[MasterID] = '1C483E02-E808-42A7-8210-B11F4051D2E4';
    DECLARE @PortalPageID_3889 nvarchar(32)= SCOPE_IDENTITY();

    INSERT INTO [app].[LT_PortalPage] ([LanguageID], [PortalPageID], [Name], [Description])
    SELECT l.[LanguageID], @PortalPageID_3889 AS [PortalPageID], N'VGO ViewType MV1' AS [Name], N'' AS [Description]
    FROM [dbo].[Language] l
    WHERE l.[LanguageID] IN (1,2,7);

    DECLARE @DataSourceID_2152_11003614485752 nvarchar(32)= '';
    SELECT @DataSourceID_2152_11003614485752 = [ds].[ItemListDataSourceID]
    FROM [list].[ItemListDataSource] [ds]
    WHERE [ds].[MasterID] = '25E1D4AF-622A-4B11-AC20-4A2452DBD696';
    DECLARE @QuestionIds_11003614485753 nvarchar(32)= '';
    SELECT @QuestionIds_11003614485753 = STUFF( (SELECT ','+CAST([q].[QuestionID] AS nvarchar(32))
                                                 FROM [at].[Question] [q]
                                                 WHERE [q].[MasterID] IN ('E7BA7871-5A4C-487E-85D2-4386655EC00F', '8A5890C7-8E79-4B68-9330-CBF8BBA8BD1E')
                                                 ORDER BY CHARINDEX(CAST([q].[MasterID] AS nvarchar(64)), N'E7BA7871-5A4C-487E-85D2-4386655EC00F,8A5890C7-8E79-4B68-9330-CBF8BBA8BD1E')
                                                 FOR XML PATH, TYPE
                                                ) .value ('.[1]', 'nvarchar(max)'), 1, 1, '');
    DECLARE @QuestionIds_11003614485754 nvarchar(32)= '';
    SELECT @QuestionIds_11003614485754 = STUFF( (SELECT ','+CAST([q].[QuestionID] AS nvarchar(32))
                                                 FROM [at].[Question] [q]
                                                 WHERE [q].[MasterID] IN ('C4CF1481-18F8-4447-BB8E-0FC02E18C9B1')
                                                 ORDER BY CHARINDEX(CAST([q].[MasterID] AS nvarchar(64)), N'C4CF1481-18F8-4447-BB8E-0FC02E18C9B1')
                                                 FOR XML PATH, TYPE
                                                ) .value ('.[1]', 'nvarchar(max)'), 1, 1, '');
    DECLARE @QuestionIds_11003614485755 nvarchar(32)= '';
    SELECT @QuestionIds_11003614485755 = STUFF( (SELECT ','+CAST([q].[QuestionID] AS nvarchar(32))
                                                 FROM [at].[Question] [q]
                                                 WHERE [q].[MasterID] IN ('D6AD7642-8F3B-49F4-977B-0B124B7563A1')
                                                 ORDER BY CHARINDEX(CAST([q].[MasterID] AS nvarchar(64)), N'D6AD7642-8F3B-49F4-977B-0B124B7563A1')
                                                 FOR XML PATH, TYPE
                                                ) .value ('.[1]', 'nvarchar(max)'), 1, 1, '');
    DECLARE @QuestionIds_11003614485756 nvarchar(32)= '';
    SELECT @QuestionIds_11003614485756 = STUFF( (SELECT ','+CAST([q].[QuestionID] AS nvarchar(32))
                                                 FROM [at].[Question] [q]
                                                 WHERE [q].[MasterID] IN ('095EF475-30FF-4009-9376-8C3CCE33E372', '641DE5C9-15C9-4ECA-862F-7BC28C9EA763')
                                                 ORDER BY CHARINDEX(CAST([q].[MasterID] AS nvarchar(64)), N'095EF475-30FF-4009-9376-8C3CCE33E372,641DE5C9-15C9-4ECA-862F-7BC28C9EA763')
                                                 FOR XML PATH, TYPE
                                                ) .value ('.[1]', 'nvarchar(max)'), 1, 1, '');
    DECLARE @QuestionIds_11003614485757 nvarchar(32)= '';
    SELECT @QuestionIds_11003614485757 = STUFF( (SELECT ','+CAST([alt].[AlternativeID] AS nvarchar(32))
                                                 FROM [at].[Alternative] [alt]
                                                 WHERE [alt].[MasterID] IN ('2AFB864C-281D-44CC-9129-3BAC999C8086')
                                                 ORDER BY CHARINDEX(CAST([alt].[MasterID] AS nvarchar(64)), N'2AFB864C-281D-44CC-9129-3BAC999C8086')
                                                 FOR XML PATH, TYPE
                                                ) .value ('.[1]', 'nvarchar(max)'), 1, 1, '');
    IF NOT EXISTS (SELECT 1 FROM [app].[PortalPart] [pt] WHERE [pt].[MasterID] = '03B547D5-189F-4EB7-B6FD-84F3B57A17F7')
    BEGIN
        INSERT INTO [app].[PortalPart] ([PortalPageID], [PortalPartTypeID], [No], [CategoryID], [Settings], [BubbleID], [ReportPartID], [ReportID], [CssClass], [InlineStyle], [ActivityID], [SurveyID], [ExtID], [MasterID])
        SELECT [pp].[PortalPageID] AS [PortalPageID], N'43' AS [PortalPartTypeID], N'1' AS [No], NULL AS [CategoryID], N'<params>isemptyhtml%3D'+'true'+'%26DataSourceID%3D'+@DataSourceID_2152_11003614485752+'%26GeneralQuestionIds%3D'+@QuestionIds_11003614485753+'%26MarkQuestionIds%3D'+@QuestionIds_11003614485754+'%26AbsenceQuestionIds%3D'+@QuestionIds_11003614485755+'%26RankSummaryQuestionIds%3D'+@QuestionIds_11003614485756+'%26RankSummaryAlternativeIds%3D'+@QuestionIds_11003614485757+'</params>' AS [Settings], NULL AS [BubbleID], NULL AS [ReportPartID], NULL AS [ReportID], N'' AS [CssClass], N'' AS [InlineStyle], NULL AS [ActivityID], NULL AS [SurveyID], N'' AS [ExtID], N'03B547D5-189F-4EB7-B6FD-84F3B57A17F7' AS [MasterID]
        FROM [app].[PortalPage] [pp]
        WHERE [pp].[MasterID] = '8779CE5E-CF5E-48E9-A33F-13C25B115D06';
        DECLARE @PortalPartID_5617 nvarchar(32)= SCOPE_IDENTITY();

        INSERT INTO [app].[LT_PortalPart] ([LanguageID], [PortalPartID], [Name], [Description], [Text], [TextAbove], [TextBelow], [HeaderText], [FooterText], [BeforeContentText], [AfterContentText])
        SELECT l.[LanguageID], @PortalPartID_5617 AS [PortalPartID], N'MV1' AS [Name], N'' AS [Description], N'' AS [Text], N'' AS [TextAbove], N'' AS [TextBelow], N'' AS [HeaderText], N'' AS [FooterText], N'' AS [BeforeContentText], N'' AS [AfterContentText]
        FROM [dbo].[Language] l
        WHERE l.[LanguageID] IN (1,2,7);
    END;
END;
--#endregion PortalPage 3889

--#region PortalPage 3890
IF NOT EXISTS (SELECT 1 FROM [app].[PortalPage] [pp] WHERE [pp].[MasterID] = '820414F8-777B-490D-8D7F-9BF89AAEE3EA')
BEGIN
    INSERT INTO [app].[PortalPage] ([OwnerID], [No], [Parameter], [ExtID], [Created], [MasterID])
    SELECT [o].[OwnerID] AS [OwnerID], N'130' AS [No], N'' AS [Parameter], N'' AS [ExtID], GETDATE() AS [Created], N'820414F8-777B-490D-8D7F-9BF89AAEE3EA' AS [MasterID]
    FROM [org].[Owner] [o]
    WHERE [o].[MasterID] = '1C483E02-E808-42A7-8210-B11F4051D2E4';
    DECLARE @PortalPageID_3890 nvarchar(32)= SCOPE_IDENTITY();

    INSERT INTO [app].[LT_PortalPage] ([LanguageID], [PortalPageID], [Name], [Description])
    SELECT l.[LanguageID], @PortalPageID_3890 AS [PortalPageID], N'VGO Viewtype T1' AS [Name], N'' AS [Description]
    FROM [dbo].[Language] l
    WHERE l.[LanguageID] IN (1,2,7);

    DECLARE @DataSourceID_2152_11032912978802 nvarchar(32)= '';
    SELECT @DataSourceID_2152_11032912978802 = [ds].[ItemListDataSourceID]
    FROM [list].[ItemListDataSource] [ds]
    WHERE [ds].[MasterID] = '25E1D4AF-622A-4B11-AC20-4A2452DBD696';
    DECLARE @QuestionIds_11032912978803 nvarchar(32)= '';
    SELECT @QuestionIds_11032912978803 = STUFF( (SELECT ','+CAST([q].[QuestionID] AS nvarchar(32))
                                                 FROM [at].[Question] [q]
                                                 WHERE [q].[MasterID] IN ('E7BA7871-5A4C-487E-85D2-4386655EC00F', '8A5890C7-8E79-4B68-9330-CBF8BBA8BD1E')
                                                 ORDER BY CHARINDEX(CAST([q].[MasterID] AS nvarchar(64)), N'E7BA7871-5A4C-487E-85D2-4386655EC00F,8A5890C7-8E79-4B68-9330-CBF8BBA8BD1E')
                                                 FOR XML PATH, TYPE
                                                ) .value ('.[1]', 'nvarchar(max)'), 1, 1, '');
    DECLARE @QuestionIds_11032912978804 nvarchar(32)= '';
    SELECT @QuestionIds_11032912978804 = STUFF( (SELECT ','+CAST([q].[QuestionID] AS nvarchar(32))
                                                 FROM [at].[Question] [q]
                                                 WHERE [q].[MasterID] IN ('DF914DD2-941F-45A0-924A-DCFB67281FC3')
                                                 ORDER BY CHARINDEX(CAST([q].[MasterID] AS nvarchar(64)), N'DF914DD2-941F-45A0-924A-DCFB67281FC3')
                                                 FOR XML PATH, TYPE
                                                ) .value ('.[1]', 'nvarchar(max)'), 1, 1, '');
    DECLARE @QuestionIds_11032912978805 nvarchar(32)= '';
    SELECT @QuestionIds_11032912978805 = STUFF( (SELECT ','+CAST([q].[QuestionID] AS nvarchar(32))
                                                 FROM [at].[Question] [q]
                                                 WHERE [q].[MasterID] IN ('D6AD7642-8F3B-49F4-977B-0B124B7563A1')
                                                 ORDER BY CHARINDEX(CAST([q].[MasterID] AS nvarchar(64)), N'D6AD7642-8F3B-49F4-977B-0B124B7563A1')
                                                 FOR XML PATH, TYPE
                                                ) .value ('.[1]', 'nvarchar(max)'), 1, 1, '');
    DECLARE @QuestionIds_11032912978806 nvarchar(32)= '';
    SELECT @QuestionIds_11032912978806 = STUFF( (SELECT ','+CAST([q].[QuestionID] AS nvarchar(32))
                                                 FROM [at].[Question] [q]
                                                 WHERE [q].[MasterID] IN ('095EF475-30FF-4009-9376-8C3CCE33E372', '641DE5C9-15C9-4ECA-862F-7BC28C9EA763')
                                                 ORDER BY CHARINDEX(CAST([q].[MasterID] AS nvarchar(64)), N'095EF475-30FF-4009-9376-8C3CCE33E372,641DE5C9-15C9-4ECA-862F-7BC28C9EA763')
                                                 FOR XML PATH, TYPE
                                                ) .value ('.[1]', 'nvarchar(max)'), 1, 1, '');
    DECLARE @QuestionIds_11032914533717 nvarchar(32)= '';
    SELECT @QuestionIds_11032914533717 = STUFF( (SELECT ','+CAST([alt].[AlternativeID] AS nvarchar(32))
                                                 FROM [at].[Alternative] [alt]
                                                 WHERE [alt].[MasterID] IN ('99E534EF-A3BE-439A-AA8E-094C698F86F5')
                                                 ORDER BY CHARINDEX(CAST([alt].[MasterID] AS nvarchar(64)), N'99E534EF-A3BE-439A-AA8E-094C698F86F5')
                                                 FOR XML PATH, TYPE
                                                ) .value ('.[1]', 'nvarchar(max)'), 1, 1, '');
    IF NOT EXISTS (SELECT 1 FROM [app].[PortalPart] [pt] WHERE [pt].[MasterID] = '41070FE7-FF2E-419A-8815-B7C56E7DC884')
    BEGIN
        INSERT INTO [app].[PortalPart] ([PortalPageID], [PortalPartTypeID], [No], [CategoryID], [Settings], [BubbleID], [ReportPartID], [ReportID], [CssClass], [InlineStyle], [ActivityID], [SurveyID], [ExtID], [MasterID])
        SELECT [pp].[PortalPageID] AS [PortalPageID], N'43' AS [PortalPartTypeID], N'1' AS [No], NULL AS [CategoryID], N'<params>isemptyhtml%3D'+'true'+'%26DataSourceID%3D'+@DataSourceID_2152_11032912978802+'%26GeneralQuestionIds%3D'+@QuestionIds_11032912978803+'%26MarkQuestionIds%3D'+@QuestionIds_11032912978804+'%26AbsenceQuestionIds%3D'+@QuestionIds_11032912978805+'%26RankSummaryQuestionIds%3D'+@QuestionIds_11032912978806+'%26RankSummaryAlternativeIds%3D'+@QuestionIds_11032914533717+'</params>' AS [Settings], NULL AS [BubbleID], NULL AS [ReportPartID], NULL AS [ReportID], N'' AS [CssClass], N'' AS [InlineStyle], NULL AS [ActivityID], NULL AS [SurveyID], N'' AS [ExtID], N'41070FE7-FF2E-419A-8815-B7C56E7DC884' AS [MasterID]
        FROM [app].[PortalPage] [pp]
        WHERE [pp].[MasterID] = '820414F8-777B-490D-8D7F-9BF89AAEE3EA';
        DECLARE @PortalPartID_5618 nvarchar(32)= SCOPE_IDENTITY();

        INSERT INTO [app].[LT_PortalPart] ([LanguageID], [PortalPartID], [Name], [Description], [Text], [TextAbove], [TextBelow], [HeaderText], [FooterText], [BeforeContentText], [AfterContentText])
        SELECT l.[LanguageID], @PortalPartID_5618 AS [PortalPartID], N'T1' AS [Name], N'' AS [Description], N'' AS [Text], N'' AS [TextAbove], N'' AS [TextBelow], N'' AS [HeaderText], N'' AS [FooterText], N'' AS [BeforeContentText], N'' AS [AfterContentText]
        FROM [dbo].[Language] l
        WHERE l.[LanguageID] IN (1,2,7);
    END;
END;
--#endregion PortalPage 3890

--#region PortalPage 3891
IF NOT EXISTS (SELECT 1 FROM [app].[PortalPage] [pp] WHERE [pp].[MasterID] = 'FBE10F13-1D42-4328-A1F6-770A71E61201')
BEGIN
    INSERT INTO [app].[PortalPage] ([OwnerID], [No], [Parameter], [ExtID], [Created], [MasterID])
    SELECT [o].[OwnerID] AS [OwnerID], N'131' AS [No], N'' AS [Parameter], N'' AS [ExtID], GETDATE() AS [Created], N'FBE10F13-1D42-4328-A1F6-770A71E61201' AS [MasterID]
    FROM [org].[Owner] [o]
    WHERE [o].[MasterID] = '1C483E02-E808-42A7-8210-B11F4051D2E4';
    DECLARE @PortalPageID_3891 nvarchar(32)= SCOPE_IDENTITY();

    INSERT INTO [app].[LT_PortalPage] ([LanguageID], [PortalPageID], [Name], [Description])
    SELECT l.[LanguageID], @PortalPageID_3891 AS [PortalPageID], N'VGO Viewtype MV2' AS [Name], N'' AS [Description]
    FROM [dbo].[Language] l
    WHERE l.[LanguageID] IN (1,2,7);

    DECLARE @DataSourceID_2152_11062489472942 nvarchar(32)= '';
    SELECT @DataSourceID_2152_11062489472942 = [ds].[ItemListDataSourceID]
    FROM [list].[ItemListDataSource] [ds]
    WHERE [ds].[MasterID] = '25E1D4AF-622A-4B11-AC20-4A2452DBD696';
    DECLARE @QuestionIds_11062489472943 nvarchar(32)= '';
    SELECT @QuestionIds_11062489472943 = STUFF( (SELECT ','+CAST([q].[QuestionID] AS nvarchar(32))
                                                 FROM [at].[Question] [q]
                                                 WHERE [q].[MasterID] IN ('E7BA7871-5A4C-487E-85D2-4386655EC00F', '8A5890C7-8E79-4B68-9330-CBF8BBA8BD1E')
                                                 ORDER BY CHARINDEX(CAST([q].[MasterID] AS nvarchar(64)), N'E7BA7871-5A4C-487E-85D2-4386655EC00F,8A5890C7-8E79-4B68-9330-CBF8BBA8BD1E')
                                                 FOR XML PATH, TYPE
                                                ) .value ('.[1]', 'nvarchar(max)'), 1, 1, '');
    DECLARE @QuestionIds_11062489472944 nvarchar(32)= '';
    SELECT @QuestionIds_11062489472944 = STUFF( (SELECT ','+CAST([q].[QuestionID] AS nvarchar(32))
                                                 FROM [at].[Question] [q]
                                                 WHERE [q].[MasterID] IN ('7160E01A-6E37-4BBA-931B-5C782416A515')
                                                 ORDER BY CHARINDEX(CAST([q].[MasterID] AS nvarchar(64)), N'7160E01A-6E37-4BBA-931B-5C782416A515')
                                                 FOR XML PATH, TYPE
                                                ) .value ('.[1]', 'nvarchar(max)'), 1, 1, '');
    DECLARE @QuestionIds_11062489472945 nvarchar(32)= '';
    SELECT @QuestionIds_11062489472945 = STUFF( (SELECT ','+CAST([q].[QuestionID] AS nvarchar(32))
                                                 FROM [at].[Question] [q]
                                                 WHERE [q].[MasterID] IN ('F9508ECD-F240-4408-A32E-74EFB8A411E9')
                                                 ORDER BY CHARINDEX(CAST([q].[MasterID] AS nvarchar(64)), N'F9508ECD-F240-4408-A32E-74EFB8A411E9')
                                                 FOR XML PATH, TYPE
                                                ) .value ('.[1]', 'nvarchar(max)'), 1, 1, '');
    DECLARE @QuestionIds_11062489472946 nvarchar(32)= '';
    SELECT @QuestionIds_11062489472946 = STUFF( (SELECT ','+CAST([q].[QuestionID] AS nvarchar(32))
                                                 FROM [at].[Question] [q]
                                                 WHERE [q].[MasterID] IN ('095EF475-30FF-4009-9376-8C3CCE33E372', '641DE5C9-15C9-4ECA-862F-7BC28C9EA763')
                                                 ORDER BY CHARINDEX(CAST([q].[MasterID] AS nvarchar(64)), N'095EF475-30FF-4009-9376-8C3CCE33E372,641DE5C9-15C9-4ECA-862F-7BC28C9EA763')
                                                 FOR XML PATH, TYPE
                                                ) .value ('.[1]', 'nvarchar(max)'), 1, 1, '');
    DECLARE @QuestionIds_11062489472947 nvarchar(32)= '';
    SELECT @QuestionIds_11062489472947 = STUFF( (SELECT ','+CAST([alt].[AlternativeID] AS nvarchar(32))
                                                 FROM [at].[Alternative] [alt]
                                                 WHERE [alt].[MasterID] IN ('CA2D721E-96D0-47EB-8160-D6CBE5FF0735')
                                                 ORDER BY CHARINDEX(CAST([alt].[MasterID] AS nvarchar(64)), N'CA2D721E-96D0-47EB-8160-D6CBE5FF0735')
                                                 FOR XML PATH, TYPE
                                                ) .value ('.[1]', 'nvarchar(max)'), 1, 1, '');
    IF NOT EXISTS (SELECT 1 FROM [app].[PortalPart] [pt] WHERE [pt].[MasterID] = '8A4C2406-2EA7-411B-AF46-3D88AF965872')
    BEGIN
        INSERT INTO [app].[PortalPart] ([PortalPageID], [PortalPartTypeID], [No], [CategoryID], [Settings], [BubbleID], [ReportPartID], [ReportID], [CssClass], [InlineStyle], [ActivityID], [SurveyID], [ExtID], [MasterID])
        SELECT [pp].[PortalPageID] AS [PortalPageID], N'43' AS [PortalPartTypeID], N'1' AS [No], NULL AS [CategoryID], N'<params>isemptyhtml%3D'+'true'+'%26DataSourceID%3D'+@DataSourceID_2152_11062489472942+'%26GeneralQuestionIds%3D'+@QuestionIds_11062489472943+'%26MarkQuestionIds%3D'+@QuestionIds_11062489472944+'%26AbsenceQuestionIds%3D'+@QuestionIds_11062489472945+'%26RankSummaryQuestionIds%3D'+@QuestionIds_11062489472946+'%26RankSummaryAlternativeIds%3D'+@QuestionIds_11062489472947+'</params>' AS [Settings], NULL AS [BubbleID], NULL AS [ReportPartID], NULL AS [ReportID], N'' AS [CssClass], N'' AS [InlineStyle], NULL AS [ActivityID], NULL AS [SurveyID], N'' AS [ExtID], N'8A4C2406-2EA7-411B-AF46-3D88AF965872' AS [MasterID]
        FROM [app].[PortalPage] [pp]
        WHERE [pp].[MasterID] = 'FBE10F13-1D42-4328-A1F6-770A71E61201';
        DECLARE @PortalPartID_5619 nvarchar(32)= SCOPE_IDENTITY();

        INSERT INTO [app].[LT_PortalPart] ([LanguageID], [PortalPartID], [Name], [Description], [Text], [TextAbove], [TextBelow], [HeaderText], [FooterText], [BeforeContentText], [AfterContentText])
        SELECT l.[LanguageID], @PortalPartID_5619 AS [PortalPartID], N'MV2' AS [Name], N'' AS [Description], N'' AS [Text], N'' AS [TextAbove], N'' AS [TextBelow], N'' AS [HeaderText], N'' AS [FooterText], N'' AS [BeforeContentText], N'' AS [AfterContentText]
        FROM [dbo].[Language] l
        WHERE l.[LanguageID] IN (1,2,7);
    END;
END;
--#endregion PortalPage 3891

--#region PortalPage 3892
IF NOT EXISTS (SELECT 1 FROM [app].[PortalPage] [pp] WHERE [pp].[MasterID] = '679F2196-E4D8-4B98-851D-D29070AE0CD1')
BEGIN
    INSERT INTO [app].[PortalPage] ([OwnerID], [No], [Parameter], [ExtID], [Created], [MasterID])
    SELECT [o].[OwnerID] AS [OwnerID], N'132' AS [No], N'' AS [Parameter], N'' AS [ExtID], GETDATE() AS [Created], N'679F2196-E4D8-4B98-851D-D29070AE0CD1' AS [MasterID]
    FROM [org].[Owner] [o]
    WHERE [o].[MasterID] = '1C483E02-E808-42A7-8210-B11F4051D2E4';
    DECLARE @PortalPageID_3892 nvarchar(32)= SCOPE_IDENTITY();

    INSERT INTO [app].[LT_PortalPage] ([LanguageID], [PortalPageID], [Name], [Description])
    SELECT l.[LanguageID], @PortalPageID_3892 AS [PortalPageID], N'VGO Viewtype T2' AS [Name], N'' AS [Description]
    FROM [dbo].[Language] l
    WHERE l.[LanguageID] IN (1,2,7);

    DECLARE @DataSourceID_2152_11092533253402 nvarchar(32)= '';
    SELECT @DataSourceID_2152_11092533253402 = [ds].[ItemListDataSourceID]
    FROM [list].[ItemListDataSource] [ds]
    WHERE [ds].[MasterID] = '25E1D4AF-622A-4B11-AC20-4A2452DBD696';
    DECLARE @QuestionIds_11092533253403 nvarchar(32)= '';
    SELECT @QuestionIds_11092533253403 = STUFF( (SELECT ','+CAST([q].[QuestionID] AS nvarchar(32))
                                                 FROM [at].[Question] [q]
                                                 WHERE [q].[MasterID] IN ('E7BA7871-5A4C-487E-85D2-4386655EC00F', '8A5890C7-8E79-4B68-9330-CBF8BBA8BD1E')
                                                 ORDER BY CHARINDEX(CAST([q].[MasterID] AS nvarchar(64)), N'E7BA7871-5A4C-487E-85D2-4386655EC00F,8A5890C7-8E79-4B68-9330-CBF8BBA8BD1E')
                                                 FOR XML PATH, TYPE
                                                ) .value ('.[1]', 'nvarchar(max)'), 1, 1, '');
    DECLARE @QuestionIds_11092533253404 nvarchar(32)= '';
    SELECT @QuestionIds_11092533253404 = STUFF( (SELECT ','+CAST([q].[QuestionID] AS nvarchar(32))
                                                 FROM [at].[Question] [q]
                                                 WHERE [q].[MasterID] IN ('CF4E288E-16E9-4B64-BBCE-6CC13E7C3CD4')
                                                 ORDER BY CHARINDEX(CAST([q].[MasterID] AS nvarchar(64)), N'CF4E288E-16E9-4B64-BBCE-6CC13E7C3CD4')
                                                 FOR XML PATH, TYPE
                                                ) .value ('.[1]', 'nvarchar(max)'), 1, 1, '');
    DECLARE @QuestionIds_11092533253405 nvarchar(32)= '';
    SELECT @QuestionIds_11092533253405 = STUFF( (SELECT ','+CAST([q].[QuestionID] AS nvarchar(32))
                                                 FROM [at].[Question] [q]
                                                 WHERE [q].[MasterID] IN ('F9508ECD-F240-4408-A32E-74EFB8A411E9')
                                                 ORDER BY CHARINDEX(CAST([q].[MasterID] AS nvarchar(64)), N'F9508ECD-F240-4408-A32E-74EFB8A411E9')
                                                 FOR XML PATH, TYPE
                                                ) .value ('.[1]', 'nvarchar(max)'), 1, 1, '');
    DECLARE @QuestionIds_11092533253406 nvarchar(32)= '';
    SELECT @QuestionIds_11092533253406 = STUFF( (SELECT ','+CAST([q].[QuestionID] AS nvarchar(32))
                                                 FROM [at].[Question] [q]
                                                 WHERE [q].[MasterID] IN ('095EF475-30FF-4009-9376-8C3CCE33E372', '641DE5C9-15C9-4ECA-862F-7BC28C9EA763')
                                                 ORDER BY CHARINDEX(CAST([q].[MasterID] AS nvarchar(64)), N'095EF475-30FF-4009-9376-8C3CCE33E372,641DE5C9-15C9-4ECA-862F-7BC28C9EA763')
                                                 FOR XML PATH, TYPE
                                                ) .value ('.[1]', 'nvarchar(max)'), 1, 1, '');
    DECLARE @QuestionIds_11092533253407 nvarchar(32)= '';
    SELECT @QuestionIds_11092533253407 = STUFF( (SELECT ','+CAST([alt].[AlternativeID] AS nvarchar(32))
                                                 FROM [at].[Alternative] [alt]
                                                 WHERE [alt].[MasterID] IN ('F94FAAD5-C0D7-4B92-9515-1991EFB44402')
                                                 ORDER BY CHARINDEX(CAST([alt].[MasterID] AS nvarchar(64)), N'F94FAAD5-C0D7-4B92-9515-1991EFB44402')
                                                 FOR XML PATH, TYPE
                                                ) .value ('.[1]', 'nvarchar(max)'), 1, 1, '');
    IF NOT EXISTS (SELECT 1 FROM [app].[PortalPart] [pt] WHERE [pt].[MasterID] = '3BD57557-33F7-41FC-9F50-896EAB77FA26')
    BEGIN
        INSERT INTO [app].[PortalPart] ([PortalPageID], [PortalPartTypeID], [No], [CategoryID], [Settings], [BubbleID], [ReportPartID], [ReportID], [CssClass], [InlineStyle], [ActivityID], [SurveyID], [ExtID], [MasterID])
        SELECT [pp].[PortalPageID] AS [PortalPageID], N'43' AS [PortalPartTypeID], N'1' AS [No], NULL AS [CategoryID], N'<params>isemptyhtml%3D'+'true'+'%26DataSourceID%3D'+@DataSourceID_2152_11092533253402+'%26GeneralQuestionIds%3D'+@QuestionIds_11092533253403+'%26MarkQuestionIds%3D'+@QuestionIds_11092533253404+'%26AbsenceQuestionIds%3D'+@QuestionIds_11092533253405+'%26RankSummaryQuestionIds%3D'+@QuestionIds_11092533253406+'%26RankSummaryAlternativeIds%3D'+@QuestionIds_11092533253407+'</params>' AS [Settings], NULL AS [BubbleID], NULL AS [ReportPartID], NULL AS [ReportID], N'' AS [CssClass], N'' AS [InlineStyle], NULL AS [ActivityID], NULL AS [SurveyID], N'' AS [ExtID], N'3BD57557-33F7-41FC-9F50-896EAB77FA26' AS [MasterID]
        FROM [app].[PortalPage] [pp]
        WHERE [pp].[MasterID] = '679F2196-E4D8-4B98-851D-D29070AE0CD1';
        DECLARE @PortalPartID_5620 nvarchar(32)= SCOPE_IDENTITY();

        INSERT INTO [app].[LT_PortalPart] ([LanguageID], [PortalPartID], [Name], [Description], [Text], [TextAbove], [TextBelow], [HeaderText], [FooterText], [BeforeContentText], [AfterContentText])
        SELECT l.[LanguageID], @PortalPartID_5620 AS [PortalPartID], N'T2' AS [Name], N'' AS [Description], N'' AS [Text], N'' AS [TextAbove], N'' AS [TextBelow], N'' AS [HeaderText], N'' AS [FooterText], N'' AS [BeforeContentText], N'' AS [AfterContentText]
        FROM [dbo].[Language] l
        WHERE l.[LanguageID] IN (1,2,7);
    END;
END;
--#endregion PortalPage 3892

UPDATE pt SET pt.[Settings] = REPLACE(CAST(pt.[Settings] AS nvarchar(MAX)), 'IKOActivityId%3D600', 'IKOActivityId%3D1')
FROM [app].[PortalPart] pt
WHERE pt.[MasterID] = '645AA189-9647-440E-B734-E5D66B93074B';

UPDATE [app].[SiteParameter] SET [Value] = '1' WHERE [Key] = 'Vokal.Organization.DenyActivityIds';

--#endregion 100. CE-200 Individual View Of Activity 600
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
--#region 102. Fix Extra Answers Of Single Choice Scales
-- Dev: Ray
-- Feature: fix answers
-- Fix number of answers more than scale allowed

--#region Single choice scales
SELECT scl.[ActivityID], q.[PageID], scl.[ScaleID], a.[ResultID], a.[QuestionID], ISNULL(a.[ItemID],0) AS [ItemID], COUNT(a.[AnswerID]) AS [AnswerCount]
INTO #SingleChoiceAnswer
FROM [at].[Scale] scl
JOIN [at].[Alternative] alt ON alt.[ScaleID] = scl.[ScaleID] AND alt.[Type] = 0 AND scl.[MinSelect] = 1 AND scl.[MaxSelect] = 1
JOIN [dbo].[Answer] a ON a.[AlternativeID] = alt.[AlternativeID]
JOIN [at].[Question] q ON q.[QuestionID] = a.[QuestionID]
GROUP BY scl.[ActivityID], q.[PageID], scl.[ScaleID], a.[ResultID], a.[QuestionID], a.[ItemID]
HAVING COUNT(a.[AnswerID]) > 1;

DELETE ans
FROM (SELECT a.[ResultID], a.[QuestionID], a.[AlternativeID], a.[ItemID], a.[AnswerID],
             FIRST_VALUE(a.[AnswerID]) OVER (PARTITION BY a.[ResultID], a.[QuestionID], a.[ItemID] ORDER BY a.[AnswerID] DESC) AS [KeepAnswerID]
      FROM #SingleChoiceAnswer s
      JOIN [dbo].[Answer] a ON a.[ResultID] = s.[ResultID] AND a.[QuestionID] = s.[QuestionID] AND ISNULL(a.[ItemID],0) = s.[ItemID]
      JOIN [at].[Alternative] alt ON alt.[AlternativeID] = a.[AlternativeID] AND alt.[Type] = 0) AS v
JOIN [dbo].[Answer] ans ON ans.[AnswerID] = v.[AnswerID] AND v.[AnswerID] <> v.[KeepAnswerID];

DROP TABLE IF EXISTS #SingleChoiceAnswer;
--#endregion Single choice scales

--#endregion 102. Fix Extra Answers Of Single Choice Scales
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
-------------------------------------------------------------------------------------------------------------------
PRINT 'Setting database version'
/*
Start of versioning database-level extended properties.
*/
BEGIN
    DECLARE @DB_VERSION varchar(32), @ConceptName nvarchar(128), @ReleaseScriptName nvarchar(4000)
    SET @DB_VERSION = '6.12.006.002.01'
    SET @ConceptName = 'Engage Main'
    SET @ReleaseScriptName = N'v' + @DB_VERSION + N'-03. Engage_NO Data Update Since 2019-01-07'

    IF EXISTS (SELECT 1 FROM fn_listextendedproperty(N'cxStudio Database Version', NULL, NULL, NULL, NULL, NULL, NULL))
        EXEC sp_dropextendedproperty N'cxStudio Database Version', NULL, NULL, NULL, NULL, NULL, NULL
    EXEC sp_addextendedproperty N'cxStudio Database Version', @DB_VERSION, NULL, NULL, NULL, NULL, NULL, NULL
		
    IF EXISTS (SELECT 1 FROM fn_listextendedproperty(N'Script Name', NULL, NULL, NULL, NULL, NULL, NULL))
        EXEC sp_dropextendedproperty N'Script Name', NULL, NULL, NULL, NULL, NULL, NULL
    EXEC sp_addextendedproperty N'Script Name',  @ReleaseScriptName , NULL, NULL, NULL, NULL, NULL, NULL
END
/*
End of versioning database-level extended properties.
*/
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
IF EXISTS (SELECT * FROM #tmpErrors) ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT>0 BEGIN
PRINT 'The database update succeeded'
COMMIT TRANSACTION
END
ELSE PRINT 'The database update failed'
GO
DROP TABLE #tmpErrors
GO
SET NOEXEC OFF
GO
-------------------------------------------------------------------------------------------------------------------------
DECLARE @Current_DB_Version varchar(32), @CurrentConceptName nvarchar(128),
        @Required_DB_Version varchar(32), @RequiredConceptName nvarchar(128)

SET @Required_DB_Version = '6.12.006.002.01'
SET @RequiredConceptName = 'Engage Main'
PRINT 'This script is generated for executing on ' + @RequiredConceptName + ' version ' + @Required_DB_Version

SELECT @Current_DB_Version = convert(nvarchar(max),value) FROM fn_listextendedproperty(N'cxStudio Database Version', NULL, NULL, NULL, NULL, NULL, NULL)
SELECT @CurrentConceptName = convert(nvarchar(max),value) FROM fn_listextendedproperty(N'Concept Name', NULL, NULL, NULL, NULL, NULL, NULL)

IF @CurrentConceptName != @RequiredConceptName OR @Current_DB_Version NOT LIKE @Required_DB_Version
BEGIN
    PRINT 'Incorrect solution or cxStudio Database version'
    SET NOEXEC ON
END
GO
-------------------------------------------------------------------------------------------------------------------------
IF EXISTS (SELECT * FROM tempdb..sysobjects WHERE id=OBJECT_ID('tempdb..#tmpErrors')) DROP TABLE #tmpErrors
GO
CREATE TABLE #tmpErrors (Error int)
GO
SET XACT_ABORT ON
GO
SET TRANSACTION ISOLATION LEVEL SERIALIZABLE
GO
BEGIN TRANSACTION
GO
----------------------------------------------------------------------------------------------------------------
--#region 103. CE-1871 Student Report Popup
-- Dev: Thomas
-- Feature: Object follow up
-- CE-1871 Remove old "oppfølging i fag" tab from student report popup
DELETE dp
FROM [list].[ItemListDataParameter] dp
WHERE dp.[MasterID] IN ('1F50DF3B-925D-4316-AD68-14440EB191E7','CC51B847-5862-45FA-B54A-64154A9EF3A9','DB9F1318-4A7C-4D7F-ABBF-06F3EDA09214',
                        '63BCBE3E-E3BF-45C7-B2B5-D38801107307','6310B0CD-D752-49C5-BFF5-D6EBA4244367','46319ED6-8F20-4CB2-BBED-7889C7F5EDA5',
                        '7BF1D8A8-83F9-4D66-A5CB-1B627EC9E1D1','CFE85C7A-B00C-48A6-A1F3-C244352B62EB','335F4962-04C6-4EA7-8B65-300DE9F779CE');

--#endregion 103. CE-1871 Student Report Popup
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
--#region 107. Class View For Activity 600

--#region 01. CE-2059 Class View For Activity 600
-- Dev: Gary
-- Feature: Activity follow up
-- CE-2059 Class view for activity 600

--#region Procedure
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[prc_SubjectList_getByUsers]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [dbo].[prc_SubjectList_getByUsers] AS' 
END
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
/*  2019-01-11 Ray:     Get list of subjects related to given users
*/
ALTER PROCEDURE [dbo].[prc_SubjectList_getByUsers]
(   @SchoolDepartmentID         int,
    @UserIDList                 nvarchar(max),
    @ArchetypeID_TeachingGroup  int,
    @ActivityID                 int,
    @PeriodID                   int
) AS
BEGIN
    DECLARE @EntityStatus_Active int
    SELECT @EntityStatus_Active = es.[EntityStatusID] FROM [dbo].[EntityStatus] es WHERE es.[CodeName] = 'Active';

    DECLARE @UserIDTable TABLE ([UserID] int);
    INSERT INTO @UserIDTable ([UserID])
    SELECT [Value] FROM [dbo].[funcListToTableInt] (@UserIDList, ',');
    
    SELECT DISTINCT c.*
    FROM [org].[UserGroup] ug
    JOIN [org].[UGMember] ugm ON ugm.[UserGroupID] = ug.[UserGroupID] AND ug.[ArchetypeID] = @ArchetypeID_TeachingGroup AND ug.[DepartmentID] = @SchoolDepartmentID
     AND ugm.[EntityStatusID] = @EntityStatus_Active AND ugm.[Deleted] IS NULL AND ug.[PeriodID] = @PeriodID
    JOIN @UserIDTable uit ON uit.[UserID] = ugm.[UserID]
    JOIN [at].[Category] c ON c.[ActivityID] = @ActivityID AND c.[ExtID] = ug.[Tag];
END
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
--#endregion Procedure

--#region Config Data

--#region DataSource 2161
IF NOT EXISTS (SELECT 1 FROM [list].[ItemListDataSource] [ds] WHERE [ds].[MasterID] = '9DB9B94B-D050-44D5-AD3A-4913AF1442C9')
BEGIN
    INSERT INTO [list].[ItemListDataSource] ([OwnerID], [FunctionName], [Created], [MasterID])
    SELECT [o].[OwnerID] AS [OwnerID], N'VgoTableContent' AS [FunctionName], GETDATE() AS [Created], N'9DB9B94B-D050-44D5-AD3A-4913AF1442C9' AS [MasterID]
    FROM [org].[Owner] [o]
    WHERE [o].[MasterID] = '1C483E02-E808-42A7-8210-B11F4051D2E4';
    DECLARE @ItemListDataSourceID_2161 nvarchar(32)= SCOPE_IDENTITY();

    INSERT INTO [list].[LT_ItemListDataSource] ([LanguageID], [ItemListDataSourceID], [Name], [Description])
    SELECT l.[LanguageID], @ItemListDataSourceID_2161 AS [ItemListDataSourceID], N'[VGO UserList] Table Content' AS [Name], N'' AS [Description]
    FROM [dbo].[Language] l
    WHERE l.[LanguageID] IN (1,2,7);

    IF NOT EXISTS (SELECT 1 FROM [list].[ItemListDataParameter] [dp] WHERE [dp].[MasterID] = '2FA86FC9-97C6-4B42-9BB9-3CF6304C26ED')
    BEGIN
        INSERT INTO [list].[ItemListDataParameter] ([ItemListDataSourceID], [DataType], [Type], [Mandatory], [DefaultValue], [ConstructionPattern], [Created], [MasterID])
        SELECT [ds].[ItemListDataSourceID] AS [ItemListDataSourceID], N'String' AS [DataType], N'PortalPartParam' AS [Type], N'0' AS [Mandatory], N''+'VgoTableContent'+'' AS [DefaultValue], N'' AS [ConstructionPattern], GETDATE() AS [Created], N'2FA86FC9-97C6-4B42-9BB9-3CF6304C26ED' AS [MasterID]
        FROM [list].[ItemListDataSource] [ds]
        WHERE [ds].[MasterID] = '9DB9B94B-D050-44D5-AD3A-4913AF1442C9';
        DECLARE @ParamID_2776 nvarchar(32)= SCOPE_IDENTITY();

        INSERT INTO [list].[LT_ItemListDataParameter] ([LanguageID], [ItemListDataParameterID], [Name], [Description])
        SELECT l.[LanguageID], @ParamID_2776 AS [ItemListDataParameterID], N'ViewName' AS [Name], N'' AS [Description]
        FROM [dbo].[Language] l
        WHERE l.[LanguageID] IN (1,2,7);
    END;

    DECLARE @ItemListID_1369_15543240046371 nvarchar(32)= '';
    SELECT @ItemListID_1369_15543240046371 = [l].[ItemListID]
    FROM [list].[ItemList] [l]
    WHERE [l].[MasterID] = 'A6F5E879-610C-496E-A3B0-3CAB8331AE17';
    IF NOT EXISTS (SELECT 1 FROM [list].[ItemListDataParameter] [dp] WHERE [dp].[MasterID] = 'BC8C605E-113A-4CE9-9126-F8FB2BFD193A')
    BEGIN
        INSERT INTO [list].[ItemListDataParameter] ([ItemListDataSourceID], [DataType], [Type], [Mandatory], [DefaultValue], [ConstructionPattern], [Created], [MasterID])
        SELECT [ds].[ItemListDataSourceID] AS [ItemListDataSourceID], N'Integer' AS [DataType], N'PortalPartParam' AS [Type], N'0' AS [Mandatory], N''+@ItemListID_1369_15543240046371+'' AS [DefaultValue], N'' AS [ConstructionPattern], GETDATE() AS [Created], N'BC8C605E-113A-4CE9-9126-F8FB2BFD193A' AS [MasterID]
        FROM [list].[ItemListDataSource] [ds]
        WHERE [ds].[MasterID] = '9DB9B94B-D050-44D5-AD3A-4913AF1442C9';
        DECLARE @ParamID_2780 nvarchar(32)= SCOPE_IDENTITY();

        INSERT INTO [list].[LT_ItemListDataParameter] ([LanguageID], [ItemListDataParameterID], [Name], [Description])
        SELECT l.[LanguageID], @ParamID_2780 AS [ItemListDataParameterID], N'userlistid' AS [Name], N'' AS [Description]
        FROM [dbo].[Language] l
        WHERE l.[LanguageID] IN (1,2,7);
    END;

    IF NOT EXISTS (SELECT 1 FROM [list].[ItemListDataParameter] [dp] WHERE [dp].[MasterID] = 'B63B6EDE-A818-4A65-B913-572956486475')
    BEGIN
        INSERT INTO [list].[ItemListDataParameter] ([ItemListDataSourceID], [DataType], [Type], [Mandatory], [DefaultValue], [ConstructionPattern], [Created], [MasterID])
        SELECT [ds].[ItemListDataSourceID] AS [ItemListDataSourceID], N'Integer' AS [DataType], N'DynamicallyAddedPortalPartParam' AS [Type], N'0' AS [Mandatory], N'' AS [DefaultValue], N'' AS [ConstructionPattern], GETDATE() AS [Created], N'B63B6EDE-A818-4A65-B913-572956486475' AS [MasterID]
        FROM [list].[ItemListDataSource] [ds]
        WHERE [ds].[MasterID] = '9DB9B94B-D050-44D5-AD3A-4913AF1442C9';
        DECLARE @ParamID_2781 nvarchar(32)= SCOPE_IDENTITY();

        INSERT INTO [list].[LT_ItemListDataParameter] ([LanguageID], [ItemListDataParameterID], [Name], [Description])
        SELECT l.[LanguageID], @ParamID_2781 AS [ItemListDataParameterID], N'ActivityId' AS [Name], N'' AS [Description]
        FROM [dbo].[Language] l
        WHERE l.[LanguageID] IN (1,2,7);
    END;

    IF NOT EXISTS (SELECT 1 FROM [list].[ItemListDataParameter] [dp] WHERE [dp].[MasterID] = '331E4436-1E8F-4559-BC2A-27F4FEA23448')
    BEGIN
        INSERT INTO [list].[ItemListDataParameter] ([ItemListDataSourceID], [DataType], [Type], [Mandatory], [DefaultValue], [ConstructionPattern], [Created], [MasterID])
        SELECT [ds].[ItemListDataSourceID] AS [ItemListDataSourceID], N'Integer' AS [DataType], N'DynamicallyAddedPortalPartParam' AS [Type], N'0' AS [Mandatory], N'' AS [DefaultValue], N'' AS [ConstructionPattern], GETDATE() AS [Created], N'331E4436-1E8F-4559-BC2A-27F4FEA23448' AS [MasterID]
        FROM [list].[ItemListDataSource] [ds]
        WHERE [ds].[MasterID] = '9DB9B94B-D050-44D5-AD3A-4913AF1442C9';
        DECLARE @ParamID_2782 nvarchar(32)= SCOPE_IDENTITY();

        INSERT INTO [list].[LT_ItemListDataParameter] ([LanguageID], [ItemListDataParameterID], [Name], [Description])
        SELECT l.[LanguageID], @ParamID_2782 AS [ItemListDataParameterID], N'SurveyId' AS [Name], N'' AS [Description]
        FROM [dbo].[Language] l
        WHERE l.[LanguageID] IN (1,2,7);
    END;

    IF NOT EXISTS (SELECT 1 FROM [list].[ItemListDataParameter] [dp] WHERE [dp].[MasterID] = 'AF5F2190-9327-4988-B40A-0F0C72F8F90A')
    BEGIN
        INSERT INTO [list].[ItemListDataParameter] ([ItemListDataSourceID], [DataType], [Type], [Mandatory], [DefaultValue], [ConstructionPattern], [Created], [MasterID])
        SELECT [ds].[ItemListDataSourceID] AS [ItemListDataSourceID], N'Integer' AS [DataType], N'DynamicallyAddedPortalPartParam' AS [Type], N'0' AS [Mandatory], N'' AS [DefaultValue], N'' AS [ConstructionPattern], GETDATE() AS [Created], N'AF5F2190-9327-4988-B40A-0F0C72F8F90A' AS [MasterID]
        FROM [list].[ItemListDataSource] [ds]
        WHERE [ds].[MasterID] = '9DB9B94B-D050-44D5-AD3A-4913AF1442C9';
        DECLARE @ParamID_2783 nvarchar(32)= SCOPE_IDENTITY();

        INSERT INTO [list].[LT_ItemListDataParameter] ([LanguageID], [ItemListDataParameterID], [Name], [Description])
        SELECT l.[LanguageID], @ParamID_2783 AS [ItemListDataParameterID], N'UserGroupId' AS [Name], N'' AS [Description]
        FROM [dbo].[Language] l
        WHERE l.[LanguageID] IN (1,2,7);
    END;

    IF NOT EXISTS (SELECT 1 FROM [list].[ItemListDataParameter] [dp] WHERE [dp].[MasterID] = 'DAEC4CAF-FC90-4226-94CA-41D755905C44')
    BEGIN
        INSERT INTO [list].[ItemListDataParameter] ([ItemListDataSourceID], [DataType], [Type], [Mandatory], [DefaultValue], [ConstructionPattern], [Created], [MasterID])
        SELECT [ds].[ItemListDataSourceID] AS [ItemListDataSourceID], N'Integer' AS [DataType], N'DynamicallyAddedPortalPartParam' AS [Type], N'0' AS [Mandatory], N'' AS [DefaultValue], N'' AS [ConstructionPattern], GETDATE() AS [Created], N'DAEC4CAF-FC90-4226-94CA-41D755905C44' AS [MasterID]
        FROM [list].[ItemListDataSource] [ds]
        WHERE [ds].[MasterID] = '9DB9B94B-D050-44D5-AD3A-4913AF1442C9';
        DECLARE @ParamID_2784 nvarchar(32)= SCOPE_IDENTITY();

        INSERT INTO [list].[LT_ItemListDataParameter] ([LanguageID], [ItemListDataParameterID], [Name], [Description])
        SELECT l.[LanguageID], @ParamID_2784 AS [ItemListDataParameterID], N'UserTypeGradeId' AS [Name], N'' AS [Description]
        FROM [dbo].[Language] l
        WHERE l.[LanguageID] IN (1,2,7);
    END;

    DECLARE @QuestionIds_15543132239281 nvarchar(32)= '';
    SELECT @QuestionIds_15543132239281 = STUFF( (SELECT ','+CAST([q].[QuestionID] AS nvarchar(32))
                                                 FROM [at].[Question] [q]
                                                 WHERE [q].[MasterID] IN ('85EA23C0-705D-4362-9C14-941F12D744C2')
                                                 ORDER BY CHARINDEX(CAST([q].[MasterID] AS nvarchar(64)), N'85EA23C0-705D-4362-9C14-941F12D744C2')
                                                 FOR XML PATH, TYPE
                                                ) .value ('.[1]', 'nvarchar(max)'), 1, 1, '');
    IF NOT EXISTS (SELECT 1 FROM [list].[ItemListDataParameter] [dp] WHERE [dp].[MasterID] = '3C7ED5AF-8DF3-49F4-9315-A55B6C49B277')
    BEGIN
        INSERT INTO [list].[ItemListDataParameter] ([ItemListDataSourceID], [DataType], [Type], [Mandatory], [DefaultValue], [ConstructionPattern], [Created], [MasterID])
        SELECT [ds].[ItemListDataSourceID] AS [ItemListDataSourceID], N'String' AS [DataType], N'PortalPartParam' AS [Type], N'0' AS [Mandatory], N''+@QuestionIds_15543132239281+'' AS [DefaultValue], N'' AS [ConstructionPattern], GETDATE() AS [Created], N'3C7ED5AF-8DF3-49F4-9315-A55B6C49B277' AS [MasterID]
        FROM [list].[ItemListDataSource] [ds]
        WHERE [ds].[MasterID] = '9DB9B94B-D050-44D5-AD3A-4913AF1442C9';
        DECLARE @ParamID_2785 nvarchar(32)= SCOPE_IDENTITY();

        INSERT INTO [list].[LT_ItemListDataParameter] ([LanguageID], [ItemListDataParameterID], [Name], [Description])
        SELECT l.[LanguageID], @ParamID_2785 AS [ItemListDataParameterID], N'SummaryQuestionIds' AS [Name], N'' AS [Description]
        FROM [dbo].[Language] l
        WHERE l.[LanguageID] IN (1,2,7);
    END;

    DECLARE @QuestionIds_15543027538361 nvarchar(32)= '';
    SELECT @QuestionIds_15543027538361 = STUFF( (SELECT ','+CAST([q].[QuestionID] AS nvarchar(32))
                                                 FROM [at].[Question] [q]
                                                 WHERE [q].[MasterID] IN ('C4CF1481-18F8-4447-BB8E-0FC02E18C9B1', 'DF914DD2-941F-45A0-924A-DCFB67281FC3', '7160E01A-6E37-4BBA-931B-5C782416A515', 'CF4E288E-16E9-4B64-BBCE-6CC13E7C3CD4')
                                                 ORDER BY CHARINDEX(CAST([q].[MasterID] AS nvarchar(64)), N'C4CF1481-18F8-4447-BB8E-0FC02E18C9B1,DF914DD2-941F-45A0-924A-DCFB67281FC3,7160E01A-6E37-4BBA-931B-5C782416A515,CF4E288E-16E9-4B64-BBCE-6CC13E7C3CD4')
                                                 FOR XML PATH, TYPE
                                                ) .value ('.[1]', 'nvarchar(max)'), 1, 1, '');
    IF NOT EXISTS (SELECT 1 FROM [list].[ItemListDataParameter] [dp] WHERE [dp].[MasterID] = '906EB681-10F6-4E3F-B170-055CFDFAEBCF')
    BEGIN
        INSERT INTO [list].[ItemListDataParameter] ([ItemListDataSourceID], [DataType], [Type], [Mandatory], [DefaultValue], [ConstructionPattern], [Created], [MasterID])
        SELECT [ds].[ItemListDataSourceID] AS [ItemListDataSourceID], N'String' AS [DataType], N'PortalPartParam' AS [Type], N'0' AS [Mandatory], N''+@QuestionIds_15543027538361+'' AS [DefaultValue], N'' AS [ConstructionPattern], GETDATE() AS [Created], N'906EB681-10F6-4E3F-B170-055CFDFAEBCF' AS [MasterID]
        FROM [list].[ItemListDataSource] [ds]
        WHERE [ds].[MasterID] = '9DB9B94B-D050-44D5-AD3A-4913AF1442C9';
        DECLARE @ParamID_2786 nvarchar(32)= SCOPE_IDENTITY();

        INSERT INTO [list].[LT_ItemListDataParameter] ([LanguageID], [ItemListDataParameterID], [Name], [Description])
        SELECT l.[LanguageID], @ParamID_2786 AS [ItemListDataParameterID], N'RepeatedQuestionIds' AS [Name], N'' AS [Description]
        FROM [dbo].[Language] l
        WHERE l.[LanguageID] IN (1,2,7);
    END;

    DECLARE @CategoryIds_15542924412641 nvarchar(32)= '';
    SELECT @CategoryIds_15542924412641 = STUFF( (SELECT ','+CAST([c].[CategoryID] AS nvarchar(32))
                                                 FROM [at].[Category] [c]
                                                 WHERE [c].[MasterID] IN ('50768261-AE12-4528-B86C-84EC27467712', 'CDD1ED1A-7375-46AC-9197-566B1B5747C1', '16D9297E-B833-484E-976E-8BFA8D64D749')
                                                 ORDER BY CHARINDEX(CAST([c].[MasterID] AS nvarchar(64)), N'50768261-AE12-4528-B86C-84EC27467712,CDD1ED1A-7375-46AC-9197-566B1B5747C1,16D9297E-B833-484E-976E-8BFA8D64D749')
                                                 FOR XML PATH, TYPE
                                                ) .value ('.[1]', 'nvarchar(max)'), 1, 1, '');
    IF NOT EXISTS (SELECT 1 FROM [list].[ItemListDataParameter] [dp] WHERE [dp].[MasterID] = '5E2452CD-C770-4672-B344-FF0FF6329795')
    BEGIN
        INSERT INTO [list].[ItemListDataParameter] ([ItemListDataSourceID], [DataType], [Type], [Mandatory], [DefaultValue], [ConstructionPattern], [Created], [MasterID])
        SELECT [ds].[ItemListDataSourceID] AS [ItemListDataSourceID], N'String' AS [DataType], N'PortalPartParam' AS [Type], N'0' AS [Mandatory], N''+@CategoryIds_15542924412641+'' AS [DefaultValue], N'' AS [ConstructionPattern], GETDATE() AS [Created], N'5E2452CD-C770-4672-B344-FF0FF6329795' AS [MasterID]
        FROM [list].[ItemListDataSource] [ds]
        WHERE [ds].[MasterID] = '9DB9B94B-D050-44D5-AD3A-4913AF1442C9';
        DECLARE @ParamID_2787 nvarchar(32)= SCOPE_IDENTITY();

        INSERT INTO [list].[LT_ItemListDataParameter] ([LanguageID], [ItemListDataParameterID], [Name], [Description])
        SELECT l.[LanguageID], @ParamID_2787 AS [ItemListDataParameterID], N'listCategoryIds' AS [Name], N'' AS [Description]
        FROM [dbo].[Language] l
        WHERE l.[LanguageID] IN (1,2,7);
    END;

    IF NOT EXISTS (SELECT 1 FROM [list].[ItemListDataParameter] [dp] WHERE [dp].[MasterID] = '3DCC7A3B-59F6-46EC-94E2-9430BE95AD50')
    BEGIN
        INSERT INTO [list].[ItemListDataParameter] ([ItemListDataSourceID], [DataType], [Type], [Mandatory], [DefaultValue], [ConstructionPattern], [Created], [MasterID])
        SELECT [ds].[ItemListDataSourceID] AS [ItemListDataSourceID], N'Integer' AS [DataType], N'PortalPartParam' AS [Type], N'0' AS [Mandatory], N''+'37'+'' AS [DefaultValue], N'' AS [ConstructionPattern], GETDATE() AS [Created], N'3DCC7A3B-59F6-46EC-94E2-9430BE95AD50' AS [MasterID]
        FROM [list].[ItemListDataSource] [ds]
        WHERE [ds].[MasterID] = '9DB9B94B-D050-44D5-AD3A-4913AF1442C9';
        DECLARE @ParamID_2788 nvarchar(32)= SCOPE_IDENTITY();

        INSERT INTO [list].[LT_ItemListDataParameter] ([LanguageID], [ItemListDataParameterID], [Name], [Description])
        SELECT l.[LanguageID], @ParamID_2788 AS [ItemListDataParameterID], N'usertypegradeid' AS [Name], N'' AS [Description]
        FROM [dbo].[Language] l
        WHERE l.[LanguageID] IN (1,2,7);
    END;

    IF NOT EXISTS (SELECT 1 FROM [list].[ItemListDataParameter] [dp] WHERE [dp].[MasterID] = '795B06D2-316D-418F-A686-7B0D16B519D8')
    BEGIN
        INSERT INTO [list].[ItemListDataParameter] ([ItemListDataSourceID], [DataType], [Type], [Mandatory], [DefaultValue], [ConstructionPattern], [Created], [MasterID])
        SELECT [ds].[ItemListDataSourceID] AS [ItemListDataSourceID], N'Integer' AS [DataType], N'PortalPartParam' AS [Type], N'0' AS [Mandatory], N''+'1'+'' AS [DefaultValue], N'' AS [ConstructionPattern], GETDATE() AS [Created], N'795B06D2-316D-418F-A686-7B0D16B519D8' AS [MasterID]
        FROM [list].[ItemListDataSource] [ds]
        WHERE [ds].[MasterID] = '9DB9B94B-D050-44D5-AD3A-4913AF1442C9';
        DECLARE @ParamID_2789 nvarchar(32)= SCOPE_IDENTITY();

        INSERT INTO [list].[LT_ItemListDataParameter] ([LanguageID], [ItemListDataParameterID], [Name], [Description])
        SELECT l.[LanguageID], @ParamID_2789 AS [ItemListDataParameterID], N'groupType' AS [Name], N'' AS [Description]
        FROM [dbo].[Language] l
        WHERE l.[LanguageID] IN (1,2,7);
    END;

    IF NOT EXISTS (SELECT 1 FROM [list].[ItemListDataParameter] [dp] WHERE [dp].[MasterID] = '6B5F8465-6E5D-425C-AACC-2E03D887FC40')
    BEGIN
        INSERT INTO [list].[ItemListDataParameter] ([ItemListDataSourceID], [DataType], [Type], [Mandatory], [DefaultValue], [ConstructionPattern], [Created], [MasterID])
        SELECT [ds].[ItemListDataSourceID] AS [ItemListDataSourceID], N'Integer' AS [DataType], N'DynamicallyAddedPortalPartParam' AS [Type], N'0' AS [Mandatory], N'' AS [DefaultValue], N'' AS [ConstructionPattern], GETDATE() AS [Created], N'6B5F8465-6E5D-425C-AACC-2E03D887FC40' AS [MasterID]
        FROM [list].[ItemListDataSource] [ds]
        WHERE [ds].[MasterID] = '9DB9B94B-D050-44D5-AD3A-4913AF1442C9';
        DECLARE @ParamID_2790 nvarchar(32)= SCOPE_IDENTITY();

        INSERT INTO [list].[LT_ItemListDataParameter] ([LanguageID], [ItemListDataParameterID], [Name], [Description])
        SELECT l.[LanguageID], @ParamID_2790 AS [ItemListDataParameterID], N'miid' AS [Name], N'' AS [Description]
        FROM [dbo].[Language] l
        WHERE l.[LanguageID] IN (1,2,7);
    END;

    DECLARE @PortalPageID_3823_15542611913321 nvarchar(32)= '';
    SELECT @PortalPageID_3823_15542611913321 = [pp].[PortalPageID]
    FROM [app].[PortalPage] [pp]
    WHERE [pp].[MasterID] = '92CAA5F6-D10C-457A-8598-626114B96FD8';
    IF NOT EXISTS (SELECT 1 FROM [list].[ItemListDataParameter] [dp] WHERE [dp].[MasterID] = '5F2B6E93-B7A5-4ACD-80A7-49CB71D7DD66')
    BEGIN
        INSERT INTO [list].[ItemListDataParameter] ([ItemListDataSourceID], [DataType], [Type], [Mandatory], [DefaultValue], [ConstructionPattern], [Created], [MasterID])
        SELECT [ds].[ItemListDataSourceID] AS [ItemListDataSourceID], N'Integer' AS [DataType], N'PortalPartParam' AS [Type], N'0' AS [Mandatory], N''+@PortalPageID_3823_15542611913321+'' AS [DefaultValue], N'' AS [ConstructionPattern], GETDATE() AS [Created], N'5F2B6E93-B7A5-4ACD-80A7-49CB71D7DD66' AS [MasterID]
        FROM [list].[ItemListDataSource] [ds]
        WHERE [ds].[MasterID] = '9DB9B94B-D050-44D5-AD3A-4913AF1442C9';
        DECLARE @ParamID_2792 nvarchar(32)= SCOPE_IDENTITY();

        INSERT INTO [list].[LT_ItemListDataParameter] ([LanguageID], [ItemListDataParameterID], [Name], [Description])
        SELECT l.[LanguageID], @ParamID_2792 AS [ItemListDataParameterID], N'RegistrationPortalPageId' AS [Name], N'' AS [Description]
        FROM [dbo].[Language] l
        WHERE l.[LanguageID] IN (1,2,7);
    END;

    IF NOT EXISTS (SELECT 1 FROM [list].[ItemListDataParameter] [dp] WHERE [dp].[MasterID] = '6C73C6CA-6D30-4E9F-9B34-74E7246AF062')
    BEGIN
        INSERT INTO [list].[ItemListDataParameter] ([ItemListDataSourceID], [DataType], [Type], [Mandatory], [DefaultValue], [ConstructionPattern], [Created], [MasterID])
        SELECT [ds].[ItemListDataSourceID] AS [ItemListDataSourceID], N'Integer' AS [DataType], N'DynamicallyAddedPortalPartParam' AS [Type], N'0' AS [Mandatory], N'' AS [DefaultValue], N'' AS [ConstructionPattern], GETDATE() AS [Created], N'6C73C6CA-6D30-4E9F-9B34-74E7246AF062' AS [MasterID]
        FROM [list].[ItemListDataSource] [ds]
        WHERE [ds].[MasterID] = '9DB9B94B-D050-44D5-AD3A-4913AF1442C9';
        DECLARE @ParamID_3794 nvarchar(32)= SCOPE_IDENTITY();

        INSERT INTO [list].[LT_ItemListDataParameter] ([LanguageID], [ItemListDataParameterID], [Name], [Description])
        SELECT l.[LanguageID], @ParamID_3794 AS [ItemListDataParameterID], N'Submiid' AS [Name], N'' AS [Description]
        FROM [dbo].[Language] l
        WHERE l.[LanguageID] IN (1,2,7);
    END;

    IF NOT EXISTS (SELECT 1 FROM [list].[ItemListDataParameter] [dp] WHERE [dp].[MasterID] = '7EEAB8FC-3FD4-4B49-9D22-56EF8A248431')
    BEGIN
        INSERT INTO [list].[ItemListDataParameter] ([ItemListDataSourceID], [DataType], [Type], [Mandatory], [DefaultValue], [ConstructionPattern], [Created], [MasterID])
        SELECT [ds].[ItemListDataSourceID] AS [ItemListDataSourceID], N'Integer' AS [DataType], N'DynamicallyAddedPortalPartParam' AS [Type], N'0' AS [Mandatory], N'' AS [DefaultValue], N'' AS [ConstructionPattern], GETDATE() AS [Created], N'7EEAB8FC-3FD4-4B49-9D22-56EF8A248431' AS [MasterID]
        FROM [list].[ItemListDataSource] [ds]
        WHERE [ds].[MasterID] = '9DB9B94B-D050-44D5-AD3A-4913AF1442C9';
        DECLARE @ParamID_3797 nvarchar(32)= SCOPE_IDENTITY();

        INSERT INTO [list].[LT_ItemListDataParameter] ([LanguageID], [ItemListDataParameterID], [Name], [Description])
        SELECT l.[LanguageID], @ParamID_3797 AS [ItemListDataParameterID], N'DepartmentId' AS [Name], N'' AS [Description]
        FROM [dbo].[Language] l
        WHERE l.[LanguageID] IN (1,2,7);
    END;

END;
--#endregion DataSource 2161

--#region PortalPage 3896
IF NOT EXISTS (SELECT 1 FROM [app].[PortalPage] [pp] WHERE [pp].[MasterID] = '3F1F5E61-10A3-446B-98BC-8860215814CE')
BEGIN
    INSERT INTO [app].[PortalPage] ([OwnerID], [No], [Parameter], [ExtID], [Created], [MasterID])
    SELECT [o].[OwnerID] AS [OwnerID], N'134' AS [No], N'RenderToolbar=false&rendername=false&cssClass=clearfix top-padding-content container-userlist' AS [Parameter], N'' AS [ExtID], GETDATE() AS [Created], N'3F1F5E61-10A3-446B-98BC-8860215814CE' AS [MasterID]
    FROM [org].[Owner] [o]
    WHERE [o].[MasterID] = '1C483E02-E808-42A7-8210-B11F4051D2E4';
    DECLARE @PortalPageID_3896 nvarchar(32)= SCOPE_IDENTITY();

    INSERT INTO [app].[LT_PortalPage] ([LanguageID], [PortalPageID], [Name], [Description])
    SELECT l.[LanguageID], @PortalPageID_3896 AS [PortalPageID], N'[VGO User List View] mass registration' AS [Name], N'' AS [Description]
    FROM [dbo].[Language] l
    WHERE l.[LanguageID] IN (1,2,7);

    DECLARE @DataSourceID_1098_16124501116632 nvarchar(32)= '';
    SELECT @DataSourceID_1098_16124501116632 = [ds].[ItemListDataSourceID]
    FROM [list].[ItemListDataSource] [ds]
    WHERE [ds].[MasterID] = '084410EE-D546-46E2-A1F6-7C3D0DFDA550';
    IF NOT EXISTS (SELECT 1 FROM [app].[PortalPart] [pt] WHERE [pt].[MasterID] = '999DB5E4-9068-400C-8A03-2F435FF99035')
    BEGIN
        INSERT INTO [app].[PortalPart] ([PortalPageID], [PortalPartTypeID], [No], [CategoryID], [Settings], [BubbleID], [ReportPartID], [ReportID], [CssClass], [InlineStyle], [ActivityID], [SurveyID], [ExtID], [MasterID])
        SELECT [pp].[PortalPageID] AS [PortalPageID], N'43' AS [PortalPartTypeID], N'1' AS [No], NULL AS [CategoryID], N'<params>isemptyhtml%3D'+'false'+'%26DataSourceID%3D'+@DataSourceID_1098_16124501116632+'%26RenderIfEmpty%3D'+'false'+'</params>' AS [Settings], NULL AS [BubbleID], NULL AS [ReportPartID], NULL AS [ReportID], N'col-xs-12 container-history-selector' AS [CssClass], N'' AS [InlineStyle], NULL AS [ActivityID], NULL AS [SurveyID], N'' AS [ExtID], N'999DB5E4-9068-400C-8A03-2F435FF99035' AS [MasterID]
        FROM [app].[PortalPage] [pp]
        WHERE [pp].[MasterID] = '3F1F5E61-10A3-446B-98BC-8860215814CE';
        DECLARE @PortalPartID_5625 nvarchar(32)= SCOPE_IDENTITY();

        INSERT INTO [app].[LT_PortalPart] ([LanguageID], [PortalPartID], [Name], [Description], [Text], [TextAbove], [TextBelow], [HeaderText], [FooterText], [BeforeContentText], [AfterContentText])
        SELECT l.[LanguageID], @PortalPartID_5625 AS [PortalPartID], N'[History data] View result' AS [Name], N'' AS [Description], N'' AS [Text], N'' AS [TextAbove], N'' AS [TextBelow], N'' AS [HeaderText], N'' AS [FooterText], N'' AS [BeforeContentText], N'' AS [AfterContentText]
        FROM [dbo].[Language] l
        WHERE l.[LanguageID] IN (1,2,7);
    END;

    DECLARE @DataSourceID_96_16124396424321 nvarchar(32)= '';
    SELECT @DataSourceID_96_16124396424321 = [ds].[ItemListDataSourceID]
    FROM [list].[ItemListDataSource] [ds]
    WHERE [ds].[MasterID] = '1A5C8D71-7B44-41EB-A60E-1C141E881853';
    IF NOT EXISTS (SELECT 1 FROM [app].[PortalPart] [pt] WHERE [pt].[MasterID] = 'DA5C0C24-5684-497D-AEE2-99F38DDAEBB5')
    BEGIN
        INSERT INTO [app].[PortalPart] ([PortalPageID], [PortalPartTypeID], [No], [CategoryID], [Settings], [BubbleID], [ReportPartID], [ReportID], [CssClass], [InlineStyle], [ActivityID], [SurveyID], [ExtID], [MasterID])
        SELECT [pp].[PortalPageID] AS [PortalPageID], N'43' AS [PortalPartTypeID], N'2' AS [No], NULL AS [CategoryID], N'<params>DataSourceId%3D'+@DataSourceID_96_16124396424321+'</params>' AS [Settings], NULL AS [BubbleID], NULL AS [ReportPartID], NULL AS [ReportID], N'col-xs-12 container-list-user-department' AS [CssClass], N'' AS [InlineStyle], NULL AS [ActivityID], NULL AS [SurveyID], N'' AS [ExtID], N'DA5C0C24-5684-497D-AEE2-99F38DDAEBB5' AS [MasterID]
        FROM [app].[PortalPage] [pp]
        WHERE [pp].[MasterID] = '3F1F5E61-10A3-446B-98BC-8860215814CE';
        DECLARE @PortalPartID_5626 nvarchar(32)= SCOPE_IDENTITY();

        INSERT INTO [app].[LT_PortalPart] ([LanguageID], [PortalPartID], [Name], [Description], [Text], [TextAbove], [TextBelow], [HeaderText], [FooterText], [BeforeContentText], [AfterContentText])
        SELECT l.[LanguageID], @PortalPartID_5626 AS [PortalPartID], N'List usergroup' AS [Name], N'' AS [Description], N'' AS [Text], N'' AS [TextAbove], N'' AS [TextBelow], N'' AS [HeaderText], N'' AS [FooterText], N'' AS [BeforeContentText], N'' AS [AfterContentText]
        FROM [dbo].[Language] l
        WHERE l.[LanguageID] IN (1,2,7);
    END;

    DECLARE @DataSourceID_2161_16124291733942 nvarchar(32)= '';
    SELECT @DataSourceID_2161_16124291733942 = [ds].[ItemListDataSourceID]
    FROM [list].[ItemListDataSource] [ds]
    WHERE [ds].[MasterID] = '9DB9B94B-D050-44D5-AD3A-4913AF1442C9';
    IF NOT EXISTS (SELECT 1 FROM [app].[PortalPart] [pt] WHERE [pt].[MasterID] = '71E55B8F-85B0-4165-A08F-3BC4785E60CC')
    BEGIN
        INSERT INTO [app].[PortalPart] ([PortalPageID], [PortalPartTypeID], [No], [CategoryID], [Settings], [BubbleID], [ReportPartID], [ReportID], [CssClass], [InlineStyle], [ActivityID], [SurveyID], [ExtID], [MasterID])
        SELECT [pp].[PortalPageID] AS [PortalPageID], N'43' AS [PortalPartTypeID], N'3' AS [No], NULL AS [CategoryID], N'<params>isemptyhtml%3D'+'true'+'%26DataSourceID%3D'+@DataSourceID_2161_16124291733942+'</params>' AS [Settings], NULL AS [BubbleID], NULL AS [ReportPartID], NULL AS [ReportID], N'col-xs-12 container-list-user-content tbl-container' AS [CssClass], N'' AS [InlineStyle], NULL AS [ActivityID], NULL AS [SurveyID], N'' AS [ExtID], N'71E55B8F-85B0-4165-A08F-3BC4785E60CC' AS [MasterID]
        FROM [app].[PortalPage] [pp]
        WHERE [pp].[MasterID] = '3F1F5E61-10A3-446B-98BC-8860215814CE';
        DECLARE @PortalPartID_5627 nvarchar(32)= SCOPE_IDENTITY();

        INSERT INTO [app].[LT_PortalPart] ([LanguageID], [PortalPartID], [Name], [Description], [Text], [TextAbove], [TextBelow], [HeaderText], [FooterText], [BeforeContentText], [AfterContentText])
        SELECT l.[LanguageID], @PortalPartID_5627 AS [PortalPartID], N'User list view' AS [Name], N'' AS [Description], N'' AS [Text], N'' AS [TextAbove], N'' AS [TextBelow], N'' AS [HeaderText], N'' AS [FooterText], N'' AS [BeforeContentText], N'' AS [AfterContentText]
        FROM [dbo].[Language] l
        WHERE l.[LanguageID] IN (1,2,7);
    END;

END;
--#endregion PortalPage 3896

--#region DataSource 2160
IF NOT EXISTS (SELECT 1 FROM [list].[ItemListDataSource] [ds] WHERE [ds].[MasterID] = '3E8E8988-573D-48FF-9263-E357DF69636E')
BEGIN
    INSERT INTO [list].[ItemListDataSource] ([OwnerID], [FunctionName], [Created], [MasterID])
    SELECT [o].[OwnerID] AS [OwnerID], N'GetVGOUserListView' AS [FunctionName], GETDATE() AS [Created], N'3E8E8988-573D-48FF-9263-E357DF69636E' AS [MasterID]
    FROM [org].[Owner] [o]
    WHERE [o].[MasterID] = '1C483E02-E808-42A7-8210-B11F4051D2E4';
    DECLARE @ItemListDataSourceID_2160 nvarchar(32)= SCOPE_IDENTITY();

    INSERT INTO [list].[LT_ItemListDataSource] ([LanguageID], [ItemListDataSourceID], [Name], [Description])
    SELECT l.[LanguageID], @ItemListDataSourceID_2160 AS [ItemListDataSourceID], N'[Vgo Userlist] List UserGroups' AS [Name], N'' AS [Description]
    FROM [dbo].[Language] l
    WHERE l.[LanguageID] IN (1,2,7);

    IF NOT EXISTS (SELECT 1 FROM [list].[ItemListDataParameter] [dp] WHERE [dp].[MasterID] = '6DF3D1E7-6723-4C1F-8D45-8C3D0F051D34')
    BEGIN
        INSERT INTO [list].[ItemListDataParameter] ([ItemListDataSourceID], [DataType], [Type], [Mandatory], [DefaultValue], [ConstructionPattern], [Created], [MasterID])
        SELECT [ds].[ItemListDataSourceID] AS [ItemListDataSourceID], N'String' AS [DataType], N'PortalPartParam' AS [Type], N'0' AS [Mandatory], N''+'IkoUsergroupList'+'' AS [DefaultValue], N'' AS [ConstructionPattern], GETDATE() AS [Created], N'6DF3D1E7-6723-4C1F-8D45-8C3D0F051D34' AS [MasterID]
        FROM [list].[ItemListDataSource] [ds]
        WHERE [ds].[MasterID] = '3E8E8988-573D-48FF-9263-E357DF69636E';
        DECLARE @ParamID_2769 nvarchar(32)= SCOPE_IDENTITY();

        INSERT INTO [list].[LT_ItemListDataParameter] ([LanguageID], [ItemListDataParameterID], [Name], [Description])
        SELECT l.[LanguageID], @ParamID_2769 AS [ItemListDataParameterID], N'ViewName' AS [Name], N'' AS [Description]
        FROM [dbo].[Language] l
        WHERE l.[LanguageID] IN (1,2,7);
    END;

    IF NOT EXISTS (SELECT 1 FROM [list].[ItemListDataParameter] [dp] WHERE [dp].[MasterID] = '87AC8336-92E4-4861-B942-9455CB3C571B')
    BEGIN
        INSERT INTO [list].[ItemListDataParameter] ([ItemListDataSourceID], [DataType], [Type], [Mandatory], [DefaultValue], [ConstructionPattern], [Created], [MasterID])
        SELECT [ds].[ItemListDataSourceID] AS [ItemListDataSourceID], N'Integer' AS [DataType], N'DynamicallyAddedPortalPartParam' AS [Type], N'0' AS [Mandatory], N'' AS [DefaultValue], N'' AS [ConstructionPattern], GETDATE() AS [Created], N'87AC8336-92E4-4861-B942-9455CB3C571B' AS [MasterID]
        FROM [list].[ItemListDataSource] [ds]
        WHERE [ds].[MasterID] = '3E8E8988-573D-48FF-9263-E357DF69636E';
        DECLARE @ParamID_2771 nvarchar(32)= SCOPE_IDENTITY();

        INSERT INTO [list].[LT_ItemListDataParameter] ([LanguageID], [ItemListDataParameterID], [Name], [Description])
        SELECT l.[LanguageID], @ParamID_2771 AS [ItemListDataParameterID], N'miid' AS [Name], N'' AS [Description]
        FROM [dbo].[Language] l
        WHERE l.[LanguageID] IN (1,2,7);
    END;

    IF NOT EXISTS (SELECT 1 FROM [list].[ItemListDataParameter] [dp] WHERE [dp].[MasterID] = 'BF8C86DB-80D1-44D6-8C69-D234B7D7865D')
    BEGIN
        INSERT INTO [list].[ItemListDataParameter] ([ItemListDataSourceID], [DataType], [Type], [Mandatory], [DefaultValue], [ConstructionPattern], [Created], [MasterID])
        SELECT [ds].[ItemListDataSourceID] AS [ItemListDataSourceID], N'Integer' AS [DataType], N'DynamicallyAddedPortalPartParam' AS [Type], N'0' AS [Mandatory], N'' AS [DefaultValue], N'' AS [ConstructionPattern], GETDATE() AS [Created], N'BF8C86DB-80D1-44D6-8C69-D234B7D7865D' AS [MasterID]
        FROM [list].[ItemListDataSource] [ds]
        WHERE [ds].[MasterID] = '3E8E8988-573D-48FF-9263-E357DF69636E';
        DECLARE @ParamID_2772 nvarchar(32)= SCOPE_IDENTITY();

        INSERT INTO [list].[LT_ItemListDataParameter] ([LanguageID], [ItemListDataParameterID], [Name], [Description])
        SELECT l.[LanguageID], @ParamID_2772 AS [ItemListDataParameterID], N'submiid' AS [Name], N'' AS [Description]
        FROM [dbo].[Language] l
        WHERE l.[LanguageID] IN (1,2,7);
    END;

    IF NOT EXISTS (SELECT 1 FROM [list].[ItemListDataParameter] [dp] WHERE [dp].[MasterID] = 'DC32FB26-551B-4DA9-B96D-0101678A02D6')
    BEGIN
        INSERT INTO [list].[ItemListDataParameter] ([ItemListDataSourceID], [DataType], [Type], [Mandatory], [DefaultValue], [ConstructionPattern], [Created], [MasterID])
        SELECT [ds].[ItemListDataSourceID] AS [ItemListDataSourceID], N'Integer' AS [DataType], N'PortalPartParam' AS [Type], N'0' AS [Mandatory], N''+'1'+'' AS [DefaultValue], N'' AS [ConstructionPattern], GETDATE() AS [Created], N'DC32FB26-551B-4DA9-B96D-0101678A02D6' AS [MasterID]
        FROM [list].[ItemListDataSource] [ds]
        WHERE [ds].[MasterID] = '3E8E8988-573D-48FF-9263-E357DF69636E';
        DECLARE @ParamID_2773 nvarchar(32)= SCOPE_IDENTITY();

        INSERT INTO [list].[LT_ItemListDataParameter] ([LanguageID], [ItemListDataParameterID], [Name], [Description])
        SELECT l.[LanguageID], @ParamID_2773 AS [ItemListDataParameterID], N'groupType' AS [Name], N'' AS [Description]
        FROM [dbo].[Language] l
        WHERE l.[LanguageID] IN (1,2,7);
    END;

    DECLARE @PortalPartID_5627_15403679149701 nvarchar(32)= '';
    SELECT @PortalPartID_5627_15403679149701 = [pt].[PortalPartID]
    FROM [app].[PortalPart] [pt]
    WHERE [pt].[MasterID] = '71E55B8F-85B0-4165-A08F-3BC4785E60CC';
    IF NOT EXISTS (SELECT 1 FROM [list].[ItemListDataParameter] [dp] WHERE [dp].[MasterID] = '82121B4E-57F4-40F9-86BC-2131035ABADD')
    BEGIN
        INSERT INTO [list].[ItemListDataParameter] ([ItemListDataSourceID], [DataType], [Type], [Mandatory], [DefaultValue], [ConstructionPattern], [Created], [MasterID])
        SELECT [ds].[ItemListDataSourceID] AS [ItemListDataSourceID], N'Integer' AS [DataType], N'PortalPartParam' AS [Type], N'0' AS [Mandatory], N''+@PortalPartID_5627_15403679149701+'' AS [DefaultValue], N'' AS [ConstructionPattern], GETDATE() AS [Created], N'82121B4E-57F4-40F9-86BC-2131035ABADD' AS [MasterID]
        FROM [list].[ItemListDataSource] [ds]
        WHERE [ds].[MasterID] = '3E8E8988-573D-48FF-9263-E357DF69636E';
        DECLARE @ParamID_2774 nvarchar(32)= SCOPE_IDENTITY();

        INSERT INTO [list].[LT_ItemListDataParameter] ([LanguageID], [ItemListDataParameterID], [Name], [Description])
        SELECT l.[LanguageID], @ParamID_2774 AS [ItemListDataParameterID], N'multiListportalPartId' AS [Name], N'' AS [Description]
        FROM [dbo].[Language] l
        WHERE l.[LanguageID] IN (1,2,7);
    END;

    IF NOT EXISTS (SELECT 1 FROM [list].[ItemListDataParameter] [dp] WHERE [dp].[MasterID] = 'A71CFEE5-D000-425A-9489-50C1F422C609')
    BEGIN
        INSERT INTO [list].[ItemListDataParameter] ([ItemListDataSourceID], [DataType], [Type], [Mandatory], [DefaultValue], [ConstructionPattern], [Created], [MasterID])
        SELECT [ds].[ItemListDataSourceID] AS [ItemListDataSourceID], N'Integer' AS [DataType], N'DynamicallyAddedPortalPartParam' AS [Type], N'0' AS [Mandatory], N'' AS [DefaultValue], N'' AS [ConstructionPattern], GETDATE() AS [Created], N'A71CFEE5-D000-425A-9489-50C1F422C609' AS [MasterID]
        FROM [list].[ItemListDataSource] [ds]
        WHERE [ds].[MasterID] = '3E8E8988-573D-48FF-9263-E357DF69636E';
        DECLARE @ParamID_2775 nvarchar(32)= SCOPE_IDENTITY();

        INSERT INTO [list].[LT_ItemListDataParameter] ([LanguageID], [ItemListDataParameterID], [Name], [Description])
        SELECT l.[LanguageID], @ParamID_2775 AS [ItemListDataParameterID], N'usergroupid' AS [Name], N'' AS [Description]
        FROM [dbo].[Language] l
        WHERE l.[LanguageID] IN (1,2,7);
    END;

END;
--#endregion DataSource 2160

--#region DataSource 89
DECLARE @PortalPageID_3896_16203043263161 nvarchar(32)= '';
SELECT @PortalPageID_3896_16203043263161 = [pp].[PortalPageID]
FROM [app].[PortalPage] [pp]
WHERE [pp].[MasterID] = '3F1F5E61-10A3-446B-98BC-8860215814CE';
IF NOT EXISTS (SELECT 1 FROM [list].[ItemListDataParameter] [dp] WHERE [dp].[MasterID] = 'C102B066-CA4E-4043-8291-DED7F01F6F6D')
BEGIN
    INSERT INTO [list].[ItemListDataParameter] ([ItemListDataSourceID], [DataType], [Type], [Mandatory], [DefaultValue], [ConstructionPattern], [Created], [MasterID])
    SELECT [ds].[ItemListDataSourceID] AS [ItemListDataSourceID], N'Integer' AS [DataType], N'PortalPartParam' AS [Type], N'0' AS [Mandatory], N''+@PortalPageID_3896_16203043263161+'' AS [DefaultValue], N'' AS [ConstructionPattern], GETDATE() AS [Created], N'C102B066-CA4E-4043-8291-DED7F01F6F6D' AS [MasterID]
    FROM [list].[ItemListDataSource] [ds]
    WHERE [ds].[MasterID] = '9196CFF3-6E58-4CEF-9532-A63F1BF5BF95';
    DECLARE @ParamID_3795 nvarchar(32)= SCOPE_IDENTITY();

    INSERT INTO [list].[LT_ItemListDataParameter] ([LanguageID], [ItemListDataParameterID], [Name], [Description])
    SELECT l.[LanguageID], @ParamID_3795 AS [ItemListDataParameterID], N'VgoUserListViewPortalpageId' AS [Name], N'' AS [Description]
    FROM [dbo].[Language] l
    WHERE l.[LanguageID] IN (1,2,7);
END;

IF NOT EXISTS (SELECT 1 FROM [list].[ItemListDataParameter] [dp] WHERE [dp].[MasterID] = 'C0F5E4AA-885D-401C-9DEE-5D1CDF72D33F')
BEGIN
    INSERT INTO [list].[ItemListDataParameter] ([ItemListDataSourceID], [DataType], [Type], [Mandatory], [DefaultValue], [ConstructionPattern], [Created], [MasterID])
    SELECT [ds].[ItemListDataSourceID] AS [ItemListDataSourceID], N'Integer' AS [DataType], N'DynamicallyAddedPortalPartParam' AS [Type], N'0' AS [Mandatory], N'' AS [DefaultValue], N'' AS [ConstructionPattern], GETDATE() AS [Created], N'C0F5E4AA-885D-401C-9DEE-5D1CDF72D33F' AS [MasterID]
    FROM [list].[ItemListDataSource] [ds]
    WHERE [ds].[MasterID] = '9196CFF3-6E58-4CEF-9532-A63F1BF5BF95';
    DECLARE @ParamID_3798 nvarchar(32)= SCOPE_IDENTITY();

    INSERT INTO [list].[LT_ItemListDataParameter] ([LanguageID], [ItemListDataParameterID], [Name], [Description])
    SELECT l.[LanguageID], @ParamID_3798 AS [ItemListDataParameterID], N'activityid' AS [Name], N'' AS [Description]
    FROM [dbo].[Language] l
    WHERE l.[LanguageID] IN (1,2,7);
END;

DECLARE @PortalPartID_4438_10221606103571 nvarchar(32)= '';
SELECT @PortalPartID_4438_10221606103571 = [pt].[PortalPartID]
FROM [app].[PortalPart] [pt]
WHERE [pt].[MasterID] = '16B63CAF-5533-43FE-92FC-F14F9CE86309';
IF NOT EXISTS (SELECT 1 FROM [list].[ItemListDataParameter] [dp] WHERE [dp].[MasterID] = '7D906A69-6C7E-4863-8F87-63CEC5CDBAE2')
BEGIN
    INSERT INTO [list].[ItemListDataParameter] ([ItemListDataSourceID], [DataType], [Type], [Mandatory], [DefaultValue], [ConstructionPattern], [Created], [MasterID])
    SELECT [ds].[ItemListDataSourceID] AS [ItemListDataSourceID], N'Integer' AS [DataType], N'PortalPartParam' AS [Type], N'0' AS [Mandatory], N''+@PortalPartID_4438_10221606103571+'' AS [DefaultValue], N'' AS [ConstructionPattern], GETDATE() AS [Created], N'7D906A69-6C7E-4863-8F87-63CEC5CDBAE2' AS [MasterID]
    FROM [list].[ItemListDataSource] [ds]
    WHERE [ds].[MasterID] = '9196CFF3-6E58-4CEF-9532-A63F1BF5BF95';
    DECLARE @ParamID_3799 nvarchar(32)= SCOPE_IDENTITY();

    INSERT INTO [list].[LT_ItemListDataParameter] ([LanguageID], [ItemListDataParameterID], [Name], [Description])
    SELECT l.[LanguageID], @ParamID_3799 AS [ItemListDataParameterID], N'vgoRegistrationPortalPartId' AS [Name], N'' AS [Description]
    FROM [dbo].[Language] l
    WHERE l.[LanguageID] IN (1,2,7);
END;
--#endregion DataSource 89

--#region DataSource 94
DECLARE @PortalPageID_3896_16215810521301 nvarchar(32)= '';
SELECT @PortalPageID_3896_16215810521301 = [pp].[PortalPageID]
FROM [app].[PortalPage] [pp]
WHERE [pp].[MasterID] = '3F1F5E61-10A3-446B-98BC-8860215814CE';
IF NOT EXISTS (SELECT 1 FROM [list].[ItemListDataParameter] [dp] WHERE [dp].[MasterID] = '07A805A5-4D5A-4A73-8C3C-7DD097E2B452')
BEGIN
    INSERT INTO [list].[ItemListDataParameter] ([ItemListDataSourceID], [DataType], [Type], [Mandatory], [DefaultValue], [ConstructionPattern], [Created], [MasterID])
    SELECT [ds].[ItemListDataSourceID] AS [ItemListDataSourceID], N'Integer' AS [DataType], N'PortalPartParam' AS [Type], N'0' AS [Mandatory], N''+@PortalPageID_3896_16215810521301+'' AS [DefaultValue], N'' AS [ConstructionPattern], GETDATE() AS [Created], N'07A805A5-4D5A-4A73-8C3C-7DD097E2B452' AS [MasterID]
    FROM [list].[ItemListDataSource] [ds]
    WHERE [ds].[MasterID] = 'E1EBF0CA-3EEB-4A1C-881E-2C336483AC31';
    DECLARE @ParamID_2768 nvarchar(32)= SCOPE_IDENTITY();

    INSERT INTO [list].[LT_ItemListDataParameter] ([LanguageID], [ItemListDataParameterID], [Name], [Description])
    SELECT l.[LanguageID], @ParamID_2768 AS [ItemListDataParameterID], N'vgoUserListPortalpageId' AS [Name], N'' AS [Description]
    FROM [dbo].[Language] l
    WHERE l.[LanguageID] IN (1,2,7);
END;
--#endregion DataSource 94

--#region DataSource 96
DECLARE @PortalPartID_5627_16235357350971 nvarchar(32)= '';
SELECT @PortalPartID_5627_16235357350971 = [pt].[PortalPartID]
FROM [app].[PortalPart] [pt]
WHERE [pt].[MasterID] = '71E55B8F-85B0-4165-A08F-3BC4785E60CC';
IF NOT EXISTS (SELECT 1 FROM [list].[ItemListDataParameter] [dp] WHERE [dp].[MasterID] = '0E459B13-0813-4C43-878B-625943730709')
BEGIN
    INSERT INTO [list].[ItemListDataParameter] ([ItemListDataSourceID], [DataType], [Type], [Mandatory], [DefaultValue], [ConstructionPattern], [Created], [MasterID])
    SELECT [ds].[ItemListDataSourceID] AS [ItemListDataSourceID], N'Integer' AS [DataType], N'PortalPartParam' AS [Type], N'0' AS [Mandatory], N''+@PortalPartID_5627_16235357350971+'' AS [DefaultValue], N'' AS [ConstructionPattern], GETDATE() AS [Created], N'0E459B13-0813-4C43-878B-625943730709' AS [MasterID]
    FROM [list].[ItemListDataSource] [ds]
    WHERE [ds].[MasterID] = '1A5C8D71-7B44-41EB-A60E-1C141E881853';
    DECLARE @ParamID_3796 nvarchar(32)= SCOPE_IDENTITY();

    INSERT INTO [list].[LT_ItemListDataParameter] ([LanguageID], [ItemListDataParameterID], [Name], [Description])
    SELECT l.[LanguageID], @ParamID_3796 AS [ItemListDataParameterID], N'vgoRegistrationPortalPartId' AS [Name], N'' AS [Description]
    FROM [dbo].[Language] l
    WHERE l.[LanguageID] IN (1,2,7);
END;
--#endregion DataSource 96

--#region ActivityView 1788
IF NOT EXISTS (SELECT 1 FROM [at].[ActivityView] [av] WHERE [av].[MasterID] = '9633F41B-5334-4D02-A9B6-DCB104FD03C5')
BEGIN
    INSERT INTO [at].[ActivityView] ([DepartmentID], [CustomerID], [ActivityID], [Created], [ExtId], [MasterID])
    SELECT NULL AS [DepartmentID], NULL AS [CustomerID], [a].[ActivityID] AS [ActivityID], GETDATE() AS [Created], N'' AS [ExtId], N'9633F41B-5334-4D02-A9B6-DCB104FD03C5' AS [MasterID]
    FROM [at].[Activity] [a]
    WHERE [a].[MasterID] = 'C9CD0427-CBE9-4AB1-8F96-916771EE444E';
    DECLARE @ActivityViewID_1788 nvarchar(32)= SCOPE_IDENTITY();

    INSERT INTO [at].[LT_ActivityView] ([LanguageID], [ActivityViewID], [Name], [Description])
    SELECT l.[LanguageID], @ActivityViewID_1788 AS [ActivityViewID], N'userListView' AS [Name], N'' AS [Description]
    FROM [dbo].[Language] l
    WHERE l.[LanguageID] IN (1,2,7);

    DECLARE @QuestionID_63023_171124744449981755 nvarchar(32)= '';
    SELECT @QuestionID_63023_171124744449981755 = [q].[QuestionID]
    FROM [at].[Question] [q]
    WHERE [q].[MasterID] = 'D6AD7642-8F3B-49F4-977B-0B124B7563A1';

    DECLARE @AlternativeID_94212_171124744449981755 nvarchar(32)= '';
    SELECT @AlternativeID_94212_171124744449981755 = [alt].[AlternativeID]
    FROM [at].[Alternative] [alt]
    WHERE [alt].[MasterID] = '247A0C4D-8AF0-45F9-BDF9-2D8452AAE908';
    IF NOT EXISTS (SELECT 1
                   FROM [at].[ActivityView_Q] [avq]
                   JOIN [at].[ActivityView] [av] ON [avq].[ActivityViewID] = [av].[ActivityViewID]
                                                    AND [av].[MasterID] = '9633F41B-5334-4D02-A9B6-DCB104FD03C5'
                                                    AND [QuestionID] = @QuestionID_63023_171124744449981755
                                                    AND [SectionID] IS NULL
                                                    AND [No] = 1
                                                    AND [CategoryID] IS NULL
                                                    AND [AlternativeID] = @AlternativeID_94212_171124744449981755
                  )
    BEGIN
        INSERT INTO [at].[ActivityView_Q] ([ActivityViewID], [QuestionID], [SectionID], [No], [CategoryID], [AlternativeID], [ItemID])
        SELECT [av].[ActivityViewID] AS [ActivityViewID], @QuestionID_63023_171124744449981755 AS [QuestionID], NULL AS [SectionID], 1 AS [No], NULL AS [CategoryID], @AlternativeID_94212_171124744449981755 AS [AlternativeID], NULL AS [ItemID]
        FROM [at].[ActivityView] [av]
        WHERE [av].[MasterID] = '9633F41B-5334-4D02-A9B6-DCB104FD03C5';

    END;

    DECLARE @QuestionID_63023_171124728887681756 nvarchar(32)= '';
    SELECT @QuestionID_63023_171124728887681756 = [q].[QuestionID]
    FROM [at].[Question] [q]
    WHERE [q].[MasterID] = 'D6AD7642-8F3B-49F4-977B-0B124B7563A1';

    DECLARE @AlternativeID_94213_171124728887681756 nvarchar(32)= '';
    SELECT @AlternativeID_94213_171124728887681756 = [alt].[AlternativeID]
    FROM [at].[Alternative] [alt]
    WHERE [alt].[MasterID] = 'A9643C5F-1B75-412F-A00C-FA374D972E52';
    IF NOT EXISTS (SELECT 1
                   FROM [at].[ActivityView_Q] [avq]
                   JOIN [at].[ActivityView] [av] ON [avq].[ActivityViewID] = [av].[ActivityViewID]
                                                    AND [av].[MasterID] = '9633F41B-5334-4D02-A9B6-DCB104FD03C5'
                                                    AND [QuestionID] = @QuestionID_63023_171124728887681756
                                                    AND [SectionID] IS NULL
                                                    AND [No] = 2
                                                    AND [CategoryID] IS NULL
                                                    AND [AlternativeID] = @AlternativeID_94213_171124728887681756
                  )
    BEGIN
        INSERT INTO [at].[ActivityView_Q] ([ActivityViewID], [QuestionID], [SectionID], [No], [CategoryID], [AlternativeID], [ItemID])
        SELECT [av].[ActivityViewID] AS [ActivityViewID], @QuestionID_63023_171124728887681756 AS [QuestionID], NULL AS [SectionID], 2 AS [No], NULL AS [CategoryID], @AlternativeID_94213_171124728887681756 AS [AlternativeID], NULL AS [ItemID]
        FROM [at].[ActivityView] [av]
        WHERE [av].[MasterID] = '9633F41B-5334-4D02-A9B6-DCB104FD03C5';

    END;

    DECLARE @QuestionID_63024_171124713142281757 nvarchar(32)= '';
    SELECT @QuestionID_63024_171124713142281757 = [q].[QuestionID]
    FROM [at].[Question] [q]
    WHERE [q].[MasterID] = 'F9508ECD-F240-4408-A32E-74EFB8A411E9';

    DECLARE @AlternativeID_94212_171124713142281757 nvarchar(32)= '';
    SELECT @AlternativeID_94212_171124713142281757 = [alt].[AlternativeID]
    FROM [at].[Alternative] [alt]
    WHERE [alt].[MasterID] = '247A0C4D-8AF0-45F9-BDF9-2D8452AAE908';
    IF NOT EXISTS (SELECT 1
                   FROM [at].[ActivityView_Q] [avq]
                   JOIN [at].[ActivityView] [av] ON [avq].[ActivityViewID] = [av].[ActivityViewID]
                                                    AND [av].[MasterID] = '9633F41B-5334-4D02-A9B6-DCB104FD03C5'
                                                    AND [QuestionID] = @QuestionID_63024_171124713142281757
                                                    AND [SectionID] IS NULL
                                                    AND [No] = 3
                                                    AND [CategoryID] IS NULL
                                                    AND [AlternativeID] = @AlternativeID_94212_171124713142281757
                  )
    BEGIN
        INSERT INTO [at].[ActivityView_Q] ([ActivityViewID], [QuestionID], [SectionID], [No], [CategoryID], [AlternativeID], [ItemID])
        SELECT [av].[ActivityViewID] AS [ActivityViewID], @QuestionID_63024_171124713142281757 AS [QuestionID], NULL AS [SectionID], 3 AS [No], NULL AS [CategoryID], @AlternativeID_94212_171124713142281757 AS [AlternativeID], NULL AS [ItemID]
        FROM [at].[ActivityView] [av]
        WHERE [av].[MasterID] = '9633F41B-5334-4D02-A9B6-DCB104FD03C5';

    END;

    DECLARE @QuestionID_63024_171124697516981758 nvarchar(32)= '';
    SELECT @QuestionID_63024_171124697516981758 = [q].[QuestionID]
    FROM [at].[Question] [q]
    WHERE [q].[MasterID] = 'F9508ECD-F240-4408-A32E-74EFB8A411E9';

    DECLARE @AlternativeID_94213_171124697516981758 nvarchar(32)= '';
    SELECT @AlternativeID_94213_171124697516981758 = [alt].[AlternativeID]
    FROM [at].[Alternative] [alt]
    WHERE [alt].[MasterID] = 'A9643C5F-1B75-412F-A00C-FA374D972E52';
    IF NOT EXISTS (SELECT 1
                   FROM [at].[ActivityView_Q] [avq]
                   JOIN [at].[ActivityView] [av] ON [avq].[ActivityViewID] = [av].[ActivityViewID]
                                                    AND [av].[MasterID] = '9633F41B-5334-4D02-A9B6-DCB104FD03C5'
                                                    AND [QuestionID] = @QuestionID_63024_171124697516981758
                                                    AND [SectionID] IS NULL
                                                    AND [No] = 4
                                                    AND [CategoryID] IS NULL
                                                    AND [AlternativeID] = @AlternativeID_94213_171124697516981758
                  )
    BEGIN
        INSERT INTO [at].[ActivityView_Q] ([ActivityViewID], [QuestionID], [SectionID], [No], [CategoryID], [AlternativeID], [ItemID])
        SELECT [av].[ActivityViewID] AS [ActivityViewID], @QuestionID_63024_171124697516981758 AS [QuestionID], NULL AS [SectionID], 4 AS [No], NULL AS [CategoryID], @AlternativeID_94213_171124697516981758 AS [AlternativeID], NULL AS [ItemID]
        FROM [at].[ActivityView] [av]
        WHERE [av].[MasterID] = '9633F41B-5334-4D02-A9B6-DCB104FD03C5';

    END;

END;
--#endregion ActivityView 1788

--#region DataParameter 3800
DECLARE @ActivityViewID_1788_17150554080101 nvarchar(32)= '';
SELECT @ActivityViewID_1788_17150554080101 = [av].[ActivityViewID]
FROM [at].[ActivityView] [av]
WHERE [av].[MasterID] = '9633F41B-5334-4D02-A9B6-DCB104FD03C5';
IF NOT EXISTS (SELECT 1 FROM [list].[ItemListDataParameter] [dp] WHERE [dp].[MasterID] = 'B6356F45-063B-4898-8B6B-7404DA908B95')
BEGIN
    INSERT INTO [list].[ItemListDataParameter] ([ItemListDataSourceID], [DataType], [Type], [Mandatory], [DefaultValue], [ConstructionPattern], [Created], [MasterID])
    SELECT [ds].[ItemListDataSourceID] AS [ItemListDataSourceID], N'Integer' AS [DataType], N'PortalPartParam' AS [Type], N'0' AS [Mandatory], N''+@ActivityViewID_1788_17150554080101+'' AS [DefaultValue], N'' AS [ConstructionPattern], GETDATE() AS [Created], N'B6356F45-063B-4898-8B6B-7404DA908B95' AS [MasterID]
    FROM [list].[ItemListDataSource] [ds]
    WHERE [ds].[MasterID] = '9DB9B94B-D050-44D5-AD3A-4913AF1442C9';
    DECLARE @ParamID_3800 nvarchar(32)= SCOPE_IDENTITY();

    INSERT INTO [list].[LT_ItemListDataParameter] ([LanguageID], [ItemListDataParameterID], [Name], [Description])
    SELECT l.[LanguageID], @ParamID_3800 AS [ItemListDataParameterID], N'activityViewId' AS [Name], N'' AS [Description]
    FROM [dbo].[Language] l
    WHERE l.[LanguageID] IN (1,2,7);
END;
--#endregion DataParameter 3800

--#endregion Config Data

--#endregion 01. CE-2059 Class View For Activity 600

--#region 02. CE-2284 Export Excel For Class View
--#region Update Command 140
DECLARE @UserTypeID_37_09365441084811 nvarchar(32)= '';
    SELECT @UserTypeID_37_09365441084811 = [ut].[UserTypeID]
    FROM [org].[UserType] [ut]
    WHERE [ut].[MasterID] = 'A8D74D77-E251-4F22-BC78-07B8DB689000';
DECLARE @ItemListID_1369_09365442647382 nvarchar(32)= '';
    SELECT @ItemListID_1369_09365442647382 = [l].[ItemListID]
    FROM [list].[ItemList] [l]
    WHERE [l].[MasterID] = 'A6F5E879-610C-496E-A3B0-3CAB8331AE17';
DECLARE @ItemListID_1385_09365442647383 nvarchar(32)= '';
    SELECT @ItemListID_1385_09365442647383 = [l].[ItemListID]
    FROM [list].[ItemList] [l]
    WHERE [l].[MasterID] = '920E1B8A-8EDD-456F-9D78-7762E934DE26';
DECLARE @DataSourceID_2154_09365442647385 nvarchar(32)= '';
    SELECT @DataSourceID_2154_09365442647385 = [ds].[ItemListDataSourceID]
    FROM [list].[ItemListDataSource] [ds]
    WHERE [ds].[MasterID] = '9DB9B94B-D050-44D5-AD3A-4913AF1442C9';

UPDATE c SET c.[ClientFunction] = N'exportResultToExcel('+@UserTypeID_37_09365441084811+','+@ItemListID_1369_09365442647382+','+@ItemListID_1385_09365442647383+','+'0'+','+@DataSourceID_2154_09365442647385+')'
FROM [list].[ItemListCommand] c
WHERE c.[MasterID] = 'D2D7F155-D94D-4223-8344-11D7E69492EF';
--#endregion Update Command 140
--#endregion 02. CE-2284 Export Excel For Class View

--#endregion 107. Class View For Activity 600
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
--#region 110. CE-2207 Student Report Popup
-- Dev: Thomas
-- Feature: Object follow up
-- CE-1871 Remove old "oppfølging i fag" tab from student report popup
DECLARE @ActivityID_600_11001773049041 nvarchar(32)= '';
SELECT @ActivityID_600_11001773049041 = [a].[ActivityID] FROM [at].[Activity] [a] WHERE [a].[MasterID] = 'C9CD0427-CBE9-4AB1-8F96-916771EE444E';
IF NOT EXISTS (SELECT 1 FROM [list].[ItemListDataParameter] [dp] WHERE [dp].[MasterID] = 'CF873C14-9C03-44B6-8AF5-1314B456BC3F')
BEGIN
    INSERT INTO [list].[ItemListDataParameter] ([ItemListDataSourceID], [DataType], [Type], [Mandatory], [DefaultValue], [ConstructionPattern], [Created], [MasterID])
    SELECT [ds].[ItemListDataSourceID] AS [ItemListDataSourceID], N'Integer' AS [DataType], N'PortalPartParam' AS [Type], N'0' AS [Mandatory], N''+@ActivityID_600_11001773049041+'' AS [DefaultValue], N'' AS [ConstructionPattern], GETDATE() AS [Created], N'CF873C14-9C03-44B6-8AF5-1314B456BC3F' AS [MasterID]
    FROM [list].[ItemListDataSource] [ds]
    WHERE [ds].[MasterID] = 'C5AD0998-036D-42B6-93F1-D4015269F832';
    DECLARE @ParamID_3804 nvarchar(32)= SCOPE_IDENTITY();

    INSERT INTO [list].[LT_ItemListDataParameter] ([LanguageID], [ItemListDataParameterID], [Name], [Description])
    SELECT l.[LanguageID], @ParamID_3804 AS [ItemListDataParameterID], N'ObjectFollowUpActivityId' AS [Name], N'' AS [Description]
    FROM [dbo].[Language] l
    WHERE l.[LanguageID] IN (1,2,7);
END;
--#endregion 110. CE-2207 Student Report Popup
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
-------------------------------------------------------------------------------------------------------------------
PRINT 'Setting database version'
/*
Start of versioning database-level extended properties.
*/
BEGIN
    DECLARE @DB_VERSION varchar(32), @ConceptName nvarchar(128), @ReleaseScriptName nvarchar(4000)
    SET @DB_VERSION = '6.12.006.002.02'
    SET @ConceptName = 'Engage Main'
    SET @ReleaseScriptName = N'v' + @DB_VERSION + N'-03. Engage_NO Data Update Since 2019-01-07'

    IF EXISTS (SELECT 1 FROM fn_listextendedproperty(N'cxStudio Database Version', NULL, NULL, NULL, NULL, NULL, NULL))
        EXEC sp_dropextendedproperty N'cxStudio Database Version', NULL, NULL, NULL, NULL, NULL, NULL
    EXEC sp_addextendedproperty N'cxStudio Database Version', @DB_VERSION, NULL, NULL, NULL, NULL, NULL, NULL
		
    IF EXISTS (SELECT 1 FROM fn_listextendedproperty(N'Script Name', NULL, NULL, NULL, NULL, NULL, NULL))
        EXEC sp_dropextendedproperty N'Script Name', NULL, NULL, NULL, NULL, NULL, NULL
    EXEC sp_addextendedproperty N'Script Name',  @ReleaseScriptName , NULL, NULL, NULL, NULL, NULL, NULL
END
/*
End of versioning database-level extended properties.
*/
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
IF EXISTS (SELECT * FROM #tmpErrors) ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT>0 BEGIN
PRINT 'The database update succeeded'
COMMIT TRANSACTION
END
ELSE PRINT 'The database update failed'
GO
DROP TABLE #tmpErrors
GO
SET NOEXEC OFF
GO
-------------------------------------------------------------------------------------------------------------------------
DECLARE @Current_DB_Version varchar(32), @CurrentConceptName nvarchar(128),
        @Required_DB_Version varchar(32), @RequiredConceptName nvarchar(128)

SET @Required_DB_Version = '6.12.006.002.02'
SET @RequiredConceptName = 'Engage Main'
PRINT 'This script is generated for executing on ' + @RequiredConceptName + ' version ' + @Required_DB_Version

SELECT @Current_DB_Version = convert(nvarchar(max),value) FROM fn_listextendedproperty(N'cxStudio Database Version', NULL, NULL, NULL, NULL, NULL, NULL)
SELECT @CurrentConceptName = convert(nvarchar(max),value) FROM fn_listextendedproperty(N'Concept Name', NULL, NULL, NULL, NULL, NULL, NULL)

IF @CurrentConceptName != @RequiredConceptName OR @Current_DB_Version NOT LIKE @Required_DB_Version
BEGIN
    PRINT 'Incorrect solution or cxStudio Database version'
    SET NOEXEC ON
END
GO
-------------------------------------------------------------------------------------------------------------------------
IF EXISTS (SELECT * FROM tempdb..sysobjects WHERE id=OBJECT_ID('tempdb..#tmpErrors')) DROP TABLE #tmpErrors
GO
CREATE TABLE #tmpErrors (Error int)
GO
SET XACT_ABORT ON
GO
SET TRANSACTION ISOLATION LEVEL SERIALIZABLE
GO
BEGIN TRANSACTION
GO
----------------------------------------------------------------------------------------------------------------
--#region 111. CE-2195 Historical Result For Object Followup
-- Dev: Gary
-- Feature: Object Followup
-- CE-2195 View historical result of activity 600

--#region Initial
DECLARE @PortalPageID_3896_11171887181701 nvarchar(32)= '';
SELECT @PortalPageID_3896_11171887181701 = [pp].[PortalPageID]
FROM [app].[PortalPage] [pp]
WHERE [pp].[MasterID] = '3F1F5E61-10A3-446B-98BC-8860215814CE';
IF NOT EXISTS (SELECT 1 FROM [list].[ItemListDataParameter] [dp] WHERE [dp].[MasterID] = 'D6DC3B6B-3DD9-4B99-8305-145A2A9C5E03')
BEGIN
    INSERT INTO [list].[ItemListDataParameter] ([ItemListDataSourceID], [DataType], [Type], [Mandatory], [DefaultValue], [ConstructionPattern], [Created], [MasterID])
    SELECT [ds].[ItemListDataSourceID] AS [ItemListDataSourceID], N'Integer' AS [DataType], N'PortalPartParam' AS [Type], N'0' AS [Mandatory], N''+@PortalPageID_3896_11171887181701+'' AS [DefaultValue], N'' AS [ConstructionPattern], GETDATE() AS [Created], N'D6DC3B6B-3DD9-4B99-8305-145A2A9C5E03' AS [MasterID]
    FROM [list].[ItemListDataSource] [ds]
    WHERE [ds].[MasterID] = '8985A1B2-4D6C-49FF-B617-9D46CB6DCF9D';
    DECLARE @ParamID_3803 nvarchar(32)= SCOPE_IDENTITY();

    INSERT INTO [list].[LT_ItemListDataParameter] ([LanguageID], [ItemListDataParameterID], [Name], [Description])
    SELECT l.[LanguageID], @ParamID_3803 AS [ItemListDataParameterID], N'vgoUserListViewPortalPageId' AS [Name], N'' AS [Description]
    FROM [dbo].[Language] l
    WHERE l.[LanguageID] IN (1,2,7);
END;

DECLARE @PortalPageID_3896_11180604345141 nvarchar(32)= '';
SELECT @PortalPageID_3896_11180604345141 = [pp].[PortalPageID]
FROM [app].[PortalPage] [pp]
WHERE [pp].[MasterID] = '3F1F5E61-10A3-446B-98BC-8860215814CE';
IF NOT EXISTS (SELECT 1 FROM [list].[ItemListDataParameter] [dp] WHERE [dp].[MasterID] = '65B4A423-4A65-47FB-B652-3DFF26EB4EC4')
BEGIN
    INSERT INTO [list].[ItemListDataParameter] ([ItemListDataSourceID], [DataType], [Type], [Mandatory], [DefaultValue], [ConstructionPattern], [Created], [MasterID])
    SELECT [ds].[ItemListDataSourceID] AS [ItemListDataSourceID], N'Integer' AS [DataType], N'PortalPartParam' AS [Type], N'0' AS [Mandatory], N''+@PortalPageID_3896_11180604345141+'' AS [DefaultValue], N'' AS [ConstructionPattern], GETDATE() AS [Created], N'65B4A423-4A65-47FB-B652-3DFF26EB4EC4' AS [MasterID]
    FROM [list].[ItemListDataSource] [ds]
    WHERE [ds].[MasterID] = '773F77D9-4445-4422-A1D4-42F4E9D62684';
    DECLARE @ParamID_3805 nvarchar(32)= SCOPE_IDENTITY();

    INSERT INTO [list].[LT_ItemListDataParameter] ([LanguageID], [ItemListDataParameterID], [Name], [Description])
    SELECT l.[LanguageID], @ParamID_3805 AS [ItemListDataParameterID], N'vgoUserListViewPortalPageId' AS [Name], N'' AS [Description]
    FROM [dbo].[Language] l
    WHERE l.[LanguageID] IN (1,2,7);
END;

IF NOT EXISTS (SELECT 1 FROM [list].[ItemListDataParameter] [dp] WHERE [dp].[MasterID] = '35B4B452-D1C9-4736-B085-A8137B0827BF')
BEGIN
    INSERT INTO [list].[ItemListDataParameter] ([ItemListDataSourceID], [DataType], [Type], [Mandatory], [DefaultValue], [ConstructionPattern], [Created], [MasterID])
    SELECT [ds].[ItemListDataSourceID] AS [ItemListDataSourceID], N'Boolean' AS [DataType], N'PortalPartParam' AS [Type], N'0' AS [Mandatory], N''+'false'+'' AS [DefaultValue], N'' AS [ConstructionPattern], GETDATE() AS [Created], N'35B4B452-D1C9-4736-B085-A8137B0827BF' AS [MasterID]
    FROM [list].[ItemListDataSource] [ds]
    WHERE [ds].[MasterID] = '9DB9B94B-D050-44D5-AD3A-4913AF1442C9';
    DECLARE @ParamID_3806 nvarchar(32)= SCOPE_IDENTITY();

    INSERT INTO [list].[LT_ItemListDataParameter] ([LanguageID], [ItemListDataParameterID], [Name], [Description])
    SELECT l.[LanguageID], @ParamID_3806 AS [ItemListDataParameterID], N'isHistory' AS [Name], N'' AS [Description]
    FROM [dbo].[Language] l
    WHERE l.[LanguageID] IN (1,2,7);
END;

DECLARE @PortalPageID_3852_14002313673221 nvarchar(32)= '';
SELECT @PortalPageID_3852_14002313673221 = [pp].[PortalPageID]
FROM [app].[PortalPage] [pp]
WHERE [pp].[MasterID] = '47F48E5F-8D06-45A2-8DDD-39C087535BAF';
IF NOT EXISTS (SELECT 1 FROM [list].[ItemListDataParameter] [dp] WHERE [dp].[MasterID] = '8C2A5CF5-2EBF-45C7-A557-9DA8C81DD33E')
BEGIN
    INSERT INTO [list].[ItemListDataParameter] ([ItemListDataSourceID], [DataType], [Type], [Mandatory], [DefaultValue], [ConstructionPattern], [Created], [MasterID])
    SELECT [ds].[ItemListDataSourceID] AS [ItemListDataSourceID], N'Integer' AS [DataType], N'PortalPartParam' AS [Type], N'0' AS [Mandatory], N''+@PortalPageID_3852_14002313673221+'' AS [DefaultValue], N'' AS [ConstructionPattern], GETDATE() AS [Created], N'8C2A5CF5-2EBF-45C7-A557-9DA8C81DD33E' AS [MasterID]
    FROM [list].[ItemListDataSource] [ds]
    WHERE [ds].[MasterID] = '9DB9B94B-D050-44D5-AD3A-4913AF1442C9';
    DECLARE @ParamID_4806 nvarchar(32)= SCOPE_IDENTITY();

    INSERT INTO [list].[LT_ItemListDataParameter] ([LanguageID], [ItemListDataParameterID], [Name], [Description])
    SELECT l.[LanguageID], @ParamID_4806 AS [ItemListDataParameterID], N'ViewHistoryResultPortalPageId' AS [Name], N'' AS [Description]
    FROM [dbo].[Language] l
    WHERE l.[LanguageID] IN (1,2,7);
END;
--#endregion Initial
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
--#region Update 1
UPDATE pp SET pp.[Parameter] = N'rendername=false'
FROM [app].[PortalPage] pp
WHERE pp.[MasterID] IN ('2A7684DB-A110-4111-954F-42EA70B8B0BC','467CFFB5-434B-477A-8EA8-C7DD8797EFB0','8779CE5E-CF5E-48E9-A33F-13C25B115D06','820414F8-777B-490D-8D7F-9BF89AAEE3EA','FBE10F13-1D42-4328-A1F6-770A71E61201','679F2196-E4D8-4B98-851D-D29070AE0CD1')
  AND [pp].[Parameter] NOT LIKE '%rendername%';

DECLARE @PortalPageID_3896_11171887181701 nvarchar(32)= '';
SELECT @PortalPageID_3896_11171887181701 = [pp].[PortalPageID]
FROM [app].[PortalPage] [pp]
WHERE [pp].[MasterID] = '3F1F5E61-10A3-446B-98BC-8860215814CE';

UPDATE pt SET pt.[Settings] = REPLACE(CAST(pt.[Settings] AS nvarchar(MAX)), '</params>', '%26vgouserlistviewppid%3D'+@PortalPageID_3896_11171887181701+'</params>')
FROM [app].[PortalPart] pt
WHERE pt.[MasterID] IN ('4A470D2A-75DB-441C-A424-0B6FE1F62081', '262A4BFB-C50B-4EAB-969A-A3845C3BBCBF')
  AND CAST(pt.[Settings] AS nvarchar(MAX)) NOT LIKE '%vgouserlistviewppid%';
--#endregion Update 1

--#endregion 111. CE-2195 Historical Result For Object Followup
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
--#region 113. CE-2252 IKO Report Selection
-- Dev: Thomas
-- Feature: Report
-- CE-2252 IKO - Reports - Selections
INSERT INTO [app].[SiteParameter] ([SiteID], [Key], [Value], [Created], [GroupName])
SELECT s.[SiteID], N'Platform.Reporting.ObjectFollowUpActivityId' AS [Key], a.[ActivityID] AS [Value], GETDATE() AS [Created], N'Platform.Reporting' AS [GroupName]
FROM [app].[Site] s
JOIN [at].[Activity] a ON a.[MasterID] = 'C9CD0427-CBE9-4AB1-8F96-916771EE444E'
 AND NOT EXISTS (SELECT 1 FROM [app].[SiteParameter] sp WHERE sp.[SiteID] = s.[SiteID] AND sp.[Key] = 'Platform.Reporting.ObjectFollowUpActivityId');
--#endregion 113. CE-2252 IKO Report Selection
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
--#region 114. Misc Update
-- Dev: David
UPDATE lt SET lt.[TextAbove] = N'<div class="header-panel iko-category-header">IKO - oppfølging i fag</div>'
FROM [app].[PortalPart] pt
JOIN [app].[LT_PortalPart] lt ON lt.[PortalPartID] = pt.[PortalPartID] AND pt.[MasterID] = 'E0D4FDE3-7963-4FEB-9D7E-3237C28D90C5';

UPDATE [app].[LoginService] SET [IconUrl] = '<a href="javascript:void(0)">logg inn med FEIDE</a><img src="{GetStylesThemeURL()}/Images/login_bg_Feide.png" alt="feide login" width="24">' WHERE [LoginServiceID] = 5;

--#endregion 114. Misc Update
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
--#region 115. CE-2217 Remove ShowInChartView For All AVT In Activity 600
-- Dev: Ray
-- Feature: Object Followup
-- CE-2217 [VGO][ChartView]- Chart view icon still be displayed for Object follow up activity.
UPDATE avt SET avt.[Type] = avt.[Type] - 2
FROM [at].[A_VT] avt
JOIN [at].[Activity] a ON a.[ActivityID] = avt.[ActivityID] AND a.[MasterID] = 'C9CD0427-CBE9-4AB1-8F96-916771EE444E'
 AND (avt.[Type] & 2) = 2;
--#endregion 115. CE-2217 Remove ShowInChartView For All AVT In Activity 600
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
-------------------------------------------------------------------------------------------------------------------
PRINT 'Setting database version'
/*
Start of versioning database-level extended properties.
*/
BEGIN
    DECLARE @DB_VERSION varchar(32), @ConceptName nvarchar(128), @ReleaseScriptName nvarchar(4000)
    SET @DB_VERSION = '6.12.006.002.03'
    SET @ConceptName = 'Engage Main'
    SET @ReleaseScriptName = N'v' + @DB_VERSION + N'-03. Engage_NO Data Update Since 2019-01-07'

    IF EXISTS (SELECT 1 FROM fn_listextendedproperty(N'cxStudio Database Version', NULL, NULL, NULL, NULL, NULL, NULL))
        EXEC sp_dropextendedproperty N'cxStudio Database Version', NULL, NULL, NULL, NULL, NULL, NULL
    EXEC sp_addextendedproperty N'cxStudio Database Version', @DB_VERSION, NULL, NULL, NULL, NULL, NULL, NULL
		
    IF EXISTS (SELECT 1 FROM fn_listextendedproperty(N'Script Name', NULL, NULL, NULL, NULL, NULL, NULL))
        EXEC sp_dropextendedproperty N'Script Name', NULL, NULL, NULL, NULL, NULL, NULL
    EXEC sp_addextendedproperty N'Script Name',  @ReleaseScriptName , NULL, NULL, NULL, NULL, NULL, NULL
END
/*
End of versioning database-level extended properties.
*/
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
IF EXISTS (SELECT * FROM #tmpErrors) ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT>0 BEGIN
PRINT 'The database update succeeded'
COMMIT TRANSACTION
END
ELSE PRINT 'The database update failed'
GO
DROP TABLE #tmpErrors
GO
SET NOEXEC OFF
GO