SET QUOTED_IDENTIFIER ON
GO
DECLARE @Current_DB_Version varchar(32), @CurrentConceptName nvarchar(128),
        @Required_DB_Version varchar(32), @RequiredConceptName nvarchar(128)

SET @Required_DB_Version = '6.12.006.%'
SET @RequiredConceptName = 'Engage Main'
PRINT 'This script is generated for executing on ' + @RequiredConceptName + ' version ' + @Required_DB_Version

SELECT @Current_DB_Version = convert(nvarchar(max),value) FROM fn_listextendedproperty(N'cxStudio Database Version', NULL, NULL, NULL, NULL, NULL, NULL)
SELECT @CurrentConceptName = convert(nvarchar(max),value) FROM fn_listextendedproperty(N'Concept Name', NULL, NULL, NULL, NULL, NULL, NULL)

IF @CurrentConceptName != @RequiredConceptName OR @Current_DB_Version NOT LIKE @Required_DB_Version
BEGIN
    PRINT 'Incorrect solution or cxStudio Database version'
    SET NOEXEC ON
END
GO
-------------------------------------------------------------------------------------------------------------------------
IF EXISTS (SELECT * FROM tempdb..sysobjects WHERE id=OBJECT_ID('tempdb..#tmpErrors')) DROP TABLE #tmpErrors
GO
CREATE TABLE #tmpErrors (Error int)
GO
SET XACT_ABORT ON
GO
SET TRANSACTION ISOLATION LEVEL SERIALIZABLE
GO
BEGIN TRANSACTION
GO
----------------------------------------------------------------------------------------------------------------
--#region 085.01. Copy VIGO Tables From IDB
-- Dev: Ray
-- Feature: CE-1840 Copy VIGO data from I-DB to Q
-- CE-1997 Create new table in Q
-- CE-1998 Import data from I-DB tables
IF NOT EXISTS (SELECT 1 FROM sys.[schemas] WHERE [name] = 'vigo')
BEGIN
    EXEC ('CREATE SCHEMA [vigo] AUTHORIZATION [dbo]');
END
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[vigo].[vokal_fag]') AND type in (N'U'))
BEGIN
SELECT * INTO [vigo].[vokal_fag] FROM [systemtest-engage-no-at6i].[tmp].[vokal_fag];
END
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[vigo].[vokal_karakterkode]') AND type in (N'U'))
BEGIN
SELECT * INTO [vigo].[vokal_karakterkode] FROM [systemtest-engage-no-at6i].[tmp].[vokal_karakterkode];
END
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[vigo].[vokal_niva]') AND type in (N'U'))
BEGIN
SELECT * INTO [vigo].[vokal_niva] FROM [systemtest-engage-no-at6i].[tmp].[vokal_niva];
END
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[vigo].[vokal_program]') AND type in (N'U'))
BEGIN
SELECT * INTO [vigo].[vokal_program] FROM [systemtest-engage-no-at6i].[tmp].[vokal_program];
END
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[vigo].[vokal_skole]') AND type in (N'U'))
BEGIN
SELECT * INTO [vigo].[vokal_skole] FROM [systemtest-engage-no-at6i].[tmp].[vokal_skole];
END
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[vigo].[vokal_soker]') AND type in (N'U'))
BEGIN
SELECT * INTO [vigo].[vokal_soker] FROM [systemtest-engage-no-at6i].[tmp].[vokal_soker];
END
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[vigo].[vokal_soker_fag]') AND type in (N'U'))
BEGIN
SELECT * INTO [vigo].[vokal_soker_fag] FROM [systemtest-engage-no-at6i].[tmp].[vokal_soker_fag];
END
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[vigo].[vokal_soker_onske]') AND type in (N'U'))
BEGIN
SELECT * INTO [vigo].[vokal_soker_onske] FROM [systemtest-engage-no-at6i].[tmp].[vokal_soker_onske];
END
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[vigo].[vokal_soker_program]') AND type in (N'U'))
BEGIN
SELECT * INTO [vigo].[vokal_soker_program] FROM [systemtest-engage-no-at6i].[tmp].[vokal_soker_program];
END
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
--#endregion 085.01. Copy VIGO Tables From IDB

--#region 085.02 VIGO Views
CREATE OR ALTER VIEW [vigo].[vokal_subject] AS
SELECT [fagkode] AS [SubjectCode],
       [fagnavn] AS [SubjectName],
       [aktiv] AS [Active]
FROM [vigo].[vokal_fag];
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
CREATE OR ALTER VIEW [vigo].[vokal_grade] AS
SELECT [karaktertype] AS [GradeType],
       [karakterkode] AS [GradeCode],
       [karakternavn] AS [GradeName],
       [tallverdi] AS [Value],
       [aktiv] AS [Active]
FROM [vigo].[vokal_karakterkode];
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
CREATE OR ALTER VIEW [vigo].[vokal_level] AS
SELECT [niv�kode] AS [LevelCode],
       [beskrivelse] AS [Description]
FROM [vigo].[vokal_niva];
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
CREATE OR ALTER VIEW [vigo].[vokal_programArea] AS
SELECT [kurskode] AS [ProgramAreaCode],
       [kursnavn] AS [ProgramAreaName],
       [aktiv] AS [Active]
FROM [vigo].[vokal_program];
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
CREATE OR ALTER VIEW [vigo].[vokal_school] AS
SELECT [skolenummer] AS [SchoolNumber],
       [skolenavn] AS [SchoolName],
       [fylkesnr] AS [Countynumber],
       [orgnr] AS [OrganizationNumber],
       [aktiv] AS [Active]
FROM [vigo].[vokal_skole];
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
CREATE OR ALTER VIEW [vigo].[vokal_applicant] AS
SELECT [fylkesnr - avsender] AS [CountyNumber],
       [f�dselsnummer] AS [SSN],
       [elevnavn] AS [Name],
       [fylkesnr - elev] AS [ApplicantCountyNumber],
       [avgangs�r_GSK] AS [GraduationYearGSK],
       [spesialundervisning vurderes] AS [SpecialEducationAssessed]
FROM [vigo].[vokal_soker]
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
CREATE OR ALTER VIEW [vigo].[vokal_applicant_subject] AS
SELECT [fylkesnr - avsender] AS [CountyNumber],
       [f�dselsnummer] AS [SSN],
       [skole�r] AS [SchoolYear],
       [skolenr] AS [SchoolNumber],
       [programomr�de] AS [ProgramArea],
       [fagkode] AS [SubjectCode],
       [karaktertype] AS [GradeType],
       [karakter - standpunkt] AS [FinalGrade],
       [id],
       [Niv�] AS [Level]
FROM [vigo].[vokal_soker_fag];
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
CREATE OR ALTER VIEW [vigo].[vokal_applicant_preference] AS
SELECT [fylkesnr - avsender] AS [CountyNumber],
       [f�dselsnummer] AS [SSN],
       [skole�r] AS [SchoolYear],
       [�nskenr] AS [Priority],
       [skolenr] AS [SchoolNumber],
       [programomr�de] AS [ProgramArea],
       [Niv�] AS [Level],
       [statusnr] AS [StatusNumber]
FROM [vigo].[vokal_soker_onske]
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
CREATE OR ALTER VIEW [vigo].[vokal_applicant_programArea] AS
SELECT [fylkesnr - avsender] AS [CountyNumber],
       [f�dselsnummer] AS [SSN],
       [skole�r] AS [SchoolYear],
       [skolenr] AS [SchoolNumber],
       [programomr�de] AS [ProgramArea],
       [fylkesnr - kompetansebevis] AS [IssuerOfAdmissionCertificate],
       [karakterpoengsum] AS [Points],
       [frav�r - dager] AS [AbsenceDays],
       [frav�r - timer] AS [AbsenceHours],
       [karaktertype] AS [GradeType],
       [karakter - orden] AS [GradeOrder],
       [karakter - atferd] AS [GradeConduct],
       [Niv�] AS [Level]
FROM [vigo].[vokal_soker_program]
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
--#endregion 085.02 VIGO Views

--#region 085.03 VIGO Indexes
DROP INDEX IF EXISTS [IX_Vigo_SokerProgram_ProgramCode] ON [vigo].[vokal_soker_program];
DROP INDEX IF EXISTS [IX_Vigo_SokerProgram_CountySSNProgramSchoolYear] ON [vigo].[vokal_soker_program];
DROP INDEX IF EXISTS [IX_Vigo_SokerFag_CountySSNProgramSchoolYear] ON [vigo].[vokal_soker_fag];
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
IF NOT EXISTS (SELECT * FROM sys.[indexes] ix WHERE ix.[object_id] = OBJECT_ID('[vigo].[vokal_program]') AND ix.[name] = 'IX_Vigo_Program_Code')
BEGIN
    CREATE INDEX [IX_Vigo_Program_Code] ON [vigo].[vokal_program] ([kurskode], [aktiv]) INCLUDE ([kursnavn]);
END
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
IF NOT EXISTS (SELECT * FROM sys.[indexes] ix WHERE ix.[object_id] = OBJECT_ID('[vigo].[vokal_fag]') AND ix.[name] = 'IX_Vigo_Subject_Code')
BEGIN
    CREATE INDEX [IX_Vigo_Subject_Code] ON [vigo].[vokal_fag] ([fagkode], [aktiv]) INCLUDE ([fagnavn]);
END
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
IF NOT EXISTS (SELECT * FROM sys.[indexes] ix WHERE ix.[object_id] = OBJECT_ID('[vigo].[vokal_soker_program]') AND ix.[name] = 'IX_Vigo_SokerProgram_ProgramCode')
BEGIN
    CREATE INDEX [IX_Vigo_SokerProgram_ProgramCode] ON [vigo].[vokal_soker_program] ([fylkesnr - avsender], [f�dselsnummer], [skolenr], [programomr�de]) INCLUDE ([skole�r]);
END
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
IF NOT EXISTS (SELECT * FROM sys.[indexes] ix WHERE ix.[object_id] = OBJECT_ID('[vigo].[vokal_soker_program]') AND ix.[name] = 'IX_Vigo_SokerProgram_CountySSNProgramSchoolYear')
BEGIN
    CREATE INDEX [IX_Vigo_SokerProgram_CountySSNProgramSchoolYear] ON [vigo].[vokal_soker_program] ([fylkesnr - avsender], [f�dselsnummer], [skole�r], [skolenr], [programomr�de]) INCLUDE ([karakterpoengsum], [frav�r - dager], [frav�r - timer], [karaktertype], [karakter - orden], [karakter - atferd], [niv�]);
END
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
IF NOT EXISTS (SELECT * FROM sys.[indexes] ix WHERE ix.[object_id] = OBJECT_ID('[vigo].[vokal_soker_fag]') AND ix.[name] = 'IX_Vigo_SokerFag_CountySSNProgramSchoolYear')
BEGIN
    CREATE INDEX [IX_Vigo_SokerFag_CountySSNProgramSchoolYear] ON [vigo].[vokal_soker_fag] ([fylkesnr - avsender], [f�dselsnummer], [skole�r], [skolenr], [programomr�de]) INCLUDE ([fagkode], [karaktertype], [karakter - standpunkt], [id], [Niv�]);
END
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
--#endregion 085.03 VIGO Indexes

--#region 085.04 VIGO Procedures
-- Dev: Ray
-- Feature: VIGO data
-- CE-2002 Get data from VIGO tables and map them into dto to show in ui
PRINT 'Procedure [vigo].[prc_GetProgramAreaSchoolYear]'
GO
CREATE OR ALTER PROCEDURE [vigo].[prc_GetProgramAreaSchoolYear]
(   @CountyNumber   nvarchar(max),
    @UserID         int
) AS
BEGIN
    SELECT DISTINCT pa.[ProgramAreaCode], pa.[ProgramAreaName], apa.[SchoolYear], apa.[SchoolNumber]
    FROM [vigo].[vokal_programArea] pa
    JOIN [vigo].[vokal_applicant_programArea] apa ON apa.[ProgramArea] = pa.[ProgramAreaCode] AND pa.[Active] = 'J' AND apa.[CountyNumber] = @CountyNumber
    JOIN [org].[User] u ON u.[SSN] = apa.[SSN] AND u.[SSN] <> '' AND u.[UserID] = @UserID
    ORDER BY apa.[SchoolYear] DESC;
END
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT 'Procedure [vigo].[prc_VigoSummary]'
GO
CREATE OR ALTER PROCEDURE [vigo].[prc_VigoSummary]
(   @CountyNumber   nvarchar(max),
    @UserID         int,
    @ProgramArea    nvarchar(max),
    @SchoolYear     nvarchar(max),
    @SchoolNumber   nvarchar(max)
) AS
BEGIN
    DECLARE @CountGradeIV int = 0, @CountGrade1 int = 0, @CountGrade2 int = 0;
    SELECT @CountGradeIV = SUM(CASE WHEN asj.[FinalGrade] = 'IV' THEN 1 ELSE 0 END),
           @CountGrade1 = SUM(CASE WHEN asj.[FinalGrade] = '1' THEN 1 ELSE 0 END),
           @CountGrade2 =SUM(CASE WHEN asj.[FinalGrade] = '2' THEN 1 ELSE 0 END)
    FROM [org].[User] u
    JOIN [vigo].[vokal_applicant_subject] asj ON u.[SSN] = asj.[SSN] AND u.[SSN] <> '' AND u.[UserID] = @UserID
     AND asj.[CountyNumber] = @CountyNumber
     AND asj.[ProgramArea] = @ProgramArea
     AND asj.[SchoolYear] = @SchoolYear
     AND asj.[SchoolNumber] = @SchoolNumber
    JOIN [vigo].[vokal_subject] sj ON sj.[SubjectCode] = asj.[SubjectCode] AND sj.[Active] = 'J';

    SELECT @CountGradeIV AS [CountGradeIV], @CountGrade1 AS [CountGrade1], @CountGrade2 AS [CountGrade2],
           apa.[Points], apa.[GradeOrder], apa.[GradeConduct], apa.[AbsenceDays], apa.[AbsenceHours]
    FROM [org].[User] u
    JOIN [vigo].[vokal_applicant_programArea] apa ON u.[SSN] = apa.[SSN] AND u.[SSN] <> '' AND u.[UserID] = @UserID
     AND apa.[CountyNumber] = @CountyNumber
     AND apa.[ProgramArea] = @ProgramArea
     AND apa.[SchoolYear] = @SchoolYear
     AND apa.[SchoolNumber] = @SchoolNumber;
END
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT 'Procedure [vigo].[prc_GetSubjectGrade]'
GO
CREATE OR ALTER PROCEDURE [vigo].[prc_GetSubjectGrade]
(   @CountyNumber   nvarchar(max),
    @UserID         int,
    @ProgramArea    nvarchar(max),
    @SchoolYear     nvarchar(max),
    @SchoolNumber   nvarchar(max)
) AS
BEGIN
    SELECT asj.[SubjectCode], sj.[SubjectName], asj.[FinalGrade]
    FROM [org].[User] u
    JOIN [vigo].[vokal_applicant_subject] asj ON u.[SSN] = asj.[SSN] AND u.[SSN] <> '' AND u.[UserID] = @UserID
     AND asj.[CountyNumber] = @CountyNumber
     AND asj.[ProgramArea] = @ProgramArea
     AND asj.[SchoolYear] = @SchoolYear
     AND asj.[SchoolNumber] = @SchoolNumber
    JOIN [vigo].[vokal_subject] sj ON sj.[SubjectCode] = asj.[SubjectCode] AND sj.[Active] = 'J'
    ORDER BY asj.[SubjectCode];
END
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
--#endregion 085.04 VIGO Procedures

--#region 085.05 VIGO Config Data
-- Dev: Gary
-- Feature: VIGO Data
-- CE-2001 Make dto to show data in viewtype

--#region DataSource
-- 3146
IF NOT EXISTS (SELECT 1 FROM [list].[ItemListDataSource] [ds] WHERE [ds].[MasterID] = '739567A1-C0A2-4FEF-88A2-2903C050D59C')
BEGIN
    INSERT INTO [list].[ItemListDataSource] ([OwnerID], [FunctionName], [Created], [MasterID])
    SELECT [o].[OwnerID] AS [OwnerID], N'getlistsprogramvigo' AS [FunctionName], GETDATE() AS [Created], N'739567A1-C0A2-4FEF-88A2-2903C050D59C' AS [MasterID]
    FROM [org].[Owner] [o]
    WHERE [o].[MasterID] = '1C483E02-E808-42A7-8210-B11F4051D2E4';
    DECLARE @ItemListDataSourceID_3146 nvarchar(32)= SCOPE_IDENTITY();

    INSERT INTO [list].[LT_ItemListDataSource] ([LanguageID], [ItemListDataSourceID], [Name], [Description])
    SELECT l.[LanguageID], @ItemListDataSourceID_3146 AS [ItemListDataSourceID], N'Vigo Program list' AS [Name], N'' AS [Description]
    FROM [dbo].[Language] l
    WHERE l.[LanguageID] IN (1,2,7);
END;

-- 3148
IF NOT EXISTS (SELECT 1 FROM [list].[ItemListDataSource] [ds] WHERE [ds].[MasterID] = '9513241E-7EEB-4E83-8EC0-1898D9175F70')
BEGIN
    INSERT INTO [list].[ItemListDataSource] ([OwnerID], [FunctionName], [Created], [MasterID])
    SELECT [o].[OwnerID] AS [OwnerID], N'getvigosubjectcontent' AS [FunctionName], GETDATE() AS [Created], N'9513241E-7EEB-4E83-8EC0-1898D9175F70' AS [MasterID]
    FROM [org].[Owner] [o]
    WHERE [o].[MasterID] = '1C483E02-E808-42A7-8210-B11F4051D2E4';
    DECLARE @ItemListDataSourceID_3148 nvarchar(32)= SCOPE_IDENTITY();

    INSERT INTO [list].[LT_ItemListDataSource] ([LanguageID], [ItemListDataSourceID], [Name], [Description])
    SELECT l.[LanguageID], @ItemListDataSourceID_3148 AS [ItemListDataSourceID], N'Vigo subject content' AS [Name], N'' AS [Description]
    FROM [dbo].[Language] l
    WHERE l.[LanguageID] IN (1,2,7);

    IF NOT EXISTS (SELECT 1 FROM [list].[ItemListDataParameter] [dp] WHERE [dp].[MasterID] = 'C301D2EA-EA6D-4AE9-B4B3-0DE8172CB8C6')
    BEGIN
        INSERT INTO [list].[ItemListDataParameter] ([ItemListDataSourceID], [DataType], [Type], [Mandatory], [DefaultValue], [ConstructionPattern], [Created], [MasterID])
        SELECT [ds].[ItemListDataSourceID] AS [ItemListDataSourceID], N'String' AS [DataType], N'PortalPartParam' AS [Type], N'0' AS [Mandatory], N''+'VigoSubjectContent'+'' AS [DefaultValue], N'' AS [ConstructionPattern], GETDATE() AS [Created], N'C301D2EA-EA6D-4AE9-B4B3-0DE8172CB8C6' AS [MasterID]
        FROM [list].[ItemListDataSource] [ds]
        WHERE [ds].[MasterID] = '9513241E-7EEB-4E83-8EC0-1898D9175F70';
        DECLARE @ParamID_4696 nvarchar(32)= SCOPE_IDENTITY();

        INSERT INTO [list].[LT_ItemListDataParameter] ([LanguageID], [ItemListDataParameterID], [Name], [Description])
        SELECT l.[LanguageID], @ParamID_4696 AS [ItemListDataParameterID], N'ViewName' AS [Name], N'' AS [Description]
        FROM [dbo].[Language] l
        WHERE l.[LanguageID] IN (1,2,7);
    END;

    IF NOT EXISTS (SELECT 1 FROM [list].[ItemListDataParameter] [dp] WHERE [dp].[MasterID] = '3928AA84-C883-4213-872F-2D437FC2D394')
    BEGIN
        INSERT INTO [list].[ItemListDataParameter] ([ItemListDataSourceID], [DataType], [Type], [Mandatory], [DefaultValue], [ConstructionPattern], [Created], [MasterID])
        SELECT [ds].[ItemListDataSourceID] AS [ItemListDataSourceID], N'String' AS [DataType], N'DynamicallyAddedPortalPartParam' AS [Type], N'0' AS [Mandatory], N'' AS [DefaultValue], N'' AS [ConstructionPattern], GETDATE() AS [Created], N'3928AA84-C883-4213-872F-2D437FC2D394' AS [MasterID]
        FROM [list].[ItemListDataSource] [ds]
        WHERE [ds].[MasterID] = '9513241E-7EEB-4E83-8EC0-1898D9175F70';
        DECLARE @ParamID_4698 nvarchar(32)= SCOPE_IDENTITY();

        INSERT INTO [list].[LT_ItemListDataParameter] ([LanguageID], [ItemListDataParameterID], [Name], [Description])
        SELECT l.[LanguageID], @ParamID_4698 AS [ItemListDataParameterID], N'ssn' AS [Name], N'' AS [Description]
        FROM [dbo].[Language] l
        WHERE l.[LanguageID] IN (1,2,7);
    END;

    IF NOT EXISTS (SELECT 1 FROM [list].[ItemListDataParameter] [dp] WHERE [dp].[MasterID] = '89502AD9-E89C-4DE5-A0E1-77A194A935AD')
    BEGIN
        INSERT INTO [list].[ItemListDataParameter] ([ItemListDataSourceID], [DataType], [Type], [Mandatory], [DefaultValue], [ConstructionPattern], [Created], [MasterID])
        SELECT [ds].[ItemListDataSourceID] AS [ItemListDataSourceID], N'String' AS [DataType], N'DynamicallyAddedPortalPartParam' AS [Type], N'0' AS [Mandatory], N'' AS [DefaultValue], N'' AS [ConstructionPattern], GETDATE() AS [Created], N'89502AD9-E89C-4DE5-A0E1-77A194A935AD' AS [MasterID]
        FROM [list].[ItemListDataSource] [ds]
        WHERE [ds].[MasterID] = '9513241E-7EEB-4E83-8EC0-1898D9175F70';
        DECLARE @ParamID_4699 nvarchar(32)= SCOPE_IDENTITY();

        INSERT INTO [list].[LT_ItemListDataParameter] ([LanguageID], [ItemListDataParameterID], [Name], [Description])
        SELECT l.[LanguageID], @ParamID_4699 AS [ItemListDataParameterID], N'programArea' AS [Name], N'' AS [Description]
        FROM [dbo].[Language] l
        WHERE l.[LanguageID] IN (1,2,7);
    END;

    IF NOT EXISTS (SELECT 1 FROM [list].[ItemListDataParameter] [dp] WHERE [dp].[MasterID] = 'F91E229D-CB8D-492B-99E7-3E62ADD65ADD')
    BEGIN
        INSERT INTO [list].[ItemListDataParameter] ([ItemListDataSourceID], [DataType], [Type], [Mandatory], [DefaultValue], [ConstructionPattern], [Created], [MasterID])
        SELECT [ds].[ItemListDataSourceID] AS [ItemListDataSourceID], N'String' AS [DataType], N'DynamicallyAddedPortalPartParam' AS [Type], N'0' AS [Mandatory], N'' AS [DefaultValue], N'' AS [ConstructionPattern], GETDATE() AS [Created], N'F91E229D-CB8D-492B-99E7-3E62ADD65ADD' AS [MasterID]
        FROM [list].[ItemListDataSource] [ds]
        WHERE [ds].[MasterID] = '9513241E-7EEB-4E83-8EC0-1898D9175F70';
        DECLARE @ParamID_4700 nvarchar(32)= SCOPE_IDENTITY();

        INSERT INTO [list].[LT_ItemListDataParameter] ([LanguageID], [ItemListDataParameterID], [Name], [Description])
        SELECT l.[LanguageID], @ParamID_4700 AS [ItemListDataParameterID], N'schoolYear' AS [Name], N'' AS [Description]
        FROM [dbo].[Language] l
        WHERE l.[LanguageID] IN (1,2,7);
    END;

    IF NOT EXISTS (SELECT 1 FROM [list].[ItemListDataParameter] [dp] WHERE [dp].[MasterID] = '0D5BF8CF-0AE8-4BFC-944A-629E13520C29')
    BEGIN
        INSERT INTO [list].[ItemListDataParameter] ([ItemListDataSourceID], [DataType], [Type], [Mandatory], [DefaultValue], [ConstructionPattern], [Created], [MasterID])
        SELECT [ds].[ItemListDataSourceID] AS [ItemListDataSourceID], N'Integer' AS [DataType], N'DynamicallyAddedPortalPartParam' AS [Type], N'0' AS [Mandatory], N'' AS [DefaultValue], N'' AS [ConstructionPattern], GETDATE() AS [Created], N'0D5BF8CF-0AE8-4BFC-944A-629E13520C29' AS [MasterID]
        FROM [list].[ItemListDataSource] [ds]
        WHERE [ds].[MasterID] = '9513241E-7EEB-4E83-8EC0-1898D9175F70';
        DECLARE @ParamID_4704 nvarchar(32)= SCOPE_IDENTITY();

        INSERT INTO [list].[LT_ItemListDataParameter] ([LanguageID], [ItemListDataParameterID], [Name], [Description])
        SELECT l.[LanguageID], @ParamID_4704 AS [ItemListDataParameterID], N'userId' AS [Name], N'' AS [Description]
        FROM [dbo].[Language] l
        WHERE l.[LanguageID] IN (1,2,7);
    END;

    IF NOT EXISTS (SELECT 1 FROM [list].[ItemListDataParameter] [dp] WHERE [dp].[MasterID] = '7B76FC65-B7FE-4590-9B5A-8C0C328FCCB5')
    BEGIN
        INSERT INTO [list].[ItemListDataParameter] ([ItemListDataSourceID], [DataType], [Type], [Mandatory], [DefaultValue], [ConstructionPattern], [Created], [MasterID])
        SELECT [ds].[ItemListDataSourceID] AS [ItemListDataSourceID], N'String' AS [DataType], N'DynamicallyAddedPortalPartParam' AS [Type], N'0' AS [Mandatory], N'' AS [DefaultValue], N'' AS [ConstructionPattern], GETDATE() AS [Created], N'7B76FC65-B7FE-4590-9B5A-8C0C328FCCB5' AS [MasterID]
        FROM [list].[ItemListDataSource] [ds]
        WHERE [ds].[MasterID] = '9513241E-7EEB-4E83-8EC0-1898D9175F70';
        DECLARE @ParamID_4705 nvarchar(32)= SCOPE_IDENTITY();

        INSERT INTO [list].[LT_ItemListDataParameter] ([LanguageID], [ItemListDataParameterID], [Name], [Description])
        SELECT N'1' AS [LanguageID], @ParamID_4705 AS [ItemListDataParameterID], N'schoolNumber' AS [Name], N'' AS [Description];

    END;
END;
--#endregion DataSource

--#region PortalPage

IF NOT EXISTS (SELECT 1 FROM [app].[PortalPage] [pp] WHERE [pp].[MasterID] = '99870C3C-FBDF-4946-84F5-D73A8AA0503B')
BEGIN
    INSERT INTO [app].[PortalPage] ([OwnerID], [No], [Parameter], [ExtID], [Created], [MasterID])
    SELECT [o].[OwnerID] AS [OwnerID], N'122' AS [No], N'rendername=false' AS [Parameter], N'' AS [ExtID], GETDATE() AS [Created], N'99870C3C-FBDF-4946-84F5-D73A8AA0503B' AS [MasterID]
    FROM [org].[Owner] [o]
    WHERE [o].[MasterID] = '1C483E02-E808-42A7-8210-B11F4051D2E4';
    DECLARE @PortalPageID_3884 nvarchar(32)= SCOPE_IDENTITY();

    INSERT INTO [app].[LT_PortalPage] ([LanguageID], [PortalPageID], [Name], [Description])
    SELECT l.[LanguageID], @PortalPageID_3884 AS [PortalPageID], N'Vigo program list' AS [Name], N'' AS [Description]
    FROM [dbo].[Language] l
    WHERE l.[LanguageID] IN (1,2,7);

    DECLARE @DataSourceID_3146_15042992703191 nvarchar(32)= '';
    SELECT @DataSourceID_3146_15042992703191 = [ds].[ItemListDataSourceID]
    FROM [list].[ItemListDataSource] [ds]
    WHERE [ds].[MasterID] = '739567A1-C0A2-4FEF-88A2-2903C050D59C';
    IF NOT EXISTS (SELECT 1 FROM [app].[PortalPart] [pt] WHERE [pt].[MasterID] = '7AD6DD6C-3F60-485D-96D6-7A4E8D19B40F')
    BEGIN
        INSERT INTO [app].[PortalPart] ([PortalPageID], [PortalPartTypeID], [No], [CategoryID], [Settings], [BubbleID], [ReportPartID], [ReportID], [CssClass], [InlineStyle], [ActivityID], [SurveyID], [ExtID], [MasterID])
        SELECT [pp].[PortalPageID] AS [PortalPageID], N'43' AS [PortalPartTypeID], N'1' AS [No], NULL AS [CategoryID], N'<params>DataSourceId%3D'+@DataSourceID_3146_15042992703191+'</params>' AS [Settings], NULL AS [BubbleID], NULL AS [ReportPartID], NULL AS [ReportID], N'' AS [CssClass], N'' AS [InlineStyle], NULL AS [ActivityID], NULL AS [SurveyID], N'' AS [ExtID], N'7AD6DD6C-3F60-485D-96D6-7A4E8D19B40F' AS [MasterID]
        FROM [app].[PortalPage] [pp]
        WHERE [pp].[MasterID] = '99870C3C-FBDF-4946-84F5-D73A8AA0503B';
        DECLARE @PortalPartID_5612 nvarchar(32)= SCOPE_IDENTITY();

        INSERT INTO [app].[LT_PortalPart] ([LanguageID], [PortalPartID], [Name], [Description], [Text], [TextAbove], [TextBelow], [HeaderText], [FooterText], [BeforeContentText], [AfterContentText])
        SELECT l.[LanguageID], @PortalPartID_5612 AS [PortalPartID], N'list program' AS [Name], N'' AS [Description], N'' AS [Text], N'' AS [TextAbove], N'' AS [TextBelow], N'' AS [HeaderText], N'' AS [FooterText], N'' AS [BeforeContentText], N'' AS [AfterContentText]
        FROM [dbo].[Language] l
        WHERE l.[LanguageID] IN (1,2,7);
    END;
END;

IF NOT EXISTS (SELECT 1 FROM [app].[PortalPage] [pp] WHERE [pp].[MasterID] = '7ED53A13-32A7-4E36-847F-A72CCB0045F9')
BEGIN
    INSERT INTO [app].[PortalPage] ([OwnerID], [No], [Parameter], [ExtID], [Created], [MasterID])
    SELECT [o].[OwnerID] AS [OwnerID], N'123' AS [No], N'' AS [Parameter], N'' AS [ExtID], GETDATE() AS [Created], N'7ED53A13-32A7-4E36-847F-A72CCB0045F9' AS [MasterID]
    FROM [org].[Owner] [o]
    WHERE [o].[MasterID] = '1C483E02-E808-42A7-8210-B11F4051D2E4';
    DECLARE @PortalPageID_3885 nvarchar(32)= SCOPE_IDENTITY();

    INSERT INTO [app].[LT_PortalPage] ([LanguageID], [PortalPageID], [Name], [Description])
    SELECT l.[LanguageID], @PortalPageID_3885 AS [PortalPageID], N'Vigo subject content by program' AS [Name], N'' AS [Description]
    FROM [dbo].[Language] l
    WHERE l.[LanguageID] IN (1,2,7);

    DECLARE @DataSourceID_3148_15200133353421 nvarchar(32)= '';
    SELECT @DataSourceID_3148_15200133353421 = [ds].[ItemListDataSourceID]
    FROM [list].[ItemListDataSource] [ds]
    WHERE [ds].[MasterID] = '9513241E-7EEB-4E83-8EC0-1898D9175F70';
    IF NOT EXISTS (SELECT 1 FROM [app].[PortalPart] [pt] WHERE [pt].[MasterID] = 'EE500A1B-EF00-4D3E-89D9-0B017CF1CD47')
    BEGIN
        INSERT INTO [app].[PortalPart] ([PortalPageID], [PortalPartTypeID], [No], [CategoryID], [Settings], [BubbleID], [ReportPartID], [ReportID], [CssClass], [InlineStyle], [ActivityID], [SurveyID], [ExtID], [MasterID])
        SELECT [pp].[PortalPageID] AS [PortalPageID], N'43' AS [PortalPartTypeID], N'1' AS [No], NULL AS [CategoryID], N'<params>DataSourceId%3D'+@DataSourceID_3148_15200133353421+'</params>' AS [Settings], NULL AS [BubbleID], NULL AS [ReportPartID], NULL AS [ReportID], N'' AS [CssClass], N'' AS [InlineStyle], NULL AS [ActivityID], NULL AS [SurveyID], N'' AS [ExtID], N'EE500A1B-EF00-4D3E-89D9-0B017CF1CD47' AS [MasterID]
        FROM [app].[PortalPage] [pp]
        WHERE [pp].[MasterID] = '7ED53A13-32A7-4E36-847F-A72CCB0045F9';
        DECLARE @PortalPartID_5613 nvarchar(32)= SCOPE_IDENTITY();

        INSERT INTO [app].[LT_PortalPart] ([LanguageID], [PortalPartID], [Name], [Description], [Text], [TextAbove], [TextBelow], [HeaderText], [FooterText], [BeforeContentText], [AfterContentText])
        SELECT l.[LanguageID], @PortalPartID_5613 AS [PortalPartID], N'content' AS [Name], N'' AS [Description], N'' AS [Text], N'' AS [TextAbove], N'' AS [TextBelow], N'' AS [HeaderText], N'' AS [FooterText], N'' AS [BeforeContentText], N'' AS [AfterContentText]
        FROM [dbo].[Language] l
        WHERE l.[LanguageID] IN (1,2,7);
    END;
END;
--#endregion PortalPage

--#region DataParameter for source 3146

IF NOT EXISTS (SELECT 1 FROM [list].[ItemListDataParameter] [dp] WHERE [dp].[MasterID] = '6A5E617D-36CA-44F1-B97C-161C94B7B5B4')
BEGIN
    INSERT INTO [list].[ItemListDataParameter] ([ItemListDataSourceID], [DataType], [Type], [Mandatory], [DefaultValue], [ConstructionPattern], [Created], [MasterID])
    SELECT [ds].[ItemListDataSourceID] AS [ItemListDataSourceID], N'String' AS [DataType], N'PortalPartParam' AS [Type], N'0' AS [Mandatory], N''+'VigoProgramList'+'' AS [DefaultValue], N'' AS [ConstructionPattern], GETDATE() AS [Created], N'6A5E617D-36CA-44F1-B97C-161C94B7B5B4' AS [MasterID]
    FROM [list].[ItemListDataSource] [ds]
    WHERE [ds].[MasterID] = '739567A1-C0A2-4FEF-88A2-2903C050D59C';
    DECLARE @ParamID_4695 nvarchar(32)= SCOPE_IDENTITY();

    INSERT INTO [list].[LT_ItemListDataParameter] ([LanguageID], [ItemListDataParameterID], [Name], [Description])
    SELECT l.[LanguageID], @ParamID_4695 AS [ItemListDataParameterID], N'ViewName' AS [Name], N'' AS [Description]
    FROM [dbo].[Language] l
    WHERE l.[LanguageID] IN (1,2,7);
END;
    
DECLARE @PortalPartID_5613_15260650488981 nvarchar(32)= '';
SELECT @PortalPartID_5613_15260650488981 = [pt].[PortalPartID]
FROM [app].[PortalPart] [pt]
WHERE [pt].[MasterID] = 'EE500A1B-EF00-4D3E-89D9-0B017CF1CD47';
IF NOT EXISTS (SELECT 1 FROM [list].[ItemListDataParameter] [dp] WHERE [dp].[MasterID] = '6EAB8EC1-76E0-49C8-89BC-8ECBE921A698')
BEGIN
    INSERT INTO [list].[ItemListDataParameter] ([ItemListDataSourceID], [DataType], [Type], [Mandatory], [DefaultValue], [ConstructionPattern], [Created], [MasterID])
    SELECT [ds].[ItemListDataSourceID] AS [ItemListDataSourceID], N'Integer' AS [DataType], N'PortalPartParam' AS [Type], N'0' AS [Mandatory], N''+@PortalPartID_5613_15260650488981+'' AS [DefaultValue], N'' AS [ConstructionPattern], GETDATE() AS [Created], N'6EAB8EC1-76E0-49C8-89BC-8ECBE921A698' AS [MasterID]
    FROM [list].[ItemListDataSource] [ds]
    WHERE [ds].[MasterID] = '739567A1-C0A2-4FEF-88A2-2903C050D59C';
    DECLARE @ParamID_4701 nvarchar(32)= SCOPE_IDENTITY();

    INSERT INTO [list].[LT_ItemListDataParameter] ([LanguageID], [ItemListDataParameterID], [Name], [Description])
    SELECT l.[LanguageID], @ParamID_4701 AS [ItemListDataParameterID], N'vigocontentportalpartId' AS [Name], N'' AS [Description]
    FROM [dbo].[Language] l
    WHERE l.[LanguageID] IN (1,2,7);
END;

IF NOT EXISTS (SELECT 1 FROM [list].[ItemListDataParameter] [dp] WHERE [dp].[MasterID] = '498F3C63-1E57-497B-ABFB-88056402C313')
BEGIN
    INSERT INTO [list].[ItemListDataParameter] ([ItemListDataSourceID], [DataType], [Type], [Mandatory], [DefaultValue], [ConstructionPattern], [Created], [MasterID])
    SELECT [ds].[ItemListDataSourceID] AS [ItemListDataSourceID], N'Integer' AS [DataType], N'DynamicallyAddedPortalPartParam' AS [Type], N'0' AS [Mandatory], N'' AS [DefaultValue], N'' AS [ConstructionPattern], GETDATE() AS [Created], N'498F3C63-1E57-497B-ABFB-88056402C313' AS [MasterID]
    FROM [list].[ItemListDataSource] [ds]
    WHERE [ds].[MasterID] = '739567A1-C0A2-4FEF-88A2-2903C050D59C';
    DECLARE @ParamID_4703 nvarchar(32)= SCOPE_IDENTITY();

    INSERT INTO [list].[LT_ItemListDataParameter] ([LanguageID], [ItemListDataParameterID], [Name], [Description])
    SELECT l.[LanguageID], @ParamID_4703 AS [ItemListDataParameterID], N'userId' AS [Name], N'' AS [Description]
    FROM [dbo].[Language] l
    WHERE l.[LanguageID] IN (1,2,7);
END;

--#endregion DataParameter for source 3146

INSERT INTO [app].[SiteParameter] ([SiteID], [Key], [Value], [Created], [GroupName])
SELECT s.[SiteID], 'Platform.Admin.CountyDepartmentTypeId' AS [Key], '34' AS [Value], GETDATE() AS [Created], 'Platform.Admin' AS [GroupName]
FROM [app].[Site] s
WHERE NOT EXISTS (SELECT 1 FROM [app].[SiteParameter] sp WHERE sp.[SiteID] = s.[SiteID] AND sp.[Key] = 'Platform.Admin.CountyDepartmentTypeId');

----------------------------------------------------------------------------------------------
UPDATE avt SET avt.[ViewTypeID] = vt.[ViewTypeID], avt.[Template] = pp.[PortalPageID]
FROM [at].[A_VT] avt
JOIN [at].[ViewType] vt ON vt.[MasterID] = '8131CA40-9C0B-43EA-8998-B667986F765C'
 AND avt.[MasterID] IN ('B4D4949E-AAFF-4928-8DD5-95C11C739063','0F5B7F54-9834-457E-8CCD-833F637542A7','3CA5621B-8CB1-4A9F-AAAF-235974ABB80E')
JOIN [app].[PortalPage] pp ON pp.[MasterID] = '99870C3C-FBDF-4946-84F5-D73A8AA0503B';

--UPDATE [list].[ItemListDataSource] SET [FunctionName] = 'getlistsprogramvigo' WHERE [MasterID] = '739567A1-C0A2-4FEF-88A2-2903C050D59C';
UPDATE [list].[ItemListDataSource] SET [FunctionName] = 'getlistprogramesvigo' WHERE [MasterID] = '739567A1-C0A2-4FEF-88A2-2903C050D59C';

--#endregion 085.05 VIGO Config Data

GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
-------------------------------------------------------------------------------------------------------------------
PRINT 'Setting database version'
/*
Start of versioning database-level extended properties.
*/
BEGIN
    DECLARE @DB_VERSION varchar(32), @ConceptName nvarchar(128), @ReleaseScriptName nvarchar(4000)
    SET @DB_VERSION = '6.12.006.001.01'
    SET @ConceptName = 'Engage Main'
    SET @ReleaseScriptName = N'v' + @DB_VERSION + N'-01. Engage_NO VIGO Data'

    IF EXISTS (SELECT 1 FROM fn_listextendedproperty(N'cxStudio Database Version', NULL, NULL, NULL, NULL, NULL, NULL))
        EXEC sp_dropextendedproperty N'cxStudio Database Version', NULL, NULL, NULL, NULL, NULL, NULL
    EXEC sp_addextendedproperty N'cxStudio Database Version', @DB_VERSION, NULL, NULL, NULL, NULL, NULL, NULL
		
    IF EXISTS (SELECT 1 FROM fn_listextendedproperty(N'Script Name', NULL, NULL, NULL, NULL, NULL, NULL))
        EXEC sp_dropextendedproperty N'Script Name', NULL, NULL, NULL, NULL, NULL, NULL
    EXEC sp_addextendedproperty N'Script Name',  @ReleaseScriptName , NULL, NULL, NULL, NULL, NULL, NULL
END
/*
End of versioning database-level extended properties.
*/
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
IF EXISTS (SELECT * FROM #tmpErrors) ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT>0 BEGIN
PRINT 'The database update succeeded'
COMMIT TRANSACTION
END
ELSE PRINT 'The database update failed'
GO
DROP TABLE #tmpErrors
GO
SET NOEXEC OFF
GO