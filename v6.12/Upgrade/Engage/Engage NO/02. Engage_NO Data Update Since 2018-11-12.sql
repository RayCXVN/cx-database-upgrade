SET QUOTED_IDENTIFIER ON
GO
DECLARE @Current_DB_Version varchar(32), @CurrentConceptName nvarchar(128),
        @Required_DB_Version varchar(32), @RequiredConceptName nvarchar(128)

SET @Required_DB_Version = '6.12.006.001.01'
SET @RequiredConceptName = 'Engage Main'
PRINT 'This script is generated for executing on ' + @RequiredConceptName + ' version ' + @Required_DB_Version

SELECT @Current_DB_Version = convert(nvarchar(max),value) FROM fn_listextendedproperty(N'cxStudio Database Version', NULL, NULL, NULL, NULL, NULL, NULL)
SELECT @CurrentConceptName = convert(nvarchar(max),value) FROM fn_listextendedproperty(N'Concept Name', NULL, NULL, NULL, NULL, NULL, NULL)

IF @CurrentConceptName != @RequiredConceptName OR @Current_DB_Version NOT LIKE @Required_DB_Version
BEGIN
    PRINT 'Incorrect solution or cxStudio Database version'
    SET NOEXEC ON
END
GO
-------------------------------------------------------------------------------------------------------------------------
IF EXISTS (SELECT * FROM tempdb..sysobjects WHERE id=OBJECT_ID('tempdb..#tmpErrors')) DROP TABLE #tmpErrors
GO
CREATE TABLE #tmpErrors (Error int)
GO
SET XACT_ABORT ON
GO
SET TRANSACTION ISOLATION LEVEL SERIALIZABLE
GO
BEGIN TRANSACTION
GO
----------------------------------------------------------------------------------------------------------------
--#region 089. Fix A_VT ShowPercentage
-- Dev: David
-- Feature: Improve chart
-- (Manual run) Fix A_VT ShowPercentage
UPDATE [at].[A_VT] SET [ShowPercentage] = 0 WHERE [AVTID] IN (5936,5937,5938,5963,5964,5965,6929,6928,4786,6476,6691,6711,6791,6794,6807,6810,6830,6833,6838,6841,6846,6849,6854,6857,7164,7212,7234,7237,7272,7275,7300,7303,7308,7311,7322,7325,7336,7339,7400,2709,2710,2711,5570,5571,5572,2842,2843,2844,2700,2701,5746,5747,5748,5963,5964,5965,5969,5970,7342,7202,7203,7206,7206,4783,4729,4769,4784,4773,4785,4786,4777,4784,4779,4788,4781,4789);
--#endregion 089. Fix A_VT ShowPercentage
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
--#region 097. Cleanup Unused PortalPage 3878
-- Dev: Thomas
-- Feature: Cleanup
-- Cleanup Unused PortalPage 3878

DELETE pt
FROM [app].[PortalPart] pt
JOIN [app].[PortalPage] pp ON pp.[PortalPageID] = pt.[PortalPageID] AND pp.[MasterID] = '5BCD5A51-1875-4BA7-8769-6910A52848D5';

DELETE pp FROM [app].[PortalPage] pp WHERE pp.[MasterID] = '5BCD5A51-1875-4BA7-8769-6910A52848D5';

--#endregion 097. Cleanup Unused PortalPage 3878
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
-------------------------------------------------------------------------------------------------------------------
PRINT 'Setting database version'
/*
Start of versioning database-level extended properties.
*/
BEGIN
    DECLARE @DB_VERSION varchar(32), @ConceptName nvarchar(128), @ReleaseScriptName nvarchar(4000)
    SET @DB_VERSION = '6.12.006.001.02'
    SET @ConceptName = 'Engage Main'
    SET @ReleaseScriptName = N'v' + @DB_VERSION + N'-02. Engage_NO Data Update Since 2018-11-12'

    IF EXISTS (SELECT 1 FROM fn_listextendedproperty(N'cxStudio Database Version', NULL, NULL, NULL, NULL, NULL, NULL))
        EXEC sp_dropextendedproperty N'cxStudio Database Version', NULL, NULL, NULL, NULL, NULL, NULL
    EXEC sp_addextendedproperty N'cxStudio Database Version', @DB_VERSION, NULL, NULL, NULL, NULL, NULL, NULL
		
    IF EXISTS (SELECT 1 FROM fn_listextendedproperty(N'Script Name', NULL, NULL, NULL, NULL, NULL, NULL))
        EXEC sp_dropextendedproperty N'Script Name', NULL, NULL, NULL, NULL, NULL, NULL
    EXEC sp_addextendedproperty N'Script Name',  @ReleaseScriptName , NULL, NULL, NULL, NULL, NULL, NULL
END
/*
End of versioning database-level extended properties.
*/
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
IF EXISTS (SELECT * FROM #tmpErrors) ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT>0 BEGIN
PRINT 'The database update succeeded'
COMMIT TRANSACTION
END
ELSE PRINT 'The database update failed'
GO
DROP TABLE #tmpErrors
GO
SET NOEXEC OFF
GO