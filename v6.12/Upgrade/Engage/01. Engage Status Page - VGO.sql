SET QUOTED_IDENTIFIER ON
GO
DECLARE @Current_DB_Version varchar(32), @CurrentConceptName nvarchar(128),
        @Required_DB_Version varchar(32), @RequiredConceptName nvarchar(128)

SET @Required_DB_Version = '6.12.006.%'
SET @RequiredConceptName = 'Engage Main'
PRINT 'This script is generated for executing on ' + @RequiredConceptName + ' version ' + @Required_DB_Version

SELECT @Current_DB_Version = convert(nvarchar(max),value) FROM fn_listextendedproperty(N'cxStudio Database Version', NULL, NULL, NULL, NULL, NULL, NULL)
SELECT @CurrentConceptName = convert(nvarchar(max),value) FROM fn_listextendedproperty(N'Concept Name', NULL, NULL, NULL, NULL, NULL, NULL)

IF @CurrentConceptName != @RequiredConceptName OR @Current_DB_Version NOT LIKE @Required_DB_Version
BEGIN
    PRINT 'Incorrect solution or cxStudio Database version'
    SET NOEXEC ON
END
GO
-------------------------------------------------------------------------------------------------------------------------
IF EXISTS (SELECT * FROM tempdb..sysobjects WHERE id=OBJECT_ID('tempdb..#tmpErrors')) DROP TABLE #tmpErrors
GO
CREATE TABLE #tmpErrors (Error int)
GO
SET XACT_ABORT ON
GO
SET TRANSACTION ISOLATION LEVEL SERIALIZABLE
GO
BEGIN TRANSACTION
GO
----------------------------------------------------------------------------------------------------------------
--#region 098. Status Page For VGO

--#region 01. CE-2127 Surveys Status By UG And Subjects
-- Dev: Ray
-- Feature: Status Page
-- CE-2127 Create stored to get data for status page of activity 600 (Oppfølging i fag)
INSERT INTO [app].[SiteParameter] ([SiteID], [Key], [Value], [Created], [GroupName])
SELECT s.[SiteID], 'Vokal.StatusPage.VGOColumnList' AS [Key], N'<StatusColumn Name="Mv1" QuestionMasterID="C4CF1481-18F8-4447-BB8E-0FC02E18C9B1" DataType="text" />
<StatusColumn Name="T1" QuestionMasterID="DF914DD2-941F-45A0-924A-DCFB67281FC3" DataType="text" />
<StatusColumn Name="Mv2" QuestionMasterID="7160E01A-6E37-4BBA-931B-5C782416A515" DataType="text" />
<StatusColumn Name="T2" QuestionMasterID="CF4E288E-16E9-4B64-BBCE-6CC13E7C3CD4" DataType="text" />
<IncludeAlternative Name="AlternativeList" MasterID="9737D88F-9280-459C-B3E2-5037C256207B,ADFC1687-2662-4E46-B636-0A1C36DEF32D,6BDC28B2-C3AC-4F7C-9F9C-BE24956EE0F4,8845CEB4-5443-4BF3-AFAB-D4B1CF33E4C3,BCB0E897-FB68-4201-ADE3-85123EF6F0A7,E73A8F11-0F0A-453B-A192-05F3F3CEA79C,D0F07986-DF4B-46E8-9B74-E034C4A91211" DataType="text" />
' AS [Value], GETDATE() AS [Created], 'Vokal.StatusPage' AS [GroupName]
FROM [app].[Site] s
WHERE NOT EXISTS (SELECT 1 FROM [app].[SiteParameter] sp WHERE sp.[SiteID] = s.[SiteID] AND sp.[Key] = 'Vokal.StatusPage.VGOColumnList');
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[at].[prc_SurveysStatusByUGAndSubject]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [at].[prc_SurveysStatusByUGAndSubject] AS' 
END
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
/*  2018-12-17 Ray:     CE-2127 Create stored to get data for status page of activity 600 (Oppfølging i fag)
    2018-12-26 Ray:     CE-2182 Ignore filter on alternative values (counts - and Fritatt)
                        Excludes deActive users
    2018-12-31 Ray:     Count distinct users

Test cases:
1. School level: @GetSubDepartment = 0
   a. Current/history survey summary: @SurveyID > 0, @ViewDetail = 0 ==> Click view detail: see 1b
   b. Current/history survey detail: @SurveyID > 0, @ViewDetail = 1
   c. All surveys summary: @SurveyID = 0, @ViewDetail = 0 ==> Click view detail: see 1a
2. Municipality and higher levels: @GetSubDepartment = 1
   a. Current survey summary: @SurveyID > 0, @ViewDetail = 0 ==> Click view detail: see 1a
   b. All surveys summary: @SurveyID = 0, @ViewDetail = 0 ==> Click view detail: see 1a (with @HDID = current HD)

School level: EXEC [at].[prc_SurveysStatusByUGAndSubject] @HDID = 39485, @ActivityID = 600, @SurveyID = 4247, @Archetype_TeachingGroup = 7, @SiteID = 5, @ViewDetail = 1, @GetSubDepartment = 0, @SubjectCode = ''
Municipality and higher levels: EXEC [at].[prc_SurveysStatusByUGAndSubject] @HDID = 39384, @ActivityID = 600, @SurveyID = 4247, @Archetype_TeachingGroup = 7, @SiteID = 5, @ViewDetail = 0, @GetSubDepartment = 1, @SubjectCode = ''
*/
ALTER PROCEDURE [at].[prc_SurveysStatusByUGAndSubject]
(   @HDID                       int,
    @ActivityID                 int,
    @SurveyID                   int,
    @Archetype_TeachingGroup    int,
    @SiteID                     int,
    @ViewDetail                 bit = 0,
    @GetSubDepartment           bit = 0,                -- Set this to false when querying the leave nodes, which are schools
    @SubjectCode                nvarchar(max) = ''
) AS
BEGIN
    DECLARE @EntityStatus_Active int = 1, @strIKO_StatusColumn nvarchar(max) = '', @xmlStatusColumn XML, @AlternativeMasterIDList nvarchar(max) = '';
    SELECT @EntityStatus_Active = es.[EntityStatusID] FROM [dbo].[EntityStatus] es WHERE es.[CodeName] = 'Active';

    DECLARE @FilterTable TABLE ([QuestionID] int, [QuestionName] nvarchar(256), [AlternativeID] int, [ItemID] int);
    DECLARE @GroupLevel1 TABLE ([Level1ID] int, [Level1Name] nvarchar(4000));

    SELECT @strIKO_StatusColumn = sp.[Value] FROM [app].[SiteParameter] sp WHERE sp.[SiteID] = @SiteID AND sp.[Key] = 'Vokal.StatusPage.VGOColumnList';
    SET @xmlStatusColumn = CONVERT(xml, @strIKO_StatusColumn)
    SELECT @AlternativeMasterIDList = al.IDList.value('@MasterID', 'nvarchar(max)') FROM @xmlStatusColumn.nodes('//IncludeAlternative') AS al(IDList);

    IF @GetSubDepartment = 1
    BEGIN
        INSERT INTO @GroupLevel1 ([Level1ID], [Level1Name])
        SELECT hd.[HDID] AS [Level1ID], d.[Name] AS [Level1Name]
        FROM [org].[H_D] hd
        JOIN [org].[Department] d ON d.[DepartmentID] = hd.[DepartmentID] AND hd.[ParentID] = @HDID
         AND d.[EntityStatusID] = 1 AND d.[Deleted] IS NULL AND hd.[Deleted] = 0;
    END
    ELSE
    BEGIN
        INSERT INTO @GroupLevel1 ([Level1ID], [Level1Name])
        SELECT hd.[HDID] AS [Level1ID], d.[Name] AS [Level1Name]
        FROM [org].[H_D] hd
        JOIN [org].[Department] d ON d.[DepartmentID] = hd.[DepartmentID] AND hd.[HDID] = @HDID
         AND d.[EntityStatusID] = 1 AND d.[Deleted] IS NULL;
    END

--#region Status detail
    SELECT (CASE WHEN ISNULL(@SurveyID, 0) = 0 THEN s.[SurveyID]
                 WHEN @GetSubDepartment = 0 AND @ViewDetail = 0 THEN c.[CategoryID]
                 WHEN @GetSubDepartment = 0 AND @ViewDetail = 1 THEN ug.[UserGroupID]
                 WHEN @GetSubDepartment = 1 AND @ViewDetail = 0 THEN lv1.[Level1ID]
                 WHEN @GetSubDepartment = 1 AND @ViewDetail = 1 THEN c.[CategoryID]
            END) AS [Level1ID],
           (CASE WHEN ISNULL(@SurveyID, 0) = 0 THEN N'Survey'
                 WHEN @GetSubDepartment = 0 AND @ViewDetail = 0 THEN c.[ExtID]
                 WHEN @GetSubDepartment = 0 AND @ViewDetail = 1 THEN ug.[Name]
                 WHEN @GetSubDepartment = 1 AND @ViewDetail = 0 THEN lv1.[Level1Name]
                 WHEN @GetSubDepartment = 1 AND @ViewDetail = 1 THEN c.[ExtID]
            END) AS [Level1Name],
           s.[SurveyID], c.[ExtID] AS [SubjectCode], c.[CategoryID], ug.[UserGroupID], ug.[Name] AS [UserGroupName]
    INTO #UserGroupDetail
    FROM @GroupLevel1 lv1
    JOIN [org].[H_D] hd ON hd.[Path] LIKE '%\' + CAST(lv1.[Level1ID] AS nvarchar(32)) + '\%' AND hd.[Deleted] = 0
    JOIN [at].[Survey] s ON s.[ActivityID] = @ActivityID AND s.[Status] = 2 AND (s.[SurveyID] = @SurveyID OR ISNULL(@SurveyID, 0) = 0) AND s.[ExtID] <> 'Global'
    JOIN [org].[UserGroup] ug ON ug.[DepartmentID] = hd.[DepartmentID] AND ug.[ArchetypeID] = @Archetype_TeachingGroup 
     AND ug.[PeriodID] = s.[PeriodID] AND ug.[EntityStatusID] = @EntityStatus_Active AND ug.[Deleted] IS NULL
     AND (ug.[Tag] = @SubjectCode OR @SubjectCode = '')
    JOIN [at].[Category] c ON c.[ActivityID] = @ActivityID AND c.[ExtID] = ug.[Tag];

    INSERT INTO @FilterTable ([QuestionID], [QuestionName], [AlternativeID], [ItemID])
    SELECT q.[QuestionID], Tbl.Col.value('@Name', 'nvarchar(max)') AS [QuestionName], NULL AS [AlternativeID], ugd.[CategoryID] AS [ItemID]
    FROM @xmlStatusColumn.nodes('//StatusColumn') AS Tbl(Col)
    JOIN [at].[Question] q ON q.[MasterID] = Tbl.Col.value('@QuestionMasterID', 'nvarchar(64)')
    --CROSS JOIN [dbo].[StringToArray] (@AlternativeMasterIDList, ',') AS altl
    --JOIN [at].[Alternative] alt ON alt.[MasterID] = altl.[ParseValue]
    CROSS JOIN (SELECT DISTINCT [CategoryID] FROM #UserGroupDetail) AS ugd;

    SELECT ugd.[Level1ID], r.[SurveyID], ft.[QuestionName], COUNT(ugm.[UserID]) AS [AnswerCount]
    INTO #AnswerDetail
    FROM #UserGroupDetail ugd
    JOIN [org].[UGMember] ugm ON ugm.[UserGroupID] = ugd.[UserGroupID] AND ugm.[EntityStatusID] = @EntityStatus_Active AND ugm.[Deleted] IS NULL
    JOIN [dbo].[Synonym_VOKAL_Result] r ON r.[SurveyID] = ugd.[SurveyID] AND r.[UserID] = ugm.[UserID]
     AND r.[EntityStatusID] = @EntityStatus_Active AND r.[Deleted] IS NULL
    JOIN @FilterTable ft ON ft.[ItemID] = ugd.[CategoryID]
    JOIN [dbo].[Synonym_VOKAL_Answer] a WITH (INDEX([IX_R_Q_A])) ON a.[ResultID] = r.[ResultID] AND a.[QuestionID] = ft.[QuestionID] --AND a.[AlternativeID] = ft.[AlternativeID]
     AND a.[ItemID] = ft.[ItemID]
    GROUP BY ugd.[Level1ID], r.[SurveyID], ft.[QuestionName];

    SELECT ugd.[Level1ID], ugd.[Level1Name], ugd.[SurveyID], COUNT(DISTINCT ugm.[UserID]) AS [UserCount]
    INTO #UserCount
    FROM #UserGroupDetail ugd
    LEFT JOIN [org].[UGMember] ugm ON ugm.[UserGroupID] = ugd.[UserGroupID] AND ugm.[EntityStatusID] = @EntityStatus_Active AND ugm.[Deleted] IS NULL
    WHERE (ugm.[UserID] IS NULL
           OR EXISTS (SELECT 1 FROM [org].[User] u WHERE u.[UserID] = ugm.[UserID] AND u.[EntityStatusID] = @EntityStatus_Active AND u.[Deleted] IS NULL)
          )
    GROUP BY ugd.[Level1ID], ugd.[Level1Name], ugd.[SurveyID];
--#endregion Status detail

--#region Return data
    SELECT p.[Level1ID], p.[SurveyID], p.[Mv1], p.[T1], p.[Mv2], p.[T2]
    INTO #StatusTable
    FROM #AnswerDetail ad 
    PIVOT (SUM(ad.[AnswerCount]) FOR ad.[QuestionName] IN ([Mv1], [T1], [Mv2], [T2])) AS p;

    SELECT uc.[Level1ID], uc.[Level1Name], uc.[UserCount],
           ISNULL(st.[Mv1],0) AS [Mv1], ISNULL(st.[T1],0) AS [T1], ISNULL(st.[Mv2],0) AS [Mv2], ISNULL(st.[T2],0) AS [T2]
    FROM #UserCount uc
    LEFT JOIN #StatusTable st ON st.[Level1ID] = uc.[Level1ID] AND st.[SurveyID] = uc.[SurveyID];
--#endregion Return data

--#region Cleanup
    DROP TABLE IF EXISTS #UserGroupDetail;
    DROP TABLE IF EXISTS #AnswerDetail;
    DROP TABLE IF EXISTS #StatusTable;
    DROP TABLE IF EXISTS #UserCount;
--#endregion Cleanup

END
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
--#endregion 01. CE-2127 Surveys Status By UG And Subjects

--#region 02. CE-2163 Config Data
-- Dev: Thomas
-- Feature: Status Page For VGO
-- CE-2163 Status page of activity 600 (Oppfølging i fag)

--#region DataSource 2157
IF NOT EXISTS (SELECT 1 FROM [list].[ItemListDataSource] [ds] WHERE [ds].[MasterID] = '19967A4C-DAF5-4F9D-82D4-4EA5606E1A51')
BEGIN
    INSERT INTO [list].[ItemListDataSource] ([OwnerID], [FunctionName], [Created], [MasterID])
    SELECT [o].[OwnerID] AS [OwnerID], N'getobjectfollowupstatus' AS [FunctionName], GETDATE() AS [Created], N'19967A4C-DAF5-4F9D-82D4-4EA5606E1A51' AS [MasterID]
    FROM [org].[Owner] [o]
    WHERE [o].[MasterID] = '1C483E02-E808-42A7-8210-B11F4051D2E4';
    DECLARE @ItemListDataSourceID_2157 nvarchar(32)= SCOPE_IDENTITY();

    INSERT INTO [list].[LT_ItemListDataSource] ([LanguageID], [ItemListDataSourceID], [Name], [Description])
    SELECT l.[LanguageID], @ItemListDataSourceID_2157 AS [ItemListDataSourceID], N'Get Object Follow Up Status' AS [Name], N'getobjectfollowupstatus' AS [Description]
    FROM [dbo].[Language] l
    WHERE l.[LanguageID] IN (1,2,7);

    IF NOT EXISTS (SELECT 1 FROM [list].[ItemListDataParameter] [dp] WHERE [dp].[MasterID] = '2273912B-5039-41CA-B2C5-A9077C419047')
    BEGIN
        INSERT INTO [list].[ItemListDataParameter] ([ItemListDataSourceID], [DataType], [Type], [Mandatory], [DefaultValue], [ConstructionPattern], [Created], [MasterID])
        SELECT [ds].[ItemListDataSourceID] AS [ItemListDataSourceID], N'String' AS [DataType], N'PortalPartParam' AS [Type], N'0' AS [Mandatory], N'' AS [DefaultValue], N'' AS [ConstructionPattern], GETDATE() AS [Created], N'2273912B-5039-41CA-B2C5-A9077C419047' AS [MasterID]
        FROM [list].[ItemListDataSource] [ds]
        WHERE [ds].[MasterID] = '19967A4C-DAF5-4F9D-82D4-4EA5606E1A51';
        DECLARE @ParamID_2759 nvarchar(32)= SCOPE_IDENTITY();

        INSERT INTO [list].[LT_ItemListDataParameter] ([LanguageID], [ItemListDataParameterID], [Name], [Description])
        SELECT l.[LanguageID], @ParamID_2759 AS [ItemListDataParameterID], N'hiddenActivityIds' AS [Name], N'' AS [Description]
        FROM [dbo].[Language] l
        WHERE l.[LanguageID] IN (1,2,7);
    END;

    DECLARE @ActivityID_600_10073248626711 nvarchar(32)= '';
    SELECT @ActivityID_600_10073248626711 = [a].[ActivityID] FROM [at].[Activity] [a] WHERE [a].[MasterID] = 'C9CD0427-CBE9-4AB1-8F96-916771EE444E';
    IF NOT EXISTS (SELECT 1 FROM [list].[ItemListDataParameter] [dp] WHERE [dp].[MasterID] = '1DA5C0EE-244A-45E9-BD93-20B863555DF0')
    BEGIN
        INSERT INTO [list].[ItemListDataParameter] ([ItemListDataSourceID], [DataType], [Type], [Mandatory], [DefaultValue], [ConstructionPattern], [Created], [MasterID])
        SELECT [ds].[ItemListDataSourceID] AS [ItemListDataSourceID], N'String' AS [DataType], N'PortalPartParam' AS [Type], N'0' AS [Mandatory], N''+@ActivityID_600_10073248626711+'' AS [DefaultValue], N'' AS [ConstructionPattern], GETDATE() AS [Created], N'1DA5C0EE-244A-45E9-BD93-20B863555DF0' AS [MasterID]
        FROM [list].[ItemListDataSource] [ds]
        WHERE [ds].[MasterID] = '19967A4C-DAF5-4F9D-82D4-4EA5606E1A51';
        DECLARE @ParamID_2766 nvarchar(32)= SCOPE_IDENTITY();

        INSERT INTO [list].[LT_ItemListDataParameter] ([LanguageID], [ItemListDataParameterID], [Name], [Description])
        SELECT l.[LanguageID], @ParamID_2766 AS [ItemListDataParameterID], N'ActivityId' AS [Name], N'' AS [Description]
        FROM [dbo].[Language] l
        WHERE l.[LanguageID] IN (1,2,7);
    END;
END;
--#endregion DataSource 2157

--#region DataSource 2158
IF NOT EXISTS (SELECT 1 FROM [list].[ItemListDataSource] [ds] WHERE [ds].[MasterID] = '206C97A4-884A-452B-8112-99BD450F9176')
BEGIN
    INSERT INTO [list].[ItemListDataSource] ([OwnerID], [FunctionName], [Created], [MasterID])
    SELECT [o].[OwnerID] AS [OwnerID], N'getobjectfollowupstatusdetail' AS [FunctionName], GETDATE() AS [Created], N'206C97A4-884A-452B-8112-99BD450F9176' AS [MasterID]
    FROM [org].[Owner] [o]
    WHERE [o].[MasterID] = '1C483E02-E808-42A7-8210-B11F4051D2E4';
    DECLARE @ItemListDataSourceID_2158 nvarchar(32)= SCOPE_IDENTITY();

    INSERT INTO [list].[LT_ItemListDataSource] ([LanguageID], [ItemListDataSourceID], [Name], [Description])
    SELECT l.[LanguageID], @ItemListDataSourceID_2158 AS [ItemListDataSourceID], N'Get Object Follow Up Status By Detail' AS [Name], N'getobjectfollowupstatusdetail' AS [Description]
    FROM [dbo].[Language] l
    WHERE l.[LanguageID] IN (1,2,7);

    IF NOT EXISTS (SELECT 1 FROM [list].[ItemListDataParameter] [dp] WHERE [dp].[MasterID] = '0B1F4D80-324D-4551-BBB5-198CEBE8C42D')
    BEGIN
        INSERT INTO [list].[ItemListDataParameter] ([ItemListDataSourceID], [DataType], [Type], [Mandatory], [DefaultValue], [ConstructionPattern], [Created], [MasterID])
        SELECT [ds].[ItemListDataSourceID] AS [ItemListDataSourceID], N'Integer' AS [DataType], N'PortalPartParam' AS [Type], N'1' AS [Mandatory], N'-1' AS [DefaultValue], N'' AS [ConstructionPattern], GETDATE() AS [Created], N'0B1F4D80-324D-4551-BBB5-198CEBE8C42D' AS [MasterID]
        FROM [list].[ItemListDataSource] [ds]
        WHERE [ds].[MasterID] = '206C97A4-884A-452B-8112-99BD450F9176';
        DECLARE @ParamID_2760 nvarchar(32)= SCOPE_IDENTITY();

        INSERT INTO [list].[LT_ItemListDataParameter] ([LanguageID], [ItemListDataParameterID], [Name], [Description])
        SELECT l.[LanguageID], @ParamID_2760 AS [ItemListDataParameterID], N'ActivityID' AS [Name], N'Liste med ActivityIDer for mer eksakt angivelse av aktiviteter' AS [Description]
        FROM [dbo].[Language] l
        WHERE l.[LanguageID] IN (1,2,7);
    END;

    IF NOT EXISTS (SELECT 1 FROM [list].[ItemListDataParameter] [dp] WHERE [dp].[MasterID] = '8388DF71-5254-40D7-B29A-CE6C35073880')
    BEGIN
        INSERT INTO [list].[ItemListDataParameter] ([ItemListDataSourceID], [DataType], [Type], [Mandatory], [DefaultValue], [ConstructionPattern], [Created], [MasterID])
        SELECT [ds].[ItemListDataSourceID] AS [ItemListDataSourceID], N'Integer' AS [DataType], N'PortalPartParam' AS [Type], N'0' AS [Mandatory], N''+'-1'+'' AS [DefaultValue], N'' AS [ConstructionPattern], GETDATE() AS [Created], N'8388DF71-5254-40D7-B29A-CE6C35073880' AS [MasterID]
        FROM [list].[ItemListDataSource] [ds]
        WHERE [ds].[MasterID] = '206C97A4-884A-452B-8112-99BD450F9176';
        DECLARE @ParamID_2761 nvarchar(32)= SCOPE_IDENTITY();

        INSERT INTO [list].[LT_ItemListDataParameter] ([LanguageID], [ItemListDataParameterID], [Name], [Description])
        SELECT l.[LanguageID], @ParamID_2761 AS [ItemListDataParameterID], N'SurveyID' AS [Name], N'' AS [Description]
        FROM [dbo].[Language] l
        WHERE l.[LanguageID] IN (1,2,7);
    END;

    IF NOT EXISTS (SELECT 1 FROM [list].[ItemListDataParameter] [dp] WHERE [dp].[MasterID] = 'E784459E-4FE9-4CB5-824B-EB9CD91899DE')
    BEGIN
        INSERT INTO [list].[ItemListDataParameter] ([ItemListDataSourceID], [DataType], [Type], [Mandatory], [DefaultValue], [ConstructionPattern], [Created], [MasterID])
        SELECT [ds].[ItemListDataSourceID] AS [ItemListDataSourceID], N'String' AS [DataType], N'PortalPartParam' AS [Type], N'0' AS [Mandatory], N'' AS [DefaultValue], N'' AS [ConstructionPattern], GETDATE() AS [Created], N'E784459E-4FE9-4CB5-824B-EB9CD91899DE' AS [MasterID]
        FROM [list].[ItemListDataSource] [ds]
        WHERE [ds].[MasterID] = '206C97A4-884A-452B-8112-99BD450F9176';
        DECLARE @ParamID_2763 nvarchar(32)= SCOPE_IDENTITY();

        INSERT INTO [list].[LT_ItemListDataParameter] ([LanguageID], [ItemListDataParameterID], [Name], [Description])
        SELECT l.[LanguageID], @ParamID_2763 AS [ItemListDataParameterID], N'SubjectCode' AS [Name], N'' AS [Description]
        FROM [dbo].[Language] l
        WHERE l.[LanguageID] IN (1,2,7);
    END;

    IF NOT EXISTS (SELECT 1 FROM [list].[ItemListDataParameter] [dp] WHERE [dp].[MasterID] = '4CF700B0-A909-4B7E-A2B1-EF4DA99E706B')
    BEGIN
        INSERT INTO [list].[ItemListDataParameter] ([ItemListDataSourceID], [DataType], [Type], [Mandatory], [DefaultValue], [ConstructionPattern], [Created], [MasterID])
        SELECT [ds].[ItemListDataSourceID] AS [ItemListDataSourceID], N'String' AS [DataType], N'PortalPartParam' AS [Type], N'0' AS [Mandatory], N'' AS [DefaultValue], N'' AS [ConstructionPattern], GETDATE() AS [Created], N'4CF700B0-A909-4B7E-A2B1-EF4DA99E706B' AS [MasterID]
        FROM [list].[ItemListDataSource] [ds]
        WHERE [ds].[MasterID] = '206C97A4-884A-452B-8112-99BD450F9176';
        DECLARE @ParamID_2767 nvarchar(32)= SCOPE_IDENTITY();

        INSERT INTO [list].[LT_ItemListDataParameter] ([LanguageID], [ItemListDataParameterID], [Name], [Description])
        SELECT l.[LanguageID], @ParamID_2767 AS [ItemListDataParameterID], N'HDID' AS [Name], N'' AS [Description]
        FROM [dbo].[Language] l
        WHERE l.[LanguageID] IN (1,2,7);
    END;
END;
--#endregion DataSource 2158

--#region DataSource 2159
IF NOT EXISTS (SELECT 1 FROM [list].[ItemListDataSource] [ds] WHERE [ds].[MasterID] = 'D6B1D0A0-0278-4B2D-8E88-1D77458A6EA2')
BEGIN
    INSERT INTO [list].[ItemListDataSource] ([OwnerID], [FunctionName], [Created], [MasterID])
    SELECT [o].[OwnerID] AS [OwnerID], N'getobjectfollowupstatusperiods' AS [FunctionName], GETDATE() AS [Created], N'D6B1D0A0-0278-4B2D-8E88-1D77458A6EA2' AS [MasterID]
    FROM [org].[Owner] [o]
    WHERE [o].[MasterID] = '1C483E02-E808-42A7-8210-B11F4051D2E4';
    DECLARE @ItemListDataSourceID_2159 nvarchar(32)= SCOPE_IDENTITY();

    INSERT INTO [list].[LT_ItemListDataSource] ([LanguageID], [ItemListDataSourceID], [Name], [Description])
    SELECT l.[LanguageID], @ItemListDataSourceID_2159 AS [ItemListDataSourceID], N'Get Object Follow Up Status On Periods' AS [Name], N'getobjectfollowupstatusperiods' AS [Description]
    FROM [dbo].[Language] l
    WHERE l.[LanguageID] IN (1,2,7);

    IF NOT EXISTS (SELECT 1 FROM [list].[ItemListDataParameter] [dp] WHERE [dp].[MasterID] = '367C03EF-9457-4819-B8DE-4992A5B1D556')
    BEGIN
        INSERT INTO [list].[ItemListDataParameter] ([ItemListDataSourceID], [DataType], [Type], [Mandatory], [DefaultValue], [ConstructionPattern], [Created], [MasterID])
        SELECT [ds].[ItemListDataSourceID] AS [ItemListDataSourceID], N'Integer' AS [DataType], N'DynamicallyAddedPortalPartParam' AS [Type], N'1' AS [Mandatory], N'-1' AS [DefaultValue], N'' AS [ConstructionPattern], GETDATE() AS [Created], N'367C03EF-9457-4819-B8DE-4992A5B1D556' AS [MasterID]
        FROM [list].[ItemListDataSource] [ds]
        WHERE [ds].[MasterID] = 'D6B1D0A0-0278-4B2D-8E88-1D77458A6EA2';
        DECLARE @ParamID_2764 nvarchar(32)= SCOPE_IDENTITY();

        INSERT INTO [list].[LT_ItemListDataParameter] ([LanguageID], [ItemListDataParameterID], [Name], [Description])
        SELECT l.[LanguageID], @ParamID_2764 AS [ItemListDataParameterID], N'ActivityID' AS [Name], N'ActivityID' AS [Description]
        FROM [dbo].[Language] l
        WHERE l.[LanguageID] IN (1,2,7);
    END;

    IF NOT EXISTS (SELECT 1 FROM [list].[ItemListDataParameter] [dp] WHERE [dp].[MasterID] = 'E57E5A28-9FAC-413B-B791-03BC6B54B55F')
    BEGIN
        INSERT INTO [list].[ItemListDataParameter] ([ItemListDataSourceID], [DataType], [Type], [Mandatory], [DefaultValue], [ConstructionPattern], [Created], [MasterID])
        SELECT [ds].[ItemListDataSourceID] AS [ItemListDataSourceID], N'String' AS [DataType], N'PortalPartParam' AS [Type], N'0' AS [Mandatory], N'&lt;span class=''icon-vokal-plus-circle''&gt;&lt;/span&gt;' AS [DefaultValue], N'' AS [ConstructionPattern], GETDATE() AS [Created], N'E57E5A28-9FAC-413B-B791-03BC6B54B55F' AS [MasterID]
        FROM [list].[ItemListDataSource] [ds]
        WHERE [ds].[MasterID] = 'D6B1D0A0-0278-4B2D-8E88-1D77458A6EA2';
        DECLARE @ParamID_2765 nvarchar(32)= SCOPE_IDENTITY();

        INSERT INTO [list].[LT_ItemListDataParameter] ([LanguageID], [ItemListDataParameterID], [Name], [Description])
        SELECT l.[LanguageID], @ParamID_2765 AS [ItemListDataParameterID], N'IconExpanding' AS [Name], N'' AS [Description]
        FROM [dbo].[Language] l
        WHERE l.[LanguageID] IN (1,2,7);
    END;
END;
--#endregion DataSource 2159

--#region DataParam 2770
IF NOT EXISTS (SELECT 1 FROM [list].[ItemListDataParameter] [dp] WHERE [dp].[MasterID] = '98EEDF90-3475-40FD-BC19-29CBDFE1735D')
BEGIN
    INSERT INTO [list].[ItemListDataParameter] ([ItemListDataSourceID], [DataType], [Type], [Mandatory], [DefaultValue], [ConstructionPattern], [Created], [MasterID])
    SELECT [ds].[ItemListDataSourceID] AS [ItemListDataSourceID], N'String' AS [DataType], N'PortalPartParam' AS [Type], N'0' AS [Mandatory], N''+'0'+'' AS [DefaultValue], N'' AS [ConstructionPattern], GETDATE() AS [Created], N'98EEDF90-3475-40FD-BC19-29CBDFE1735D' AS [MasterID]
    FROM [list].[ItemListDataSource] [ds]
    WHERE [ds].[MasterID] = 'F70DDA3E-C348-4D8F-950A-EADDA2D7317C';
    DECLARE @ParamID_2770 nvarchar(32)= SCOPE_IDENTITY();

    INSERT INTO [list].[LT_ItemListDataParameter] ([LanguageID], [ItemListDataParameterID], [Name], [Description])
    SELECT l.[LanguageID], @ParamID_2770 AS [ItemListDataParameterID], N'ObjectFollowUpActivityId' AS [Name], N'' AS [Description]
    FROM [dbo].[Language] l
    WHERE l.[LanguageID] IN (1,2,7);
END;
--#endregion DataParam 2770

--#region ItemList 1407
DECLARE @DataSourceID_2157_102427502333214071 nvarchar(32)= '';
SELECT @DataSourceID_2157_102427502333214071 = [ds].[ItemListDataSourceID]
FROM [list].[ItemListDataSource] [ds]
WHERE [ds].[MasterID] = '19967A4C-DAF5-4F9D-82D4-4EA5606E1A51';
IF NOT EXISTS (SELECT 1 FROM [list].[ItemList] [l] WHERE [l].[MasterID] = '422130C6-7601-4288-9D87-96144387C6DA')
BEGIN
    INSERT INTO [list].[ItemList] ([OwnerID], [ItemListDataSourceID], [ListTableCSSClass], [ListTableCSSCStyle], [ListHeaderRowCSSClass], [ListRowCSSClass], [ListAltRowCSSClass], [ExpandingItemListDatasourceID], [ExpandingListRowCSSClass], [ExpandingListAltRowCSSClass], [ExpandingExpression], [InsertMultiCheckColumn], [DisableCheckColumnExpression], [DisplayUpperCmdBarThreshold], [MaxRows], [ExtID], [Created], [MasterID])
    SELECT [o].[OwnerID] AS [OwnerID], @DataSourceID_2157_102427502333214071 AS [ItemListDataSourceID], N'table-user stripe no-border tablesorter tablesorter-default' AS [ListTableCSSClass], N'' AS [ListTableCSSCStyle], N'header' AS [ListHeaderRowCSSClass], N'' AS [ListRowCSSClass], N'' AS [ListAltRowCSSClass], NULL AS [ExpandingItemListDatasourceID], N'' AS [ExpandingListRowCSSClass], N'' AS [ExpandingListAltRowCSSClass], N'' AS [ExpandingExpression], N'0' AS [InsertMultiCheckColumn], N'' AS [DisableCheckColumnExpression], N'0' AS [DisplayUpperCmdBarThreshold], N'-1' AS [MaxRows], N'' AS [ExtID], GETDATE() AS [Created], N'422130C6-7601-4288-9D87-96144387C6DA' AS [MasterID]
    FROM [org].[Owner] [o]
    WHERE [o].[MasterID] = '1C483E02-E808-42A7-8210-B11F4051D2E4';
    DECLARE @ItemListID_1407 nvarchar(32)= SCOPE_IDENTITY();

    INSERT INTO [list].[LT_ItemList] ([LanguageID], [ItemListID], [Name], [Description], [RowTooltipText], [EmptyListText])
    SELECT l.[LanguageID], @ItemListID_1407 AS [ItemListID], N'[Status content] Object Follow Up' AS [Name], N'' AS [Description], N'' AS [RowTooltipText], N'<div class="empty-list">
        <img src="{GetBaseUrl()}/Content/Styles/Themes/Magnum/Images/no-elements.png" alt="Empty elements">
        <span>Ingen elementer i listen</span>
    </div>' AS [EmptyListText]
    FROM [dbo].[Language] l
    WHERE l.[LanguageID] IN (1,7);

    INSERT INTO [list].[LT_ItemList] ([LanguageID], [ItemListID], [Name], [Description], [RowTooltipText], [EmptyListText])
    SELECT N'2' AS [LanguageID], @ItemListID_1407 AS [ItemListID], N'[Status content] Object Follow Up' AS [Name], N'' AS [Description], N'' AS [RowTooltipText], N'<div class="empty-list">
        <img src="{GetBaseUrl()}/Content/Styles/Themes/Magnum/Images/no-elements.png" alt="Empty elements">
        <span>No elements in list</span>
    </div>' AS [EmptyListText];

    IF NOT EXISTS (SELECT 1 FROM [list].[ItemListRowAttribute] [att] WHERE [att].[MasterID] = '28C8760C-A2D6-436C-97EA-FBA5E93A5637')
    BEGIN
        INSERT INTO [list].[ItemListRowAttribute] ([ItemListID], [Key], [Value], [Created], [MasterID])
        SELECT [l].[ItemListID] AS [ItemListID], N'data-params' AS [Key], N''+'600-0'+'' AS [Value], GETDATE() AS [Created], N'28C8760C-A2D6-436C-97EA-FBA5E93A5637' AS [MasterID]
        FROM [list].[ItemList] [l]
        WHERE [l].[MasterID] = '422130C6-7601-4288-9D87-96144387C6DA';
    END;

    IF NOT EXISTS (SELECT 1 FROM [list].[ItemListRowAttribute] [att] WHERE [att].[MasterID] = '07DA84F7-0BDA-4FBD-A04E-80A197D7C928')
    BEGIN
        INSERT INTO [list].[ItemListRowAttribute] ([ItemListID], [Key], [Value], [Created], [MasterID])
        SELECT [l].[ItemListID] AS [ItemListID], N'data-id' AS [Key], N'{if({"{GetSubDepartment}"="0"},"{CategoryID}","{Level1ID}")}' AS [Value], GETDATE() AS [Created], N'07DA84F7-0BDA-4FBD-A04E-80A197D7C928' AS [MasterID]
        FROM [list].[ItemList] [l]
        WHERE [l].[MasterID] = '422130C6-7601-4288-9D87-96144387C6DA';
    END;

    IF NOT EXISTS (SELECT 1 FROM [list].[ItemListRowAttribute] [att] WHERE [att].[MasterID] = 'B5D30170-57D7-49A6-8B49-3D8386190464')
    BEGIN
        INSERT INTO [list].[ItemListRowAttribute] ([ItemListID], [Key], [Value], [Created], [MasterID])
        SELECT [l].[ItemListID] AS [ItemListID], N'data-subjectcode' AS [Key], N''+'{SubjectCode}'+'' AS [Value], GETDATE() AS [Created], N'B5D30170-57D7-49A6-8B49-3D8386190464' AS [MasterID]
        FROM [list].[ItemList] [l]
        WHERE [l].[MasterID] = '422130C6-7601-4288-9D87-96144387C6DA';
    END;

    IF NOT EXISTS (SELECT 1 FROM [list].[ItemListRowAttribute] [att] WHERE [att].[MasterID] = '6DDD817F-EBB3-4B2E-B81B-E494F31561DA')
    BEGIN
        INSERT INTO [list].[ItemListRowAttribute] ([ItemListID], [Key], [Value], [Created], [MasterID])
        SELECT [l].[ItemListID] AS [ItemListID], N'data-hdid' AS [Key], N'{if({"{GetSubDepartment}"="0"},"","{Level1ID}")}' AS [Value], GETDATE() AS [Created], N'6DDD817F-EBB3-4B2E-B81B-E494F31561DA' AS [MasterID]
        FROM [list].[ItemList] [l]
        WHERE [l].[MasterID] = '422130C6-7601-4288-9D87-96144387C6DA';
    END;

    IF NOT EXISTS (SELECT 1 FROM [list].[ItemListField] [f] WHERE [f].[MasterID] = '00773C12-2B03-4151-8972-612FC6A03AC5')
    BEGIN
        INSERT INTO [list].[ItemListField] ([ItemListID], [No], [TableTypeID], [FieldType], [ValueType], [FieldName], [PropID], [QuestionID], [OverrideLocked], [ShowInUserList], [ShowInTableView], [ShowInExport], [Mandatory], [Sortable], [SortNo], [SortDirection], [Width], [Align], [ItemPrefix], [ItemSuffix], [HeaderCssClass], [ItemCssClass], [Created], [ColumnCssClass], [HeaderGroupCSSClass], [HeaderGroupLastItem], [MasterID])
        SELECT [l].[ItemListID] AS [ItemListID], N'1' AS [No], NULL AS [TableTypeID], N'0' AS [FieldType], N'0' AS [ValueType], N'Navn' AS [FieldName], N'0' AS [PropID], NULL AS [QuestionID], N'0' AS [OverrideLocked], N'0' AS [ShowInUserList], N'0' AS [ShowInTableView], N'0' AS [ShowInExport], N'0' AS [Mandatory], N'1' AS [Sortable], N'1' AS [SortNo], N'1' AS [SortDirection], N'0' AS [Width], N'0' AS [Align], N'' AS [ItemPrefix], N'' AS [ItemSuffix], N'' AS [HeaderCssClass], N'command' AS [ItemCssClass], GETDATE() AS [Created], N'' AS [ColumnCssClass], N'' AS [HeaderGroupCSSClass], N'0' AS [HeaderGroupLastItem], N'00773C12-2B03-4151-8972-612FC6A03AC5' AS [MasterID]
        FROM [list].[ItemList] [l]
        WHERE [l].[MasterID] = '422130C6-7601-4288-9D87-96144387C6DA';
        DECLARE @ItemListFieldID_3730 nvarchar(32)= SCOPE_IDENTITY();

        INSERT INTO [list].[LT_ItemListField] ([LanguageID], [ItemListFieldID], [Name], [Description], [HeaderText], [HeaderGroupText], [HeaderTooltipText], [ItemTooltipText], [ItemText], [ExpandingItemText], [DefaultValues], [AvailableValues])
        SELECT l.[LanguageID], @ItemListFieldID_3730 AS [ItemListFieldID], N'Navn' AS [Name], N'' AS [Description], N'Navn' AS [HeaderText], N'' AS [HeaderGroupText], N'' AS [HeaderTooltipText], N'' AS [ItemTooltipText], N'<span class="icon-vokal-right-line-arrow btn-toggle"></span>
<a href="#">{if({"{GetSubDepartment}"="0"},"{SubjectName}","{Level1Name}")}</a>' AS [ItemText], N'' AS [ExpandingItemText], N'' AS [DefaultValues], N'' AS [AvailableValues]
        FROM [dbo].[Language] l
        WHERE l.[LanguageID] IN (1,7);

        INSERT INTO [list].[LT_ItemListField] ([LanguageID], [ItemListFieldID], [Name], [Description], [HeaderText], [HeaderGroupText], [HeaderTooltipText], [ItemTooltipText], [ItemText], [ExpandingItemText], [DefaultValues], [AvailableValues])
        SELECT N'2' AS [LanguageID], @ItemListFieldID_3730 AS [ItemListFieldID], N'Name' AS [Name], N'' AS [Description], N'Name' AS [HeaderText], N'' AS [HeaderGroupText], N'' AS [HeaderTooltipText], N'' AS [ItemTooltipText], N'<span class="icon-vokal-right-line-arrow btn-toggle"></span>
<a href="#">{if({"{GetSubDepartment}"="0"},"{SubjectName}","{Level1Name}")}</a>' AS [ItemText], N'' AS [ExpandingItemText], N'' AS [DefaultValues], N'' AS [AvailableValues];
    END;

    IF NOT EXISTS (SELECT 1 FROM [list].[ItemListField] [f] WHERE [f].[MasterID] = '9C5B25BD-2748-4F23-94D5-960867637106')
    BEGIN
        INSERT INTO [list].[ItemListField] ([ItemListID], [No], [TableTypeID], [FieldType], [ValueType], [FieldName], [PropID], [QuestionID], [OverrideLocked], [ShowInUserList], [ShowInTableView], [ShowInExport], [Mandatory], [Sortable], [SortNo], [SortDirection], [Width], [Align], [ItemPrefix], [ItemSuffix], [HeaderCssClass], [ItemCssClass], [Created], [ColumnCssClass], [HeaderGroupCSSClass], [HeaderGroupLastItem], [MasterID])
        SELECT [l].[ItemListID] AS [ItemListID], N'2' AS [No], NULL AS [TableTypeID], N'0' AS [FieldType], N'0' AS [ValueType], N'PeriodName' AS [FieldName], N'0' AS [PropID], NULL AS [QuestionID], N'0' AS [OverrideLocked], N'0' AS [ShowInUserList], N'0' AS [ShowInTableView], N'0' AS [ShowInExport], N'0' AS [Mandatory], N'1' AS [Sortable], N'-1' AS [SortNo], N'1' AS [SortDirection], N'120' AS [Width], N'0' AS [Align], N'' AS [ItemPrefix], N'' AS [ItemSuffix], N'' AS [HeaderCssClass], N'' AS [ItemCssClass], GETDATE() AS [Created], N'' AS [ColumnCssClass], N'' AS [HeaderGroupCSSClass], N'0' AS [HeaderGroupLastItem], N'9C5B25BD-2748-4F23-94D5-960867637106' AS [MasterID]
        FROM [list].[ItemList] [l]
        WHERE [l].[MasterID] = '422130C6-7601-4288-9D87-96144387C6DA';
        DECLARE @ItemListFieldID_3731 nvarchar(32)= SCOPE_IDENTITY();

        INSERT INTO [list].[LT_ItemListField] ([LanguageID], [ItemListFieldID], [Name], [Description], [HeaderText], [HeaderGroupText], [HeaderTooltipText], [ItemTooltipText], [ItemText], [ExpandingItemText], [DefaultValues], [AvailableValues])
        SELECT l.[LanguageID], @ItemListFieldID_3731 AS [ItemListFieldID], N'Periode' AS [Name], N'Period' AS [Description], N'PERIODE' AS [HeaderText], N'' AS [HeaderGroupText], N'' AS [HeaderTooltipText], N'' AS [ItemTooltipText], N'{SurveyName}' AS [ItemText], N'' AS [ExpandingItemText], N'' AS [DefaultValues], N'' AS [AvailableValues]
        FROM [dbo].[Language] l
        WHERE l.[LanguageID] IN (1,7);

        INSERT INTO [list].[LT_ItemListField] ([LanguageID], [ItemListFieldID], [Name], [Description], [HeaderText], [HeaderGroupText], [HeaderTooltipText], [ItemTooltipText], [ItemText], [ExpandingItemText], [DefaultValues], [AvailableValues])
        SELECT N'2' AS [LanguageID], @ItemListFieldID_3731 AS [ItemListFieldID], N'Period' AS [Name], N'Period' AS [Description], N'Period' AS [HeaderText], N'' AS [HeaderGroupText], N'' AS [HeaderTooltipText], N'' AS [ItemTooltipText], N'{SurveyName}' AS [ItemText], N'' AS [ExpandingItemText], N'' AS [DefaultValues], N'' AS [AvailableValues];
    END;

    IF NOT EXISTS (SELECT 1 FROM [list].[ItemListField] [f] WHERE [f].[MasterID] = 'C7EA6B6C-77D8-4679-9604-257ABCCD008B')
    BEGIN
        INSERT INTO [list].[ItemListField] ([ItemListID], [No], [TableTypeID], [FieldType], [ValueType], [FieldName], [PropID], [QuestionID], [OverrideLocked], [ShowInUserList], [ShowInTableView], [ShowInExport], [Mandatory], [Sortable], [SortNo], [SortDirection], [Width], [Align], [ItemPrefix], [ItemSuffix], [HeaderCssClass], [ItemCssClass], [Created], [ColumnCssClass], [HeaderGroupCSSClass], [HeaderGroupLastItem], [MasterID])
        SELECT [l].[ItemListID] AS [ItemListID], N'3' AS [No], NULL AS [TableTypeID], N'0' AS [FieldType], N'0' AS [ValueType], N'UserCount' AS [FieldName], N'0' AS [PropID], NULL AS [QuestionID], N'0' AS [OverrideLocked], N'0' AS [ShowInUserList], N'0' AS [ShowInTableView], N'0' AS [ShowInExport], N'0' AS [Mandatory], N'1' AS [Sortable], N'-1' AS [SortNo], N'1' AS [SortDirection], N'120' AS [Width], N'1' AS [Align], N'' AS [ItemPrefix], N'' AS [ItemSuffix], N'' AS [HeaderCssClass], N'' AS [ItemCssClass], GETDATE() AS [Created], N'' AS [ColumnCssClass], N'' AS [HeaderGroupCSSClass], N'0' AS [HeaderGroupLastItem], N'C7EA6B6C-77D8-4679-9604-257ABCCD008B' AS [MasterID]
        FROM [list].[ItemList] [l]
        WHERE [l].[MasterID] = '422130C6-7601-4288-9D87-96144387C6DA';
        DECLARE @ItemListFieldID_3732 nvarchar(32)= SCOPE_IDENTITY();

        INSERT INTO [list].[LT_ItemListField] ([LanguageID], [ItemListFieldID], [Name], [Description], [HeaderText], [HeaderGroupText], [HeaderTooltipText], [ItemTooltipText], [ItemText], [ExpandingItemText], [DefaultValues], [AvailableValues])
        SELECT l.[LanguageID], @ItemListFieldID_3732 AS [ItemListFieldID], N'Antall elever' AS [Name], N'Antall elever' AS [Description], N'Antall elever' AS [HeaderText], N'' AS [HeaderGroupText], N'' AS [HeaderTooltipText], N'' AS [ItemTooltipText], N'{UserCount}' AS [ItemText], N'' AS [ExpandingItemText], N'' AS [DefaultValues], N'' AS [AvailableValues]
        FROM [dbo].[Language] l
        WHERE l.[LanguageID] IN (1,7);

        INSERT INTO [list].[LT_ItemListField] ([LanguageID], [ItemListFieldID], [Name], [Description], [HeaderText], [HeaderGroupText], [HeaderTooltipText], [ItemTooltipText], [ItemText], [ExpandingItemText], [DefaultValues], [AvailableValues])
        SELECT N'2' AS [LanguageID], @ItemListFieldID_3732 AS [ItemListFieldID], N'Number of students' AS [Name], N'Number of students' AS [Description], N'Number of students' AS [HeaderText], N'' AS [HeaderGroupText], N'' AS [HeaderTooltipText], N'' AS [ItemTooltipText], N'{UserCount}' AS [ItemText], N'' AS [ExpandingItemText], N'' AS [DefaultValues], N'' AS [AvailableValues];
    END;

    IF NOT EXISTS (SELECT 1 FROM [list].[ItemListField] [f] WHERE [f].[MasterID] = 'C146D36C-EFDB-4B1F-A932-ABD2DBA4C6F3')
    BEGIN
        INSERT INTO [list].[ItemListField] ([ItemListID], [No], [TableTypeID], [FieldType], [ValueType], [FieldName], [PropID], [QuestionID], [OverrideLocked], [ShowInUserList], [ShowInTableView], [ShowInExport], [Mandatory], [Sortable], [SortNo], [SortDirection], [Width], [Align], [ItemPrefix], [ItemSuffix], [HeaderCssClass], [ItemCssClass], [Created], [ColumnCssClass], [HeaderGroupCSSClass], [HeaderGroupLastItem], [MasterID])
        SELECT [l].[ItemListID] AS [ItemListID], N'4' AS [No], NULL AS [TableTypeID], N'0' AS [FieldType], N'0' AS [ValueType], N'Mv1' AS [FieldName], N'0' AS [PropID], NULL AS [QuestionID], N'0' AS [OverrideLocked], N'0' AS [ShowInUserList], N'0' AS [ShowInTableView], N'0' AS [ShowInExport], N'0' AS [Mandatory], N'1' AS [Sortable], N'-1' AS [SortNo], N'1' AS [SortDirection], N'120' AS [Width], N'1' AS [Align], N'' AS [ItemPrefix], N'' AS [ItemSuffix], N'' AS [HeaderCssClass], N'' AS [ItemCssClass], GETDATE() AS [Created], N'' AS [ColumnCssClass], N'' AS [HeaderGroupCSSClass], N'0' AS [HeaderGroupLastItem], N'C146D36C-EFDB-4B1F-A932-ABD2DBA4C6F3' AS [MasterID]
        FROM [list].[ItemList] [l]
        WHERE [l].[MasterID] = '422130C6-7601-4288-9D87-96144387C6DA';
        DECLARE @ItemListFieldID_3733 nvarchar(32)= SCOPE_IDENTITY();

        INSERT INTO [list].[LT_ItemListField] ([LanguageID], [ItemListFieldID], [Name], [Description], [HeaderText], [HeaderGroupText], [HeaderTooltipText], [ItemTooltipText], [ItemText], [ExpandingItemText], [DefaultValues], [AvailableValues])
        SELECT l.[LanguageID], @ItemListFieldID_3733 AS [ItemListFieldID], N'KARAKTER MV1' AS [Name], N'KARAKTER MV1' AS [Description], N'KARAKTER MV1' AS [HeaderText], N'' AS [HeaderGroupText], N'' AS [HeaderTooltipText], N'' AS [ItemTooltipText], N'{Mv1}' AS [ItemText], N'' AS [ExpandingItemText], N'' AS [DefaultValues], N'' AS [AvailableValues]
        FROM [dbo].[Language] l
        WHERE l.[LanguageID] IN (1,7);

        INSERT INTO [list].[LT_ItemListField] ([LanguageID], [ItemListFieldID], [Name], [Description], [HeaderText], [HeaderGroupText], [HeaderTooltipText], [ItemTooltipText], [ItemText], [ExpandingItemText], [DefaultValues], [AvailableValues])
        SELECT N'2' AS [LanguageID], @ItemListFieldID_3733 AS [ItemListFieldID], N'MV1' AS [Name], N'MV1' AS [Description], N'MV1' AS [HeaderText], N'' AS [HeaderGroupText], N'' AS [HeaderTooltipText], N'' AS [ItemTooltipText], N'{Mv1}' AS [ItemText], N'' AS [ExpandingItemText], N'' AS [DefaultValues], N'' AS [AvailableValues];
    END;

    IF NOT EXISTS (SELECT 1 FROM [list].[ItemListField] [f] WHERE [f].[MasterID] = '8A78176C-BE80-471E-9EC8-303B0E12848A')
    BEGIN
        INSERT INTO [list].[ItemListField] ([ItemListID], [No], [TableTypeID], [FieldType], [ValueType], [FieldName], [PropID], [QuestionID], [OverrideLocked], [ShowInUserList], [ShowInTableView], [ShowInExport], [Mandatory], [Sortable], [SortNo], [SortDirection], [Width], [Align], [ItemPrefix], [ItemSuffix], [HeaderCssClass], [ItemCssClass], [Created], [ColumnCssClass], [HeaderGroupCSSClass], [HeaderGroupLastItem], [MasterID])
        SELECT [l].[ItemListID] AS [ItemListID], N'5' AS [No], NULL AS [TableTypeID], N'0' AS [FieldType], N'0' AS [ValueType], N'T1' AS [FieldName], N'0' AS [PropID], NULL AS [QuestionID], N'0' AS [OverrideLocked], N'0' AS [ShowInUserList], N'0' AS [ShowInTableView], N'0' AS [ShowInExport], N'0' AS [Mandatory], N'1' AS [Sortable], N'-1' AS [SortNo], N'1' AS [SortDirection], N'120' AS [Width], N'1' AS [Align], N'' AS [ItemPrefix], N'' AS [ItemSuffix], N'' AS [HeaderCssClass], N'' AS [ItemCssClass], GETDATE() AS [Created], N'' AS [ColumnCssClass], N'' AS [HeaderGroupCSSClass], N'0' AS [HeaderGroupLastItem], N'8A78176C-BE80-471E-9EC8-303B0E12848A' AS [MasterID]
        FROM [list].[ItemList] [l]
        WHERE [l].[MasterID] = '422130C6-7601-4288-9D87-96144387C6DA';
        DECLARE @ItemListFieldID_3734 nvarchar(32)= SCOPE_IDENTITY();

        INSERT INTO [list].[LT_ItemListField] ([LanguageID], [ItemListFieldID], [Name], [Description], [HeaderText], [HeaderGroupText], [HeaderTooltipText], [ItemTooltipText], [ItemText], [ExpandingItemText], [DefaultValues], [AvailableValues])
        SELECT l.[LanguageID], @ItemListFieldID_3734 AS [ItemListFieldID], N'KARAKTER T1' AS [Name], N'KARAKTER T1' AS [Description], N'KARAKTER T1' AS [HeaderText], N'' AS [HeaderGroupText], N'' AS [HeaderTooltipText], N'' AS [ItemTooltipText], N'{T1}' AS [ItemText], N'' AS [ExpandingItemText], N'' AS [DefaultValues], N'' AS [AvailableValues]
        FROM [dbo].[Language] l
        WHERE l.[LanguageID] IN (1,7);

        INSERT INTO [list].[LT_ItemListField] ([LanguageID], [ItemListFieldID], [Name], [Description], [HeaderText], [HeaderGroupText], [HeaderTooltipText], [ItemTooltipText], [ItemText], [ExpandingItemText], [DefaultValues], [AvailableValues])
        SELECT N'2' AS [LanguageID], @ItemListFieldID_3734 AS [ItemListFieldID], N'T1' AS [Name], N'T1' AS [Description], N'T1' AS [HeaderText], N'' AS [HeaderGroupText], N'' AS [HeaderTooltipText], N'' AS [ItemTooltipText], N'{T1}' AS [ItemText], N'' AS [ExpandingItemText], N'' AS [DefaultValues], N'' AS [AvailableValues];
    END;

    IF NOT EXISTS (SELECT 1 FROM [list].[ItemListField] [f] WHERE [f].[MasterID] = '04289A7A-5B9E-44E5-96FF-20A8EF75B08B')
    BEGIN
        INSERT INTO [list].[ItemListField] ([ItemListID], [No], [TableTypeID], [FieldType], [ValueType], [FieldName], [PropID], [QuestionID], [OverrideLocked], [ShowInUserList], [ShowInTableView], [ShowInExport], [Mandatory], [Sortable], [SortNo], [SortDirection], [Width], [Align], [ItemPrefix], [ItemSuffix], [HeaderCssClass], [ItemCssClass], [Created], [ColumnCssClass], [HeaderGroupCSSClass], [HeaderGroupLastItem], [MasterID])
        SELECT [l].[ItemListID] AS [ItemListID], N'6' AS [No], NULL AS [TableTypeID], N'0' AS [FieldType], N'0' AS [ValueType], N'Mv2' AS [FieldName], N'0' AS [PropID], NULL AS [QuestionID], N'0' AS [OverrideLocked], N'0' AS [ShowInUserList], N'0' AS [ShowInTableView], N'0' AS [ShowInExport], N'0' AS [Mandatory], N'1' AS [Sortable], N'-1' AS [SortNo], N'1' AS [SortDirection], N'120' AS [Width], N'1' AS [Align], N'' AS [ItemPrefix], N'' AS [ItemSuffix], N'' AS [HeaderCssClass], N'' AS [ItemCssClass], GETDATE() AS [Created], N'' AS [ColumnCssClass], N'' AS [HeaderGroupCSSClass], N'0' AS [HeaderGroupLastItem], N'04289A7A-5B9E-44E5-96FF-20A8EF75B08B' AS [MasterID]
        FROM [list].[ItemList] [l]
        WHERE [l].[MasterID] = '422130C6-7601-4288-9D87-96144387C6DA';
        DECLARE @ItemListFieldID_3735 nvarchar(32)= SCOPE_IDENTITY();

        INSERT INTO [list].[LT_ItemListField] ([LanguageID], [ItemListFieldID], [Name], [Description], [HeaderText], [HeaderGroupText], [HeaderTooltipText], [ItemTooltipText], [ItemText], [ExpandingItemText], [DefaultValues], [AvailableValues])
        SELECT l.[LanguageID], @ItemListFieldID_3735 AS [ItemListFieldID], N'KARAKTER MV2' AS [Name], N'KARAKTER MV2' AS [Description], N'KARAKTER MV2' AS [HeaderText], N'' AS [HeaderGroupText], N'' AS [HeaderTooltipText], N'' AS [ItemTooltipText], N'{Mv2}' AS [ItemText], N'' AS [ExpandingItemText], N'' AS [DefaultValues], N'' AS [AvailableValues]
        FROM [dbo].[Language] l
        WHERE l.[LanguageID] IN (1,7);

        INSERT INTO [list].[LT_ItemListField] ([LanguageID], [ItemListFieldID], [Name], [Description], [HeaderText], [HeaderGroupText], [HeaderTooltipText], [ItemTooltipText], [ItemText], [ExpandingItemText], [DefaultValues], [AvailableValues])
        SELECT N'2' AS [LanguageID], @ItemListFieldID_3735 AS [ItemListFieldID], N'MV2' AS [Name], N'MV2' AS [Description], N'MV2' AS [HeaderText], N'' AS [HeaderGroupText], N'' AS [HeaderTooltipText], N'' AS [ItemTooltipText], N'{Mv2}' AS [ItemText], N'' AS [ExpandingItemText], N'' AS [DefaultValues], N'' AS [AvailableValues];
    END;

    IF NOT EXISTS (SELECT 1 FROM [list].[ItemListField] [f] WHERE [f].[MasterID] = '80BBCFBE-780A-4B2C-B924-1CAE5BB66C72')
    BEGIN
        INSERT INTO [list].[ItemListField] ([ItemListID], [No], [TableTypeID], [FieldType], [ValueType], [FieldName], [PropID], [QuestionID], [OverrideLocked], [ShowInUserList], [ShowInTableView], [ShowInExport], [Mandatory], [Sortable], [SortNo], [SortDirection], [Width], [Align], [ItemPrefix], [ItemSuffix], [HeaderCssClass], [ItemCssClass], [Created], [ColumnCssClass], [HeaderGroupCSSClass], [HeaderGroupLastItem], [MasterID])
        SELECT [l].[ItemListID] AS [ItemListID], N'7' AS [No], NULL AS [TableTypeID], N'0' AS [FieldType], N'0' AS [ValueType], N'T2' AS [FieldName], N'0' AS [PropID], NULL AS [QuestionID], N'0' AS [OverrideLocked], N'0' AS [ShowInUserList], N'0' AS [ShowInTableView], N'0' AS [ShowInExport], N'0' AS [Mandatory], N'1' AS [Sortable], N'-1' AS [SortNo], N'1' AS [SortDirection], N'120' AS [Width], N'1' AS [Align], N'' AS [ItemPrefix], N'' AS [ItemSuffix], N'' AS [HeaderCssClass], N'' AS [ItemCssClass], GETDATE() AS [Created], N'' AS [ColumnCssClass], N'' AS [HeaderGroupCSSClass], N'0' AS [HeaderGroupLastItem], N'80BBCFBE-780A-4B2C-B924-1CAE5BB66C72' AS [MasterID]
        FROM [list].[ItemList] [l]
        WHERE [l].[MasterID] = '422130C6-7601-4288-9D87-96144387C6DA';
        DECLARE @ItemListFieldID_3736 nvarchar(32)= SCOPE_IDENTITY();

        INSERT INTO [list].[LT_ItemListField] ([LanguageID], [ItemListFieldID], [Name], [Description], [HeaderText], [HeaderGroupText], [HeaderTooltipText], [ItemTooltipText], [ItemText], [ExpandingItemText], [DefaultValues], [AvailableValues])
        SELECT l.[LanguageID], @ItemListFieldID_3736 AS [ItemListFieldID], N'KARAKTER T2' AS [Name], N'KARAKTER T2' AS [Description], N'KARAKTER T2' AS [HeaderText], N'' AS [HeaderGroupText], N'' AS [HeaderTooltipText], N'' AS [ItemTooltipText], N'{T2}' AS [ItemText], N'' AS [ExpandingItemText], N'' AS [DefaultValues], N'' AS [AvailableValues]
        FROM [dbo].[Language] l
        WHERE l.[LanguageID] IN (1,7);

        INSERT INTO [list].[LT_ItemListField] ([LanguageID], [ItemListFieldID], [Name], [Description], [HeaderText], [HeaderGroupText], [HeaderTooltipText], [ItemTooltipText], [ItemText], [ExpandingItemText], [DefaultValues], [AvailableValues])
        SELECT N'2' AS [LanguageID], @ItemListFieldID_3736 AS [ItemListFieldID], N'T2' AS [Name], N'T2' AS [Description], N'T2' AS [HeaderText], N'' AS [HeaderGroupText], N'' AS [HeaderTooltipText], N'' AS [ItemTooltipText], N'{T2}' AS [ItemText], N'' AS [ExpandingItemText], N'' AS [DefaultValues], N'' AS [AvailableValues];
    END;
END;
--#endregion ItemList 1407

--#region ItemList 1408
DECLARE @DataSourceID_2158_130229148332714081 nvarchar(32)= '';
SELECT @DataSourceID_2158_130229148332714081 = [ds].[ItemListDataSourceID]
FROM [list].[ItemListDataSource] [ds]
WHERE [ds].[MasterID] = '206C97A4-884A-452B-8112-99BD450F9176';
IF NOT EXISTS (SELECT 1 FROM [list].[ItemList] [l] WHERE [l].[MasterID] = '45891B66-0415-46FD-9ADF-DBB4221EFFD0')
BEGIN
    INSERT INTO [list].[ItemList] ([OwnerID], [ItemListDataSourceID], [ListTableCSSClass], [ListTableCSSCStyle], [ListHeaderRowCSSClass], [ListRowCSSClass], [ListAltRowCSSClass], [ExpandingItemListDatasourceID], [ExpandingListRowCSSClass], [ExpandingListAltRowCSSClass], [ExpandingExpression], [InsertMultiCheckColumn], [DisableCheckColumnExpression], [DisplayUpperCmdBarThreshold], [MaxRows], [ExtID], [Created], [MasterID])
    SELECT [o].[OwnerID] AS [OwnerID], @DataSourceID_2158_130229148332714081 AS [ItemListDataSourceID], N'table dotted-border vertical-middle tbl' AS [ListTableCSSClass], N'' AS [ListTableCSSCStyle], N'th' AS [ListHeaderRowCSSClass], N'no-padding' AS [ListRowCSSClass], N'trAlt' AS [ListAltRowCSSClass], NULL AS [ExpandingItemListDatasourceID], N'' AS [ExpandingListRowCSSClass], N'' AS [ExpandingListAltRowCSSClass], N'' AS [ExpandingExpression], N'0' AS [InsertMultiCheckColumn], N'' AS [DisableCheckColumnExpression], N'0' AS [DisplayUpperCmdBarThreshold], N'-1' AS [MaxRows], N'' AS [ExtID], GETDATE() AS [Created], N'45891B66-0415-46FD-9ADF-DBB4221EFFD0' AS [MasterID]
    FROM [org].[Owner] [o]
    WHERE [o].[MasterID] = '1C483E02-E808-42A7-8210-B11F4051D2E4';
    DECLARE @ItemListID_1408 nvarchar(32)= SCOPE_IDENTITY();

    INSERT INTO [list].[LT_ItemList] ([LanguageID], [ItemListID], [Name], [Description], [RowTooltipText], [EmptyListText])
    SELECT l.[LanguageID], @ItemListID_1408 AS [ItemListID], N'[Status] Expanding Object Follow Up - Status detail' AS [Name], N'' AS [Description], N'' AS [RowTooltipText], N'Ingen elementer i listen' AS [EmptyListText]
    FROM [dbo].[Language] l
    WHERE l.[LanguageID] IN (1,7);

    INSERT INTO [list].[LT_ItemList] ([LanguageID], [ItemListID], [Name], [Description], [RowTooltipText], [EmptyListText])
    SELECT N'2' AS [LanguageID], @ItemListID_1408 AS [ItemListID], N'[Status] Expanding SurveyList - status detail' AS [Name], N'' AS [Description], N'' AS [RowTooltipText], N'No elements in list' AS [EmptyListText];

    IF NOT EXISTS (SELECT 1 FROM [list].[ItemListRowAttribute] [att] WHERE [att].[MasterID] = '905A28CF-4BFD-4231-B3BC-C3BBFB0086AC')
    BEGIN
        INSERT INTO [list].[ItemListRowAttribute] ([ItemListID], [Key], [Value], [Created], [MasterID])
        SELECT [l].[ItemListID] AS [ItemListID], N'data-params' AS [Key], N''+'600-{Unknown}'+'' AS [Value], GETDATE() AS [Created], N'905A28CF-4BFD-4231-B3BC-C3BBFB0086AC' AS [MasterID]
        FROM [list].[ItemList] [l]
        WHERE [l].[MasterID] = '45891B66-0415-46FD-9ADF-DBB4221EFFD0';
    END;

    IF NOT EXISTS (SELECT 1 FROM [list].[ItemListCommand] [c] WHERE [c].[MasterID] = '1C1DE021-695A-49E9-A4B6-4BDD1D1DCDB0')
    BEGIN
        INSERT INTO [list].[ItemListCommand] ([ItemListID], [PlaceAbove], [PlaceBelow], [MultiCheckCommand], [ClientFunction], [DisabledInEmptyList], [CssClass], [Created], [No], [MasterID])
        SELECT [l].[ItemListID] AS [ItemListID], N'1' AS [PlaceAbove], N'0' AS [PlaceBelow], N'0' AS [MultiCheckCommand], N'exportGenericListStatus('+'1408'+','+'$(this)'+');' AS [ClientFunction], N'1' AS [DisabledInEmptyList], N'icon-vokal-excel green|btn btn-secondary btn-lg link' AS [CssClass], GETDATE() AS [Created], N'0' AS [No], N'1C1DE021-695A-49E9-A4B6-4BDD1D1DCDB0' AS [MasterID]
        FROM [list].[ItemList] [l]
        WHERE [l].[MasterID] = '45891B66-0415-46FD-9ADF-DBB4221EFFD0';
        DECLARE @ListCommandID_191 nvarchar(32)= SCOPE_IDENTITY();

        INSERT INTO [list].[LT_ItemListCommand] ([LanguageID], [ItemListCommandID], [Name], [Description], [Text], [TooltipText], [HotKey])
        SELECT l.[LanguageID], @ListCommandID_191 AS [ItemListCommandID], N'Eksporter til regneark' AS [Name], N'' AS [Description], N'Eksport' AS [Text], N'' AS [TooltipText], N'' AS [HotKey]
        FROM [dbo].[Language] l
        WHERE l.[LanguageID] IN (1,7);

        INSERT INTO [list].[LT_ItemListCommand] ([LanguageID], [ItemListCommandID], [Name], [Description], [Text], [TooltipText], [HotKey])
        SELECT N'2' AS [LanguageID], @ListCommandID_191 AS [ItemListCommandID], N'Export to spreadsheet' AS [Name], N'' AS [Description], N'Export to spreadsheet' AS [Text], N'' AS [TooltipText], N'' AS [HotKey];
    END;

    IF NOT EXISTS (SELECT 1 FROM [list].[ItemListField] [f] WHERE [f].[MasterID] = '06A9637F-7CBF-43B4-A247-D60F0897638F')
    BEGIN
        INSERT INTO [list].[ItemListField] ([ItemListID], [No], [TableTypeID], [FieldType], [ValueType], [FieldName], [PropID], [QuestionID], [OverrideLocked], [ShowInUserList], [ShowInTableView], [ShowInExport], [Mandatory], [Sortable], [SortNo], [SortDirection], [Width], [Align], [ItemPrefix], [ItemSuffix], [HeaderCssClass], [ItemCssClass], [Created], [ColumnCssClass], [HeaderGroupCSSClass], [HeaderGroupLastItem], [MasterID])
        SELECT [l].[ItemListID] AS [ItemListID], N'1' AS [No], NULL AS [TableTypeID], N'0' AS [FieldType], N'0' AS [ValueType], N'Level1Name' AS [FieldName], N'0' AS [PropID], NULL AS [QuestionID], N'0' AS [OverrideLocked], N'0' AS [ShowInUserList], N'0' AS [ShowInTableView], N'1' AS [ShowInExport], N'0' AS [Mandatory], N'1' AS [Sortable], N'1' AS [SortNo], N'1' AS [SortDirection], N'0' AS [Width], N'0' AS [Align], N'' AS [ItemPrefix], N'' AS [ItemSuffix], N'tdSort parser-text sorter-text' AS [HeaderCssClass], N'bold td' AS [ItemCssClass], GETDATE() AS [Created], N'' AS [ColumnCssClass], N'tblListGroupTd' AS [HeaderGroupCSSClass], N'0' AS [HeaderGroupLastItem], N'06A9637F-7CBF-43B4-A247-D60F0897638F' AS [MasterID]
        FROM [list].[ItemList] [l]
        WHERE [l].[MasterID] = '45891B66-0415-46FD-9ADF-DBB4221EFFD0';
        DECLARE @ItemListFieldID_3739 nvarchar(32)= SCOPE_IDENTITY();

        INSERT INTO [list].[LT_ItemListField] ([LanguageID], [ItemListFieldID], [Name], [Description], [HeaderText], [HeaderGroupText], [HeaderTooltipText], [ItemTooltipText], [ItemText], [ExpandingItemText], [DefaultValues], [AvailableValues])
        SELECT l.[LanguageID], @ItemListFieldID_3739 AS [ItemListFieldID], N'Enhet' AS [Name], N'Enhet' AS [Description], N'Enhet' AS [HeaderText], N'' AS [HeaderGroupText], N'' AS [HeaderTooltipText], N'' AS [ItemTooltipText], N'{if({"{GetSubDepartment}"="0"},"{Level1Name}","{SubjectName}")}' AS [ItemText], N'' AS [ExpandingItemText], N'' AS [DefaultValues], N'' AS [AvailableValues]
        FROM [dbo].[Language] l
        WHERE l.[LanguageID] IN (1,2,7);
    END;

    IF NOT EXISTS (SELECT 1 FROM [list].[ItemListField] [f] WHERE [f].[MasterID] = 'B9D63925-C77D-461B-94AF-474318C93E2E')
    BEGIN
        INSERT INTO [list].[ItemListField] ([ItemListID], [No], [TableTypeID], [FieldType], [ValueType], [FieldName], [PropID], [QuestionID], [OverrideLocked], [ShowInUserList], [ShowInTableView], [ShowInExport], [Mandatory], [Sortable], [SortNo], [SortDirection], [Width], [Align], [ItemPrefix], [ItemSuffix], [HeaderCssClass], [ItemCssClass], [Created], [ColumnCssClass], [HeaderGroupCSSClass], [HeaderGroupLastItem], [MasterID])
        SELECT [l].[ItemListID] AS [ItemListID], N'2' AS [No], NULL AS [TableTypeID], N'0' AS [FieldType], N'0' AS [ValueType], N'UserCount' AS [FieldName], N'0' AS [PropID], NULL AS [QuestionID], N'0' AS [OverrideLocked], N'0' AS [ShowInUserList], N'0' AS [ShowInTableView], N'1' AS [ShowInExport], N'0' AS [Mandatory], N'1' AS [Sortable], N'-1' AS [SortNo], N'1' AS [SortDirection], N'150' AS [Width], N'1' AS [Align], N'' AS [ItemPrefix], N'' AS [ItemSuffix], N'tdSort' AS [HeaderCssClass], N'td' AS [ItemCssClass], GETDATE() AS [Created], N'' AS [ColumnCssClass], N'tblListGroupTd' AS [HeaderGroupCSSClass], N'0' AS [HeaderGroupLastItem], N'B9D63925-C77D-461B-94AF-474318C93E2E' AS [MasterID]
        FROM [list].[ItemList] [l]
        WHERE [l].[MasterID] = '45891B66-0415-46FD-9ADF-DBB4221EFFD0';
        DECLARE @ItemListFieldID_3740 nvarchar(32)= SCOPE_IDENTITY();

        INSERT INTO [list].[LT_ItemListField] ([LanguageID], [ItemListFieldID], [Name], [Description], [HeaderText], [HeaderGroupText], [HeaderTooltipText], [ItemTooltipText], [ItemText], [ExpandingItemText], [DefaultValues], [AvailableValues])
        SELECT l.[LanguageID], @ItemListFieldID_3740 AS [ItemListFieldID], N'Antall elever' AS [Name], N'Antall elever' AS [Description], N'Antall elever' AS [HeaderText], N'' AS [HeaderGroupText], N'' AS [HeaderTooltipText], N'' AS [ItemTooltipText], N'{UserCount}' AS [ItemText], N'' AS [ExpandingItemText], N'' AS [DefaultValues], N'' AS [AvailableValues]
        FROM [dbo].[Language] l
        WHERE l.[LanguageID] IN (1,7);

        INSERT INTO [list].[LT_ItemListField] ([LanguageID], [ItemListFieldID], [Name], [Description], [HeaderText], [HeaderGroupText], [HeaderTooltipText], [ItemTooltipText], [ItemText], [ExpandingItemText], [DefaultValues], [AvailableValues])
        SELECT N'2' AS [LanguageID], @ItemListFieldID_3740 AS [ItemListFieldID], N'Number of students' AS [Name], N'Number of students' AS [Description], N'Number of students' AS [HeaderText], N'' AS [HeaderGroupText], N'' AS [HeaderTooltipText], N'' AS [ItemTooltipText], N'{UserCount}' AS [ItemText], N'' AS [ExpandingItemText], N'' AS [DefaultValues], N'' AS [AvailableValues];
    END;

    IF NOT EXISTS (SELECT 1 FROM [list].[ItemListField] [f] WHERE [f].[MasterID] = '2D1AA225-A894-4230-BE3C-A62B628C2283')
    BEGIN
        INSERT INTO [list].[ItemListField] ([ItemListID], [No], [TableTypeID], [FieldType], [ValueType], [FieldName], [PropID], [QuestionID], [OverrideLocked], [ShowInUserList], [ShowInTableView], [ShowInExport], [Mandatory], [Sortable], [SortNo], [SortDirection], [Width], [Align], [ItemPrefix], [ItemSuffix], [HeaderCssClass], [ItemCssClass], [Created], [ColumnCssClass], [HeaderGroupCSSClass], [HeaderGroupLastItem], [MasterID])
        SELECT [l].[ItemListID] AS [ItemListID], N'3' AS [No], NULL AS [TableTypeID], N'0' AS [FieldType], N'0' AS [ValueType], N'Mv1' AS [FieldName], N'0' AS [PropID], NULL AS [QuestionID], N'0' AS [OverrideLocked], N'0' AS [ShowInUserList], N'0' AS [ShowInTableView], N'1' AS [ShowInExport], N'0' AS [Mandatory], N'1' AS [Sortable], N'-1' AS [SortNo], N'1' AS [SortDirection], N'150' AS [Width], N'1' AS [Align], N'' AS [ItemPrefix], N'' AS [ItemSuffix], N'tdSort' AS [HeaderCssClass], N'td' AS [ItemCssClass], GETDATE() AS [Created], N'' AS [ColumnCssClass], N'tblListGroupTd' AS [HeaderGroupCSSClass], N'0' AS [HeaderGroupLastItem], N'2D1AA225-A894-4230-BE3C-A62B628C2283' AS [MasterID]
        FROM [list].[ItemList] [l]
        WHERE [l].[MasterID] = '45891B66-0415-46FD-9ADF-DBB4221EFFD0';
        DECLARE @ItemListFieldID_3741 nvarchar(32)= SCOPE_IDENTITY();

        INSERT INTO [list].[LT_ItemListField] ([LanguageID], [ItemListFieldID], [Name], [Description], [HeaderText], [HeaderGroupText], [HeaderTooltipText], [ItemTooltipText], [ItemText], [ExpandingItemText], [DefaultValues], [AvailableValues])
        SELECT l.[LanguageID], @ItemListFieldID_3741 AS [ItemListFieldID], N'MV1' AS [Name], N'MV1' AS [Description], N'MV1' AS [HeaderText], N'' AS [HeaderGroupText], N'' AS [HeaderTooltipText], N'' AS [ItemTooltipText], N'{Mv1}' AS [ItemText], N'' AS [ExpandingItemText], N'' AS [DefaultValues], N'' AS [AvailableValues]
        FROM [dbo].[Language] l
        WHERE l.[LanguageID] IN (1,2,7);
    END;

    IF NOT EXISTS (SELECT 1 FROM [list].[ItemListField] [f] WHERE [f].[MasterID] = 'E388F55B-D30E-41B6-B99D-DAA2804C8B23')
    BEGIN
        INSERT INTO [list].[ItemListField] ([ItemListID], [No], [TableTypeID], [FieldType], [ValueType], [FieldName], [PropID], [QuestionID], [OverrideLocked], [ShowInUserList], [ShowInTableView], [ShowInExport], [Mandatory], [Sortable], [SortNo], [SortDirection], [Width], [Align], [ItemPrefix], [ItemSuffix], [HeaderCssClass], [ItemCssClass], [Created], [ColumnCssClass], [HeaderGroupCSSClass], [HeaderGroupLastItem], [MasterID])
        SELECT [l].[ItemListID] AS [ItemListID], N'4' AS [No], NULL AS [TableTypeID], N'0' AS [FieldType], N'0' AS [ValueType], N'T1' AS [FieldName], N'0' AS [PropID], NULL AS [QuestionID], N'0' AS [OverrideLocked], N'0' AS [ShowInUserList], N'0' AS [ShowInTableView], N'1' AS [ShowInExport], N'0' AS [Mandatory], N'1' AS [Sortable], N'-1' AS [SortNo], N'1' AS [SortDirection], N'150' AS [Width], N'1' AS [Align], N'' AS [ItemPrefix], N'' AS [ItemSuffix], N'tdSort' AS [HeaderCssClass], N'td' AS [ItemCssClass], GETDATE() AS [Created], N'' AS [ColumnCssClass], N'tblListGroupTd' AS [HeaderGroupCSSClass], N'0' AS [HeaderGroupLastItem], N'E388F55B-D30E-41B6-B99D-DAA2804C8B23' AS [MasterID]
        FROM [list].[ItemList] [l]
        WHERE [l].[MasterID] = '45891B66-0415-46FD-9ADF-DBB4221EFFD0';
        DECLARE @ItemListFieldID_3742 nvarchar(32)= SCOPE_IDENTITY();

        INSERT INTO [list].[LT_ItemListField] ([LanguageID], [ItemListFieldID], [Name], [Description], [HeaderText], [HeaderGroupText], [HeaderTooltipText], [ItemTooltipText], [ItemText], [ExpandingItemText], [DefaultValues], [AvailableValues])
        SELECT l.[LanguageID], @ItemListFieldID_3742 AS [ItemListFieldID], N'T1' AS [Name], N'T1' AS [Description], N'T1' AS [HeaderText], N'' AS [HeaderGroupText], N'' AS [HeaderTooltipText], N'' AS [ItemTooltipText], N'{T1}' AS [ItemText], N'' AS [ExpandingItemText], N'' AS [DefaultValues], N'' AS [AvailableValues]
        FROM [dbo].[Language] l
        WHERE l.[LanguageID] IN (1,2,7);
    END;

    IF NOT EXISTS (SELECT 1 FROM [list].[ItemListField] [f] WHERE [f].[MasterID] = '887EA147-3BE5-4189-8EB8-E418B32DDB50')
    BEGIN
        INSERT INTO [list].[ItemListField] ([ItemListID], [No], [TableTypeID], [FieldType], [ValueType], [FieldName], [PropID], [QuestionID], [OverrideLocked], [ShowInUserList], [ShowInTableView], [ShowInExport], [Mandatory], [Sortable], [SortNo], [SortDirection], [Width], [Align], [ItemPrefix], [ItemSuffix], [HeaderCssClass], [ItemCssClass], [Created], [ColumnCssClass], [HeaderGroupCSSClass], [HeaderGroupLastItem], [MasterID])
        SELECT [l].[ItemListID] AS [ItemListID], N'5' AS [No], NULL AS [TableTypeID], N'0' AS [FieldType], N'0' AS [ValueType], N'Mv2' AS [FieldName], N'0' AS [PropID], NULL AS [QuestionID], N'0' AS [OverrideLocked], N'0' AS [ShowInUserList], N'0' AS [ShowInTableView], N'1' AS [ShowInExport], N'0' AS [Mandatory], N'1' AS [Sortable], N'-1' AS [SortNo], N'1' AS [SortDirection], N'120' AS [Width], N'1' AS [Align], N'' AS [ItemPrefix], N'' AS [ItemSuffix], N'tdSort' AS [HeaderCssClass], N'td' AS [ItemCssClass], GETDATE() AS [Created], N'' AS [ColumnCssClass], N'tblListGroupTd' AS [HeaderGroupCSSClass], N'0' AS [HeaderGroupLastItem], N'887EA147-3BE5-4189-8EB8-E418B32DDB50' AS [MasterID]
        FROM [list].[ItemList] [l]
        WHERE [l].[MasterID] = '45891B66-0415-46FD-9ADF-DBB4221EFFD0';
        DECLARE @ItemListFieldID_3743 nvarchar(32)= SCOPE_IDENTITY();

        INSERT INTO [list].[LT_ItemListField] ([LanguageID], [ItemListFieldID], [Name], [Description], [HeaderText], [HeaderGroupText], [HeaderTooltipText], [ItemTooltipText], [ItemText], [ExpandingItemText], [DefaultValues], [AvailableValues])
        SELECT l.[LanguageID], @ItemListFieldID_3743 AS [ItemListFieldID], N'MV2' AS [Name], N'MV2' AS [Description], N'MV2' AS [HeaderText], N'' AS [HeaderGroupText], N'' AS [HeaderTooltipText], N'' AS [ItemTooltipText], N'{Mv2}' AS [ItemText], N'' AS [ExpandingItemText], N'' AS [DefaultValues], N'' AS [AvailableValues]
        FROM [dbo].[Language] l
        WHERE l.[LanguageID] IN (1,2,7);
    END;

    IF NOT EXISTS (SELECT 1 FROM [list].[ItemListField] [f] WHERE [f].[MasterID] = '587533DF-A1A9-44A1-B2B4-DFA5C07EA422')
    BEGIN
        INSERT INTO [list].[ItemListField] ([ItemListID], [No], [TableTypeID], [FieldType], [ValueType], [FieldName], [PropID], [QuestionID], [OverrideLocked], [ShowInUserList], [ShowInTableView], [ShowInExport], [Mandatory], [Sortable], [SortNo], [SortDirection], [Width], [Align], [ItemPrefix], [ItemSuffix], [HeaderCssClass], [ItemCssClass], [Created], [ColumnCssClass], [HeaderGroupCSSClass], [HeaderGroupLastItem], [MasterID])
        SELECT [l].[ItemListID] AS [ItemListID], N'6' AS [No], NULL AS [TableTypeID], N'0' AS [FieldType], N'0' AS [ValueType], N'T2' AS [FieldName], N'0' AS [PropID], NULL AS [QuestionID], N'0' AS [OverrideLocked], N'0' AS [ShowInUserList], N'0' AS [ShowInTableView], N'1' AS [ShowInExport], N'0' AS [Mandatory], N'1' AS [Sortable], N'-1' AS [SortNo], N'1' AS [SortDirection], N'100' AS [Width], N'1' AS [Align], N'' AS [ItemPrefix], N'' AS [ItemSuffix], N'tdSort' AS [HeaderCssClass], N'td' AS [ItemCssClass], GETDATE() AS [Created], N'' AS [ColumnCssClass], N'tblListGroupTd' AS [HeaderGroupCSSClass], N'0' AS [HeaderGroupLastItem], N'587533DF-A1A9-44A1-B2B4-DFA5C07EA422' AS [MasterID]
        FROM [list].[ItemList] [l]
        WHERE [l].[MasterID] = '45891B66-0415-46FD-9ADF-DBB4221EFFD0';
        DECLARE @ItemListFieldID_3744 nvarchar(32)= SCOPE_IDENTITY();

        INSERT INTO [list].[LT_ItemListField] ([LanguageID], [ItemListFieldID], [Name], [Description], [HeaderText], [HeaderGroupText], [HeaderTooltipText], [ItemTooltipText], [ItemText], [ExpandingItemText], [DefaultValues], [AvailableValues])
        SELECT l.[LanguageID], @ItemListFieldID_3744 AS [ItemListFieldID], N'T2' AS [Name], N'T2' AS [Description], N'T2' AS [HeaderText], N'' AS [HeaderGroupText], N'' AS [HeaderTooltipText], N'' AS [ItemTooltipText], N'{T2}' AS [ItemText], N'' AS [ExpandingItemText], N'' AS [DefaultValues], N'' AS [AvailableValues]
        FROM [dbo].[Language] l
        WHERE l.[LanguageID] IN (1,2,7);
    END;
END;
--#endregion ItemList 1408

--#region ItemList 1409
DECLARE @DataSourceID_44_131834101530614091 nvarchar(32)= '';
SELECT @DataSourceID_44_131834101530614091 = [ds].[ItemListDataSourceID]
FROM [list].[ItemListDataSource] [ds]
WHERE [ds].[MasterID] = 'F70DDA3E-C348-4D8F-950A-EADDA2D7317C';

DECLARE @DataSourceID_2159_131834101530614092 nvarchar(32)= '';
SELECT @DataSourceID_2159_131834101530614092 = [ds].[ItemListDataSourceID]
FROM [list].[ItemListDataSource] [ds]
WHERE [ds].[MasterID] = 'D6B1D0A0-0278-4B2D-8E88-1D77458A6EA2';
IF NOT EXISTS (SELECT 1 FROM [list].[ItemList] [l] WHERE [l].[MasterID] = '4A424300-A5C4-4511-A8EB-B837A5358B11')
BEGIN
    INSERT INTO [list].[ItemList] ([OwnerID], [ItemListDataSourceID], [ListTableCSSClass], [ListTableCSSCStyle], [ListHeaderRowCSSClass], [ListRowCSSClass], [ListAltRowCSSClass], [ExpandingItemListDatasourceID], [ExpandingListRowCSSClass], [ExpandingListAltRowCSSClass], [ExpandingExpression], [InsertMultiCheckColumn], [DisableCheckColumnExpression], [DisplayUpperCmdBarThreshold], [MaxRows], [ExtID], [Created], [MasterID])
    SELECT [o].[OwnerID] AS [OwnerID], @DataSourceID_44_131834101530614091 AS [ItemListDataSourceID], N'table-user stripe line-bottom' AS [ListTableCSSClass], N'' AS [ListTableCSSCStyle], N'th' AS [ListHeaderRowCSSClass], N'' AS [ListRowCSSClass], N'trAlt' AS [ListAltRowCSSClass], @DataSourceID_2159_131834101530614092 AS [ExpandingItemListDatasourceID], N'trExp' AS [ExpandingListRowCSSClass], N'trExpAlt' AS [ExpandingListAltRowCSSClass], N'True' AS [ExpandingExpression], N'0' AS [InsertMultiCheckColumn], N'' AS [DisableCheckColumnExpression], N'0' AS [DisplayUpperCmdBarThreshold], N'-1' AS [MaxRows], N'' AS [ExtID], GETDATE() AS [Created], N'4A424300-A5C4-4511-A8EB-B837A5358B11' AS [MasterID]
    FROM [org].[Owner] [o]
    WHERE [o].[MasterID] = '1C483E02-E808-42A7-8210-B11F4051D2E4';
    DECLARE @ItemListID_1409 nvarchar(32)= SCOPE_IDENTITY();

    INSERT INTO [list].[LT_ItemList] ([LanguageID], [ItemListID], [Name], [Description], [RowTooltipText], [EmptyListText])
    SELECT l.[LanguageID], @ItemListID_1409 AS [ItemListID], N'[Status expanding] List of all status for object follow up' AS [Name], N'' AS [Description], N'' AS [RowTooltipText], N'Ingen elementer i listen' AS [EmptyListText]
    FROM [dbo].[Language] l
    WHERE l.[LanguageID] IN (1,7);

    INSERT INTO [list].[LT_ItemList] ([LanguageID], [ItemListID], [Name], [Description], [RowTooltipText], [EmptyListText])
    SELECT N'2' AS [LanguageID], @ItemListID_1409 AS [ItemListID], N'[Status expanding] List of all status for object follow up' AS [Name], N'' AS [Description], N'' AS [RowTooltipText], N'No elements in list' AS [EmptyListText];

    IF NOT EXISTS (SELECT 1 FROM [list].[ItemListRowAttribute] [att] WHERE [att].[MasterID] = '66821347-F3A0-4CC5-B3FD-BC1A70510542')
    BEGIN
        INSERT INTO [list].[ItemListRowAttribute] ([ItemListID], [Key], [Value], [Created], [MasterID])
        SELECT [l].[ItemListID] AS [ItemListID], N'data-params' AS [Key], N''+'600-{SurveyID}'+'' AS [Value], GETDATE() AS [Created], N'66821347-F3A0-4CC5-B3FD-BC1A70510542' AS [MasterID]
        FROM [list].[ItemList] [l]
        WHERE [l].[MasterID] = '4A424300-A5C4-4511-A8EB-B837A5358B11';
    END;

    IF NOT EXISTS (SELECT 1 FROM [list].[ItemListRowAttribute] [att] WHERE [att].[MasterID] = '77047262-CD18-4D0E-912B-21AC629B4709')
    BEGIN
        INSERT INTO [list].[ItemListRowAttribute] ([ItemListID], [Key], [Value], [Created], [MasterID])
        SELECT [l].[ItemListID] AS [ItemListID], N'data-id' AS [Key], N''+'{SurveyID}'+'' AS [Value], GETDATE() AS [Created], N'77047262-CD18-4D0E-912B-21AC629B4709' AS [MasterID]
        FROM [list].[ItemList] [l]
        WHERE [l].[MasterID] = '4A424300-A5C4-4511-A8EB-B837A5358B11';
    END;

    IF NOT EXISTS (SELECT 1 FROM [list].[ItemListField] [f] WHERE [f].[MasterID] = '24ED1A5F-941A-4AF1-9E43-B6159D481D06')
    BEGIN
        INSERT INTO [list].[ItemListField] ([ItemListID], [No], [TableTypeID], [FieldType], [ValueType], [FieldName], [PropID], [QuestionID], [OverrideLocked], [ShowInUserList], [ShowInTableView], [ShowInExport], [Mandatory], [Sortable], [SortNo], [SortDirection], [Width], [Align], [ItemPrefix], [ItemSuffix], [HeaderCssClass], [ItemCssClass], [Created], [ColumnCssClass], [HeaderGroupCSSClass], [HeaderGroupLastItem], [MasterID])
        SELECT [l].[ItemListID] AS [ItemListID], N'1' AS [No], NULL AS [TableTypeID], N'0' AS [FieldType], N'0' AS [ValueType], N'' AS [FieldName], N'0' AS [PropID], NULL AS [QuestionID], N'0' AS [OverrideLocked], N'0' AS [ShowInUserList], N'0' AS [ShowInTableView], N'1' AS [ShowInExport], N'0' AS [Mandatory], N'0' AS [Sortable], N'-1' AS [SortNo], N'1' AS [SortDirection], N'0' AS [Width], N'0' AS [Align], N'' AS [ItemPrefix], N'' AS [ItemSuffix], N'tdNosort' AS [HeaderCssClass], N'td' AS [ItemCssClass], GETDATE() AS [Created], N'' AS [ColumnCssClass], N'tblListGroupTd' AS [HeaderGroupCSSClass], N'0' AS [HeaderGroupLastItem], N'24ED1A5F-941A-4AF1-9E43-B6159D481D06' AS [MasterID]
        FROM [list].[ItemList] [l]
        WHERE [l].[MasterID] = '4A424300-A5C4-4511-A8EB-B837A5358B11';
        DECLARE @ItemListFieldID_3747 nvarchar(32)= SCOPE_IDENTITY();

        INSERT INTO [list].[LT_ItemListField] ([LanguageID], [ItemListFieldID], [Name], [Description], [HeaderText], [HeaderGroupText], [HeaderTooltipText], [ItemTooltipText], [ItemText], [ExpandingItemText], [DefaultValues], [AvailableValues])
        SELECT l.[LanguageID], @ItemListFieldID_3747 AS [ItemListFieldID], N'Navn' AS [Name], N'' AS [Description], N'Navn' AS [HeaderText], N'' AS [HeaderGroupText], N'' AS [HeaderTooltipText], N'' AS [ItemTooltipText], N'{SubXCategoryName} {if({"{NumberOfSurveys()}"=""},"","({NumberOfSurveys()})")}' AS [ItemText], N'<span class="icon-vokal-right-line-arrow btn-toggle"></span><a href="#">{SubXCategoryName}</a>' AS [ExpandingItemText], N'' AS [DefaultValues], N'' AS [AvailableValues]
        FROM [dbo].[Language] l
        WHERE l.[LanguageID] IN (1,7);

        INSERT INTO [list].[LT_ItemListField] ([LanguageID], [ItemListFieldID], [Name], [Description], [HeaderText], [HeaderGroupText], [HeaderTooltipText], [ItemTooltipText], [ItemText], [ExpandingItemText], [DefaultValues], [AvailableValues])
        SELECT N'2' AS [LanguageID], @ItemListFieldID_3747 AS [ItemListFieldID], N'Name' AS [Name], N'' AS [Description], N'Name' AS [HeaderText], N'' AS [HeaderGroupText], N'' AS [HeaderTooltipText], N'' AS [ItemTooltipText], N'{SubXCategoryName} {if({"{NumberOfSurveys()}"=""},"","({NumberOfSurveys()})")}' AS [ItemText], N'<span class="icon-vokal-right-line-arrow btn-toggle"></span><a href="#">{SubXCategoryName}</a>' AS [ExpandingItemText], N'' AS [DefaultValues], N'' AS [AvailableValues];
    END;

    IF NOT EXISTS (SELECT 1 FROM [list].[ItemListField] [f] WHERE [f].[MasterID] = '2749ECD6-2C5C-4655-9420-A1098EE8576B')
    BEGIN
        INSERT INTO [list].[ItemListField] ([ItemListID], [No], [TableTypeID], [FieldType], [ValueType], [FieldName], [PropID], [QuestionID], [OverrideLocked], [ShowInUserList], [ShowInTableView], [ShowInExport], [Mandatory], [Sortable], [SortNo], [SortDirection], [Width], [Align], [ItemPrefix], [ItemSuffix], [HeaderCssClass], [ItemCssClass], [Created], [ColumnCssClass], [HeaderGroupCSSClass], [HeaderGroupLastItem], [MasterID])
        SELECT [l].[ItemListID] AS [ItemListID], N'2' AS [No], NULL AS [TableTypeID], N'0' AS [FieldType], N'0' AS [ValueType], N'' AS [FieldName], N'0' AS [PropID], NULL AS [QuestionID], N'0' AS [OverrideLocked], N'0' AS [ShowInUserList], N'0' AS [ShowInTableView], N'1' AS [ShowInExport], N'0' AS [Mandatory], N'0' AS [Sortable], N'-1' AS [SortNo], N'1' AS [SortDirection], N'120' AS [Width], N'0' AS [Align], N'' AS [ItemPrefix], N'' AS [ItemSuffix], N'tdNosort' AS [HeaderCssClass], N'td' AS [ItemCssClass], GETDATE() AS [Created], N'' AS [ColumnCssClass], N'tblListGroupTd' AS [HeaderGroupCSSClass], N'0' AS [HeaderGroupLastItem], N'2749ECD6-2C5C-4655-9420-A1098EE8576B' AS [MasterID]
        FROM [list].[ItemList] [l]
        WHERE [l].[MasterID] = '4A424300-A5C4-4511-A8EB-B837A5358B11';
        DECLARE @ItemListFieldID_3748 nvarchar(32)= SCOPE_IDENTITY();

        INSERT INTO [list].[LT_ItemListField] ([LanguageID], [ItemListFieldID], [Name], [Description], [HeaderText], [HeaderGroupText], [HeaderTooltipText], [ItemTooltipText], [ItemText], [ExpandingItemText], [DefaultValues], [AvailableValues])
        SELECT l.[LanguageID], @ItemListFieldID_3748 AS [ItemListFieldID], N'Periode' AS [Name], N'' AS [Description], N'Periode' AS [HeaderText], N'' AS [HeaderGroupText], N'' AS [HeaderTooltipText], N'' AS [ItemTooltipText], N'' AS [ItemText], N'{SurveyName}' AS [ExpandingItemText], N'' AS [DefaultValues], N'' AS [AvailableValues]
        FROM [dbo].[Language] l
        WHERE l.[LanguageID] IN (1,7);

        INSERT INTO [list].[LT_ItemListField] ([LanguageID], [ItemListFieldID], [Name], [Description], [HeaderText], [HeaderGroupText], [HeaderTooltipText], [ItemTooltipText], [ItemText], [ExpandingItemText], [DefaultValues], [AvailableValues])
        SELECT N'2' AS [LanguageID], @ItemListFieldID_3748 AS [ItemListFieldID], N'Period' AS [Name], N'' AS [Description], N'Period' AS [HeaderText], N'' AS [HeaderGroupText], N'' AS [HeaderTooltipText], N'' AS [ItemTooltipText], N'' AS [ItemText], N'{SurveyName}' AS [ExpandingItemText], N'' AS [DefaultValues], N'' AS [AvailableValues];
    END;

    IF NOT EXISTS (SELECT 1 FROM [list].[ItemListField] [f] WHERE [f].[MasterID] = 'B8B988E0-64B0-44E3-9426-F7E1DD42048E')
    BEGIN
        INSERT INTO [list].[ItemListField] ([ItemListID], [No], [TableTypeID], [FieldType], [ValueType], [FieldName], [PropID], [QuestionID], [OverrideLocked], [ShowInUserList], [ShowInTableView], [ShowInExport], [Mandatory], [Sortable], [SortNo], [SortDirection], [Width], [Align], [ItemPrefix], [ItemSuffix], [HeaderCssClass], [ItemCssClass], [Created], [ColumnCssClass], [HeaderGroupCSSClass], [HeaderGroupLastItem], [MasterID])
        SELECT [l].[ItemListID] AS [ItemListID], N'3' AS [No], NULL AS [TableTypeID], N'0' AS [FieldType], N'0' AS [ValueType], N'' AS [FieldName], N'0' AS [PropID], NULL AS [QuestionID], N'0' AS [OverrideLocked], N'0' AS [ShowInUserList], N'0' AS [ShowInTableView], N'1' AS [ShowInExport], N'0' AS [Mandatory], N'0' AS [Sortable], N'-1' AS [SortNo], N'1' AS [SortDirection], N'120' AS [Width], N'1' AS [Align], N'' AS [ItemPrefix], N'' AS [ItemSuffix], N'tdNosort' AS [HeaderCssClass], N'td' AS [ItemCssClass], GETDATE() AS [Created], N'' AS [ColumnCssClass], N'tblListGroupTd' AS [HeaderGroupCSSClass], N'0' AS [HeaderGroupLastItem], N'B8B988E0-64B0-44E3-9426-F7E1DD42048E' AS [MasterID]
        FROM [list].[ItemList] [l]
        WHERE [l].[MasterID] = '4A424300-A5C4-4511-A8EB-B837A5358B11';
        DECLARE @ItemListFieldID_3749 nvarchar(32)= SCOPE_IDENTITY();

        INSERT INTO [list].[LT_ItemListField] ([LanguageID], [ItemListFieldID], [Name], [Description], [HeaderText], [HeaderGroupText], [HeaderTooltipText], [ItemTooltipText], [ItemText], [ExpandingItemText], [DefaultValues], [AvailableValues])
        SELECT l.[LanguageID], @ItemListFieldID_3749 AS [ItemListFieldID], N'Antall elever' AS [Name], N'' AS [Description], N'Antall elever' AS [HeaderText], N'' AS [HeaderGroupText], N'' AS [HeaderTooltipText], N'' AS [ItemTooltipText], N'' AS [ItemText], N'{UserCount}' AS [ExpandingItemText], N'' AS [DefaultValues], N'' AS [AvailableValues]
        FROM [dbo].[Language] l
        WHERE l.[LanguageID] IN (1,7);

        INSERT INTO [list].[LT_ItemListField] ([LanguageID], [ItemListFieldID], [Name], [Description], [HeaderText], [HeaderGroupText], [HeaderTooltipText], [ItemTooltipText], [ItemText], [ExpandingItemText], [DefaultValues], [AvailableValues])
        SELECT N'2' AS [LanguageID], @ItemListFieldID_3749 AS [ItemListFieldID], N'Number of students' AS [Name], N'' AS [Description], N'Number of students' AS [HeaderText], N'' AS [HeaderGroupText], N'' AS [HeaderTooltipText], N'' AS [ItemTooltipText], N'' AS [ItemText], N'{UserCount}' AS [ExpandingItemText], N'' AS [DefaultValues], N'' AS [AvailableValues];
    END;

    IF NOT EXISTS (SELECT 1 FROM [list].[ItemListField] [f] WHERE [f].[MasterID] = '02F273CF-68C5-4622-800E-59DB65EE474C')
    BEGIN
        INSERT INTO [list].[ItemListField] ([ItemListID], [No], [TableTypeID], [FieldType], [ValueType], [FieldName], [PropID], [QuestionID], [OverrideLocked], [ShowInUserList], [ShowInTableView], [ShowInExport], [Mandatory], [Sortable], [SortNo], [SortDirection], [Width], [Align], [ItemPrefix], [ItemSuffix], [HeaderCssClass], [ItemCssClass], [Created], [ColumnCssClass], [HeaderGroupCSSClass], [HeaderGroupLastItem], [MasterID])
        SELECT [l].[ItemListID] AS [ItemListID], N'4' AS [No], NULL AS [TableTypeID], N'0' AS [FieldType], N'0' AS [ValueType], N'' AS [FieldName], N'0' AS [PropID], NULL AS [QuestionID], N'0' AS [OverrideLocked], N'0' AS [ShowInUserList], N'0' AS [ShowInTableView], N'1' AS [ShowInExport], N'0' AS [Mandatory], N'0' AS [Sortable], N'-1' AS [SortNo], N'1' AS [SortDirection], N'120' AS [Width], N'1' AS [Align], N'' AS [ItemPrefix], N'' AS [ItemSuffix], N'tdSort' AS [HeaderCssClass], N'td' AS [ItemCssClass], GETDATE() AS [Created], N'' AS [ColumnCssClass], N'tblListGroupTd' AS [HeaderGroupCSSClass], N'0' AS [HeaderGroupLastItem], N'02F273CF-68C5-4622-800E-59DB65EE474C' AS [MasterID]
        FROM [list].[ItemList] [l]
        WHERE [l].[MasterID] = '4A424300-A5C4-4511-A8EB-B837A5358B11';
        DECLARE @ItemListFieldID_3750 nvarchar(32)= SCOPE_IDENTITY();

        INSERT INTO [list].[LT_ItemListField] ([LanguageID], [ItemListFieldID], [Name], [Description], [HeaderText], [HeaderGroupText], [HeaderTooltipText], [ItemTooltipText], [ItemText], [ExpandingItemText], [DefaultValues], [AvailableValues])
        SELECT l.[LanguageID], @ItemListFieldID_3750 AS [ItemListFieldID], N'KARAKTER  MV1' AS [Name], N'KARAKTER  MV1' AS [Description], N'KARAKTER  MV1' AS [HeaderText], N'' AS [HeaderGroupText], N'' AS [HeaderTooltipText], N'' AS [ItemTooltipText], N'' AS [ItemText], N'{Mv1}' AS [ExpandingItemText], N'' AS [DefaultValues], N'' AS [AvailableValues]
        FROM [dbo].[Language] l
        WHERE l.[LanguageID] IN (1,7);

        INSERT INTO [list].[LT_ItemListField] ([LanguageID], [ItemListFieldID], [Name], [Description], [HeaderText], [HeaderGroupText], [HeaderTooltipText], [ItemTooltipText], [ItemText], [ExpandingItemText], [DefaultValues], [AvailableValues])
        SELECT N'2' AS [LanguageID], @ItemListFieldID_3750 AS [ItemListFieldID], N'MV1' AS [Name], N'MV1' AS [Description], N'MV1' AS [HeaderText], N'' AS [HeaderGroupText], N'' AS [HeaderTooltipText], N'' AS [ItemTooltipText], N'' AS [ItemText], N'{Mv1}' AS [ExpandingItemText], N'' AS [DefaultValues], N'' AS [AvailableValues];
    END;

    IF NOT EXISTS (SELECT 1 FROM [list].[ItemListField] [f] WHERE [f].[MasterID] = 'C73302F2-EF7C-4FE8-B32F-7D935E9F710B')
    BEGIN
        INSERT INTO [list].[ItemListField] ([ItemListID], [No], [TableTypeID], [FieldType], [ValueType], [FieldName], [PropID], [QuestionID], [OverrideLocked], [ShowInUserList], [ShowInTableView], [ShowInExport], [Mandatory], [Sortable], [SortNo], [SortDirection], [Width], [Align], [ItemPrefix], [ItemSuffix], [HeaderCssClass], [ItemCssClass], [Created], [ColumnCssClass], [HeaderGroupCSSClass], [HeaderGroupLastItem], [MasterID])
        SELECT [l].[ItemListID] AS [ItemListID], N'5' AS [No], NULL AS [TableTypeID], N'0' AS [FieldType], N'0' AS [ValueType], N'' AS [FieldName], N'0' AS [PropID], NULL AS [QuestionID], N'0' AS [OverrideLocked], N'0' AS [ShowInUserList], N'0' AS [ShowInTableView], N'1' AS [ShowInExport], N'0' AS [Mandatory], N'0' AS [Sortable], N'-1' AS [SortNo], N'1' AS [SortDirection], N'120' AS [Width], N'1' AS [Align], N'' AS [ItemPrefix], N'' AS [ItemSuffix], N'tdNosort' AS [HeaderCssClass], N'td' AS [ItemCssClass], GETDATE() AS [Created], N'' AS [ColumnCssClass], N'tblListGroupTd' AS [HeaderGroupCSSClass], N'0' AS [HeaderGroupLastItem], N'C73302F2-EF7C-4FE8-B32F-7D935E9F710B' AS [MasterID]
        FROM [list].[ItemList] [l]
        WHERE [l].[MasterID] = '4A424300-A5C4-4511-A8EB-B837A5358B11';
        DECLARE @ItemListFieldID_3751 nvarchar(32)= SCOPE_IDENTITY();

        INSERT INTO [list].[LT_ItemListField] ([LanguageID], [ItemListFieldID], [Name], [Description], [HeaderText], [HeaderGroupText], [HeaderTooltipText], [ItemTooltipText], [ItemText], [ExpandingItemText], [DefaultValues], [AvailableValues])
        SELECT l.[LanguageID], @ItemListFieldID_3751 AS [ItemListFieldID], N'KARAKTER T1' AS [Name], N'KARAKTER T1' AS [Description], N'KARAKTER T1' AS [HeaderText], N'' AS [HeaderGroupText], N'' AS [HeaderTooltipText], N'' AS [ItemTooltipText], N'' AS [ItemText], N'{T1}' AS [ExpandingItemText], N'' AS [DefaultValues], N'' AS [AvailableValues]
        FROM [dbo].[Language] l
        WHERE l.[LanguageID] IN (1,7);

        INSERT INTO [list].[LT_ItemListField] ([LanguageID], [ItemListFieldID], [Name], [Description], [HeaderText], [HeaderGroupText], [HeaderTooltipText], [ItemTooltipText], [ItemText], [ExpandingItemText], [DefaultValues], [AvailableValues])
        SELECT N'2' AS [LanguageID], @ItemListFieldID_3751 AS [ItemListFieldID], N'T1' AS [Name], N'T1' AS [Description], N'T1' AS [HeaderText], N'' AS [HeaderGroupText], N'' AS [HeaderTooltipText], N'' AS [ItemTooltipText], N'' AS [ItemText], N'{T1}' AS [ExpandingItemText], N'' AS [DefaultValues], N'' AS [AvailableValues];
    END;

    IF NOT EXISTS (SELECT 1 FROM [list].[ItemListField] [f] WHERE [f].[MasterID] = '461444E1-9C52-4446-9B8D-738838A83E10')
    BEGIN
        INSERT INTO [list].[ItemListField] ([ItemListID], [No], [TableTypeID], [FieldType], [ValueType], [FieldName], [PropID], [QuestionID], [OverrideLocked], [ShowInUserList], [ShowInTableView], [ShowInExport], [Mandatory], [Sortable], [SortNo], [SortDirection], [Width], [Align], [ItemPrefix], [ItemSuffix], [HeaderCssClass], [ItemCssClass], [Created], [ColumnCssClass], [HeaderGroupCSSClass], [HeaderGroupLastItem], [MasterID])
        SELECT [l].[ItemListID] AS [ItemListID], N'6' AS [No], NULL AS [TableTypeID], N'0' AS [FieldType], N'0' AS [ValueType], N'' AS [FieldName], N'0' AS [PropID], NULL AS [QuestionID], N'0' AS [OverrideLocked], N'0' AS [ShowInUserList], N'0' AS [ShowInTableView], N'1' AS [ShowInExport], N'0' AS [Mandatory], N'0' AS [Sortable], N'-1' AS [SortNo], N'1' AS [SortDirection], N'120' AS [Width], N'1' AS [Align], N'' AS [ItemPrefix], N'' AS [ItemSuffix], N'tdNosort' AS [HeaderCssClass], N'td' AS [ItemCssClass], GETDATE() AS [Created], N'' AS [ColumnCssClass], N'tblListGroupTd' AS [HeaderGroupCSSClass], N'0' AS [HeaderGroupLastItem], N'461444E1-9C52-4446-9B8D-738838A83E10' AS [MasterID]
        FROM [list].[ItemList] [l]
        WHERE [l].[MasterID] = '4A424300-A5C4-4511-A8EB-B837A5358B11';
        DECLARE @ItemListFieldID_3752 nvarchar(32)= SCOPE_IDENTITY();

        INSERT INTO [list].[LT_ItemListField] ([LanguageID], [ItemListFieldID], [Name], [Description], [HeaderText], [HeaderGroupText], [HeaderTooltipText], [ItemTooltipText], [ItemText], [ExpandingItemText], [DefaultValues], [AvailableValues])
        SELECT l.[LanguageID], @ItemListFieldID_3752 AS [ItemListFieldID], N'KARAKTER  MV2' AS [Name], N'KARAKTER  MV2' AS [Description], N'KARAKTER  MV2' AS [HeaderText], N'' AS [HeaderGroupText], N'' AS [HeaderTooltipText], N'' AS [ItemTooltipText], N'' AS [ItemText], N'{Mv2}' AS [ExpandingItemText], N'' AS [DefaultValues], N'' AS [AvailableValues]
        FROM [dbo].[Language] l
        WHERE l.[LanguageID] IN (1,7);

        INSERT INTO [list].[LT_ItemListField] ([LanguageID], [ItemListFieldID], [Name], [Description], [HeaderText], [HeaderGroupText], [HeaderTooltipText], [ItemTooltipText], [ItemText], [ExpandingItemText], [DefaultValues], [AvailableValues])
        SELECT N'2' AS [LanguageID], @ItemListFieldID_3752 AS [ItemListFieldID], N'MV2' AS [Name], N'MV2' AS [Description], N'MV2' AS [HeaderText], N'' AS [HeaderGroupText], N'' AS [HeaderTooltipText], N'' AS [ItemTooltipText], N'' AS [ItemText], N'{Mv2}' AS [ExpandingItemText], N'' AS [DefaultValues], N'' AS [AvailableValues];
    END;

    IF NOT EXISTS (SELECT 1 FROM [list].[ItemListField] [f] WHERE [f].[MasterID] = 'BB3868F6-62CB-454D-BFE7-947FDACBE5AD')
    BEGIN
        INSERT INTO [list].[ItemListField] ([ItemListID], [No], [TableTypeID], [FieldType], [ValueType], [FieldName], [PropID], [QuestionID], [OverrideLocked], [ShowInUserList], [ShowInTableView], [ShowInExport], [Mandatory], [Sortable], [SortNo], [SortDirection], [Width], [Align], [ItemPrefix], [ItemSuffix], [HeaderCssClass], [ItemCssClass], [Created], [ColumnCssClass], [HeaderGroupCSSClass], [HeaderGroupLastItem], [MasterID])
        SELECT [l].[ItemListID] AS [ItemListID], N'7' AS [No], NULL AS [TableTypeID], N'0' AS [FieldType], N'0' AS [ValueType], N'' AS [FieldName], N'0' AS [PropID], NULL AS [QuestionID], N'0' AS [OverrideLocked], N'0' AS [ShowInUserList], N'0' AS [ShowInTableView], N'1' AS [ShowInExport], N'0' AS [Mandatory], N'0' AS [Sortable], N'-1' AS [SortNo], N'1' AS [SortDirection], N'120' AS [Width], N'1' AS [Align], N'' AS [ItemPrefix], N'' AS [ItemSuffix], N'tdNosort' AS [HeaderCssClass], N'td' AS [ItemCssClass], GETDATE() AS [Created], N'' AS [ColumnCssClass], N'tblListGroupTd' AS [HeaderGroupCSSClass], N'0' AS [HeaderGroupLastItem], N'BB3868F6-62CB-454D-BFE7-947FDACBE5AD' AS [MasterID]
        FROM [list].[ItemList] [l]
        WHERE [l].[MasterID] = '4A424300-A5C4-4511-A8EB-B837A5358B11';
        DECLARE @ItemListFieldID_3753 nvarchar(32)= SCOPE_IDENTITY();

        INSERT INTO [list].[LT_ItemListField] ([LanguageID], [ItemListFieldID], [Name], [Description], [HeaderText], [HeaderGroupText], [HeaderTooltipText], [ItemTooltipText], [ItemText], [ExpandingItemText], [DefaultValues], [AvailableValues])
        SELECT l.[LanguageID], @ItemListFieldID_3753 AS [ItemListFieldID], N'KARAKTER T2' AS [Name], N'KARAKTER T2' AS [Description], N'KARAKTER T2' AS [HeaderText], N'' AS [HeaderGroupText], N'' AS [HeaderTooltipText], N'' AS [ItemTooltipText], N'' AS [ItemText], N'{T2}' AS [ExpandingItemText], N'' AS [DefaultValues], N'' AS [AvailableValues]
        FROM [dbo].[Language] l
        WHERE l.[LanguageID] IN (1,7);

        INSERT INTO [list].[LT_ItemListField] ([LanguageID], [ItemListFieldID], [Name], [Description], [HeaderText], [HeaderGroupText], [HeaderTooltipText], [ItemTooltipText], [ItemText], [ExpandingItemText], [DefaultValues], [AvailableValues])
        SELECT N'2' AS [LanguageID], @ItemListFieldID_3753 AS [ItemListFieldID], N'T2' AS [Name], N'T2' AS [Description], N'T2' AS [HeaderText], N'' AS [HeaderGroupText], N'' AS [HeaderTooltipText], N'' AS [ItemTooltipText], N'' AS [ItemText], N'{T2}' AS [ExpandingItemText], N'' AS [DefaultValues], N'' AS [AvailableValues];
    END;
END;
--#endregion ItemList 1409

--#region PortalPage 3893
IF NOT EXISTS (SELECT 1 FROM [app].[PortalPage] [pp] WHERE [pp].[MasterID] = '2291E839-0E40-44F2-9C8C-1E86991101A1')
BEGIN
    INSERT INTO [app].[PortalPage] ([OwnerID], [No], [Parameter], [ExtID], [Created], [MasterID])
    SELECT [o].[OwnerID] AS [OwnerID], N'61' AS [No], N'RenderToolbar=false&rendername=false&cssClass=clearfix col-xs-12 container-detail-status' AS [Parameter], N'' AS [ExtID], GETDATE() AS [Created], N'2291E839-0E40-44F2-9C8C-1E86991101A1' AS [MasterID]
    FROM [org].[Owner] [o]
    WHERE [o].[MasterID] = '1C483E02-E808-42A7-8210-B11F4051D2E4';
    DECLARE @PortalPageID_3893 nvarchar(32)= SCOPE_IDENTITY();

    INSERT INTO [app].[LT_PortalPage] ([LanguageID], [PortalPageID], [Name], [Description])
    SELECT l.[LanguageID], @PortalPageID_3893 AS [PortalPageID], N'[Content page] Status Object Follow Up' AS [Name], N'' AS [Description]
    FROM [dbo].[Language] l
    WHERE l.[LanguageID] IN (1,2,7);

    DECLARE @ItemListID_1407_11021352998251 nvarchar(32)= '';
    SELECT @ItemListID_1407_11021352998251 = [l].[ItemListID]
    FROM [list].[ItemList] [l]
    WHERE [l].[MasterID] = '422130C6-7601-4288-9D87-96144387C6DA';
    IF NOT EXISTS (SELECT 1 FROM [app].[PortalPart] [pt] WHERE [pt].[MasterID] = '2B7F7F91-CA04-468B-AFBC-1C8930C1EAC0')
    BEGIN
        INSERT INTO [app].[PortalPart] ([PortalPageID], [PortalPartTypeID], [No], [CategoryID], [Settings], [BubbleID], [ReportPartID], [ReportID], [CssClass], [InlineStyle], [ActivityID], [SurveyID], [ExtID], [MasterID])
        SELECT [pp].[PortalPageID] AS [PortalPageID], N'21' AS [PortalPartTypeID], N'1' AS [No], NULL AS [CategoryID], N'<params>listid%3D'+@ItemListID_1407_11021352998251+'%26groupBox%3D'+'false'+'%26renderTopContentBox%3D'+'false'+'%26HiddenActivityIds%3D'+'0'+'</params>' AS [Settings], NULL AS [BubbleID], NULL AS [ReportPartID], NULL AS [ReportID], N'table-responsive' AS [CssClass], N'' AS [InlineStyle], NULL AS [ActivityID], NULL AS [SurveyID], N'' AS [ExtID], N'2B7F7F91-CA04-468B-AFBC-1C8930C1EAC0' AS [MasterID]
        FROM [app].[PortalPage] [pp]
        WHERE [pp].[MasterID] = '2291E839-0E40-44F2-9C8C-1E86991101A1';
        DECLARE @PortalPartID_5622 nvarchar(32)= SCOPE_IDENTITY();

        INSERT INTO [app].[LT_PortalPart] ([LanguageID], [PortalPartID], [Name], [Description], [Text], [TextAbove], [TextBelow], [HeaderText], [FooterText], [BeforeContentText], [AfterContentText])
        SELECT l.[LanguageID], @PortalPartID_5622 AS [PortalPartID], N'Object Follow Up' AS [Name], N'' AS [Description], N'' AS [Text], N'' AS [TextAbove], N'' AS [TextBelow], N'' AS [HeaderText], N'' AS [FooterText], N'' AS [BeforeContentText], N'' AS [AfterContentText]
        FROM [dbo].[Language] l
        WHERE l.[LanguageID] IN (1,2,7);
    END;
END;
--#endregion PortalPage 3893

--#region PortalPage 3894
IF NOT EXISTS (SELECT 1 FROM [app].[PortalPage] [pp] WHERE [pp].[MasterID] = '46C3D143-9995-46AD-AD7C-E12123D89900')
BEGIN
    INSERT INTO [app].[PortalPage] ([OwnerID], [No], [Parameter], [ExtID], [Created], [MasterID])
    SELECT [o].[OwnerID] AS [OwnerID], N'62' AS [No], N'RenderToolbar=false&rendername=false&cssClass=clearfix col-xs-12 container-detail-status' AS [Parameter], N'' AS [ExtID], GETDATE() AS [Created], N'46C3D143-9995-46AD-AD7C-E12123D89900' AS [MasterID]
    FROM [org].[Owner] [o]
    WHERE [o].[MasterID] = '1C483E02-E808-42A7-8210-B11F4051D2E4';
    DECLARE @PortalPageID_3894 nvarchar(32)= SCOPE_IDENTITY();

    INSERT INTO [app].[LT_PortalPage] ([LanguageID], [PortalPageID], [Name], [Description])
    SELECT l.[LanguageID], @PortalPageID_3894 AS [PortalPageID], N'[Detail page] Object Follow Up Status' AS [Name], N'' AS [Description]
    FROM [dbo].[Language] l
    WHERE l.[LanguageID] IN (1,2,7);

    DECLARE @ItemListID_1408_11115758020931 nvarchar(32)= '';
    SELECT @ItemListID_1408_11115758020931 = [l].[ItemListID]
    FROM [list].[ItemList] [l]
    WHERE [l].[MasterID] = '45891B66-0415-46FD-9ADF-DBB4221EFFD0';
    IF NOT EXISTS (SELECT 1 FROM [app].[PortalPart] [pt] WHERE [pt].[MasterID] = '207E06DF-BDDC-4BE7-AB87-9E24FB405089')
    BEGIN
        INSERT INTO [app].[PortalPart] ([PortalPageID], [PortalPartTypeID], [No], [CategoryID], [Settings], [BubbleID], [ReportPartID], [ReportID], [CssClass], [InlineStyle], [ActivityID], [SurveyID], [ExtID], [MasterID])
        SELECT [pp].[PortalPageID] AS [PortalPageID], N'21' AS [PortalPartTypeID], N'1' AS [No], NULL AS [CategoryID], N'<params>listid%3D'+@ItemListID_1408_11115758020931+'%26groupbox%3D'+'false'+'%26renderTopContentBox%3D'+'false'+'</params>' AS [Settings], NULL AS [BubbleID], NULL AS [ReportPartID], NULL AS [ReportID], N'' AS [CssClass], N'' AS [InlineStyle], NULL AS [ActivityID], NULL AS [SurveyID], N'' AS [ExtID], N'207E06DF-BDDC-4BE7-AB87-9E24FB405089' AS [MasterID]
        FROM [app].[PortalPage] [pp]
        WHERE [pp].[MasterID] = '46C3D143-9995-46AD-AD7C-E12123D89900';
        DECLARE @PortalPartID_5623 nvarchar(32)= SCOPE_IDENTITY();

        INSERT INTO [app].[LT_PortalPart] ([LanguageID], [PortalPartID], [Name], [Description], [Text], [TextAbove], [TextBelow], [HeaderText], [FooterText], [BeforeContentText], [AfterContentText])
        SELECT l.[LanguageID], @PortalPartID_5623 AS [PortalPartID], N'Activity detail list' AS [Name], N'' AS [Description], N'' AS [Text], N'' AS [TextAbove], N'' AS [TextBelow], N'' AS [HeaderText], N'' AS [FooterText], N'' AS [BeforeContentText], N'' AS [AfterContentText]
        FROM [dbo].[Language] l
        WHERE l.[LanguageID] IN (1,2,7);
    END;

    DECLARE @PortalPartID_5623_10243289286381 nvarchar(32)= '';
    SELECT @PortalPartID_5623_10243289286381 = [pt].[PortalPartID]
    FROM [app].[PortalPart] [pt]
    WHERE [pt].[MasterID] = '207E06DF-BDDC-4BE7-AB87-9E24FB405089';
    IF NOT EXISTS (SELECT 1 FROM [list].[ItemListRowAttribute] [att] WHERE [att].[MasterID] = 'FE1556FA-A373-48EF-BD04-790278E6AC7A')
    BEGIN
        INSERT INTO [list].[ItemListRowAttribute] ([ItemListID], [Key], [Value], [Created], [MasterID])
        SELECT [l].[ItemListID] AS [ItemListID], N'data-ppartid' AS [Key], N''+@PortalPartID_5623_10243289286381+'' AS [Value], GETDATE() AS [Created], N'FE1556FA-A373-48EF-BD04-790278E6AC7A' AS [MasterID]
        FROM [list].[ItemList] [l]
        WHERE [l].[MasterID] = '422130C6-7601-4288-9D87-96144387C6DA';
    END;
    
    DECLARE @PortalPartID_5623_13183661727441 nvarchar(32)= '';
    SELECT @PortalPartID_5623_13183661727441 = [pt].[PortalPartID]
    FROM [app].[PortalPart] [pt]
    WHERE [pt].[MasterID] = '207E06DF-BDDC-4BE7-AB87-9E24FB405089';
    IF NOT EXISTS (SELECT 1 FROM [list].[ItemListRowAttribute] [att] WHERE [att].[MasterID] = '261DA53E-4028-43B9-B4E2-73B9A289C1D5')
    BEGIN
        INSERT INTO [list].[ItemListRowAttribute] ([ItemListID], [Key], [Value], [Created], [MasterID])
        SELECT [l].[ItemListID] AS [ItemListID], N'data-ppartid' AS [Key], N''+@PortalPartID_5623_13183661727441+'' AS [Value], GETDATE() AS [Created], N'261DA53E-4028-43B9-B4E2-73B9A289C1D5' AS [MasterID]
        FROM [list].[ItemList] [l]
        WHERE [l].[MasterID] = '4A424300-A5C4-4511-A8EB-B837A5358B11';
    END;

END;
--#endregion PortalPage 3894

--#region PortalPage 3895
IF NOT EXISTS (SELECT 1 FROM [app].[PortalPage] [pp] WHERE [pp].[MasterID] = '23E4FEEE-BCC1-47FD-AE5F-AE6D377F958E')
BEGIN
    INSERT INTO [app].[PortalPage] ([OwnerID], [No], [Parameter], [ExtID], [Created], [MasterID])
    SELECT [o].[OwnerID] AS [OwnerID], N'66' AS [No], N'RenderToolbar=false&rendername=false&cssClass=clearfix col-xs-12 container-detail-status' AS [Parameter], N'' AS [ExtID], GETDATE() AS [Created], N'23E4FEEE-BCC1-47FD-AE5F-AE6D377F958E' AS [MasterID]
    FROM [org].[Owner] [o]
    WHERE [o].[MasterID] = '1C483E02-E808-42A7-8210-B11F4051D2E4';
    DECLARE @PortalPageID_3895 nvarchar(32)= SCOPE_IDENTITY();

    INSERT INTO [app].[LT_PortalPage] ([LanguageID], [PortalPageID], [Name], [Description])
    SELECT l.[LanguageID], @PortalPageID_3895 AS [PortalPageID], N'[Expanding page] History Object Follow Up Status List' AS [Name], N'' AS [Description]
    FROM [dbo].[Language] l
    WHERE l.[LanguageID] IN (1,2,7);

    DECLARE @ItemListID_1409_11162680118191 nvarchar(32)= '';
    SELECT @ItemListID_1409_11162680118191 = [l].[ItemListID]
    FROM [list].[ItemList] [l]
    WHERE [l].[MasterID] = '4A424300-A5C4-4511-A8EB-B837A5358B11';
    IF NOT EXISTS (SELECT 1 FROM [app].[PortalPart] [pt] WHERE [pt].[MasterID] = '0584792F-4F3F-4B3E-85DE-83960E3A823A')
    BEGIN
        INSERT INTO [app].[PortalPart] ([PortalPageID], [PortalPartTypeID], [No], [CategoryID], [Settings], [BubbleID], [ReportPartID], [ReportID], [CssClass], [InlineStyle], [ActivityID], [SurveyID], [ExtID], [MasterID])
        SELECT [pp].[PortalPageID] AS [PortalPageID], N'21' AS [PortalPartTypeID], N'1' AS [No], NULL AS [CategoryID], N'<params>listid%3D'+@ItemListID_1409_11162680118191+'%26groupBox%3D'+'false'+'%26renderTopContentBox%3D'+'false'+'%26HiddenActivityIds%3D'+'0'+'</params>' AS [Settings], NULL AS [BubbleID], NULL AS [ReportPartID], NULL AS [ReportID], N'container-xcategory' AS [CssClass], N'' AS [InlineStyle], NULL AS [ActivityID], NULL AS [SurveyID], N'' AS [ExtID], N'0584792F-4F3F-4B3E-85DE-83960E3A823A' AS [MasterID]
        FROM [app].[PortalPage] [pp]
        WHERE [pp].[MasterID] = '23E4FEEE-BCC1-47FD-AE5F-AE6D377F958E';
        DECLARE @PortalPartID_5624 nvarchar(32)= SCOPE_IDENTITY();

        INSERT INTO [app].[LT_PortalPart] ([LanguageID], [PortalPartID], [Name], [Description], [Text], [TextAbove], [TextBelow], [HeaderText], [FooterText], [BeforeContentText], [AfterContentText])
        SELECT l.[LanguageID], @PortalPartID_5624 AS [PortalPartID], N'Object Follow Up history list' AS [Name], N'' AS [Description], N'' AS [Text], N'' AS [TextAbove], N'' AS [TextBelow], N'' AS [HeaderText], N'' AS [FooterText], N'' AS [BeforeContentText], N'' AS [AfterContentText]
        FROM [dbo].[Language] l
        WHERE l.[LanguageID] IN (1,2,7);
    END;
END;
--#endregion PortalPage 3895

--#region Update ItemList 1364
DECLARE @XCID_82 nvarchar(32) = '', @PortalPage_3893 nvarchar(32) = '', @PortalPage_3832 nvarchar(32) = '', @PortalPart_5624 nvarchar(32) = '',
        @PortalPart_5473 nvarchar(32) = '', @ActivityID_600 nvarchar(32) = '';

SELECT @XCID_82 = xc.[XCID] FROM [at].[XCategory] xc WHERE xc.[MasterID] = '2DC9D36B-E35C-48B6-B4A3-818AF62EF88B';
SELECT @PortalPage_3893 = pp.[PortalPageID] FROM [app].[PortalPage] pp WHERE pp.[MasterID] = '2291E839-0E40-44F2-9C8C-1E86991101A1';
SELECT @PortalPage_3832 = pp.[PortalPageID] FROM [app].[PortalPage] pp WHERE pp.[MasterID] = 'B1171FD9-6E7E-461C-BE37-AFCDE76A0B03';
SELECT @PortalPart_5473 = pt.[PortalPartID] FROM [app].[PortalPart] pt WHERE pt.[MasterID] = '4680DDC1-ED57-4749-A5BF-8704AA886D3A';
SELECT @PortalPart_5624 = pt.[PortalPartID] FROM [app].[PortalPart] pt WHERE pt.[MasterID] = '0584792F-4F3F-4B3E-85DE-83960E3A823A';
SELECT @ActivityID_600 = a.[ActivityID] FROM [at].[Activity] a WHERE a.[MasterID] = 'C9CD0427-CBE9-4AB1-8F96-916771EE444E';

UPDATE lt SET lt.[ItemText] = N'<a data-xcid="{XCategoryId}" data-ppageid={if({"{XCategoryId}"="'+@XCID_82+'"},"'+@PortalPage_3893+'","'+@PortalPage_3832+'")} data-partid={if({"{XCategoryId}"="'+@XCID_82+'"},"'+@PortalPart_5624+'","'+@PortalPart_5473+'")} data-aid={if({"{XCategoryId}"="'+@XCID_82+'"},"'+@ActivityID_600+'","0")} href="#">{XCategoryName}</a>'
FROM [list].[ItemListField] f
JOIN [list].[LT_ItemListField] lt ON lt.[ItemListFieldID] = f.[ItemListFieldID] AND f.[MasterID] = '23A5ADC2-0D65-4FC2-A141-289F9D918A5E'
--#endregion Update ItemList 1364

--#region Misc Update
UPDATE [list].[ItemListField] SET [ItemCssClass] = [ItemCssClass] + ' td-show-more'
WHERE [MasterID] = '24ED1A5F-941A-4AF1-9E43-B6159D481D06' AND [ItemCssClass] NOT LIKE '%td-show-more%';

UPDATE lt SET lt.[ItemText] = REPLACE(lt.[ItemText], '"{SubjectName}"', '"{SubjectCode} - {SubjectName}"')
FROM [list].[ItemListField] f
JOIN [list].[LT_ItemListField] lt ON lt.[ItemListFieldID] = f.[ItemListFieldID]
 AND f.[MasterID] IN ('00773C12-2B03-4151-8972-612FC6A03AC5','06A9637F-7CBF-43B4-A247-D60F0897638F')
 AND lt.[ItemText] NOT LIKE '%{SubjectCode} - %';

UPDATE [list].[ItemListField] SET [ItemCssClass] = 'command ' + [ItemCssClass]
WHERE [MasterID] = '24ED1A5F-941A-4AF1-9E43-B6159D481D06' AND [ItemCssClass] NOT LIKE '%command%';
--#endregion Misc Update
--#endregion 02. CE-2163 Config Data
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
--#region 03. CE-2191 Status Page Group By Schools
-- Dev: Ray
-- Feature: Status Page for VGO
-- CE-2208 [DB] Status page group by schools
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[at].[prc_SurveysStatusByUGAndSubject]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [at].[prc_SurveysStatusByUGAndSubject] AS' 
END
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
/*  2018-12-17 Ray:     CE-2127 Create stored to get data for status page of activity 600 (Oppfølging i fag)
    2018-12-26 Ray:     CE-2182 Ignore filter on alternative values (counts - and Fritatt)
                        Excludes deActive users
    2018-12-31 Ray:     Count distinct users
    2019-01-07 Ray:     CE-2208 [DB] Status page group by schools

Test cases:
1. School level: @GetSubDepartment = 0
   a. Current/history survey summary: @SurveyID > 0, @ViewDetail = 0 ==> Click view detail: see 1b
   b. Current/history survey detail: @SurveyID > 0, @ViewDetail = 1
   c. All surveys summary: @SurveyID = 0, @ViewDetail = 0 ==> Click view detail: see 1a
2. Municipality and higher levels: @GetSubDepartment = 1
   a. Current survey summary: @SurveyID > 0, @ViewDetail = 0 ==> Click view detail: see 1a
   b. All surveys summary: @SurveyID = 0, @ViewDetail = 0 ==> Click view detail: see 1a (with @HDID = current HD)

School level: EXEC [at].[prc_SurveysStatusByUGAndSubject] @HDID = 39485, @ActivityID = 600, @SurveyID = 4247, @Archetype_TeachingGroup = 7, @SiteID = 5, @ViewDetail = 1, @GetSubDepartment = 0, @SubjectCode = ''
Municipality and higher levels: EXEC [at].[prc_SurveysStatusByUGAndSubject] @HDID = 39384, @ActivityID = 600, @SurveyID = 4247, @Archetype_TeachingGroup = 7, @SiteID = 5, @ViewDetail = 0, @GetSubDepartment = 1, @SubjectCode = ''
*/
ALTER PROCEDURE [at].[prc_SurveysStatusByUGAndSubject]
(   @HDID                       int,
    @ActivityID                 int,
    @SurveyID                   int,
    @Archetype_TeachingGroup    int,
    @SiteID                     int,
    @ViewDetail                 bit = 0,
    @GetSubDepartment           bit = 0,                -- Set this to false when querying the leave nodes, which are schools
    @SubjectCode                nvarchar(max) = ''
) AS
BEGIN
    DECLARE @EntityStatus_Active int = 1, @strIKO_StatusColumn nvarchar(max) = '', @xmlStatusColumn XML, @AlternativeMasterIDList nvarchar(max) = '';
    SELECT @EntityStatus_Active = es.[EntityStatusID] FROM [dbo].[EntityStatus] es WHERE es.[CodeName] = 'Active';

    DECLARE @FilterTable TABLE ([QuestionID] int, [QuestionName] nvarchar(256), [AlternativeID] int, [ItemID] int);
    DECLARE @GroupLevel1 TABLE ([Level1ID] int, [Level1Name] nvarchar(4000));

    SELECT @strIKO_StatusColumn = sp.[Value] FROM [app].[SiteParameter] sp WHERE sp.[SiteID] = @SiteID AND sp.[Key] = 'Vokal.StatusPage.VGOColumnList';
    SET @xmlStatusColumn = CONVERT(xml, @strIKO_StatusColumn)
    SELECT @AlternativeMasterIDList = al.IDList.value('@MasterID', 'nvarchar(max)') FROM @xmlStatusColumn.nodes('//IncludeAlternative') AS al(IDList);

    IF @GetSubDepartment = 1
    BEGIN
        INSERT INTO @GroupLevel1 ([Level1ID], [Level1Name])
        SELECT hd.[HDID] AS [Level1ID], d.[Name] AS [Level1Name]
        FROM [org].[H_D] hd
        JOIN [org].[Department] d ON d.[DepartmentID] = hd.[DepartmentID] AND hd.[ParentID] = @HDID
         AND d.[EntityStatusID] = 1 AND d.[Deleted] IS NULL AND hd.[Deleted] = 0;
    END
    ELSE
    BEGIN
        INSERT INTO @GroupLevel1 ([Level1ID], [Level1Name])
        SELECT hd.[HDID] AS [Level1ID], d.[Name] AS [Level1Name]
        FROM [org].[H_D] hd
        JOIN [org].[Department] d ON d.[DepartmentID] = hd.[DepartmentID] AND hd.[HDID] = @HDID
         AND d.[EntityStatusID] = 1 AND d.[Deleted] IS NULL;
    END

--#region Status detail
    SELECT (CASE WHEN ISNULL(@SurveyID, 0) = 0 THEN s.[SurveyID]
                 WHEN @GetSubDepartment = 1 THEN lv1.[Level1ID]
                 WHEN @GetSubDepartment = 0 AND @ViewDetail = 0 THEN c.[CategoryID]
                 WHEN @GetSubDepartment = 0 AND @ViewDetail = 1 THEN ug.[UserGroupID]
            END) AS [Level1ID],
           (CASE WHEN ISNULL(@SurveyID, 0) = 0 THEN N'Survey'
                 WHEN @GetSubDepartment = 1 THEN lv1.[Level1Name]
                 WHEN @GetSubDepartment = 0 AND @ViewDetail = 0 THEN c.[ExtID]
                 WHEN @GetSubDepartment = 0 AND @ViewDetail = 1 THEN ug.[Name]
            END) AS [Level1Name],
           s.[SurveyID], c.[ExtID] AS [SubjectCode], c.[CategoryID], ug.[UserGroupID], ug.[Name] AS [UserGroupName]
    INTO #UserGroupDetail
    FROM @GroupLevel1 lv1
    JOIN [org].[H_D] hd ON hd.[Path] LIKE '%\' + CAST(lv1.[Level1ID] AS nvarchar(32)) + '\%' AND hd.[Deleted] = 0
    JOIN [at].[Survey] s ON s.[ActivityID] = @ActivityID AND s.[Status] = 2 AND (s.[SurveyID] = @SurveyID OR ISNULL(@SurveyID, 0) = 0) AND s.[ExtID] <> 'Global'
    JOIN [org].[UserGroup] ug ON ug.[DepartmentID] = hd.[DepartmentID] AND ug.[ArchetypeID] = @Archetype_TeachingGroup 
     AND ug.[PeriodID] = s.[PeriodID] AND ug.[EntityStatusID] = @EntityStatus_Active AND ug.[Deleted] IS NULL
     AND (ug.[Tag] = @SubjectCode OR @SubjectCode = '')
    JOIN [at].[Category] c ON c.[ActivityID] = @ActivityID AND c.[ExtID] = ug.[Tag];

    INSERT INTO @FilterTable ([QuestionID], [QuestionName], [AlternativeID], [ItemID])
    SELECT q.[QuestionID], Tbl.Col.value('@Name', 'nvarchar(max)') AS [QuestionName], NULL AS [AlternativeID], ugd.[CategoryID] AS [ItemID]
    FROM @xmlStatusColumn.nodes('//StatusColumn') AS Tbl(Col)
    JOIN [at].[Question] q ON q.[MasterID] = Tbl.Col.value('@QuestionMasterID', 'nvarchar(64)')
    --CROSS JOIN [dbo].[StringToArray] (@AlternativeMasterIDList, ',') AS altl
    --JOIN [at].[Alternative] alt ON alt.[MasterID] = altl.[ParseValue]
    CROSS JOIN (SELECT DISTINCT [CategoryID] FROM #UserGroupDetail) AS ugd;

    SELECT ugd.[Level1ID], r.[SurveyID], ft.[QuestionName], COUNT(ugm.[UserID]) AS [AnswerCount]
    INTO #AnswerDetail
    FROM #UserGroupDetail ugd
    JOIN [org].[UGMember] ugm ON ugm.[UserGroupID] = ugd.[UserGroupID] AND ugm.[EntityStatusID] = @EntityStatus_Active AND ugm.[Deleted] IS NULL
    JOIN [dbo].[Synonym_VOKAL_Result] r ON r.[SurveyID] = ugd.[SurveyID] AND r.[UserID] = ugm.[UserID]
     AND r.[EntityStatusID] = @EntityStatus_Active AND r.[Deleted] IS NULL
    JOIN @FilterTable ft ON ft.[ItemID] = ugd.[CategoryID]
    JOIN [dbo].[Synonym_VOKAL_Answer] a WITH (INDEX([IX_R_Q_A])) ON a.[ResultID] = r.[ResultID] AND a.[QuestionID] = ft.[QuestionID] --AND a.[AlternativeID] = ft.[AlternativeID]
     AND a.[ItemID] = ft.[ItemID]
    GROUP BY ugd.[Level1ID], r.[SurveyID], ft.[QuestionName];

    SELECT ugd.[Level1ID], ugd.[Level1Name], ugd.[SurveyID], COUNT(DISTINCT ugm.[UserID]) AS [UserCount]
    INTO #UserCount
    FROM #UserGroupDetail ugd
    LEFT JOIN [org].[UGMember] ugm ON ugm.[UserGroupID] = ugd.[UserGroupID] AND ugm.[EntityStatusID] = @EntityStatus_Active AND ugm.[Deleted] IS NULL
    WHERE (ugm.[UserID] IS NULL
           OR EXISTS (SELECT 1 FROM [org].[User] u WHERE u.[UserID] = ugm.[UserID] AND u.[EntityStatusID] = @EntityStatus_Active AND u.[Deleted] IS NULL)
          )
    GROUP BY ugd.[Level1ID], ugd.[Level1Name], ugd.[SurveyID];
--#endregion Status detail

--#region Return data
    SELECT p.[Level1ID], p.[SurveyID], p.[Mv1], p.[T1], p.[Mv2], p.[T2]
    INTO #StatusTable
    FROM #AnswerDetail ad 
    PIVOT (SUM(ad.[AnswerCount]) FOR ad.[QuestionName] IN ([Mv1], [T1], [Mv2], [T2])) AS p;

    SELECT uc.[Level1ID], uc.[Level1Name], uc.[UserCount],
           ISNULL(st.[Mv1],0) AS [Mv1], ISNULL(st.[T1],0) AS [T1], ISNULL(st.[Mv2],0) AS [Mv2], ISNULL(st.[T2],0) AS [T2]
    FROM #UserCount uc
    LEFT JOIN #StatusTable st ON st.[Level1ID] = uc.[Level1ID] AND st.[SurveyID] = uc.[SurveyID];
--#endregion Return data

--#region Cleanup
    DROP TABLE IF EXISTS #UserGroupDetail;
    DROP TABLE IF EXISTS #AnswerDetail;
    DROP TABLE IF EXISTS #StatusTable;
    DROP TABLE IF EXISTS #UserCount;
--#endregion Cleanup

END
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
------------------------------------------------------------------------------
-- Dev: Thomas
-- Feature: Status Page for VGO
-- CE-2204 [Server code] Status for Oppfolging i fag activity should group by school in case show all surveys
UPDATE lt SET lt.[ItemText] = N'{Level1Name}'
FROM [list].[ItemListField] f
JOIN [list].[LT_ItemListField] lt ON lt.[ItemListFieldID] = f.[ItemListFieldID] AND f.[MasterID] = '06A9637F-7CBF-43B4-A247-D60F0897638F';
--#endregion 03. CE-2191 Status Page Group By Schools

--#region 04. CE-2255 Filter Answers By Active Students
-- Dev: Ray
-- Feature: Status Page for VGO
-- CE-2255 [VGO][Status]- The number answer for each question is counted incorrectly.
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[at].[prc_SurveysStatusByUGAndSubject]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [at].[prc_SurveysStatusByUGAndSubject] AS' 
END
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
/*  2018-12-17 Ray:     CE-2127 Create stored to get data for status page of activity 600 (Oppfølging i fag)
    2018-12-26 Ray:     CE-2182 Ignore filter on alternative values (counts - and Fritatt)
                        Excludes deActive users
    2018-12-31 Ray:     Count distinct users
    2019-01-07 Ray:     CE-2208 [DB] Status page group by schools
    2019-01-23 Ray:     CE-2255 Filter answers by active students

Test cases:
1. School level: @GetSubDepartment = 0
   a. Current/history survey summary: @SurveyID > 0, @ViewDetail = 0 ==> Click view detail: see 1b
   b. Current/history survey detail: @SurveyID > 0, @ViewDetail = 1
   c. All surveys summary: @SurveyID = 0, @ViewDetail = 0 ==> Click view detail: see 1a
2. Municipality and higher levels: @GetSubDepartment = 1
   a. Current survey summary: @SurveyID > 0, @ViewDetail = 0 ==> Click view detail: see 1a
   b. All surveys summary: @SurveyID = 0, @ViewDetail = 0 ==> Click view detail: see 1a (with @HDID = current HD)

School level: EXEC [at].[prc_SurveysStatusByUGAndSubject] @HDID = 39485, @ActivityID = 600, @SurveyID = 4247, @Archetype_TeachingGroup = 7, @SiteID = 5, @ViewDetail = 1, @GetSubDepartment = 0, @SubjectCode = ''
Municipality and higher levels: EXEC [at].[prc_SurveysStatusByUGAndSubject] @HDID = 39384, @ActivityID = 600, @SurveyID = 4247, @Archetype_TeachingGroup = 7, @SiteID = 5, @ViewDetail = 0, @GetSubDepartment = 1, @SubjectCode = ''
*/
ALTER PROCEDURE [at].[prc_SurveysStatusByUGAndSubject]
(   @HDID                       int,
    @ActivityID                 int,
    @SurveyID                   int,
    @Archetype_TeachingGroup    int,
    @SiteID                     int,
    @ViewDetail                 bit = 0,
    @GetSubDepartment           bit = 0,                -- Set this to false when querying the leave nodes, which are schools
    @SubjectCode                nvarchar(max) = ''
) AS
BEGIN
    DECLARE @EntityStatus_Active int = 1, @strIKO_StatusColumn nvarchar(max) = '', @xmlStatusColumn XML, @AlternativeMasterIDList nvarchar(max) = '';
    SELECT @EntityStatus_Active = es.[EntityStatusID] FROM [dbo].[EntityStatus] es WHERE es.[CodeName] = 'Active';

    DECLARE @FilterTable TABLE ([QuestionID] int, [QuestionName] nvarchar(256), [AlternativeID] int, [ItemID] int);
    DECLARE @GroupLevel1 TABLE ([Level1ID] int, [Level1Name] nvarchar(4000));

    SELECT @strIKO_StatusColumn = sp.[Value] FROM [app].[SiteParameter] sp WHERE sp.[SiteID] = @SiteID AND sp.[Key] = 'Vokal.StatusPage.VGOColumnList';
    SET @xmlStatusColumn = CONVERT(xml, @strIKO_StatusColumn)
    SELECT @AlternativeMasterIDList = al.IDList.value('@MasterID', 'nvarchar(max)') FROM @xmlStatusColumn.nodes('//IncludeAlternative') AS al(IDList);

    IF @GetSubDepartment = 1
    BEGIN
        INSERT INTO @GroupLevel1 ([Level1ID], [Level1Name])
        SELECT hd.[HDID] AS [Level1ID], d.[Name] AS [Level1Name]
        FROM [org].[H_D] hd
        JOIN [org].[Department] d ON d.[DepartmentID] = hd.[DepartmentID] AND hd.[ParentID] = @HDID
         AND d.[EntityStatusID] = 1 AND d.[Deleted] IS NULL AND hd.[Deleted] = 0;
    END
    ELSE
    BEGIN
        INSERT INTO @GroupLevel1 ([Level1ID], [Level1Name])
        SELECT hd.[HDID] AS [Level1ID], d.[Name] AS [Level1Name]
        FROM [org].[H_D] hd
        JOIN [org].[Department] d ON d.[DepartmentID] = hd.[DepartmentID] AND hd.[HDID] = @HDID
         AND d.[EntityStatusID] = 1 AND d.[Deleted] IS NULL;
    END

--#region Status detail
    SELECT (CASE WHEN ISNULL(@SurveyID, 0) = 0 THEN s.[SurveyID]
                 WHEN @GetSubDepartment = 1 THEN lv1.[Level1ID]
                 WHEN @GetSubDepartment = 0 AND @ViewDetail = 0 THEN c.[CategoryID]
                 WHEN @GetSubDepartment = 0 AND @ViewDetail = 1 THEN ug.[UserGroupID]
            END) AS [Level1ID],
           (CASE WHEN ISNULL(@SurveyID, 0) = 0 THEN N'Survey'
                 WHEN @GetSubDepartment = 1 THEN lv1.[Level1Name]
                 WHEN @GetSubDepartment = 0 AND @ViewDetail = 0 THEN c.[ExtID]
                 WHEN @GetSubDepartment = 0 AND @ViewDetail = 1 THEN ug.[Name]
            END) AS [Level1Name],
           s.[SurveyID], c.[ExtID] AS [SubjectCode], c.[CategoryID], ug.[UserGroupID], ug.[Name] AS [UserGroupName]
    INTO #UserGroupDetail
    FROM @GroupLevel1 lv1
    JOIN [org].[H_D] hd ON hd.[Path] LIKE '%\' + CAST(lv1.[Level1ID] AS nvarchar(32)) + '\%' AND hd.[Deleted] = 0
    JOIN [at].[Survey] s ON s.[ActivityID] = @ActivityID AND s.[Status] = 2 AND (s.[SurveyID] = @SurveyID OR ISNULL(@SurveyID, 0) = 0) AND s.[ExtID] <> 'Global'
    JOIN [org].[UserGroup] ug ON ug.[DepartmentID] = hd.[DepartmentID] AND ug.[ArchetypeID] = @Archetype_TeachingGroup 
     AND ug.[PeriodID] = s.[PeriodID] AND ug.[EntityStatusID] = @EntityStatus_Active AND ug.[Deleted] IS NULL
     AND (ug.[Tag] = @SubjectCode OR @SubjectCode = '')
    JOIN [at].[Category] c ON c.[ActivityID] = @ActivityID AND c.[ExtID] = ug.[Tag];

    INSERT INTO @FilterTable ([QuestionID], [QuestionName], [AlternativeID], [ItemID])
    SELECT q.[QuestionID], Tbl.Col.value('@Name', 'nvarchar(max)') AS [QuestionName], NULL AS [AlternativeID], ugd.[CategoryID] AS [ItemID]
    FROM @xmlStatusColumn.nodes('//StatusColumn') AS Tbl(Col)
    JOIN [at].[Question] q ON q.[MasterID] = Tbl.Col.value('@QuestionMasterID', 'nvarchar(64)')
    --CROSS JOIN [dbo].[StringToArray] (@AlternativeMasterIDList, ',') AS altl
    --JOIN [at].[Alternative] alt ON alt.[MasterID] = altl.[ParseValue]
    CROSS JOIN (SELECT DISTINCT [CategoryID] FROM #UserGroupDetail) AS ugd;

    SELECT ugd.[Level1ID], r.[SurveyID], ft.[QuestionName], COUNT(ugm.[UserID]) AS [AnswerCount]
    INTO #AnswerDetail
    FROM #UserGroupDetail ugd
    JOIN [org].[UGMember] ugm ON ugm.[UserGroupID] = ugd.[UserGroupID] AND ugm.[EntityStatusID] = @EntityStatus_Active AND ugm.[Deleted] IS NULL
    JOIN [org].[User] u ON u.[UserID] = ugm.[UserID] AND u.[EntityStatusID] = @EntityStatus_Active AND u.[Deleted] IS NULL
    JOIN [dbo].[Synonym_VOKAL_Result] r ON r.[SurveyID] = ugd.[SurveyID] AND r.[UserID] = ugm.[UserID]
     AND r.[EntityStatusID] = @EntityStatus_Active AND r.[Deleted] IS NULL
    JOIN @FilterTable ft ON ft.[ItemID] = ugd.[CategoryID]
    JOIN [dbo].[Synonym_VOKAL_Answer] a WITH (INDEX([IX_R_Q_A])) ON a.[ResultID] = r.[ResultID] AND a.[QuestionID] = ft.[QuestionID] --AND a.[AlternativeID] = ft.[AlternativeID]
     AND a.[ItemID] = ft.[ItemID]
    GROUP BY ugd.[Level1ID], r.[SurveyID], ft.[QuestionName];

    SELECT ugd.[Level1ID], ugd.[Level1Name], ugd.[SurveyID], COUNT(DISTINCT ugm.[UserID]) AS [UserCount]
    INTO #UserCount
    FROM #UserGroupDetail ugd
    LEFT JOIN [org].[UGMember] ugm ON ugm.[UserGroupID] = ugd.[UserGroupID] AND ugm.[EntityStatusID] = @EntityStatus_Active AND ugm.[Deleted] IS NULL
    WHERE (ugm.[UserID] IS NULL
           OR EXISTS (SELECT 1 FROM [org].[User] u WHERE u.[UserID] = ugm.[UserID] AND u.[EntityStatusID] = @EntityStatus_Active AND u.[Deleted] IS NULL)
          )
    GROUP BY ugd.[Level1ID], ugd.[Level1Name], ugd.[SurveyID];
--#endregion Status detail

--#region Return data
    SELECT p.[Level1ID], p.[SurveyID], p.[Mv1], p.[T1], p.[Mv2], p.[T2]
    INTO #StatusTable
    FROM #AnswerDetail ad 
    PIVOT (SUM(ad.[AnswerCount]) FOR ad.[QuestionName] IN ([Mv1], [T1], [Mv2], [T2])) AS p;

    SELECT uc.[Level1ID], uc.[Level1Name], uc.[UserCount],
           ISNULL(st.[Mv1],0) AS [Mv1], ISNULL(st.[T1],0) AS [T1], ISNULL(st.[Mv2],0) AS [Mv2], ISNULL(st.[T2],0) AS [T2]
    FROM #UserCount uc
    LEFT JOIN #StatusTable st ON st.[Level1ID] = uc.[Level1ID] AND st.[SurveyID] = uc.[SurveyID];
--#endregion Return data

--#region Cleanup
    DROP TABLE IF EXISTS #UserGroupDetail;
    DROP TABLE IF EXISTS #AnswerDetail;
    DROP TABLE IF EXISTS #StatusTable;
    DROP TABLE IF EXISTS #UserCount;
--#endregion Cleanup

END
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
--#endregion 04. CE-2255 Filter Answers By Active Students

--#region 05. CE-2204 Fix Config For Grouping By Schools
-- Dev: Thomas
-- Feature: Status Page for VGO
-- CE-2204 [Server code] Status for Oppfolging i fag activity should group by school in case show all surveys
UPDATE lt SET lt.[ItemText] = N'{if({"{GetSubDepartment}"="0"},"{Level1Name}","{Level1Name} - {SubjectName}")}'
FROM [list].[ItemListField] f
JOIN [list].[LT_ItemListField] lt ON lt.[ItemListFieldID] = f.[ItemListFieldID] AND f.[MasterID] = '06A9637F-7CBF-43B4-A247-D60F0897638F';

--#endregion 05. CE-2204 Fix Config For Grouping By Schools

--#endregion 098. Status Page For VGO
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
-------------------------------------------------------------------------------------------------------------------
PRINT 'Setting database version'
/*
Start of versioning database-level extended properties.
*/
BEGIN
    DECLARE @DB_VERSION varchar(32), @ConceptName nvarchar(128), @ReleaseScriptName nvarchar(4000)
    SET @DB_VERSION = '6.12.006.002.00'
    SET @ConceptName = 'Engage Main'
    SET @ReleaseScriptName = N'v' + @DB_VERSION + N'-01. Engage Status Page - VGO'

    IF EXISTS (SELECT 1 FROM fn_listextendedproperty(N'cxStudio Database Version', NULL, NULL, NULL, NULL, NULL, NULL))
        EXEC sp_dropextendedproperty N'cxStudio Database Version', NULL, NULL, NULL, NULL, NULL, NULL
    EXEC sp_addextendedproperty N'cxStudio Database Version', @DB_VERSION, NULL, NULL, NULL, NULL, NULL, NULL
		
    IF EXISTS (SELECT 1 FROM fn_listextendedproperty(N'Script Name', NULL, NULL, NULL, NULL, NULL, NULL))
        EXEC sp_dropextendedproperty N'Script Name', NULL, NULL, NULL, NULL, NULL, NULL
    EXEC sp_addextendedproperty N'Script Name',  @ReleaseScriptName , NULL, NULL, NULL, NULL, NULL, NULL
END
/*
End of versioning database-level extended properties.
*/
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
IF EXISTS (SELECT * FROM #tmpErrors) ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT>0 BEGIN
PRINT 'The database update succeeded'
COMMIT TRANSACTION
END
ELSE PRINT 'The database update failed'
GO
DROP TABLE #tmpErrors
GO
SET NOEXEC OFF
GO