SET QUOTED_IDENTIFIER ON
GO
DECLARE @Current_DB_Version varchar(32), @CurrentConceptName nvarchar(128),
        @Required_DB_Version varchar(32), @RequiredConceptName nvarchar(128)

SET @Required_DB_Version = '6.12.006.%'
SET @RequiredConceptName = 'Engage Main'
PRINT 'This script is generated for executing on ' + @RequiredConceptName + ' version ' + @Required_DB_Version

SELECT @Current_DB_Version = convert(nvarchar(max),value) FROM fn_listextendedproperty(N'cxStudio Database Version', NULL, NULL, NULL, NULL, NULL, NULL)
SELECT @CurrentConceptName = convert(nvarchar(max),value) FROM fn_listextendedproperty(N'Concept Name', NULL, NULL, NULL, NULL, NULL, NULL)

IF @CurrentConceptName != @RequiredConceptName OR @Current_DB_Version NOT LIKE @Required_DB_Version
BEGIN
    PRINT 'Incorrect solution or cxStudio Database version'
    SET NOEXEC ON
END
GO
-------------------------------------------------------------------------------------------------------------------------
IF EXISTS (SELECT * FROM tempdb..sysobjects WHERE id=OBJECT_ID('tempdb..#tmpErrors')) DROP TABLE #tmpErrors
GO
CREATE TABLE #tmpErrors (Error int)
GO
SET XACT_ABORT ON
GO
SET TRANSACTION ISOLATION LEVEL SERIALIZABLE
GO
BEGIN TRANSACTION
GO
----------------------------------------------------------------------------------------------------------------
--#region 112. CE-2261 Download Document Template
-- Dev: David
-- Feature: Document template
-- CE-2261 [Atadmin][Settings][DocumentTemplate]- Cannot download document template.
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[rep].[prc_DocumentTemplate_Fileget]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [rep].[prc_DocumentTemplate_Fileget] AS' 
END
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
ALTER PROCEDURE [rep].[prc_DocumentTemplate_Fileget]
(
    @LanguageID         int,
    @DocumentTemplateID int
)
AS
BEGIN
    SET NOCOUNT ON
    DECLARE @Err int

    SELECT [FileContent]
    FROM [rep].[LT_DocumentTemplate]
    WHERE [LanguageID] = @LanguageID
      AND [DocumentTemplateID] = @DocumentTemplateID;

    SET @Err = @@Error

    RETURN @Err
END
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
--#endregion 112. CE-2261 Download Document Template
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
--#region 119. CE-2307 Access Control On TeachingGroup

--#region 01. Member Role
-- Dev: Thomas
-- Feature: VGO
-- CE-2307 Access control on teaching groups
IF NOT EXISTS (SELECT 1 FROM [org].[MemberRole] mr WHERE mr.[MasterID] = '9CF8821C-6B97-4FA4-84DE-80C28E9D779C')
BEGIN
    SET IDENTITY_INSERT [org].[MemberRole] ON
    INSERT INTO [org].[MemberRole] ([MemberRoleID], [OwnerID], [ExtID], [No], [Created], [EntityStatusID], [EntityStatusReasonID], [MasterID])
    SELECT 1 AS [MemberRoleID], o.[OwnerID], 'employee' AS [ExtID], 0 AS [No], GETDATE() AS [Created], 1 AS [EntityStatusID], 100 AS [EntityStatusReasonID], '9CF8821C-6B97-4FA4-84DE-80C28E9D779C' AS [MasterID]
    FROM [org].[Owner] o;
    SET IDENTITY_INSERT [org].[MemberRole] OFF

    INSERT INTO [org].[LT_MemberRole] ([MemberRoleID], [LanguageID], [Name], [Description])
    SELECT 1 AS [MemberRoleID], l.[LanguageID], 'Employee' AS [Name], '' AS [Description]
    FROM [dbo].[Language] l
    WHERE l.[LanguageID] IN (1,2,7);
END

IF NOT EXISTS (SELECT 1 FROM [org].[MemberRole] mr WHERE mr.[MasterID] = 'C36C34A6-A1B4-4625-9470-F37BE94691B4')
BEGIN
    SET IDENTITY_INSERT [org].[MemberRole] ON
    INSERT INTO [org].[MemberRole] ([MemberRoleID], [OwnerID], [ExtID], [No], [Created], [EntityStatusID], [EntityStatusReasonID], [MasterID])
    SELECT 2 AS [MemberRoleID], o.[OwnerID], 'learner' AS [ExtID], 0 AS [No], GETDATE() AS [Created], 1 AS [EntityStatusID], 100 AS [EntityStatusReasonID], 'C36C34A6-A1B4-4625-9470-F37BE94691B4' AS [MasterID]
    FROM [org].[Owner] o;
    SET IDENTITY_INSERT [org].[MemberRole] OFF

    INSERT INTO [org].[LT_MemberRole] ([MemberRoleID], [LanguageID], [Name], [Description])
    SELECT 2 AS [MemberRoleID], l.[LanguageID], 'Learner' AS [Name], '' AS [Description]
    FROM [dbo].[Language] l
    WHERE l.[LanguageID] IN (1,2,7);
END
--#endregion 01. Member Role

--#endregion 119. CE-2307 Access Control On TeachingGroup
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
--#region 123. Sort Questions By Page
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[at].[prc_Question_get_by_activityID]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [at].[prc_Question_get_by_activityID] AS' 
END
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
/*  2019-02-21 Ray:     Sort by Pages then Questions
*/
ALTER PROCEDURE [at].[prc_Question_get_by_activityID](
    @ActivityID int)
AS
BEGIN
    SET NOCOUNT ON;
    DECLARE @Err int;

    SELECT q.[QuestionID], q.[PageID], ISNULL(q.[ScaleID], 0) AS 'ScaleID', q.[No], q.[Type], q.[Inverted], q.[Mandatory], q.[Status], q.[CssClass], q.[ExtId],
           q.[Tag], q.[Created], ISNULL(q.[SectionID], 0) AS 'SectionID', q.[Width]
    FROM [at].[Question] q
    JOIN [at].[Page] p ON p.[PageID] = q.[PageID] AND p.[ActivityID] = @ActivityID
    ORDER BY p.[No], q.[No];

    SET @Err = @@Error;

    RETURN @Err;
END;
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
--#endregion 123. Sort Questions By Page
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
--#region 124. Load MemberRole From ATAdmin
IF NOT EXISTS (SELECT 1 FROM sys.objects WHERE name = N'prc_MemberRole_get' AND type IN ( N'P', N'PC' )  AND SCHEMA_NAME(schema_id) = 'org')
	EXEC ('CREATE PROC [org].[prc_MemberRole_get] AS ')
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
/*  
    2018-07-19 Sarah    Create procedures for Insert, Update, Delete, Get of org.MemberRole
    2019-02-21 Ray      Get by OwnerID
*/
ALTER PROCEDURE [org].[prc_MemberRole_get]
(
    @OwnerID int
)
AS
BEGIN
    SET NOCOUNT ON
    DECLARE @Err Int

    SELECT [MemberRoleID], [OwnerID], [ExtID], [No], [Created], [EntityStatusID], [EntityStatusReasonID], [MasterID]
    FROM [org].[MemberRole]
    WHERE [OwnerID] = @OwnerID;

    SET @Err = @@Error

    RETURN @Err
END
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
IF NOT EXISTS (SELECT 1 FROM sys.objects WHERE name = N'prc_LT_MemberRole_get' AND type IN ( N'P', N'PC' )  AND SCHEMA_NAME(schema_id) = 'org')
	EXEC ('CREATE PROC [org].[prc_LT_MemberRole_get] AS ')
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
/*  
    2018-07-19 Sarah    Create procedures for Insert, Update, Delete, Get of org.MemberRole
    2019-02-21 Ray      Get By @OwnerID
*/
ALTER PROCEDURE [org].[prc_LT_MemberRole_get]
(
    @OwnerID int
)
AS
BEGIN
    SET NOCOUNT ON
    DECLARE @Err Int

    SELECT lt.[MemberRoleID], lt.[LanguageID], lt.[Name], lt.[Description]
    FROM [org].[MemberRole] mr
    JOIN [org].[LT_MemberRole] lt ON lt.[MemberRoleID] = mr.[MemberRoleID] AND mr.[OwnerID] = @OwnerID;

    SET @Err = @@Error

    RETURN @Err
END
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
IF NOT EXISTS (SELECT 1 FROM sys.objects WHERE name = N'prc_UGMember_ins' AND type IN ( N'P', N'PC' )  AND SCHEMA_NAME(schema_id) = 'org')
    EXEC ('CREATE PROC org.prc_UGMember_ins AS ')
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
/*
    2018-07-03 Sarah    Created for ATAdmin
    2019-02-22 Ray      Default values for optional parameters
*/
ALTER PROCEDURE [org].[prc_UGMember_ins](
    @UGMemberID           bigint        = NULL OUTPUT,
    @UserGroupID          int,
    @UserID               int           = NULL,
    @CreatedBy            int           = NULL,
    @MemberRoleID         int           = NULL,
    @ValidFrom            datetime2(7)  = NULL,
    @ValidTo              datetime2(7)  = NULL,
    @LastUpdatedBy        int           = NULL,
    @EntityStatusID       int           = NULL,
    @EntityStatusReasonID int           = NULL,
    @CustomerID           int           = NULL,
    @PeriodID             int           = NULL,
    @ExtID                nvarchar(256) = '',
    @Deleted              datetime2(7)  = NULL,
    @DisplayName          nvarchar(max) = '',
    @ReferrerResource     nvarchar(512) = '',
    @ReferrerToken        nvarchar(512) = '',
    @ReferrerArchetypeID  int           = NULL,
    @cUserid              int,
    @Log                  smallint      = 1
) AS
BEGIN
    SET NOCOUNT ON;
    DECLARE @Err int;

    INSERT INTO [org].[UGMember] ([UserGroupID], [UserID], [Created], [CreatedBy], [MemberRoleID], [ValidFrom], [ValidTo], [LastUpdated], [LastUpdatedBy],
                                  [EntityStatusID], [EntityStatusReasonID], [CustomerID], [PeriodID], [ExtID], [Deleted], [DisplayName],
                                  [ReferrerResource], [ReferrerToken], [ReferrerArchetypeID])
    VALUES (@UserGroupID, @UserID, GETDATE(), @CreatedBy, @MemberRoleID, @ValidFrom, @ValidTo, GETDATE(), @LastUpdatedBy,
            @EntityStatusID, @EntityStatusReasonID, @CustomerID, @PeriodID, @ExtID, @Deleted, @DisplayName,
            @ReferrerResource, @ReferrerToken, @ReferrerArchetypeID);

    SET @Err = @@Error;
    SET @UGMemberID = SCOPE_IDENTITY();

    IF @Log = 1
    BEGIN
        INSERT INTO [Log].[AuditLog] ([UserId], [TableName], [Type], [Data], [Created])
        SELECT @cUserid, 'UGMember', 0, (SELECT * FROM [org].[UGMember] WHERE [UGMemberID] = @UGMemberID FOR XML AUTO) AS [data], GETDATE();
    END;

    RETURN @Err;
END;
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
IF NOT EXISTS (SELECT 1 FROM sys.objects WHERE name = N'prc_UGMember_upd' AND type IN ( N'P', N'PC' )  AND SCHEMA_NAME(schema_id) = 'org')
    EXEC ('CREATE PROC org.prc_UGMember_upd AS ')
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
/*
    2018-07-03 Sarah    Created for ATAdmin
    2019-02-22 Ray      Default values for optional parameters
*/
ALTER PROCEDURE [org].[prc_UGMember_upd](
    @UGMemberID           bigint,
    @UserGroupID          int,
    @UserID               int           = NULL,
    @CreatedBy            int           = NULL,
    @MemberRoleID         int           = NULL,
    @ValidFrom            datetime2(7)  = NULL,
    @ValidTo              datetime2(7)  = NULL,
    @LastUpdatedBy        int           = NULL,
    @EntityStatusID       int           = NULL,
    @EntityStatusReasonID int           = NULL,
    @CustomerID           int           = NULL,
    @PeriodID             int           = NULL,
    @ExtID                nvarchar(256) = '',
    @Deleted              datetime2(7)  = NULL,
    @DisplayName          nvarchar(max) = '',
    @ReferrerResource     nvarchar(512) = '',
    @ReferrerToken        nvarchar(512) = '',
    @ReferrerArchetypeID  int           = NULL,
    @cUserid              int,
    @Log                  smallint      = 1
) AS
BEGIN
    SET NOCOUNT ON;
    DECLARE @Err int;

    UPDATE [org].[UGMember]
    SET [UserGroupID] = @UserGroupID, [UserID] = @UserID, [CreatedBy] = @CreatedBy, [MemberRoleID] = @MemberRoleID,
        [ValidFrom] = @ValidFrom, [ValidTo] = @ValidTo, [LastUpdated] = GETDATE(), [LastUpdatedBy] = @LastUpdatedBy,
        [EntityStatusID] = @EntityStatusID, [EntityStatusReasonID] = @EntityStatusReasonID, [CustomerID] = @CustomerID, [PeriodID] = @PeriodID, [ExtID] = @ExtID,
        [Deleted] = @Deleted, [DisplayName] = @DisplayName, [ReferrerResource] = @ReferrerResource, [ReferrerToken] = @ReferrerToken, [ReferrerArchetypeID] = @ReferrerArchetypeID
    WHERE [UGMemberID] = @UGMemberID;

    SET @Err = @@Error;

    IF @Log = 1
    BEGIN
        INSERT INTO [Log].[AuditLog] ([UserId], [TableName], [Type], [Data], [Created])
        SELECT @cUserid, 'UGMember', 1, (SELECT * FROM [org].[UGMember] WHERE [UGMemberID] = @UGMemberID FOR XML AUTO) AS [data], GETDATE();
    END;

    RETURN @Err;
END;
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
IF EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[org].[prc_UG_U_get_ByUser]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'DROP PROCEDURE [org].[prc_UG_U_get_ByUser]' 
END
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[org].[prc_UGMember_get_ByUser]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [org].[prc_UGMember_get_ByUser] AS' 
END
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
/*
    Steve - Sept 22 2016 - EntityStatus for table UserGroup
    2018-07-09 Ray:     Replace UG_U by UGMember
    2019-02-25 Ray:     Rename to prc_UGMember_get_ByUser
*/
ALTER PROCEDURE [org].[prc_UGMember_get_ByUser](
    @UserID          int,
    @UserGroupTypeID varchar(max) = ''
) AS
BEGIN
    SET NOCOUNT ON;
    DECLARE @Err int;
    DECLARE @ActiveEntityStatusID int= (SELECT [EntityStatusID] FROM [EntityStatus] WHERE [CodeName] = 'Active');

    SELECT [ugm].[UserGroupID], [ug].[Name], [ug].[UserID] [GroupOwnerID]
    FROM [org].[UGMember] [ugm]
    JOIN [org].[UserGroup] [ug] ON [ugm].[UserGroupID] = [ug].[UserGroupID] AND [ugm].[UserID] = @UserID
     AND [ug].[EntityStatusID] = @ActiveEntityStatusID AND [ug].[Deleted] IS NULL
     AND ugm.[EntityStatusID] = @ActiveEntityStatusID AND ugm.[Deleted] IS NULL
     AND (ISNULL(@UserGroupTypeID, '') = '' OR [ug].[UserGroupTypeID] IN (SELECT * FROM [dbo].[funcListToTableInt] (@UserGroupTypeID, ',')
                                                                         )
         );

    SET @Err = @@Error;

    RETURN @Err;
END;
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
--#endregion 124. Load MemberRole From ATAdmin
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
--#region 125. Restrict Archetype To TableType

--#region Config data
IF NOT EXISTS (SELECT 1 FROM [dbo].[Archetype] a WHERE a.[MasterID] = 'A0E05766-7BD4-4FCA-A709-A66AEE8F291A')
BEGIN
    INSERT INTO [dbo].[Archetype] ([ArchetypeID], [CodeName], [TableTypeID], [MasterID])
    SELECT 41 AS [ArchetypeID], 'ArchivedClass' AS [CodeName], 16 AS [TableTypeID], 'A0E05766-7BD4-4FCA-A709-A66AEE8F291A' AS [MasterID];
END
UPDATE [org].[UserGroup] SET [ArchetypeID] = 41 WHERE [ArchetypeID] IS NULL OR [ArchetypeID] = 3;

UPDATE [dbo].[TableType] SET [Schema] = 'org' WHERE [Name] IN ('UserGroup','UserType')
--#endregion Config data

--#region Function
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[Check_Archetype_TableType]') AND type in (N'FN', N'IF', N'TF', N'FS', N'FT'))
BEGIN
execute dbo.sp_executesql @statement = N'CREATE FUNCTION [dbo].[Check_Archetype_TableType] (@Temp int) RETURNS int AS BEGIN RETURN 1 END';
END
GO
/*  2019-02-27 Ray:     Restrict archetype to tabletype
*/
ALTER FUNCTION [dbo].[Check_Archetype_TableType] (
    @SourceSchema       nvarchar(256),
    @SourceTable        nvarchar(256),
    @ArchetypeID        int
) RETURNS int
AS
BEGIN
    DECLARE @Matched int = 0;

    IF EXISTS (SELECT 1
               FROM [dbo].[Archetype] a
               JOIN [dbo].[TableType] t ON (t.[TableTypeID] = a.[TableTypeID] OR a.[TableTypeID] IS NULL)
                AND a.[ArchetypeID] = @ArchetypeID AND t.[Schema] = @SourceSchema AND t.[Name] = @SourceTable)
    OR @ArchetypeID IS NULL
    BEGIN
        SET @Matched = 1;
    END;

    RETURN @Matched
END
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
--#endregion Function

--#endregion 125. Restrict Archetype To TableType
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
-------------------------------------------------------------------------------------------------------------------
PRINT 'Setting database version'
/*
Start of versioning database-level extended properties.
*/
BEGIN
    DECLARE @DB_VERSION varchar(32), @ConceptName nvarchar(128), @ReleaseScriptName nvarchar(4000)
    SET @DB_VERSION = '6.12.007.000.00'
    SET @ConceptName = 'Engage Main'
    SET @ReleaseScriptName = N'v' + @DB_VERSION + N'-01. v6.12 Update Since 2019-01-23'

    IF EXISTS (SELECT 1 FROM fn_listextendedproperty(N'cxStudio Database Version', NULL, NULL, NULL, NULL, NULL, NULL))
        EXEC sp_dropextendedproperty N'cxStudio Database Version', NULL, NULL, NULL, NULL, NULL, NULL
    EXEC sp_addextendedproperty N'cxStudio Database Version', @DB_VERSION, NULL, NULL, NULL, NULL, NULL, NULL
		
    IF EXISTS (SELECT 1 FROM fn_listextendedproperty(N'Script Name', NULL, NULL, NULL, NULL, NULL, NULL))
        EXEC sp_dropextendedproperty N'Script Name', NULL, NULL, NULL, NULL, NULL, NULL
    EXEC sp_addextendedproperty N'Script Name',  @ReleaseScriptName , NULL, NULL, NULL, NULL, NULL, NULL
END
/*
End of versioning database-level extended properties.
*/
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
IF EXISTS (SELECT * FROM #tmpErrors) ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT>0 BEGIN
PRINT 'The database update succeeded'
COMMIT TRANSACTION
END
ELSE PRINT 'The database update failed'
GO
DROP TABLE #tmpErrors
GO
SET NOEXEC OFF
GO