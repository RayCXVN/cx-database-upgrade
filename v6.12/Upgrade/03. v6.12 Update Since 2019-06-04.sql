SET QUOTED_IDENTIFIER ON
GO
DECLARE @Current_DB_Version varchar(32), @CurrentConceptName nvarchar(128),
        @Required_DB_Version varchar(32), @RequiredConceptName nvarchar(128)

SET @Required_DB_Version = '6.12.009.%'
SET @RequiredConceptName = 'Common Main'
PRINT 'This script is generated for executing on ' + @RequiredConceptName + ' version ' + @Required_DB_Version

SELECT @Current_DB_Version = convert(nvarchar(max),value) FROM fn_listextendedproperty(N'cxStudio Database Version', NULL, NULL, NULL, NULL, NULL, NULL)
SELECT @CurrentConceptName = convert(nvarchar(max),value) FROM fn_listextendedproperty(N'Concept Name', NULL, NULL, NULL, NULL, NULL, NULL)

IF @CurrentConceptName != @RequiredConceptName OR @Current_DB_Version NOT LIKE @Required_DB_Version
BEGIN
    PRINT 'Incorrect solution or cxStudio Database version'
    SET NOEXEC ON
END
GO
-------------------------------------------------------------------------------------------------------------------------
IF EXISTS (SELECT * FROM tempdb..sysobjects WHERE id=OBJECT_ID('tempdb..#tmpErrors')) DROP TABLE #tmpErrors
GO
CREATE TABLE #tmpErrors (Error int)
GO
SET XACT_ABORT ON
GO
SET TRANSACTION ISOLATION LEVEL SERIALIZABLE
GO
BEGIN TRANSACTION
GO
----------------------------------------------------------------------------------------------------------------
IF NOT EXISTS (SELECT * FROM EntityStatus WHERE MasterID = '3EFF3982-D9CE-49AB-9FD5-BE4DE0B61365')
BEGIN
    INSERT INTO EntityStatus(EntityStatusID, CodeName, MasterID)
    SELECT 5 EntityStatusID, 'PendingApproval1st' CodeName, '3EFF3982-D9CE-49AB-9FD5-BE4DE0B61365' MasterID    
END

IF NOT EXISTS (SELECT * FROM EntityStatus WHERE MasterID = 'B1B5172E-85A3-478C-BE52-60687D29855F')
BEGIN
    INSERT INTO EntityStatus(EntityStatusID, CodeName, MasterID)
    SELECT 6 EntityStatusID, 'PendingApproval2nd' CodeName, 'B1B5172E-85A3-478C-BE52-60687D29855F' MasterID    
END
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
-------------------------------------------------------------------------------------------------------------------
PRINT 'Setting database version'
/*
Start of versioning database-level extended properties.
*/
BEGIN
    DECLARE @DB_VERSION varchar(32), @ConceptName nvarchar(128), @ReleaseScriptName nvarchar(4000)
    SET @DB_VERSION = '6.12.010.000.00'
    SET @ConceptName = 'Engage Main'
    SET @ReleaseScriptName = N'v' + @DB_VERSION + N'-03. v6.12 Update Since 2019-06-04'

    IF EXISTS (SELECT 1 FROM fn_listextendedproperty(N'cxStudio Database Version', NULL, NULL, NULL, NULL, NULL, NULL))
        EXEC sp_dropextendedproperty N'cxStudio Database Version', NULL, NULL, NULL, NULL, NULL, NULL
    EXEC sp_addextendedproperty N'cxStudio Database Version', @DB_VERSION, NULL, NULL, NULL, NULL, NULL, NULL
		
    IF EXISTS (SELECT 1 FROM fn_listextendedproperty(N'Script Name', NULL, NULL, NULL, NULL, NULL, NULL))
        EXEC sp_dropextendedproperty N'Script Name', NULL, NULL, NULL, NULL, NULL, NULL
    EXEC sp_addextendedproperty N'Script Name',  @ReleaseScriptName , NULL, NULL, NULL, NULL, NULL, NULL
END
/*
End of versioning database-level extended properties.
*/
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
IF EXISTS (SELECT * FROM #tmpErrors) ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT>0 BEGIN
PRINT 'The database update succeeded'
COMMIT TRANSACTION
END
ELSE PRINT 'The database update failed'
GO
DROP TABLE #tmpErrors
GO
SET NOEXEC OFF
GO