/*  Created By: Ray
    This script copies entire table data from a remote database to current database.
    To be run on QDB of destination database.
*/
SET NOCOUNT ON
GO
DECLARE @CleanupBeforeCopy bit = 1, @CopyFromDatabase nvarchar(256) = 'systemtest-engage-no-at6q',
        @TableName nvarchar(max) = '', @ObjectID int,
        @sqlCommand nvarchar(max) = '', @ColumnList nvarchar(max) = '';
DECLARE @TableList TABLE ([OrderNo] int IDENTITY(1,1), [TwoPartTableName] nvarchar(max), [ObjectID] int);

INSERT INTO @TableList ([TwoPartTableName]) VALUES ('[app].[Theme]');
INSERT INTO @TableList ([TwoPartTableName]) VALUES ('[at].[XCategoryType]');
INSERT INTO @TableList ([TwoPartTableName]) VALUES ('[at].[LT_XCategoryType]');
INSERT INTO @TableList ([TwoPartTableName]) VALUES ('[dbo].[RelationType]');
INSERT INTO @TableList ([TwoPartTableName]) VALUES ('[dbo].[LT_RelationType]');
INSERT INTO @TableList ([TwoPartTableName]) VALUES ('[sec].[AccessControlList]');
INSERT INTO @TableList ([TwoPartTableName]) VALUES ('[sec].[AccessRule]');
INSERT INTO @TableList ([TwoPartTableName]) VALUES ('[sec].[GroupAccessRule]');
INSERT INTO @TableList ([TwoPartTableName]) VALUES ('[dbo].[GUIText]');
INSERT INTO @TableList ([TwoPartTableName]) VALUES ('[dbo].[IP_Country]');
INSERT INTO @TableList ([TwoPartTableName]) VALUES ('[sec].[RuleGroup]');
INSERT INTO @TableList ([TwoPartTableName]) VALUES ('[log].[EventKey]');
INSERT INTO @TableList ([TwoPartTableName]) VALUES ('[log].[EventType]');
INSERT INTO @TableList ([TwoPartTableName]) VALUES ('[log].[LT_EventKey]');
INSERT INTO @TableList ([TwoPartTableName]) VALUES ('[log].[LT_EventType]');
INSERT INTO @TableList ([TwoPartTableName]) VALUES ('[mea].[Category]');
INSERT INTO @TableList ([TwoPartTableName]) VALUES ('[mea].[LT_Category]');
INSERT INTO @TableList ([TwoPartTableName]) VALUES ('[mea].[MeasureType]');
INSERT INTO @TableList ([TwoPartTableName]) VALUES ('[mea].[MeasureType_TableType]');
INSERT INTO @TableList ([TwoPartTableName]) VALUES ('[mea].[Status]');
INSERT INTO @TableList ([TwoPartTableName]) VALUES ('[mea].[LT_Status]');
INSERT INTO @TableList ([TwoPartTableName]) VALUES ('[org].[UserGroupType]');
INSERT INTO @TableList ([TwoPartTableName]) VALUES ('[org].[LT_UserGroupType]');
INSERT INTO @TableList ([TwoPartTableName]) VALUES ('[org].[Hierarchy]');
INSERT INTO @TableList ([TwoPartTableName]) VALUES ('[proc].[ProcessType]');
INSERT INTO @TableList ([TwoPartTableName]) VALUES ('[proc].[LT_ProcessType]');
INSERT INTO @TableList ([TwoPartTableName]) VALUES ('[proc].[ProcessGroup]');
INSERT INTO @TableList ([TwoPartTableName]) VALUES ('[proc].[LT_ProcessGroup]');
INSERT INTO @TableList ([TwoPartTableName]) VALUES ('[rep].[Bubble_Category]');
INSERT INTO @TableList ([TwoPartTableName]) VALUES ('[rep].[DF_O]');
INSERT INTO @TableList ([TwoPartTableName]) VALUES ('[res].[Category]');
INSERT INTO @TableList ([TwoPartTableName]) VALUES ('[res].[LT_Category]');
INSERT INTO @TableList ([TwoPartTableName]) VALUES ('[res].[MediaType]');
INSERT INTO @TableList ([TwoPartTableName]) VALUES ('[res].[LT_MediaType]');
INSERT INTO @TableList ([TwoPartTableName]) VALUES ('[res].[Status]');
INSERT INTO @TableList ([TwoPartTableName]) VALUES ('[res].[LT_Status]');
INSERT INTO @TableList ([TwoPartTableName]) VALUES ('[note].[NoteType]');
------------------------------------------------------------------------------------------------------------------------------------
UPDATE @TableList SET [ObjectID] = OBJECT_ID([TwoPartTableName]);

IF EXISTS (SELECT 1 FROM @TableList WHERE [ObjectID] IS NULL)
BEGIN
    SELECT 'Error: invalid table name. Script stop' AS [ErrorInfo], tl.* FROM @TableList tl WHERE tl.[ObjectID] IS NULL;
    SET NOEXEC ON;
END
IF LEFT(@CopyFromDatabase,1) <> '[' SET @CopyFromDatabase = '[' + @CopyFromDatabase;
IF RIGHT(@CopyFromDatabase,1) <> ']' SET @CopyFromDatabase = @CopyFromDatabase + ']';

-- Disable all constraints for database
RAISERROR ('Disable all constraints', 0, 1) WITH NOWAIT;
EXEC sp_msforeachtable 'ALTER TABLE ? NOCHECK CONSTRAINT all'
------------------------------------------------------------------------------------------------------------------------------------
DECLARE c_Table CURSOR LOCAL FAST_FORWARD READ_ONLY FOR
SELECT [TwoPartTableName], [ObjectID] FROM @TableList;

OPEN c_Table;
FETCH NEXT FROM c_Table INTO @TableName, @ObjectID;
WHILE @@FETCH_STATUS = 0
BEGIN
    IF @CleanupBeforeCopy = 1
    BEGIN
        SET @sqlCommand = 'DELETE FROM ' + @TableName;
        RAISERROR (@sqlCommand, 0, 1) WITH NOWAIT;
        EXEC sys.sp_executesql @sqlCommand;
    END
    
    SET @ColumnList = STUFF((SELECT ',' + QUOTENAME(c.[name]) AS [text()]
                             FROM sys.[columns] c
                             WHERE c.[object_id] = @ObjectID
                             ORDER BY c.[column_id]
                             FOR XML PATH, TYPE).value('.[1]','nvarchar(max)')
                            , 1, 1, '');
    SET @sqlCommand = 'INSERT INTO ' + @TableName + ' (' + @ColumnList + ')
    SELECT ' + @ColumnList + '
    FROM ' + @CopyFromDatabase + '.' + @TableName + ';';
    
    IF EXISTS (SELECT 1 FROM sys.[columns] c WHERE c.[object_id] = @ObjectID AND [c].[is_identity] = 1)
    BEGIN
        SET @sqlCommand = 'SET IDENTITY_INSERT ' + @TableName + ' ON;
' + @sqlCommand + '
SET IDENTITY_INSERT ' + @TableName + ' OFF;'
    END
    RAISERROR (@sqlCommand, 0, 1) WITH NOWAIT;
    EXEC sys.sp_executesql @sqlCommand;

    FETCH NEXT FROM c_Table INTO @TableName, @ObjectID;
END
CLOSE c_Table;
DEALLOCATE c_Table;
------------------------------------------------------------------------------------------------------------------------------------
-- Enable all constraints for database
RAISERROR ('Enable all constraints', 0, 1) WITH NOWAIT;
EXEC sp_msforeachtable 'IF OBJECT_ID(''?'') NOT IN (ISNULL(OBJECT_ID(''[prop].[PropFileOld]''),0)
                                                   )
                        ALTER TABLE ? WITH CHECK CHECK CONSTRAINT all'

SET NOEXEC OFF;
