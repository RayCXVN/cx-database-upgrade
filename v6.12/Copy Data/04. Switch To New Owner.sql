SET NOCOUNT ON
GO
SET XACT_ABORT ON
GO
DECLARE @NewOwnerID int = 0,
        @SchemaName nvarchar(32) = '', @TableName nvarchar(256) = '', @sqlCommand nvarchar(max) = '';

IF @NewOwnerID <= 0
BEGIN
    RAISERROR ('Please give a value to @NewOwnerID', 0, 1) WITH NOWAIT;
    SET NOEXEC ON;
END

BEGIN TRANSACTION

SET IDENTITY_INSERT [org].[Owner] ON;
INSERT INTO [org].[Owner] ([OwnerID], [LanguageID], [Name], [ReportServer], [ReportDB], [MainHierarchyID], [OLAPServer], [OLAPDB], [Css], [Url], [LoginType],
                           [Prefix], [Description], [Logging], [Created], [OTPLength], [OTPCharacters], [OTPAllowLowercase], [OTPAllowUppercase],
                           [UseOTPCaseSensitive], [UseHashPassword], [UseOTP], [DefaultHashMethod], [OTPDuration], [MasterID])
SELECT TOP 1 @NewOwnerID AS [OwnerID], [LanguageID], [Name], [ReportServer], [ReportDB], [MainHierarchyID], [OLAPServer], [OLAPDB], [Css], [Url], [LoginType],
       [Prefix], [Description], [Logging], GETDATE() AS [Created], [OTPLength], [OTPCharacters], [OTPAllowLowercase], [OTPAllowUppercase],
       [UseOTPCaseSensitive], [UseHashPassword], [UseOTP], [DefaultHashMethod], [OTPDuration], NEWID() AS [MasterID]
FROM [org].[Owner] o;
SET IDENTITY_INSERT [org].[Owner] OFF;

DECLARE c_Table CURSOR LOCAL FAST_FORWARD READ_ONLY FOR
SELECT s.[name] AS [SchemaName], t.[name] AS [TableName]
FROM sys.[tables] t
JOIN sys.[columns] c ON c.[object_id] = t.[object_id] AND c.[name] = 'OwnerID' AND t.[name] NOT IN ('Owner', 'PropFileOld')
JOIN sys.[schemas] s ON s.[schema_id] = t.[schema_id];

OPEN c_Table
FETCH NEXT FROM c_Table INTO @SchemaName, @TableName
WHILE @@FETCH_STATUS = 0
BEGIN
    SET @sqlCommand = 'UPDATE ' + QUOTENAME(@SchemaName) + '.' + QUOTENAME(@TableName) + ' SET [OwnerID] = ' + CAST(@NewOwnerID AS nvarchar(32));
    EXEC sys.sp_executesql @sqlCommand;

    FETCH NEXT FROM c_Table INTO @SchemaName, @TableName
END
CLOSE c_Table
DEALLOCATE c_Table

DELETE FROM [org].[Owner] WHERE [OwnerID] <> @NewOwnerID;

COMMIT TRANSACTION
SET NOEXEC OFF;