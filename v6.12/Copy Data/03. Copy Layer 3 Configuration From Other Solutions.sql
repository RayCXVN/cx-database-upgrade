/*  Created By: Ray
    This script copies entire table data from a remote database to current database.
    To be run on QDB of destination database.
*/
SET NOCOUNT ON
GO
DECLARE @CleanupBeforeCopy bit = 1, @CopyFromDatabase nvarchar(256) = 'systemtest-engage-no-at6q',
        @TableName nvarchar(max) = '', @ObjectID int,
        @sqlCommand nvarchar(max) = '', @ColumnList nvarchar(max) = '';
DECLARE @TableList TABLE ([OrderNo] int IDENTITY(1,1), [TwoPartTableName] nvarchar(max), [ObjectID] int);

INSERT INTO @TableList ([TwoPartTableName]) VALUES ('[app].[LoginService]');
INSERT INTO @TableList ([TwoPartTableName]) VALUES ('[app].[LT_LoginService]');
INSERT INTO @TableList ([TwoPartTableName]) VALUES ('[app].[SiteParameter]');
INSERT INTO @TableList ([TwoPartTableName]) VALUES ('[exp].[ExportColumn]');
INSERT INTO @TableList ([TwoPartTableName]) VALUES ('[exp].[LT_ExportColumn]');
INSERT INTO @TableList ([TwoPartTableName]) VALUES ('[rep].[DocumentTemplate]');
INSERT INTO @TableList ([TwoPartTableName]) VALUES ('[rep].[LT_DocumentTemplate]');
INSERT INTO @TableList ([TwoPartTableName]) VALUES ('[app].[Menu]');
INSERT INTO @TableList ([TwoPartTableName]) VALUES ('[app].[LT_Menu]');
INSERT INTO @TableList ([TwoPartTableName]) VALUES ('[app].[MenuItem]');
INSERT INTO @TableList ([TwoPartTableName]) VALUES ('[app].[LT_MenuItem]');
INSERT INTO @TableList ([TwoPartTableName]) VALUES ('[app].[PortalPage]');
INSERT INTO @TableList ([TwoPartTableName]) VALUES ('[app].[LT_PortalPage]');
INSERT INTO @TableList ([TwoPartTableName]) VALUES ('[app].[PortalPart]');
INSERT INTO @TableList ([TwoPartTableName]) VALUES ('[app].[LT_PortalPart]');
INSERT INTO @TableList ([TwoPartTableName]) VALUES ('[dbo].[AccessGeneric]');
INSERT INTO @TableList ([TwoPartTableName]) VALUES ('[dbo].[AccessGeneric]');
INSERT INTO @TableList ([TwoPartTableName]) VALUES ('[dbo].[AccessGroup]');
INSERT INTO @TableList ([TwoPartTableName]) VALUES ('[dbo].[AG_AG]');
INSERT INTO @TableList ([TwoPartTableName]) VALUES ('[dbo].[LT_AccessGroup]');
INSERT INTO @TableList ([TwoPartTableName]) VALUES ('[org].[DepartmentType]');
INSERT INTO @TableList ([TwoPartTableName]) VALUES ('[org].[LT_DepartmentType]');
INSERT INTO @TableList ([TwoPartTableName]) VALUES ('[org].[UserType]');
INSERT INTO @TableList ([TwoPartTableName]) VALUES ('[org].[LT_UserType]');
INSERT INTO @TableList ([TwoPartTableName]) VALUES ('[prop].[Prop]');
INSERT INTO @TableList ([TwoPartTableName]) VALUES ('[prop].[PropOption]');
INSERT INTO @TableList ([TwoPartTableName]) VALUES ('[prop].[PropPage]');
INSERT INTO @TableList ([TwoPartTableName]) VALUES ('[prop].[LT_Prop]');
INSERT INTO @TableList ([TwoPartTableName]) VALUES ('[prop].[LT_PropOption]');
INSERT INTO @TableList ([TwoPartTableName]) VALUES ('[prop].[LT_PropPage]');
INSERT INTO @TableList ([TwoPartTableName]) VALUES ('[rep].[DocumentItem]');
INSERT INTO @TableList ([TwoPartTableName]) VALUES ('[app].[UT_AccessArea]');
INSERT INTO @TableList ([TwoPartTableName]) VALUES ('[at].[StatusType]');
INSERT INTO @TableList ([TwoPartTableName]) VALUES ('[at].[LT_StatusType]');
INSERT INTO @TableList ([TwoPartTableName]) VALUES ('[form].[Form]');
INSERT INTO @TableList ([TwoPartTableName]) VALUES ('[form].[FormCell]');
INSERT INTO @TableList ([TwoPartTableName]) VALUES ('[form].[FormCommand]');
INSERT INTO @TableList ([TwoPartTableName]) VALUES ('[form].[FormCommandCondition]');
INSERT INTO @TableList ([TwoPartTableName]) VALUES ('[form].[FormField]');
INSERT INTO @TableList ([TwoPartTableName]) VALUES ('[form].[FormFieldCondition]');
INSERT INTO @TableList ([TwoPartTableName]) VALUES ('[form].[FormRow]');
INSERT INTO @TableList ([TwoPartTableName]) VALUES ('[form].[LT_Form]');
INSERT INTO @TableList ([TwoPartTableName]) VALUES ('[form].[LT_FormCell]');
INSERT INTO @TableList ([TwoPartTableName]) VALUES ('[form].[LT_FormCommand]');
INSERT INTO @TableList ([TwoPartTableName]) VALUES ('[form].[LT_FormField]');
INSERT INTO @TableList ([TwoPartTableName]) VALUES ('[list].[ItemList]');
INSERT INTO @TableList ([TwoPartTableName]) VALUES ('[list].[ItemListCommand]');
INSERT INTO @TableList ([TwoPartTableName]) VALUES ('[list].[ItemListCommandCondition]');
INSERT INTO @TableList ([TwoPartTableName]) VALUES ('[list].[ItemListDataParameter]');
INSERT INTO @TableList ([TwoPartTableName]) VALUES ('[list].[ItemListDataSource]');
INSERT INTO @TableList ([TwoPartTableName]) VALUES ('[list].[ItemListField]');
INSERT INTO @TableList ([TwoPartTableName]) VALUES ('[list].[ItemListFieldAttribute]');
INSERT INTO @TableList ([TwoPartTableName]) VALUES ('[list].[ItemListFieldCondition]');
INSERT INTO @TableList ([TwoPartTableName]) VALUES ('[list].[ItemListFieldExpandingAttribute]');
INSERT INTO @TableList ([TwoPartTableName]) VALUES ('[list].[ItemListRowAttribute]');
INSERT INTO @TableList ([TwoPartTableName]) VALUES ('[list].[LT_ItemList]');
INSERT INTO @TableList ([TwoPartTableName]) VALUES ('[list].[LT_ItemListCommand]');
INSERT INTO @TableList ([TwoPartTableName]) VALUES ('[list].[LT_ItemListDataParameter]');
INSERT INTO @TableList ([TwoPartTableName]) VALUES ('[list].[LT_ItemListDataSource]');
INSERT INTO @TableList ([TwoPartTableName]) VALUES ('[list].[LT_ItemListField]');
------------------------------------------------------------------------------------------------------------------------------------
UPDATE @TableList SET [ObjectID] = OBJECT_ID([TwoPartTableName]);

IF EXISTS (SELECT 1 FROM @TableList WHERE [ObjectID] IS NULL)
BEGIN
    SELECT 'Error: invalid table name. Script stop' AS [ErrorInfo], tl.* FROM @TableList tl WHERE tl.[ObjectID] IS NULL;
    SET NOEXEC ON;
END
IF LEFT(@CopyFromDatabase,1) <> '[' SET @CopyFromDatabase = '[' + @CopyFromDatabase;
IF RIGHT(@CopyFromDatabase,1) <> ']' SET @CopyFromDatabase = @CopyFromDatabase + ']';

-- Disable all constraints for database
RAISERROR ('Disable all constraints', 0, 1) WITH NOWAIT;
EXEC sp_msforeachtable 'ALTER TABLE ? NOCHECK CONSTRAINT all'
------------------------------------------------------------------------------------------------------------------------------------
DECLARE c_Table CURSOR LOCAL FAST_FORWARD READ_ONLY FOR
SELECT [TwoPartTableName], [ObjectID] FROM @TableList;

OPEN c_Table;
FETCH NEXT FROM c_Table INTO @TableName, @ObjectID;
WHILE @@FETCH_STATUS = 0
BEGIN
    IF @CleanupBeforeCopy = 1
    BEGIN
        SET @sqlCommand = 'DELETE FROM ' + @TableName;
        RAISERROR (@sqlCommand, 0, 1) WITH NOWAIT;
        EXEC sys.sp_executesql @sqlCommand;
    END
    
    SET @ColumnList = STUFF((SELECT ',' + QUOTENAME(c.[name]) AS [text()]
                             FROM sys.[columns] c
                             WHERE c.[object_id] = @ObjectID
                             ORDER BY c.[column_id]
                             FOR XML PATH, TYPE).value('.[1]','nvarchar(max)')
                            , 1, 1, '');
    SET @sqlCommand = 'INSERT INTO ' + @TableName + ' (' + @ColumnList + ')
    SELECT ' + @ColumnList + '
    FROM ' + @CopyFromDatabase + '.' + @TableName + ';';
    
    IF EXISTS (SELECT 1 FROM sys.[columns] c WHERE c.[object_id] = @ObjectID AND [c].[is_identity] = 1)
    BEGIN
        SET @sqlCommand = 'SET IDENTITY_INSERT ' + @TableName + ' ON;
' + @sqlCommand + '
SET IDENTITY_INSERT ' + @TableName + ' OFF;'
    END
    RAISERROR (@sqlCommand, 0, 1) WITH NOWAIT;
    EXEC sys.sp_executesql @sqlCommand;

    FETCH NEXT FROM c_Table INTO @TableName, @ObjectID;
END
CLOSE c_Table;
DEALLOCATE c_Table;
------------------------------------------------------------------------------------------------------------------------------------
-- Copy with some criteria
-- ObjectMapping
IF @CleanupBeforeCopy = 1
BEGIN
    DELETE FROM [dbo].[ObjectMapping] WHERE [RelationTypeID] IN (1,2,3);
END

SET @sqlCommand = 'INSERT INTO [dbo].[ObjectMapping] ([OMID], [OwnerID], [FromTableTypeID], [FromID], [ToTableTypeID], [ToID], [RelationTypeID], [MasterID])
SELECT [OMID], [OwnerID], [FromTableTypeID], [FromID], [ToTableTypeID], [ToID], [RelationTypeID], [MasterID]
FROM ' + @CopyFromDatabase + '.[dbo].[ObjectMapping]
WHERE [RelationTypeID] IN (1,2,3);';
SET IDENTITY_INSERT [dbo].[ObjectMapping] ON;
EXEC sys.sp_executesql @sqlCommand;
SET IDENTITY_INSERT [dbo].[ObjectMapping] OFF;
------------------------------------------------------------------------------------------------------------------------------------
-- Enable all constraints for database
RAISERROR ('Enable all constraints', 0, 1) WITH NOWAIT;
EXEC sp_msforeachtable 'IF OBJECT_ID(''?'') NOT IN (ISNULL(OBJECT_ID(''[prop].[PropFileOld]''),0)
                                                   )
                        ALTER TABLE ? WITH CHECK CHECK CONSTRAINT all'

SET NOEXEC OFF;
