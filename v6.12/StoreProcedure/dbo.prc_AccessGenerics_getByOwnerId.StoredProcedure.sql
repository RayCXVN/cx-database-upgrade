SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[prc_AccessGenerics_getByOwnerId]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [dbo].[prc_AccessGenerics_getByOwnerId] AS' 
END
GO
ALTER PROCEDURE [dbo].[prc_AccessGenerics_getByOwnerId]
	@OwnerId int
AS
BEGIN 	
	SELECT A.*
	FROM  dbo.AccessGeneric A INNER JOIN dbo.AccessGroup AG ON AG.AccessGroupID = A.AccessGroupID 
	WHERE AG.OwnerID = @OwnerID
END

GO
