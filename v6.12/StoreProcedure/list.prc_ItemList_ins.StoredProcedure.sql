SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[list].[prc_ItemList_ins]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [list].[prc_ItemList_ins] AS' 
END
GO
/*
	2017-06-13 Steve: Change the length of column ExtID in all related tables from xAPI format
*/
ALTER PROCEDURE [list].[prc_ItemList_ins]
	@ItemListID int = null output,
	@OwnerID int,
    @ItemListDataSourceID int = null,
    @ListTableCSSClass nvarchar(128),
    @ListTableCSSCStyle nvarchar(64),
    @ListHeaderRowCSSClass nvarchar(64),
    @ListRowCSSClass nvarchar(64),
    @ListAltRowCSSClass nvarchar(64),
    @ExpandingItemListDatasourceID int = null,
    @ExpandingListRowCSSClass nvarchar(64),
    @ExpandingListAltRowCSSClass nvarchar(64),
    @ExpandingExpression nvarchar(64),
    @InsertMultiCheckColumn bit,
    @DisableCheckColumnExpression nvarchar(128),
    @DisplayUpperCmdBarThreshold int,
    @MaxRows int,
	@ExtID nvarchar(256) = '',
    @cUserid int,
    @Log smallint = 1
AS
BEGIN
	SET NOCOUNT ON;
	DECLARE @Err Int

	INSERT INTO [list].[ItemList]
           ([OwnerID]
           ,[ItemListDataSourceID]
           ,[ListTableCSSClass]
           ,[ListTableCSSCStyle]
           ,[ListHeaderRowCSSClass]
           ,[ListRowCSSClass]
           ,[ListAltRowCSSClass]
           ,[ExpandingItemListDatasourceID]
           ,[ExpandingListRowCSSClass]
           ,[ExpandingListAltRowCSSClass]
           ,[ExpandingExpression]
           ,[InsertMultiCheckColumn]
           ,[DisableCheckColumnExpression]
           ,[DisplayUpperCmdBarThreshold]
           ,[MaxRows]
		   ,[ExtID])
     VALUES
           (@OwnerID
           ,@ItemListDataSourceID
           ,@ListTableCSSClass
           ,@ListTableCSSCStyle
           ,@ListHeaderRowCSSClass
           ,@ListRowCSSClass
           ,@ListAltRowCSSClass
           ,@ExpandingItemListDatasourceID
           ,@ExpandingListRowCSSClass
           ,@ExpandingListAltRowCSSClass
           ,@ExpandingExpression
           ,@InsertMultiCheckColumn
           ,@DisableCheckColumnExpression
           ,@DisplayUpperCmdBarThreshold
           ,@MaxRows
		   ,@ExtID)

     Set @Err = @@Error
	 Set @ItemListID = scope_identity()
	 IF @Log = 1 
	 BEGIN 
		INSERT INTO [Log].[AuditLog] ( UserId, TableName, Type, Data, Created) 
		SELECT @cUserid,'ItemList',0,
		( SELECT * FROM [list].[ItemList] 
			WHERE
			[ItemListID] = @ItemListID				 FOR XML AUTO) as data,
				getdate() 
	 END


	 RETURN @Err
END

GO
