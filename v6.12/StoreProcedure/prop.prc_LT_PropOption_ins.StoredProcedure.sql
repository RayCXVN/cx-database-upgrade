SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[prop].[prc_LT_PropOption_ins]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [prop].[prc_LT_PropOption_ins] AS' 
END
GO

ALTER PROCEDURE [prop].[prc_LT_PropOption_ins]
(
	@LanguageID int,
	@PropOptionID int,
	@Name nvarchar(256),
	@Description nvarchar(max),
	@cUserid int,
	@Log smallint = 1
)
AS
BEGIN
	SET NOCOUNT ON
	DECLARE @Err Int

	INSERT INTO [prop].[LT_PropOption]
	(
		[LanguageID],
		[PropOptionID],
		[Name],
		[Description]
	)
	VALUES
	(
		@LanguageID,
		@PropOptionID,
		@Name,
		@Description
	)

	Set @Err = @@Error

	IF @Log = 1 
	BEGIN 
		INSERT INTO [Log].[AuditLog] ( UserId, TableName, Type, Data, Created) 
		SELECT @cUserid,'LT_PropOption',0,
		( SELECT * FROM [prop].[LT_PropOption] 
			WHERE
			[LanguageID] = @LanguageID AND
			[PropOptionID] = @PropOptionID				 FOR XML AUTO) as data,
				getdate() 
	 END

	RETURN @Err
END


GO
