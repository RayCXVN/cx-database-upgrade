SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[rep].[prc_ReportColumnParameter_upd]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [rep].[prc_ReportColumnParameter_upd] AS' 
END
GO
ALTER PROCEDURE [rep].[prc_ReportColumnParameter_upd]  
(  
 @ReportColumnParameterID int,  
 @ReportColumnID int,  
 @No smallint,  
 @Name varchar(32),  
 @ActivityID int,  
 @QuestionID INT=NULL,  
 @AlternativeID INT=NULL,  
 @CategoryID INT=NULL,  
 @CalcType smallint,  
 @cUserid int,  
 @Log smallint = 1,
 @ItemID int = NULL
)  
AS  
BEGIN  
 SET NOCOUNT ON  
 DECLARE @Err Int  
  
 UPDATE [rep].[ReportColumnParameter]  
 SET  
  [ReportColumnID] = @ReportColumnID,  
  [No] = @No,  
  [Name] = @Name,  
  [ActivityID] = @ActivityID,  
  [QuestionID] = @QuestionID,  
  [AlternativeID] = @AlternativeID,  
  [CategoryID] = @CategoryID,  
  [CalcType] = @CalcType,
  [ItemID] = @ItemID
 WHERE  
  [ReportColumnParameterID] = @ReportColumnParameterID  
  
 IF @Log = 1   
 BEGIN   
  INSERT INTO [Log].[AuditLog] ( UserId, TableName, Type, Data, Created)   
  SELECT @cUserid,'ReportColumnParameter',1,  
  ( SELECT * FROM [rep].[ReportColumnParameter]   
   WHERE  
   [ReportColumnParameterID] = @ReportColumnParameterID    FOR XML AUTO) as data,  
   getdate()  
 END  
  
 Set @Err = @@Error  
  
 RETURN @Err  
END  
    

GO
