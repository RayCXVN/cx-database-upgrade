SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[prc_AccessGeneric_get]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [dbo].[prc_AccessGeneric_get] AS' 
END
GO
/* 
	2017-08-21 Johnny: Add column SiteID for Accessgeneric
*/
ALTER PROCEDURE [dbo].[prc_AccessGeneric_get]
(
	@AccessGroupID int
)
AS
BEGIN
	SET NOCOUNT ON
	DECLARE @Err Int

	SELECT
	[AccessID],
	ISNULL([TableTypeID], 0) AS 'TableTypeID',
	ISNULL([Elementid], 0) AS 'Elementid',
	[Type],
	[AccessGroupID],
	[Created],
    [SiteID]
	FROM [dbo].[AccessGeneric]
	WHERE
	[AccessGroupID] = @AccessGroupID

	Set @Err = @@Error

	RETURN @Err
END

GO
