SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[proc].[prc_ProcessType_upd]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [proc].[prc_ProcessType_upd] AS' 
END
GO
ALTER PROCEDURE [proc].[prc_ProcessType_upd]
(
	@ProcessTypeID int,
	@No smallint,
	@OwnerID int,
	@CssClass nvarchar(128) = '',
	@DepartmentSelect bit = 1,
	@UserSelect bit = 0,
	@FilesAbove bit = 1,
	@cUserid int,
	@Log smallint = 1
)
AS
BEGIN
	SET NOCOUNT ON
	DECLARE @Err Int

	UPDATE [proc].[ProcessType]
	SET
		[No] = @No,
		[OwnerID] = @OwnerID,
		[CssClass] = @CssClass,
		[DepartmentSelect] = @DepartmentSelect,
		[UserSelect] = @UserSelect,
		[FilesAbove] = @FilesAbove
	WHERE
		[ProcessTypeID] = @ProcessTypeID

	IF @Log = 1 
	BEGIN 
		INSERT INTO [Log].[AuditLog] ( UserId, TableName, Type, Data, Created) 
		SELECT @cUserid,'ProcessType',1,
		( SELECT * FROM [proc].[ProcessType] 
			WHERE
			[ProcessTypeID] = @ProcessTypeID			 FOR XML AUTO) as data,
			getdate()
	END

	Set @Err = @@Error

	RETURN @Err
END

GO
