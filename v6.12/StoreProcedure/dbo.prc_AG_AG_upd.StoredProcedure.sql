SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[prc_AG_AG_upd]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [dbo].[prc_AG_AG_upd] AS' 
END
GO

ALTER PROCEDURE [dbo].[prc_AG_AG_upd]
(
	@AG_AGID int,
	@AccessGroupID int,
	@ToAccessGroupID int,
	@Type int,
	@cUserid int,
	@Log smallint = 1
)
AS
BEGIN
	SET NOCOUNT ON
	DECLARE @Err Int

	UPDATE [dbo].[AG_AG]
	SET
		[AccessGroupID] = @AccessGroupID,
		[ToAccessGroupID] = @ToAccessGroupID,
		[Type] = @Type
	WHERE
		[AG_AGID] = @AG_AGID

	IF @Log = 1 
	BEGIN 
		INSERT INTO [Log].[AuditLog] ( UserId, TableName, Type, Data, Created) 
		SELECT @cUserid,'AG_AG',1,
		( SELECT * FROM [dbo].[AG_AG] 
			WHERE
			[AG_AGID] = @AG_AGID			 FOR XML AUTO) as data,
			getdate()
	END

	Set @Err = @@Error

	RETURN @Err
END


GO
