SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[CreateAutoCalcJob]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [dbo].[CreateAutoCalcJob] AS' 
END
GO
ALTER Proc [dbo].[CreateAutoCalcJob]
(
  @OwnerID as integer,
  @Userid as integer,
  @Servername as nvarchar(64),
  @QDB as nvarchar(64),
  @ActivityID as integer,
  @SurveyID as integer,
  @Username as nvarchar(64),
  @Password as nvarchar(64),
  @Description as nvarchar(64) = 'AutoCalc',
  @BatchID as integer = -1
)
AS
BEGIN
	  Declare @JobID as int


	  insert into job.job (JobTypeID, JobStatusID, OwnerID, UserID, Name, Priority, [Option], Created, StartDate, EndDate, Description)
	   values (26,0,@OwnerID,@Userid,'Calculate',0,0,GETDATE(),getdate(),null,@Description )

	   select @JobID = scope_identity()

         insert into  job.JobParameter (JobID, No, Name, Value, Type)
		 values (@JobID,1,'Server',@ServerName,0)

		 insert into  job.JobParameter (JobID, No, Name, Value, Type)
		 values (@JobID,2,'Database',@QDB,0)

		 insert into  job.JobParameter (JobID, No, Name, Value, Type)
		 values (@JobID,3,'Username',@Username,0)

		 insert into  job.JobParameter (JobID, No, Name, Value, Type)
		 values (@JobID,4,'Password',@Password,0)

		 insert into  job.JobParameter (JobID, No, Name, Value, Type)
		 values (@JobID,5,'Ownerid',@OwnerID,0)

		  insert into  job.JobParameter (JobID, No, Name, Value, Type)
		 values (@JobID,6,'ActivityId',cast(@ActivityID as nvarchar(64)),0)

		  insert into  job.JobParameter (JobID, No, Name, Value, Type)
		  values (@JobID,7,'SurveyId',cast(@SurveyID as nvarchar(64)),0)

		  insert into  job.JobParameter (JobID, No, Name, Value, Type)
		  values (@JobID,8,'BatchId',@BatchID,0) 

		   insert into  job.JobParameter (JobID, No, Name, Value, Type)
		  values (@JobID,9,'ReprocessOlap','1',0) 


END


GO
