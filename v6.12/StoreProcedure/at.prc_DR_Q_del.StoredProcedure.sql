SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[at].[prc_DR_Q_del]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [at].[prc_DR_Q_del] AS' 
END
GO

ALTER PROCEDURE [at].[prc_DR_Q_del]
(
	@DottedRuleID int,
	@QuestionID int,
	@cUserid int,
	@Log smallint = 1
)
AS
BEGIN
	SET NOCOUNT ON
	DECLARE @Err Int

	IF @Log = 1 
	BEGIN 
		INSERT INTO [Log].[AuditLog] ( UserId, TableName, Type, Data, Created) 
		SELECT @cUserid,'DR_Q',2,
		( SELECT * FROM [at].[DR_Q] 
			WHERE
			[DottedRuleID] = @DottedRuleID AND
			[QuestionID] = @QuestionID
			 FOR XML AUTO) as data,
			getdate() 
	END 


	DELETE FROM [at].[DR_Q]
	WHERE
		[DottedRuleID] = @DottedRuleID AND
		[QuestionID] = @QuestionID

	Set @Err = @@Error

	RETURN @Err
END


GO
