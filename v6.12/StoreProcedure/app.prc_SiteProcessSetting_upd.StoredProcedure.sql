SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[app].[prc_SiteProcessSetting_upd]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [app].[prc_SiteProcessSetting_upd] AS' 
END
GO
ALTER PROCEDURE [app].[prc_SiteProcessSetting_upd]
	@SiteProcessSettingID int = null output,
	@SiteID int,
	@No int,
	@UseLocalProcessGroups bit,
	@AllCanAddAnswers bit,
	@UsePeriod bit,
	@CanChangeuser bit,
	@ShowOnlyAnswersBlowOnAdminPage bit,
	@CanAddProcessGroup bit,
	@CanCreatorEditAnswer bit,
	@ShowEditButtonOnProcessAnswer bit,
	@cUserid int,
    @Log smallint = 1
AS
BEGIN
	SET NOCOUNT ON;
	DECLARE @Err Int
    
    UPDATE [app].[SiteProcessSetting]
          SET 
           [No] = @No
           ,[UseLocalProcessGroups] = @UseLocalProcessGroups
           ,[AllCanAddAnswers] = @AllCanAddAnswers
           ,[UsePeriod] = @UsePeriod
           ,[CanChangeuser] = @CanChangeuser
           ,[ShowOnlyAnswersBlowOnAdminPage] = @ShowOnlyAnswersBlowOnAdminPage
           ,[CanAddProcessGroup] = @CanAddProcessGroup
           ,[CanCreatorEditAnswer] = @CanCreatorEditAnswer
           ,[ShowEditButtonOnProcessAnswer] = @ShowEditButtonOnProcessAnswer
	WHERE 
		 [SiteProcessSettingID] = @SiteProcessSettingID          
		 AND [SiteID] = @SiteID
    Set @Err = @@Error
    Set @SiteProcessSettingID = scope_identity()
	IF @Log = 1 
	BEGIN 
		INSERT INTO [Log].[AuditLog] ( UserId, TableName, Type, Data, Created) 
		SELECT @cUserid,'SiteProcessSetting',0,
		( SELECT * FROM [app].[SiteProcessSetting]
			WHERE
			[SiteProcessSettingID] = @SiteProcessSettingID				 FOR XML AUTO) as data,
				getdate() 
	END
	
	RETURN @Err       
END

GO
