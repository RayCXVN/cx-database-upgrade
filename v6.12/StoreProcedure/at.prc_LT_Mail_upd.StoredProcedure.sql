SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[at].[prc_LT_Mail_upd]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [at].[prc_LT_Mail_upd] AS' 
END
GO

ALTER PROCEDURE [at].[prc_LT_Mail_upd]
(
	@LanguageID int,
	@MailID int,
	@Name nvarchar(256),
	@Subject nvarchar(256),
	@Body nvarchar(max),
	@cUserid int,
	@Log smallint = 1
)
AS
BEGIN
	SET NOCOUNT ON
	DECLARE @Err Int

	UPDATE [at].[LT_Mail]
	SET
		[LanguageID] = @LanguageID,
		[MailID] = @MailID,
		[Name] = @Name,
		[Subject] = @Subject,
		[Body] = @Body
	WHERE
		[LanguageID] = @LanguageID AND
		[MailID] = @MailID

	IF @Log = 1 
	BEGIN 
		INSERT INTO [Log].[AuditLog] ( UserId, TableName, Type, Data, Created) 
		SELECT @cUserid,'LT_Mail',1,
		( SELECT * FROM [at].[LT_Mail] 
			WHERE
			[LanguageID] = @LanguageID AND
			[MailID] = @MailID			 FOR XML AUTO) as data,
			getdate()
	END

	Set @Err = @@Error

	RETURN @Err
END


GO
