SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[rep].[prc_Selection_Survey_upd]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [rep].[prc_Selection_Survey_upd] AS' 
END
GO

ALTER PROCEDURE [rep].[prc_Selection_Survey_upd]
(
	@S_SurveyID int,
	@SelectionID int,
	@SurveyID int,
	@cUserid int,
	@Log smallint = 1
)
AS
BEGIN
	SET NOCOUNT ON
	DECLARE @Err Int

	UPDATE [rep].[Selection_Survey]
	SET
		[SelectionID] = @SelectionID,
		[SurveyID] = @SurveyID
	WHERE
		[S_SurveyID] = @S_SurveyID

	IF @Log = 1 
	BEGIN 
		INSERT INTO [Log].[AuditLog] ( UserId, TableName, Type, Data, Created) 
		SELECT @cUserid,'Selection_Survey',1,
		( SELECT * FROM [rep].[Selection_Survey] 
			WHERE
			[S_SurveyID] = @S_SurveyID			 FOR XML AUTO) as data,
			getdate()
	END

	Set @Err = @@Error

	RETURN @Err
END


GO
