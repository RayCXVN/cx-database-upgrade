SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[rep].[prc_ReportColumnParameter_ins]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [rep].[prc_ReportColumnParameter_ins] AS' 
END
GO
  
ALTER PROCEDURE [rep].[prc_ReportColumnParameter_ins]  
(  
 @ReportColumnParameterID int = null output,  
 @ReportColumnID int,  
 @No smallint,  
 @Name varchar(32),  
 @ActivityID int,  
 @QuestionID INT=NULL,  
 @AlternativeID INT=NULL,  
 @CategoryID INT=NULL,  
 @CalcType smallint,  
 @cUserid int,  
 @Log smallint = 1,
 @ItemID int = NULL
)  
AS  
BEGIN  
 SET NOCOUNT ON  
 DECLARE @Err Int  
  
 INSERT INTO [rep].[ReportColumnParameter]  
 (  
  [ReportColumnID],  
  [No],  
  [Name],  
  [ActivityID],  
  [QuestionID],  
  [AlternativeID],  
  [CategoryID],  
  [CalcType],
  [ItemID]  
 )  
 VALUES  
 (  
  @ReportColumnID,  
  @No,  
  @Name,  
  @ActivityID,  
  @QuestionID,  
  @AlternativeID,  
  @CategoryID,  
  @CalcType,
  @ItemID  
 )  
  
 Set @Err = @@Error  
 Set @ReportColumnParameterID = scope_identity()  
  
 IF @Log = 1   
 BEGIN   
  INSERT INTO [Log].[AuditLog] ( UserId, TableName, Type, Data, Created)   
  SELECT @cUserid,'ReportColumnParameter',0,  
  ( SELECT * FROM [rep].[ReportColumnParameter]   
   WHERE  
   [ReportColumnParameterID] = @ReportColumnParameterID     FOR XML AUTO) as data,  
    getdate()   
  END  
  
 RETURN @Err  
END  
  

GO
