SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[org].[prc_Hierarchy_upd]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [org].[prc_Hierarchy_upd] AS' 
END
GO

ALTER PROCEDURE [org].[prc_Hierarchy_upd]
(
	@HierarchyID int,
	@OwnerID int,
	@Name varchar(256),
	@Description nvarchar(max),
	@Type smallint,
	@Deleted bit,
	@cUserid int,
	@Log smallint = 1
)
AS
BEGIN
	SET NOCOUNT ON
	DECLARE @Err Int

	UPDATE [org].[Hierarchy]
	SET
		[OwnerID] = @OwnerID,
		[Name] = @Name,
		[Description] = @Description,
		[Type] = @Type,
		[Deleted] = @Deleted
	WHERE
		[HierarchyID] = @HierarchyID

	IF @Log = 1 
	BEGIN 
		INSERT INTO [Log].[AuditLog] ( UserId, TableName, Type, Data, Created) 
		SELECT @cUserid,'Hierarchy',1,
		( SELECT * FROM [org].[Hierarchy] 
			WHERE
			[HierarchyID] = @HierarchyID			 FOR XML AUTO) as data,
			getdate()
	END

	Set @Err = @@Error

	RETURN @Err
END


GO
