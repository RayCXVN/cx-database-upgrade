SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[at].[prc_CalcField_get]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [at].[prc_CalcField_get] AS' 
END
GO
ALTER PROCEDURE  [at].[prc_CalcField_get]
(
	@ActivityID int
)
AS
BEGIN
	SET NOCOUNT ON
	DECLARE @Err Int

	SELECT
	[CFID],
	[ActivityID],
	[Name],
	[Formula],
	[Format],
	[Created],
	AllParametersMustExist,
	[Type]
	FROM [at].[CalcField]
	WHERE
	[ActivityID] = @ActivityID

	Set @Err = @@Error

	RETURN @Err
END

GO
