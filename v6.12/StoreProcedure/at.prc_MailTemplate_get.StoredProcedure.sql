SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[at].[prc_MailTemplate_get]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [at].[prc_MailTemplate_get] AS' 
END
GO
ALTER PROCEDURE [at].[prc_MailTemplate_get]
(
	@OwnerID int
)
AS
BEGIN
	SET NOCOUNT ON
	DECLARE @Err Int

	SELECT
	[MailTemplateID],
	ISNULL([OwnerID], 0) AS 'OwnerID'
	FROM [at].[MailTemplate]
	WHERE
	[OwnerID] = @OwnerID

	Set @Err = @@Error

	RETURN @Err
END

GO
