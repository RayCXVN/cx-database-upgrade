SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[at].[prc_ActivityView_Q_get]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [at].[prc_ActivityView_Q_get] AS' 
END
GO

ALTER PROCEDURE [at].[prc_ActivityView_Q_get]
(
	@ActivityViewID int
)
AS
BEGIN
	SET NOCOUNT ON
	DECLARE @Err Int

	SELECT
	[ActivityViewQID],
	[ActivityViewID],
	ISNULL([CategoryID], 0) AS 'CategoryID',
	ISNULL([QuestionID], 0) AS 'QuestionID',
	ISNULL([AlternativeID], 0) AS 'AlternativeID',
	ISNULL([SectionID], 0) AS 'SectionID',
	[No],
	[ItemID]
	FROM [at].[ActivityView_Q]
	WHERE
	[ActivityViewID] = @ActivityViewID
	ORDER BY [No]

	Set @Err = @@Error

	RETURN @Err
END


GO
