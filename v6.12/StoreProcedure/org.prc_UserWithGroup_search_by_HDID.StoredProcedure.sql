SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[org].[prc_UserWithGroup_search_by_HDID]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [org].[prc_UserWithGroup_search_by_HDID] AS' 
END
GO
/*
    Steve - Sept 22 2016 - EntityStatus for table UserGroup
    2016-10-10 Ray:     Remove column Status from org.User, org.Department, RDB.dbo.Result
    2018-07-12 Ray:     Replace UG_U by UGMember
*/
ALTER PROCEDURE [org].[prc_UserWithGroup_search_by_HDID]
(
    @HDID int,
    @SearchString    nvarchar(32),
    @SearchUsername int=0,
    @SearchExtID    int=0
)
AS
BEGIN
    SET NOCOUNT ON
    DECLARE @ActiveEntityStatusID INT = (SELECT EntityStatusID FROM EntityStatus WHERE CodeName='Active')
    DECLARE @Err Int,
            @ColumnDelimiter nvarchar(2) = '*',
            @RowDelimiter nvarchar(2) = '|'

    SELECT [u].[UserID], [u].[DepartmentID], [u].[LanguageID], [u].[UserName], [u].[Password], [u].[LastName], [u].[FirstName], [u].[Email], [u].[ExtID],
           [u].[SSN], [u].[Created], [u].[Mobile], [u].[Tag], [u].[Locked], [u].[Ownerid], [u].[ChangePassword], [d].[Name] AS [DepartmentName],
           (SELECT CONVERT(nvarchar, [ug].[UserGroupID]) + @ColumnDelimiter + [ug].[Name] + @RowDelimiter
            FROM [org].[UGMember] [ugm]
            JOIN [org].[UserGroup] [ug] ON [ugm].[UserGroupID] = [ug].[UserGroupID] AND [ugm].[UserID] = [u].[UserID]
             AND [ug].[EntityStatusID] = @ActiveEntityStatusID AND [ug].[Deleted] IS NULL
             AND [ugm].[EntityStatusID] = @ActiveEntityStatusID AND [ugm].[Deleted] IS NULL
            FOR XML PATH('')
           ) AS 'strUserGroups', [u].[EntityStatusID], [u].[Deleted], [u].[EntityStatusReasonID]
    FROM [org].[User] [u]
    JOIN [org].[H_D] [hd] ON [hd].[DepartmentID] = [u].[DepartmentID] AND [hd].[path] LIKE '%\'+ CAST(@HDID AS varchar(32)) + '\%' AND [hd].[deleted] = 0
    JOIN [org].[Department] [d] ON [d].[DepartmentID] = [hd].[DepartmentID] AND [d].[EntityStatusID] = @ActiveEntityStatusID AND [d].[Deleted] IS NULL
    WHERE ( ([u].[UserName] LIKE N'%'+@SearchString+'%')
           OR ([u].[FirstName] LIKE N'%'+@SearchString+'%')
           OR ([u].[FirstName]+' '+[u].[LastName] LIKE N'%'+@SearchString+'%')
           OR ([u].[LastName] LIKE N'%'+@SearchString+'%')
           OR ([u].[Email] LIKE N'%'+@SearchString+'%')
           OR ([u].[UserID] LIKE N'%'+@SearchString+'%')
           OR ([u].[ExtID] LIKE N'%'+@SearchString+'%')
           OR ([u].[Mobile] LIKE N'%'+@SearchString+'%')
          )
      AND [u].[EntityStatusID] = @ActiveEntityStatusID AND [u].[Deleted] IS NULL;

    SET @Err = @@Error

    RETURN @Err
END
GO
