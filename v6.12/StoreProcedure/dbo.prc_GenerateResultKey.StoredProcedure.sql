SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[prc_GenerateResultKey]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [dbo].[prc_GenerateResultKey] AS' 
END
GO
/*
	Steve - Dec 8, 2016 - Job Fair project: merge Q and R DB, then change how to make a dynamic query
	Steve - May 19, 2017 - Make SiteID a parameter
*/
ALTER PROCEDURE [dbo].[prc_GenerateResultKey]
(
    @NumberOutPutList int =10,
	@SiteID	int = 5
)
AS
BEGIN
    DECLARE @i int = 0, @j int =0
    DECLARE @tbRandom Table (String nvarchar(max))
    DECLARE @Chars varchar(100) = '' 
    DECLARE @RandomString varchar(max) = ''
    DECLARE @StringLen int
	DECLARE @LinkedDB NVARCHAR(MAX) = ' '
    --
    SELECT @Chars = Value from app.SiteParameter WHERE [Key] ='RESULT_KEY_CHARACTERS' AND SiteID = @SiteID
    SELECT @StringLen = Value from app.SiteParameter WHERE [Key] ='RESULT_KEY_NUM_OF_CHARS' AND SiteID = @SiteID

    DECLARE @sqlCommand nvarchar(max),@ReportServer nvarchar(max), @ReportDB nvarchar(max)
    CREATE TABLE #tbRandomTemp (String nvarchar(max))
    DECLARE c_ResultDatabase CURSOR READ_ONLY FOR
    SELECT  DISTINCT  ReportServer, ReportDB
    FROM at.Survey 
    OPEN c_ResultDatabase
	    FETCH NEXT FROM c_ResultDatabase INTO  @ReportServer, @ReportDB
	    WHILE @@FETCH_STATUS=0
	    BEGIN
			IF (@ReportServer = @@servername AND @ReportDB = DB_NAME())
				SET @LinkedDB = ' '
			ELSE 
				SET @LinkedDB = '[' + @ReportServer + '].[' + @ReportDB + '].'

			SET @sqlCommand ='INSERT INTO #tbRandomTemp (String) SELECT ResultKey FROM ' + @LinkedDB + 'dbo.Result WHERE ResultKey <>'''''
			EXECUTE  sp_executesql  @sqlCommand
        
			FETCH NEXT FROM c_ResultDatabase INTO  @ReportServer, @ReportDB
	    END
    CLOSE c_ResultDatabase
    DEALLOCATE c_ResultDatabase

    WHILE @i <@NumberOutPutList
    BEGIN
        WHILE @j < @StringLen
        BEGIN        
            SET @RandomString = @RandomString + RIGHT(LEFT(@Chars,ABS(BINARY_CHECKSUM(NEWID())%35) + 1),1)
            SET @j +=1
        END
        IF NOT EXISTS (SELECT * FROM #tbRandomTemp rt WHERE rt.String =@RandomString )
        BEGIN
            INSERT INTO @tbRandom (String) VALUES (@RandomString)
            SET @i +=1     
        END
        SET @RandomString =''
        SET @j =0
    END
    SELECT * FROM @tbRandom
    DROP TABLE #tbRandomTemp
END

GO
