SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[prc_AccessGroup_upd]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [dbo].[prc_AccessGroup_upd] AS' 
END
GO

ALTER PROCEDURE [dbo].[prc_AccessGroup_upd]
(
	@AccessGroupID int,
	@OwnerID int,
	@No smallint,
	@cUserid int,
	@Log smallint = 1
)
AS
BEGIN
	SET NOCOUNT ON
	DECLARE @Err Int

	UPDATE [dbo].[AccessGroup]
	SET
		[OwnerID] = @OwnerID,
		[No] = @No
	WHERE
		[AccessGroupID] = @AccessGroupID

	IF @Log = 1 
	BEGIN 
		INSERT INTO [Log].[AuditLog] ( UserId, TableName, Type, Data, Created) 
		SELECT @cUserid,'AccessGroup',1,
		( SELECT * FROM [dbo].[AccessGroup] 
			WHERE
			[AccessGroupID] = @AccessGroupID			 FOR XML AUTO) as data,
			getdate()
	END

	Set @Err = @@Error

	RETURN @Err
END


GO
