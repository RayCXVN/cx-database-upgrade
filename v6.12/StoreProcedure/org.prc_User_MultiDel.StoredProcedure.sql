SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[org].[prc_User_MultiDel]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [org].[prc_User_MultiDel] AS' 
END
GO
/*  
    2016-10-10 Ray:     Remove column Status from org.User, org.Department, RDB.dbo.Result
    2016-10-31 Sarah:   Hot fix: Get EntityStatus value on QDB and Set limit length of string which is used to update UserName
    Johnny - Dec 07 2016 - Update store procedure for dynamic SQL
    2017-01-11 Steve:   Fix prc_User_MultiDel for better transaction control   
    2017-02-09 Ray:     Remove cross database reference for Azure compatible
    2017-07-14 Steve:   add new param @DepartmentID which refers to a primary school of some users (Employees) among @UserIds (list of users)
				    IF (@DepartmentID IS NULL) OR (@DepartmentID IS NOT NULL And @UserIds (list of users) refers to list of students)
				        use old logic (Set users and their results Deactive)
				    IF (@DepartmentID IS NOT NULL And @UserIds (list of users) refers to list of employees)
				        set Deactive for users who have @DepartmentID as their primary school and delete U_D of other users
    -- For testing:
    DECLARE	@OutResult varchar(max)
    EXEC	[org].[prc_User_MultiDel] @OutResult = @OutResult OUTPUT, @OwnerId = 9, @UserIds = N'437370,17227', @DepartmentID = 18918, @UserId = 17225, @Log = 1
    SELECT	@OutResult as N'@OutResult'

    2017-11-24 Ray:     switch to use archetype instead of usertype, refactor
*/
ALTER PROCEDURE [org].[prc_User_MultiDel](
    @OutResult    varchar(max) OUT,
    @OwnerId      int,
    @UserIds      nvarchar(max) = N'',
    @UserId       int,
    @Log          smallint      = 1,
    @DepartmentID int           = NULL)
AS
BEGIN
    SET XACT_ABORT ON;
    BEGIN TRAN;
    SET NOCOUNT ON;
    DECLARE @sqlCommand_UpdateUsers AS nvarchar(max);
    DECLARE @SurveyIds AS varchar(max)= '';  -- Not null
    DECLARE @ResultIds AS varchar(max)= '';  -- Not null
    DECLARE @Result int= 0;

    IF LEN(@UserIds) > 0
    BEGIN
        DECLARE @DeactiveEntityStatusID int= 0, @StatusReason_ManuallySetDeactive int, @StatusReason_DeactivatedByRelatedObject int, @Archetype_Employee int;
        SELECT @DeactiveEntityStatusID = [EntityStatusID] FROM [EntityStatus] WHERE [CodeName] = 'Deactive';
        SELECT @StatusReason_ManuallySetDeactive = [EntityStatusReasonID] FROM [EntityStatusReason] WHERE [CodeName] = 'Deactive_ManuallySetDeactive';
        SELECT @StatusReason_DeactivatedByRelatedObject = [EntityStatusReasonID] FROM [EntityStatusReason] WHERE [CodeName] = 'Deactive_SetByRelatedObject';
        SELECT @Archetype_Employee = [ArchetypeID] FROM [dbo].[Archetype] WHERE [CodeName] = 'Employee';

        DECLARE @TableUser TABLE
        (
            [UserID]          int,
            [InPrimarySchool] bit,
            [IsEmployee]      bit
        );
        INSERT INTO @TableUser ([UserID], [InPrimarySchool], [IsEmployee])
        SELECT Value, 0, 0 FROM [dbo].[funcListToTableInt] (@UserIds, N',');

        UPDATE [t] SET [t].[IsEmployee] = 1
        FROM @TableUser [t]
        JOIN [org].[User] u ON u.[UserID] = t.[UserID] AND u.[ArchetypeID] = @Archetype_Employee;

        IF (@DepartmentID IS NULL) OR ( (@DepartmentID IS NOT NULL
                                        ) AND NOT EXISTS (SELECT 1 FROM @TableUser WHERE [IsEmployee] = 1) )
        BEGIN
            SET @sqlCommand_UpdateUsers = N'UPDATE [org].[User]
			 SET [UserName] = SUBSTRING(''DEL_'' + cast([UserID] AS VARCHAR(32)) + ''_''+ [UserName],1,128),
				    [EntityStatusID] = '+CONVERT(nvarchar(14), @DeactiveEntityStatusID)+',
				    [EntityStatusReasonID] = '+CAST(@StatusReason_ManuallySetDeactive AS nvarchar(32))+'
			 WHERE  UserID IN ('+@UserIds+')';
            --Delete users
            EXEC [sp_executesql] @sqlCommand_UpdateUsers;

            CREATE TABLE [#temptable]
            (
                [SurveyID] bigint,
                [ResultID] bigint
            );

            -- Set status for results on HD, loop for all Report database	
            DECLARE ReportDB_cursor CURSOR FORWARD_ONLY READ_ONLY
            FOR SELECT DISTINCT [s].[ReportServer], [s].[ReportDB]
                FROM [at].[Survey] [s]
                JOIN [at].[Activity] [a] ON [s].[ActivityID] = [a].[ActivityID] AND [a].[OwnerID] = @OwnerId;

            DECLARE @ReportServer nvarchar(64), @ReportDB nvarchar(64), @sqlCommand nvarchar(max);
            DECLARE @LinkedDB nvarchar(max);

            OPEN ReportDB_cursor;
            FETCH NEXT FROM ReportDB_cursor INTO @ReportServer, @ReportDB;
            WHILE @@FETCH_STATUS = 0
            BEGIN
                IF @ReportDB = DB_NAME() AND @ReportServer = @@servername
                BEGIN
                    SET @LinkedDB = ' ';
                END;
                ELSE
                BEGIN
                    SET @LinkedDB = '['+@ReportServer+'].['+@ReportDB+'].';
                END;

                DECLARE @sqlCommand1 AS nvarchar(max)= 'UPDATE R SET R.EntityStatusID = '+CAST(@DeactiveEntityStatusID AS varchar(32))+', R.[EntityStatusReasonID] = '+CAST(@StatusReason_DeactivatedByRelatedObject AS varchar(32))+'
				    OUTPUT INSERTED.SurveyID, INSERTED.ResultID INTO #temptable (SurveyID, ResultID)
				    FROM '+@LinkedDB+'[dbo].[Result] R  JOIN [at].[Survey] S ON R.SurveyID=S.SurveyID AND S.DeleteResultOnUserDelete=1  WHERE R.UserID in ('+@UserIds+')';

                -- PRINT @sqlCommand1								
                EXEC [sp_executesql] @sqlCommand1;

                FETCH NEXT FROM ReportDB_cursor INTO @ReportServer, @ReportDB;
            END;

            CLOSE ReportDB_cursor;
            DEALLOCATE ReportDB_cursor;

            SET @SurveyIds = STUFF( (SELECT ','+CAST([a].[SurveyID] AS varchar(32)) AS 'text()' FROM (SELECT DISTINCT
                                                                                                                [SurveyID] FROM [#temptable]
                                                                                                        ) AS [a] FOR XML PATH('')
                                    ), 1, 1, '');
            SET @ResultIds = STUFF( (SELECT ','+CAST([ResultID] AS varchar(32)) AS 'text()' FROM [#temptable] FOR xml PATH('')), 1, 1, '');

            DECLARE @sqlCommand_UpdateSurveys AS nvarchar(max);
            SET @sqlCommand_UpdateSurveys = N'UPDATE [at].[Survey]
			 SET [ReProcessOLAP] = 2
			 WHERE  SurveyID IN ('+@SurveyIds+')';
            -- Reprocess surveys
            EXEC [sp_executesql] @sqlCommand_UpdateSurveys;
        END;

        IF (@DepartmentID IS NOT NULL) AND NOT EXISTS (SELECT 1 FROM @TableUser WHERE [IsEmployee] = 0)
        BEGIN
            UPDATE [t] SET [t].[InPrimarySchool] = 1
            FROM @TableUser [t]
            JOIN [org].[User] [u] ON [t].[UserID] = [u].[UserID] AND [u].[DepartmentID] = @DepartmentID;

            -- set Deactive for users in their primary school 
            UPDATE [org].[User] SET [UserName] = SUBSTRING('DEL_'+CAST([UserID] AS varchar(32))+'_'+[UserName], 1, 128), [EntityStatusID] = @DeactiveEntityStatusID, [EntityStatusReasonID] = @StatusReason_ManuallySetDeactive
            WHERE [UserID] IN (SELECT [UserID] FROM @TableUser WHERE [InPrimarySchool] = 1);

            -- other users: delete U_D and U_D_UT
            DELETE [udut]
            FROM [org].[U_D_UT] [udut]
            JOIN [org].[U_D] [ud] ON [ud].[U_DID] = [udut].[U_DID] AND [ud].[DepartmentID] = @DepartmentID
            JOIN @TableUser [t] ON [ud].[UserID] = [t].[UserID] AND [t].[InPrimarySchool] = 0;

            DELETE [ud]
            FROM [org].[U_D] [ud]
            JOIN @TableUser [t] ON [ud].[UserID] = [t].[UserID] AND [t].[InPrimarySchool] = 0 AND [ud].[DepartmentID] = @DepartmentID;

            UPDATE [org].[User] SET [ForceUserLoginAgain] = 1 WHERE [UserID] IN (SELECT [UserID] FROM @TableUser WHERE [InPrimarySchool] = 0);

        END;

        -- Write log
        IF @Log = 1
        BEGIN
            DECLARE @sqlCommand_WriteLog AS nvarchar(max);
            SET @sqlCommand_WriteLog = N'INSERT INTO [Log].[AuditLog] ( UserId, TableName, Type, Data, Created) 
			 SELECT '+CAST(@UserId AS varchar(32))+',''User'',2,
				    ( SELECT * FROM [org].[User] 
			           WHERE UserID IN ('+@UserIds+')  FOR XML AUTO) as data,getdate()';
            EXEC [sp_executesql] @sqlCommand_WriteLog;
        END;
    END;
    --print 'Update child department : ' + @ResultIds + @SurveyIds

    IF EXISTS (SELECT * FROM [tempdb]..[sysobjects] WHERE [id] = OBJECT_ID('tempdb..#temptable'))
    BEGIN
        DROP TABLE [#temptable]
    END;

    COMMIT TRAN;

    SET @Result = @@Error;
    SET @OutResult = CAST(@Result AS varchar(max))+'|'+ISNULL(@ResultIds, '');
    RETURN @Result;
END;
GO