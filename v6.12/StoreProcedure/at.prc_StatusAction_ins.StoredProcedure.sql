SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[at].[prc_StatusAction_ins]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [at].[prc_StatusAction_ins] AS' 
END
GO

ALTER PROCEDURE [at].[prc_StatusAction_ins]
(
	@StatusActionID int = null output,
	@ActivityID int,
	@FromStatusTypeID INT=NULL,
	@ToStatusTypeID INT=NULL,
	@StatusActionTypeID int,
	@No smallint,
	@Settings XML ='',
	@cUserid int,
	@Log smallint = 1
)
AS
BEGIN
	SET NOCOUNT ON
	DECLARE @Err Int

	INSERT INTO [at].[StatusAction]
	(
		[ActivityID],
		[FromStatusTypeID],
		[ToStatusTypeID],
		[StatusActionTypeID],
		[No],
		[Settings]
	)
	VALUES
	(
		@ActivityID,
		@FromStatusTypeID,
		@ToStatusTypeID,
		@StatusActionTypeID,
		@No,
		@Settings
	)

	Set @Err = @@Error
	Set @StatusActionID = scope_identity()

	IF @Log = 1 
	BEGIN 
		INSERT INTO [Log].[AuditLog] ( UserId, TableName, Type, Data, Created) 
		SELECT @cUserid,'StatusAction',0,
		( SELECT * FROM [at].[StatusAction] 
			WHERE
			[StatusActionID] = @StatusActionID				 FOR XML AUTO) as data,
				getdate() 
	 END

	RETURN @Err
END

GO
