SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[app].[prc_LT_Site_get]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [app].[prc_LT_Site_get] AS' 
END
GO
ALTER PROCEDURE [app].[prc_LT_Site_get]  
 @SiteID int  
AS  
BEGIN  
 SET NOCOUNT ON;  
 DECLARE @Err Int      
 SELECT   
	   [LanguageID]  
      ,[SiteID]  
      ,[Name]  
      ,[Description]  
      ,[HeadData]
      ,[LoginText]  
      ,[FooterText]  
      ,[SupportText]  
      ,[ClosedText]  
      ,[ExtraFooterTextAfterAuthentication]
	  ,[BaseURL]
	  ,[ShortName]
	  ,[ProductURL]		
	  ,[ProductURLTitle]	
	  ,[BlogURL]			
	  ,[BlogURLTitle]	
	  ,[BodyData]
 FROM         
  [LT_Site]  
 WHERE    
  LT_Site.SiteID = @SiteID  
 Set @Err = @@Error  
 
 RETURN @Err  
END  

GO
