SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[prc_Answer_del]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [dbo].[prc_Answer_del] AS' 
END
GO
/* ------------------------------------------------------------
   PROCEDURE:    prc_Answer_del

   Description:  Deletes a record from table 'prc_Answer_del'

   AUTHOR:       LockwoodTech 28.07.2006 13:01:45
   ------------------------------------------------------------ */

ALTER PROCEDURE [dbo].[prc_Answer_del]
(
	@AnswerID bigint
)
As
BEGIN
	SET NOCOUNT ON
	DECLARE @Err Int

	DELETE
	FROM [Answer]
	WHERE
	[AnswerID] = @AnswerID

	Set @Err = @@Error

	RETURN @Err
End


GO
