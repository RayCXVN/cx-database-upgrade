SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[rep].[prc_UserActivityCategorySetting]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [rep].[prc_UserActivityCategorySetting] AS' 
END
GO
-- Stored Procedure

ALTER PROCEDURE [rep].[prc_UserActivityCategorySetting]	
	@UserActivitySettingID int = null output,
	@UserID int,
	@ActivityID int,	
	@ChartTypeID int = 9,
	@ChartTypeIDAvg int = 7,		
	@ShowCategory smallint = 0,	
	@ShowCategoryType smallint = 0,
	@CategoryIds nvarchar(MAX),
	@Log smallint = 1
AS 
BEGIN
	SET NOCOUNT ON;
	DECLARE @Err int
	
	IF (@UserID = 0 or @ActivityID = 0)
		RETURN -1
		
BEGIN TRANSACTION
	SET @UserActivitySettingID = (SELECT TOP 1 UserActivitySettingID FROM [rep].[UserActivitySetting] WHERE UserID = @UserId AND ActivityID = @ActivityId)

	IF (@UserActivitySettingID is NULL)
		EXEC [rep].[prc_UserActivitySetting_ins] @UserActivitySettingID output, @UserID, @ActivityID, 1, 0, @ChartTypeID, @ChartTypeIDAvg, 1, @ShowCategory, @ShowCategoryType, @UserID, @Log
	ELSE
	BEGIN
		UPDATE [rep].[UserActivitySetting] 
		SET		[ChartTypeID] = @ChartTypeID
				,[ChartTypeIDAvg] = @ChartTypeIDAvg		
				,[ShowCategory] = @ShowCategory
				,[ShowCategoryType] = @ShowCategoryType			
		WHERE UserActivitySettingID = @UserActivitySettingID
		
		IF @Log = 1   
		BEGIN   
		  INSERT INTO [Log].[AuditLog] ( UserId, TableName, Type, Data, Created)   
		  SELECT @UserID,'UserActivitySetting',1,  
		  (SELECT * FROM [rep].[UserActivitySetting]  
		   WHERE  
		   [UserActivitySettingID] = @UserActivitySettingID     FOR XML AUTO) as data,  
			getdate()   
		END
	END			

	IF @Log = 1   
	BEGIN   
	  INSERT INTO [Log].[AuditLog] ( UserId, TableName, Type, Data, Created)   
	  SELECT @UserID,'UAS_Category',2,  
	  (SELECT * FROM [rep].[UAS_Category]  
	   WHERE  
	   [UserActivitySettingID] = @UserActivitySettingID     FOR XML AUTO) as data,  
		getdate()   
	END
		
	--Delete old records 		
	DELETE [rep].[UAS_Category] 
	WHERE [UserActivitySettingID] = @UserActivitySettingID		

	IF(@CategoryIds = '') GOTO CommitResult

	INSERT INTO [rep].[UAS_Category] (UserActivitySettingID, CategoryID)
	SELECT @UserActivitySettingID, ParseValue FROM [dbo].[StringToArray](@CategoryIds,',')
	Set @Err = @@Error
	
	IF @Log = 1   
	BEGIN   
	  INSERT INTO [Log].[AuditLog] ( UserId, TableName, Type, Data, Created)   
	  SELECT @UserID,'UAS_Category',0,  
	  (SELECT * FROM [rep].[UAS_Category]  
	   WHERE  
	   [UserActivitySettingID] = @UserActivitySettingID     FOR XML AUTO) as data,  
		getdate()   
	END

CommitResult:
	COMMIT TRANSACTION
	RETURN @Err
END

GO
