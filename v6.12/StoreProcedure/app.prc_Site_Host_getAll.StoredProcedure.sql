SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[app].[prc_Site_Host_getAll]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [app].[prc_Site_Host_getAll] AS' 
END
GO
ALTER PROCEDURE [app].[prc_Site_Host_getAll]
(
	@SiteID INT
)
AS
BEGIN
	SET NOCOUNT ON;  
	DECLARE @Err INT 
	SELECT
		SiteHostID,
		SiteID,
		HostName,
		Active
	FROM [app].[Site_Host]
	WHERE
		SiteID=@SiteID

	SET @Err = @@Error  

	RETURN @Err  
END


GO
