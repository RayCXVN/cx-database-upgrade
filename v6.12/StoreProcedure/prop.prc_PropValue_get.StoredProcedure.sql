SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[prop].[prc_PropValue_get]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [prop].[prc_PropValue_get] AS' 
END
GO

ALTER PROCEDURE [prop].[prc_PropValue_get]
(
	@PropertyID int
)
AS
BEGIN
	SET NOCOUNT ON
	DECLARE @Err Int

	SELECT
	[PropValueID],
	[PropertyID],
	ISNULL([PropOptionID], 0) AS 'PropOptionID',
	ISNULL([PropFileID], 0) AS 'PropFileID',
	[No],
	[Created],
	ISNULL([CreatedBy], 0) AS 'CreatedBy',
	[Updated],
	ISNULL([UpdatedBy], 0) AS 'UpdatedBy',
	[ItemID],
	[Value]
	FROM [prop].[PropValue]
	WHERE
	[PropertyID] = @PropertyID

	Set @Err = @@Error

	RETURN @Err
END


GO
