SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[at].[prc_Batch_find]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [at].[prc_Batch_find] AS' 
END
GO

ALTER PROCEDURE [at].[prc_Batch_find]
(
	@BatchID int
)
AS
BEGIN
	SET NOCOUNT ON
	DECLARE @Err Int

	SELECT
	[BatchID],
	[SurveyID],
	ISNULL([UserID], 0) AS 'UserID',
	ISNULL([DepartmentID], 0) AS 'DepartmentID',
	[Name],
	[No],
	ISNULL([StartDate], 0) AS 'StartDate',
	ISNULL([EndDate], 0) AS 'EndDate',
	[Status],
	[MailStatus],
	[Created]
	FROM [at].[Batch]
	WHERE
	[BatchID] = @BatchID
	ORDER BY [No]

	Set @Err = @@Error

	RETURN @Err
END

GO
