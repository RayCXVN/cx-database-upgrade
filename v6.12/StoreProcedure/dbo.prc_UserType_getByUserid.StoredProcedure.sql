SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[prc_UserType_getByUserid]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [dbo].[prc_UserType_getByUserid] AS' 
END
GO
-- EXEC [dbo].[prc_UserType_getByUserid]  31,5 
ALTER PROCEDURE [dbo].[prc_UserType_getByUserid]
(
@UserId INT,
@Departmentid INT
)
AS
DECLARE @UserDepID as int
Select @UserDepID = Departmentid from org.[user] where userid = @Userid

IF @UserDepID = @Departmentid
BEGIN
SELECT UserTypeID FROM org.UT_U WHERE UserID = @Userid
END
ELSE
BEGIN
SELECT  udut.UserTypeID
FROM	   org.U_D_UT udut JOIN org.U_D ud ON udut.U_DID = ud.U_DID AND ud.DepartmentID = @Departmentid AND ud.UserID = @Userid
END

GO
