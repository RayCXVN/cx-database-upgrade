SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[list].[prc_ItemListRowAttribute_upd]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [list].[prc_ItemListRowAttribute_upd] AS' 
END
GO

-- =============================================
ALTER PROCEDURE [list].[prc_ItemListRowAttribute_upd]
	@ItemListRowAttributeID int,
	@ItemListID int,
	@Key nvarchar(256),
	@Value nvarchar(max),
    @cUserid int,
    @Log smallint = 1
AS
BEGIN
	SET NOCOUNT ON;
	DECLARE @Err Int
	
    UPDATE [list].[ItemListRowAttribute]
    SET 
		[ItemListID] = @ItemListID,
        [Key] = @Key,
        [Value] = @Value
     WHERE 
		[ItemListRowAttributeID] = @ItemListRowAttributeID
		
	IF @Log = 1 
	BEGIN 
		INSERT INTO [Log].[AuditLog] ( UserId, TableName, Type, Data, Created) 
		SELECT @cUserid,'ItemListRowAttribute',1,
		( SELECT * FROM [list].[ItemListRowAttribute] 
			WHERE
			[ItemListRowAttributeID] = @ItemListRowAttributeID			 FOR XML AUTO) as data,
			getdate()
	END

	Set @Err = @@Error

	RETURN @Err	
END


GO
