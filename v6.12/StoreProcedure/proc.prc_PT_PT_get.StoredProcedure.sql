SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[proc].[prc_PT_PT_get]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [proc].[prc_PT_PT_get] AS' 
END
GO

ALTER PROCEDURE [proc].[prc_PT_PT_get]
(
	@FromProcessTypeID int
)
AS
BEGIN
	SET NOCOUNT ON
	DECLARE @Err Int

	SELECT
	[FromProcessTypeID],
	[ToProcessTypeID]
	FROM [proc].[PT_PT]
	WHERE
	[FromProcessTypeID] = @FromProcessTypeID
	

	Set @Err = @@Error

	RETURN @Err
END

GO
