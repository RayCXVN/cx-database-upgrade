SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[form].[prc_FormCommand_ins]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [form].[prc_FormCommand_ins] AS' 
END
GO
ALTER PROCEDURE [form].[prc_FormCommand_ins]
(
	@FormCommandID int = null output,
	@FormID int,
	@No smallint,
	@FormMode smallint,
	@FormCommandType smallint,
	@CssClass nvarchar(256),
	@ClientFunction nvarchar(256),
	@CommandGroupClass nvarchar(50)=NULL,
	@cUserid int,
	@Log smallint = 1
)
AS
BEGIN
	SET NOCOUNT ON
	DECLARE @Err Int

	INSERT INTO [form].[FormCommand]
	(
		[FormID],
		[No],
		[FormMode],
		[FormCommandType],
		[CssClass],
		[ClientFunction],
		[CommandGroupClass]
	)
	VALUES
	(
		@FormID,
		@No,
		@FormMode,
		@FormCommandType,
		@CssClass,
		@ClientFunction,
		@CommandGroupClass
	)

	Set @Err = @@Error
	Set @FormCommandID = scope_identity()

	IF @Log = 1 
	BEGIN 
		INSERT INTO [Log].[AuditLog] ( UserId, TableName, Type, Data, Created) 
		SELECT @cUserid,'FormCommand',0,
		( SELECT * FROM [form].[FormCommand] 
			WHERE
			[FormCommandID] = @FormCommandID				 FOR XML AUTO) as data,
				getdate() 
	 END

	RETURN @Err
END



GO
