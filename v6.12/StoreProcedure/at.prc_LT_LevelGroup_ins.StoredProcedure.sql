SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[at].[prc_LT_LevelGroup_ins]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [at].[prc_LT_LevelGroup_ins] AS' 
END
GO

ALTER PROCEDURE [at].[prc_LT_LevelGroup_ins]
(
	@LanguageID int,
	@LevelGroupID int,
	@Name nvarchar(256),
	@Description nvarchar(max),
	@cUserid int,
	@Log smallint = 1
)
AS
BEGIN
	SET NOCOUNT ON
	DECLARE @Err Int

	INSERT INTO [at].[LT_LevelGroup]
	(
		[LanguageID],
		[LevelGroupID],
		[Name],
		[Description]
	)
	VALUES
	(
		@LanguageID,
		@LevelGroupID,
		@Name,
		@Description
	)

	Set @Err = @@Error

	IF @Log = 1 
	BEGIN 
		INSERT INTO [Log].[AuditLog] ( UserId, TableName, Type, Data, Created) 
		SELECT @cUserid,'LT_LevelGroup',0,
		( SELECT * FROM [at].[LT_LevelGroup] 
			WHERE
			[LanguageID] = @LanguageID AND
			[LevelGroupID] = @LevelGroupID				 FOR XML AUTO) as data,
				getdate() 
	 END

	RETURN @Err
END



GO
