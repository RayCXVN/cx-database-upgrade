SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[prc_AccessGeneric_ins]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [dbo].[prc_AccessGeneric_ins] AS' 
END
GO
/* 
	2017-08-21 Johnny: Add column SiteID for Accessgeneric
*/
ALTER PROCEDURE [dbo].[prc_AccessGeneric_ins]
(
	@AccessID int = null output,
	@TableTypeID smallint,
	@Elementid int,
	@Type smallint,
	@AccessGroupID int,
	@cUserid int,
	@Log smallint = 1,
    @SiteID INT=NULL
)
AS
BEGIN
	SET NOCOUNT ON
	DECLARE @Err Int

	INSERT INTO [dbo].[AccessGeneric]
	(
		[TableTypeID],
		[Elementid],
		[Type],
		[AccessGroupID],
        [SiteID]
	)
	VALUES
	(
		@TableTypeID,
		@Elementid,
		@Type,
		@AccessGroupID,
        @SiteID
	)

	Set @Err = @@Error
	Set @AccessID = scope_identity()

	IF @Log = 1 
	BEGIN 
		INSERT INTO [Log].[AuditLog] ( UserId, TableName, Type, Data, Created) 
		SELECT @cUserid,'AccessGeneric',0,
		( SELECT * FROM [dbo].[AccessGeneric] 
			WHERE
			[AccessID] = @AccessID				 FOR XML AUTO) as data,
				getdate() 
	    END

	RETURN @Err
END

GO
