SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[mea].[prc_LT_CheckListItemTemplate_ins]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [mea].[prc_LT_CheckListItemTemplate_ins] AS' 
END
GO
ALTER PROCEDURE [mea].[prc_LT_CheckListItemTemplate_ins]
	@LanguageID int,
	@CheckListItemTemplateID int,
	@Text nvarchar(max) = '',
	@cUserid int,
    @Log smallint = 1
AS
BEGIN
	SET NOCOUNT ON;
	DECLARE @Err Int
    
    INSERT INTO [mea].[LT_CheckListItemTemplate]
           ([LanguageID]
           ,[CheckListItemTemplateID]
           ,[Text])
    VALUES
           (@LanguageID
           ,@CheckListItemTemplateID
           ,@Text)
           
    Set @Err = @@Error
	IF @Log = 1 
	BEGIN 
		INSERT INTO [Log].[AuditLog] ( UserId, TableName, Type, Data, Created) 
		SELECT @cUserid,'LT_CheckListItemTemplate',0,
		( SELECT * FROM [mea].[LT_CheckListItemTemplate]
			WHERE
			[LanguageID] = @LanguageID AND
			[CheckListItemTemplateID] = @CheckListItemTemplateID				 FOR XML AUTO) as data,
				getdate() 
	END
	
	RETURN @Err       
END

GO
