SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[at].[prc_LT_Question_Activity_Get]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [at].[prc_LT_Question_Activity_Get] AS' 
END
GO



-- AT.prc_LT_Question_Activity_Get 19
ALTER Proc [at].[prc_LT_Question_Activity_Get]
(
  @ActivityID int
)
as

Select  LTQ.LanguageID, LTQ.QuestionID, LTQ.Name, LTQ.Title, LTQ.ReportText, LTQ.Description , LTQ.ShortName
from At.Page P
join At.Question Q on Q.Pageid = P.Pageid and P.activityid = @ActivityID
Join at.LT_Question LTQ on LTQ.Questionid = Q.Questionid
Order by LTQ.QuestionID


GO
