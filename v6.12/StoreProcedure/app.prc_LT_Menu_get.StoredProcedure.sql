SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[app].[prc_LT_Menu_get]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [app].[prc_LT_Menu_get] AS' 
END
GO

ALTER PROCEDURE [app].[prc_LT_Menu_get]
(
	@MenuID int
)
AS
BEGIN
	SET NOCOUNT ON
	DECLARE @Err Int

	SELECT
	[LanguageID],
	[MenuID],
	[Name],
	[Description],
	[Title]
	FROM [app].[LT_Menu]
	WHERE
	[MenuID] = @MenuID

	Set @Err = @@Error

	RETURN @Err
END


GO
