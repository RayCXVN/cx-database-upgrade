SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[at].[prc_CalcParameter_get]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [at].[prc_CalcParameter_get] AS' 
END
GO

ALTER PROCEDURE [at].[prc_CalcParameter_get]
(
	@CFID int
)
AS
BEGIN
	SET NOCOUNT ON
	DECLARE @Err Int

	SELECT
	[CalcParameterID],
	[CFID],
	[No],
	[Name],
	ISNULL([QuestionID], 0) AS 'QuestionID',
	ISNULL([AlternativeID], 0) AS 'AlternativeID',
	ISNULL([CategoryID], 0) AS 'CategoryID',
	[CalcType],
	[Format],
	[ItemID],
	[Created]
	FROM [at].[CalcParameter]
	WHERE
	[CFID] = @CFID
	ORDER BY [No]

	Set @Err = @@Error

	RETURN @Err
END

GO
