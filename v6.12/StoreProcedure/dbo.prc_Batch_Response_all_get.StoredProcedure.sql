SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[prc_Batch_Response_all_get]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [dbo].[prc_Batch_Response_all_get] AS' 
END
GO
ALTER PROCEDURE [dbo].[prc_Batch_Response_all_get]
(   @BatchId    int, 
    @Invited    int = 0 OUTPUT,
    @Finished   int = 0 OUTPUT
)
AS
    DECLARE @ActiveEntityStatusID INT = (SELECT EntityStatusID FROM EntityStatus WHERE CodeName='Active')

    SELECT @Invited = count(*), @Finished = count(Enddate) 
    FROM Result
    WHERE BatchId =  @BatchId AND EntityStatusID = @ActiveEntityStatusID AND Deleted IS NULL


GO
