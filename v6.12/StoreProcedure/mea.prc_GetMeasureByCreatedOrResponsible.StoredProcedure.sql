SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[mea].[prc_GetMeasureByCreatedOrResponsible]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [mea].[prc_GetMeasureByCreatedOrResponsible] AS' 
END
GO
ALTER PROCEDURE [mea].[prc_GetMeasureByCreatedOrResponsible]
(
	@UserID		INT
)
AS
BEGIN

	DECLARE @MeasureId INT
	CREATE TABLE #TABLE  (MeasureId INT ,MeasureTypeId INT,ParentId INT,CreatedBy INT,CreatedDate DATETIME,ChangedBy INT, ChangedDate DATETIME, Responsible INT,  StatusId INT, [Private] BIT, [Subject] nvarchar(256) , [Description] nvarchar(max),[Started] DATETIME, DueDate DATETIME, Completed DATETIME, CopyFrom INT, Active BIT, Deleted BIT, MeasureTemplateID INT, CanEdit BIT)
	INSERT INTO #TABLE  SELECT MeasureId,MeasureTypeId, ParentId,CreatedBy,CreatedDate,ChangedBy, ChangedDate, Responsible, StatusId, [Private], [Subject], [Description],[Started], [DueDate], [Completed], CopyFrom, Active, Deleted, MeasureTemplateID, CanEdit
	   FROM mea.Measure 
	   WHERE (CreatedBy =@UserID OR Responsible =@UserID) AND Deleted =0 AND Active =1  
	   
	DECLARE c_measure CURSOR READ_ONLY FOR SELECT MeasureId FROM #TABLE
	OPEN c_measure
	FETCH NEXT FROM c_measure INTO @MeasureId
		WHILE @@FETCH_STATUS=0
		BEGIN
		WITH RecQry AS
				(
					SELECT m.MeasureId 
					  FROM mea.Measure m
					 WHERE m.ParentId =@MeasureId
					UNION ALL
					SELECT a.MeasureId
					  FROM mea.Measure a INNER JOIN RecQry b
						ON a.ParentID = b.MeasureId
				)
			UPDATE #TABLE SET Deleted =1 WHERE (ParentId =@MeasureId OR ParentId IN (SELECT MeasureId FROM RecQry )) AND Deleted =0
		FETCH NEXT FROM c_measure INTO @MeasureId
		END
	CLOSE c_measure
	DEALLOCATE c_measure

	SELECT MeasureId, MeasureTypeId, ParentId, CreatedBy,CreatedDate,ChangedBy, ChangedDate, Responsible, StatusId, [Private], [Subject], [Description],[Started], [DueDate], [Completed], CopyFrom, Active, Deleted, MeasureTemplateID, CanEdit FROM #TABLE WHERE Deleted =0 --AND CopyFrom IS NULL
	DROP TABLE #TABLE
END

GO
