SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[form].[prc_FormRow_ins]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [form].[prc_FormRow_ins] AS' 
END
GO

ALTER PROCEDURE [form].[prc_FormRow_ins]
(
	@FormRowID int = null output,
	@FormID int,
	@No smallint,
	@RowTypeID smallint,
	@CssClass nvarchar(128),
	@cUserid int,
	@Log smallint = 1
)
AS
BEGIN
	SET NOCOUNT ON
	DECLARE @Err Int

	INSERT INTO [form].[FormRow]
	(
		[FormID],
		[No],
		[RowTypeID],
		[CssClass]
	)
	VALUES
	(
		@FormID,
		@No,
		@RowTypeID,
		@CssClass
	)

	Set @Err = @@Error
	Set @FormRowID = scope_identity()

	IF @Log = 1 
	BEGIN 
		INSERT INTO [Log].[AuditLog] ( UserId, TableName, Type, Data, Created) 
		SELECT @cUserid,'FormRow',0,
		( SELECT * FROM [form].[FormRow] 
			WHERE
			[FormRowID] = @FormRowID				 FOR XML AUTO) as data,
				getdate() 
	 END

	RETURN @Err
END


GO
