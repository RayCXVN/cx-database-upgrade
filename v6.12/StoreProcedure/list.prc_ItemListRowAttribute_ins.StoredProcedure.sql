SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[list].[prc_ItemListRowAttribute_ins]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [list].[prc_ItemListRowAttribute_ins] AS' 
END
GO
ALTER PROCEDURE [list].[prc_ItemListRowAttribute_ins]
	@ItemListRowAttributeID int = null output,
	@ItemListID int,
	@Key nvarchar(256),
	@Value nvarchar(max),
    @cUserid int,
    @Log smallint = 1
AS
BEGIN
	SET NOCOUNT ON;

    DECLARE @Err Int
	
	INSERT INTO [list].[ItemListRowAttribute]
           ([ItemListID]
           ,[Key]
           ,[Value])
     VALUES
           (@ItemListID
           ,@Key
           ,@Value)
           
     Set @Err = @@Error
	 Set @ItemListRowAttributeID = scope_identity()
	 IF @Log = 1 
	 BEGIN 
		INSERT INTO [Log].[AuditLog] ( UserId, TableName, Type, Data, Created) 
		SELECT @cUserid,'ItemListRowAttributeID',0,
		( SELECT * FROM [list].[ItemListRowAttributeID]
			WHERE
			[ItemListRowAttributeID] = @ItemListRowAttributeID				 FOR XML AUTO) as data,
				getdate() 
	 END

	
	 RETURN @Err
END

GO
