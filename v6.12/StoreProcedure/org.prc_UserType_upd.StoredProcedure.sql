SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[org].[prc_UserType_upd]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [org].[prc_UserType_upd] AS' 
END
GO
/*  
	2017-07-27 Steve: Change the length of column ExtID in all related tables from xAPI format
*/
ALTER PROCEDURE [org].[prc_UserType_upd]
(
	@UserTypeID int,
	@OwnerID int,
	@ExtID nvarchar(256),
	@No smallint,
	@cUserid int,
	@Log smallint = 1,
	@ArchetypeID int = NULL,
	@ParentID int = NULL
)
AS
BEGIN
	SET NOCOUNT ON
	DECLARE @Err Int

	UPDATE [org].[UserType]
	SET
		[OwnerID] = @OwnerID,
		[ExtID] = @ExtID,
		[No] = @No,
		[ArchetypeID] = @ArchetypeID,
		[ParentID] = @ParentID
	WHERE
		[UserTypeID] = @UserTypeID

	IF @Log = 1 
	BEGIN 
		INSERT INTO [Log].[AuditLog] ( UserId, TableName, Type, Data, Created) 
		SELECT @cUserid,'UserType',1,
		( SELECT * FROM [org].[UserType] 
			WHERE
			[UserTypeID] = @UserTypeID			 FOR XML AUTO) as data,
			getdate()
	END

	Set @Err = @@Error

	RETURN @Err
END


GO
