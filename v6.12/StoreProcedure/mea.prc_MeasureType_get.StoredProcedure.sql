SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[mea].[prc_MeasureType_get]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [mea].[prc_MeasureType_get] AS' 
END
GO
ALTER PROCEDURE [mea].[prc_MeasureType_get]    
(
 @OwnerID AS int
)
AS    
BEGIN    
 SELECT [MeasureTypeId]  
      ,[OwnerID]  
      ,[Name]  
      ,[AllowNestedMeasures]  
      ,[AllowCategories]  
      ,[AllowChecklist]  
      ,[AllowDueDate]  
      ,[DueDateMandatory]  
      ,[DueDateDefault]  
      ,[AllowRelated]  
      ,[AllowResources]  
  FROM [mea].[MeasureType]  
    
END    

GO
