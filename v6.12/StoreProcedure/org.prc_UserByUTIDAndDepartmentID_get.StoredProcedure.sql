SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[org].[prc_UserByUTIDAndDepartmentID_get]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [org].[prc_UserByUTIDAndDepartmentID_get] AS' 
END
GO
/*  2016-10-10 Ray: Remove column Status from org.User, org.Department, RDB.dbo.Result
*/
ALTER PROCEDURE [org].[prc_UserByUTIDAndDepartmentID_get]
(
  @DepartmentID INT,
  @UsertypeID int,
  @UsertypeID2 INT 
)
AS
BEGIN
	SET NOCOUNT ON

	DECLARE @EntityStatusID INT = 0
	SELECT @EntityStatusID = EntityStatusID FROM dbo.EntityStatus WHERE CodeName = N'Active'

	SELECT u.[UserID],
		u.[Ownerid],
		u.[DepartmentID],
		u.[LanguageID],
		ISNULL(u.[RoleID], 0) AS 'RoleID',
		u.[UserName],
		u.[Password],
		u.[LastName],
		u.[FirstName],
		u.[Email],
		u.[Mobile],
		u.[ExtID],
		u.[SSN],
		u.[Tag],
		u.[Locked],
		u.[ChangePassword],
		u.[HashPassword],
		u.[SaltPassword],
		u.[OneTimePassword],
		u.[OTPExpireTime],
		u.[Created],
		u.[EntityStatusID],
        u.Deleted,
        u.EntityStatusReasonID,
		d.Name as DepartmentName
     FROM org.[USER] u    
	JOIN org.[Department] d ON u.departmentid = d.departmentid and d.EntityStatusID = @EntityStatusID AND d.Deleted IS NULL AND u.EntityStatusID = @EntityStatusID AND u.Deleted IS NULL AND D.DepartmentID = @DepartmentID  
	JOIN org.[UT_U] utu on utu.userid = u.userid and (utu.usertypeid = @Usertypeid)
	JOIN org.[UT_U] utu2 on utu2.userid = u.userid and (utu2.usertypeid = @Usertypeid2)
    UNION ALL
    
    SELECT u.[UserID],
		u.[Ownerid],
		u.[DepartmentID],
		u.[LanguageID],
		ISNULL(u.[RoleID], 0) AS 'RoleID',
		u.[UserName],
		u.[Password],
		u.[LastName],
		u.[FirstName],
		u.[Email],
		u.[Mobile],
		u.[ExtID],
		u.[SSN],
		u.[Tag],
		u.[Locked],
		u.[ChangePassword],
		u.[HashPassword],
		u.[SaltPassword],
		u.[OneTimePassword],
		u.[OTPExpireTime],
		u.[Created],
		u.[EntityStatusID],
        u.Deleted,
        u.EntityStatusReasonID,
		d.Name as DepartmentName
     FROM org.[USER] u    
	JOIN org.U_D UD ON u.DepartmentID = UD.DepartmentID AND u.EntityStatusID = @EntityStatusID AND u.Deleted IS NULL AND UD.DepartmentID = @DepartmentID  
    JOIN org.Department D ON UD.DepartmentID = D.DepartmentID AND d.EntityStatusID = @EntityStatusID AND d.Deleted IS NULL
	JOIN org.[UT_U] utu on utu.userid = u.userid and (utu.usertypeid = @Usertypeid)
	JOIN org.[UT_U] utu2 on utu2.userid = u.userid and (utu2.usertypeid = @Usertypeid2)  
END

GO
