SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[prc_ResultGetBySurveyIDAndBatchId]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [dbo].[prc_ResultGetBySurveyIDAndBatchId] AS' 
END
GO
/*
    Sarah - Oct 31, 2016 - Hot fix: Get EntityStatus value on QDB
	Johnny - Des 07 2016 - Update store procedure for dynamic SQL
*/
ALTER PROCEDURE [dbo].[prc_ResultGetBySurveyIDAndBatchId]
	@SurveyId INT,
	@BatchId INT
AS
BEGIN
	DECLARE @ReportServer NVARCHAR(64),@ReportDB NVARCHAR(64),@ActiveEntityStatusID INT = 0
    SELECT @ActiveEntityStatusID = EntityStatusID FROM EntityStatus WHERE CodeName='Active'

	SELECT @ReportServer=ReportServer,@ReportDB=ReportDB FROM at.Survey
	WHERE SurveyID=@SurveyId
	DECLARE @LinkedDB NVARCHAR(MAX)
	IF @ReportDB=DB_NAME() AND @ReportServer=@@servername
		BEGIN
		SET @LinkedDB=' '
		END
	ELSE
		BEGIN
		SET @LinkedDB= '['+ @ReportServer + '].['+ @ReportDB + '].'
		END
	DECLARE @sqlCommand NVARCHAR(max),@sqlParameters NVARCHAR(Max)

	SET @sqlCommand='SELECT r.UserID,r.ResultID'+ 
		' FROM '+ @LinkedDB +'dbo.Result r'+
	    ' WHERE r.SurveyID=@p_SurveyID AND r.BatchID = @p_BatchID AND r.EntityStatusID = '+ CONVERT(NVARCHAR(14),@ActiveEntityStatusID) + ' AND r.Deleted IS NULL '	    
	SET @sqlParameters = N'@p_SurveyID int,@p_BatchID int'
	EXECUTE sp_executesql @sqlCommand, @sqlParameters, @p_SurveyID = @SurveyId, @p_BatchID = @BatchId
END

GO
