SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[note].[prc_Note_upd]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [note].[prc_Note_upd] AS' 
END
GO
/*
	2017-06-13 Steve: Change the length of column ExtID in all related tables from xAPI format
*/
ALTER PROCEDURE [note].[prc_Note_upd]
(
	@NoteID int,
	@NoteTypeID int,
	@Active bit,
	@ExtID nvarchar(256),
	@NotifyAtNextLogin bit,
	@StartDate datetime,
	@EndDate datetime,
	@CreatedBy int,
	@ChangedBy int,
	@Deleted bit,
	@cUserid int,
	@Log smallint = 1
)
AS
BEGIN
	SET NOCOUNT ON
	DECLARE @Err Int

	UPDATE [note].[Note]
	SET
		[NoteTypeID] = @NoteTypeID,
		[Active] = @Active,
		[ExtID] = @ExtID,
		[NotifyAtNextLogin] = @NotifyAtNextLogin,
		[StartDate] = @StartDate,
		[EndDate] = @EndDate,
		[CreatedBy] = @CreatedBy,
		[Changed] = getdate(),
		[ChangedBy] = @ChangedBy,
		[Deleted] = @Deleted
	WHERE
		[NoteID] = @NoteID

	Set @Err = @@Error

	IF @Log = 1 
	BEGIN 
		INSERT INTO [Log].[AuditLog] ( UserId, TableName, Type, Data, Created) 
		SELECT @cUserid,'Note',1,
		( SELECT * FROM [note].[Note] 
			WHERE
			[NoteID] = @NoteID FOR XML AUTO) as data,
				getdate() 
	 END

	RETURN @Err
END


GO
