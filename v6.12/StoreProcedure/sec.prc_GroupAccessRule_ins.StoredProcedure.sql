SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[sec].[prc_GroupAccessRule_ins]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [sec].[prc_GroupAccessRule_ins] AS' 
END
GO
ALTER PROCEDURE [sec].[prc_GroupAccessRule_ins]
(
	@GroupAccessRuleID int = NULL output,
	@GroupName nvarchar (512),
    @Description  nvarchar (512),
	@cUserid int,
	@Log smallint = 1
)
AS
BEGIN
SET NOCOUNT ON
DECLARE @Err int
INSERT INTO [sec].[GroupAccessRule] ([GroupName], [Description]) VALUES (@GroupName, @Description)
SET @Err = @@Error
SET @GroupAccessRuleID = SCOPE_IDENTITY()
IF @Log = 1 BEGIN
INSERT INTO [Log].[AuditLog] (UserId, TableName, Type, Data, Created)
    SELECT
        @cUserid,
        'GroupAccessRule',
        0,
        (SELECT
            *
        FROM [sec].[GroupAccessRule]
        WHERE [GroupAccessRuleID] = @GroupAccessRuleID
        FOR xml AUTO)
        AS data,
        GETDATE()
END
RETURN @Err
END

GO
