SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[org].[prc_DepartmentType_get]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [org].[prc_DepartmentType_get] AS' 
END
GO
ALTER PROCEDURE [org].[prc_DepartmentType_get]
(
	@OwnerID int
)
AS
BEGIN
	SET NOCOUNT ON
	DECLARE @Err Int

	SELECT
	[DepartmentTypeID],
	[OwnerID],
	[ExtID],
	[No],
	[Created],
	[ArchetypeID],
	[ParentID]
	FROM [org].[DepartmentType]
	WHERE
	[OwnerID] = @OwnerID
	ORDER BY [No]

	Set @Err = @@Error

	RETURN @Err
END

GO
