SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[org].[prc_Hierarchy_clone_HDs]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [org].[prc_Hierarchy_clone_HDs] AS' 
END
GO
/* ------------------------------------------------------------
   PROCEDURE:    prc_Hierarchy_clone_HDs

   Description:  Copy collection of HD's to the new hierarchy

   AUTHOR:       LockwoodTech 07.07.2006 12:52:51
   ------------------------------------------------------------ */

ALTER PROCEDURE [org].[prc_Hierarchy_clone_HDs]
(
	@FromHierarchyID int,
    @FromParentid int,
	@ToHierarchyID int,
    @ToParentId int
)
As
BEGIN
	SET NOCOUNT ON
	
    DECLARE @Err Int
	DECLARE @HDID as int
	DECLARE @NewHDID as int
	DECLARE @HierarchyID as int
	DECLARE @DepartmentID as int
	DECLARE @ParentID as int
	DECLARE @Created as smalldatetime
	DECLARE @Deleted as bit

	DECLARE H_D_Cursor CURSOR LOCAL FAST_FORWARD  FOR
	SELECT [HDID], [HierarchyID], [DepartmentID], [ParentID], [Deleted] FROM org.H_D WHERE HierarchyID = @FromHierarchyID and isnull(parentid,-1) = isnull(@FromParentid,-1)

	OPEN H_D_Cursor;
	FETCH NEXT FROM H_D_Cursor INTO @HDID, @HierarchyID, @DepartmentID, @ParentID,  @Deleted;
	WHILE @@FETCH_STATUS = 0
		BEGIN
			insert into org.H_D (HierarchyID, DepartmentID, ParentID, Path, PathName, Created, Deleted)
			Values (@ToHierarchyID, @DepartmentID, @ToParentId,'','', getdate(), @Deleted)
            
            set @NewHDID  = scope_identity()

            exec org.[prc_Hierarchy_clone_HDs]  @FromHierarchyID ,@HDID ,@ToHierarchyID ,@NewHDID 

			FETCH NEXT FROM H_D_Cursor INTO @HDID, @HierarchyID, @DepartmentID, @ParentID,  @Deleted;
		END

	CLOSE H_D_Cursor
	DEALLOCATE H_D_Cursor

	Set @Err = @@Error

	RETURN @Err
End

GO
