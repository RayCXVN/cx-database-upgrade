SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[rep].[prc_DocumentTemplate_ins]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [rep].[prc_DocumentTemplate_ins] AS' 
END
GO



ALTER PROCEDURE [rep].[prc_DocumentTemplate_ins]
(
	@DocumentTemplateID int = null output,
	@DocumentFormatID int,
	@OwnerID int,
	@ActivityID int = null ,
	@Template nvarchar(128),
	@CustomerID int = null,
	@cUserid int,
	@Log smallint = 1
)
AS
BEGIN
	SET NOCOUNT ON
	DECLARE @Err Int

	INSERT INTO [rep].[DocumentTemplate]
	(
		[DocumentFormatID],
		[OwnerID],
		[ActivityID],
		[Template],
		[CustomerID]
	)
	VALUES
	(
		@DocumentFormatID,
		@OwnerID,
		@ActivityID,
		@Template,
		@CustomerID
	)

	Set @Err = @@Error
	Set @DocumentTemplateID = scope_identity()

	IF @Log = 1 
	BEGIN 
		INSERT INTO [Log].[AuditLog] ( UserId, TableName, Type, Data, Created) 
		SELECT @cUserid,'DocumentTemplate',0,
		( SELECT * FROM [rep].[DocumentTemplate] 
			WHERE
			[DocumentTemplateID] = @DocumentTemplateID				 FOR XML AUTO) as data,
				getdate() 
	 END

	RETURN @Err
END


/****** Object:  StoredProcedure [rep].[prc_DocumentTemplate_ins]    Script Date: 03/10/2011 12:41:33 ******/
SET ANSI_NULLS ON

GO
