SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[rep].[prc_Bubble_SurveyLimit_upd]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [rep].[prc_Bubble_SurveyLimit_upd] AS' 
END
GO

ALTER PROCEDURE [rep].[prc_Bubble_SurveyLimit_upd]
(
	@BubbleID int,
	@SurveyID int,
	@AxisNo smallint,
	@LimitLow float,
	@LimitHigh float,
	@cUserid int,
	@Log smallint = 1
)
AS
BEGIN
	SET NOCOUNT ON
	DECLARE @Err Int

	UPDATE [rep].[Bubble_SurveyLimit]
	SET
		[BubbleID] = @BubbleID,
		[SurveyID] = @SurveyID,
		[AxisNo] = @AxisNo,
		[LimitLow] = @LimitLow,
		[LimitHigh] = @LimitHigh
	WHERE
		[BubbleID] = @BubbleID AND
		[SurveyID] = @SurveyID AND
		[AxisNo] = @AxisNo

	IF @Log = 1 
	BEGIN 
		INSERT INTO [Log].[AuditLog] ( UserId, TableName, Type, Data, Created) 
		SELECT @cUserid,'Bubble_SurveyLimit',1,
		( SELECT * FROM [rep].[Bubble_SurveyLimit] 
			WHERE
			[BubbleID] = @BubbleID AND
			[SurveyID] = @SurveyID AND
			[AxisNo] = @AxisNo			 FOR XML AUTO) as data,
			getdate()
	END

	Set @Err = @@Error

	RETURN @Err
END


GO
