SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[list].[prc_LT_ItemListCommand_upd]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [list].[prc_LT_ItemListCommand_upd] AS' 
END
GO

ALTER PROCEDURE [list].[prc_LT_ItemListCommand_upd]
	@LanguageID int,
	@ItemListCommandID int,
	@Name nvarchar(256),
	@Description nvarchar(max),
	@Text nvarchar(256),
	@TooltipText nvarchar(512),
    @cUserid int,
    @Log smallint = 1,
    @HotKey nvarchar(5)
AS
BEGIN
	SET NOCOUNT ON;
	DECLARE @Err Int
	
    UPDATE [list].[LT_ItemListCommand]
    SET 
		[Name] = @Name,
        [Description] = @Description,
        [Text] = @Text,
        [TooltipText] = @TooltipText,
        [HotKey] = @HotKey
     WHERE 
		[ItemListCommandID] = @ItemListCommandID AND
		[LanguageID] = @LanguageID
		
	IF @Log = 1 
	BEGIN 
		INSERT INTO [Log].[AuditLog] ( UserId, TableName, Type, Data, Created) 
		SELECT @cUserid,'LT_ItemListCommand',1,
		( SELECT * FROM [list].[LT_ItemListCommand] 
			WHERE
				[ItemListCommandID] = @ItemListCommandID AND
				[LanguageID] = @LanguageID			 FOR XML AUTO) as data,
			getdate()
	END

	Set @Err = @@Error

	RETURN @Err	
END

GO
