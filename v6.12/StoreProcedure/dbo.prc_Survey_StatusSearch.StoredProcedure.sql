SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[prc_Survey_StatusSearch]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [dbo].[prc_Survey_StatusSearch] AS' 
END
GO
ALTER PROC [dbo].[prc_Survey_StatusSearch]
    @SurveyID AS INT,
    @sName AS VARCHAR(256)
AS
BEGIN
    DECLARE @ActiveEntityStatusID INT = (SELECT EntityStatusID FROM EntityStatus WHERE CodeName='Active')

    SELECT sum(CASE WHEN resultid IS NULL THEN 0 ELSE 1 END) AS Invited,sum(CASE WHEN enddate IS NULL THEN 0 ELSE 1 END) AS Finished,
    department.departmentid, [name]
    FROM Result 
        JOIN org.department on department.departmentid =  Result.departmentid AND Result.EntityStatusID = @ActiveEntityStatusID AND result.Deleted IS NULL
            AND surveyid = @SurveyID AND department.EntityStatusID = @ActiveEntityStatusID AND department.Deleted IS NULL
    WHERE name LIKE @sName 
    GROUP BY department.departmentid,[name]
END
---------------------------------------------------------------------


GO
