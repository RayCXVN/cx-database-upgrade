SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[job].[prc_JobStatusLog_del]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [job].[prc_JobStatusLog_del] AS' 
END
GO


ALTER PROCEDURE [job].[prc_JobStatusLog_del]
(
	@JobStatusLogID int,
	@cUserid int,
	@Log smallint = 1
)
AS
BEGIN
	SET NOCOUNT ON
	DECLARE @Err Int

	IF @Log = 1 
	BEGIN 
		INSERT INTO [Log].[AuditLog] ( UserId, TableName, Type, Data, Created) 
		SELECT @cUserid,'JobStatusLog',2,
		( SELECT * FROM [job].[JobStatusLog] 
			WHERE
			[JobStatusLogID] = @JobStatusLogID
			 FOR XML AUTO) as data,
			getdate() 
	END 


	DELETE FROM [job].[JobStatusLog]
	WHERE
	[JobStatusLogID] = @JobStatusLogID

	Set @Err = @@Error

	RETURN @Err
END


GO
