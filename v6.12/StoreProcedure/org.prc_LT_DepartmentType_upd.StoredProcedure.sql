SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[org].[prc_LT_DepartmentType_upd]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [org].[prc_LT_DepartmentType_upd] AS' 
END
GO

ALTER PROCEDURE [org].[prc_LT_DepartmentType_upd]
(
	@LanguageID int,
	@DepartmentTypeID int,
	@Name nvarchar(256),
	@Description nvarchar(max),
	@cUserid int,
	@Log smallint = 1
)
AS
BEGIN
	SET NOCOUNT ON
	DECLARE @Err Int

	UPDATE [org].[LT_DepartmentType]
	SET
		[LanguageID] = @LanguageID,
		[DepartmentTypeID] = @DepartmentTypeID,
		[Name] = @Name,
		[Description] = @Description
	WHERE
		[LanguageID] = @LanguageID AND
		[DepartmentTypeID] = @DepartmentTypeID

	IF @Log = 1 
	BEGIN 
		INSERT INTO [Log].[AuditLog] ( UserId, TableName, Type, Data, Created) 
		SELECT @cUserid,'LT_DepartmentType',1,
		( SELECT * FROM [org].[LT_DepartmentType] 
			WHERE
			[LanguageID] = @LanguageID AND
			[DepartmentTypeID] = @DepartmentTypeID			 FOR XML AUTO) as data,
			getdate()
	END

	Set @Err = @@Error

	RETURN @Err
END


GO
