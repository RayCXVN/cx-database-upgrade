SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[prc_Batch_quick]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [dbo].[prc_Batch_quick] AS' 
END
GO
-- prc_Batch_quick 16860,36,5,0,0,1
ALTER PROCEDURE [dbo].[prc_Batch_quick]
(
	@HDID  int,
	@DTID  int,
	@PID	  int,
	@XCID  int,
	@AID	  int,
	@LanguageID int
)
AS
BEGIN
IF @DTID>0 
	SELECT s.ActivityID, s.SurveyID, b.Batchid, d.Name, lta.Name ActivityName, lts.Name SurveyName, ltp.Name Period
	FROM	  at.Batch b
	INNER JOIN at.Survey s ON b.SurveyID = s.SurveyID AND s.PeriodID = @PID
	INNER JOIN (SELECT DISTINCT ActivityID FROM at.XC_A WHERE (XCID = @XCID OR @XCID = 0) AND (ActivityID = @AID OR @AID = 0)) xc ON s.ActivityID = xc.ActivityID
	INNER JOIN org.H_D hd ON b.DepartmentID = hd.DepartmentID AND hd.[Path] like '%\' + CAST(@HDID as varchar(12)) + '\%' AND hd.Deleted = 0
	INNER JOIN org.DT_D dt ON b.DepartmentID = dt.DepartmentID AND dt.DepartmentTypeID = @DTID
	INNER JOIN org.Department d ON hd.DepartmentID = d.DepartmentID
	LEFT JOIN at.LT_Activity lta ON s.ActivityID = lta.ActivityID AND lta.LanguageID = @LanguageID
	LEFT JOIN at.LT_Survey lts ON s.SurveyID = lts.SurveyID AND lts.LanguageID = @LanguageID
	LEFT JOIN at.LT_Period ltp ON s.PeriodID = ltp.PeriodID AND ltp.LanguageID = @LanguageID
	ORDER BY s.ActivityID, s.SurveyID, b.BatchID, d.Name
ELSE
     SELECT s.ActivityID, s.SurveyID, b.Batchid, b.Name, lta.Name ActivityName, lts.Name SurveyName, ltp.Name Period
	FROM	  at.Batch b
	INNER JOIN at.Survey s ON b.SurveyID = s.SurveyID AND s.PeriodID = @PID
	INNER JOIN (SELECT DISTINCT ActivityID FROM at.XC_A WHERE (XCID = @XCID OR @XCID = 0) AND (ActivityID = @AID OR @AID = 0)) xc ON s.ActivityID = xc.ActivityID
	INNER JOIN org.H_D hd ON b.DepartmentID = hd.DepartmentID AND hd.HDID = @HDID
	LEFT JOIN at.LT_Activity lta ON s.ActivityID = lta.ActivityID AND lta.LanguageID = @LanguageID
	LEFT JOIN at.LT_Survey lts ON s.SurveyID = lts.SurveyID AND lts.LanguageID = @LanguageID
	LEFT JOIN at.LT_Period ltp ON s.PeriodID = ltp.PeriodID AND ltp.LanguageID = @LanguageID
	ORDER BY s.ActivityID, s.SurveyID, b.BatchID, b.Name
END

GO
