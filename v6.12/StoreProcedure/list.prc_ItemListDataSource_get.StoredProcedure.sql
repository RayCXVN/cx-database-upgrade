SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[list].[prc_ItemListDataSource_get]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [list].[prc_ItemListDataSource_get] AS' 
END
GO
ALTER PROCEDURE [list].[prc_ItemListDataSource_get]
	@OwnerID int
AS
BEGIN
	SET NOCOUNT ON;
	DECLARE @Err Int
	
    SELECT 
		[ItemListDataSourceID],
		[OwnerID],
		[FunctionName],
		[Created]
	FROM [list].[ItemListDataSource]
	WHERE [OwnerID] = @OwnerID
	
	Set @Err = @@Error

	RETURN @Err
  
END

GO
