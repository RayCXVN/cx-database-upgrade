SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[app].[prc_LT_MenuItem_get]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [app].[prc_LT_MenuItem_get] AS' 
END
GO
ALTER PROCEDURE [app].[prc_LT_MenuItem_get]  
(  
 @MenuItemID int  
)  
AS  
BEGIN  
 SET NOCOUNT ON  
 DECLARE @Err Int  
  
 SELECT  
 [LanguageID],  
 [MenuItemID],  
 ISNULL([Name], '') AS 'Name',  
 ISNULL([Description], '') AS 'Description',  
 ISNULL([ToolTip], '') AS 'ToolTip',  
 ISNULL([URL], '') AS 'URL'
 FROM [app].[LT_MenuItem]  
 WHERE  
 [MenuItemID] = @MenuItemID  
  
 Set @Err = @@Error  
  
 RETURN @Err  
END  
    

GO
