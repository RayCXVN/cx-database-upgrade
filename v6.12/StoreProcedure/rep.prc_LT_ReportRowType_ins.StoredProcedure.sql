SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[rep].[prc_LT_ReportRowType_ins]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [rep].[prc_LT_ReportRowType_ins] AS' 
END
GO

ALTER PROCEDURE [rep].[prc_LT_ReportRowType_ins]
(
	@ReportRowTypeID int,
	@LanguageID int,
	@Name nvarchar(256),
	@Description nvarchar(max),
	@cUserid int,
	@Log smallint = 1
)
AS
BEGIN
	SET NOCOUNT ON
	DECLARE @Err Int

	INSERT INTO [rep].[LT_ReportRowType]
	(
		[ReportRowTypeID],
		[LanguageID],
		[Name],
		[Description]
	)
	VALUES
	(
		@ReportRowTypeID,
		@LanguageID,
		@Name,
		@Description
	)

	Set @Err = @@Error

	IF @Log = 1 
	BEGIN 
		INSERT INTO [Log].[AuditLog] ( UserId, TableName, Type, Data, Created) 
		SELECT @cUserid,'LT_ReportRowType',0,
		( SELECT * FROM [rep].[LT_ReportRowType] 
			WHERE
			[ReportRowTypeID] = @ReportRowTypeID AND
			[LanguageID] = @LanguageID				 FOR XML AUTO) as data,
				getdate() 
	 END

	RETURN @Err
END


GO
