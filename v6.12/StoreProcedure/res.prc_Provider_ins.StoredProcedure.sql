SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[res].[prc_Provider_ins]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [res].[prc_Provider_ins] AS' 
END
GO
ALTER PROCEDURE [res].[prc_Provider_ins]    
(
	@ProviderID int = null output,
	@Icon nvarchar(128),
	@OwnerID int,
	@CustomerID int,
	@LinkGenerationMethod nvarchar(512),
    @ExtID nvarchar(512),
	@UseLogging bit = 0,
	@cUserid int,  
	@Log smallint = 1
)
AS    
BEGIN    
 SET NOCOUNT ON;    
 DECLARE @Err Int    

 INSERT INTO [res].[Provider]
 (
	[Icon],
	[OwnerId],
	[CustomerId],
	[LinkGenerationMethod],
    [ExtID],
	[UseLogging]
 )
 VALUES
 (
	@Icon,
	@OwnerId,
	@CustomerId,
	@LinkGenerationMethod,
    @ExtID,
	@UseLogging
 )
 
 Set @Err = @@Error 
 Set @ProviderID = scope_identity()
  
 IF @Log = 1   
 BEGIN   
  INSERT INTO [Log].[AuditLog] ( UserId, TableName, Type, Data, Created)   
  SELECT @cUserid,'res.Provider',0,  
  ( SELECT * FROM [res].[Provider]   
   WHERE  
   [ProviderID] = @ProviderID    FOR XML AUTO) as data,  
    getdate()   
  END  
  
 RETURN @Err  

END

GO
