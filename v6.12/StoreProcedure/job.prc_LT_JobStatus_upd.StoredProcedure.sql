SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[job].[prc_LT_JobStatus_upd]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [job].[prc_LT_JobStatus_upd] AS' 
END
GO

ALTER PROCEDURE [job].[prc_LT_JobStatus_upd]
(
	@LanguageID int,
	@JobStatusID smallint,
	@Name nvarchar(256),
	@Description nvarchar(max),
	@cUserid int,
	@Log smallint = 1
)
AS
BEGIN
	SET NOCOUNT ON
	DECLARE @Err Int

	UPDATE [job].[LT_JobStatus]
	SET
		[LanguageID] = @LanguageID,
		[JobStatusID] = @JobStatusID,
		[Name] = @Name,
		[Description] = @Description
	WHERE
		[LanguageID] = @LanguageID AND
		[JobStatusID] = @JobStatusID

	IF @Log = 1 
	BEGIN 
		INSERT INTO [Log].[AuditLog] ( UserId, TableName, Type, Data, Created) 
		SELECT @cUserid,'LT_JobStatus',1,
		( SELECT * FROM [job].[LT_JobStatus] 
			WHERE
			[LanguageID] = @LanguageID AND
			[JobStatusID] = @JobStatusID			 FOR XML AUTO) as data,
			getdate()
	END

	Set @Err = @@Error

	RETURN @Err
END


GO
