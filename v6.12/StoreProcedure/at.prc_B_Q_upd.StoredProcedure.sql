SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[at].[prc_B_Q_upd]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [at].[prc_B_Q_upd] AS' 
END
GO
ALTER PROCEDURE [at].[prc_B_Q_upd]
(
	@BulkID int,
	@QuestionID int
)
AS
BEGIN
	SET NOCOUNT ON
	DECLARE @Err Int

	UPDATE [at].[B_Q]
	SET
		[BulkID] = @BulkID,
		[QuestionID] = @QuestionID
	WHERE
		[BulkID] = @BulkID AND
		[QuestionID] = @QuestionID


	Set @Err = @@Error

	RETURN @Err
END



GO
