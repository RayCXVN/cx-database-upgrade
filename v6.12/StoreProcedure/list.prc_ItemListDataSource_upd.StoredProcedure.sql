SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[list].[prc_ItemListDataSource_upd]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [list].[prc_ItemListDataSource_upd] AS' 
END
GO

-- =============================================
ALTER PROCEDURE [list].[prc_ItemListDataSource_upd]
	@ItemListDataSourceID int,
	@OwnerID int,
    @FunctionName nvarchar(64),
    @cUserid int,
    @Log smallint = 1
AS
BEGIN
	SET NOCOUNT ON;
	DECLARE @Err Int
	
    UPDATE [list].[ItemListDataSource]
    SET 
		[OwnerID] = @OwnerID,
        [FunctionName] = @FunctionName
     WHERE 
		[ItemListDataSourceID] = @ItemListDataSourceID
		
	IF @Log = 1 
	BEGIN 
		INSERT INTO [Log].[AuditLog] ( UserId, TableName, Type, Data, Created) 
		SELECT @cUserid,'ItemListDataSource',1,
		( SELECT * FROM [list].[ItemListDataSource] 
			WHERE
			[ItemListDataSourceID] = @ItemListDataSourceID			 FOR XML AUTO) as data,
			getdate()
	END

	Set @Err = @@Error

	RETURN @Err	
END


GO
