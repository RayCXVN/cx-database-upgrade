SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[prc_Archetype_ins]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [dbo].[prc_Archetype_ins] AS' 
END
GO
ALTER PROCEDURE [dbo].[prc_Archetype_ins]
(
	@ArchetypeID smallint ,
	@CodeName nvarchar(128),
	@TableTypeID smallint= null,
	@cUserid int,
	@Log smallint = 1
)
AS
BEGIN
	SET NOCOUNT ON
	DECLARE @Err Int

	INSERT INTO [dbo].[Archetype]
	(
		[ArchetypeID],
		[CodeName],
		[TableTypeID]
	)
	VALUES
	(
		@ArchetypeID,
		@CodeName,
		@TableTypeID
	)

	Set @Err = @@Error
	Set @ArchetypeID = scope_identity()

	IF @Log = 1 
	BEGIN 
		INSERT INTO [Log].[AuditLog] ( UserId, TableName, Type, Data, Created) 
		SELECT @cUserid,'Archetype',0,
		( SELECT * FROM [dbo].[Archetype]
			WHERE
			[ArchetypeID] = @ArchetypeID				 FOR XML AUTO) as data,
				getdate() 
	 END

	RETURN @Err
END


GO
