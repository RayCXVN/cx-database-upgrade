SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[at].[prc_LT_LevelGroup_del]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [at].[prc_LT_LevelGroup_del] AS' 
END
GO

ALTER PROCEDURE [at].[prc_LT_LevelGroup_del]
(
	@LanguageID int,
	@LevelGroupID int,
	@cUserid int,
	@Log smallint = 1
)
AS
BEGIN
	SET NOCOUNT ON
	DECLARE @Err Int

	IF @Log = 1 
	BEGIN 
		INSERT INTO [Log].[AuditLog] ( UserId, TableName, Type, Data, Created) 
		SELECT @cUserid,'LT_LevelGroup',2,
		( SELECT * FROM [at].[LT_LevelGroup] 
			WHERE
			[LanguageID] = @LanguageID AND
			[LevelGroupID] = @LevelGroupID
			 FOR XML AUTO) as data,
			getdate() 
	END 


	DELETE FROM [at].[LT_LevelGroup]
	WHERE
		[LanguageID] = @LanguageID AND
		[LevelGroupID] = @LevelGroupID

	Set @Err = @@Error

	RETURN @Err
END


GO
