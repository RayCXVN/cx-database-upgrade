SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[job].[prc_JobParameter_upd]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [job].[prc_JobParameter_upd] AS' 
END
GO


ALTER PROCEDURE [job].[prc_JobParameter_upd]
(
	@JobParameterID int,
	@JobID int,
	@No smallint,
	@Name nvarchar(256),
	@Value nvarchar(max),
	@Type smallint,
	@cUserid int,
	@Log smallint = 1
)
AS
BEGIN
	SET NOCOUNT ON
	DECLARE @Err Int

	UPDATE [job].[JobParameter]
	SET
		[JobID] = @JobID,
		[No] = @No,
		[Name] = @Name,
		[Value] = @Value,
		[Type] = @Type
	WHERE
		[JobParameterID] = @JobParameterID

	IF @Log = 1 
	BEGIN 
		INSERT INTO [Log].[AuditLog] ( UserId, TableName, Type, Data, Created) 
		SELECT @cUserid,'JobParameter',1,
		( SELECT * FROM [job].[JobParameter] 
			WHERE
			[JobParameterID] = @JobParameterID			 FOR XML AUTO) as data,
			getdate()
	END

	Set @Err = @@Error

	RETURN @Err
END



GO
