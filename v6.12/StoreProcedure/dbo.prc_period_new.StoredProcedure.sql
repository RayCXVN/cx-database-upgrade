SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[prc_period_new]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [dbo].[prc_period_new] AS' 
END
GO
/*
    Sarah - Oct 10, 2016 - Remove column Status on User, Department and Result tables

    [dbo].[prc_period_new] 1, 2, 3
*/

ALTER proc [dbo].[prc_period_new] 
(
	@XCID		int,
	@PID1		int,
	@PID2		int
) --WITH RECOMPILE
AS
BEGIN
BEGIN TRANSACTION

DECLARE @ActiveEntityStatusID INT = (SELECT EntityStatusID FROM EntityStatus WHERE CodeName='Active')
DECLARE @StartDate	datetime,
		@EndDate	datetime,
		@SID		int,
		@NewID		int,
		@NewSID		int,
		@BID		int,
		@Name		varchar(200)

SELECT @StartDate = StartDate, @EndDate = EndDate FROM at.Period WHERE PeriodID = @PID2	

--Survey
DECLARE curSur CURSOR LOCAL FAST_FORWARD READ_ONLY FOR
SELECT SurveyID
FROM at.Survey s
INNER JOIN at.XC_A xc ON s.ActivityID = xc.ActivityID
WHERE s.PeriodID = @PID1 AND xc.XCID = @XCID

OPEN curSur
FETCH NEXT FROM curSur INTO @SID

PRINT 'START'

WHILE (@@FETCH_STATUS = 0)
BEGIN
	PRINT 'Insert Survey:'
	INSERT at.Survey(ActivityID, HierarchyID, StartDate, EndDate, Anonymous, Status, ShowBack, LanguageID, ButtonPlacement, UsePageNo, LinkURL, FinishURL, CreateResult, ReportDB, ReportServer, StyleSheet, Type, Created, LastProcessed, No, OLAPServer, OLAPDB, PeriodID, ProcessCategorys, DeleteResultOnUserDelete, FromSurveyID, LastProcessedResultID, LastProcessedAnswerID, ExtId, Tag)
	SELECT ActivityID, HierarchyID, @StartDate, @EndDate, Anonymous, Status, ShowBack, LanguageID, ButtonPlacement, UsePageNo, LinkURL, FinishURL, CreateResult, ReportDB, ReportServer, StyleSheet, Type, GETDATE(), LastProcessed, No, OLAPServer, OLAPDB, @PID2, 0, DeleteResultOnUserDelete, FromSurveyID, LastProcessedResultID, LastProcessedAnswerID, ExtId, Tag
	FROM at.Survey WHERE SurveyID = @SID
	PRINT 'GML:'
	SET @NewSID = SCOPE_IDENTITY()
	PRINT 'NY:'
	INSERT at.Access(SurveyID, RoleID, DepartmentID, DepartmentTypeID, CustomerID, PageID, QuestionID, Type, Mandatory)
	SELECT @NewSID, RoleID, DepartmentID, DepartmentTypeID, CustomerID, PageID, QuestionID, Type, Mandatory
	FROM at.Access 
	WHERE SurveyID = @SID
	
	INSERT at.LT_Survey(LanguageID, SurveyID, Name, Description, Info, FinishText, DisplayName)
	SELECT l.LanguageID, @NewSID, lp.Name, l.Description, Info, FinishText, DisplayName
	FROM at.LT_Survey l
	INNER JOIN at.LT_Period lp on l.languageid=lp.languageid and lp.Periodid=@PID2
	WHERE SurveyID = @SID
			
	--Batch
	DECLARE curB CURSOR LOCAL FAST_FORWARD READ_ONLY FOR
	SELECT BatchID
	FROM at.[Batch]
	WHERE SurveyID = @SID
	
	OPEN curB
	FETCH NEXT FROM curB INTO @BID
	
	WHILE (@@FETCH_STATUS = 0)
	BEGIN
		INSERT at.[Batch](SurveyID, UserID, DepartmentID, Name, No, StartDate, EndDate, Status, MailStatus)
		SELECT @NewSID, UserID, DepartmentID, Name, No, @StartDate, @EndDate, Status, MailStatus
		FROM at.[Batch] 		
		WHERE BatchID = @BID
		
		SET @NewID = SCOPE_IDENTITY()
		
		print 'Kopierer resultater inn i ny batch'
		INSERT Result(StartDate, EndDate, RoleID, UserID, UserGroupID, SurveyID, BatchID, LanguageID, PageNo, DepartmentID, [Anonymous], ShowBack, ResultKey, Email, LastUpdated, LastUpdatedBy, StatusTypeID, Created, CreatedBy, CustomerID, Deleted, EntityStatusID, EntityStatusReasonID)
		SELECT StartDate, EndDate, RoleID, UserID, UserGroupID, @NewSID, @NewID, LanguageID, PageNo, DepartmentID, [Anonymous], ShowBack, ResultKey, CAST(ResultID as varchar(20)), LastUpdated, LastUpdatedBy, StatusTypeID, Created, CreatedBy, CustomerID, Deleted, EntityStatusID, EntityStatusReasonID
		FROM Result 	
		WHERE BatchID = @BID AND EntityStatusID = @ActiveEntityStatusID AND Deleted IS NULL
						
		print 'Kopierer svarene'
		INSERT Answer(ResultID, QuestionID, AlternativeID, Value, DateValue, Free, [No], ItemID, [CustomerID], [LastUpdated], [LastUpdatedBy], [Created], [CreatedBy]) 
		SELECT RN.ResultID, A.QuestionID, A.AlternativeID, A.Value, A.DateValue, A.Free, A.[No], A.ItemID , A.[CustomerID], A.[LastUpdated], A.[LastUpdatedBy], A.[Created] ,A.[CreatedBy] FROM Answer A
		INNER JOIN Result R ON A.ResultID = R.ResultID
		INNER JOIN Result RN ON CAST(RN.Email As int) = R.ResultID		
		WHERE R.BatchID = @BID AND RN.BatchID = @NewID AND ISNUMERIC(RN.Email) = 1

		
		print 'Rydder opp i resultatene'
		UPDATE Result
		SET Email = ''
		FROM Result R 		
		WHERE R.BatchID = @NewID
		
		
		FETCH NEXT FROM curB INTO @BID
	END
	CLOSE curB
	DEALLOCATE curB
		
	FETCH NEXT FROM curSur INTO @SID
END
CLOSE curSur
DEALLOCATE curSur 
	
update at.Survey set LastProcessed='1900-01-01' where PeriodID=@pid2
	
COMMIT TRANSACTION
END



GO
