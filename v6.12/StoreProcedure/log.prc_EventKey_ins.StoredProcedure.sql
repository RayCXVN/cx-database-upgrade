SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[log].[prc_EventKey_ins]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [log].[prc_EventKey_ins] AS' 
END
GO

ALTER PROCEDURE [log].[prc_EventKey_ins]
(
	@EventKeyID	int = NULL output,
	@EventTypeID	int,
	@CodeName		varchar(32),
	@Encrypt		bit,
	@cUserid		int,
	@Log			smallint = 1
)
AS
BEGIN
	SET NOCOUNT ON
	DECLARE @Err Int

	INSERT INTO [log].[EventKey]
	(
		[EventTypeID],
		[CodeName],
		[Encrypt]
	)
	VALUES
	(
		@EventTypeID,
		@CodeName,
		@Encrypt
	)

	Set @Err = @@Error
	Set @EventKeyID = scope_identity()
	
	IF @Log = 1 
	BEGIN 
		INSERT INTO [Log].[AuditLog] ( UserId, TableName, Type, Data, Created) 
		SELECT @cUserid,'EventKey',0,
		( SELECT * FROM [log].[EventKey]
			WHERE
			[EventKeyID] = @EventKeyID FOR XML AUTO) as data,
				getdate() 
	END

	RETURN @Err
END


GO
