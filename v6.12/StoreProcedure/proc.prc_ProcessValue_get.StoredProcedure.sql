SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[proc].[prc_ProcessValue_get]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [proc].[prc_ProcessValue_get] AS' 
END
GO
ALTER PROCEDURE [proc].[prc_ProcessValue_get]
(
	@OwnerID int = null,
	@ProcessTypeID int = null
)
AS
BEGIN
	SET NOCOUNT ON
	DECLARE @Err Int

	SELECT
	[ProcessValueID],
	[No],
	[Value],
	[BackGroundColor],
	[OwnerID],
	isnull([ProcessTypeID],0) as 'ProcessTypeID',
	[URL],
	[ReadOnly] as 'ReadOnly', 
	[Created]
	FROM [proc].[ProcessValue]
	WHERE
	([OwnerID] = @OwnerID or @OwnerID is null)
	and  ([ProcessTypeID] = @ProcessTypeID or @ProcessTypeID is null)
	ORDER BY [No]

	Set @Err = @@Error

	RETURN @Err
END

GO
