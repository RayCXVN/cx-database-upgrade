SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[prc_TableType_upd]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [dbo].[prc_TableType_upd] AS' 
END
GO


ALTER PROCEDURE [dbo].[prc_TableType_upd]
(
	@TableTypeID smallint,
	@Name nvarchar(128),
	@Schema nvarchar(8),
	@cUserid int,
	@Log smallint = 1
)
AS
BEGIN
	SET NOCOUNT ON
	DECLARE @Err Int

	UPDATE [dbo].[TableType]
	SET
		[Name] = @Name,
		[Schema] = @Schema
	WHERE
		[TableTypeID] = @TableTypeID

	IF @Log = 1 
	BEGIN 
		INSERT INTO [Log].[AuditLog] ( UserId, TableName, Type, Data, Created) 
		SELECT @cUserid,'TableType',1,
		( SELECT * FROM [dbo].[TableType] 
			WHERE
			[TableTypeID] = @TableTypeID			 FOR XML AUTO) as data,
			getdate()
	END

	Set @Err = @@Error

	RETURN @Err
END



GO
