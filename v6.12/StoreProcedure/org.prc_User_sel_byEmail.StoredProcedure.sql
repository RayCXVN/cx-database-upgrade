SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[org].[prc_User_sel_byEmail]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [org].[prc_User_sel_byEmail] AS' 
END
GO
/*  2016-10-10 Ray: Remove column Status from org.User, org.Department, RDB.dbo.Result
*/
ALTER PROCEDURE [org].[prc_User_sel_byEmail]
(    
  @Email nvarchar(256),    
  @OwnerId int  
 )    
AS    
BEGIN
	DECLARE @EntityStatusID INT = 0
	SELECT @EntityStatusID = EntityStatusID FROM dbo.EntityStatus WHERE CodeName = N'Active'

	Select UserID, Ownerid, DepartmentID, LanguageID, isnull(RoleID,0) 'RoleID', UserName, Password, LastName, FirstName, Email, Mobile, ExtID, SSN, Tag,
           Locked, ChangePassword, Created, [HashPassword], [SaltPassword],	[OneTimePassword], [OTPExpireTime], CountryCode
	FROM org.[User]    
	WHERE Email = @Email    
	AND Ownerid = @OwnerId
	AND EntityStatusID = @EntityStatusID AND Deleted IS NULL	 
END

GO
