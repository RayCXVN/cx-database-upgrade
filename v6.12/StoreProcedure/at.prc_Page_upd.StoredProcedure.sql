SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[at].[prc_Page_upd]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [at].[prc_Page_upd] AS' 
END
GO
/*
	2017-06-13 Steve: Change the length of column ExtID in all related tables from xAPI format
*/
ALTER PROCEDURE [at].[prc_Page_upd]  
(  
 @PageID int,  
 @ActivityID int,  
 @No smallint,  
 @Type smallint,  
 @CssClass nvarchar(64) = '',  
 @ExtID nvarchar(256), 
 @Tag nvarchar(128) = '',  
 @cUserid int,  
 @Log smallint = 1  
)  
AS  
BEGIN  
 SET NOCOUNT ON  
 DECLARE @Err Int  
  
 UPDATE [at].[Page]  
 SET  
  [ActivityID] = @ActivityID,  
  [No] = @No,  
  [Type] = @Type,  
  [CssClass] = @CssClass,
  [ExtID]  = @ExtID,
  [Tag] = @Tag
 WHERE  
  [PageID] = @PageID  
  
 IF @Log = 1   
 BEGIN   
  INSERT INTO [Log].[AuditLog] ( UserId, TableName, Type, Data, Created)   
  SELECT @cUserid,'Page',1,  
  ( SELECT * FROM [at].[Page]   
   WHERE  
   [PageID] = @PageID    FOR XML AUTO) as data,  
   getdate()  
 END  
  
 Set @Err = @@Error  
  
 RETURN @Err  
END 


GO
