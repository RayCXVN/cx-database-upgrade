SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[rep].[prc_UAS_Category_ins]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [rep].[prc_UAS_Category_ins] AS' 
END
GO
ALTER PROCEDURE [rep].[prc_UAS_Category_ins]
 @UserActivitySettingID int,
 @CategoryID int,
 @cUserid int,
 @Log smallint = 1
AS
BEGIN
	SET NOCOUNT ON;
	DECLARE @Err Int
    
    INSERT INTO [rep].[UAS_Category]
           ([UserActivitySettingID]
			,[CategoryID])
     VALUES
           (@UserActivitySettingID
           ,@CategoryID)
           
    Set @Err = @@Error
    
	IF @Log = 1 
	BEGIN 
		INSERT INTO [Log].[AuditLog] ( UserId, TableName, Type, Data, Created) 
		SELECT @cUserid,'UAS_Category',0,
		( SELECT * FROM [rep].[UAS_Category]
		  WHERE
			[UserActivitySettingID] = @UserActivitySettingID
			AND [CategoryID] = @CategoryID				 FOR XML AUTO) as data,
				getdate() 
	END
	
	RETURN @Err       
END

GO
