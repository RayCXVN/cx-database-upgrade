SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[sec].[prc_AccessRule_get]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [sec].[prc_AccessRule_get] AS' 
END
GO
ALTER PROCEDURE [sec].[prc_AccessRule_get]
AS
BEGIN
	SET NOCOUNT ON
	DECLARE @Err Int
	SELECT [AccessRuleID]
      ,[No]
      ,[Inheritance]
      ,[ItemID]
      ,[TableTypeID]
      ,[TableTypeParameters]
      ,[Access]
      ,[Type]
      ,[AccessRuleName]
    FROM [sec].[AccessRule]
	ORDER BY [No]
	Set @Err = @@Error
	RETURN @Err
END

GO
