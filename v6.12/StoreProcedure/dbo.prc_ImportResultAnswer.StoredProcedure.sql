SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[prc_ImportResultAnswer]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [dbo].[prc_ImportResultAnswer] AS' 
END
GO
/*  Target: copy results/answers from manually registered activities to read-only activities
    Author: Ray Tran

    EXEC [dbo].[prc_ImportResultAnswer] @SourceActivityExtIDList = 'GIK-VG1', @TargetPageExtID = 'IMPORT', @PeriodID = 0, @Username = 'vokalint'
*/
ALTER PROCEDURE [dbo].[prc_ImportResultAnswer]
(   @SourceActivityExtIDList    nvarchar(max) = '',
    @TargetPageExtID            nvarchar(256) = '',
    @PeriodID                   int = 0,
    @Username                   nvarchar(64) = ''
)
AS
BEGIN
    SET XACT_ABORT ON
    SET NOCOUNT ON
    DECLARE @EntityStatus_Active int
    SELECT @EntityStatus_Active = es.[EntityStatusID] FROM [dbo].[EntityStatus] es WHERE es.[CodeName] = 'Active';

    -- Prepare mapping information
    DECLARE @ActivityMapping TABLE ([FromActivityID] int, [ToActivityID] int, [FromExtID] nvarchar(256), [ToExtID] nvarchar(256));
    DECLARE @BatchMapping TABLE ([FromSurveyID] int, [ToSurveyID] int, [FromBatchID] int, [ToBatchID] int);
    DECLARE @QuestionAlternativeMapping TABLE ([FromQuestionID] int, [ToQuestionID] int, [FromAlternativeID] int, [ToAlternativeID] int, [ToMinValue] float, [ToMaxValue] float);
    DECLARE @UpdatedResult TABLE ([ResultID] bigint, [FromResultID] bigint, [SurveyID] int);

    IF @PeriodID = 0
    BEGIN
        SELECT @PeriodID = p.[PeriodID] FROM [at].[Period] p WHERE CAST(GETDATE() AS date) BETWEEN p.[Startdate] AND p.[Enddate];
    END

    INSERT INTO @ActivityMapping ([FromActivityID], [FromExtID], [ToExtID])
    SELECT a.[ActivityID], [ParseValue] AS [FromExtID], STUFF([ParseValue], 1, 1, '') AS [ToExtID]
    FROM dbo.StringToArray(@SourceActivityExtIDList, ',') AS el
    JOIN [at].[Activity] a ON el.[ParseValue] = a.[ExtID];

    UPDATE m SET m.[ToActivityID] = a.[ActivityID]
    FROM @ActivityMapping m
    JOIN [at].[Activity] a ON m.[ToExtID] = a.[ExtID]
     AND (@TargetPageExtID = '' OR EXISTS (SELECT 1 FROM [at].[Page] p WHERE p.[ActivityID] = a.[ActivityID] AND p.[ExtID] = @TargetPageExtID));

    DELETE FROM @ActivityMapping WHERE [ToActivityID] IS NULL;

    INSERT INTO @BatchMapping ([FromSurveyID], [ToSurveyID], [FromBatchID], [ToBatchID])
    SELECT fs.[SurveyID] AS [FromSurveyID], ts.[SurveyID] AS [ToSurveyID], fb.[BatchID] AS [FromBatchID], tb.[BatchID] AS [ToBatchID]
    FROM @ActivityMapping m
    JOIN [at].[Survey] fs ON fs.[ActivityID] = m.[FromActivityID] AND fs.[PeriodID] = @PeriodID AND fs.[Status] = 2
    JOIN [at].[Batch] fb ON fb.[SurveyID] = fs.[SurveyID] AND fb.[Status] = 2
    JOIN [at].[Survey] ts ON ts.[ActivityID] = m.[ToActivityID] AND ts.[PeriodID] = @PeriodID AND ts.[Status] = 2
    JOIN [at].[Batch] tb ON tb.[SurveyID] = ts.[SurveyID] AND tb.[Status] = 2 AND tb.[DepartmentID] = fb.[DepartmentID];

    INSERT INTO @QuestionAlternativeMapping ([FromQuestionID], [ToQuestionID], [FromAlternativeID], [ToAlternativeID],
                                             [ToMinValue], [ToMaxValue])
    SELECT fq.[QuestionID] AS [FromQuestionID], tq.[QuestionID] AS [ToQuestionID], falt.[AlternativeID] AS [FromAlternativeID], talt.[AlternativeID] AS [ToAlternativeID],
           talt.[MinValue] AS [ToMinValue], talt.[MaxValue] AS [ToMaxValue]
    FROM @ActivityMapping m
    JOIN [at].[Page] fp ON fp.[ActivityID] = m.[FromActivityID]
    JOIN [at].[Question] fq ON fq.[PageID] = fp.[PageID]
    JOIN [at].[Alternative] falt ON falt.[ScaleID] = fq.[ScaleID]
    JOIN [at].[Page] tp ON tp.[ActivityID] = m.[ToActivityID] AND (@TargetPageExtID = '' OR tp.[ExtID] = @TargetPageExtID)
    JOIN [at].[Question] tq ON tq.[PageID] = tp.[PageID]
    JOIN [at].[Alternative] talt ON talt.[ScaleID] = tq.[ScaleID]
     AND (ISNULL(tq.[ExtId],'') + ISNULL(talt.[ExtID],'')) = (ISNULL(fq.[ExtId],'') + ISNULL(falt.[ExtID],''))  -- some match question's ExtID to alternative's ExtID and some others match question to question

    /* Verify */
    SELECT * FROM @ActivityMapping
    SELECT * FROM @BatchMapping
    SELECT qam.[FromQuestionID], qam.[ToQuestionID], fq.[ExtId] AS [FromQuestionExtID], tq.[ExtId] AS [ToQuestionExtID], ltfq.[Name] AS [FromQuestion], lttq.[Name] AS [ToQuestion],
           qam.[FromAlternativeID], qam.[ToAlternativeID], falt.[ExtID] AS [FromAltExtID], talt.[ExtID] AS [ToAltExtID], ltfa.[Name] AS [FromAlternative], ltta.[Name] AS [ToAlternative]
    FROM @QuestionAlternativeMapping qam
    JOIN [at].[Question] fq ON qam.[FromQuestionID] = fq.[QuestionID]
    JOIN [at].[LT_Question] ltfq ON ltfq.[QuestionID] = fq.[QuestionID] AND ltfq.[LanguageID] = 1
    JOIN [at].[Alternative] falt ON qam.[FromAlternativeID] = falt.[AlternativeID]
    JOIN [at].[LT_Alternative] ltfa ON falt.[AlternativeID] = ltfa.[AlternativeID] AND ltfa.[LanguageID] = 1
    JOIN [at].[Question] tq ON qam.[ToQuestionID] = tq.[QuestionID]
    JOIN [at].[LT_Question] lttq ON lttq.[QuestionID] = tq.[QuestionID] AND lttq.[LanguageID] = 1
    JOIN [at].[Alternative] talt ON qam.[ToAlternativeID] = talt.[AlternativeID]
    JOIN [at].[LT_Alternative] ltta ON talt.[AlternativeID] = ltta.[AlternativeID] AND ltta.[LanguageID] = 1

    -- Sync new updates for imported results
    UPDATE tr SET tr.[LastUpdated] = fr.[LastUpdated], tr.[LastUpdatedBy] = fr.[LastUpdatedBy], tr.[StatusTypeID] = fr.[StatusTypeID],
                  tr.[ParentResultID] = fr.[ResultID], tr.[ParentResultSurveyID] = fr.[SurveyID]
    OUTPUT [DELETED].[ResultID], [INSERTED].[ParentResultID], [DELETED].[SurveyID]
    INTO @UpdatedResult ([ResultID], [FromResultID], [SurveyID])
    FROM @BatchMapping bm
    JOIN [dbo].[Synonym_VOKAL_Result] fr ON fr.[SurveyID] = bm.[FromSurveyID] AND fr.[BatchID] = bm.[FromBatchID]
     AND fr.[EntityStatusID] = @EntityStatus_Active AND fr.[Deleted] IS NULL
    JOIN [dbo].[Synonym_VOKAL_Result] tr ON tr.[SurveyID] = bm.[ToSurveyID] AND tr.[BatchID] = bm.[ToBatchID] AND tr.[UserID] = fr.[UserID]
     AND tr.[EntityStatusID] = @EntityStatus_Active AND tr.[Deleted] IS NULL
     AND fr.[LastUpdated] > tr.[LastUpdated]; -- New changes detected
    RAISERROR ('%d results updated with new changes', 0, 1, @@rowcount) WITH NOWAIT;
    /*
    -- Delete target answers if source set to no answers
    DELETE ta
    FROM @UpdatedResult ur
    JOIN [dbo].[Synonym_VOKAL_Answer] fa ON ur.[FromResultID] = fa.[ResultID]
    JOIN @QuestionAlternativeMapping qam ON qam.[FromQuestionID] = fa.[QuestionID] AND qam.[FromAlternativeID] = fa.[AlternativeID]
    JOIN [dbo].[Synonym_VOKAL_Answer] ta ON ur.[ResultID] = ta.[ResultID] AND ta.[QuestionID] = qam.[ToQuestionID] AND ta.[AlternativeID] = qam.[ToAlternativeID]
     AND fa.[Value] NOT BETWEEN qam.[ToMinValue] AND qam.[ToMaxValue];
    RAISERROR ('%d answers deleted: out of range answers', 0, 1, @@rowcount) WITH NOWAIT;
    */
    -- Delete target answers if no answers found in source
    DELETE ta
    FROM @UpdatedResult ur
    JOIN [dbo].[Synonym_VOKAL_Answer] ta ON ur.[ResultID] = ta.[ResultID]
    JOIN @QuestionAlternativeMapping qam ON ta.[QuestionID] = qam.[ToQuestionID] AND ta.[AlternativeID] = qam.[ToAlternativeID]
    WHERE NOT EXISTS (SELECT 1 FROM [dbo].[Synonym_VOKAL_Answer] fa WHERE ur.[FromResultID] = fa.[ResultID] AND qam.[FromQuestionID] = fa.[QuestionID] AND qam.[FromAlternativeID] = fa.[AlternativeID]);
    RAISERROR ('%d answers deleted: not found in source', 0, 1, @@rowcount) WITH NOWAIT;

    -- Delete target answers of some questions to avoid autocalculation failures
    DELETE ta
    FROM @UpdatedResult ur
    JOIN [dbo].[Synonym_VOKAL_Answer] ta ON ur.[ResultID] = ta.[ResultID]
    JOIN [at].[Question] q ON q.[QuestionID] = ta.[QuestionID] AND q.[ExtId] = 'D';
    RAISERROR ('%d answers deleted: to avoid autocalculation failures', 0, 1, @@rowcount) WITH NOWAIT;

    UPDATE ta SET ta.[Value] = fa.[Value], ta.[Free] = fa.[Free], ta.[LastUpdated] = fa.[LastUpdated], ta.[LastUpdatedBy] = fa.[LastUpdatedBy]
    FROM @UpdatedResult ur
    JOIN [dbo].[Synonym_VOKAL_Answer] fa ON ur.[FromResultID] = fa.[ResultID]
    JOIN @QuestionAlternativeMapping qam ON qam.[FromQuestionID] = fa.[QuestionID] AND qam.[FromAlternativeID] = fa.[AlternativeID]
    JOIN [dbo].[Synonym_VOKAL_Answer] ta ON ur.[ResultID] = ta.[ResultID] AND ta.[QuestionID] = qam.[ToQuestionID] AND ta.[AlternativeID] = qam.[ToAlternativeID]
     AND (ta.[Value] <> fa.[Value] OR ta.[Free] <> fa.[Free]); -- New changes detected
    RAISERROR ('%d answers updated with new changes', 0, 1, @@rowcount) WITH NOWAIT;

    -- Copy new results
    INSERT INTO [dbo].[Synonym_VOKAL_Result] ([StartDate], [EndDate], [RoleID], [UserID], [UserGroupID], [SurveyID], [BatchID], [LanguageID], [PageNo],
                                              [DepartmentID], [Anonymous], [ShowBack], [ResultKey], [Email], [Created], [LastUpdated], [LastUpdatedBy],
                                              [StatusTypeID], [ValidFrom], [ValidTo], [DueDate], [ParentResultID], [ParentResultSurveyID], [CustomerID],
                                              [CreatedBy], [Deleted], [EntityStatusID], [EntityStatusReasonID])
    OUTPUT [INSERTED].[ResultID], [INSERTED].[ParentResultID], [INSERTED].[SurveyID]
    INTO @UpdatedResult ([ResultID], [FromResultID], [SurveyID])
    SELECT r.[StartDate], r.[EndDate], r.[RoleID], r.[UserID], r.[UserGroupID], bm.[ToSurveyID] AS [SurveyID], bm.[ToBatchID] AS [BatchID], r.[LanguageID], r.[PageNo],
           r.[DepartmentID], r.[Anonymous], r.[ShowBack], r.[ResultKey], r.[Email], r.[Created], r.[LastUpdated], r.[LastUpdatedBy],
           r.[StatusTypeID], r.[ValidFrom], r.[ValidTo], r.[DueDate], r.[ResultID] AS [ParentResultID], r.[SurveyID] AS [ParentResultSurveyID], r.[CustomerID],
           r.[CreatedBy], r.[Deleted], r.[EntityStatusID], r.[EntityStatusReasonID]
    FROM @BatchMapping bm
    JOIN [dbo].[Synonym_VOKAL_Result] r ON r.[SurveyID] = bm.[FromSurveyID] AND r.[BatchID] = bm.[FromBatchID] AND r.[EntityStatusID] = @EntityStatus_Active AND r.[Deleted] IS NULL
    WHERE NOT EXISTS (SELECT 1 FROM [dbo].[Synonym_VOKAL_Result] tr WHERE tr.[SurveyID] = bm.[ToSurveyID] AND tr.[BatchID] = bm.[ToBatchID] AND tr.[UserID] = r.[UserID]
                                                                      AND tr.[EntityStatusID] = @EntityStatus_Active AND tr.[Deleted] IS NULL);
    RAISERROR ('%d new results imported', 0, 1, @@rowcount) WITH NOWAIT;

    INSERT INTO [dbo].[Synonym_VOKAL_Answer] ([ResultID], [QuestionID], [AlternativeID], [Value], [DateValue], [No], [ItemID], [Free],
                                              [CustomerID], [LastUpdated], [LastUpdatedBy], [Created], [CreatedBy])
    SELECT ur.[ResultID], qam.[ToQuestionID] AS [QuestionID], qam.[ToAlternativeID] AS [AlternativeID], a.[Value], a.[DateValue], a.[No], a.[ItemID], a.[Free],
           a.[CustomerID], a.[LastUpdated], a.[LastUpdatedBy], a.[Created], a.[CreatedBy]
    FROM @UpdatedResult ur
    JOIN [dbo].[Synonym_VOKAL_Answer] a ON ur.[FromResultID] = a.[ResultID]
    JOIN @QuestionAlternativeMapping qam ON a.[QuestionID] = qam.[FromQuestionID] AND a.[AlternativeID] = qam.[FromAlternativeID] --AND a.[Value] BETWEEN qam.[ToMinValue] AND qam.[ToMaxValue]
    WHERE NOT EXISTS (SELECT 1 FROM [dbo].[Synonym_VOKAL_Answer] ta WHERE ta.[ResultID] = ur.[ResultID] AND ta.[QuestionID] = qam.[ToQuestionID]
                                                                      AND ta.[AlternativeID] = qam.[ToAlternativeID] AND ta.[No] = a.[No] AND ISNULL(ta.[ItemID],'') = ISNULL(a.[ItemID],''));
    RAISERROR ('%d new answers imported', 0, 1, @@rowcount) WITH NOWAIT;

    DECLARE @OwnerID int, @JobUserID int, @DBname nvarchar(256) = DB_NAME(), @ActivityID int, @SurveyID int, @ResultIDs nvarchar(MAX) = ''
    SELECT @JobUserID = [UserID] from [org].[User] WHERE [UserName] = @Username;

    DECLARE c_Survey CURSOR LOCAL FAST_FORWARD READ_ONLY FOR
    SELECT a.[OwnerID], a.[ActivityID], s.[SurveyID],
           STUFF((SELECT ',' + CAST(ur.[ResultID] AS nvarchar(32)) AS [text()]
                  FROM @UpdatedResult ur
                  WHERE ur.[SurveyID] = s.[SurveyID]
                  FOR XML PATH, TYPE).value('.[1]','nvarchar(max)')
                  , 1, 1, '') AS [ResultIDs]
    FROM @UpdatedResult r
    JOIN [at].[Survey] s ON r.[SurveyID] = [s].[SurveyID]
    JOIN [at].[Activity] a ON s.[ActivityID] = a.[ActivityID]
    GROUP BY a.[OwnerID], a.[ActivityID], s.[SurveyID]

    OPEN c_Survey
    FETCH NEXT FROM c_Survey INTO @OwnerID, @ActivityID, @SurveyID, @ResultIDs
    WHILE @@FETCH_STATUS = 0
    BEGIN
        EXEC [job].[CreateAutoCalcJob] @OwnerID = @OwnerID, @Userid = @JobUserID, @Servername = @@servername, @QDB = @DBname, @ActivityID = @ActivityID,
                                       @SurveyID = @SurveyID, @Username = @Username, @Password = '', @Description = 'AutoCalc for imported results',
                                       @BatchID = 0, @ResultIDs = @ResultIDs
        RAISERROR ('Auto calculated job created for Activity %d survey %d', 0, 1, @ActivityID, @SurveyID) WITH NOWAIT;
        FETCH NEXT FROM c_Survey INTO @OwnerID, @ActivityID, @SurveyID, @ResultIDs
    END
    CLOSE c_Survey
    DEALLOCATE c_Survey
END

GO
