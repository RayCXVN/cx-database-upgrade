SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[at].[prc_ReportDB_getByAccessGroup]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [at].[prc_ReportDB_getByAccessGroup] AS' 
END
GO

/* ------------------------------------------------------------
   PROCEDURE:    prc_ReportDB_getByAccessGroup

   Description:  Selects report servers and databases along with the denied survey list
   ------------------------------------------------------------ */

ALTER PROCEDURE [at].[prc_ReportDB_getByAccessGroup]
(
	@OwnerID			int,
	@AccessGroupList	nvarchar(max) = N''
)
As
BEGIN
	--exec [at].[prc_ReportDB_getByAccessGroup] 9, N''
	SET NOCOUNT ON
	DECLARE @Err				Int,
			@SurveyTableType	Int,	-- get the ID of Survey from dbo.TableType
			@ActivityTableType	Int,	-- get the ID of Activity from dbo.TableType
			@DeniedSurveyList	varchar(max)='',
			@AccessGenericCount	Int,
			@ReportServer		varchar(64)='',
			@ReportDB			varchar(64)='',
			@sqlCommand			nvarchar(max)

	SELECT  @SurveyTableType   = TableTypeID FROM dbo.TableType WHERE Name = 'Survey'
	SELECT  @ActivityTableType = TableTypeID FROM dbo.TableType WHERE Name = 'Activity'

	SELECT  @AccessGenericCount = count(AccessID)
	FROM	AccessGeneric a
			INNER JOIN AccessGroup g ON a.AccessGroupID = g.AccessGroupID
	WHERE	a.TableTypeID IN (@ActivityTableType, @SurveyTableType)
	  AND	g.OwnerID = @OwnerID
	
	IF @AccessGenericCount = 0
		BEGIN
			SELECT	DISTINCT s.ReportServer, s.ReportDB, '' as DeniedSurveyList
			FROM	at.Survey s
					INNER JOIN at.Activity act ON act.ActivityID = s.ActivityID AND act.OwnerID = @OwnerID
		END
	ELSE
		BEGIN
			CREATE TABLE #SurveyList (SurveyID int, ReportServer varchar(64), ReportDB varchar(64))
			
			-- Denied activities to group
			SET @sqlCommand = N'INSERT INTO #SurveyList (SurveyID, ReportServer, ReportDB)
			SELECT  s.SurveyID, s.ReportServer, s.ReportDB
			FROM	AccessGeneric a
					INNER JOIN at.Survey s ON a.Elementid = s.ActivityID
			WHERE	a.TableTypeID = ' + convert(nvarchar,@ActivityTableType) + '
			  AND	a.Type = 2'
			IF len(@AccessGroupList) > 0
			  SET @sqlCommand += ' AND a.AccessGroupID IN (' + @AccessGroupList + ')'
			EXECUTE sp_executesql @sqlCommand
			
			-- Full/Read access activities to others group
			SET @sqlCommand = N'INSERT INTO #SurveyList (SurveyID, ReportServer, ReportDB)
			SELECT  s.SurveyID, s.ReportServer, s.ReportDB
			FROM	AccessGeneric a
					INNER JOIN AccessGroup g ON a.AccessGroupID = g.AccessGroupID
					INNER JOIN at.Survey s ON a.Elementid = s.ActivityID
			WHERE	a.TableTypeID = ' + convert(nvarchar,@ActivityTableType) + '
			  AND	a.Type IN (0,1)
			  AND	g.OwnerID = ' + convert(nvarchar,@OwnerID)
			IF len(@AccessGroupList) > 0
			  SET @sqlCommand += ' AND a.AccessGroupID NOT IN (' + @AccessGroupList + ')'
			EXECUTE sp_executesql @sqlCommand
			
			-- Denied surveys to group
			SET @sqlCommand = N'INSERT INTO #SurveyList (SurveyID, ReportServer, ReportDB)
			SELECT  s.SurveyID, s.ReportServer, s.ReportDB
			FROM	AccessGeneric a
					INNER JOIN at.Survey s ON a.Elementid = s.SurveyID
			WHERE	a.TableTypeID = ' + convert(nvarchar,@SurveyTableType) + '
			  AND	a.Type = 2'
			IF len(@AccessGroupList) > 0
			  SET @sqlCommand += ' AND a.AccessGroupID IN (' + @AccessGroupList + ')'
			EXECUTE sp_executesql @sqlCommand
			
			-- Full/Read access surveys to others group
			SET @sqlCommand = N'INSERT INTO #SurveyList (SurveyID, ReportServer, ReportDB)
			SELECT  s.SurveyID, s.ReportServer, s.ReportDB
			FROM	AccessGeneric a
					INNER JOIN AccessGroup g ON a.AccessGroupID = g.AccessGroupID
					INNER JOIN at.Survey s ON a.Elementid = s.SurveyID
			WHERE	a.TableTypeID = ' + convert(nvarchar,@SurveyTableType) + '
			  AND	a.Type IN (0,1)
			  AND	g.OwnerID = ' + convert(nvarchar,@OwnerID)
			IF len(@AccessGroupList) > 0
			  SET @sqlCommand += ' AND a.AccessGroupID NOT IN (' + @AccessGroupList + ')'
			EXECUTE sp_executesql @sqlCommand
		
			CREATE Table #ResultDatabase (ReportServer varchar(64), ReportDB varchar(64), DeniedSurveyList varchar(max))
			
			-- Get a list of result databases which grant access to user
			INSERT INTO #ResultDatabase (ReportServer, ReportDB)
			SELECT	DISTINCT s.ReportServer, s.ReportDB
			FROM	at.Survey s
					INNER JOIN at.Activity act ON act.ActivityID = s.ActivityID AND act.OwnerID = @OwnerID
			
			DECLARE c_ResultDatabase CURSOR READ_ONLY FOR
			SELECT  ReportServer, ReportDB FROM #ResultDatabase
			
			OPEN c_ResultDatabase
			FETCH NEXT FROM c_ResultDatabase INTO @ReportServer, @ReportDB
			WHILE @@FETCH_STATUS=0
			BEGIN
				-- Getting denied survey list
				SELECT  @DeniedSurveyList += ',' + convert(varchar,SurveyID)
				FROM	#SurveyList
				WHERE	ReportServer = @ReportServer AND ReportDB = @ReportDB
			
				-- Update the denied survey list in each result database
				UPDATE #ResultDatabase SET DeniedSurveyList = stuff(@DeniedSurveyList, 1, 1, '')
				WHERE  ReportServer = @ReportServer AND ReportDB = @ReportDB
				
				FETCH NEXT FROM c_ResultDatabase INTO @ReportServer, @ReportDB
			END
			CLOSE c_ResultDatabase
			DEALLOCATE c_ResultDatabase
			
			SELECT ReportServer, ReportDB, DeniedSurveyList FROM #ResultDatabase
			
			DROP TABLE #SurveyList
			DROP TABLE #ResultDatabase
		END
	
	Set @Err = @@Error

	RETURN @Err
End

SET ANSI_NULLS ON

GO
