SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[at].[prc_LT_StatusType_get]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [at].[prc_LT_StatusType_get] AS' 
END
GO

ALTER PROCEDURE [at].[prc_LT_StatusType_get]
(
	@StatusTypeID int
)
AS
BEGIN
	SET NOCOUNT ON
	DECLARE @Err Int

	SELECT
	[LanguageID],
	[StatusTypeID],
	[Name],
	[Description],
	[DefaultActionName],
	[DefaultActionDescription]
	FROM [at].[LT_StatusType]
	WHERE
	[StatusTypeID] = @StatusTypeID

	Set @Err = @@Error

	RETURN @Err
END

GO
