SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[prop].[prc_PropPage_ins]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [prop].[prc_PropPage_ins] AS' 
END
GO

ALTER PROCEDURE [prop].[prc_PropPage_ins]
(
	@PropPageID int = null output,
	@TableTypeID smallint,
	@Type smallint,
	@No smallint,
	@cUserid int,
	@Log smallint = 1
)
AS
BEGIN
	SET NOCOUNT ON
	DECLARE @Err Int

	INSERT INTO [prop].[PropPage]
	(
		[TableTypeID],
		[Type],
		[No]
	)
	VALUES
	(
		@TableTypeID,
		@Type,
		@No
	)

	Set @Err = @@Error
	Set @PropPageID = scope_identity()

	IF @Log = 1 
	BEGIN 
		INSERT INTO [Log].[AuditLog] ( UserId, TableName, Type, Data, Created) 
		SELECT @cUserid,'PropPage',0,
		( SELECT * FROM [prop].[PropPage] 
			WHERE
			[PropPageID] = @PropPageID				 FOR XML AUTO) as data,
				getdate() 
	 END

	RETURN @Err
END


GO
