SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[prc_Survey_StatusByHDID]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [dbo].[prc_Survey_StatusByHDID] AS' 
END
GO
-- prc_Survey_StatusByHDID 7,604,1
ALTER proc [dbo].[prc_Survey_StatusByHDID]
@SurveyID as int,
@HDID as int,
@bAll as Bit = 0
AS
BEGIN
DECLARE @ActiveEntityStatusID INT = (SELECT EntityStatusID FROM EntityStatus WHERE CodeName='Active')
DECLARE @HierarchyID AS INT
DECLARE @DepartmentID AS INT

Select @HierarchyID = Hierarchyid,@DepartmentID = Departmentid from org.H_D where HDID = @HDID

IF @bAll = 1
BEGIN
	select dbo.GetPathNameDep(@HierarchyID,department.departmentid),  sum(case when resultid is null then 0 else 1 end) as Invited,sum(case when enddate is null then 0 else 1 end) as Finished,
	department.departmentid, [name]
	from Result  join org.department on department.departmentid =  Result.departmentid and result.EntityStatusID = @ActiveEntityStatusID AND result.Deleted IS NULL
	and department.departmentid IN (select departmentid from org.H_D where path like '%\' + cast(@HDID as varchar(10)) + '\%')
	and surveyid =  @SurveyID  and department.EntityStatusID = @ActiveEntityStatusID AND department.Deleted IS NULL
	group by department.departmentid,[name],dbo.GetPathNameDep(@HierarchyID,department.departmentid)
	order by dbo.GetPathNameDep(@HierarchyID,department.departmentid)
END
ELSE
BEGIN
    Select pathstring,Invited,Finished from 
	( select dbo.GetPathNameDep(@HierarchyID,@DepartmentID) as pathstring,  sum(case when resultid is null then 0 else 1 end) as Invited,sum(case when enddate is null then 0 else 1 end) as Finished
	from Result  join org.department on department.departmentid =  Result.departmentid and result.EntityStatusID = @ActiveEntityStatusID AND result.Deleted IS NULL
	join dbo.[fun_H_D_All_Departments](@HDID) d2 on d2.depid= department.departmentid
	and surveyid =  @SurveyID  and department.EntityStatusID = @ActiveEntityStatusID AND department.Deleted IS NULL) as a
    where Invited > 0
    order by pathstring

END
END
-------------------------------------------------------------------------------------


GO
