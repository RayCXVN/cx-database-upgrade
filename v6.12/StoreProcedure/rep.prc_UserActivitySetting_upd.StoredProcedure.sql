SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[rep].[prc_UserActivitySetting_upd]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [rep].[prc_UserActivitySetting_upd] AS' 
END
GO
ALTER PROCEDURE [rep].[prc_UserActivitySetting_upd]
 @UserActivitySettingID int = null output,
 @UserID int,
 @ActivityID int,
 @ShowTable smallint = 1,
 @ShowChart smallint = 0,
 @ChartTypeID int = 9,
 @ChartTypeIDAvg int = 7,
 @ShowPage smallint = 1,
 @ShowCategory smallint = 0,
 @ShowCategoryType smallint = 0,
 @cUserid int,
 @Log smallint = 1
AS  
BEGIN  
 SET NOCOUNT ON;  
 DECLARE @Err Int  
      
    UPDATE [rep].[UserActivitySetting] 
    SET		[ShowTable] = @ShowTable
			,[ShowChart] = @ShowChart
			,[ChartTypeID] = @ChartTypeID
			,[ChartTypeIDAvg] = @ChartTypeIDAvg
			,[ShowPage] = @ShowPage
			,[ShowCategory] = @ShowCategory
			,[ShowCategoryType] = @ShowCategoryType
     WHERE [UserID] = @UserID
			AND [ActivityID] = @ActivityID
             
    Set @Err = @@Error  
    Set @UserActivitySettingID = scope_identity()  
 IF @Log = 1   
 BEGIN   
  INSERT INTO [Log].[AuditLog] ( UserId, TableName, Type, Data, Created)   
  SELECT @cUserid,'UserActivitySetting',1,  
  (SELECT * FROM [rep].[UserActivitySetting]  
   WHERE  
   [UserActivitySettingID] = @UserActivitySettingID     FOR XML AUTO) as data,  
    getdate()   
 END  
   
 RETURN @Err         
END  

GO
