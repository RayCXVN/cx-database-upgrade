SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[proc].[prc_ProcessURL_upd]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [proc].[prc_ProcessURL_upd] AS' 
END
GO

ALTER PROCEDURE [proc].[prc_ProcessURL_upd]
(
	@ProcessURLID int,
	@ProcessID int,
	@URL nvarchar(512),
	@Name nvarchar(128),
	@Target nvarchar(32),
	@No smallint,
	@cUserid int,
	@Log smallint = 1
)
AS
BEGIN
	SET NOCOUNT ON
	DECLARE @Err Int

	UPDATE [proc].[ProcessURL]
	SET
		[ProcessID] = @ProcessID,
		[URL] = @URL,
		[Name] = @Name,
		[Target] = @Target,
		[No] = @No
	WHERE
		[ProcessURLID] = @ProcessURLID

	IF @Log = 1 
	BEGIN 
		INSERT INTO [Log].[AuditLog] ( UserId, TableName, Type, Data, Created) 
		SELECT @cUserid,'ProcessURL',1,
		( SELECT * FROM [proc].[ProcessURL] 
			WHERE
			[ProcessURLID] = @ProcessURLID			 FOR XML AUTO) as data,
			getdate()
	END

	Set @Err = @@Error

	RETURN @Err
END

GO
