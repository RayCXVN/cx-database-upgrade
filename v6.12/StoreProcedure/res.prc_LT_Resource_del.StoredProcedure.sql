SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[res].[prc_LT_Resource_del]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [res].[prc_LT_Resource_del] AS' 
END
GO
  
ALTER PROCEDURE [res].[prc_LT_Resource_del]
(
	@ResourceID int,
	@LanguageID int,
	@cUserid int,
	@Log smallint = 1
)
AS  
BEGIN  
	SET NOCOUNT ON  
	DECLARE @Err Int  
  
	IF @Log = 1 
	BEGIN 
		INSERT INTO [Log].[AuditLog] ( UserId, TableName, Type, Data, Created) 
		SELECT @cUserid,'res.LT_Resource',0,
		( SELECT * FROM [res].[LT_Resource] 
			WHERE
			[ResourceID] = @ResourceID AND [LanguageID] = @LanguageID	FOR XML AUTO) as data,
				getdate() 
	END

	DELETE FROM res.LT_Resource
	WHERE
		[ResourceID] = @ResourceID AND [LanguageID] = @LanguageID
	
	Set @Err = @@Error  
	
	RETURN @Err
END  

GO
