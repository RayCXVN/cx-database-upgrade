SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[org].[prc_GetActivityStatusByUserGroup]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [org].[prc_GetActivityStatusByUserGroup] AS' 
END
GO
/*
	Steve - Sept 22 2016 - EntityStatus for table UserGroup
     2018-07-09 Ray:    Replace UG_U by UGMember
*/
ALTER PROC [org].[prc_GetActivityStatusByUserGroup]
(
  @UserGroupIDList      varchar(max)='',
  @ActivityList         NVARCHAR(MAX),
  @ExcludeStatusTypeID  varchar(max),
  @PriorStatusTypeID    INT
) AS
BEGIN
    SET NOCOUNT ON
    DECLARE @ColumnDelimiter        nvarchar(5) = CHAR(131) + CHAR(7),
            @RowDelimiter           nvarchar(5) = CHAR(137) + CHAR(8),
            @ColumnDefinition       nvarchar(max) = N'',        -- in form of Activity_ + ID from @ActivityList
            @sqlCommand             nvarchar(max) = N'',
            @UpdateCommand          nvarchar(max) = N'',
            @Parameters             nvarchar(max) = N'@p_StatusTypeID int,@p_UserID int',
            @ActivityID             int,
            @EntityStatusID_Active  int = 0

    SELECT @EntityStatusID_Active = EntityStatusID FROM dbo.EntityStatus WHERE CodeName = N'Active'

    DECLARE c_Activity CURSOR READ_ONLY FOR
    SELECT ParseValue FROM dbo.StringToArray(@ActivityList,',')

    SELECT [u].[UserID], [u].[DepartmentID], [u].[UserName], [u].[LastName], [u].[FirstName], [d].[Name] AS [DepartmentName],
           (SELECT CONVERT(nvarchar, [ug].[UserGroupID]) + @ColumnDelimiter + [ug].[Name] + @ColumnDelimiter + CONVERT(nvarchar, [ug].[UserGroupTypeID]) + @RowDelimiter
            FROM [org].[UGMember] ugm
            JOIN [org].[UserGroup] [ug] ON [ugm].[UserGroupID] = [ug].[UserGroupID] AND [ugm].[UserID] = [u].[UserID]
             AND [ug].[EntityStatusID] = @EntityStatusID_Active AND [ug].[Deleted] IS NULL
             AND ugm.[EntityStatusID] = @EntityStatusID_Active AND ugm.[Deleted] IS NULL
            FOR XML PATH('')
            ) AS 'StrUserGroups'
    INTO [#ResultStatus]
    FROM [org].[USER] [u]
    JOIN [org].[Department] [d] ON [u].[departmentid] = [d].[departmentid] AND [d].[EntityStatusID] = @EntityStatusID_Active AND [d].[Deleted] IS NULL
     AND [u].[EntityStatusID] = @EntityStatusID_Active AND [u].[Deleted] IS NULL
    JOIN [org].[UGMember] [ugm] ON [ugm].[UserID] = [u].[UserID] AND ugm.[EntityStatusID] = @EntityStatusID_Active AND ugm.[Deleted] IS NULL
    JOIN [org].[UserGroup] [g] ON [ugm].[UserGroupID] = [g].[UserGroupID] AND [g].[EntityStatusID] = @EntityStatusID_Active AND [g].[Deleted] IS NULL
    WHERE [g].[UserGroupID] IN (SELECT [ParseValue] FROM [dbo].[StringToArray] (@UserGroupIDList, ',') );

    DECLARE c_Users CURSOR SCROLL READ_ONLY FOR
    SELECT UserID FROM #ResultStatus
    OPEN c_Users

    DECLARE @v_UserID int, @v_StatusTypeID int

    SET @sqlCommand = 'ALTER TABLE #ResultStatus ADD '

    OPEN c_Activity
    FETCH NEXT FROM c_Activity INTO @ActivityID
    WHILE @@FETCH_STATUS=0
    BEGIN
        -- Add column to temp table to store result status
        SET @ColumnDefinition = @sqlCommand + N'Activity_' + convert(nvarchar,@ActivityID) + N' int'
        EXECUTE sp_executesql @ColumnDefinition

        FETCH FIRST FROM c_Users INTO @v_UserID
        WHILE @@FETCH_STATUS=0
        BEGIN
            EXEC @v_StatusTypeID = [at].[GetLatestStatusByActivity] @v_UserID, @ActivityID, @ExcludeStatusTypeID, @PriorStatusTypeID

            SET @UpdateCommand = 'UPDATE #ResultStatus SET Activity_' + convert(nvarchar,@ActivityID) + ' = @p_StatusTypeID '
                        + 'WHERE UserID = @p_UserID'
            EXECUTE sp_executesql @UpdateCommand, @Parameters, @p_StatusTypeID = @v_StatusTypeID, @p_UserID = @v_UserID
            FETCH NEXT FROM c_Users INTO @v_UserID
        END
        FETCH NEXT FROM c_Activity INTO @ActivityID
    END
    CLOSE c_Activity
    DEALLOCATE c_Activity

    CLOSE c_Users
    DEALLOCATE c_Users

    SELECT * FROM #ResultStatus  

    DROP TABLE #ResultStatus
END

GO
