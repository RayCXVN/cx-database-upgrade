SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[app].[prc_OwnerId_By_SiteId_get]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [app].[prc_OwnerId_By_SiteId_get] AS' 
END
GO
ALTER PROCEDURE [app].[prc_OwnerId_By_SiteId_get]
 @SiteId Int
AS
BEGIN
 SELECT [OwnerID]
 FROM [app].[Site]
 Where [app].[Site].SiteID = @SiteId
END

GO
