SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[at].[prc_Batch_HaveBatchesBySurveys]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [at].[prc_Batch_HaveBatchesBySurveys] AS' 
END
GO
----------------------------------------------------------------------------------------------------------------
--Dev: Tyler
--Bug: 56931 [Report]- Survey of outsite reporting showed in Utvalg popup.
--EXEC [at].[prc_Batch_HaveBatchesBySurveys] '3054, 559, 594,595', 18448
ALTER PROCEDURE [at].[prc_Batch_HaveBatchesBySurveys]
(
	@SurveyIds varchar(max),
	@GlobalId INT = 0
)
AS
BEGIN
	SET NOCOUNT ON
	DECLARE @Err Int

	Declare @tblSurveyIds table (SurveyId int)
	
	Insert INTO @tblSurveyIds(SurveyId)
	Select Value from dbo.funcListToTableInt(@SurveyIds, ',')
	IF @GlobalId=0 
	BEGIN
		Select tbl.SurveyId 
		From @tblSurveyIds tbl
		Where Exists(SELECT TOP 1 * FROM at.Batch b WHERE b.SurveyID = tbl.SurveyId)
	END
	ELSE
	BEGIN
		DECLARE @SurveyID INT= 0, @BatchID INT=0, @CountDepartmentID INT=0, @DepartmentID INT=0
		DECLARE cur_survey CURSOR FOR
		Select SurveyId From @tblSurveyIds
		OPEN cur_survey
		FETCH NEXT FROM cur_survey INTO @SurveyId
		WHILE @@FETCH_STATUS = 0
			BEGIN
				SELECT  @CountDepartmentID=COUNT(DISTINCT(b.DepartmentID))  FROM at.Batch b WHERE SurveyID = @SurveyID

				IF @CountDepartmentID = 1
				BEGIN 
					SELECT  @BatchID=b.BatchID, @DepartmentID=b.DepartmentID  FROM at.Batch b WHERE b.SurveyID = @SurveyID
					IF @DepartmentID=@GlobalId
					BEGIN
						DELETE @tblSurveyIds WHERE SurveyId=@SurveyId
					END 
				END
				FETCH NEXT FROM cur_survey INTO @SurveyId
			END
		CLOSE cur_survey
		DEALLOCATE cur_survey
		
		Select tbl.SurveyId 
		From @tblSurveyIds tbl
	END
	Set @Err = @@Error

	RETURN @Err
END

GO
