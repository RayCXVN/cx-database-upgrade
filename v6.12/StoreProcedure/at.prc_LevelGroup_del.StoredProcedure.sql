SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[at].[prc_LevelGroup_del]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [at].[prc_LevelGroup_del] AS' 
END
GO

ALTER PROCEDURE [at].[prc_LevelGroup_del]
(
	@LevelGroupID int,
	@cUserid int,
	@Log smallint = 1
)
AS
BEGIN
	SET NOCOUNT ON
	DECLARE @Err Int

	IF @Log = 1 
	BEGIN 
		INSERT INTO [Log].[AuditLog] ( UserId, TableName, Type, Data, Created) 
		SELECT @cUserid,'LevelGroup',2,
		( SELECT * FROM [at].[LevelGroup] 
			WHERE
			[LevelGroupID] = @LevelGroupID
			 FOR XML AUTO) as data,
			getdate() 
	END 

	DELETE  from rep.RepLevelLimit where levelgroupid = @LevelGroupID
	Update  rep.Selection set LevelGroupID = null   where levelgroupid = @LevelGroupID


	DELETE FROM [at].[LevelGroup]
	WHERE
		[LevelGroupID] = @LevelGroupID

	Set @Err = @@Error

	RETURN @Err
END



GO
