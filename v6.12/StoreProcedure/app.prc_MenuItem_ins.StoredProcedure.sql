SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[app].[prc_MenuItem_ins]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [app].[prc_MenuItem_ins] AS' 
END
GO
/*
	2017-06-13 Steve: Change the length of column ExtID in all related tables from xAPI format
*/
ALTER PROCEDURE [app].[prc_MenuItem_ins]
(
	@MenuItemID int = null output,
	@MenuID int,
	@ParentID INT=NULL,
	@MenuItemTypeID INT=NULL,
	@PortalPageID INT=NULL,
	@No smallint,
	@CssClass nvarchar(64),
	@URL nvarchar(512),
	@Target nvarchar(64),
	@IsDefault bit,
	@Active bit,
	@cUserid int,
	@Log smallint = 1,
	@NoRender bit = 0,
     @Created DATETIME = NULL,
	@ExtID nvarchar(256)=''
    
)
AS
BEGIN
	SET NOCOUNT ON
	DECLARE @Err Int

	SET @Created = ISNULL(@Created, GETDATE())

	INSERT INTO [app].[MenuItem]
	(
		[MenuID],
		[ParentID],
		[MenuItemTypeID],
		[PortalPageID],
		[No],
		[CssClass],
		[URL],
		[Target],
		[IsDefault],
		[Active],
        [NoRender],
		[Created],
		[ExtID]
	)
	VALUES
	(
		@MenuID,
		@ParentID,
		@MenuItemTypeID,
		@PortalPageID,
		@No,
		@CssClass,
		@URL,
		@Target,
		@IsDefault,
		@Active,
        @NoRender,
		@Created,
		@ExtID
	)

	Set @Err = @@Error
	Set @MenuItemID = scope_identity()

	IF @Log = 1 
	BEGIN 
		INSERT INTO [Log].[AuditLog] ( UserId, TableName, Type, Data, Created) 
		SELECT @cUserid,'MenuItem',0,
		( SELECT * FROM [app].[MenuItem] 
			WHERE
			[MenuItemID] = @MenuItemID				 FOR XML AUTO) as data,
				getdate() 
	 END

	RETURN @Err
END

GO
