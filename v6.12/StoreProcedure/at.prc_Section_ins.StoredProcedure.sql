SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[at].[prc_Section_ins]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [at].[prc_Section_ins] AS' 
END
GO

ALTER PROCEDURE [at].[prc_Section_ins]
(
	@SectionID int = null output,
	@ActivityID int,
	@Type smallint,
	@MinOccurance smallint,
	@MaxOccurance smallint,
	@Moveable bit,
	@CssClass nvarchar(64) = '',
	@cUserid int,
	@Log smallint = 1
)
AS
BEGIN
	SET NOCOUNT ON
	DECLARE @Err Int

	INSERT INTO [at].[Section]
	(
		[ActivityID],
		[Type],
		[MinOccurance],
		[MaxOccurance],
		[Moveable],
		[CssClass]
	)
	VALUES
	(
		@ActivityID,
		@Type,
		@MinOccurance,
		@MaxOccurance,
		@Moveable,
		@CssClass
	)

	Set @Err = @@Error
	Set @SectionID = scope_identity()

	IF @Log = 1 
	BEGIN 
		INSERT INTO [Log].[AuditLog] ( UserId, TableName, Type, Data, Created) 
		SELECT @cUserid,'Section',0,
		( SELECT * FROM [at].[Section] 
			WHERE
			[SectionID] = @SectionID				 FOR XML AUTO) as data,
				getdate() 
	 END

	RETURN @Err
END


GO
