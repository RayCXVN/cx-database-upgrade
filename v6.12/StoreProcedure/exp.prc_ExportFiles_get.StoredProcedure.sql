SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[exp].[prc_ExportFiles_get]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [exp].[prc_ExportFiles_get] AS' 
END
GO
ALTER PROCEDURE [exp].[prc_ExportFiles_get]
(
  @Userid INT
)
AS
BEGIN
	SET NOCOUNT ON
	DECLARE @Err Int

	SELECT
	[ExportFileID],
	[UserID],
	[MIMEType],
	[Data],
	[Created],
	[EmailSendtTo],
	[Description],
	[Size],
	[Filename],
	ISNULL([SurveyID], 0) AS 'SurveyID'
	FROM [exp].[ExportFiles]
	WHERE UserId = @Userid

	Set @Err = @@Error

	RETURN @Err
END
GO
