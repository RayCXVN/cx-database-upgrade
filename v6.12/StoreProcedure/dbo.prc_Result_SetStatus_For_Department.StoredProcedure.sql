SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[prc_Result_SetStatus_For_Department]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [dbo].[prc_Result_SetStatus_For_Department] AS' 
END
GO
/*  2016-10-10 Ray: Remove column Status from org.User, org.Department, RDB.dbo.Result
*/
ALTER PROCEDURE [dbo].[prc_Result_SetStatus_For_Department]
(
    @SurveyID INT,
    @Departmentid INT,
    @EntityStatusID INT
)
AS
BEGIN
  UPDATE dbo.RESULT SET EntityStatusID = @EntityStatusID
  WHERE SurveyId = @SurveyID AND departmentid = @DepartmentID
END


GO
