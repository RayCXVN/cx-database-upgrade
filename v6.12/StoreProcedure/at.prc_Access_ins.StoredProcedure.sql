SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[at].[prc_Access_ins]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [at].[prc_Access_ins] AS' 
END
GO
ALTER PROCEDURE [at].[prc_Access_ins](
    @AccessID         int      = NULL OUTPUT,
    @SurveyID         int,
    @BatchID          int      = NULL,
    @RoleID           int      = NULL,
    @DepartmentID     int      = NULL,
    @HDID             int      = NULL,
    @DepartmentTypeID int      = NULL,
    @CustomerID       int      = NULL,
    @PageID           int      = NULL,
    @QuestionID       int      = NULL,
    @Type             smallint,
    @Mandatory        bit,
    @cUserid          int,
    @Log              smallint = 1,
    @ArchetypeID      int      = NULL)
AS
BEGIN
    SET NOCOUNT ON;
    DECLARE @Err int;

    INSERT INTO [at].[Access] ([SurveyID], [BatchID], [RoleID], [DepartmentID], [DepartmentTypeID], [HDID], [CustomerID], [PageID], [QuestionID], [Type],
                               [Mandatory], [ArchetypeID])
    VALUES (@SurveyID, @BatchID, @RoleID, @DepartmentID, @DepartmentTypeID, @HDID, @CustomerID, @PageID, @QuestionID, @Type, @Mandatory, @ArchetypeID);

    SET @Err = @@Error;
    SET @AccessID = SCOPE_IDENTITY();

    IF @Log = 1
    BEGIN
        INSERT INTO [Log].[AuditLog] ([UserId], [TableName], [Type], [Data], [Created])
        SELECT @cUserid, 'Access', 0, (SELECT * FROM [at].[Access] WHERE [AccessID] = @AccessID FOR XML AUTO) AS [data], GETDATE();
    END;

    RETURN @Err;
END;
GO