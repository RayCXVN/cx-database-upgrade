SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[Logon]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [dbo].[Logon] AS' 
END
GO
/*
    Sarah - Oct 10, 2016 - Remove column Status on User, Department and Result tables
    Ray - 2017-02-08: Move Encrypt/Decrypt out of database, return encrypted password to code for comparison
	Johnny 2017-02-09: Add condition Deleted IS NULL
*/
ALTER PROCEDURE [dbo].[Logon]
(
    @UserName           nvarchar(128),
    @PassWord           nvarchar(64),
    @UserID             int OUTPUT,
    @LanguageID         int = 1 OUTPUT,
    @DepartmentID       int OUTPUT,
    @Firstname          nvarchar(64) OUTPUT,
    @Lastname           nvarchar(64) OUTPUT,
    @Email              nvarchar(256) OUTPUT,
    @Mobile             nvarchar(16) OUTPUT,
    @ExtId              nvarchar(64) OUTPUT,
    @OwnerId            int,
    @ErrNo              int OUTPUT,
    @ChangePassword     bit = 0 OUTPUT,
    @LogonType          INT = 0 OUTPUT,
    @OutputPassword     nvarchar(64) = '' OUTPUT,
    @OutputOTP          nvarchar(64) = '' OUTPUT
)
AS
BEGIN

DECLARE @EntityStatusID INT = 0 
SELECT @EntityStatusID=EntityStatusID FROM EntityStatus WHERE CodeName='Active'

    SELECT 	
        @DepartmentID = DepartmentID,
        @LanguageID = LanguageID,
        @UserID = UserID,
        @Firstname = Firstname,
        @Lastname = Lastname,
        @Email = Email,
        @Mobile = Mobile,
        @ExtId = ExtId,
        @OwnerId = OwnerId,
        @ChangePassword = ChangePassword,
        @OutputPassword = [Password],
        @OutputOTP = [OneTimePassword]
    FROM 	
        [org].[User] 
    WHERE 	
        UserName = @UserName
        AND [EntityStatusID] = @EntityStatusID
        AND [Ownerid] = @OwnerId
		AND Deleted IS NULL

IF @@RowCount < 1 
BEGIN
	SET @ErrNo = 1
END
ELSE
BEGIN	
	SET @ErrNo = 0
END
END

GO
