SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[prc_Answer_InsertOrUpdate]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [dbo].[prc_Answer_InsertOrUpdate] AS' 
END
GO
ALTER PROCEDURE [dbo].[prc_Answer_InsertOrUpdate]

@Err int out,
@resultId int,
@itemId int,--student id
@answervalues nvarchar(max),
@metadataId int,
@altStuUpdatedId int,--Student last updated
@altStuStatusId int, --Student status
@stuStatusValue int
/*form: 'questionId1, valueAltenativeId1,value1, commentAlternativeId1, comment1;
								questionId2,valueAltenativeId2,value2, commentAlternativeId2, comment2' 
						--e.g: '1123,4568,1,2333,agsb sjssolsbhchsc'¦1124,4569,0,563,2333,hhuiqhwec jqwd'
						
						if metadataId<=0 or matadata alternativeId <=0 will not update that metadat.
						
						*/

AS
BEGIN
SET NOCOUNT OFF
SET @Err = 0

BEGIN TRANSACTION

IF(LEN(@answervalues) > 0)
BEGIN
DECLARE @tlValues table (qId int, altId int, value int)
DECLARE @tlComments table (qId int, altId int, comment  nvarchar(max))  --Temp table
begin -- insert questionIds, alternativeIds, values into temptable
DECLARE	@col_sep char,
	@row_sep char, 
	@colIdex int, 
	@precolIdex int,
	@rowIndex int,
	@preRowIndex int,
	@questionId varchar(20), @ValueAltId varchar(20), @Value varchar(20),@CommmetAltId varchar(20), @Comment nvarchar(max),
	@answerRecord nvarchar(max)
	set @row_sep='¦'
	set @answervalues=@answervalues+@row_sep	
	set @col_sep=','	
	set @colIdex=0
	set @rowIndex=0
	set @preRowIndex=0
	set @rowIndex=CHARINDEX(@row_sep,@answervalues)
	WHILE @rowIndex >0
	begin
	set @answerRecord=SUBSTRING(@answervalues,@preRowIndex,@rowIndex-@preRowIndex)	
	if(len(@answerRecord)>0)
	begin		
		set @colIdex=CHARINDEX(@col_sep,@answerRecord)
		set @questionId=SUBSTRING(@answerRecord,0,@colIdex)
		
		set @precolIdex=@colIdex+1;
		set @colIdex=CHARINDEX(@col_sep,@answerRecord,@precolIdex)			
		set @ValueAltId=SUBSTRING(@answerRecord,@precolIdex,@colIdex-@precolIdex)		
		
		set @precolIdex=@colIdex+1
		set @colIdex=CHARINDEX(@col_sep,@answerRecord,@precolIdex)		
		set @Value=SUBSTRING(@answerRecord,@precolIdex, @colIdex-@precolIdex)
		
		set @precolIdex=@colIdex+1
		set @colIdex=CHARINDEX(@col_sep,@answerRecord,@precolIdex)	
		set @CommmetAltId=SUBSTRING(@answerRecord,@precolIdex,@colIdex-@precolIdex)			
		set @Comment=STUFF(@answerRecord,1,@colIdex,'')
	
		if(len(@ValueAltId)>0 )
		begin
		insert INTO @tlValues(qId,altId,value)
		VALUES (@questionId,@ValueAltId,@Value)	
		end
		
		if(len(@CommmetAltId)>0 )
		begin
		insert INTO @tlComments(qId,altId,comment)
		VALUES (@questionId,@CommmetAltId,@Comment)	
		end
	end
	set @preRowIndex=@rowIndex+1;
	set @rowIndex=CHARINDEX(@row_sep,@answervalues,@preRowIndex)
	end
	
	IF @@ERROR <> 0 AND @@TRANCOUNT > 0
	BEGIN
	SET @Err = @@ERROR
	ROLLBACK TRANSACTION
	GOTO Finish
	END
end

--Delete old anser of values (except from comment answer)
delete a
FROM Answer a
inner join @tlValues t ON t.qId=a.QuestionID 
where a.ResultID=@resultId and a.ItemID=@itemId and  a.value>0
IF @@ERROR <> 0 AND @@TRANCOUNT > 0
	BEGIN
	SET @Err = @@ERROR
	ROLLBACK TRANSACTION
	GOTO Finish
	END
-- Insert new answers for new values
insert INTO Answer(ResultID,QuestionID,AlternativeID,Value,Free,ItemID)
select @resultId, t.qId,t.altId,t.value,'',@itemId
from @tlValues t
IF @@ERROR <> 0 AND @@TRANCOUNT > 0
	BEGIN
	SET @Err = @@ERROR
	ROLLBACK TRANSACTION
	GOTO Finish
	END

--Update old answer of comments
update an
SET an.Free=t.comment
from Answer an
inner JOIN @tlComments t ON t.qId=an.QuestionID and an.AlternativeID=t.altId 
where an.ResultID=@resultId and an.ItemID=@itemId
IF @@ERROR <> 0 AND @@TRANCOUNT > 0
	BEGIN
	SET @Err = @@ERROR
	ROLLBACK TRANSACTION
	GOTO Finish
	END

--Inser new answer for new comments
insert INTO Answer(ResultID,QuestionID,AlternativeID,Value,Free,ItemID)
select @resultId, t.qId,t.altId,0,t.comment,@itemId
from @tlComments t
where NOT EXISTS(SELECT * FROM Answer an WHERE an.ResultID=@resultId AND an.QuestionID=t.qId
AND an.AlternativeID=t.altId AND an.ItemID=@itemId)
IF @@ERROR <> 0 AND @@TRANCOUNT > 0
	BEGIN
	SET @Err = @@ERROR
	ROLLBACK TRANSACTION
	GOTO Finish
	END

--Update metadata
if(@metadataId >0)
begin 
	IF(@altStuUpdatedId>0)--Update student last updated
	begin 
		declare @answerStuLastUpdatedId int
		SELECT @answerStuLastUpdatedId=a.AnswerID 
		from Answer a
		where a.ResultID=@resultId and a.QuestionID=@metadataId and a.AlternativeID= @altStuUpdatedId
		if(@answerStuLastUpdatedId is not null and @answerStuLastUpdatedId>0)
		begin
			update Answer 
			SET DateValue=GETDATE()
			where AnswerID=@answerStuLastUpdatedId
		end
		else
		begin
			insert Answer(ResultID,QuestionID,AlternativeID,Value,DateValue,Free)
			VALUES(@resultId, @metadataId, @altStuUpdatedId,0,GETDATE(),'')
		end
		IF @@ERROR <> 0 AND @@TRANCOUNT > 0
		BEGIN
		SET @Err = @@ERROR
		ROLLBACK TRANSACTION
		GOTO Finish
		END

	end	
	IF(@altStuStatusId>0)--Update student status
	begin 
		declare @answerStuStatusId int
		SELECT @answerStuStatusId=a.AnswerID 
		from Answer a
		where a.ResultID=@resultId and a.QuestionID=@metadataId and a.AlternativeID= @altStuStatusId
		if(@answerStuStatusId is not null and @answerStuStatusId>0)
		begin
			update Answer 
			SET Value=@stuStatusValue
			where AnswerID=@answerStuStatusId
		end
		else
		begin
			insert Answer(ResultID,QuestionID,AlternativeID,Value,Free)
			VALUES(@resultId, @metadataId, @answerStuStatusId,@stuStatusValue,'')
		end
		IF @@ERROR <> 0 AND @@TRANCOUNT > 0
		BEGIN
		SET @Err = @@ERROR
		ROLLBACK TRANSACTION
		GOTO Finish
		END

	end			
end

END
COMMIT TRANSACTION
Finish:
	RETURN @Err
END


GO
