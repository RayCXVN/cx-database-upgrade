SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[prop].[prc_PropValue_Get_All_By_TableTypeID_And_ItemID]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [prop].[prc_PropValue_Get_All_By_TableTypeID_And_ItemID] AS' 
END
GO

ALTER PROCEDURE [prop].[prc_PropValue_Get_All_By_TableTypeID_And_ItemID]
(
	@TableTypeID int,
	@ItemID int
)
AS
BEGIN
	SET NOCOUNT ON
	DECLARE @Err Int

	SELECT
		pv.[PropValueID],
		pv.[PropertyID],
		ISNULL(pv.[PropOptionID], 0) AS 'PropOptionID',
		ISNULL(pv.[PropFileID], 0) AS 'PropFileID',
		pv.[No],
		pv.[Created],
		ISNULL(pv.[CreatedBy], 0) AS 'CreatedBy',
		pv.[Updated],
		ISNULL(pv.[UpdatedBy], 0) AS 'UpdatedBy',
		pv.[Value]
	FROM 
		[prop].[PropPage] pp 
		INNER JOIN [prop].[prop] p ON p.PropPageID = pp.PropPageID
		INNER JOIN [prop].PropValue pv ON pv.PropertyID = p.PropertyId
			AND pv.ItemID = @ItemID
    WHERE pp.TableTypeID = @TableTypeID
	Set @Err = @@Error

	RETURN @Err
END


GO
