SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[org].[prc_H_D_GetHDIdsHaveChildren]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [org].[prc_H_D_GetHDIdsHaveChildren] AS' 
END
GO
ALTER PROCEDURE [org].[prc_H_D_GetHDIdsHaveChildren]
(
	@HDIds varchar(max)
)
AS
BEGIN
	SET NOCOUNT ON
	DECLARE @Err Int	
	
	Declare @tblHDIds table (HDID int)
    DECLARE @ActiveEntityStatusID INT = (SELECT EntityStatusID FROM EntityStatus WHERE CodeName='Active')
	
	Insert INTO @tblHDIds(HDID)
	Select Value from dbo.funcListToTableInt(@HDIds, ',')
	
	Select tbl.HDID 
	From @tblHDIds tbl
	Where Exists(SELECT TOP 1 * 
				FROM org.H_D hd LEFT JOIN org.Department d ON hd.DepartmentID = d.DepartmentID
				WHERE hd.ParentID = tbl.HDID AND hd.Deleted = 0 
				AND (d.DepartmentID IS NULL OR d.EntityStatusID = @ActiveEntityStatusID AND d.Deleted IS NULL))
		
	Set @Err = @@Error

	RETURN @Err
END

GO
