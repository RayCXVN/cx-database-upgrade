SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[at].[prc_ActivityView_Q_upd]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [at].[prc_ActivityView_Q_upd] AS' 
END
GO
ALTER PROCEDURE [at].[prc_ActivityView_Q_upd]  
(  
 @ActivityViewQID int,  
 @ActivityViewID int,  
 @CategoryID int = null,  
 @QuestionID int = null,  
 @AlternativeID int = null,  
 @SectionID int = null,  
 @No smallint,  
 @cUserid int,  
 @Log smallint = 1,
 @ItemID int = NULL
)  
AS  
BEGIN  
 SET NOCOUNT ON  
 DECLARE @Err Int  
  
 UPDATE [at].[ActivityView_Q]  
 SET  
  [ActivityViewID] = @ActivityViewID,  
  [CategoryID] = @CategoryID,  
  [QuestionID] = @QuestionID,  
  [AlternativeID] = @AlternativeID,  
  [SectionID] = @SectionID,  
  [No] = @No,
  [ItemID] = @ItemID
 WHERE  
  [ActivityViewQID] = @ActivityViewQID  
  
 IF @Log = 1   
 BEGIN   
  INSERT INTO [Log].[AuditLog] ( UserId, TableName, Type, Data, Created)   
  SELECT @cUserid,'ActivityView_Q',1,  
  ( SELECT * FROM [at].[ActivityView_Q]   
   WHERE  
   [ActivityViewQID] = @ActivityViewQID    FOR XML AUTO) as data,  
   getdate()  
 END  
  
 Set @Err = @@Error  
  
 RETURN @Err  
END  
    

GO
