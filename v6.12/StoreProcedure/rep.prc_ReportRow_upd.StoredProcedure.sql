SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[rep].[prc_ReportRow_upd]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [rep].[prc_ReportRow_upd] AS' 
END
GO

ALTER PROCEDURE [rep].[prc_ReportRow_upd]
(
	@ReportRowID int,
	@ReportPartID int,
	@ReportRowTypeID int,
	@No smallint,
	@CssClass varchar(64),
	@cUserid int,
	@Log smallint = 1
)
AS
BEGIN
	SET NOCOUNT ON
	DECLARE @Err Int

	UPDATE [rep].[ReportRow]
	SET
		[ReportPartID] = @ReportPartID,
		[ReportRowTypeID] = @ReportRowTypeID,
		[No] = @No,
		[CssClass] = @CssClass
	WHERE
		[ReportRowID] = @ReportRowID

	IF @Log = 1 
	BEGIN 
		INSERT INTO [Log].[AuditLog] ( UserId, TableName, Type, Data, Created) 
		SELECT @cUserid,'ReportRow',1,
		( SELECT * FROM [rep].[ReportRow] 
			WHERE
			[ReportRowID] = @ReportRowID			 FOR XML AUTO) as data,
			getdate()
	END

	Set @Err = @@Error

	RETURN @Err
END


GO
