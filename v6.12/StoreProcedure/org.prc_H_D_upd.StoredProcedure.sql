SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[org].[prc_H_D_upd]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [org].[prc_H_D_upd] AS' 
END
GO
ALTER PROCEDURE [org].[prc_H_D_upd]
(
	@HDID int,
	@HierarchyID int,
	@DepartmentID int,
	@ParentID INT=NULL,
	@Path nvarchar(900),
	@PathName nvarchar(4000),
	@Deleted smallint,
	@cUserid int,
	@Log smallint = 1
)
AS
BEGIN
	SET NOCOUNT ON
	DECLARE @Err Int

	UPDATE [org].[H_D]
	SET
		[HierarchyID] = @HierarchyID,
		[DepartmentID] = @DepartmentID,
		[ParentID] = @ParentID,
		[Path] = @Path,
		[PathName] = @PathName,
		[Deleted] = @Deleted
	WHERE
		[HDID] = @HDID

	IF @Log = 1 
	BEGIN 
		INSERT INTO [Log].[AuditLog] ( UserId, TableName, Type, Data, Created) 
		SELECT @cUserid,'H_D',1,
		( SELECT * FROM [org].[H_D] 
			WHERE
			[HDID] = @HDID			 FOR XML AUTO) as data,
			getdate()
	END

	Set @Err = @@Error

	RETURN @Err
END

GO
