SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[form].[prc_FormCell_upd]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [form].[prc_FormCell_upd] AS' 
END
GO
/*
	2016-12-05 Sarah - Increase Form.CssClass from 256 to 4000, FormCell.CssClass from 128 to 4000
*/
ALTER PROCEDURE [form].[prc_FormCell_upd]
(
	@FormCellID int,
	@FormRowID int,
	@No smallint,
	@CellTypeID smallint,
	@FormFieldID INT=NULL,
	@FormCommandID INT=NULL,
	@CssClass nvarchar(4000),
	@Alignment smallint,
	@Colspan smallint,
	@Width smallint,
	@cUserid int,
	@Log smallint = 1
)
AS
BEGIN
	SET NOCOUNT ON
	DECLARE @Err Int

	UPDATE [form].[FormCell]
	SET
		[FormRowID] = @FormRowID,
		[No] = @No,
		[CellTypeID] = @CellTypeID,
		[FormFieldID] = @FormFieldID,
		[FormCommandID] = @FormCommandID,
		[CssClass] = @CssClass,
		[Alignment] = @Alignment,
		[Colspan] = @Colspan,
		[Width] = @Width
	WHERE
		[FormCellID] = @FormCellID

	IF @Log = 1 
	BEGIN 
		INSERT INTO [Log].[AuditLog] ( UserId, TableName, Type, Data, Created) 
		SELECT @cUserid,'FormCell',1,
		( SELECT * FROM [form].[FormCell] 
			WHERE
			[FormCellID] = @FormCellID			 FOR XML AUTO) as data,
			getdate()
	END

	Set @Err = @@Error

	RETURN @Err
END


GO
