SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[at].[prc_OwnerColor_upd]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [at].[prc_OwnerColor_upd] AS' 
END
GO

ALTER PROCEDURE [at].[prc_OwnerColor_upd]
(
	@OwnerColorID int,
	@ThemeID INT=NULL,
	@OwnerID INT=NULL,
	@CustomerID INT=NULL,
	@Friendlyname nvarchar(64),
	@FillColor nvarchar(16),
	@TextColor nvarchar(16),
	@BorderColor nvarchar(16),
	@No int,
	@cUserid int,
	@Log smallint = 1
)
AS
BEGIN
	SET NOCOUNT ON
	DECLARE @Err Int

	UPDATE [at].[OwnerColor]
	SET
	    [ThemeID] = @ThemeID,
		[OwnerID] = @OwnerID,
		[CustomerID] = @CustomerID,
		[Friendlyname] = @Friendlyname,
		[FillColor] = @FillColor,
		[TextColor] = @TextColor,
		[BorderColor] = @BorderColor,
		[No] = @No
	WHERE
		[OwnerColorID] = @OwnerColorID

	IF @Log = 1 
	BEGIN 
	   INSERT INTO [Log].[AuditLog] ( UserId, TableName, Type, Data, Created) 
		SELECT @cUserid,'OwnerColor',1,
		( SELECT * FROM [at].[OwnerColor] 
			WHERE
			[OwnerColorID] = @OwnerColorID			 FOR XML AUTO) as data,
			getdate()
	END

	Set @Err = @@Error

	RETURN @Err
END


GO
