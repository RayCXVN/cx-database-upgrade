SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[RetrieveCountryByIPNumber]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [dbo].[RetrieveCountryByIPNumber] AS' 
END
GO


ALTER PROCEDURE [dbo].[RetrieveCountryByIPNumber]
(
	@IPNumber	numeric
)
AS
BEGIN
	SET NOCOUNT ON
	DECLARE @Err Int

	SELECT
	[Country_Code_2],
	[Country_Code_3],
	[Country_Name]
	FROM [dbo].[IP_Country]
	WHERE @IPNumber between IP_From AND IP_To

	Set @Err = @@Error

	RETURN @Err
END


GO
