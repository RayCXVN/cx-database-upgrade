SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[at].[prc_A_CalcType_upd]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [at].[prc_A_CalcType_upd] AS' 
END
GO



ALTER PROCEDURE [at].[prc_A_CalcType_upd]
(
	@ACALCTYPEID int,
	@ReportCalcTypeID int,
	@ActivityID int,
	@No smallint,
	@DefaultSelected smallint = 0,
	@Format nvarchar(32) = '',
	@cUserid int,
	@Log smallint = 1
)
AS
BEGIN
	SET NOCOUNT ON
	DECLARE @Err Int

	UPDATE [at].[A_CalcType]
	SET
		[ReportCalcTypeID] = @ReportCalcTypeID,
		[ActivityID] = @ActivityID,
		[No] = @No,
		[DefaultSelected] = @DefaultSelected,
		[Format] = @Format
	WHERE
		[ACALCTYPEID] = @ACALCTYPEID

	IF @Log = 1 
	BEGIN 
		INSERT INTO [Log].[AuditLog] ( UserId, TableName, Type, Data, Created) 
		SELECT @cUserid,'A_CalcType',1,
		( SELECT * FROM [at].[A_CalcType] 
			WHERE
			[ACALCTYPEID] = @ACALCTYPEID			 FOR XML AUTO) as data,
			getdate()
	END

	Set @Err = @@Error

	RETURN @Err
END




GO
