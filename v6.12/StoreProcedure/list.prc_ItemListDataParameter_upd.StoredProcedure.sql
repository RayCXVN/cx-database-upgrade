SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[list].[prc_ItemListDataParameter_upd]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [list].[prc_ItemListDataParameter_upd] AS' 
END
GO

-- =============================================
ALTER PROCEDURE [list].[prc_ItemListDataParameter_upd]
	@ItemListDataParameterID int,
	@ItemListDataSourceID int,
    @DataType nvarchar(32),
    @Type nvarchar(32),
    @Mandatory bit,
    @DefaultValue nvarchar(256),
    @ConstructionPattern nvarchar(64),
    @cUserid int,
    @Log smallint = 1
AS
BEGIN
	SET NOCOUNT ON;
	DECLARE @Err Int
	
    UPDATE [list].[ItemListDataParameter]
    SET 
		[ItemListDataSourceID] = @ItemListDataSourceID,
        [DataType] = @DataType,
        [Type] = @Type,
        [Mandatory] = @Mandatory,
        [DefaultValue] = @DefaultValue,
        [ConstructionPattern] = @ConstructionPattern
     WHERE 
		[ItemListDataParameterID] = @ItemListDataParameterID
		
	IF @Log = 1 
	BEGIN 
		INSERT INTO [Log].[AuditLog] ( UserId, TableName, Type, Data, Created) 
		SELECT @cUserid,'ItemListDataParameter',1,
		( SELECT * FROM [list].[ItemListDataParameter] 
			WHERE
			[ItemListDataParameterID] = @ItemListDataParameterID			 FOR XML AUTO) as data,
			getdate()
	END

	Set @Err = @@Error

	RETURN @Err	
END


GO
