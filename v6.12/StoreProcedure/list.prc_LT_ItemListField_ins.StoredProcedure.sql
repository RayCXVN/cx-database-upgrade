SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[list].[prc_LT_ItemListField_ins]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [list].[prc_LT_ItemListField_ins] AS' 
END
GO
ALTER PROCEDURE [list].[prc_LT_ItemListField_ins]
(
	@LanguageID int,
	@ItemListFieldID int,
	@Name nvarchar(256),
	@Description nvarchar(max),
	@HeaderText nvarchar(max),
	@HeaderGroupText nvarchar(256),
	@HeaderTooltipText nvarchar(512),
	@ItemTooltipText nvarchar(512),
	@ItemText nvarchar(max),
	@ExpandingItemText nvarchar(max),
    @DefaultValues nvarchar(128),
    @AvailableValues nvarchar(128),
    @cUserid int,
    @Log smallint = 1
)    
AS
BEGIN
	SET NOCOUNT ON;
	DECLARE @Err Int

	INSERT INTO [list].[LT_ItemListField]
           ([LanguageID]
           ,[ItemListFieldID]
           ,[Name]
           ,[Description]
           ,[HeaderText]
           ,[HeaderGroupText]
           ,[HeaderTooltipText]
           ,[ItemTooltipText]
           ,[ItemText]
           ,[ExpandingItemText]
           ,[DefaultValues]
           ,[AvailableValues])
     VALUES
           (@LanguageID
           ,@ItemListFieldID
           ,@Name
           ,@Description
           ,@HeaderText
           ,@HeaderGroupText
           ,@HeaderTooltipText
           ,@ItemTooltipText
           ,@ItemText
           ,@ExpandingItemText
           ,@DefaultValues
           ,@AvailableValues)

     Set @Err = @@Error
	 IF @Log = 1 
	 BEGIN 
		INSERT INTO [Log].[AuditLog] ( UserId, TableName, Type, Data, Created) 
		SELECT @cUserid,'LT_ItemListField',0,
		( SELECT * FROM [list].[LT_ItemListField] 
			WHERE
			[LanguageID] = @LanguageID AND
			[ItemListFieldID] = @ItemListFieldID				 FOR XML AUTO) as data,
				getdate() 
	  END


	 RETURN @Err
END

GO
