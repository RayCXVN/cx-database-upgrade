SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[mea].[prc_GetMeasureByCreatedOrResponsibleUsers]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [mea].[prc_GetMeasureByCreatedOrResponsibleUsers] AS' 
END
GO
ALTER PROCEDURE [mea].[prc_GetMeasureByCreatedOrResponsibleUsers]
(
	@UserIDList	   varchar(max) = ''
)
AS
BEGIN
	DECLARE @User TABLE (UserID INT)
	DECLARE @MeasureId INT

	CREATE TABLE #TABLE  (MeasureId INT ,MeasureTypeId INT,ParentId INT,CreatedBy INT,CreatedDate DATETIME,ChangedBy INT, ChangedDate DATETIME, Responsible INT,  StatusId INT, [Private] BIT, [Subject] nvarchar(256) , [Description] nvarchar(max),[Started] DATETIME, DueDate DATETIME, Completed DATETIME, CopyFrom INT, Active BIT, Deleted BIT, MeasureTemplateID INT, CanEdit BIT)
	
	IF (@UserIDList IS NOT NULL AND @UserIDList <> '')
	BEGIN
		INSERT INTO @User (UserID) SELECT value FROM dbo.funcListToTableInt(@UserIDList,',')
		INSERT INTO #TABLE  SELECT m.MeasureId,m.MeasureTypeId, m.ParentId,m.CreatedBy,m.CreatedDate,m.ChangedBy, m.ChangedDate, m.Responsible, m.StatusId, m.Private, m.Subject, m.Description,m.Started, m.DueDate, m.Completed, m.CopyFrom, m.Active, m.Deleted, m.MeasureTemplateID, m.CanEdit
		   FROM mea.Measure m
		   WHERE m.Deleted = 0 AND m.Active = 1  
		   AND EXISTS (SELECT 1 FROM @User u WHERE u.UserID = m.CreatedBy OR u.UserID = m.Responsible)
	   
		DECLARE c_measure CURSOR READ_ONLY FOR SELECT MeasureId FROM #TABLE
		OPEN c_measure
		FETCH NEXT FROM c_measure INTO @MeasureId
			WHILE @@FETCH_STATUS=0
			BEGIN
			WITH RecQry AS
					(
						SELECT m.MeasureId 
						  FROM mea.Measure m
						 WHERE m.ParentId =@MeasureId
						UNION ALL
						SELECT a.MeasureId
						  FROM mea.Measure a INNER JOIN RecQry b
							ON a.ParentID = b.MeasureId
					)
				UPDATE #TABLE SET Deleted =1 WHERE (ParentId =@MeasureId OR ParentId IN (SELECT MeasureId FROM RecQry )) AND Deleted =0
			FETCH NEXT FROM c_measure INTO @MeasureId
			END
		CLOSE c_measure
		DEALLOCATE c_measure
	END

	SELECT MeasureId, MeasureTypeId, ParentId, CreatedBy,CreatedDate,ChangedBy, ChangedDate, Responsible, StatusId, [Private], [Subject], [Description],[Started], [DueDate], [Completed], CopyFrom, Active, Deleted, MeasureTemplateID, CanEdit FROM #TABLE WHERE Deleted =0 --AND CopyFrom IS NULL
	DROP TABLE #TABLE
END

GO
