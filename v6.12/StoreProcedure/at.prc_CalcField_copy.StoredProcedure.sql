SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[at].[prc_CalcField_copy]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [at].[prc_CalcField_copy] AS' 
END
GO
ALTER PROC [at].[prc_CalcField_copy]    
(     
 @CFID INT,    
 @NewCFID  INT = NULL OUTPUT,  
 @cUserid int,      
 @Log smallint = 1    
)    
AS    
BEGIN    
DECLARE @Err Int      
SET NOCOUNT ON    
SET XACT_ABORT ON    
BEGIN TRANSACTION    
  
DECLARE @ActivityID AS INT   
SELECT @ActivityID = ActivityID   
FROM [AT].[CalcField]    
WHERE [CFID] = @CFID    
  
INSERT INTO [AT].[CalcField]      
 (      
  [ActivityID],      
  [Name],      
  [Formula],      
  [Format],      
  [AllParametersMustExist]  ,
  [Type]  
 )   
 SELECT [ActivityID],      
  [Name],      
  [Formula],      
  [Format],      
  [AllParametersMustExist],
  [Type]    
  FROM   [AT].[CalcField]    
  WHERE [CFID] = @CFID    
   
   
 Set @Err = @@Error      
 Set @NewCFID = scope_identity()       
   
  IF @Log = 1       
 BEGIN       
  INSERT INTO [Log].[AuditLog] ( UserId, TableName, Type, Data, Created)       
  SELECT @cUserid,'CalcField',0,      
  ( SELECT * FROM [at].[CalcField]       
   WHERE      
   [CFID] = @CFID     FOR XML AUTO) as data,      
    getdate()       
  END      
       
  
   --CalcFieldParameter    
   INSERT INTO AT.CalcParameter(CFID, [No], Name, QuestionID, AlternativeID, CategoryID, CalcType, Format, Created)    
   SELECT  
 @NewCFID,  
 [No],  
 [Name],  
 [QuestionID],  
 [AlternativeID],  
 [CategoryID],  
 [CalcType],  
 [Format],  
 GETDATE()  
 FROM [at].[CalcParameter]  
 WHERE  
 [CFID] = @CFID  
 ORDER BY [No]  
   
 DECLARE @CurrentNo int  
  
 SELECT @CurrentNo = MAX(No)   
 FROM [at].[QA_Calc],  
 [at].[CalcField]  
 where [at].[QA_Calc].CFID= [at].[CalcField].CFID  
 and [at].[CalcField].ActivityID=@ActivityID  
  
 --QA_Calc 
    
 INSERT INTO AT.QA_Calc(QuestionID, AlternativeID, CFID, MinValue, MaxValue, [No], NewValue, UseNewValue, Created, UseCalculatedValue)    
 SELECT  
 qa.[QuestionID],  
 qa.[AlternativeID],  
 @NewCFID,  
 qa.[MinValue],  
 qa.[MaxValue],  
 ROW_NUMBER() Over(Order by qa.NO)  + @CurrentNo ,  
 qa.[NewValue],  
 qa.[UseNewValue],  
 GETDATE(),  
 qa.[UseCalculatedValue]  
 FROM [at].[QA_Calc] qa  
 WHERE  
 [CFID] = @CFID  
 ORDER BY [No]  
  
COMMIT TRANSACTION       
END 


GO
