SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[at].[prc_SentMailFiles_get]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [at].[prc_SentMailFiles_get] AS' 
END
GO

ALTER PROCEDURE [at].[prc_SentMailFiles_get]
(
	@SentMailID int
)
AS
BEGIN
	SET NOCOUNT ON
	DECLARE @Err Int

	SELECT
	[SentMailFileID],
	[MIMEType],
	[Data],
	[Created],
	[Description],
	[Size],
	ISNULL([Filename], '') AS 'Filename',
	ISNULL([SentMailID], 0) AS 'SentMailID'
	FROM [at].[SentMailFiles]
	WHERE
	[SentMailID] = @SentMailID

	Set @Err = @@Error

	RETURN @Err
END


GO
