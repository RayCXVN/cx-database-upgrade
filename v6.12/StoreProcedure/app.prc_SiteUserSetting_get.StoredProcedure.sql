SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[app].[prc_SiteUserSetting_get]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [app].[prc_SiteUserSetting_get] AS' 
END
GO
ALTER PROCEDURE [app].[prc_SiteUserSetting_get]      
 @SiteID int,
 @UserID int
AS      
BEGIN      
 SET NOCOUNT ON;      
 DECLARE @Err Int          
 SELECT       
	 [SiteUserSettingID]
     ,[UserID]
     ,[SiteID]
     ,[EditMode]
     ,[AutoFitScreen]
     ,[ScreenResolution]
     ,[ExportTooltip]
 FROM             
  [app].[SiteUserSetting]      
 WHERE        
  [SiteUserSetting].SiteID = @SiteID      
  AND [SiteUserSetting].[UserID] = @UserID
 Set @Err = @@Error      
      
 RETURN @Err      
END 

GO
