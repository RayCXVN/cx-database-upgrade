SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[prc_Job_getByOwnerId]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [dbo].[prc_Job_getByOwnerId] AS' 
END
GO
ALTER PROCEDURE [dbo].[prc_Job_getByOwnerId]  
(  
 @OwnerID smallint = 0  
)  
AS  
BEGIN  
 SET NOCOUNT ON  
 DECLARE @Err Int  

 SELECT  
 [JobID],  
 [JobTypeID],  
 [JobStatusID],  
 [OwnerID],  
 [UserID],  
 [Name],  
 [Priority],  
 ISNULL([Option], 0) AS 'Option',  
 [Created],  
 [StartDate],  
 ISNULL([EndDate], '1900-01-01') AS 'EndDate',  
 [Description]  
 FROM [job].[Job]  
 WHERE  
 [OwnerID] = @OwnerID  

 Set @Err = @@Error  

 RETURN @Err  
END  


GO
