SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[at].[prc_Choice_get]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [at].[prc_Choice_get] AS' 
END
GO
ALTER PROCEDURE [at].[prc_Choice_get] (
	@SurveyID INT = NULL
	,@ActivityID INT = NULL
	)
AS
BEGIN
	SET NOCOUNT ON

	DECLARE @Err INT

	SELECT [ChoiceID]
		,ISNULL([ActivityID], 0) AS 'ActivityID'
		,ISNULL([SurveyID], 0) AS 'SurveyID'
		,ISNULL([RoleID], 0) AS 'RoleID'
		,ISNULL([DepartmentID], 0) AS 'DepartmentID'
		,ISNULL([HDID], 0) AS 'HDID'
		,ISNULL([DepartmentTypeID], 0) AS 'DepartmentTypeID'
		,ISNULL([UserGroupID], 0) AS 'UserGroupID'
		,ISNULL([UserTypeID], 0) AS 'UserTypeID'
		,[Name]
		,[No]
		,[Type]
		,[Created]
	FROM [at].[Choice]
	WHERE (
			[SurveyID] = @SurveyID
			AND @ActivityID IS NULL
			)
		OR (
			[ActivityID] = @ActivityID
			AND @SurveyID IS NULL
			)
	ORDER BY [No]

	SET @Err = @@Error

	RETURN @Err
END

GO
