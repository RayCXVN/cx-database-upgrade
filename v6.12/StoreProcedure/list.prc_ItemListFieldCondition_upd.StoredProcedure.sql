SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[list].[prc_ItemListFieldCondition_upd]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [list].[prc_ItemListFieldCondition_upd] AS' 
END
GO
ALTER PROCEDURE [list].[prc_ItemListFieldCondition_upd]
	@ItemListFieldConditionID int,
	@ItemListFieldID int,
	@Type nvarchar(32),
	@Param nvarchar(MAX),
	@Value nvarchar(32),
    @cUserid int,
    @Log smallint = 1
AS
BEGIN
	SET NOCOUNT ON;
	DECLARE @Err Int
	
    UPDATE [list].[ItemListFieldCondition]
    SET 
		[ItemListFieldID] = @ItemListFieldID,
        [Type] = @Type,
        [Param] = @Param,
        [Value] = @Value
     WHERE 
		[ItemListFieldConditionID] = @ItemListFieldConditionID
		
	IF @Log = 1 
	BEGIN 
		INSERT INTO [Log].[AuditLog] ( UserId, TableName, Type, Data, Created) 
		SELECT @cUserid,'ItemListFieldCondition',1,
		( SELECT * FROM [list].[ItemListFieldCondition] 
			WHERE
			[ItemListFieldConditionID] = @ItemListFieldConditionID			 FOR XML AUTO) as data,
			getdate()
	END

	Set @Err = @@Error

	RETURN @Err	
END

GO
