SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[app].[prc_LT_LoginService_get]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [app].[prc_LT_LoginService_get] AS' 
END
GO

ALTER PROCEDURE [app].[prc_LT_LoginService_get]  
(  
 @LoginServiceID int  
)  
AS  
BEGIN  
 SET NOCOUNT ON  
 DECLARE @Err Int  
  
 SELECT  
 [LanguageID]  
      ,[LoginServiceID]  
      ,[Name]  
      ,[Description]  
      ,[ToolTip]  
 FROM [app].[LT_LoginService]  
 WHERE  
 [LoginServiceID] = @LoginServiceID  
  
 Set @Err = @@Error  
  
 RETURN @Err  
END  


GO
