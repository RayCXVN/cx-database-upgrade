SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[prop].[prc_PropOption_upd]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [prop].[prc_PropOption_upd] AS' 
END
GO

ALTER PROCEDURE [prop].[prc_PropOption_upd]
(
	@PropOptionID int,
	@PropertyID int,
	@Value float,
	@cUserid int,
	@Log smallint = 1
)
AS
BEGIN
	SET NOCOUNT ON
	DECLARE @Err Int

	UPDATE [prop].[PropOption]
	SET
		[PropertyID] = @PropertyID,
		[Value] = @Value
	WHERE
		[PropOptionID] = @PropOptionID

	IF @Log = 1 
	BEGIN 
		INSERT INTO [Log].[AuditLog] ( UserId, TableName, Type, Data, Created) 
		SELECT @cUserid,'PropOption',1,
		( SELECT * FROM [prop].[PropOption] 
			WHERE
			[PropOptionID] = @PropOptionID			 FOR XML AUTO) as data,
			getdate()
	END

	Set @Err = @@Error

	RETURN @Err
END


GO
