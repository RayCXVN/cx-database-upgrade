SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[add_Owner]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [dbo].[add_Owner] AS' 
END
GO


-- add_owner 'TestKunde', 'cxdev2', 'AT5R_COMMON', 'cxdev3', 'AT5Q_TEST', 'noldus', '123'

ALTER proc [dbo].[add_Owner](
	@OwnerName nvarchar(256), 
	@ReportServer varchar(64), 
	@ReportDB varchar(64), 
	@OLAPServer varchar(64), 
	@OLAPDB varchar(64), 
	@Username varchar(128), 
	@Password varchar(64)
) AS
BEGIN
	DECLARE @Count			int,
			@OwnerID		int,
			@HierarchyID	int,
			@DepartmentID	int,
			@UserID			int,
			@UserTypeID		int
			
	SET @Count = 0
	SELECT @Count = Count(*) FROM org.[User] WHERE Username = @Username
	IF @Count > 0 
		PRINT 'Brukernavn er opptatt'
	ELSE
	BEGIN
		BEGIN TRANSACTION
		INSERT org.Owner(LanguageID, Name, ReportServer, ReportDB, OLAPServer, OLAPDB)
		VALUES(1, @OwnerName, @ReportServer, @ReportDB, @OLAPServer, @OLAPDB)
		SET @OwnerID = scope_identity()
		
		INSERT org.Hierarchy(OwnerID, Name) VALUES(@OwnerID, @OwnerName)
		SET @HierarchyID = scope_identity()
		
		INSERT org.Department(LanguageID, OwnerID, Name) VALUES(1, @OwnerID, @OwnerName)
		SET @DepartmentID = scope_identity()
		
		INSERT org.H_D(HierarchyID, DepartmentID, ParentID) VALUES(@HierarchyID, @DepartmentID, NULL)
		
		INSERT org.[User](LanguageID, Ownerid, DepartmentID, UserName, Password) VALUES(1, @OwnerID, @DepartmentID, @Username, dbo.EncryptString(@Password))
		SET @UserID = scope_identity()
		
		INSERT org.UserType(OwnerID) VALUES(@OwnerID)
		SET @UserTypeID = scope_identity()
		
		INSERT org.LT_UserType(LanguageID, UserTypeID, Name, Description) VALUES(1, @UserTypeID, 'Superadministrator', 'Conexus')
		
		INSERT org.UT_U VALUES(@UserTypeID, @UserID)
		
		INSERT app.UT_AccessArea(AccessAreaID, UserTypeID, OwnerID)		
		SELECT AccessAreaID, @UserTypeID, @OwnerID FROM app.AccessArea
		
		COMMIT TRANSACTION
	END


END

GO
