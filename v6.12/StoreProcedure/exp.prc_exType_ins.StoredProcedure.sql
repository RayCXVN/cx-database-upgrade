SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[exp].[prc_exType_ins]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [exp].[prc_exType_ins] AS' 
END
GO

ALTER PROCEDURE [exp].[prc_exType_ins]
(
	@TypeID smallint = null output,
	@Name nvarchar(256),
	@cUserid int,
	@Log smallint = 1
)
AS
BEGIN
	SET NOCOUNT ON
	DECLARE @Err Int

	INSERT INTO [exp].[exType]
	(
		[Name]
	)
	VALUES
	(
		@Name
	)

	Set @Err = @@Error
	Set @TypeID = scope_identity()

	IF @Log = 1 
	BEGIN 
		INSERT INTO [Log].[AuditLog] ( UserId, TableName, Type, Data, Created) 
		SELECT @cUserid,'exType',0,
		( SELECT * FROM [exp].[exType] 
			WHERE
			[TypeID] = @TypeID				 FOR XML AUTO) as data,
				getdate() 
	 END

	RETURN @Err
END


GO
