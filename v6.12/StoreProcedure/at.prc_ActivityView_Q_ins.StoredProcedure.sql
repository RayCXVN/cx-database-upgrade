SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[at].[prc_ActivityView_Q_ins]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [at].[prc_ActivityView_Q_ins] AS' 
END
GO
ALTER PROCEDURE [at].[prc_ActivityView_Q_ins]  
(  
 @ActivityViewQID int = null output,  
 @ActivityViewID int,  
 @CategoryID int = null,  
 @QuestionID int = null,  
 @AlternativeID int = null,  
 @SectionID int = null,  
 @No smallint,  
 @cUserid int,  
 @Log smallint = 1,
 @ItemID int = NULL
)  
AS  
BEGIN  
 SET NOCOUNT ON  
 DECLARE @Err Int  
  
 INSERT INTO [at].[ActivityView_Q]  
 (  
  [ActivityViewID],  
  [CategoryID],  
  [QuestionID],  
  [AlternativeID],  
  [SectionID],  
  [No],
  [ItemID]  
 )  
 VALUES  
 (  
  @ActivityViewID,  
  @CategoryID,  
  @QuestionID,  
  @AlternativeID,  
  @SectionID,  
  @No,
  @ItemID  
 )  
  
 Set @Err = @@Error  
 Set @ActivityViewQID = scope_identity()  
  
 IF @Log = 1   
 BEGIN   
  INSERT INTO [Log].[AuditLog] ( UserId, TableName, Type, Data, Created)   
  SELECT @cUserid,'ActivityView_Q',0,  
  ( SELECT * FROM [at].[ActivityView_Q]   
   WHERE  
   [ActivityViewQID] = @ActivityViewQID     FOR XML AUTO) as data,  
    getdate()   
  END  
  
 RETURN @Err  
END  

GO
