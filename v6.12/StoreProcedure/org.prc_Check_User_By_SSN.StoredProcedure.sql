SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[org].[prc_Check_User_By_SSN]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [org].[prc_Check_User_By_SSN] AS' 
END
GO
/*  2016-10-10 Ray: Remove column Status from org.User, org.Department, RDB.dbo.Result
*/
ALTER PROCEDURE [org].[prc_Check_User_By_SSN]
(
  @SSN nvarchar(64)
 )
AS
BEGIN  
  DECLARE @ActiveEntityStatusID INT = (SELECT EntityStatusID FROM EntityStatus WHERE CodeName='Active')

  Select UserID, Ownerid, DepartmentID, LanguageID, isnull(RoleID,0) 'RoleID', UserName, Password, LastName, FirstName, Email, Mobile, ExtID, 
    SSN, Tag, Locked, ChangePassword, Created, [HashPassword], [SaltPassword],	[OneTimePassword], [OTPExpireTime], EntityStatusID,
    Deleted, EntityStatusReasonID
  FROM org.[User]
  WHERE SSN = @SSN  and EntityStatusID = @ActiveEntityStatusID AND Deleted IS NULL

END 

GO
