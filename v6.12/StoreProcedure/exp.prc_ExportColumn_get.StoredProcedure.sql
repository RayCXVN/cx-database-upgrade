SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[exp].[prc_ExportColumn_get]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [exp].[prc_ExportColumn_get] AS' 
END
GO
ALTER PROCEDURE [exp].[prc_ExportColumn_get]
AS
BEGIN
	SET NOCOUNT ON
	DECLARE @Err Int

	SELECT
	[ExportColumnID],
	[TableTypeID],
	[ColumnName],
	[DataType]
	FROM [exp].[ExportColumn]

	Set @Err = @@Error

	RETURN @Err
END

GO
