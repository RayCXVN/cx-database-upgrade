SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[at].[prc_LT_Section_get]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [at].[prc_LT_Section_get] AS' 
END
GO

ALTER PROCEDURE [at].[prc_LT_Section_get]
(
	@SectionID int
)
AS
BEGIN
	SET NOCOUNT ON
	DECLARE @Err Int

	SELECT
	[LanguageID],
	[SectionID],
	[Name],
	[Title],
	[Description]
	FROM [at].[LT_Section]
	WHERE
	[SectionID] = @SectionID

	Set @Err = @@Error

	RETURN @Err
END


GO
