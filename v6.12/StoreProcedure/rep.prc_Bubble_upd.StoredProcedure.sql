SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[rep].[prc_Bubble_upd]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [rep].[prc_Bubble_upd] AS' 
END
GO

ALTER PROCEDURE [rep].[prc_Bubble_upd]
(
	@BubbleID int,
	@OwnerID int,
	@ActivityID int,
	@No smallint,
	@Status smallint,
	@Created datetime,
	@cUserid int,
	@Log smallint = 1
)
AS
BEGIN
	SET NOCOUNT ON
	DECLARE @Err Int

	UPDATE [rep].[Bubble]
	SET
		[OwnerID] = @OwnerID,
		[ActivityID] = @ActivityID,
		[No] = @No,
		[Status] = @Status,
		[Created] = @Created
	WHERE
		[BubbleID] = @BubbleID

	IF @Log = 1 
	BEGIN 
		INSERT INTO [Log].[AuditLog] ( UserId, TableName, Type, Data, Created) 
		SELECT @cUserid,'Bubble',1,
		( SELECT * FROM [rep].[Bubble] 
			WHERE
			[BubbleID] = @BubbleID			 FOR XML AUTO) as data,
			getdate()
	END

	Set @Err = @@Error

	RETURN @Err
END


GO
