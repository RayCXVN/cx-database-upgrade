SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[at].[prc_A_S_ins]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [at].[prc_A_S_ins] AS' 
END
GO
ALTER PROCEDURE [at].[prc_A_S_ins] (
	@ASID INT = NULL OUTPUT,
	@ActivityID INT,
	@StatusTypeID INT,
	@No SMALLINT,
	@cUserid INT,
	@Log SMALLINT = 1
	)
AS
BEGIN
	SET NOCOUNT ON

	DECLARE @Err INT

	INSERT INTO [at].[A_S] (
		[ActivityID],
		[StatusTypeID],
		[No]
		)
	VALUES (
		@ActivityID,
		@StatusTypeID,
		@No
		)

	SET @Err = @@Error
	SET @ASID = scope_identity()

	IF @Log = 1
	BEGIN
		INSERT INTO [Log].[AuditLog] (
			UserId,
			TableName,
			Type,
			Data,
			Created
			)
		SELECT @cUserid,
			'A_S',
			0,
			(
				SELECT *
				FROM [at].[A_S]
				WHERE [ActivityID] = @ActivityID
					AND [StatusTypeID] = @StatusTypeID
				FOR XML AUTO
				) AS data,
			getdate()
	END

	RETURN @Err
END


GO
