SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[list].[prc_ItemListDataParameter_ins]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [list].[prc_ItemListDataParameter_ins] AS' 
END
GO
ALTER PROCEDURE [list].[prc_ItemListDataParameter_ins]
	@ItemListDataParameterID int = null output,
	@ItemListDataSourceID int,
    @DataType nvarchar(32),
    @Type nvarchar(32),
    @Mandatory bit,
    @DefaultValue nvarchar(256),
    @ConstructionPattern nvarchar(64),
    @cUserid int,
    @Log smallint = 1
AS
BEGIN
	SET NOCOUNT ON;
	DECLARE @Err Int
	
	INSERT INTO [list].[ItemListDataParameter]
           ([ItemListDataSourceID]
           ,[DataType]
           ,[Type]
           ,[Mandatory]
           ,[DefaultValue]
           ,[ConstructionPattern])
     VALUES
           (@ItemListDataSourceID
           ,@DataType
           ,@Type
           ,@Mandatory
           ,@DefaultValue
           ,@ConstructionPattern)
           
     Set @Err = @@Error
	 Set @ItemListDataParameterID = scope_identity()
	 IF @Log = 1 
	 BEGIN 
		INSERT INTO [Log].[AuditLog] ( UserId, TableName, Type, Data, Created) 
		SELECT @cUserid,'ItemListDataParameter',0,
		( SELECT * FROM [list].[ItemListDataParameter]
			WHERE
			[ItemListDataParameterID] = @ItemListDataParameterID				 FOR XML AUTO) as data,
				getdate() 
	 END

	
	 RETURN @Err
END

GO
