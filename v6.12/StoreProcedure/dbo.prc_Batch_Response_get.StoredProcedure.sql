SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[prc_Batch_Response_get]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [dbo].[prc_Batch_Response_get] AS' 
END
GO
ALTER proc [dbo].[prc_Batch_Response_get]
(
    @BatchId int, 
	@Invited	int = 0 OUTPUT,
	@Finished	int = 0 OUTPUT
)
AS
BEGIN
    DECLARE @ActiveEntityStatusID INT = (SELECT EntityStatusID FROM EntityStatus WHERE CodeName='Active')

	SELECT @Invited = COUNT(*), @Finished = COUNT(Enddate) 
	FROM Result
	WHERE BatchId =  @BatchId and EntityStatusID = @ActiveEntityStatusID AND Deleted IS NULL
END
---------------------------------------------------------------------


GO
