SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[at].[prc_StatusActionType_ins]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [at].[prc_StatusActionType_ins] AS' 
END
GO

ALTER PROCEDURE [at].[prc_StatusActionType_ins]
(
	@StatusActionTypeID int = null output,
	@Name nvarchar(64),
	@No smallint,
	@cUserid int,
	@Log smallint = 1
)
AS
BEGIN
	SET NOCOUNT ON
	DECLARE @Err Int

	INSERT INTO [at].[StatusActionType]
	(
		[Name],
		[No]
	)
	VALUES
	(
		@Name,
		@No
	)

	Set @Err = @@Error
	Set @StatusActionTypeID = scope_identity()

	IF @Log = 1 
	BEGIN 
		INSERT INTO [Log].[AuditLog] ( UserId, TableName, Type, Data, Created) 
		SELECT @cUserid,'StatusActionType',0,
		( SELECT * FROM [at].[StatusActionType] 
			WHERE
			[StatusActionTypeID] = @StatusActionTypeID				 FOR XML AUTO) as data,
				getdate() 
	 END

	RETURN @Err
END


GO
