SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[org].[prc_UserWithGroupByUTID_get]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [org].[prc_UserWithGroupByUTID_get] AS' 
END
GO
/*
    Steve - Sept 22 2016 - EntityStatus for table UserGroup
    2016-10-10 Ray:     Remove column Status from org.User, org.Department, RDB.dbo.Result
    2018-07-12 Ray:     Replace UG_U by UGMember
*/
ALTER PROCEDURE [org].[prc_UserWithGroupByUTID_get](
    @HierarchyID int,
    @UserID      int,
    @UsertypeID  int,
    @UsertypeID2 int)
AS
BEGIN
    SET NOCOUNT ON;
    DECLARE @ActiveEntityStatusID int = (SELECT [EntityStatusID] FROM [EntityStatus] WHERE [CodeName] = 'Active');
    DECLARE @ColumnDelimiter nvarchar(2)= '*', @RowDelimiter nvarchar(2)= '|', @HDID int, @LoginDepartment nvarchar(max), @i int= 0;

    DECLARE c_HD CURSOR READ_ONLY FOR
    SELECT [hd].[HDID], [d].[Name]
    FROM (SELECT [us].[UserID], [us].[DepartmentID] FROM [org].[User] [us] WHERE [us].[UserID] = @UserID
          UNION
          SELECT [ud].[UserID], [ud].[DepartmentID] FROM [org].[U_D] [ud] WHERE [ud].[UserID] = @UserID
         ) [deps]
    JOIN [org].[H_D] [hd] ON [deps].[DepartmentID] = [hd].[DepartmentID] AND [hd].[HierarchyID] = @HierarchyID AND [hd].[Deleted] = 0
    JOIN [org].[Department] [d] ON [d].[DepartmentID] = [hd].[DepartmentID] AND [d].[EntityStatusID] = @ActiveEntityStatusID AND [d].[Deleted] IS NULL;

    OPEN c_HD;
    FETCH NEXT FROM c_HD INTO @HDID, @LoginDepartment;
    WHILE @@FETCH_STATUS = 0
    BEGIN
        IF @i = 0
        BEGIN
            SELECT [u].[UserID], [u].[Ownerid], [u].[DepartmentID], [u].[LanguageID], ISNULL([u].[RoleID], 0) AS 'RoleID', [u].[UserName], [u].[Password],
                   [u].[LastName], [u].[FirstName], [u].[Email], [u].[Mobile], [u].[ExtID], [u].[SSN], [u].[Tag], [u].[EntityStatusID], [u].[Deleted],
                   [u].[EntityStatusReasonID], [u].[Locked], [u].[ChangePassword], [u].[Created], [d].[Name] AS [DepartmentName],
                   (SELECT CONVERT(nvarchar, [ug].[UserGroupID]) + @ColumnDelimiter + [ug].[Name] + @ColumnDelimiter + CONVERT(nvarchar, [ug].[UserGroupTypeID]) + @RowDelimiter
                    FROM [org].[UGMember] [ugm]
                    JOIN [org].[UserGroup] [ug] ON [ugm].[UserGroupID] = [ug].[UserGroupID] AND [ugm].[UserID] = [u].[UserID]
                     AND [ug].[EntityStatusID] = @ActiveEntityStatusID AND [ug].[Deleted] IS NULL
                     AND [ugm].[EntityStatusID] = @ActiveEntityStatusID AND [ugm].[Deleted] IS NULL
                    FOR XML PATH('')
                   ) AS 'strUserGroups', @HDID AS [HDID], @LoginDepartment AS [LoginDepartment]
            INTO [#ResultStatus]
            FROM [org].[H_D] [hd]
            JOIN [org].[USER] [u] ON [u].[departmentid] = [hd].[departmentid] AND [u].[EntityStatusID] = @ActiveEntityStatusID AND [u].[Deleted] IS NULL
             AND [hd].PATH LIKE '%\' + CAST(@HDID AS nvarchar(32)) + '\%'
            JOIN [org].[Department] [d] ON [u].[departmentid] = [d].[departmentid] AND [d].[EntityStatusID] = @ActiveEntityStatusID AND [d].[Deleted] IS NULL
            JOIN [org].[UT_U] [utu] ON [utu].[userid] = [u].[userid] AND ([utu].[usertypeid] = @Usertypeid)
            JOIN [org].[UT_U] [utu2] ON [utu2].[userid] = [u].[userid] AND ([utu2].[usertypeid] = @Usertypeid2);
        END;
        ELSE
        BEGIN
            INSERT INTO [#ResultStatus] ([UserID], [Ownerid], [DepartmentID], [LanguageID], [RoleID], [UserName], [Password],
                                         [LastName], [FirstName], [Email], [Mobile], [ExtID], [SSN], [Tag], [EntityStatusID], [Deleted],
                                         [EntityStatusReasonID], [Locked], [ChangePassword], [Created], [DepartmentName],
                                         [StrUserGroups], [HDID], [LoginDepartment])
            SELECT [u].[UserID], [u].[Ownerid], [u].[DepartmentID], [u].[LanguageID], ISNULL([u].[RoleID], 0) AS 'RoleID', [u].[UserName], [u].[Password],
                   [u].[LastName], [u].[FirstName], [u].[Email], [u].[Mobile], [u].[ExtID], [u].[SSN], [u].[Tag], [u].[EntityStatusID], [u].[Deleted],
                   [u].[EntityStatusReasonID], [u].[Locked], [u].[ChangePassword], [u].[Created], [d].[Name] AS [DepartmentName],
                   (SELECT CONVERT(nvarchar, [ug].[UserGroupID]) + @ColumnDelimiter + [ug].[Name] + @ColumnDelimiter + CONVERT(nvarchar, [ug].[UserGroupTypeID]) + @RowDelimiter
                    FROM [org].[UGMember] [ugm]
                    JOIN [org].[UserGroup] [ug] ON [ugm].[UserGroupID] = [ug].[UserGroupID] AND [ugm].[UserID] = [u].[UserID]
                     AND [ug].[EntityStatusID] = @ActiveEntityStatusID AND [ug].[Deleted] IS NULL
                     AND [ugm].[EntityStatusID] = @ActiveEntityStatusID AND [ugm].[Deleted] IS NULL
                    FOR XML PATH('')
                   ) AS 'StrUserGroups', @HDID AS [HDID], @LoginDepartment AS [LoginDepartment]
            FROM [org].[H_D] [hd]
            JOIN [org].[USER] [u] ON [u].[departmentid] = [hd].[departmentid] AND [u].[EntityStatusID] = @ActiveEntityStatusID AND [u].[Deleted] IS NULL
             AND [hd].PATH LIKE '%\' + CAST(@HDID AS nvarchar(32)) + '\%'
            JOIN [org].[Department] [d] ON [u].[departmentid] = [d].[departmentid] AND [d].[EntityStatusID] = @ActiveEntityStatusID AND [d].[Deleted] IS NULL
            JOIN [org].[UT_U] [utu] ON [utu].[userid] = [u].[userid] AND ([utu].[usertypeid] = @Usertypeid)
            JOIN [org].[UT_U] [utu2] ON [utu2].[userid] = [u].[userid] AND ([utu2].[usertypeid] = @Usertypeid2);
        END;

        SET @i = @i + 1;
        FETCH NEXT FROM c_HD INTO @HDID, @LoginDepartment;
    END;
    CLOSE c_HD;
    DEALLOCATE c_HD;

    SELECT * FROM [#ResultStatus];
    DROP TABLE [#ResultStatus];
END;
GO