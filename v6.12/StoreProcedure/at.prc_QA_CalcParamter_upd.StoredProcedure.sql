SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[at].[prc_QA_CalcParamter_upd]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [at].[prc_QA_CalcParamter_upd] AS' 
END
GO

ALTER PROCEDURE [at].[prc_QA_CalcParamter_upd]
(
	@QA_CalcParamterID int,
	@CFID int,
	@No smallint,
	@Name varchar(32),
	@QuestionID int,
	@AlternativeID int,
	@CategoryID int,
	@CalcType smallint,
	@Format nvarchar(32),
	@cUserid int,
	@Log smallint = 1
)
AS
BEGIN
	SET NOCOUNT ON
	DECLARE @Err Int

	UPDATE [at].[QA_CalcParamter]
	SET
		[CFID] = @CFID,
		[No] = @No,
		[Name] = @Name,
		[QuestionID] = @QuestionID,
		[AlternativeID] = @AlternativeID,
		[CategoryID] = @CategoryID,
		[CalcType] = @CalcType,
		[Format] = @Format
	WHERE
		[QA_CalcParamterID] = @QA_CalcParamterID

	IF @Log = 1 
	BEGIN 
		INSERT INTO [Log].[AuditLog] ( UserId, TableName, Type, Data, Created) 
		SELECT @cUserid,'QA_CalcParamter',1,
		( SELECT * FROM [at].[QA_CalcParamter] 
			WHERE
			[QA_CalcParamterID] = @QA_CalcParamterID			 FOR XML AUTO) as data,
			getdate()
	END

	Set @Err = @@Error

	RETURN @Err
END


GO
