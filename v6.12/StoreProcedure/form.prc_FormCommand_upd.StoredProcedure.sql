SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[form].[prc_FormCommand_upd]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [form].[prc_FormCommand_upd] AS' 
END
GO
ALTER PROCEDURE [form].[prc_FormCommand_upd]
(
	@FormCommandID int,
	@FormID int,
	@No smallint,
	@FormMode smallint,
	@FormCommandType smallint,
	@CssClass nvarchar(256),
	@ClientFunction nvarchar(256),
	@CommandGroupClass nvarchar(50)=NULL,
	@cUserid int,
	@Log smallint = 1
)
AS
BEGIN
	SET NOCOUNT ON
	DECLARE @Err Int

	UPDATE [form].[FormCommand]
	SET
		[FormID] = @FormID,
		[No] = @No,
		[FormMode] = @FormMode,
		[FormCommandType] = @FormCommandType,
		[CssClass] = @CssClass,
		[ClientFunction] = @ClientFunction,
		[CommandGroupClass] = @CommandGroupClass
	WHERE
		[FormCommandID] = @FormCommandID

	IF @Log = 1 
	BEGIN 
		INSERT INTO [Log].[AuditLog] ( UserId, TableName, Type, Data, Created) 
		SELECT @cUserid,'FormCommand',1,
		( SELECT * FROM [form].[FormCommand] 
			WHERE
			[FormCommandID] = @FormCommandID			 FOR XML AUTO) as data,
			getdate()
	END

	Set @Err = @@Error

	RETURN @Err
END




GO
