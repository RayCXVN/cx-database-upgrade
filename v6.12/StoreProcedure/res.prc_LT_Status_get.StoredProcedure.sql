SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[res].[prc_LT_Status_get]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [res].[prc_LT_Status_get] AS' 
END
GO

ALTER PROCEDURE [res].[prc_LT_Status_get]  
(  
	@StatusID int  
)  
AS  
BEGIN  
 SET NOCOUNT ON  
 DECLARE @Err Int  
  
 SELECT  
	[StatusID],  
	[LanguageID],  
	[Name],  
	[Description]
 FROM [res].[LT_Status]  
 WHERE  
	[StatusID] = @StatusID  
  
 Set @Err = @@Error  
  
 RETURN @Err  
END  

GO
