SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[org].[prc_User_Search_ByProfileLetter]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [org].[prc_User_Search_ByProfileLetter] AS' 
END
GO
/* 
    2016-10-25 Steve: Add condition ActivityID to query finding results
    Johnny - Des 07 2016 - Update store procedure for dynamic SQL 
*/
ALTER PROCEDURE [org].[prc_User_Search_ByProfileLetter]
(
     @SearchString1 nvarchar(max) ='O',
     @SearchString2 nvarchar(max) ='S,R',
     @SearchString3 nvarchar(max) ='P',
     @HDID          int=0,
     @CustomerID    int =16,
     @ActivityID    int =4,
     @QuestionID    int =4193,
     @AlternativeID int = 474,
     @LanguageID   int =1
) 
WITH RECOMPILE
AS
BEGIN
    DECLARE @ActiveEntityStatusID INT = (SELECT EntityStatusID FROM EntityStatus WHERE CodeName='Active')
    DECLARE @sqlCommand nvarchar(max) ='',@Parameters nvarchar(max)=''
    DECLARE @ReportServer nvarchar(max), @ReportDB nvarchar(max)
    
    DECLARE @result TABLE (UserID INT)

    DECLARE c_ResultDatabase CURSOR READ_ONLY FOR
    SELECT DISTINCT s.ReportServer, s.ReportDB
    FROM	at.Survey s JOIN at.Activity act ON act.ActivityID = s.ActivityID 
    WHERE  s.[Status]=2
		 AND s.ActivityID =@ActivityID
	
	DECLARE @LinkedDB NVARCHAR(MAX)
	
    OPEN c_ResultDatabase
    FETCH NEXT FROM c_ResultDatabase INTO @ReportServer, @ReportDB
    WHILE @@FETCH_STATUS=0
    BEGIN
		IF @ReportDB=DB_NAME() AND @ReportServer=@@servername
			BEGIN
			SET @LinkedDB=' '
			END
		ELSE
			BEGIN
			SET @LinkedDB= '['+ @ReportServer + '].['+ @ReportDB + '].'
			END	 	
	   SET @sqlCommand ='
	   WITH tblResult (ResultID, UserID, RowNum)
	   AS
	   (   
		  SELECT r.ResultID, r.UserID, ROW_NUMBER() OVER (PARTITION BY r.UserID ORDER BY r.EndDate DESC) AS RowNum 
		  FROM '+ @LinkedDB +'dbo.Result r 
		  JOIN dbo.Survey s ON r.StatusTypeID = 3 AND r.SurveyID = s.SurveyID AND s.ActivityID = ' + CONVERT(NVARCHAR(14),@ActivityID) + '
	   )
	   SELECT DISTINCT t.UserID FROM '+ @LinkedDB +'dbo.Answer a
	   JOIN tblResult t ON t.ResultID = a.ResultID AND t.RowNum = 1
	   AND a.QuestionID = @QuestionID AND a.AlternativeID = @AlternativeID
	   AND (@SearchString1 IS NULL OR EXISTS (select 1 FROM  dbo.StringToArray (@SearchString1,'','') WHERE ParseValue = SUBSTRING(a.Free,1,1)))
	   AND (@SearchString2 IS NULL OR (EXISTS (select 1 FROM  dbo.StringToArray (@SearchString2,'','') WHERE ParseValue = SUBSTRING(a.Free,2,1))))
	   AND (@SearchString3 IS NULL OR (EXISTS (select 1 FROM  dbo.StringToArray (@SearchString3,'','') WHERE ParseValue = SUBSTRING(a.Free,3,1))))
	   '
	   SET @Parameters = N'@SearchString1 nvarchar(max),@SearchString2 nvarchar(max),@SearchString3 nvarchar(max),@QuestionID int, @AlternativeID int,@LanguageID int'
	   INSERT INTO @result(UserID) EXECUTE sp_executesql @sqlCommand, @Parameters, @SearchString1 = @SearchString1,  @SearchString2 = @SearchString2, @SearchString3 = @SearchString3,@QuestionID=@QuestionID,@AlternativeID=@AlternativeID,@LanguageID =@LanguageID

	   FETCH NEXT FROM c_ResultDatabase INTO  @ReportServer, @ReportDB
    END
    CLOSE c_ResultDatabase
    DEALLOCATE c_ResultDatabase
    SELECT 
	   u.[UserID],
	   u.[DepartmentID],
	   u.[LanguageID],
	   u.[UserName],
	   u.[LastName],
	   u.[FirstName],
	   u.[Email],
	   u.[ExtID],
	   u.[SSN],
	   u.[Created],
	   u.[Mobile],
	   u.[Tag],
	   u.[Locked],
	   u.[Ownerid],
	   d.[Name] as DepartmentName,
	   u.EntityStatusID,
	   u.Deleted,
	   u.EntityStatusReasonID
    FROM org.[User] u
	   INNER JOIN org.[H_D] hd on hd.DepartmentID = u.DepartmentID and (@HDID = 0 OR (hd.path like  '%\' + cast(@HDID as varchar(16)) + '\%' and hd.deleted = 0)) AND u.EntityStatusID = @ActiveEntityStatusID AND u.Deleted IS NULL        
	   INNER JOIN org.[Department] d on d.DepartmentID = hd.DepartmentID AND d.EntityStatusID = @ActiveEntityStatusID AND d.Deleted IS NULL	   
	   AND (@HDID <> 0 OR ( hd.DepartmentID IN (SELECT d.DepartmentID from org.Customer c INNER JOIN org.Department d ON d.CustomerID = c.CustomerID AND c.CustomerID =@CustomerID)))
	   AND EXISTS (SELECT 1 FROM @result r WHERE r.UserID = u.UserID)
END

GO
