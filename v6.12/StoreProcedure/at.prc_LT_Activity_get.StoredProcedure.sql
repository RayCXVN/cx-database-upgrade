SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[at].[prc_LT_Activity_get]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [at].[prc_LT_Activity_get] AS' 
END
GO
ALTER PROCEDURE [at].[prc_LT_Activity_get]
(
	@ActivityID int
)
AS
BEGIN
	SET NOCOUNT ON
	DECLARE @Err Int

	SELECT
	[LanguageID],
	[ActivityID],
	[Name],
	[Info],
	[StartText],
	[Description],
	[RoleName],
	[SurveyName],
	[BatchName],
	[DisplayName],
	[ShortName]
	FROM [at].[LT_Activity]
	WHERE
	[ActivityID] = @ActivityID

	Set @Err = @@Error

	RETURN @Err
END

GO
