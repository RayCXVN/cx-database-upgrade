SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[at].[prc_ReportCalcType_get]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [at].[prc_ReportCalcType_get] AS' 
END
GO

ALTER PROCEDURE [at].[prc_ReportCalcType_get]
AS
BEGIN
	SET NOCOUNT ON
	DECLARE @Err Int

	SELECT
	[ReportCalcTypeID],
	[No],
	[Created]
	FROM [at].[ReportCalcType]
	ORDER BY [No]

	Set @Err = @@Error

	RETURN @Err
END


GO
