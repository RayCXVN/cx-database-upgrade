SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[org].[prc_ResultStatus_getByUsersAndActivities]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [org].[prc_ResultStatus_getByUsersAndActivities] AS' 
END
GO
ALTER PROC [org].[prc_ResultStatus_getByUsersAndActivities]
(
  @UserIDList		    varchar(max),
  @ActivityList	    varchar(max),
  @ExcludeStatusTypeID  int,
  @PriorStatusTypeID	int = 0
)
AS
BEGIN
    SET NOCOUNT ON
    DECLARE @ActiveEntityStatusID INT = (SELECT EntityStatusID FROM EntityStatus WHERE CodeName='Active')
    DECLARE @ColumnDelimiter	  nvarchar(2) = N'*',
		  @RowDelimiter	  nvarchar(2) = N'|',
		  @ColumnDefinition	  nvarchar(max) = N'',		-- in form of Activity_ + ID from @ActivityList
		  @sqlCommand		  nvarchar(max) = N'',
		  @UpdateCommand	  nvarchar(max) = N'',
		  @Parameters		  nvarchar(max) = N'@p_StatusTypeID int,@p_UserID int',
		  @ActivityID		  int,
		  @i				  int = 0
		  
    DECLARE c_Activity CURSOR READ_ONLY FOR
    SELECT ParseValue FROM dbo.StringToArray(@ActivityList,',')
    
    -- List of users
    SELECT convert(INT,ul.ParseValue) AS UserID,
	      u.FirstName,
	      u.LastName
    INTO #ResultStatus
    FROM dbo.StringToArray(@UserIDList,',') ul
    LEFT JOIN org.[USER] u ON ul.ParseValue = u.UserID
    WHERE u.EntityStatusID = @ActiveEntityStatusID AND u.Deleted IS NULL

    DECLARE c_Users CURSOR SCROLL READ_ONLY FOR
    SELECT UserID FROM #ResultStatus
    OPEN c_Users
    
    DECLARE @v_UserID int, @v_StatusTypeID int

    SET @sqlCommand = 'ALTER TABLE #ResultStatus ADD '
    	  
    OPEN c_Activity
	   FETCH NEXT FROM c_Activity INTO @ActivityID
	   WHILE @@FETCH_STATUS=0
	   BEGIN
		  -- Add column to temp table to store result status
		  SET @ColumnDefinition = @sqlCommand + N'Activity_' + convert(nvarchar,@ActivityID) + N' int'
		  EXECUTE sp_executesql @ColumnDefinition
		  
		  FETCH FIRST FROM c_Users INTO @v_UserID
		  WHILE @@FETCH_STATUS=0
		  BEGIN
			 EXEC @v_StatusTypeID = [at].[GetLatestStatusByActivity] @v_UserID, @ActivityID, @ExcludeStatusTypeID, @PriorStatusTypeID
		  
			 SET @UpdateCommand = 'UPDATE #ResultStatus SET Activity_' + convert(nvarchar,@ActivityID) + ' = @p_StatusTypeID '
							+ 'WHERE UserID = @p_UserID'
			 EXECUTE sp_executesql @UpdateCommand, @Parameters, @p_StatusTypeID = @v_StatusTypeID, @p_UserID = @v_UserID
			 FETCH NEXT FROM c_Users INTO @v_UserID
		  END
		  FETCH NEXT FROM c_Activity INTO @ActivityID
	   END
    CLOSE c_Activity
    DEALLOCATE c_Activity
    
    CLOSE c_Users
    DEALLOCATE c_Users
    
    SELECT * FROM #ResultStatus  

    DROP TABLE #ResultStatus
END

GO
