SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[prc_ObjectMapping_upd]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [dbo].[prc_ObjectMapping_upd] AS' 
END
GO


ALTER PROCEDURE [dbo].[prc_ObjectMapping_upd]
(
	@OMID int,
	@OwnerID int,
	@FromTableTypeID smallint,
	@FromID int,
	@ToTableTypeID smallint,
	@ToID int,
	@RelationTypeID int,
	@cUserid int,
	@Log smallint = 1
)
AS
BEGIN
	SET NOCOUNT ON
	DECLARE @Err Int

	UPDATE [dbo].[ObjectMapping]
	SET
		[OwnerID] = @OwnerID,
		[FromTableTypeID] = @FromTableTypeID,
		[FromID] = @FromID,
		[ToTableTypeID] = @ToTableTypeID,
		[ToID] = @ToID,
		[RelationTypeID] = @RelationTypeID
	WHERE
		[OMID] = @OMID

	IF @Log = 1 
	BEGIN 
		INSERT INTO [Log].[AuditLog] ( UserId, TableName, Type, Data, Created) 
		SELECT @cUserid,'ObjectMapping',1,
		( SELECT * FROM [dbo].[ObjectMapping] 
			WHERE
			[OMID] = @OMID			 FOR XML AUTO) as data,
			getdate()
	END

	Set @Err = @@Error

	RETURN @Err
END



GO
