SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[at].[prc_DottedRule_upd]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [at].[prc_DottedRule_upd] AS' 
END
GO
ALTER PROCEDURE [at].[prc_DottedRule_upd]
(
	@DottedRuleID int,
	@ActivityID int,
	@SurveyID INT=NULL,
	@Active bit,
	@UseOnAllQuestions bit,
	@UseOnAllCategorys bit,
	@MinResult int,
	@MinResultPercentage float,
	@FrequencyDottedType int,
	@FrequencyDottedLimit int,
	@FrequencyDotZeroValues bit,
	@FrequencyMinAlternativeCountDotted int,
	@AverageDottedType int,
	@AverageDottedLimit float,
	@OnlyDepartmentsBelow bit = 0,
	@cUserid int,
	@Log smallint = 1,
	@FrequencyMinQuestionCountDotted int =0
)
AS
BEGIN
	SET NOCOUNT ON
	DECLARE @Err Int

	UPDATE [at].[DottedRule]
	SET
		[ActivityID] = @ActivityID,
		[SurveyID] = @SurveyID,
		[Active] = @Active,
		[UseOnAllQuestions] = @UseOnAllQuestions,
		[UseOnAllCategorys] = @UseOnAllCategorys,
		[MinResult] = @MinResult,
		[MinResultPercentage] = @MinResultPercentage, 
		[FrequencyDottedType] = @FrequencyDottedType,
		[FrequencyDottedLimit] = @FrequencyDottedLimit,
		[FrequencyDotZeroValues] = @FrequencyDotZeroValues,
		[FrequencyMinAlternativeCountDotted] = @FrequencyMinAlternativeCountDotted,
		[AverageDottedType] = @AverageDottedType,
		[AverageDottedLimit] = @AverageDottedLimit,
		[OnlyDepartmentsBelow] = @OnlyDepartmentsBelow,
		[FrequencyMinQuestionCountDotted]=@FrequencyMinQuestionCountDotted
	WHERE
		[DottedRuleID] = @DottedRuleID

	IF @Log = 1 
	BEGIN 
		INSERT INTO [Log].[AuditLog] ( UserId, TableName, Type, Data, Created) 
		SELECT @cUserid,'DottedRule',1,
		( SELECT * FROM [at].[DottedRule] 
			WHERE
			[DottedRuleID] = @DottedRuleID			 FOR XML AUTO) as data,
			getdate()
	END

	Set @Err = @@Error

	RETURN @Err
END

GO
