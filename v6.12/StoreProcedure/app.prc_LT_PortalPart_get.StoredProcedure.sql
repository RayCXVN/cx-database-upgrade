SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[app].[prc_LT_PortalPart_get]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [app].[prc_LT_PortalPart_get] AS' 
END
GO

ALTER PROCEDURE [app].[prc_LT_PortalPart_get]
(
	@PortalPartID int
)
AS
BEGIN
	SET NOCOUNT ON
	DECLARE @Err Int

	SELECT
	[LanguageID],
	[PortalPartID],
	[Name],
	[Description],
	[Text],
	[TextAbove],
	[TextBelow],
	[HeaderText],
	[FooterText],
	[BeforeContentText],
	[AfterContentText]
	FROM [app].[LT_PortalPart]
	WHERE
	[PortalPartID] = @PortalPartID

	Set @Err = @@Error

	RETURN @Err
END

/****** Object:  StoredProcedure [app].[prc_LT_PortalPart_ins]    Script Date: 12/15/2010 12:16:48 ******/
SET ANSI_NULLS ON

GO
