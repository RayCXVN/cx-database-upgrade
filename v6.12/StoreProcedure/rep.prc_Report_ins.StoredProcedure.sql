SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[rep].[prc_Report_ins]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [rep].[prc_Report_ins] AS' 
END
GO

ALTER PROCEDURE [rep].[prc_Report_ins]
(
	@ReportID int = null output,
	@OwnerID int,
	@UserID int,
	@ChartTemplate nvarchar(512),
	@DefaultCalcType smallint,
	@cUserid int,
	@Log smallint = 1
)
AS
BEGIN
	SET NOCOUNT ON
	DECLARE @Err Int

	INSERT INTO [rep].[Report]
	(
		[OwnerID],
		[UserID],
		[ChartTemplate],
		[DefaultCalcType]
	)
	VALUES
	(
		@OwnerID,
		@UserID,
		@ChartTemplate,
		@DefaultCalcType
	)

	Set @Err = @@Error
	Set @ReportID = scope_identity()

	IF @Log = 1 
	BEGIN 
		INSERT INTO [Log].[AuditLog] ( UserId, TableName, Type, Data, Created) 
		SELECT @cUserid,'Report',0,
		( SELECT * FROM [rep].[Report] 
			WHERE
			[ReportID] = @ReportID				 FOR XML AUTO) as data,
				getdate() 
	 END

	RETURN @Err
END


GO
