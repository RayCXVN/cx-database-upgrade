SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[res].[prc_LT_MediaType_get]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [res].[prc_LT_MediaType_get] AS' 
END
GO
  
ALTER PROCEDURE [res].[prc_LT_MediaType_get]  
(  
	@MediaTypeID int  
)  
AS  
BEGIN  
 SET NOCOUNT ON  
 DECLARE @Err Int  
  
 SELECT  
	[MediaTypeID],  
	[LanguageID],  
	[Name]
 FROM [res].[LT_MediaType]  
 WHERE  
	[MediaTypeID] = @MediaTypeID  
  
 Set @Err = @@Error  
  
 RETURN @Err  
END  

GO
