IF NOT EXISTS (SELECT 1 FROM sys.objects WHERE name = N'prc_MemberRole_ins' AND type IN ( N'P', N'PC' )  AND SCHEMA_NAME(schema_id) = 'org')
    EXEC ('CREATE PROC [org].[prc_MemberRole_ins] AS ')
GO
/*  
    2018-07-19 Sarah    Create procedures for Insert, Update, Delete, Get of org.MemberRole
*/
ALTER PROCEDURE [org].[prc_MemberRole_ins]
(  
    @MemberRoleID int = null output,
    @OwnerID int,
    @ExtID nvarchar(max),
    @No int ,
    @Created smalldatetime,
    @EntityStatusID int,
    @EntityStatusReasonID int,
    @cUserid int,
    @Log smallint = 1
)  
AS  
BEGIN  
 SET NOCOUNT ON  
 DECLARE @Err Int  

 INSERT INTO [org].[MemberRole]
 (  
    OwnerID, 
    ExtID, 
    [No], 
    Created, 
    EntityStatusID, 
    EntityStatusReasonID
 )  
 VALUES  
 (  
    @OwnerID, 
    @ExtID, 
    @No, 
    @Created, 
    @EntityStatusID, 
    @EntityStatusReasonID
 )  

 Set @Err = @@Error  
 Set @MemberRoleID = scope_identity()  

 IF @Log = 1 
 BEGIN 
    INSERT INTO [Log].[AuditLog] ( UserId, TableName, [Type], Data, Created) 
    SELECT @cUserid,'MemberRole',0,
    ( SELECT * FROM [org].[MemberRole] 
       WHERE MemberRoleID = @MemberRoleID
       FOR XML AUTO
    ) as data,
    getdate() 
 END  

 RETURN @Err  
END