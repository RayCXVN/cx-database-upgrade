SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[prc_LT_RelationType_upd]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [dbo].[prc_LT_RelationType_upd] AS' 
END
GO

ALTER PROCEDURE [dbo].[prc_LT_RelationType_upd]
(
	@LanguageID int,
	@RelationTypeID int,
	@Name nvarchar(256),
	@Description nvarchar(max),
	@cUserid int,
	@Log smallint = 1
)
AS
BEGIN
	SET NOCOUNT ON
	DECLARE @Err Int

	UPDATE [dbo].[LT_RelationType]
	SET
		[LanguageID] = @LanguageID,
		[RelationTypeID] = @RelationTypeID,
		[Name] = @Name,
		[Description] = @Description
	WHERE
		[LanguageID] = @LanguageID AND
		[RelationTypeID] = @RelationTypeID

	IF @Log = 1 
	BEGIN 
		INSERT INTO [Log].[AuditLog] ( UserId, TableName, Type, Data, Created) 
		SELECT @cUserid,'LT_RelationType',1,
		( SELECT * FROM [dbo].[LT_RelationType] 
			WHERE
			[LanguageID] = @LanguageID AND
			[RelationTypeID] = @RelationTypeID			 FOR XML AUTO) as data,
			getdate()
	END

	Set @Err = @@Error

	RETURN @Err
END


GO
