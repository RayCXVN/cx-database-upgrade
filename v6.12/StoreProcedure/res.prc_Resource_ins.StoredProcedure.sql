SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[res].[prc_Resource_ins]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [res].[prc_Resource_ins] AS' 
END
GO
/*
	2017-07-26 Steve: Change the length of column ExtID in all related tables from xAPI format
*/
ALTER PROCEDURE [res].[prc_Resource_ins]    
(
	@ResourceID int = null output,
	@MediaTypeID int,
	@CategoryID int,
	@URL nvarchar(max),
	@StatusID int,
	@ValidFrom datetime,
	@ValidTo datetime,
	@Active bit,
	@ExtID nvarchar(256),
	@Author int,
	@Deleted bit,
	@AuthorName nvarchar(250),
	@CreatedBy int,
	@No smallint,
	@ShowAuthor bit,
	@ShowTopBar bit,
	@ShowCompleteCheckbox bit,
	@IsExternal bit,
	@ShowPublishedDate bit,
	@ShowNote bit,
	@cUserid int,  
	@Log smallint = 1
)
AS    
BEGIN    
 SET NOCOUNT ON;    
 DECLARE @Err Int    

 INSERT INTO [res].[Resource]
    ([MediaTypeId]
    ,[CategoryId]
    ,[URL]
    ,[StatusID]
    ,[ValidFrom]
	,[ValidTo]
	,[Active]
	,[ExtID]
	,[Author]
	,[Deleted]
	,[AuthorName]
	,[CreatedBy]
	,[No]
	,[ShowAuthor]
	,[ShowTopBar]
	,[ShowCompleteCheckbox]
	,[IsExternal]
	,[ShowPublishedDate]
	,[ShowNote])
 VALUES
	(@MediaTypeId
    ,@CategoryId
    ,@URL
    ,@StatusID
    ,@ValidFrom
	,@ValidTo
	,@Active
	,@ExtID
	,@Author
	,@Deleted
	,@AuthorName
	,@CreatedBy
	,@No
	,@ShowAuthor
	,@ShowTopBar
	,@ShowCompleteCheckbox
	,@IsExternal
	,@ShowPublishedDate
	,@ShowNote)
 

 Set @Err = @@Error  
 Set @ResourceID = scope_identity()  
  
 IF @Log = 1   
 BEGIN   
  INSERT INTO [Log].[AuditLog] ( UserId, TableName, Type, Data, Created)   
  SELECT @cUserid,'res.Resource',0,  
  ( SELECT * FROM [res].[Resource]   
   WHERE  
   [ResourceID] = @ResourceID     FOR XML AUTO) as data,  
    getdate()   
  END  
  
 RETURN @Err  

END

GO
