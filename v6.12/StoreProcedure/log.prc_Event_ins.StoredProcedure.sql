SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[log].[prc_Event_ins]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [log].[prc_Event_ins] AS' 
END
GO
ALTER PROCEDURE [log].[prc_Event_ins]
(
    @EventID			int = NULL output,
    @EventTypeID		int,
    @UserID				INT=NULL,
    @IPNumber			nvarchar(32),
    @TableTypeID		SMALLINT=NULL,
    @ItemID				INT=NULL,
    @cUserid			int,
    @Log				smallint = 1,
    @ApplicationName	nvarchar(128)='',
    @DepartmentID		INT =NULL,
    @CustomerID			INT =NULL,
    @UserData           XML =NULL,
	@EventTypeName		nvarchar(256),
	@ArchetypeID		INT =NULL
)
AS
BEGIN
	SET NOCOUNT ON
	DECLARE @Err Int

	INSERT INTO [log].[Event]
	(
        [EventTypeID],
        [UserID],
        [IPNumber],
        [TableTypeID],
        [ItemID],
        [Created],
        [ApplicationName],
        [DepartmentID],
        [CustomerID],
        [UserData],
		[EventTypeName],
		[ArchetypeID]
	)
	VALUES
	(
        @EventTypeID,
        @UserID,
        @IPNumber,
        @TableTypeID,
        @ItemID,
        getdate(),
        @ApplicationName,
        @DepartmentID,
        @CustomerID,
        @UserData,
		@EventTypeName,	
		@ArchetypeID	
	)

	Set @Err = @@Error
	Set @EventID = scope_identity()
	
	IF @Log = 1 AND @cUserid != 0
	BEGIN
		INSERT INTO [Log].[AuditLog] ( UserId, TableName, Type, Data, Created) 
		SELECT @cUserid,'Event',0,
		( SELECT * FROM [log].[Event]
			WHERE
			[EventID] = @EventID FOR XML AUTO) as data,
				getdate() 
	END

	RETURN @Err
END

GO
