SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[at].[prc_XCategory_ins]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [at].[prc_XCategory_ins] AS' 
END
GO
/*
	2017-07-26 Steve: Change the length of column ExtID in all related tables from xAPI format
*/
ALTER PROCEDURE [at].[prc_XCategory_ins]
(
	@XCID int = null output,
	@ParentID int = null,
	@OwnerID int,
	@Type smallint,
	@Status smallint,
	@No smallint,
	@ExtId NVARCHAR(256) = '',
	@cUserid int,
	@Log smallint = 1
)
AS
BEGIN
	SET NOCOUNT ON
	DECLARE @Err Int

	INSERT INTO [at].[XCategory]
	(
		[ParentID],
		[OwnerID],
		[Type],
		[Status],
		[No],
		[ExtId]
	)
	VALUES
	(
		@ParentID,
		@OwnerID,
		@Type,
		@Status,
		@No,
		@ExtId
	)

	Set @Err = @@Error
	Set @XCID = scope_identity()

	IF @Log = 1 
	BEGIN 
		INSERT INTO [Log].[AuditLog] ( UserId, TableName, Type, Data, Created) 
		SELECT @cUserid,'XCategory',0,
		( SELECT * FROM [at].[XCategory] 
			WHERE
			[XCID] = @XCID				 FOR XML AUTO) as data,
				getdate() 
	 END

	RETURN @Err
END

GO
