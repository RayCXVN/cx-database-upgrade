SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[note].[prc_LT_NoteTag_ins]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [note].[prc_LT_NoteTag_ins] AS' 
END
GO
ALTER PROCEDURE [note].[prc_LT_NoteTag_ins]
(
	@LanguageID int,
	@NoteTagID int,
	@Name nvarchar(128),
	@Description nvarchar(max),
	@cUserid int,
	@Log smallint = 1
)
AS
BEGIN
	SET NOCOUNT ON
	DECLARE @Err Int

	INSERT INTO [note].[LT_NoteTag]
	(
		[NoteTagID],
		[LanguageID],
		[Name],
		[Description]
	)
	VALUES
	(
		@NoteTagID,
		@LanguageID,
		@Name,
		@Description
	)

	Set @Err = @@Error
	
	IF @Log = 1 
	BEGIN 
		INSERT INTO [Log].[AuditLog] ( UserId, TableName, Type, Data, Created) 
		SELECT @cUserid,'LT_NoteTag',0,
		( SELECT * FROM [note].[LT_NoteTag] 
			WHERE
			[NoteTagID] = @NoteTagID
			AND [LanguageID] = @LanguageID FOR XML AUTO) as data,
				getdate() 
	 END

	RETURN @Err
END

GO
