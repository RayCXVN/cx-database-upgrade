SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[prop].[prc_PropFile_del]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [prop].[prc_PropFile_del] AS' 
END
GO


ALTER PROCEDURE [prop].[prc_PropFile_del]
(
	@PropFileID int,
	@cUserid int,
	@Log smallint = 1
)
AS
BEGIN
	SET NOCOUNT ON
	DECLARE @Err Int

	IF @Log = 1 
	BEGIN 
		INSERT INTO [Log].[AuditLog] ( UserId, TableName, Type, Data, Created) 
		SELECT @cUserid,'PropFile',33,
		(	SELECT *
			FROM [prop].[PropFile] 
			WHERE [PropFileID] = @PropFileID FOR XML AUTO) as data,
		getdate() 
	END
	
	UPDATE [prop].[PropFile]
	SET
		[Deleted] = 1
	WHERE
		[PropFileID] = @PropFileID
/*
DELETE FROM [prop].[PropFile]
WHERE [FileID] = @FileID
*/
	Set @Err = @@Error

	RETURN @Err
END




GO
