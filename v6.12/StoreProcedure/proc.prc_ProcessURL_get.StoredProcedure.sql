SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[proc].[prc_ProcessURL_get]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [proc].[prc_ProcessURL_get] AS' 
END
GO

ALTER PROCEDURE [proc].[prc_ProcessURL_get]
(
	@ProcessID int
)
AS
BEGIN
	SET NOCOUNT ON
	DECLARE @Err Int

	SELECT
	[ProcessURLID],
	[ProcessID],
	[URL],
	[Name],
	[Target],
	[No],
	[Created]
	FROM [proc].[ProcessURL]
	WHERE
	[ProcessID] = @ProcessID
	ORDER BY [No]

	Set @Err = @@Error

	RETURN @Err
END

GO
