SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[org].[Login]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [org].[Login] AS' 
END
GO
/*
	Nov 28, 2016 - Sarah - Remove column status
*/
ALTER PROCEDURE [org].[Login]
(
	@UserName	nvarchar(128),
	@PassWord	nvarchar(64),
	@UserID		int		OUTPUT,
	@LanguageID	int=1		OUTPUT,
	@DepartmentID	int		OUTPUT,
	@Firstname	nvarchar(64) 	OUTPUT,
	@Lastname	nvarchar(64) 	OUTPUT,
	@Email		nvarchar(256) 	OUTPUT,
	@Mobile		nvarchar(16) 	OUTPUT,
	@ExtId		nvarchar(64) 	OUTPUT,
	@EntityStatus   int         OUTPUT,
	@OwnerId	int	,
	@ErrNo		int		OUTPUT,
	@ChangePassword bit = 0 OUTPUT
)
AS
BEGIN

	DECLARE @Err Int, @EntityStatusID INT = 0
	SELECT @EntityStatusID = EntityStatusID FROM dbo.EntityStatus WHERE CodeName = N'Active'

	SELECT 
		@DepartmentID = DepartmentID,
		@LanguageID = LanguageID,
		@UserID = UserID,
		@Firstname = Firstname,
		@Lastname = Lastname,
		@Email = Email,
		@Mobile = Mobile,
		@ExtId = ExtId,
		@EntityStatus = EntityStatusID,
		@OwnerId = OwnerId,
		@ChangePassword = ChangePassword
	FROM 	
		[User] 
	WHERE 	
		UserName = @UserName
		AND ([Password] = @Password OR [OneTimePassword]= @PassWord)
		AND EntityStatusID = @EntityStatusID
		AND Deleted IS NULL
		AND [Ownerid] = @OwnerId

	Set @Err = @@Error
	RETURN @Err
END

GO
