SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[note].[prc_NoteType_get]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [note].[prc_NoteType_get] AS' 
END
GO
ALTER PROCEDURE [note].[prc_NoteType_get]
	@OwnerID int
AS
BEGIN
	SET NOCOUNT ON;
	DECLARE @Err Int

    SELECT 
		[NoteTypeID],
		[OwnerID],
		[Name],
		[UseVersioning],
		[UseReadLog],
		[UseHTML],
		[UseEncryption],
		[Created]
	FROM [note].[NoteType]
	WHERE [OwnerID] = @OwnerID

	Set @Err = @@Error

	RETURN @Err

END

GO
