SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[prc_LT_AccessGroup_upd]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [dbo].[prc_LT_AccessGroup_upd] AS' 
END
GO

ALTER PROCEDURE [dbo].[prc_LT_AccessGroup_upd]
(
	@LanguageID int,
	@AccessGroupID int,
	@Name nvarchar(256),
	@Description nvarchar(max),
	@cUserid int,
	@Log smallint = 1
)
AS
BEGIN
	SET NOCOUNT ON
	DECLARE @Err Int

	UPDATE [dbo].[LT_AccessGroup]
	SET
		[LanguageID] = @LanguageID,
		[AccessGroupID] = @AccessGroupID,
		[Name] = @Name,
		[Description] = @Description
	WHERE
		[LanguageID] = @LanguageID AND
		[AccessGroupID] = @AccessGroupID

	IF @Log = 1 
	BEGIN 
		INSERT INTO [Log].[AuditLog] ( UserId, TableName, Type, Data, Created) 
		SELECT @cUserid,'LT_AccessGroup',1,
		( SELECT * FROM [dbo].[LT_AccessGroup] 
			WHERE
			[LanguageID] = @LanguageID AND
			[AccessGroupID] = @AccessGroupID			 FOR XML AUTO) as data,
			getdate()
	END

	Set @Err = @@Error

	RETURN @Err
END


GO
