SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[at].[prc_XC_A_ins]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [at].[prc_XC_A_ins] AS' 
END
GO

ALTER PROCEDURE [at].[prc_XC_A_ins]
(
	@XCID int,
	@ActivityID int,
	@No smallint,
	@Listview smallint,
	@Descview smallint,
	@Chartview smallint,
	@FillColor varchar(16),
	@BorderColor varchar(16),
	@cUserid int,
	@Log smallint = 1
)
AS
BEGIN
	SET NOCOUNT ON
	DECLARE @Err Int

	INSERT INTO [at].[XC_A]
	(
		[XCID],
		[ActivityID],
		[No],
		[Listview],
		[Descview],
		[Chartview],
		[Fillcolor],
		[BorderColor]
	)
	VALUES
	(
		@XCID,
		@ActivityID,
		@No,
		@Listview,
		@Descview,
		@Chartview,
		@Fillcolor,
		@BorderColor
	)

	Set @Err = @@Error

	IF @Log = 1 
	BEGIN 
		INSERT INTO [Log].[AuditLog] ( UserId, TableName, Type, Data, Created) 
		SELECT @cUserid,'XC_A',0,
		( SELECT * FROM [at].[XC_A] 
			WHERE
			[XCID] = @XCID AND
			[ActivityID] = @ActivityID				 FOR XML AUTO) as data,
				getdate() 
	 END

	RETURN @Err
END


GO
