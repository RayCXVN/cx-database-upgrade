SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[prc_RelationType_upd]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [dbo].[prc_RelationType_upd] AS' 
END
GO

ALTER PROCEDURE [dbo].[prc_RelationType_upd]
(
	@RelationTypeID int,
	@OwnerID INT=NULL,
	@No SMALLINT=NULL,
	@cUserid int,
	@Log smallint = 1
)
AS
BEGIN
	SET NOCOUNT ON
	DECLARE @Err Int

	UPDATE [dbo].[RelationType]
	SET
		[OwnerID] = @OwnerID,
		[No] = @No
	WHERE
		[RelationTypeID] = @RelationTypeID

	IF @Log = 1 
	BEGIN 
		INSERT INTO [Log].[AuditLog] ( UserId, TableName, Type, Data, Created) 
		SELECT @cUserid,'RelationType',1,
		( SELECT * FROM [dbo].[RelationType] 
			WHERE
			[RelationTypeID] = @RelationTypeID			 FOR XML AUTO) as data,
			getdate()
	END

	Set @Err = @@Error

	RETURN @Err
END


GO
