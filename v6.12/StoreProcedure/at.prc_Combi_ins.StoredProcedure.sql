SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[at].[prc_Combi_ins]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [at].[prc_Combi_ins] AS' 
END
GO

ALTER PROCEDURE [at].[prc_Combi_ins]
(
	@CombiID int = null output,
	@ChoiceID int,
	@QuestionID int,
	@AlternativeID INT=NULL,
	@ValueFormula varchar(100),
	@cUserid int,
	@Log smallint = 1
)
AS
BEGIN
	SET NOCOUNT ON
	DECLARE @Err Int

	INSERT INTO [at].[Combi]
	(
		[ChoiceID],
		[QuestionID],
		[AlternativeID],
		[ValueFormula]
	)
	VALUES
	(
		@ChoiceID,
		@QuestionID,
		@AlternativeID,
		@ValueFormula
	)

	Set @Err = @@Error
	Set @CombiID = scope_identity()

	IF @Log = 1 
	BEGIN 
		INSERT INTO [Log].[AuditLog] ( UserId, TableName, Type, Data, Created) 
		SELECT @cUserid,'Combi',0,
		( SELECT * FROM [at].[Combi] 
			WHERE
			[CombiID] = @CombiID				 FOR XML AUTO) as data,
				getdate() 
	 END

	RETURN @Err
END


GO
