SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[note].[prc_LT_NoteTag_del]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [note].[prc_LT_NoteTag_del] AS' 
END
GO
ALTER PROCEDURE [note].[prc_LT_NoteTag_del]
(
	@LanguageID int,
	@NoteTagID int,
	@cUserid int,
	@Log smallint = 1
)
AS
BEGIN
	SET NOCOUNT ON
	DECLARE @Err Int
	
	IF @Log = 1 
	BEGIN 
		INSERT INTO [Log].[AuditLog] ( UserId, TableName, Type, Data, Created) 
		SELECT @cUserid,'LT_NoteTag',2,
		( SELECT * FROM [note].[LT_NoteTag] 
			WHERE
			[NoteTagID] = @NoteTagID
			AND [LanguageID] = @LanguageID FOR XML AUTO) as data,
				getdate() 
	 END
	 
	DELETE FROM [note].[LT_NoteTag]
	WHERE
		[NoteTagID] = @NoteTagID
		AND [LanguageID] = @LanguageID

	Set @Err = @@Error
	
	RETURN @Err
END

GO
