SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[proc].[prc_ProcessGroup_upd]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [proc].[prc_ProcessGroup_upd] AS' 
END
GO
/*
	2017-07-27 Steve: Change the length of column ExtID in all related tables from xAPI format
*/
ALTER PROCEDURE [proc].[prc_ProcessGroup_upd]
(
	@ProcessGroupID int,
	@OwnerID int,
	@CustomerID INT=NULL,
	@Active smallint,
	@UserID int,
	@No smallint,
	@cUserid int,
	@Log smallint = 1,
	@Created DATETIME = NULL,
	@ExtID nvarchar(256)=''
)
AS
BEGIN
	SET NOCOUNT ON
	DECLARE @Err Int

	SET @Created = ISNULL(@Created, GETDATE())

	UPDATE [proc].[ProcessGroup]
	SET
		[OwnerID] = @OwnerID,
		[CustomerID] = @CustomerID,
		[Active] = @Active,
		[UserID] = @UserID,
		[No] = @No,
		[Created] = @Created,
		[ExtID] = @ExtID
	WHERE
		[ProcessGroupID] = @ProcessGroupID

	IF @Log = 1 
	BEGIN 
		INSERT INTO [Log].[AuditLog] ( UserId, TableName, Type, Data, Created) 
		SELECT @cUserid,'ProcessGroup',1,
		( SELECT * FROM [proc].[ProcessGroup] 
			WHERE
			[ProcessGroupID] = @ProcessGroupID			 FOR XML AUTO) as data,
			getdate()
	END

	Set @Err = @@Error

	RETURN @Err
END


GO
