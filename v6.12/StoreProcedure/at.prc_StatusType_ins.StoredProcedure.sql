SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[at].[prc_StatusType_ins]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [at].[prc_StatusType_ins] AS' 
END
GO
/*
	2017-05-25 Sarah	Implement configuring StatusType's CodeName in ATAdmin for RelayLog
*/
ALTER PROCEDURE [at].[prc_StatusType_ins]
(
	@StatusTypeID int = null output,
	@OwnerID INT=NULL,
	@Type int,
	@No smallint,
	@Value float = 0,
	@cUserid int,
	@CodeName NVARCHAR(50),
	@Log smallint = 1
)
AS
BEGIN
	SET NOCOUNT ON
	DECLARE @Err Int

	INSERT INTO [at].[StatusType]
	(
		[OwnerID],
		[Type],
		[No],
		[Value],
		CodeName
	)
	VALUES
	(
		@OwnerID,
		@Type,
		@No,
		@Value,
		@CodeName
	)

	Set @Err = @@Error
	Set @StatusTypeID = scope_identity()

	IF @Log = 1 
	BEGIN 
		INSERT INTO [Log].[AuditLog] ( UserId, TableName, Type, Data, Created) 
		SELECT @cUserid,'StatusType',0,
		( SELECT * FROM [at].[StatusType] 
			WHERE
			[StatusTypeID] = @StatusTypeID				 FOR XML AUTO) as data,
				getdate() 
	 END

	RETURN @Err
END


GO
