SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[mea].[prc_MeasureType_upd]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [mea].[prc_MeasureType_upd] AS' 
END
GO
ALTER PROCEDURE [mea].[prc_MeasureType_upd]
 (		   
		   @MeasureTypeId int,  
		   @OwnerID  int,
           @Name nvarchar(256),
           @AllowNestedMeasures  bit,
           @AllowCategories bit,
           @AllowChecklist bit,
           @AllowDueDate bit,
           @DueDateMandatory bit,
           @DueDateDefault smallint,
           @AllowRelated bit,
           @AllowResources bit,
           @cUserid int,  
		   @Log smallint = 1)
AS

BEGIN  
 SET NOCOUNT ON  
 DECLARE @Err Int  
  
 UPDATE [mea].[MeasureType]
 SET          [OwnerID] = @OwnerID
           ,[Name] = @Name
           ,[AllowNestedMeasures] = @AllowNestedMeasures
           ,[AllowCategories] = @AllowCategories
           ,[AllowChecklist] = @AllowChecklist
           ,[AllowDueDate] = @AllowDueDate
           ,[DueDateMandatory] = @DueDateMandatory
           ,[DueDateDefault] = @DueDateDefault
           ,[AllowRelated] = @AllowRelated
           ,[AllowResources] = @AllowResources
WHERE     
           (
           [MeasureTypeId] = @MeasureTypeId
           )
  
 Set @Err = @@Error  
  
 IF @Log = 1   
 BEGIN   
  INSERT INTO [Log].[AuditLog] ( UserId, TableName, Type, Data, Created)   
  SELECT @cUserid,'MeasureType',1,  
  ( SELECT * FROM  [mea].[MeasureType] 
   WHERE  
   [MeasureTypeId] = @MeasureTypeId     FOR XML AUTO) as data,  
    getdate()   
  END  
  
 RETURN @Err  
END  


GO
