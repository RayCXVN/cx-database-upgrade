SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[sec].[prc_GroupAccessRule_del]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [sec].[prc_GroupAccessRule_del] AS' 
END
GO
ALTER PROCEDURE [sec].[prc_GroupAccessRule_del]
(  
 @GroupAccessRuleID int,  
 @cUserid int,  
 @Log smallint = 1  
)  
AS  
BEGIN
SET NOCOUNT ON
DECLARE @Err int
IF @Log = 1 BEGIN
INSERT INTO [Log].[AuditLog] (UserId, TableName, Type, Data, Created)
    SELECT
        @cUserid,
        'GroupAccessRule',
        2,
        (SELECT
            *
        FROM [sec].[GroupAccessRule]
        WHERE [GroupAccessRuleID] = @GroupAccessRuleID
        FOR xml AUTO)
        AS data,
        GETDATE()

END
DELETE FROM [sec].[GroupAccessRule]
WHERE [GroupAccessRuleID] = @GroupAccessRuleID
SET @Err = @@Error
RETURN @Err
END

GO
