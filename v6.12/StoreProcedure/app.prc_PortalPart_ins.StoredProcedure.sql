SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[app].[prc_PortalPart_ins]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [app].[prc_PortalPart_ins] AS' 
END
GO
/*
	2017-06-13 Steve: Change the length of column ExtID in all related tables from xAPI format
*/
ALTER PROCEDURE [app].[prc_PortalPart_ins]
(
	@PortalPartID int = null output,
	@PortalPageID int,
	@PortalPartTypeID int,
	@No smallint,
	@CategoryID INT=NULL,
	@Settings nvarchar(max)=NULL,
	@BubbleID INT=NULL,
	@ReportPartID INT=NULL,
	@ReportID int = null,
	@CssClass nvarchar(128) = '',
	@InlineStyle nvarchar(256) = '',
	@ActivityID int = null,
	@SurveyID int = null,	
	@cUserid int,
	@Log smallint = 1,
	@ExtID nvarchar(256)=''
)
AS
BEGIN
	SET NOCOUNT ON
	DECLARE @Err Int

	INSERT INTO [app].[PortalPart]
	(
		[PortalPageID],
		[PortalPartTypeID],
		[No],
		[CategoryID],
		[Settings],
		[BubbleID],
		[ReportPartID],
		[ReportID],
		[CssClass],
		[InlineStyle],
		[ActivityID],
		[SurveyID],
		[ExtID]
	)
	VALUES
	(
		@PortalPageID,
		@PortalPartTypeID,
		@No,
		@CategoryID,
		REPLACE(@Settings, '&', '&amp;'),
		@BubbleID,
		@ReportPartID,
		@ReportID,
		@CssClass,
		@InLineStyle,
		@ActivityID,
		@SurveyID,
		@ExtID
	)

	Set @Err = @@Error
	Set @PortalPartID = scope_identity()

	IF @Log = 1 
	BEGIN 
		INSERT INTO [Log].[AuditLog] ( UserId, TableName, Type, Data, Created) 
		SELECT @cUserid,'PortalPart',0,
		( SELECT * FROM [app].[PortalPart] 
			WHERE
			[PortalPartID] = @PortalPartID				 FOR XML AUTO) as data,
				getdate() 
	 END

	RETURN @Err
END

GO
