SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[proc].[prc_ProcessLevel_ins]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [proc].[prc_ProcessLevel_ins] AS' 
END
GO

ALTER PROCEDURE [proc].[prc_ProcessLevel_ins]
(
	@ProcessLevelID int = null output,
	@ProcessValueID int,
	@ProcessID INT=NULL,
	@cUserid int,
	@Log smallint = 1
)
AS
BEGIN
	SET NOCOUNT ON
	DECLARE @Err Int

	INSERT INTO [proc].[ProcessLevel]
	(
		[ProcessValueID],
		[ProcessID]
	)
	VALUES
	(
		@ProcessValueID,
		@ProcessID
	)

	Set @Err = @@Error
	Set @ProcessLevelID = scope_identity()

	IF @Log = 1 
	BEGIN 
		INSERT INTO [Log].[AuditLog] ( UserId, TableName, Type, Data, Created) 
		SELECT @cUserid,'ProcessLevel',0,
		( SELECT * FROM [proc].[ProcessLevel] 
			WHERE
			[ProcessLevelID] = @ProcessLevelID				 FOR XML AUTO) as data,
				getdate() 
	 END

	RETURN @Err
END


GO
