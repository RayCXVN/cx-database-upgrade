SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[rep].[prc_ReportPart_ins]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [rep].[prc_ReportPart_ins] AS' 
END
GO
ALTER PROCEDURE [rep].[prc_ReportPart_ins]
(
	@ReportPartID int = null output,
	@ReportID int,
	@Name nvarchar(256),
	@ReportPartTypeID int,
	@SelectionDir smallint,
	@ChartTypeID smallint,
	@ElementID int,
	@No smallint = 0,
	@ScaleMinValue float = 0,
	@ScaleMaxValue float = 1,
	@cUserid int,
	@Log smallint = 1
)
AS
BEGIN
	SET NOCOUNT ON
	DECLARE @Err Int

	INSERT INTO [rep].[ReportPart]
	(
		[ReportID],
		[Name],
		[ReportPartTypeID],
		[SelectionDir],
		[ChartTypeID],
		[ElementID],
		[No],
		[ScaleMinValue],
		[ScaleMaxValue]
	)
	VALUES
	(
		@ReportID,
		@Name,
		@ReportPartTypeID,
		@SelectionDir,
		@ChartTypeID,
		@ElementID,
		@No,
		@ScaleMinvalue,
		@ScaleMaxValue
	)

	Set @Err = @@Error
	Set @ReportPartID = scope_identity()

	IF @Log = 1 
	BEGIN 
		INSERT INTO [Log].[AuditLog] ( UserId, TableName, Type, Data, Created) 
		SELECT @cUserid,'ReportPart',0,
		( SELECT * FROM [rep].[ReportPart] 
			WHERE
			[ReportPartID] = @ReportPartID				 FOR XML AUTO) as data,
				getdate() 
	 END

	RETURN @Err
END




GO
