SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[job].[update_Job]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [job].[update_Job] AS' 
END
GO


ALTER PROC [job].[update_Job]
(
	@JobID 			bigint,
	@JobStatusID	tinyint
)
AS
BEGIN
UPDATE job.Job
SET JobStatusID = @JobStatusID
WHERE JobID = @JobID
END


GO
