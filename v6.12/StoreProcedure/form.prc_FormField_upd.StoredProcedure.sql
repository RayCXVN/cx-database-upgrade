SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[form].[prc_FormField_upd]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [form].[prc_FormField_upd] AS' 
END
GO
ALTER PROCEDURE [form].[prc_FormField_upd]  
(  
 @FormFieldID int,  
 @FormID int,  
 @No smallint = 0,  
 @TableTypeID SMALLINT=NULL,  
 @RelatedTableTypeID SMALLINT=NULL,  
 @FieldTypeID int,  
 @ValueType smallint,  
 @FormMode smallint,  
 @RenderType smallint,  
 @DefaultValue nvarchar(256),  
 @AvailableValues nvarchar(256),  
 @Mandatory bit,  
 @Multiple bit,  
 @PropertyID INT=NULL,  
 @FieldName nvarchar(256),  
 @Parameters nvarchar(256),  
 @cUserid int,  
 @Log smallint = 1,
 @OverrideLocked bit,
 @CssClass nvarchar(max) = ''
)  
AS  
BEGIN  
 SET NOCOUNT ON  
 DECLARE @Err Int  
  
 UPDATE [form].[FormField]  
 SET  
  [FormID] = @FormID,  
  [TableTypeID] = @TableTypeID,  
  [No] = @No,  
  [RelatedTableTypeID] = @RelatedTableTypeID,  
  [FieldTypeID] = @FieldTypeID,  
  [ValueType] = @ValueType,  
  [FormMode] = @FormMode,  
  [RenderType] = @RenderType,  
  [DefaultValue] = @DefaultValue,  
  [AvailableValues] = @AvailableValues,  
  [Mandatory] = @Mandatory,  
  [Multiple] = @Multiple,  
  [PropertyID] = @PropertyID,  
  [FieldName] = @FieldName,  
  [Parameters] = @Parameters,  
  [OverrideLocked] = @OverrideLocked,
  [CssClass] = @CssClass
 WHERE  
  [FormFieldID] = @FormFieldID  
  
 IF @Log = 1   
 BEGIN   
  INSERT INTO [Log].[AuditLog] ( UserId, TableName, Type, Data, Created)   
  SELECT @cUserid,'FormField',1,  
  ( SELECT * FROM [form].[FormField]   
   WHERE  
   [FormFieldID] = @FormFieldID    FOR XML AUTO) as data,  
   getdate()  
 END  
  
 Set @Err = @@Error  
  
 RETURN @Err  
END  

GO
