SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[rep].[prc_ReportPart_Copy]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [rep].[prc_ReportPart_Copy] AS' 
END
GO
ALTER PROC [rep].[prc_ReportPart_Copy]
(
  @ReportPartID INT = NULL  OUTPUT,
  @FromReportPartID INT
)
AS

SET XACT_ABORT ON
 BEGIN TRANSACTION 
 
 DECLARE @NewPartId AS INT,
         @RowId AS INT,
         @NewRowID AS INT,
         @NewColId AS INT,
         @NewLLid AS INT,
		@ID INT
 
 CREATE TABLE #ROW
(
 ID	BIGINT,
 NEWID BIGINT
)

 CREATE TABLE #Col
(
 ID	BIGINT,
 NEWID BIGINT
)

 CREATE TABLE #LL
(
 ID	BIGINT,
 NEWID BIGINT
)
 
 INSERT INTO rep.ReportPart(ReportID, Name, ReportPartTypeID, SelectionDir, ChartTypeID, ElementID,[No],created)
 SELECT ReportID, Name, ReportPartTypeID, SelectionDir, ChartTypeID, ElementID,No+1, GETDATE() 
 FROM rep.reportpart WHERE reportpartid = @FromReportPartID
 
 SET @NewPartId = SCOPE_IDENTITY()
 SET @ReportPartID = @NewPartId
 
 INSERT INTO rep.LT_ReportPart(LanguageID, ReportPartID, TEXT,Title)
 SELECT LanguageID, @NewPartId, TEXT,TITLE FROM rep.LT_ReportPart
 WHERE reportpartid = @FromReportPartID
 
 -- Rows
  DECLARE curRow CURSOR LOCAL FAST_FORWARD READ_ONLY FOR
SELECT ReportRowID
FROM rep.ReportRow
WHERE  ReportPartID= @FromReportPartID

OPEN curRow
FETCH NEXT FROM curRow INTO @RowID

WHILE (@@FETCH_STATUS = 0)
BEGIN
    INSERT INTO rep.ReportRow (ReportPartID, ReportRowTypeID, NO, CssClass, Created)
    SELECT  @NewPartId, ReportRowTypeID, NO, CssClass, GETDATE()
    FROM rep.ReportRow WHERE ReportRowID = @RowID
    
    SET @NewRowId = SCOPE_IDENTITY()
    INSERT #ROW(ID, NEWID) VALUES(@RowID, @NewRowId)
    
    FETCH NEXT FROM curRow INTO @RowID
END
CLOSE curRow
DEALLOCATE curRow
   
		-- Coloumns
		DECLARE curCol CURSOR LOCAL FAST_FORWARD READ_ONLY FOR 
		SELECT ReportColumnID
		FROM rep.ReportColumn RC
		INNER JOIN #ROW R ON RC.ReportRowID = R.ID 

		 OPEN curCol
		FETCH NEXT FROM curCol INTO @ID

		WHILE (@@FETCH_STATUS = 0)
		BEGIN

			INSERT INTO  rep.ReportColumn( ReportRowID, NO, ReportColumnTypeID, Formula, width, Format, URL, IsNegative,FormulaText,UseLevelLimit,UseLevelLimitText,OwnerColorID)
			SELECT R.NEWID, NO, ReportColumnTypeID, Formula, width, Format, URL, IsNegative,FormulaText,UseLevelLimit,UseLevelLimitText,OwnerColorID
			FROM rep.ReportColumn RC
			JOIN #ROW R ON R.id = RC.ReportRowID
			WHERE ReportColumnID = @ID
		    
			SET @NewColId = SCOPE_IDENTITY()
			INSERT #Col(ID, NEWID) VALUES(@ID, @NewColId)

     
          FETCH NEXT FROM curCol INTO @ID
		END
		
		 CLOSE curCol
		 DEALLOCATE curCol
		 
		 
		INSERT rep.LT_ReportColumn(LanguageID, ReportColumnID, TEXT)
		SELECT LanguageID, C.NEWID, TEXT
		FROM #Col C
		INNER JOIN rep.LT_ReportColumn LTC ON C.ID = LTC.ReportColumnID

		 -- Paramters
		 --PRINT 'PARAMS'
		 INSERT INTO rep.ReportColumnParameter(ReportColumnID, NO, Name, ActivityID, QuestionID, AlternativeID, CategoryID, CalcType)
		 SELECT C.NEWID, NO, Name, ActivityID, QuestionID, AlternativeID, CategoryID, CalcType
		 FROM rep.ReportColumnParameter RCP
		 JOIN #Col C ON RCP.ReportColumnID = C.ID
		 
		 
		 
		 -- Levellimit
		  
		
		  DECLARE curLL CURSOR LOCAL FAST_FORWARD READ_ONLY FOR 
		  SELECT RepLevelLimitID
		  FROM rep.RepLevelLimit  LL
		  INNER JOIN #Col C ON LL.ReportColumnID = C.ID  AND LL.ReportID  IS NULL and ReportColumnID is not null
		 

		 OPEN curLL
		 FETCH NEXT FROM curLL INTO @ID

		WHILE (@@FETCH_STATUS = 0)
		BEGIN
     
			INSERT INTO  rep.RepLevelLimit( ReportColumnID, ReportID, MinValue, MaxValue, Sigchange, Fillcolor, BorderColor, Created,LevelGroupID, NegativeTrend)
			SELECT  c.NewID, ReportID, MinValue, MaxValue, Sigchange, Fillcolor, BorderColor, GETDATE(),LevelGroupID,NegativeTrend
			FROM rep.RepLevelLimit LL
			JOIN #COL C ON C.id = LL.ReportColumnID
			AND  RepLevelLimitID = @ID
		    
			SET @NewLLid = SCOPE_IDENTITY()
			INSERT #LL(ID, NEWID) VALUES(@ID, @NewLLid)

     
            FETCH NEXT FROM curLL INTO @ID
            PRINT @@FETCH_STATUS
		END
		
		 CLOSE curLL
		 DEALLOCATE curLL
		
		 
		INSERT rep.LT_RepLevelLimit(RepLevelLimitID, LanguageID, Name, Description)
		SELECT L.NEWID,LanguageID, name,Description
		FROM #LL L
		INNER JOIN rep.LT_RepLevelLimit LTLL ON L.ID = LTLL.RepLevelLimitID
		
		
		COMMIT TRANSACTION
		
		
		

GO
