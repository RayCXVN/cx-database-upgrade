SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[imp].[prc_ImportFiles_ins]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [imp].[prc_ImportFiles_ins] AS' 
END
GO

ALTER PROCEDURE [imp].[prc_ImportFiles_ins]
(
	@ImportFileID int = null output,
	@UserID int,
	@MIMEType nvarchar(64),
	@Data varbinary,
	@DataError VARBINARY=NULL,
	@EmailSendtTo nvarchar(128),
	@Description nvarchar(128),
	@Size bigint,
	@SizeError bigint,
	@Filename nvarchar(128),
	@FilenameError nvarchar(128),
	@SurveyID INT=NULL,
	@cUserid int,
	@Log smallint = 1
)
AS
BEGIN
	SET NOCOUNT ON
	DECLARE @Err Int

	INSERT INTO [imp].[ImportFiles]
	(
		[UserID],
		[MIMEType],
		[Data],
		[DataError],
		[EmailSendtTo],
		[Description],
		[Size],
		[SizeError],
		[Filename],
		[FilenameError],
		[SurveyID]
	)
	VALUES
	(
		@UserID,
		@MIMEType,
		@Data,
		@DataError,
		@EmailSendtTo,
		@Description,
		@Size,
		@SizeError,
		@Filename,
		@FilenameError,
		@SurveyID
	)

	Set @Err = @@Error
	Set @ImportFileID = scope_identity()

	IF @Log = 1 
	BEGIN 
		INSERT INTO [Log].[AuditLog] ( UserId, TableName, Type, Data, Created) 
		SELECT @cUserid,'ImportFiles',0,
		( SELECT * FROM [imp].[ImportFiles] 
			WHERE
			[ImportFileID] = @ImportFileID				 FOR XML AUTO) as data,
				getdate() 
	 END

	RETURN @Err
END


GO
