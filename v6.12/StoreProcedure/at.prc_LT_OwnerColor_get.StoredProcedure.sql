SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[at].[prc_LT_OwnerColor_get]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [at].[prc_LT_OwnerColor_get] AS' 
END
GO

ALTER PROCEDURE [at].[prc_LT_OwnerColor_get]
(
	@OwnerColorID int
)
AS
BEGIN
	SET NOCOUNT ON
	DECLARE @Err Int

	SELECT
	[OwnerColorID],
	[LanguageID],
	[Name],
	[Created]
	FROM [at].[LT_OwnerColor]
	WHERE
	[OwnerColorID] = @OwnerColorID

	Set @Err = @@Error

	RETURN @Err
END


GO
