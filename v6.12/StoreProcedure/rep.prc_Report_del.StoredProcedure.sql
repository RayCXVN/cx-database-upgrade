SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[rep].[prc_Report_del]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [rep].[prc_Report_del] AS' 
END
GO


ALTER PROCEDURE [rep].[prc_Report_del]
(
	@ReportID int,
	@cUserid int,
	@Log smallint = 1
)
AS
BEGIN
	SET NOCOUNT ON
	DECLARE @Err Int

	IF @Log = 1 
	BEGIN 
		INSERT INTO [Log].[AuditLog] ( UserId, TableName, Type, Data, Created) 
		SELECT @cUserid,'Report',2,
		( SELECT * FROM [rep].[Report] 
			WHERE
			[ReportID] = @ReportID
			 FOR XML AUTO) as data,
			getdate() 
	END 


	DELETE FROM [rep].[Report]
	WHERE
		[ReportID] = @ReportID

	Set @Err = @@Error

	RETURN @Err
END


GO
