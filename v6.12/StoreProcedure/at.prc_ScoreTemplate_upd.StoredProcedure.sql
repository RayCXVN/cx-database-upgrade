SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[at].[prc_ScoreTemplate_upd]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [at].[prc_ScoreTemplate_upd] AS' 
END
GO

ALTER PROCEDURE [at].[prc_ScoreTemplate_upd]
(
	@ScoreTemplateID int,
	@ActivityID int = null,
	@XCID int = null,
	@Type smallint,
	@No smallint,
	@OwnerColorID int,
	@DefaultState bit=0,
	@cUserid int,
	@Log smallint = 1
)
AS
BEGIN
	SET NOCOUNT ON
	DECLARE @Err Int

	UPDATE [at].[ScoreTemplate]
	SET
		[ActivityID] = @ActivityID,
		[XCID] = @XCID,
		[Type] = @Type,
		[No] = @No,
		[OwnerColorID] = @OwnerColorID,
		[DefaultState] = @DefaultState
	WHERE
		[ScoreTemplateID] = @ScoreTemplateID

	IF @Log = 1 
	BEGIN 
		INSERT INTO [Log].[AuditLog] ( UserId, TableName, Type, Data, Created) 
		SELECT @cUserid,'ScoreTemplate',1,
		( SELECT * FROM [at].[ScoreTemplate] 
			WHERE
			[ScoreTemplateID] = @ScoreTemplateID			 FOR XML AUTO) as data,
			getdate()
	END

	Set @Err = @@Error

	RETURN @Err
END


GO
