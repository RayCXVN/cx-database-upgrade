SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[at].[prc_S_CT_ins]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [at].[prc_S_CT_ins] AS' 
END
GO

ALTER PROCEDURE [at].[prc_S_CT_ins]
(
	@ScaleID int,
	@ReportCalcTypeID int,
	@cUserid int,
	@Log smallint = 1
)
AS
BEGIN
	SET NOCOUNT ON
	DECLARE @Err Int

	INSERT INTO [at].[S_CT]
	(
		[ScaleID],
		[ReportCalcTypeID]
	)
	VALUES
	(
		@ScaleID,
		@ReportCalcTypeID
	)

	Set @Err = @@Error

	IF @Log = 1 
	BEGIN 
		INSERT INTO [Log].[AuditLog] ( UserId, TableName, Type, Data, Created) 
		SELECT @cUserid,'S_CT',0,
		( SELECT * FROM [at].[S_CT] 
			WHERE
			[ScaleID] = @ScaleID AND
			[ReportCalcTypeID] = @ReportCalcTypeID				 FOR XML AUTO) as data,
				getdate() 
	 END

	RETURN @Err
END


GO
