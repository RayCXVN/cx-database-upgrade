SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[rep].[prc_ReportColumnParameter_get]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [rep].[prc_ReportColumnParameter_get] AS' 
END
GO

ALTER PROCEDURE [rep].[prc_ReportColumnParameter_get]
(
	@ReportColumnID int
)
AS
BEGIN
	SET NOCOUNT ON
	DECLARE @Err Int

	SELECT
	[ReportColumnParameterID],
	[ReportColumnID],
	[No],
	[Name],
	[ActivityID],
	ISNULL([QuestionID], 0) AS 'QuestionID',
	ISNULL([AlternativeID], 0) AS 'AlternativeID',
	ISNULL([CategoryID], 0) AS 'CategoryID',
	[CalcType],
	[ItemID]

	FROM [rep].[ReportColumnParameter]
	WHERE
	[ReportColumnID] = @ReportColumnID
	ORDER BY [No]

	Set @Err = @@Error

	RETURN @Err
END


GO
