SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[at].[prc_Scale_get]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [at].[prc_Scale_get] AS' 
END
GO

ALTER PROCEDURE [at].[prc_Scale_get]
(
	@ActivityID int
)
AS
BEGIN
	SET NOCOUNT ON
	DECLARE @Err Int

	SELECT
	[ScaleID],
	ISNULL([ActivityID], 0) AS 'ActivityID',
	[MinSelect],
	[MaxSelect],
	[Type],
	[MinValue],
	[MaxValue],
	[Created],
	[Tag],
	[ExtID]
	FROM [at].[Scale]
	WHERE
	[ActivityID] = @ActivityID

	Set @Err = @@Error

	RETURN @Err
END


GO
