SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[prc_FagOppCopyProcess]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [dbo].[prc_FagOppCopyProcess] AS' 
END
GO
--Select * from org.[user] where ownerid = 8
ALTER Proc [dbo].[prc_FagOppCopyProcess]
(
  @Ownerid as int = 8,
  @Customerid as int = 40,
  @NewCustomerID as int = 38, --Austagder
  @OwnerUserID as int = 22700,
  @OwnerDepartmentid as int = 12376
)
AS
  Insert Into [PROC].processgroup (OwnerID, CustomerID, Active, UserID, No, Created, ExtID)
  SELECT  OwnerID, @NewCustomerID, Active, UserID, NO, Created,ProcessGroupID AS ExtID  
  FROM [PROC].processgroup 
  WHERE OwnerID = @Ownerid AND CustomerID = @Customerid
  and Extid not in (Select ExtID from [PROC].processgroup  where Ownerid = @Ownerid and customerid = @NewCustomerID)
  

  Insert into [proc].LT_ProcessGroup ( ProcessGroupid,LanguageID, Name, Description)
  SELECT pg2.processgroupid, 
        LanguageID, Name, Description 
  FROM [proc].LT_ProcessGroup ltpg
  Join [PROC].processgroup pg  on pg.processgroupid = ltpg.processgroupid and ownerID = 8 AND CustomerID = @Customerid
  Join [PROC].processgroup pg2 on pg2.CustomerID = @NewCustomerID and  pg2.Extid = pg.Processgroupid   

 --
  Insert into [PROC].[Process] (ProcessGroupID, DepartmentID, Active, TargetValue, UserID, CustomerID, StartDate, DueDate, Responsible, URL, No, Created, ProcessTypeID, isPublic, ExtID)
  SELECT pg2.ProcessGroupID, @OwnerDepartmentid, p.Active, p.TargetValue, @OwnerUserID, @NewCustomerid, p.StartDate, p.DueDate, p.Responsible, p.URL, p.NO, getdate(), ProcessTypeID, p.isPublic, p.ProcessID as ExtID 
  FROM [PROC].[Process] p
  JOIN [PROC].processgroup pg on pg.processgroupid = p.processgroupid and pg.Customerid = @Customerid
  JOIN [PROC].processgroup pg2 on pg2.Customerid = @NewCustomerid and pg2.extid = p.processgroupid
 

  Insert into [proc].LT_Process (ProcessID, LanguageID, Name, Description, Created)
  Select p2.Processid, ltp.LanguageID, ltp.Name, ltp.Description, getdate() 
  FROM [proc].LT_Process ltp
  Join [proc].Process p on p.processid = ltp.processid
  and processgroupid IN 
  (
   SELECT ProcessGroupID
   FROM [PROC].processgroup 
   WHERE OwnerID = 8 AND CustomerID = @Customerid
  )
  JOIN [proc].Process p2 on p2.CustomerID = @NewCustomerid and  p2.Extid = p.processid
 
  
-- Process Level
   Insert into [proc].ProcessLevel( ProcessValueID, ProcessID, Created)
   Select  ProcessValueID, p.Processid, getdate() 
    from [proc].ProcessLevel pl
   Join [proc].Process p on p.extid =  pl.processid and p.customerid = @NewCustomerID
   Where pl.processid in 
  (
     Select processid from [Proc].[Process] where customerid = @customerid
  )

    Insert into [proc].LT_ProcessLevel(ProcessLevelID, LanguageID, Name, Description, Created)
   Select pl2.processlevelID, LanguageID, Name, Description, getdate() 
   from [proc].LT_ProcessLevel ltpl
   Join  [proc].ProcessLevel pl on ltpl.ProcessLevelID = pl.ProcessLevelID
   And pl.processid in 
  (
     Select processid from [Proc].[Process] where customerid = @CustomerID
  )
  Join [proc].Process p2 on p2.customerid = @NewCustomerID and p2.extid = pl.processid
  Join [Proc].ProcessLevel pl2 on p2.processid = pl2.processid and pl.processvalueid  = pl2.processvalueid

 -- Add process Answer
   Insert into [proc].ProcessAnswer (ProcessID, ProcessLevelID, DepartmentID, RoleID, UserID, LastModified, Created)
   Select p.processid,pl.processlevelid,@OwnerDepartmentid,null,@OwnerUserID,getdate(),getdate() from [proc].[Process] p
   Join [proc].ProcessLevel pl on p.processid = pl.processid and  customerid = @NewCustomerID
   Join [proc].ProcessValue pv on pl.ProcessValueID = pv.ProcessValueID and pv.value = 0
   
   
   
   
 

GO
