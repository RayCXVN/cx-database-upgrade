SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[proc].[prc_ProcessAnswer_get]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [proc].[prc_ProcessAnswer_get] AS' 
END
GO

ALTER PROCEDURE [proc].[prc_ProcessAnswer_get]
(
	@ProcessID int
)
AS
BEGIN
	SET NOCOUNT ON
	DECLARE @Err Int

	SELECT
	[ProcessAnswerID],
	[ProcessID],
	[ProcessLevelID],
	[DepartmentID],
	ISNULL([RoleID], 0) AS 'RoleID',
	ISNULL([UserID], 0) AS 'UserID',
	[LastModified],
	[Created]
	FROM [proc].[ProcessAnswer]
	WHERE
	[ProcessID] = @ProcessID

	Set @Err = @@Error

	RETURN @Err
END


GO
