SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[org].[prc_User_ins]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [org].[prc_User_ins] AS' 
END
GO
/*
    2016-11-02 Steve: Set default CurrentDate for @LastUpdated, @LastSynchronized
	2017-07-26 Steve: Change the length of column ExtID in all related tables from xAPI format
*/
ALTER PROCEDURE [org].[prc_User_ins]
(  
 @UserID int = null output,  
 @Ownerid int,  
 @DepartmentID int,  
 @LanguageID int,  
 @RoleID int = null,  
 @UserName nvarchar(128),  
 @Password nvarchar(64),  
 @LastName nvarchar(64),  
 @FirstName nvarchar(64),  
 @Email nvarchar(256),  
 @Mobile nvarchar(16),  
 @ExtID nvarchar(256),  
 @SSN nvarchar(64),  
 @Tag nvarchar(128),  
 @Locked smallint,  
 @ChangePassword bit,  
 @HashPassword nvarchar(128)='',  
 @SaltPassword nvarchar(64)='',  
 @OneTimePassword nvarchar(64)='',  
 @OTPExpireTime smalldatetime='',  
 @cUserid int,  
 @Log smallint = 1,  
 @CountryCode int = 0,
 @Gender smallint = 0,
 @DateOfBirth date='' ,
 @ForceUserLoginAgain BIT =0,
 @LastUpdated datetime2=NULL,
 @LastUpdatedBy int = NULL,
 @LastSynchronized datetime2=NULL,
 @ArchetypeID int = NULL,
 @CustomerID int = NULL,
 @Deleted datetime2 = NULL,
 @EntityStatusID int = NULL,
 @EntityStatusReasonID int = NULL
)  
AS  
BEGIN  
 SET NOCOUNT ON  
 DECLARE @Err Int  

 IF @LastUpdated IS NULL
    SET @LastUpdated = GETDATE()

 IF @LastSynchronized IS NULL
    SET @LastSynchronized = GETDATE()

 INSERT INTO [org].[User]  
 (  
  [Ownerid],  
  [DepartmentID],  
  [LanguageID],  
  [RoleID],  
  [UserName],  
  [Password],  
  [LastName],  
  [FirstName],  
  [Email],  
  [Mobile],  
  [ExtID],  
  [SSN],  
  [Tag],  
  [Locked],  
  [ChangePassword],  
  [HashPassword],  
  [SaltPassword],  
  [OneTimePassword],  
  [OTPExpireTime],  
  [CountryCode],
  [Gender],
  [DateOfBirth],
  [ForceUserLoginAgain],
  [LastUpdated],
  [LastUpdatedBy],
  [LastSynchronized],
  [ArchetypeID],
  [CustomerID],
  [Deleted],
  [EntityStatusID],
  [EntityStatusReasonID]    
 )  
 VALUES  
 (  
  @Ownerid,  
  @DepartmentID,  
  @LanguageID,  
  @RoleID,  
  @UserName,  
  @Password,  
  @LastName,  
  @FirstName,  
  @Email,  
  @Mobile,  
  @ExtID,  
  @SSN,  
  @Tag,  
  @Locked,  
  @ChangePassword,  
  @HashPassword,  
  @SaltPassword,  
  @OneTimePassword,  
  @OTPExpireTime,  
  @CountryCode,
  @Gender,
  @DateOfBirth,
  @ForceUserLoginAgain,
  @LastUpdated ,
  @LastUpdatedBy,
  @LastSynchronized,
  @ArchetypeID,
  @CustomerID,
  @Deleted,
  @EntityStatusID,
  @EntityStatusReasonID   
 )  

 Set @Err = @@Error  
 Set @UserID = scope_identity()  

 IF @Log = 1   
 BEGIN   
  INSERT INTO [Log].[AuditLog] ( UserId, TableName, Type, Data, Created)   
  SELECT @cUserid,'User',0,  
  ( SELECT * FROM [org].[User]   
   WHERE  
   [UserID] = @UserID     FOR XML AUTO) as data,  
    getdate()   
  END  

 RETURN @Err  
END

GO
