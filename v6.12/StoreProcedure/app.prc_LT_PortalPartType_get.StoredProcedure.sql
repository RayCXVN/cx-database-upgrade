SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[app].[prc_LT_PortalPartType_get]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [app].[prc_LT_PortalPartType_get] AS' 
END
GO

ALTER PROCEDURE [app].[prc_LT_PortalPartType_get]
(
	@PortalPartTypeID int
)
AS
BEGIN
	SET NOCOUNT ON
	DECLARE @Err Int

	SELECT
	[LanguageID],
	[PortalPartTypeID],
	[Name],
	[Description]
	FROM [app].[LT_PortalPartType]
	WHERE
	[PortalPartTypeID] = @PortalPartTypeID

	Set @Err = @@Error

	RETURN @Err
END


GO
