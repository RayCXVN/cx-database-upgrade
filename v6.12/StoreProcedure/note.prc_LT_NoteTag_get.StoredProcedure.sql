SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[note].[prc_LT_NoteTag_get]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [note].[prc_LT_NoteTag_get] AS' 
END
GO
ALTER PROCEDURE [note].[prc_LT_NoteTag_get]
	@NoteTagID int
AS
BEGIN
	SET NOCOUNT ON;
	DECLARE @Err Int
	
    SELECT 
		[LanguageID],
		[NoteTagID],
		[Name],
		[Description],
		[Created]
	FROM [note].[LT_NoteTag]
	WHERE [NoteTagID] = @NoteTagID
	
	Set @Err = @@Error

	RETURN @Err
  
END

GO
