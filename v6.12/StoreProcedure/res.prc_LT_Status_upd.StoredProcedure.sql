SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[res].[prc_LT_Status_upd]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [res].[prc_LT_Status_upd] AS' 
END
GO

ALTER PROCEDURE [res].[prc_LT_Status_upd]  
(  
	@StatusID int,
	@LanguageID int,
	@Name nvarchar(250),
	@Description nvarchar(max),
	@cUserid int,
	@Log smallint = 1
)  
AS  
BEGIN  
 SET NOCOUNT ON  
	DECLARE @Err Int  
  
	UPDATE res.LT_Status
	SET
		[Name] = @Name,
		[Description] = @Description
	WHERE
		[StatusID] = @StatusID AND [LanguageID] = @LanguageID
  
	Set @Err = @@Error  
	IF @Log = 1 
	BEGIN 
		INSERT INTO [Log].[AuditLog] ( UserId, TableName, Type, Data, Created) 
		SELECT @cUserid,'res.LT_Status',0,
		( SELECT * FROM [res].[LT_Status] 
			WHERE
			[StatusID] = @StatusID AND [LanguageID] = @LanguageID	FOR XML AUTO) as data,
				getdate() 
	END

	RETURN @Err
END  

GO
