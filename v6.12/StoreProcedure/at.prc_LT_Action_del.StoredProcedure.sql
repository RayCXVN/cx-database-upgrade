SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[at].[prc_LT_Action_del]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [at].[prc_LT_Action_del] AS' 
END
GO


ALTER PROCEDURE [at].[prc_LT_Action_del]
(
	@LanguageID int,
	@ActionID int,
	@cUserid int,
	@Log smallint = 1
)
AS
BEGIN
	SET NOCOUNT ON
	DECLARE @Err Int

	IF @Log = 1 
	BEGIN 
		INSERT INTO [Log].[AuditLog] ( UserId, TableName, Type, Data, Created) 
		SELECT @cUserid,'LT_Action',2,
		( SELECT * FROM [at].[LT_Action] 
			WHERE
			[LanguageID] = @LanguageID AND
			[ActionID] = @ActionID
			 FOR XML AUTO) as data,
			getdate() 
	END 


	DELETE FROM [at].[LT_Action]
	WHERE
		[LanguageID] = @LanguageID AND
		[ActionID] = @ActionID

	Set @Err = @@Error

	RETURN @Err
END


GO
