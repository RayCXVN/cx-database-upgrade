SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[at].[prc_LT_ActivityView_upd]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [at].[prc_LT_ActivityView_upd] AS' 
END
GO


ALTER PROCEDURE [at].[prc_LT_ActivityView_upd]
(
	@LanguageID int,
	@ActivityViewID int,
	@Name nvarchar(256),
	@Description nvarchar(max),
	@cUserid int,
	@Log smallint = 1
)
AS
BEGIN
	SET NOCOUNT ON
	DECLARE @Err Int

	UPDATE [at].[LT_ActivityView]
	SET
		[LanguageID] = @LanguageID,
		[ActivityViewID] = @ActivityViewID,
		[Name] = @Name,
		[Description] = @Description
	WHERE
		[LanguageID] = @LanguageID AND
		[ActivityViewID] = @ActivityViewID

	IF @Log = 1 
	BEGIN 
		INSERT INTO [Log].[AuditLog] ( UserId, TableName, Type, Data, Created) 
		SELECT @cUserid,'LT_ActivityView',1,
		( SELECT * FROM [at].[LT_ActivityView] 
			WHERE
			[LanguageID] = @LanguageID AND
			[ActivityViewID] = @ActivityViewID			 FOR XML AUTO) as data,
			getdate()
	END

	Set @Err = @@Error

	RETURN @Err
END



GO
