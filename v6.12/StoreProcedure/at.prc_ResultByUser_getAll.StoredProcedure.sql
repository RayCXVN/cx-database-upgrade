SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[at].[prc_ResultByUser_getAll]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [at].[prc_ResultByUser_getAll] AS' 
END
GO
/****************************************************************
    Johnny - Dec 07 2016 - Update store procedure for dynamic SQL
    Steve - Dec 28 2016 - Add more output LastUpdatedBy
    2017-10-12 Ray:     Refactor, Get column ResultKey
****************************************************************/
ALTER PROCEDURE [at].[prc_ResultByUser_getAll](
    @OwnerID         int,
    @UserID          int,
    @AccessGroupList nvarchar(max) = N'',
    @ActivityID      int           = 0,
    @SurveyID        int           = 0)
AS
BEGIN
    --exec [at].[prc_ResultByUser_getAll] 15, 406, N'6,8'
    SET NOCOUNT ON;
    SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED;
    DECLARE @Err int, @TableType_Survey int, @TableType_Activity int,
            @DeniedSurveyList varchar(max)= '', @AccessGenericCount int, @ReportServer nvarchar(64)= N'', @ReportDB nvarchar(64)= N'',
            @sqlCommand nvarchar(max), @Parameters nvarchar(max)= N'';

    SELECT @TableType_Survey = [TableTypeID] FROM [dbo].[TableType] WHERE [Name] = 'Survey';
    SELECT @TableType_Activity = [TableTypeID] FROM [dbo].[TableType] WHERE [Name] = 'Activity';
    DECLARE @ActiveEntityStatusID varchar(32)= (SELECT [EntityStatusID] FROM [EntityStatus] WHERE [CodeName] = 'Active');

    SELECT @AccessGenericCount = COUNT([AccessID])
    FROM [AccessGeneric] [a]
    JOIN [AccessGroup] [g] ON [a].[AccessGroupID] = [g].[AccessGroupID]
    WHERE [a].[TableTypeID] IN (@TableType_Activity, @TableType_Survey) AND [g].[OwnerID] = @OwnerID;

    CREATE TABLE #DeniedSurveyList (ReportServer nvarchar(64), ReportDB nvarchar(64), DeniedSurvey int);
    CREATE TABLE #UserResult (ResultID bigint, ActivityID int, SurveyID int, BatchID int, DepartmentID int, RoleID int, UserGroupID int,
                              StartDate datetime, EndDate datetime, StatusTypeID int, ReportServer nvarchar(64), ReportDB nvarchar(64),
                              ActivityNo smallint, LastUpdated datetime, Created datetime, ValidFrom datetime, ValidTo datetime,
                              ParentResultID bigint, ParentResultSurveyID int, CustomerID int, Deleted DATETIME2(7), EntityStatusID int,
                              EntityStatusReasonID int, LastUpdatedBy int, [ResultKey] nvarchar(128));

    IF @AccessGenericCount > 0 AND LEN(@AccessGroupList) > 0
    BEGIN
        -- Denied activities to group
        SET @sqlCommand = N'INSERT INTO #DeniedSurveyList (ReportServer, ReportDB, DeniedSurvey)
    SELECT s.ReportServer, s.ReportDB, s.SurveyID
    FROM AccessGeneric a
    JOIN at.Survey s ON a.Elementid = s.ActivityID AND a.Type = 2
    WHERE  a.TableTypeID = @p_ActivityTableType
      AND  a.AccessGroupID IN ('+@AccessGroupList+')';
        IF @ActivityID > 0
        BEGIN
            SET @sqlCommand = @sqlCommand+' AND s.ActivityID = '+CONVERT(nvarchar(32), @ActivityID);
        END;
        SET @Parameters = N'@p_ActivityTableType int';
        EXECUTE [sp_executesql] @sqlCommand, @Parameters, @p_ActivityTableType = @TableType_Activity;

        -- Full/Read access activities to others group and not granted to their own groups
        SET @sqlCommand = N'INSERT INTO #DeniedSurveyList (ReportServer, ReportDB, DeniedSurvey)
    SELECT s.ReportServer, s.ReportDB, s.SurveyID
    FROM AccessGeneric a
    JOIN AccessGroup g ON a.AccessGroupID = g.AccessGroupID
    JOIN at.Survey s ON a.Elementid = s.ActivityID AND a.Type IN (0,1)
    WHERE  a.TableTypeID = @p_ActivityTableType
      AND  a.AccessGroupID NOT IN ('+@AccessGroupList+')
      AND  g.OwnerID = @p_OwnerID
    EXCEPT
    SELECT s.ReportServer, s.ReportDB, s.SurveyID
    FROM AccessGeneric a
    JOIN at.Survey s ON a.Elementid = s.ActivityID AND a.Type IN (0,1)
    WHERE  a.TableTypeID = @p_ActivityTableType
      AND  a.AccessGroupID IN ('+@AccessGroupList+')';
        IF @ActivityID > 0
        BEGIN
            SET @sqlCommand = @sqlCommand+' AND s.ActivityID = '+CONVERT(nvarchar(32), @ActivityID);
        END;
        SET @Parameters = N'@p_ActivityTableType int,@p_OwnerID int';
        EXECUTE [sp_executesql] @sqlCommand, @Parameters, @p_ActivityTableType = @TableType_Activity, @p_OwnerID = @OwnerID;

        -- Denied surveys to group
        SET @sqlCommand = N'INSERT INTO #DeniedSurveyList (ReportServer, ReportDB, DeniedSurvey)
    SELECT s.ReportServer, s.ReportDB, s.SurveyID
    FROM AccessGeneric a
    JOIN at.Survey s ON a.Elementid = s.SurveyID AND a.Type = 2
    WHERE  a.TableTypeID = @p_SurveyTableType
      AND  a.AccessGroupID IN ('+@AccessGroupList+')';
        IF @ActivityID > 0
        BEGIN
            SET @sqlCommand = @sqlCommand+' AND s.ActivityID = '+CONVERT(nvarchar(32), @ActivityID);
        END;
        IF @SurveyID > 0
        BEGIN
            SET @sqlCommand = @sqlCommand+' AND s.SurveyID = '+CONVERT(nvarchar(32), @SurveyID);
        END;
        SET @Parameters = N'@p_SurveyTableType int';
        EXECUTE [sp_executesql] @sqlCommand, @Parameters, @p_SurveyTableType = @TableType_Survey;

        -- Full/Read access surveys to others group and not granted to their own groups
        SET @sqlCommand = N'INSERT INTO #DeniedSurveyList (ReportServer, ReportDB, DeniedSurvey)
    SELECT s.ReportServer, s.ReportDB, s.SurveyID
    FROM AccessGeneric a
    JOIN AccessGroup g ON a.AccessGroupID = g.AccessGroupID
    JOIN at.Survey s ON a.Elementid = s.SurveyID AND a.Type IN (0,1)
    WHERE  a.TableTypeID = @p_SurveyTableType
      AND  a.AccessGroupID NOT IN ('+@AccessGroupList+')
      AND  g.OwnerID = @p_OwnerID
    EXCEPT
    SELECT s.ReportServer, s.ReportDB, s.SurveyID
    FROM AccessGeneric a
    JOIN at.Survey s ON a.Elementid = s.SurveyID AND a.Type IN (0,1)
    WHERE  a.TableTypeID = @p_SurveyTableType
      AND  a.AccessGroupID IN ('+@AccessGroupList+')';
        IF @ActivityID > 0
        BEGIN
            SET @sqlCommand = @sqlCommand+' AND s.ActivityID = '+CONVERT(nvarchar(32), @ActivityID);
        END;
        IF @SurveyID > 0
        BEGIN
            SET @sqlCommand = @sqlCommand+' AND s.SurveyID = '+CONVERT(nvarchar(32), @SurveyID);
        END;
        SET @Parameters = N'@p_SurveyTableType int,@p_OwnerID int';
        EXECUTE [sp_executesql] @sqlCommand, @Parameters, @p_SurveyTableType = @TableType_Survey, @p_OwnerID = @OwnerID;
    END;

    DECLARE c_ResultDatabase CURSOR READ_ONLY FOR
    SELECT DISTINCT [s].[ReportServer], [s].[ReportDB]
    FROM [at].[Survey] [s]
    JOIN [at].[Activity] [act] ON [act].[ActivityID] = [s].[ActivityID] AND [act].[OwnerID] = @OwnerID;
    
    DECLARE @LinkedDB nvarchar(max);
    OPEN c_ResultDatabase;
    FETCH NEXT FROM c_ResultDatabase INTO @ReportServer, @ReportDB;
    WHILE @@FETCH_STATUS = 0
    BEGIN
        IF @ReportDB = DB_NAME() AND @ReportServer = @@servername
        BEGIN
            SET @LinkedDB = ' ';
        END;
        ELSE
        BEGIN
            SET @LinkedDB = '['+@ReportServer+'].['+@ReportDB+'].';
        END;
        -- Get result except denied surveys
        SET @sqlCommand = N'INSERT INTO #UserResult (ResultID, ActivityID, SurveyID, BatchID, DepartmentID, RoleID, UserGroupID, StartDate, EndDate, StatusTypeID, ReportServer, ReportDB, ActivityNo, 
           LastUpdated, Created, ValidFrom, ValidTo, ParentResultID, ParentResultSurveyID, CustomerID, Deleted, EntityStatusID, EntityStatusReasonID, LastUpdatedBy, [ResultKey])
    SELECT ResultID, NULL, SurveyID, BatchID, DepartmentID, RoleID, UserGroupID, StartDate, EndDate, StatusTypeID, '''+@ReportServer+''', '''+@ReportDB+''', NULL, 
           LastUpdated, Created, ValidFrom, ValidTo, ParentResultID, ParentResultSurveyID, CustomerID, Deleted, EntityStatusID, EntityStatusReasonID, LastUpdatedBy, [ResultKey]
    FROM '+@LinkedDB+'dbo.Result r
    WHERE  NOT EXISTS (SELECT 1 FROM #DeniedSurveyList s WHERE s.DeniedSurvey = r.SurveyID)
      AND  r.UserID = @p_UserID
      AND  r.EntityStatusID = '+@ActiveEntityStatusID+' AND r.Deleted IS NULL ';
        IF @ActivityID > 0
        BEGIN
            SET @sqlCommand = @sqlCommand+' AND r.SurveyID IN (SELECT s1.SurveyID FROM at.Survey s1 WHERE s1.ActivityID = '+CONVERT(nvarchar(32), @ActivityID)+')';
        END;
        IF @SurveyID > 0
        BEGIN
            SET @sqlCommand = @sqlCommand+' AND r.SurveyID = '+CONVERT(nvarchar(32), @SurveyID);
        END;
        SET @Parameters = N'@p_UserID int';
        EXECUTE [sp_executesql] @sqlCommand, @Parameters, @p_UserID = @UserID;

        FETCH NEXT FROM c_ResultDatabase INTO @ReportServer, @ReportDB;
    END;
    CLOSE c_ResultDatabase;
    DEALLOCATE c_ResultDatabase;

    UPDATE [#UserResult] SET [ActivityID] = (SELECT [s].[ActivityID] FROM [at].[Survey] [s] WHERE [s].[SurveyID] = [ur].[SurveyID])
    FROM [#UserResult] [ur];

    DELETE FROM [#UserResult] WHERE [ActivityID] IS NULL;

    UPDATE [#UserResult] SET [ActivityNo] = (SELECT [a].[No] FROM [at].[Activity] [a] WHERE [a].[ActivityID] = [ur].[ActivityID]) FROM [#UserResult] [ur];

    SELECT * FROM [#UserResult] ORDER BY [ActivityNo], [StartDate] DESC;

    DROP TABLE [#DeniedSurveyList];
    DROP TABLE [#UserResult];

    SET @Err = @@Error;

    RETURN @Err;
END;
GO