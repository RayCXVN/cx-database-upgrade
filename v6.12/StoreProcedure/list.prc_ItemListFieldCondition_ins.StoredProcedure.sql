SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[list].[prc_ItemListFieldCondition_ins]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [list].[prc_ItemListFieldCondition_ins] AS' 
END
GO
ALTER PROCEDURE [list].[prc_ItemListFieldCondition_ins]
(
	@ItemListFieldConditionID int = null output,
	@ItemListFieldID int,
	@Type nvarchar(32),
	@Param nvarchar(MAX),
	@Value nvarchar(32),
    @cUserid int,
    @Log smallint = 1
)    
AS
BEGIN
	SET NOCOUNT ON;
	DECLARE @Err Int
	
	INSERT INTO [list].[ItemListFieldCondition]
           ([ItemListFieldID]
           ,[Type]
           ,[Param]
           ,[Value])
     VALUES
           (@ItemListFieldID
           ,@Type
           ,@Param
           ,@Value)
           
     Set @Err = @@Error
	 Set @ItemListFieldConditionID = scope_identity()
	 IF @Log = 1 
	 BEGIN 
		INSERT INTO [Log].[AuditLog] ( UserId, TableName, Type, Data, Created) 
		SELECT @cUserid,'ItemListFieldCondition',0,
		( SELECT * FROM [list].[ItemListFieldCondition]
			WHERE
			[ItemListFieldConditionID] = @ItemListFieldConditionID				 FOR XML AUTO) as data,
				getdate() 
	 END

	
	 RETURN @Err
END

GO
