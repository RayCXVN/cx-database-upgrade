SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[org].[prc_Owner_ins]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [org].[prc_Owner_ins] AS' 
END
GO
ALTER PROCEDURE [org].[prc_Owner_ins]
(
	@OwnerID int = null output,
	@LanguageID int,
	@Name nvarchar(256),
	@ReportServer varchar(64),
	@ReportDB varchar(64),
	@MainHierarchyID INT=NULL,
	@OLAPServer varchar(64),
	@OLAPDB varchar(64),
	@Css nvarchar(128),
	@Url nvarchar(128),
	@LoginType int,
	@Prefix nvarchar(8),
	@Description nvarchar(max),
	@Logging smallint,
	@cUserid int,
	@Log smallint = 1,
	@OTPLength		INT =5,
	@OTPCharacters nvarchar(512) = '',
	@OTPAllowLowercase BIT =1,
	@OTPAllowUppercase BIT = 1,
	@UseOTPCaseSensitive BIT =0,
	@UseHashPassword smallint =0,
	@UseOTP bit =0,
	@DefaultHashMethod int =2,
	@OTPDuration int =10
)
AS
BEGIN
	SET NOCOUNT ON
	DECLARE @Err Int

	INSERT INTO [org].[Owner]
	(
		[LanguageID],
		[Name],
		[ReportServer],
		[ReportDB],
		[MainHierarchyID],
		[OLAPServer],
		[OLAPDB],
		[Css],
		[Url],
		[LoginType],
		[Prefix],
		[Description],
		[Logging],
		OTPLength,
		OTPCharacters,
		OTPAllowLowercase,
		OTPAllowUppercase,
		UseOTPCaseSensitive,
		[UseHashPassword],
		[UseOTP],
		[DefaultHashMethod],
		[OTPDuration]
	)
	VALUES
	(
		@LanguageID,
		@Name,
		@ReportServer,
		@ReportDB,
		@MainHierarchyID,
		@OLAPServer,
		@OLAPDB,
		@Css,
		@Url,
		@LoginType,
		@Prefix,
		@Description,
		@Logging,
		@OTPLength,
		@OTPCharacters,
		@OTPAllowLowercase,
		@OTPAllowUppercase,
		@UseOTPCaseSensitive,
		@UseHashPassword,
		@UseOTP,
		@DefaultHashMethod,
		@OTPDuration
		
	)

	Set @Err = @@Error
	Set @OwnerID = scope_identity()

	IF @Log = 1 
	BEGIN 
		INSERT INTO [Log].[AuditLog] ( UserId, TableName, Type, Data, Created) 
		SELECT @cUserid,'Owner',0,
		( SELECT * FROM [org].[Owner] 
			WHERE
			[OwnerID] = @OwnerID				 FOR XML AUTO) as data,
				getdate() 
	 END

	RETURN @Err
END

GO
