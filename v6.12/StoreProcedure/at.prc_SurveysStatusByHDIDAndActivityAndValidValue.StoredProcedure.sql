SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[at].[prc_SurveysStatusByHDIDAndActivityAndValidValue]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [at].[prc_SurveysStatusByHDIDAndActivityAndValidValue] AS' 
END
GO
/*
	Steve - Sept 22 2016 - EntityStatus for table UserGroup
    Sarah - Oct 31, 2016 - Hot fix: Get EntityStatus value on QDB
	Johnny - Des 07 2016 - Update store procedure for dynamic SQL 
*/
ALTER PROCEDURE [at].[prc_SurveysStatusByHDIDAndActivityAndValidValue]
(
  @HDID                 int,
  @bSub                 bit = 0,
  @RoleID               varchar(max)=NULL,
  @ActivityIDList       varchar(max),
  @PeriodID             int = NULL,
  @DepartmentID         varchar(max)=NULL,
  @UserGroupID          varchar(max)=NULL,
  @DepTypeIDList        varchar(max)=NULL,
  @SurveyID             int = NULL,
  @IsDistinct           int=0,
  @PriorStatusTypeID    int =3,
  @ValidFrom            nvarchar(max) ='',
  @ValidTo              nvarchar(max) ='',
  @DistinctStatusTypeIDList varchar(max) = ''
)
AS
BEGIN
    SET NOCOUNT ON
   
    CREATE TABLE #ActivityList (ActivityID int)
    CREATE TABLE #SurveyList (ActivityID int, SurveyID int, ReportServer nvarchar(64), ReportDB nvarchar(64))
    CREATE TABLE #StatusTable (ActivityID int, HDID int, SurveyID int, DepartmentName nvarchar(256), StatusTypeID int, ResultCount int)
    
    DECLARE c_ReportDB CURSOR FORWARD_ONLY READ_ONLY FOR
    SELECT DISTINCT ReportServer, ReportDB FROM #SurveyList
    
    DECLARE @ActivityID int, @exclude_parentHD nvarchar(150), @ParentHD_clause nvarchar(512),
		  @ReportServer nvarchar(64), @ReportDB nvarchar(64),
		  @sqlCommand nvarchar(max), @parameters nvarchar(max), @HD_columns nvarchar(256), @GroupByHD nvarchar(256),
		  @ActiveEntityStatusID INT = 0, @LinkedDB NVARCHAR(MAX)

    SELECT @ActiveEntityStatusID = EntityStatusID FROM dbo.EntityStatus WHERE CodeName = 'Active'

    INSERT INTO #ActivityList (ActivityID) SELECT Value FROM dbo.funcListToTableInt(@ActivityIDList, ',')
    
    IF ISNULL(@PeriodID,0) = 0
    BEGIN
        INSERT INTO #SurveyList (ActivityID, SurveyID, ReportServer, ReportDB)
	   SELECT	  s.ActivityID, s.SurveyID, s.ReportServer, s.ReportDB
	   FROM	  at.Survey s
	   WHERE	  s.ActivityID IN (SELECT ActivityID FROM #ActivityList)
	     AND	  s.SurveyID = ISNULL(@SurveyID,s.SurveyID)
	     AND	  s.[Status] > 0
    END
    ELSE
    BEGIN
	   DECLARE c_ActivityList CURSOR FORWARD_ONLY READ_ONLY FOR
	   SELECT ActivityID FROM #ActivityList
	   
	   OPEN c_ActivityList
	     FETCH c_ActivityList INTO @ActivityID
	     WHILE @@FETCH_STATUS = 0
	     BEGIN
		  IF ISNULL(@SurveyID,0) = 0
		  BEGIN
			 INSERT INTO #SurveyList (ActivityID, SurveyID, ReportServer, ReportDB)
			 SELECT   TOP 1 s.ActivityID, s.SurveyID, s.ReportServer, s.ReportDB
			 FROM	at.Survey s
			 WHERE	s.ActivityID = @ActivityID
			   AND	s.PeriodID = @PeriodID
			   AND	s.[Status] > 0
			   AND    s.StartDate <= GETDATE() AND GETDATE() <= s.EndDate
			 ORDER BY s.EndDate DESC
		  END
		  ELSE
		  BEGIN
			 INSERT INTO #SurveyList (ActivityID, SurveyID, ReportServer, ReportDB)
			 SELECT   s.ActivityID, s.SurveyID, s.ReportServer, s.ReportDB
			 FROM	at.Survey s
			 WHERE	s.ActivityID = @ActivityID
			   AND	s.PeriodID = @PeriodID
			   AND	s.[Status] > 0
			   AND	s.SurveyID = ISNULL(@SurveyID,s.SurveyID)
			 ORDER BY s.EndDate DESC
	       END
	       FETCH c_ActivityList INTO @ActivityID
	     END
	   CLOSE c_ActivityList
	   DEALLOCATE c_ActivityList
    END

    OPEN c_ReportDB
      FETCH c_ReportDB INTO @ReportServer, @ReportDB
      WHILE @@FETCH_STATUS = 0
      BEGIN
	  IF @ReportDB=DB_NAME() AND @ReportServer=@@servername
				BEGIN
				SET @LinkedDB=' '
				END
			ELSE
				BEGIN
				SET @LinkedDB= '['+ @ReportServer + '].['+ @ReportDB + '].'
				END
	   IF @bSub <> 0
	   BEGIN
		  SET @HD_columns = CONVERT(nvarchar(16), @HDID) + ' AS HDID, ' + CONVERT(nvarchar(16), @HDID) + ' AS DepartmentName,'
		  SET @GroupByHD = ''
		  SET @exclude_parentHD = ''
		  SET @ParentHD_clause = ''
	   END
	   ELSE
	   BEGIN
		  SET @HD_columns = 'ISNULL(parent_hd.HDID, hd.HDID) AS HDID, ISNULL(parent_hd.Name, d.Name) AS DepartmentName,'
		  SET @GroupByHD = 'ISNULL(parent_hd.HDID, hd.HDID), ISNULL(parent_hd.Name, d.Name),'
		  SET @exclude_parentHD = ' AND hd.HDID <> ' + CONVERT(nvarchar(16), @HDID)
		  SET @ParentHD_clause = ' LEFT JOIN (SELECT phd.HDID, phd.[Path], d1.DepartmentID, d1.Name FROM org.H_D phd JOIN org.Department d1 ON d1.DepartmentID = phd.DepartmentID WHERE phd.ParentID=' + CONVERT(nvarchar(16), @HDID) + ') AS parent_hd ON hd.[Path] LIKE ''%'' + [parent_hd].[Path] + ''%'''
	   END

	   IF ISNULL(@UserGroupID,'') = '' AND ISNULL(@DepTypeIDList,'') = ''
	   BEGIN
		  SET @sqlCommand = N'INSERT INTO #StatusTable (HDID, DepartmentName, SurveyID, StatusTypeID, ResultCount)
		  SELECT ' + @HD_columns + ' r.SurveyID, ISNULL(r.StatusTypeID,0) AS StatusTypeID, r.UserID
		  FROM '+ @LinkedDB +'[dbo].[Result] r
		  JOIN org.Department d ON d.DepartmentID =  r.DepartmentID and r.EntityStatusID = ' + CONVERT(NVARCHAR(14), @ActiveEntityStatusID) + ' AND r.Deleted IS NULL AND d.EntityStatusID = ' + CONVERT(NVARCHAR(14), @ActiveEntityStatusID) + ' AND d.Deleted IS NULL
		  JOIN org.[User] u on u.UserID = r.UserID AND u.EntityStatusID = ' + CONVERT(NVARCHAR(14), @ActiveEntityStatusID) + ' AND u.Deleted IS NULL AND u.DepartmentID = r.DepartmentID
			  AND r.SurveyID IN (SELECT SurveyID FROM #SurveyList)'
    	  
		  IF(@DistinctStatusTypeIDList <> '')
		  BEGIN
			SET @sqlCommand += N' AND ((r.StatusTypeID IN (' + @DistinctStatusTypeIDList + ') AND r.ValidTo IS NULL) OR (r.StatusTypeID NOT IN (' + @DistinctStatusTypeIDList + ')))'
		  END
		   
		  IF ISNULL(@RoleID,'') != ''
		  BEGIN
			 SET @sqlCommand += N' AND r.RoleID IN (' + @RoleID + ')'
		  END
    	   
		  IF ISNULL(@DepartmentID,'') != ''
		  BEGIN
			 SET @sqlCommand += N' AND r.DepartmentID IN (' + @DepartmentID + ')'
		  END
	   	  IF(@ValidFrom<>'')
		  BEGIN
		    SET @sqlCommand +='AND cast(r.ValidFrom as datetime) >= cast(@p_ValidFrom as datetime2)'
		  END
		  IF(@ValidTo<>'')
		  BEGIN
		    SET @sqlCommand +='AND cast(r.ValidTo as datetime) <= cast(@p_ValidTo as datetime2)'
		  END 
		  SET @sqlCommand += N' JOIN org.H_D hd ON d.DepartmentID = hd.DepartmentID AND hd.Deleted = 0 AND hd.[PATH] LIKE ''%\' + CAST(@HDID AS VARCHAR(10)) + '\%''' + @exclude_parentHD + @ParentHD_clause + ''
		  
		  
		  --'GROUP BY ' + @GroupByHD + ' r.SurveyID, ISNULL(r.StatusTypeID,0)'
		  SET @parameters = N'@p_ValidFrom nvarchar(max), @p_ValidTo nvarchar(max)'
		  PRINT @sqlCommand
		  EXEC sp_executesql @sqlCommand, @parameters, @p_ValidFrom=@ValidFrom, @p_ValidTo=@ValidTo
		  
	   END
	   ELSE
	   BEGIN
		  SET @sqlCommand = N'INSERT INTO #StatusTable (HDID, DepartmentName, SurveyID, StatusTypeID, ResultCount)
		  SELECT ISNULL(parent_hd.HDID, r.UserGroupID) AS HDID, ISNULL(parent_hd.Name, ug.Name) AS DepartmentName, r.SurveyID, ISNULL(r.StatusTypeID,0) AS StatusTypeID, r.UserID
		  FROM '+ @LinkedDB +'[dbo].[Result] r
		  JOIN org.[User] u on u.UserID = r.UserID AND u.EntityStatusID = ' + CONVERT(NVARCHAR(14), @ActiveEntityStatusID) + ' AND u.Deleted IS NULL AND r.EntityStatusID = ' + CONVERT(NVARCHAR(14), @ActiveEntityStatusID) + ' AND r.Deleted IS NULL AND u.DepartmentID = r.DepartmentID
			  AND r.SurveyID IN (SELECT SurveyID FROM #SurveyList)'
    	   
		  IF(@DistinctStatusTypeIDList <> '')
		  BEGIN
			SET @sqlCommand += N' AND ((r.StatusTypeID IN (' + @DistinctStatusTypeIDList + ') AND r.ValidTo IS NULL) OR (r.StatusTypeID NOT IN (' + @DistinctStatusTypeIDList + ')))'
		  END

		  IF ISNULL(@RoleID,'') != ''
		  BEGIN
			 SET @sqlCommand += N' AND r.RoleID IN (' + @RoleID + ')'
		  END
    	   
		  IF ISNULL(@UserGroupID,'') != ''
		  BEGIN
			 SET @sqlCommand += N' AND r.UserGroupID IN (' + @UserGroupID + ')'
		  END
		  	  IF(@ValidFrom<>'')
		  BEGIN
		    SET @sqlCommand +='AND cast(r.ValidFrom as datetime) >= cast(@p_ValidFrom as datetime2)'
		  END
		  IF(@ValidTo<>'')
		  BEGIN
		    SET @sqlCommand +='AND cast(r.ValidTo as datetime) <= cast(@p_ValidTo as datetime2)'
		  END 
		  SET @sqlCommand += N' JOIN org.UserGroup ug ON ug.UserGroupID = r.UserGroupID AND ug.EntityStatusID = ' + CONVERT(NVARCHAR(14), @ActiveEntityStatusID) + ' AND ug.Deleted IS NULL AND ug.PeriodID = ' + CONVERT(nvarchar(16), @PeriodID) + '
		  JOIN org.H_D hd on ug.DepartmentID = hd.DepartmentID AND hd.[Path] LIKE ''%\' + CONVERT(nvarchar(16), @HDID) + '\%''' + @ParentHD_clause
		  
		  IF ISNULL(@DepTypeIDList,'') != ''
		  BEGIN
			 SET @sqlCommand += N' JOIN org.DT_UG dtug ON dtug.UserGroupID = ug.UserGroupID AND dtug.DepartmentTypeID IN (' + @DepTypeIDList + ')'
		  END
	   	   
		 -- SET @sqlCommand += N' GROUP BY ISNULL(parent_hd.HDID, r.UserGroupID), ISNULL(parent_hd.Name, ug.Name), r.SurveyID, ISNULL(r.StatusTypeID,0)'
		  --SET @parameters = N'@p_ReportServer nvarchar(64),@p_ReportDB nvarchar(64),@p_ValidFrom nvarchar(max), @p_ValidTo nvarchar(max)'
		  SET @parameters = N'@p_ValidFrom nvarchar(max), @p_ValidTo nvarchar(max)'
		  PRINT @sqlCommand
		  --EXEC sp_executesql @sqlCommand, @parameters, @p_ReportServer = @ReportServer, @p_ReportDB = @ReportDB, @p_ValidFrom=@ValidFrom, @p_ValidTo=@ValidTo
		  EXEC sp_executesql @sqlCommand, @parameters, @p_ValidFrom=@ValidFrom, @p_ValidTo=@ValidTo

	   END
	   
        FETCH c_ReportDB INTO @ReportServer, @ReportDB
      END
    CLOSE c_ReportDB
    DEALLOCATE c_ReportDB
    
    UPDATE #StatusTable SET ActivityID = (SELECT s.ActivityID FROM at.Survey s WHERE s.SurveyID = st.SurveyID) FROM #StatusTable st
    
    INSERT INTO #StatusTable (ActivityID, SurveyID, HDID, DepartmentName, StatusTypeID, ResultCount) 
    SELECT sl.ActivityID, sl.SurveyID, 0, '0', 1, 0
    FROM   #SurveyList sl 
    WHERE  NOT EXISTS (SELECT 1 FROM #StatusTable st WHERE st.ActivityID = sl.ActivityID AND st.SurveyID = sl.SurveyID)
    
    IF(@IsDistinct=0)
    BEGIN
        SELECT DISTINCT ActivityID, HDID,SurveyID,DepartmentName, StatusTypeID, count(ResultCount) as ResultCount  FROM 
		(
			SELECT DISTINCT ActivityID, HDID,SurveyID,DepartmentName, StatusTypeID, ResultCount  FROM #StatusTable WHERE ResultCount>0
		)  AS RESULT
		WHERE ResultCount>0 Group by ActivityID, HDID,SurveyID,DepartmentName, StatusTypeID ORDER BY ActivityID 
    END
    ELSE
    BEGIN
        Update #StatusTable SET SurveyID =0 --, StatusTypeID =0 WHERE StatusTypeID <> @PriorStatusTypeID
        DELETE st FROM  #StatusTable st  WHERE st.StatusTypeID <> @PriorStatusTypeID AND EXISTS  (SELECT 1 FROM #StatusTable st1 WHERE st1.StatusTypeID = @PriorStatusTypeID  AND st1.ActivityID = st.ActivityID AND st.ResultCount=st1.ResultCount ) 
        update #StatusTable SET StatusTypeID =0 WHERE StatusTypeID <> @PriorStatusTypeID
        CREATE TABLE #Counter ( ActivityID int, HDID int,SurveyID int,DepartmentName nvarchar(512), StatusTypeID int, ResultCount int )
        INSERT INTO #Counter
        SELECT DISTINCT ActivityID, HDID,SurveyID,DepartmentName, StatusTypeID, ResultCount  FROM #StatusTable WHERE StatusTypeID= @PriorStatusTypeID OR StatusTypeID =0
        SELECT ActivityID, HDID,SurveyID,DepartmentName, StatusTypeID,Count( ResultCount) as ResultCount from #Counter WHERE ResultCount>0 Group by ActivityID, HDID,SurveyID,DepartmentName, StatusTypeID ORDER BY ActivityID
        DROP TABLE #Counter
    END
    
    DROP TABLE #ActivityList
    DROP TABLE #SurveyList
    DROP TABLE #StatusTable
END

GO
