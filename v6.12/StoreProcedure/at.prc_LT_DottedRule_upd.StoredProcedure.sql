SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[at].[prc_LT_DottedRule_upd]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [at].[prc_LT_DottedRule_upd] AS' 
END
GO

ALTER PROCEDURE [at].[prc_LT_DottedRule_upd]
(
	@LanguageID int,
	@DottedRuleID int,
	@Name nvarchar(256),
	@Description nvarchar(max),
	@cUserid int,
	@Log smallint = 1
)
AS
BEGIN
	SET NOCOUNT ON
	DECLARE @Err Int

	UPDATE [at].[LT_DottedRule]
	SET
		[LanguageID] = @LanguageID,
		[DottedRuleID] = @DottedRuleID,
		[Name] = @Name,
		[Description] = @Description
	WHERE
		[LanguageID] = @LanguageID AND
		[DottedRuleID] = @DottedRuleID

	IF @Log = 1 
	BEGIN 
		INSERT INTO [Log].[AuditLog] ( UserId, TableName, Type, Data, Created) 
		SELECT @cUserid,'LT_DottedRule',1,
		( SELECT * FROM [at].[LT_DottedRule] 
			WHERE
			[LanguageID] = @LanguageID AND
			[DottedRuleID] = @DottedRuleID			 FOR XML AUTO) as data,
			getdate()
	END

	Set @Err = @@Error

	RETURN @Err
END


GO
