SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[rep].[prc_UserReportSetting_ins]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [rep].[prc_UserReportSetting_ins] AS' 
END
GO
ALTER PROCEDURE [rep].[prc_UserReportSetting_ins]
 @UserReportSettingID int = null output,
 @UserID int,
 @ReportID int,
 @ShowTable smallint = 1,
 @ShowChart smallint = 0,
 @ChartTypeID int = 9,
 @ChartTypeIDAvg int = 7,
 @cUserid int,
 @Log smallint = 1
AS
BEGIN
	SET NOCOUNT ON;
	DECLARE @Err Int
    
    INSERT INTO [rep].[UserReportSetting]
           ([UserID]
			,[ReportID]
			,[ShowTable]
			,[ShowChart]
			,[ChartTypeID]
			,[ChartTypeIDAvg])
     VALUES
           (@UserID
           ,@ReportID
           ,@ShowTable
           ,@ShowChart
           ,@ChartTypeID
           ,@ChartTypeIDAvg)
           
    Set @Err = @@Error
    Set @UserReportSettingID = scope_identity()
	IF @Log = 1 
	BEGIN 
		INSERT INTO [Log].[AuditLog] ( UserId, TableName, Type, Data, Created) 
		SELECT @cUserid,'UserReportSetting',0,
		( SELECT * FROM [rep].[UserReportSetting]
		  WHERE
			[UserReportSettingID] = @UserReportSettingID				 FOR XML AUTO) as data,
				getdate() 
	END
	
	RETURN @Err       
END

GO
