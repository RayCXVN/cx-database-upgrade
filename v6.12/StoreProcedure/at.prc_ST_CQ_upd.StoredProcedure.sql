SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[at].[prc_ST_CQ_upd]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [at].[prc_ST_CQ_upd] AS' 
END
GO

  
ALTER PROCEDURE [at].[prc_ST_CQ_upd]  
(  
 @ST_CQ_ID int,  
 @ScoreTemplateID int,  
 @CategoryID int = null,  
 @QuestionID int = null,  
 @AlternativeID int = null,
 @MinValue float,  
 @MaxValue float,  
 @cUserid int,  
 @Log smallint = 1,
 @ItemID int = NULL
)  
AS  
BEGIN  
 SET NOCOUNT ON  
 DECLARE @Err Int  
  
 UPDATE [at].[ST_CQ]  
 SET  
  [ScoreTemplateID] = @ScoreTemplateID,  
  [CategoryID] = @CategoryID,  
  [QuestionID] = @QuestionID,  
  [AlternativeID] = @AlternativeID,
  [MinValue] = @MinValue,  
  [MaxValue] = @MaxValue,
  [ItemID] = @ItemID
 WHERE  
  [ST_CQ_ID] = @ST_CQ_ID  
  
 IF @Log = 1   
 BEGIN   
  INSERT INTO [Log].[AuditLog] ( UserId, TableName, Type, Data, Created)   
  SELECT @cUserid,'ST_CQ',1,  
  ( SELECT * FROM [at].[ST_CQ]   
   WHERE  
   [ST_CQ_ID] = @ST_CQ_ID    FOR XML AUTO) as data,  
   getdate()  
 END  
  
 Set @Err = @@Error  
  
 RETURN @Err  
END  
  
/****** Object:  StoredProcedure [at].[prc_ST_CQ_ins]    Script Date: 12/01/2010 12:53:26 ******/  
SET ANSI_NULLS ON    

GO
