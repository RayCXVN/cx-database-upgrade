SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[at].[prc_SurveyQuestion_get]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [at].[prc_SurveyQuestion_get] AS' 
END
GO


/* ------------------------------------------------------------
   PROCEDURE:    prc_SurveyQuestion_get

   Description:  Selects records from the table 'prc_SurveyQuestion_get'

   AUTHOR:       LockwoodTech 11.07.2006 16:35:45
   ------------------------------------------------------------ */
ALTER PROCEDURE [at].[prc_SurveyQuestion_get]
(
	@PageID		int,
	@SurveyID	int
)
As
BEGIN
	SET NOCOUNT ON
	DECLARE @Err Int

	Select
	[QuestionID],
	[PageID],
	[No],
	[Type],
	[Inverted],
	[Mandatory],
	ISNULL([ScaleID], 0) ScaleID,
	ISNULL([SectionID], 0) SectionID,
	[Status],
	[CssClass],
	[ExtId],
	[Created]
	FROM at.[Question] Q
	WHERE Q.[PageID] = @PageID AND 
	(
	(@SurveyID IN (Select SurveyID FROM at.Access WHERE PageID IS NULL AND QuestionID IS NULL))
	OR
	(PageID IN (SELECT PageID FROM at.Access WHERE SurveyID = @SurveyID AND PageID = @PageID AND QuestionID IS NULL))
	OR
	(QuestionID IN (SELECT QuestionID FROM at.Access WHERE SurveyID = @SurveyID))
	)
	Set @Err = @@Error

	RETURN @Err
End


GO
