SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[proc].[prc_ProcessAnswerLog_get]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [proc].[prc_ProcessAnswerLog_get] AS' 
END
GO

ALTER PROCEDURE [proc].[prc_ProcessAnswerLog_get]
(
	@ProcessAnswerID int
)
AS
BEGIN
	SET NOCOUNT ON
	DECLARE @Err Int

	SELECT
	[ProcessAnswerLogID],
	[UserID],
	[ProcessAnswerID],
	[ProcessLevelID],
	[Comment],
	[Created]
	FROM [proc].[ProcessAnswerLog]
	WHERE
	[ProcessAnswerID] = @ProcessAnswerID

	Set @Err = @@Error

	RETURN @Err
END


GO
