SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[prc_Answer_get]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [dbo].[prc_Answer_get] AS' 
END
GO
  
/* ------------------------------------------------------------  
   PROCEDURE:    prc_Answer_get  
  
   Description:  Selects records from the table 'prc_Answer_get'  
  
   AUTHOR:       LockwoodTech 28.07.2006 13:01:45  
   ------------------------------------------------------------ */  
ALTER PROCEDURE [dbo].[prc_Answer_get]  
(  
 @ResultID int  
)  
As  
BEGIN  
 SET NOCOUNT ON  
 DECLARE @Err Int  
  
 Select  
 [AnswerID],  
 [ResultID],  
 [QuestionID],  
 [AlternativeID],  
 [Value],  
 [DateValue],  
 [Free],  
 [No],
 [ItemID]  
 FROM [Answer]  
 WHERE [ResultID] = @ResultID  
  
 Set @Err = @@Error  
  
 RETURN @Err  
End  
    


GO
