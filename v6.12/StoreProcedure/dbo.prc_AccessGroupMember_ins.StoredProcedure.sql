SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[prc_AccessGroupMember_ins]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [dbo].[prc_AccessGroupMember_ins] AS' 
END
GO
/*  2017-11-16 Johnny:      Add new column ArchetypeID in [dbo].[AccessGroupMember]
    2017-11-16 Ray:         Fix parameter data type of ArchetypeID, refactor
*/
ALTER PROCEDURE [dbo].[prc_AccessGroupMember_ins](
    @AccessGroupMemberID int      = NULL OUTPUT,
    @AccessGroupID       int,
    @UserID              int      = NULL,
    @UserTypeID          int      = NULL,
    @DepartmentID        int      = NULL,
    @HDID                int      = NULL,
    @UserGroupID         int      = NULL,
    @RoleID              int      = NULL,
    @DepartmentTypeID    int      = NULL,
    @DepartmentGroupID   int      = NULL,
    @CustomerID          int      = NULL,
    @ExtGroupID          int      = NULL,
    @cUserid             int,
    @Log                 smallint = 1,
    @ArchetypeID         int      = NULL)
AS
BEGIN
    SET NOCOUNT ON;
    DECLARE @Err int;

    INSERT INTO [dbo].[AccessGroupMember] ([AccessGroupID], [UserID], [UserTypeID], [DepartmentID], [HDID], [UserGroupID], [RoleID], [DepartmentTypeID],
                                           [DepartmentGroupID], [CustomerID], [ExtGroupID], [ArchetypeID])
    VALUES (@AccessGroupID, @UserID, @UserTypeID, @DepartmentID, @HDID, @UserGroupID, @RoleID, @DepartmentTypeID,
            @DepartmentGroupID, @CustomerID, @ExtGroupID, @ArchetypeID);

    SET @Err = @@Error;
    SET @AccessGroupMemberID = SCOPE_IDENTITY();

    IF @Log = 1
    BEGIN
        INSERT INTO [Log].[AuditLog] ([UserId], [TableName], [Type], [Data], [Created])
        SELECT @cUserid, 'AccessGroupMember', 0,
               (SELECT * FROM [dbo].[AccessGroupMember] WHERE [AccessGroupMemberID] = @AccessGroupMemberID FOR XML AUTO) AS [data],
               GETDATE();
    END;

    RETURN @Err;
END;
GO