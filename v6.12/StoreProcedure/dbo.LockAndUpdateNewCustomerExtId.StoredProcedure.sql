SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[LockAndUpdateNewCustomerExtId]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [dbo].[LockAndUpdateNewCustomerExtId] AS' 
END
GO
--select * from vokal.synonymdepartment where orgno ='975281922'
--history.deletejob 9334
-- exec [dbo].[GetUserInfo] @ssn='18059839345',@extid='',@lastname='',@Firstname='',@username='',@email='',@jobid=NULL
-- [dbo].[LockAndUpdateNewCustomerExtId] 9335
ALTER proc [dbo].[LockAndUpdateNewCustomerExtId](@jobid int)
 as
 begin
		declare @customerid int

		select @CustomerId=CustomerId  from history.JobLog where JobId=@jobid

		select customerid=@CustomerId,jobid=@jobid

		 delete hd from vokal.synonymh_d hd
			 join vokal.synonymdepartment d on d.departmentid=hd.departmentid
			 join vokal.synonymdt_d dt on dt.departmentid=d.departmentid and dt.departmenttypeid =37
			 where customerid =@customerid and d.status < 0
			 and not exists (select * from vokal.synonymresult r where r.statustypeid >=-1
			 and r.departmentid =d.departmentid)

		delete d from vokal.synonymdepartment d
			join vokal.synonymdt_d dt on dt.departmentid=d.departmentid and dt.departmenttypeid =37
			where customerid =@customerid and d.status < 0
			and not exists (select * from vokal.synonymresult r where r.statustypeid >=-1
			and r.departmentid =d.departmentid)


		update d
		set locked=1
		  from vokal.synonymdepartment d
		join vokal.synonymdt_d dt on dt.departmentid=d.departmentid and dt.departmenttypeid =37
		where customerid =@customerid


		update d
		set extid=s.SchoolName
		  from vokal.synonymdepartment d
		join vokal.synonymdt_d dt on dt.departmentid=d.departmentid and dt.departmenttypeid =36
		join tmp.Students s on s.jobid =@jobid and rtrim(s.OrganisationNo)=rtrim(d.orgno)  and ltrim(isnull(d.extid,'')) =''
		 where customerid =@customerid

		update d
		set extid=dschool.extid+':'+d.name from vokal.synonymdepartment d
		join vokal.synonymdt_d dt on dt.departmentid=d.departmentid and dt.departmenttypeid =37 and  d.customerid =@customerid
		join vokal.synonymH_d hd on hd.departmentid = d.departmentid 
		join vokal.synonymH_d hdSchool on hdSchool.hdid = hd.parentid  
		join vokal.synonymdt_d dtschool on dtschool.departmentid=hdSchool.departmentid and dtschool.departmenttypeid =36
		join  vokal.synonymdepartment dschool on dschool.departmentid = hdSchool.departmentid and  ltrim(isnull(dschool.extid,'')) <>''
 
		select distinct d.orgno,d.name,error='skolen finnes ikke på importen'  from vokal.synonymdepartment d
		join vokal.synonymdt_d dt on dt.departmentid=d.departmentid and dt.departmenttypeid =36 and d.status >=0
		left join tmp.Students s on s.jobid =@jobid and rtrim(s.OrganisationNo)=rtrim(d.orgno)  
		 where d.customerid =@customerid and s.jobid is null

		  
		select distinct OrganisationNo, s.SchoolName,error='skolen finnes ikke på vokal'  from tmp.Students s
			left join vokal.synonymdepartment d  on  d.customerid =@customerid  and rtrim(s.OrganisationNo)=rtrim(d.orgno)  
		 where  d.departmentid is null and s.jobid =@jobid
      
	    
		select u.userid,hd.pathname classname,s.* into #students from vokal.synonymdepartment d
		join tmp.Students s on s.jobid =@jobid and rtrim(s.SchoolName)+':'+s.Groupname=rtrim(d.extid) and d.customerid =@customerid
		join vokal.synonymuser u on u.departmentid =d.departmentid and rtrim(u.lastname) = rtrim(s.lastname) and rtrim(s.firstname)=rtrim(u.firstname)
		join vokal.synonymH_d hd  on hd.departmentid=d.departmentid

		insert into #students
		select u.userid,hd.pathname classname,s.* from vokal.synonymdepartment d
		join tmp.Students s on s.jobid =@jobid and rtrim(d.extid) like rtrim(s.SchoolName)+':%' and d.customerid =@customerid
		join vokal.synonymuser u on u.departmentid =d.departmentid and rtrim(u.lastname) = rtrim(s.lastname) and rtrim(s.firstname)=rtrim(u.firstname)
		join vokal.synonymH_d hd  on hd.departmentid=d.departmentid
		
		insert into #students
		select u.userid,hd.pathname classname, s.* from  tmp.Students s
		join vokal.synonymuser u on rtrim(u.lastname) = rtrim(s.lastname) and rtrim(s.firstname)=rtrim(u.firstname) and s.jobid =@jobid  
		join vokal.synonymdepartment d	 on u.departmentid =d.departmentid and rtrim(s.SchoolName)=d.extid and d.customerid =@customerid
		and not exists (select * from #students st where st.userid=u.userid)
		join vokal.synonymdepartment d1	 on d1.departmentid=u.departmentid
		join vokal.synonymH_d hd  on hd.departmentid=d1.departmentid
		
		select * from #students

			select distinct 'not found in import',hd.pathname Classname, d.extid, u.* 
			from vokal.synonymuser u
			join  vokal.synonymdepartment d on d.departmentid=u.departmentid and d.customerid=@customerid
			join vokal.synonymut_u ut on ut.userid =u.userid and ut.usertypeid=37
			join vokal.synonymH_d hd  on hd.departmentid=d.departmentid
			left join #students s on s.userid=u.userid
			where s.ssn is null and u.status=0 and u.userid in ( select userid  from vokal.synonymresult r where r.userid=u.userid and r.[StatusTypeID]>=0)
			order by u.firstname,u.lastname


			select distinct 'not found in vokal', * 
			from tmp.Students u
			left join #students s on s.ssn=u.ssn 
			where s.ssn is null and u.JobID=@jobid
			order by u.firstname,u.lastname

 end
 
 

GO
