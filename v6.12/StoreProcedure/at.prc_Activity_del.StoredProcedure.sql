SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[at].[prc_Activity_del]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [at].[prc_Activity_del] AS' 
END
GO
ALTER PROCEDURE [at].[prc_Activity_del] 
(
	@ActivityID INT,
	@cUserid INT,
	@Log SMALLINT = 1
)
AS
BEGIN
	SET NOCOUNT ON

	DECLARE @Err INT

	IF @Log = 1
	BEGIN
		INSERT INTO [Log].[AuditLog] (
			UserId,
			TableName,
			Type,
			Data,
			Created
			)
		SELECT @cUserid,
			'Activity',
			2,
			(
				SELECT *
				FROM [at].[Activity]
				WHERE [ActivityID] = @ActivityID
				FOR XML AUTO
				) AS data,
			getdate()
	END

	DELETE
	FROM [AT].[QA_Calc]
	WHERE Questionid IN (
			SELECT Questionid
			FROM AT.Page p
			JOIN AT.Question q ON q.pageid = p.pageid
				AND p.activityid = @ActivityID
			)

	DELETE
	FROM [AT].[CalcParameter]
	WHERE CategoryID IN (
			SELECT CategoryID
			FROM AT.Category c
			WHERE c.activityid = @ActivityID
			)

	DELETE
	FROM [at].[ActivityView_Q]
	WHERE CategoryID IN (
			SELECT CategoryID
			FROM AT.Category c
			WHERE c.activityid = @ActivityID
			)

	DELETE
	FROM [at].[Category]
	WHERE [ActivityID] = @ActivityID

	DELETE
	FROM [at].[Choice]
	WHERE [ActivityID] = @ActivityID

	DELETE
	FROM at.LevelGroup
	WHERE ActivityID = @ActivityID

     DELETE [at].[AGroup] WHERE [ActivityID] = @ActivityID;

	DELETE
	FROM [at].[Activity]
	WHERE [ActivityID] = @ActivityID

	SET @Err = @@Error

	RETURN @Err
END

GO
