SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[proc].[prc_Process_get]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [proc].[prc_Process_get] AS' 
END
GO

ALTER PROCEDURE [proc].[prc_Process_get]
(
    @ProcessGroupID int = null,
	@DepartmentID int = null,
	@ProcessTypeID int = null
)
AS
BEGIN
	SET NOCOUNT ON
	DECLARE @Err Int

	SELECT
	[ProcessID],
	[ProcessGroupID],
	[DepartmentID],
	[Active],
	[TargetValue],
	[UserID],
	ISNULL([CustomerID], 0) AS 'CustomerID',
	ISNULL([StartDate], 0) AS 'StartDate',
	ISNULL([DueDate], 0) AS 'DueDate',
	[Responsible],
	[URL],
	[No],
	ISNULL([PeriodID],0) as 'PeriodID',
	[Created],
	isnull(ProcessTypeID,0) As 'ProcessTypeID' ,
	isPublic 
	FROM [proc].[Process] P
	WHERE
	  (   
	    @DepartmentID IN  (Select Departmentid from [proc].ProcessAnswer PA where PA.ProcessID = P.ProcessID ) 
	    OR @DepartmentID = P.DepartmentID
	    OR @Departmentid is null
	    OR @DepartmentID IN (Select Departmentid from [proc].ProcessAccess PAS where PAS.ProcessID = P.ProcessID)    
	    )
	AND ([ProcessGroupID] = @ProcessGroupID OR @ProcessGroupID is null)
	AND ([ProcessTypeID] = @ProcessTypeID OR @ProcessTypeID is null)
	
	ORDER BY [No]

	Set @Err = @@Error

	RETURN @Err
END


GO
