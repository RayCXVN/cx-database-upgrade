SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[org].[prc_UserTypeMultiUsers]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [org].[prc_UserTypeMultiUsers] AS' 
END
GO
ALTER  PROCEDURE [org].[prc_UserTypeMultiUsers]
( 
	@ListUserID		nvarchar(128) ,
	@ListUserType	nvarchar(128) 
)
AS
BEGIN
	SET XACT_ABORT ON  
	BEGIN TRANSACTION  
	
	SET NOCOUNT ON
	DECLARE @UserID_table Table (id int)
	DECLARE @UserType_table Table ([type] int)
	INSERT INTO @UserID_table (id) SELECT value FROM dbo.funcListToTableInt(@ListUserID,',')
	INSERT INTO @UserType_table ([type]) SELECT value FROM dbo.funcListToTableInt(@ListUserType,',')
	
	INSERT INTO org.UT_U (UserTypeID,UserID)
	SELECT [type],id FROM @UserID_table CROSS JOIN @UserType_table
	WHERE NOT EXISTS (SELECT 1 FROM org.UT_U u WHERE u.UserID = id AND u.UserTypeID = type)
	COMMIT TRANSACTION
END

GO
