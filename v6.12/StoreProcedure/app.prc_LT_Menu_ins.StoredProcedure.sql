SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[app].[prc_LT_Menu_ins]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [app].[prc_LT_Menu_ins] AS' 
END
GO

ALTER PROCEDURE [app].[prc_LT_Menu_ins]
(
	@LanguageID int,
	@MenuID int,
	@Name nvarchar(256),
	@Description ntext,
	@Title nvarchar(256),
	@cUserid int,
	@Log smallint = 1
)
AS
BEGIN
	SET NOCOUNT ON
	DECLARE @Err Int

	INSERT INTO [app].[LT_Menu]
	(
		[LanguageID],
		[MenuID],
		[Name],
		[Description],
		[Title]
	)
	VALUES
	(
		@LanguageID,
		@MenuID,
		@Name,
		@Description,
		@Title
	)

	Set @Err = @@Error

	IF @Log = 1 
	BEGIN 
		INSERT INTO [Log].[AuditLog] ( UserId, TableName, Type, Data, Created) 
		SELECT @cUserid,'LT_Menu',0,
		( SELECT * FROM [app].[LT_Menu] 
			WHERE
			[LanguageID] = @LanguageID AND
			[MenuID] = @MenuID				 FOR XML AUTO) as data,
				getdate() 
	 END

	RETURN @Err
END


GO
