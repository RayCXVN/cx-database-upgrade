SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[at].[prc_StatusType_get]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [at].[prc_StatusType_get] AS' 
END
GO
/*
	2017-05-25 Sarah	Implement configuring StatusType's CodeName in ATAdmin for RelayLog
*/
ALTER PROCEDURE [at].[prc_StatusType_get]
(
	@OwnerID int
)
AS
BEGIN
	SET NOCOUNT ON
	DECLARE @Err Int

	SELECT
	[StatusTypeID],
	ISNULL([OwnerID], 0) AS 'OwnerID',
	[Type],
	[No],
	[Value],
	CodeName
	FROM [at].[StatusType]
	WHERE
	([OwnerID] = @OwnerID) OR ([OwnerID] IS NULL)
	ORDER BY [No]

	Set @Err = @@Error

	RETURN @Err
END


GO
