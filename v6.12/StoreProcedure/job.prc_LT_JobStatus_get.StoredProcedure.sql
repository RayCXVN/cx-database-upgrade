SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[job].[prc_LT_JobStatus_get]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [job].[prc_LT_JobStatus_get] AS' 
END
GO

ALTER PROCEDURE [job].[prc_LT_JobStatus_get]
(
	@JobStatusID smallint
)
AS
BEGIN
	SET NOCOUNT ON
	DECLARE @Err Int

	SELECT
	[LanguageID],
	[JobStatusID],
	[Name],
	[Description]
	FROM [job].[LT_JobStatus]
	WHERE
	[JobStatusID] = @JobStatusID

	Set @Err = @@Error

	RETURN @Err
END


GO
