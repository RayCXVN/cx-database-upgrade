SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[rep].[prc_DocumentItem_get]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [rep].[prc_DocumentItem_get] AS' 
END
GO


ALTER PROCEDURE [rep].[prc_DocumentItem_get]
(
	@DF_O_ID int
)
AS
BEGIN
	SET NOCOUNT ON
	DECLARE @Err Int

	SELECT
	[DocumentItemID],
	[DocumentItemTypeID],
	[DF_O_ID],
	[BGColor],
	[FGColor],
	[FontSize],
	[FontName],
	[FontIsBold],
	ISNULL([BorderColor], '') AS 'BorderColor'
	FROM [rep].[DocumentItem]
	WHERE
	[DF_O_ID] = @DF_O_ID

	Set @Err = @@Error

	RETURN @Err
END



GO
