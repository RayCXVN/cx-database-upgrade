SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[mea].[prc_LT_CheckListItemTemplate_get]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [mea].[prc_LT_CheckListItemTemplate_get] AS' 
END
GO
ALTER PROCEDURE [mea].[prc_LT_CheckListItemTemplate_get]  
(  
 @CheckListItemTemplateID int  
)  
AS  
BEGIN  
 SET NOCOUNT ON  
 DECLARE @Err Int  
  
 SELECT  
	   [LanguageID]  
      ,[CheckListItemTemplateID]  
      ,[Text] 
 FROM [mea].[LT_CheckListItemTemplate]  
 WHERE  
 [CheckListItemTemplateID] = @CheckListItemTemplateID  
  
 Set @Err = @@Error  
  
 RETURN @Err  
END  

GO
