SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[list].[prc_ItemListCommand_ins]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [list].[prc_ItemListCommand_ins] AS' 
END
GO
ALTER PROCEDURE [list].[prc_ItemListCommand_ins]
	@ItemListCommandID int = null output,
	@ItemListID int,
    @PlaceAbove bit,
    @PlaceBelow bit,
    @MultiCheckCommand bit,
    @DisabledInEmptyList bit,
    @CssClass nvarchar(256)='',
    @ClientFunction nvarchar(128),
    @cUserid int,
    @Log smallint = 1,
    @No int =0
AS
BEGIN
	SET NOCOUNT ON;
	DECLARE @Err Int
	
	INSERT INTO [list].[ItemListCommand]
           ([ItemListID]
           ,[PlaceAbove]
           ,[PlaceBelow]
           ,[MultiCheckCommand]
           ,[DisabledInEmptyList]
           ,[CssClass]
           ,[ClientFunction]
           ,[No])
     VALUES
           (@ItemListID
           ,@PlaceAbove
           ,@PlaceBelow
           ,@MultiCheckCommand
           ,@DisabledInEmptyList
           ,@CssClass
           ,@ClientFunction
           ,@No)
           
     Set @Err = @@Error
	 Set @ItemListCommandID = scope_identity()
	 IF @Log = 1 
	 BEGIN 
		INSERT INTO [Log].[AuditLog] ( UserId, TableName, Type, Data, Created) 
		SELECT @cUserid,'ItemListCommand',0,
		( SELECT * FROM [list].[ItemListCommand]
			WHERE
			[ItemListCommandID] = @ItemListCommandID				 FOR XML AUTO) as data,
				getdate() 
	 END

	
	 RETURN @Err
END

GO
