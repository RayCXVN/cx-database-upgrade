SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[at].[prc_BulkGroup_upd]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [at].[prc_BulkGroup_upd] AS' 
END
GO
ALTER PROCEDURE [at].[prc_BulkGroup_upd]
(
	@BulkGroupID int,
	@ActivityID int,
	@SurveyID int,
	@No smallint,
	@Css varchar(50),
	@Icon varchar(50),
	@Tag varchar(50),
	@cUserid int,
	@Log smallint = 1
)
AS
BEGIN
	SET NOCOUNT ON
	DECLARE @Err Int

	UPDATE [at].[BulkGroup]
	SET		
		[ActivityID] = @ActivityID,
		[SurveyID] = @SurveyID,
		[No] = @No,
		[Css] = @Css,
		[Icon] = @Icon,
		[Tag] = @Tag
	WHERE
		[BulkGroupID] = @BulkGroupID

	IF @Log = 1 
	BEGIN 
		INSERT INTO [Log].[AuditLog] ( UserId, TableName, Type, Data, Created) 
		SELECT @cUserid,'BulkGroup',1,
		( SELECT * FROM [at].[BulkGroup] 
			WHERE
			[BulkGroupID] = @BulkGroupID			 FOR XML AUTO) as data,
			getdate()
	END

	Set @Err = @@Error

	RETURN @Err
END



GO
