IF NOT EXISTS (SELECT 1 FROM sys.objects WHERE name = N'prc_UGMember_get' AND type IN ( N'P', N'PC' )  AND SCHEMA_NAME(schema_id) = 'org')
	EXEC ('CREATE PROC [org].[prc_UGMember_get] AS ')
GO
/*  
    2018-07-05  Sarah   ATAdmin/CXPLAT-294/Extend UG_UCXPLAT-854
*/
ALTER PROCEDURE [org].[prc_UGMember_get](
    @UserGroupID int)
AS
BEGIN
    SET NOCOUNT ON;
    DECLARE @Err int, @Unknown int= 0, @EntityStatusID_Active int= 1, @Inactive int= 2;
    SELECT @EntityStatusID_Active = [EntityStatusID] FROM [EntityStatus] WHERE [CodeName] = 'Active';
    SELECT @Unknown = [EntityStatusID] FROM [EntityStatus] WHERE [CodeName] = 'Unknown';
    SELECT @Inactive = [EntityStatusID] FROM [EntityStatus] WHERE [CodeName] = 'Inactive';

    SELECT [ug].*, [u].[DepartmentID], [u].[FirstName], [u].[LastName], [u].[Email], [u].[Mobile], [u].[ExtID], [u].[SSN], [u].[Tag], [u].[Locked],
            [u].[Gender], [u].[DateOfBirth], [u].[Created], [u].[ChangePassword], [u].[HashPassword], [u].[SaltPassword], [u].[OneTimePassword],
            [u].[OTPExpireTime], [u].[CountryCode], [u].[EntityStatusID], [u].[Deleted], [u].[EntityStatusReasonID]
    FROM [org].[UGMember] [ug]
    INNER JOIN [org].[User] [u] ON [ug].[UserID] = [u].[UserID]
    WHERE [ug].[UserGroupID] = @UserGroupID AND [ug].[EntityStatusID] IN (@EntityStatusID_Active, @Unknown, @Inactive) AND [ug].[Deleted] IS NULL;

    SET @Err = @@Error;

    RETURN @Err;
END;