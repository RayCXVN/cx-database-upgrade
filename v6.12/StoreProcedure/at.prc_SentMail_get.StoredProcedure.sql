SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[at].[prc_SentMail_get]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [at].[prc_SentMail_get] AS' 
END
GO

ALTER PROCEDURE [at].[prc_SentMail_get]
(
	@MailID int
)
AS
BEGIN
	SET NOCOUNT ON
	DECLARE @Err Int

	SELECT
	[SentMailID],
	[MailID],
	[LanguageID],
	[Type],
	[From],
	[Bcc],
	[StatusMail],
	[Status],
	ISNULL([Sent], 0) AS 'Sent',
	[Created]
	FROM [at].[SentMail]
	WHERE
	[MailID] = @MailID

	Set @Err = @@Error

	RETURN @Err
END


GO
