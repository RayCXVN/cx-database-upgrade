SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[form].[prc_LT_FormCommand_upd]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [form].[prc_LT_FormCommand_upd] AS' 
END
GO

ALTER PROCEDURE [form].[prc_LT_FormCommand_upd]
(
	@LanguageID int,
	@FormCommandID int,
	@Name nvarchar(256),
	@ErrorMsg nvarchar(max),
	@ConfirmMsg nvarchar(max),
	@Description nvarchar(max),
	@cUserid int,
	@Log smallint = 1
)
AS
BEGIN
	SET NOCOUNT ON
	DECLARE @Err Int

	UPDATE [form].[LT_FormCommand]
	SET
		[LanguageID] = @LanguageID,
		[FormCommandID] = @FormCommandID,
		[Name] = @Name,
		[ErrorMsg] = @ErrorMsg,
		[ConfirmMsg] = @ConfirmMsg,
		[Description] = @Description
	WHERE
		[LanguageID] = @LanguageID AND
		[FormCommandID] = @FormCommandID

	IF @Log = 1 
	BEGIN 
		INSERT INTO [Log].[AuditLog] ( UserId, TableName, Type, Data, Created) 
		SELECT @cUserid,'LT_FormCommand',1,
		( SELECT * FROM [form].[LT_FormCommand] 
			WHERE
			[LanguageID] = @LanguageID AND
			[FormCommandID] = @FormCommandID			 FOR XML AUTO) as data,
			getdate()
	END

	Set @Err = @@Error

	RETURN @Err
END


GO
