SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[at].[prc_QA_Calc_get]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [at].[prc_QA_Calc_get] AS' 
END
GO

ALTER PROCEDURE [at].[prc_QA_Calc_get]
(
  @ActivityID int
)
AS
BEGIN
	SET NOCOUNT ON
	DECLARE @Err Int

	SELECT
	qa.[QAID],
	qa.[QuestionID],
	qa.[AlternativeID],
	qa.[CFID],
	qa.[MinValue],
	qa.[MaxValue],
	qa.[No],
	qa.[NewValue],
	qa.[UseNewValue],
	qa.[ItemID],
	qa.[Created],
	qa.[UseCalculatedValue],
	isnull(qa.Roleid,0) 'RoleID'
	FROM [at].[QA_Calc] qa
	JOIN [at].[Question] q on q.questionid = qa.questionid
	JOIN [at].[Page] p on p.pageid = q.pageid
	JOIN [at].[Activity] a on a.activityID = p.ActivityID and a.activityID = @ActivityID
	ORDER BY [No]

	Set @Err = @@Error

	RETURN @Err
END




GO
