SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[list].[prc_ItemList_get]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [list].[prc_ItemList_get] AS' 
END
GO
ALTER PROCEDURE [list].[prc_ItemList_get]
	@OwnerID int
AS
BEGIN
	SET NOCOUNT ON;
	DECLARE @Err Int
	
    SELECT 
		[ItemListID],
		[OwnerID],
		[ItemListDataSourceID],
		[ListTableCSSClass],
		[ListTableCSSCStyle],
		[ListHeaderRowCSSClass],
		[ListRowCSSClass],
		[ListAltRowCSSClass],
		[ExpandingItemListDatasourceID],
		[ExpandingListRowCSSClass],
		[ExpandingListAltRowCSSClass],
		[ExpandingExpression],
		[InsertMultiCheckColumn],
		[DisableCheckColumnExpression],
		[DisplayUpperCmdBarThreshold],
		[MaxRows],
		[ExtID],
		[Created]
	FROM [list].[ItemList]
	WHERE [OwnerID] = @OwnerID
	
	Set @Err = @@Error

	RETURN @Err
  
END

GO
