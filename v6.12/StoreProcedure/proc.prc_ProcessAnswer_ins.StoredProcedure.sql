SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[proc].[prc_ProcessAnswer_ins]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [proc].[prc_ProcessAnswer_ins] AS' 
END
GO

ALTER PROCEDURE [proc].[prc_ProcessAnswer_ins]
(
	@ProcessAnswerID int = null output,
	@ProcessID int,
	@ProcessLevelID int,
	@DepartmentID int,
	@RoleID INT=NULL,
	@UserID INT=NULL,
	@LastModified datetime,
	@cUserid int,
	@Log smallint = 1
)
AS
BEGIN
	SET NOCOUNT ON
	DECLARE @Err Int

	INSERT INTO [proc].[ProcessAnswer]
	(
		[ProcessID],
		[ProcessLevelID],
		[DepartmentID],
		[RoleID],
		[UserID],
		[LastModified]
	)
	VALUES
	(
		@ProcessID,
		@ProcessLevelID,
		@DepartmentID,
		@RoleID,
		@UserID,
		@LastModified
	)

	Set @Err = @@Error
	Set @ProcessAnswerID = scope_identity()

	IF @Log = 1 
	BEGIN 
		INSERT INTO [Log].[AuditLog] ( UserId, TableName, Type, Data, Created) 
		SELECT @cUserid,'ProcessAnswer',0,
		( SELECT * FROM [proc].[ProcessAnswer] 
			WHERE
			[ProcessAnswerID] = @ProcessAnswerID				 FOR XML AUTO) as data,
				getdate() 
	 END

	RETURN @Err
END


GO
