SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[at].[prc_Q_C_ins]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [at].[prc_Q_C_ins] AS' 
END
GO
ALTER PROCEDURE [at].[prc_Q_C_ins]    
(    
 @QuestionID int,    
 @CategoryID int,    
 @Weight float,    
 @No smallint,    
 @Visible smallint,    
 @cUserid int,    
 @Log smallint = 1,
 @ItemID int = NULL
)    
AS    
BEGIN    
 SET NOCOUNT ON    
 DECLARE @Err Int    
    
 INSERT INTO [at].[Q_C]    
 (    
  [QuestionID],    
  [CategoryID],    
  [Weight],    
  [Visible],  
  [No],
  [ItemID]    
 )    
 VALUES    
 (    
  @QuestionID,    
  @CategoryID,    
  @Weight,    
  @Visible,  
  @No,
  @ItemID    
 )    
    
 Set @Err = @@Error    
    
 IF @Log = 1     
 BEGIN     
  INSERT INTO [Log].[AuditLog] ( UserId, TableName, Type, Data, Created)     
  SELECT @cUserid,'Q_C',0,    
  ( SELECT * FROM [at].[Q_C]     
   WHERE    
   [QuestionID] = @QuestionID AND    
   [CategoryID] = @CategoryID     FOR XML AUTO) as data,    
    getdate()     
  END    
    
 RETURN @Err    
END    
    

GO
