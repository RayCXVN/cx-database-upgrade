SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[proc].[prc_ProcessAnswerLog_upd]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [proc].[prc_ProcessAnswerLog_upd] AS' 
END
GO

ALTER PROCEDURE [proc].[prc_ProcessAnswerLog_upd]
(
	@ProcessAnswerLogID int,
	@UserID int,
	@ProcessAnswerID int,
	@ProcessLevelID int,
	@Comment nvarchar(max),
	@cUserid int,
	@Log smallint = 1
)
AS
BEGIN
	SET NOCOUNT ON
	DECLARE @Err Int

	UPDATE [proc].[ProcessAnswerLog]
	SET
		[UserID] = @UserID,
		[ProcessAnswerID] = @ProcessAnswerID,
		[ProcessLevelID] = @ProcessLevelID,
		[Comment] = @Comment
	WHERE
		[ProcessAnswerLogID] = @ProcessAnswerLogID

	IF @Log = 1 
	BEGIN 
		INSERT INTO [Log].[AuditLog] ( UserId, TableName, Type, Data, Created) 
		SELECT @cUserid,'ProcessAnswerLog',1,
		( SELECT * FROM [proc].[ProcessAnswerLog] 
			WHERE
			[ProcessAnswerLogID] = @ProcessAnswerLogID			 FOR XML AUTO) as data,
			getdate()
	END

	Set @Err = @@Error

	RETURN @Err
END


GO
