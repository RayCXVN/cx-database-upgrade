SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[at].[prc_StatusTypeMapping_ins]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [at].[prc_StatusTypeMapping_ins] AS' 
END
GO

ALTER PROCEDURE [at].[prc_StatusTypeMapping_ins]
(
	@StatusTypeMappingID int = null output,
	@ActivityID int,
	@CustomerID INT=NULL,
	@FromStatusTypeID int = null,
	@ToStatusTypeID int,
	@No smallint,
	@ShowActionInUI bit =0,
	@Settings XML ='',
	@cUserid int,
	@Log smallint = 1
)
AS
BEGIN
	SET NOCOUNT ON
	DECLARE @Err Int

	INSERT INTO [at].[StatusTypeMapping]
	(
		[ActivityID],
		[CustomerID],
		[FromStatusTypeID],
		[ToStatusTypeID],
		[No],
		[ShowActionInUI],
		[Settings]
	)
	VALUES
	(
		@ActivityID,
		@CustomerID,
		@FromStatusTypeID,
		@ToStatusTypeID,
		@No,
		@ShowActionInUI,
		@Settings
	)

	Set @Err = @@Error
	Set @StatusTypeMappingID = scope_identity()

	IF @Log = 1 
	BEGIN 
		INSERT INTO [Log].[AuditLog] ( UserId, TableName, Type, Data, Created) 
		SELECT @cUserid,'StatusTypeMapping',0,
		( SELECT * FROM [at].[StatusTypeMapping] 
			WHERE
			[StatusTypeMappingID] = @StatusTypeMappingID				 FOR XML AUTO) as data,
				getdate() 
	 END

	RETURN @Err
END

GO
