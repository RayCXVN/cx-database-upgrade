SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[rep].[prc_LT_ReportPartType_upd]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [rep].[prc_LT_ReportPartType_upd] AS' 
END
GO

ALTER PROCEDURE [rep].[prc_LT_ReportPartType_upd]
(
	@ReportPartTypeID int,
	@LanguageID int,
	@Name nvarchar(256),
	@Description nvarchar(max),
	@cUserid int,
	@Log smallint = 1
)
AS
BEGIN
	SET NOCOUNT ON
	DECLARE @Err Int

	UPDATE [rep].[LT_ReportPartType]
	SET
		[ReportPartTypeID] = @ReportPartTypeID,
		[LanguageID] = @LanguageID,
		[Name] = @Name,
		[Description] = @Description
	WHERE
		[ReportPartTypeID] = @ReportPartTypeID AND
		[LanguageID] = @LanguageID

	IF @Log = 1 
	BEGIN 
		INSERT INTO [Log].[AuditLog] ( UserId, TableName, Type, Data, Created) 
		SELECT @cUserid,'LT_ReportPartType',1,
		( SELECT * FROM [rep].[LT_ReportPartType] 
			WHERE
			[ReportPartTypeID] = @ReportPartTypeID AND
			[LanguageID] = @LanguageID			 FOR XML AUTO) as data,
			getdate()
	END

	Set @Err = @@Error

	RETURN @Err
END


GO
