SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[rep].[prc_LT_Report_ins]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [rep].[prc_LT_Report_ins] AS' 
END
GO

ALTER PROCEDURE [rep].[prc_LT_Report_ins]
(
	@LanguageID int,
	@ReportID int,
	@Name nvarchar(256),
	@Text nvarchar(max),
	@cUserid int,
	@Log smallint = 1
)
AS
BEGIN
	SET NOCOUNT ON
	DECLARE @Err Int

	INSERT INTO [rep].[LT_Report]
	(
		[LanguageID],
		[ReportID],
		[Name],
		[Text]
	)
	VALUES
	(
		@LanguageID,
		@ReportID,
		@Name,
		@Text
	)

	Set @Err = @@Error

	IF @Log = 1 
	BEGIN 
		INSERT INTO [Log].[AuditLog] ( UserId, TableName, Type, Data, Created) 
		SELECT @cUserid,'LT_Report',0,
		( SELECT * FROM [rep].[LT_Report] 
			WHERE
			[LanguageID] = @LanguageID AND
			[ReportID] = @ReportID				 FOR XML AUTO) as data,
				getdate() 
	 END

	RETURN @Err
END


GO
