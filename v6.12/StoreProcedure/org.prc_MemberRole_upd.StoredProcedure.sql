IF NOT EXISTS (SELECT 1 FROM sys.objects WHERE name = N'prc_MemberRole_upd' AND type IN ( N'P', N'PC' )  AND SCHEMA_NAME(schema_id) = 'org')
    EXEC ('CREATE PROC [org].[prc_MemberRole_upd] AS ')
GO
/*  
    2018-07-19 Sarah    Create procedures for Insert, Update, Delete, Get of org.MemberRole
*/
ALTER PROCEDURE [org].[prc_MemberRole_upd]
(  
    @MemberRoleID int,
    @OwnerID int,
    @ExtID nvarchar(max),
    @No int ,
    @Created smalldatetime,
    @EntityStatusID int,
    @EntityStatusReasonID int,
    @cUserid int,
    @Log smallint = 1
)  
AS  
BEGIN  
 SET NOCOUNT ON  
 DECLARE @Err Int  

 UPDATE [org].[MemberRole]
 SET 
    OwnerID = @OwnerID, 
    ExtID = @ExtID, 
    [No] = @No, 
    Created = @Created, 
    EntityStatusID = @EntityStatusID, 
    EntityStatusReasonID = @EntityStatusReasonID
 WHERE
    MemberRoleID = @MemberRoleID

 Set @Err = @@Error  

 IF @Log = 1 
 BEGIN 
    INSERT INTO [Log].[AuditLog] ( UserId, TableName, [Type], Data, Created) 
    SELECT @cUserid,'MemberRole',1,
    ( SELECT * FROM [org].[MemberRole] 
       WHERE MemberRoleID = @MemberRoleID
       FOR XML AUTO
    ) as data,
    getdate() 
 END  

 RETURN @Err  
END