SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[at].[prc_ActivityView_upd]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [at].[prc_ActivityView_upd] AS' 
END
GO
/*  
	2017-06-13 Steve: Change the length of column ExtID in all related tables from xAPI format
*/
ALTER PROCEDURE [at].[prc_ActivityView_upd]
(
	@ActivityViewID int,
	@DepartmentID int,
	@CustomerID int,
	@ActivityID int,
	@cUserid int,
	@Log smallint = 1,
	@Created DATETIME = NULL,
	@ExtID nvarchar(256)=''
)
AS
BEGIN
	SET NOCOUNT ON
	DECLARE @Err Int

	SET @Created = ISNULL(@Created, GETDATE())

	UPDATE [at].[ActivityView]
	SET
		[DepartmentID] = @DepartmentID,
		[CustomerID] = @CustomerID,
		[ActivityID] = @ActivityID,
		[Created] = @Created,
		[ExtId] = @ExtID
	WHERE
		[ActivityViewID] = @ActivityViewID

	IF @Log = 1 
	BEGIN 
		INSERT INTO [Log].[AuditLog] ( UserId, TableName, Type, Data, Created) 
		SELECT @cUserid,'ActivityView',1,
		( SELECT * FROM [at].[ActivityView] 
			WHERE
			[ActivityViewID] = @ActivityViewID			 FOR XML AUTO) as data,
			getdate()
	END

	Set @Err = @@Error

	RETURN @Err
END

GO
