SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[RebuildViews]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [dbo].[RebuildViews] AS' 
END
GO
ALTER PROCEDURE [dbo].[RebuildViews]
(
	@Old_SourceDB	nvarchar(100),
	@New_SourceDB	nvarchar(100)
)
AS
BEGIN
	SET NOCOUNT ON;

    DECLARE c_views CURSOR LOCAL FOR
    SELECT	table_name, view_definition
	FROM	INFORMATION_SCHEMA.VIEWS
    ORDER BY table_name

	DECLARE @view_name			NVARCHAR(200)
	DECLARE @view_definition	NVARCHAR(max)
	DECLARE @cmd				NVARCHAR(max)

	OPEN  c_views
	FETCH NEXT FROM c_views INTO @view_name, @view_definition
	WHILE @@FETCH_STATUS = 0
	BEGIN
        IF CHARINDEX(@Old_SourceDB + '.', @view_definition) > 0 OR CHARINDEX('[' + @Old_SourceDB + '].', @view_definition) > 0
        BEGIN
            SET @cmd = replace(@view_definition, 'CREATE VIEW ', 'ALTER VIEW ')
            SET @cmd = replace(@cmd, @Old_SourceDB + '.', '[' + @New_SourceDB + '].')
		    SET @cmd = replace(@cmd, '[' + @Old_SourceDB + '].', '[' + @New_SourceDB + '].')
		    EXECUTE (@cmd)
        END
        ELSE
        BEGIN
            PRINT 'Skip view ' + @view_name
        END

		FETCH NEXT FROM c_views INTO @view_name, @view_definition
	END

	CLOSE c_views
	DEALLOCATE c_views

END

GO
