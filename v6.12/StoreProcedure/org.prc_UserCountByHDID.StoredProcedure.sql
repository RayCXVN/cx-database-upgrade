SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[org].[prc_UserCountByHDID]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [org].[prc_UserCountByHDID] AS' 
END
GO
 ALTER PROC [org].[prc_UserCountByHDID]
(
  @HDID INT
)
AS
BEGIN
    DECLARE @ActiveEntityStatusID INT = (SELECT EntityStatusID FROM EntityStatus WHERE CodeName='Active')

    SELECT count(u.userid) as 'Antall' FROM org.H_D hd 
    JOIN org.department d ON hd.PATH LIKE '%\' + cast(@HDID as nvarchar(16)) + '\%' AND d.departmentid = hd.departmentid 
    JOIN org.[USER] u ON u.departmentid = d.departmentid
    WHERE u.EntityStatusID = @ActiveEntityStatusID AND u.Deleted IS NULL
END

GO
