SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[form].[prc_FormRow_upd]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [form].[prc_FormRow_upd] AS' 
END
GO

ALTER PROCEDURE [form].[prc_FormRow_upd]
(
	@FormRowID int,
	@FormID int,
	@No smallint,
	@RowTypeID smallint,
	@CssClass nvarchar(128),
	@cUserid int,
	@Log smallint = 1
)
AS
BEGIN
	SET NOCOUNT ON
	DECLARE @Err Int

	UPDATE [form].[FormRow]
	SET
		[FormID] = @FormID,
		[No] = @No,
		[RowTypeID] = @RowTypeID,
		[CssClass] = @CssClass
	WHERE
		[FormRowID] = @FormRowID

	IF @Log = 1 
	BEGIN 
		INSERT INTO [Log].[AuditLog] ( UserId, TableName, Type, Data, Created) 
		SELECT @cUserid,'FormRow',1,
		( SELECT * FROM [form].[FormRow] 
			WHERE
			[FormRowID] = @FormRowID			 FOR XML AUTO) as data,
			getdate()
	END

	Set @Err = @@Error

	RETURN @Err
END


GO
