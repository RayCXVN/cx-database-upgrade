SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[prc_Result_SetStatus_For_HD]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [dbo].[prc_Result_SetStatus_For_HD] AS' 
END
GO
/*  2016-10-10 Ray: Remove column Status from org.User, org.Department, RDB.dbo.Result
*/
ALTER PROCEDURE [dbo].[prc_Result_SetStatus_For_HD]
(
 @SurveyID INT,
 @HDID INT,
 @EntityStatusID INT
)
AS
BEGIN
    UPDATE dbo.RESULT SET [EntityStatusID] = @EntityStatusID 
    WHERE SurveyId = @SurveyID 
    AND departmentid IN 
    ( SELECT departmentid FROM org.H_D WHERE PATH LIKE '%\' +  CAST(@HDID AS VARCHAR(16)) + '\%' )
END


GO
