SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[form].[prc_FormRow_get]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [form].[prc_FormRow_get] AS' 
END
GO

ALTER PROCEDURE [form].[prc_FormRow_get]
(
	@FormID int
)
AS
BEGIN
	SET NOCOUNT ON
	DECLARE @Err Int

	SELECT
	[FormRowID],
	[FormID],
	[No],
	[RowTypeID],
	[CssClass],
	[Created]
	FROM [form].[FormRow]
	WHERE
	[FormID] = @FormID
	ORDER BY [No]

	Set @Err = @@Error

	RETURN @Err
END


GO
