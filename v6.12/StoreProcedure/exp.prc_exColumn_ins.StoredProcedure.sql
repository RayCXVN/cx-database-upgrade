SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[exp].[prc_exColumn_ins]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [exp].[prc_exColumn_ins] AS' 
END
GO

ALTER PROCEDURE [exp].[prc_exColumn_ins]
(
	@ColumnID smallint = null output,
	@TableTypeID smallint,
	@PropertyName nvarchar(64),
	@DataType nvarchar(64),
	@No smallint,
	@FriendlyName nvarchar(256),
	@cUserid int,
	@Log smallint = 1
)
AS
BEGIN
	SET NOCOUNT ON
	DECLARE @Err Int

	INSERT INTO [exp].[exColumn]
	(
		[TableTypeID],
		[PropertyName],
		[DataType],
		[No],
		[FriendlyName]
	)
	VALUES
	(
		@TableTypeID,
		@PropertyName,
		@DataType,
		@No,
		@FriendlyName
	)

	Set @Err = @@Error
	Set @ColumnID = scope_identity()

	IF @Log = 1 
	BEGIN 
		INSERT INTO [Log].[AuditLog] ( UserId, TableName, Type, Data, Created) 
		SELECT @cUserid,'exColumn',0,
		( SELECT * FROM [exp].[exColumn] 
			WHERE
			[ColumnID] = @ColumnID				 FOR XML AUTO) as data,
				getdate() 
	 END

	RETURN @Err
END


GO
