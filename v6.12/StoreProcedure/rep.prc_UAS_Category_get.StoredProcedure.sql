SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[rep].[prc_UAS_Category_get]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [rep].[prc_UAS_Category_get] AS' 
END
GO
ALTER PROCEDURE [rep].[prc_UAS_Category_get]
 @UserActivitySettingID int
AS      
BEGIN      
 SET NOCOUNT ON;      
 DECLARE @Err Int          
 SELECT       
	 [UserActivitySettingID]
	,[CategoryID]
 FROM             
  [rep].[UAS_Category]     
 WHERE        
  [UserActivitySettingID] = @UserActivitySettingID      
 Set @Err = @@Error      
      
 RETURN @Err      
END 

GO
