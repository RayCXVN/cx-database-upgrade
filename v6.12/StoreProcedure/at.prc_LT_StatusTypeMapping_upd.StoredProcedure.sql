SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[at].[prc_LT_StatusTypeMapping_upd]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [at].[prc_LT_StatusTypeMapping_upd] AS' 
END
GO

ALTER PROCEDURE [at].[prc_LT_StatusTypeMapping_upd]
(
	@LanguageID int,
	@StatusTypeMappingID int,
    @ActionName Nvarchar(512) ='',
    @ActionDescription NVARCHAR(MAX)='',
	@cUserid int,
	@Log smallint = 1
)
AS
BEGIN
	SET NOCOUNT ON
	DECLARE @Err Int
	UPDATE [at].[LT_StatusTypeMapping]
	SET
		[LanguageID] = @LanguageID,
		[StatusTypeMappingID] = @StatusTypeMappingID,
		[ActionName]=@ActionName,
		[ActionDescription]=@ActionDescription
	WHERE
		[LanguageID] = @LanguageID AND
		[StatusTypeMappingID] = @StatusTypeMappingID
	IF @Log = 1 
	BEGIN 
		INSERT INTO [Log].[AuditLog] ( UserId, TableName, Type, Data, Created) 
		SELECT @cUserid,'LT_StatusTypeMapping',1,
		( SELECT * FROM [at].[LT_StatusTypeMapping] 
			WHERE
			[LanguageID] = @LanguageID AND
			[StatusTypeMappingID] = @StatusTypeMappingID			 FOR XML AUTO) as data,
			getdate()
	END
	Set @Err = @@Error
	RETURN @Err
END

GO
