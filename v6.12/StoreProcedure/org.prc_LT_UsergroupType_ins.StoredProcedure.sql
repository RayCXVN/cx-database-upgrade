SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[org].[prc_LT_UsergroupType_ins]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [org].[prc_LT_UsergroupType_ins] AS' 
END
GO

ALTER PROCEDURE [org].[prc_LT_UsergroupType_ins]
(
	@LanguageID int,
	@UsergroupTypeID int,
	@Name nvarchar(256),
	@Description nvarchar(max),
	@cUserid int,
	@Log smallint = 1
)
AS
BEGIN
	SET NOCOUNT ON
	DECLARE @Err Int

	INSERT INTO [org].[LT_UsergroupType]
	(
		[LanguageID],
		[UsergroupTypeID],
		[Name],
		[Description]
	)
	VALUES
	(
		@LanguageID,
		@UsergroupTypeID,
		@Name,
		@Description
	)

	Set @Err = @@Error

	IF @Log = 1 
	BEGIN 
		INSERT INTO [Log].[AuditLog] ( UserId, TableName, Type, Data, Created) 
		SELECT @cUserid,'LT_UsergroupType',0,
		( SELECT * FROM [org].[LT_UsergroupType] 
			WHERE
			[LanguageID] = @LanguageID AND
			[UsergroupTypeID] = @UsergroupTypeID				 FOR XML AUTO) as data,
				getdate() 
	 END

	RETURN @Err
END


GO
