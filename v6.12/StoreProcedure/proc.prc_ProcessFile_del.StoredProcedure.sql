SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[proc].[prc_ProcessFile_del]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [proc].[prc_ProcessFile_del] AS' 
END
GO

ALTER PROCEDURE [proc].[prc_ProcessFile_del]
(
	@ProcessFileId int,
	@cUserid int,
	@Log smallint = 1
)
AS
BEGIN
	SET NOCOUNT ON
	DECLARE @Err Int

	IF @Log = 1 
	BEGIN 
		INSERT INTO [Log].[AuditLog] ( UserId, TableName, Type, Data, Created) 
		SELECT @cUserid,'ProcessFile',2,
		( SELECT * FROM [proc].[ProcessFile] 
			WHERE
			[ProcessFileId] = @ProcessFileId
			 FOR XML AUTO) as data,
			getdate() 
	END 


	DELETE FROM [proc].[ProcessFile]
	WHERE
		[ProcessFileId] = @ProcessFileId

	Set @Err = @@Error

	RETURN @Err
END

GO
