SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[log].[prc_CleanupEventByType]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [log].[prc_CleanupEventByType] AS' 
END
GO
/*  Target: Cleanup events of given types by their retention period settings
    Author: Ray Tran

    2017-09-22 Ray:     Remove filter by EventType
    2017-09-27 Ray:     Output messages
*/
ALTER PROCEDURE [log].[prc_CleanupEventByType]
(   @BatchSize          int = 100,               -- Number of events to be deleted at once
    @OutputMessage      nvarchar(max) = '' OUTPUT
) AS
BEGIN
    SET NOCOUNT ON

    DELETE TOP (@BatchSize) e
    FROM [log].[Event] e
    JOIN [log].[EventType] et ON et.[EventTypeID] = e.[EventTypeID] AND et.[RetentionPeriod] > 0 AND e.[Created] < GETDATE() - et.[RetentionPeriod];

    SET @OutputMessage = 'Deleted ' + CAST(@@rowcount AS nvarchar(32)) + ' events';
    RAISERROR (@OutputMessage, 0, 1) WITH NOWAIT
END
GO
