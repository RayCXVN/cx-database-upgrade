SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[log].[prc_EventCountByKeyValue]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [log].[prc_EventCountByKeyValue] AS' 
END
GO
ALTER PROCEDURE [log].[prc_EventCountByKeyValue]
    @EventKeyID     int,
    @Value          nvarchar(max) = '', -- List of values separated by '|'
    @EventKeyID2    int = 0,
    @Value2         nvarchar(max) = '', -- List of values separated by '|', optional
    @HDID           int = 0,
    @FromDate       date = NULL,
    @ToDate         date = NULL,
    @Level          int = 0 -- Level 0 - From HDID and below; 1 - HDs under given HDID; 2 - Users inside given HDID
AS
BEGIN
    SET NOCOUNT ON

    IF @Level = 0
    BEGIN
        SELECT evi.Value [ID], @HDID [SubID], count(e.EventID) EventCount
        FROM log.Event e
        INNER JOIN log.EventInfo evi ON e.EventID = evi.EventID AND evi.EventKeyID = @EventKeyID AND evi.Value IN (SELECT ParseValue FROM dbo.StringToArray(@Value, '|'))
                                    AND EXISTS (SELECT 1 FROM org.H_D hd WHERE hd.DepartmentID = e.DepartmentID AND hd.[Path] LIKE '%\' + CONVERT(nvarchar(16),@HDID) + '\%')
                                    AND (cast(e.Created as date) >= @FromDate OR @FromDate IS NULL)
                                    AND (cast(e.Created as date) <= @ToDate OR @ToDate IS NULL)
        INNER JOIN log.EventKey evk ON evk.EventKeyID = evi.EventKeyID
        GROUP BY evi.Value
    END
    ELSE IF @Level = 1
    BEGIN
        SELECT evi.Value [ID], hdt.HDID [SubID], count(e.EventID) EventCount
        FROM log.Event e JOIN org.H_D hd ON hd.DepartmentID = e.DepartmentID
        JOIN org.H_D hdt ON hd.[Path] LIKE '%\' + CONVERT(nvarchar(16),hdt.HDID) + '\%' AND hdt.ParentID = @HDID
        JOIN log.EventInfo evi ON e.EventID = evi.EventID AND evi.EventKeyID = @EventKeyID AND evi.Value IN (SELECT ParseValue FROM dbo.StringToArray(@Value, '|'))
         AND (cast(e.Created as date) >= @FromDate OR @FromDate IS NULL)
         AND (cast(e.Created as date) <= @ToDate OR @ToDate IS NULL)
        JOIN log.EventKey evk ON evk.EventKeyID = evi.EventKeyID
        GROUP BY evi.Value, hdt.HDID
    END
    ELSE IF @Level = 2
    BEGIN
        IF @EventKeyID2 = 0
        BEGIN
            SELECT evi.Value [ID], e.UserID [SubID], count(e.EventID) EventCount
            FROM log.Event e JOIN org.H_D hd ON hd.DepartmentID = e.DepartmentID AND hd.HDID = @HDID
            JOIN log.EventInfo evi ON e.EventID = evi.EventID AND evi.EventKeyID = @EventKeyID AND evi.Value IN (SELECT ParseValue FROM dbo.StringToArray(@Value, '|'))
             AND (cast(e.Created as date) >= @FromDate OR @FromDate IS NULL)
             AND (cast(e.Created as date) <= @ToDate OR @ToDate IS NULL)
            JOIN log.EventKey evk ON evk.EventKeyID = evi.EventKeyID
            GROUP BY evi.Value, e.UserID
        END
        ELSE
        BEGIN
            SELECT evi.Value [ID], e.UserID [SubID], count(e.EventID) EventCount
            FROM log.Event e JOIN org.H_D hd ON hd.DepartmentID = e.DepartmentID AND hd.HDID = @HDID
            JOIN log.EventInfo evi ON e.EventID = evi.EventID AND evi.EventKeyID = @EventKeyID AND evi.Value IN (SELECT ParseValue FROM dbo.StringToArray(@Value, '|'))
            JOIN log.EventInfo ei ON e.EventID = ei.EventID AND ei.EventKeyID = @EventKeyID2 AND (ei.Value IN (SELECT ParseValue FROM dbo.StringToArray(@Value2, '|')) OR ISNULL(@Value2,'') = '')
             AND (cast(e.Created as date) >= @FromDate OR @FromDate IS NULL)
             AND (cast(e.Created as date) <= @ToDate OR @ToDate IS NULL)
            JOIN log.EventKey evk ON evk.EventKeyID = evi.EventKeyID
            GROUP BY evi.Value, e.UserID
        END
    END
END


GO
