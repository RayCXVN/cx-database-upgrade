SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[org].[prc_U_D_get]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [org].[prc_U_D_get] AS' 
END
GO

ALTER PROCEDURE [org].[prc_U_D_get]
(
	@UserID int
)
AS
BEGIN
	SET NOCOUNT ON
	DECLARE @Err Int

	SELECT
	[U_DID],
	[DepartmentID],
	[UserID],
	[Selected],
	[Created]
	FROM [org].[U_D]
	WHERE
	[UserID] = @UserID

	Set @Err = @@Error

	RETURN @Err
END


GO
