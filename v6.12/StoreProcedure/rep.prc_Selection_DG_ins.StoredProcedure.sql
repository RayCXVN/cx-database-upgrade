SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[rep].[prc_Selection_DG_ins]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [rep].[prc_Selection_DG_ins] AS' 
END
GO

ALTER PROCEDURE [rep].[prc_Selection_DG_ins]
(
	@SelectionID int,
	@DepartmentGroupID int,
	@cUserid int,
	@Log smallint = 1
)
AS
BEGIN
	SET NOCOUNT ON
	DECLARE @Err Int

	INSERT INTO [rep].[Selection_DG]
	(
		[SelectionID],
		[DepartmentGroupID]
	)
	VALUES
	(
		@SelectionID,
		@DepartmentGroupID
	)

	Set @Err = @@Error

	IF @Log = 1 
	BEGIN 
		INSERT INTO [Log].[AuditLog] ( UserId, TableName, Type, Data, Created) 
		SELECT @cUserid,'Selection_DG',0,
		( SELECT * FROM [rep].[Selection_DG] 
			WHERE
			[SelectionID] = @SelectionID AND
			[DepartmentGroupID] = @DepartmentGroupID				 FOR XML AUTO) as data,
				getdate() 
	 END

	RETURN @Err
END


GO
