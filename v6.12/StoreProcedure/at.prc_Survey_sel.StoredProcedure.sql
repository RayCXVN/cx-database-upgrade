SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[at].[prc_Survey_sel]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [at].[prc_Survey_sel] AS' 
END
GO
ALTER PROCEDURE [at].[prc_Survey_sel]
(
	@SurveyID int
)
AS
BEGIN
	SET NOCOUNT ON
	DECLARE @Err Int

	SELECT
	[SurveyID],
	[ActivityID],
	ISNULL([HierarchyID], 0) AS 'HierarchyID',
	[StartDate],
	[EndDate],
	[Anonymous],
	[Status],
	[ShowBack],
	[LanguageID],
	[ButtonPlacement],
	[UsePageNo],
	[LinkURL],
	[FinishURL],
	[CreateResult],
	[ReportDB],
	[ReportServer],
	[StyleSheet],
	[Type],
	[Created],
	[LastProcessed],
	[ReProcessOLAP],
	[No],
	[OLAPServer],
	[OLAPDB],
	ISNULL([PeriodID],0) as 'PeriodID',
	DeleteResultOnUserDelete,
	ProcessCategorys
	FROM [at].[Survey]
	WHERE
	[SurveyID] = @SurveyID

	Set @Err = @@Error

	RETURN @Err
END


GO
