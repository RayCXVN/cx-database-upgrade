SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[rep].[prc_Bubble_get]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [rep].[prc_Bubble_get] AS' 
END
GO

ALTER PROCEDURE [rep].[prc_Bubble_get]
(
	@OwnerID	int
)
AS
BEGIN
	SET NOCOUNT ON
	DECLARE @Err Int

	SELECT
	[BubbleID],
	[OwnerID],
	[ActivityID],
	[No],
	[Status],
	[Created]
	FROM [rep].[Bubble]
	WHERE OwnerID = @OwnerID
	ORDER BY [No]

	Set @Err = @@Error

	RETURN @Err
END


GO
