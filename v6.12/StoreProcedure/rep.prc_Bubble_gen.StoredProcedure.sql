SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[rep].[prc_Bubble_gen]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [rep].[prc_Bubble_gen] AS' 
END
GO
-- DELETE from cxdev2.AT5R_COMMON.dbo.Bubble WHERE BubbleID = 4
-- [rep].[prc_Bubble_gen] 16,18,14,'strutsen','AT5R_COMMON'
-- [rep].[prc_Bubble_gen] 77,102,30,'strutsen','AT5R_ASPLANVIAK'
-- Select * from rep.bubble
ALTER PROC [rep].[prc_Bubble_gen]
(
                @ActivityID		INT,
				@SurveyID		INT,
				@BubbleID       INT,
				@ServerName     NVARCHAR(64),
				@DbName			NVARCHAR(64)
               
)
AS
BEGIN
    SET NOCOUNT ON
    SET ANSI_WARNINGS ON
	CREATE TABLE #tmp
	(
                ResultID	BIGINT,
                Axes0       DECIMAL(37,34),
                Axes1		DECIMAL(37,34),
                A1          INT,
                B1          INT,
                C1          INT,
                D1          INT,
                E1          INT,
                F           INT,
                G           INT,
                H           INT,
                I           INT,
                a           REAL,
                b           REAL,
                c           REAL,
                d           REAL,
                e           REAL
	)

    DECLARE
	@A0L	REAL,
	@A0H	REAL,
	@A1L	REAL,
	@A1H	REAL
	
	DECLARE 
	@A0Count INT,
	@A1Count INT
	
	DECLARE
	@CategoryID INT,
	@AxisNo INT,
	@PrevNo INT
	
	DECLARE @bFirst AS INT 
	
	
	DECLARE @cmd AS VARCHAR(8000)
	DECLARE @cmd2 AS VARCHAR(2000) 
	DECLARE @cmd3 AS VARCHAR(2000) 
    
	SET @cmd = ' DELETE FROM ' + @ServerName + '.' + @DbName+ '.dbo.Bubble WHERE BubbleID = ' + CAST(@BubbleID AS VARCHAR(8)) + ' AND surveyid = ' +cast(@surveyid as varchar(16))
    EXEC(@cmd) 
   	--PRINT(@cmd)

	SELECT @A0L = LimitLow, @A0H = LimitHigh FROM [rep].Bubble_SurveyLimit WHERE SurveyID = @surveyid AND AxisNO = 0 AND BUBBLEID =  @BubbleID
	SELECT @A1L = LimitLow, @A1H = LimitHigh FROM [rep].Bubble_SurveyLimit WHERE SurveyID = @surveyid AND AxisNO = 1 AND BUBBLEID =  @BubbleID

	
    SELECT @A0Count = COUNT(*)  FROM [rep].Bubble_Category WHERE BubbleID = @BubbleID AND AxisNo = 0
    
    SELECT @A1Count = COUNT(*)  FROM [rep].Bubble_Category WHERE BubbleID = @BubbleID AND AxisNo = 1
    
    --PRINT @A0Count
	--PRINT @A1Count
   
    -- Beregn aksene-versiene pr. resultat
    SET @cmd = 'Insert into ' + @ServerName + '.' + @DbName+ '.dbo.Bubble(BubbleID,SurveyID,Resultid,Axes0,Axes1) 
    Select  ' + CAST(@BubbleID AS VARCHAR(8))+ ' as BubbleID,R.surveyid,R.resultid, '
    --PRINT(@cmd)
    
    DECLARE Cat_cursor CURSOR FOR 
    SELECT CategoryID,AXISNO FROM [rep].Bubble_Category WHERE BubbleID = @BubbleID
    ORDER BY AXISNO
    
    SET @bFirst = 1  
    SET @PrevNo = 0
    
    OPEN Cat_cursor
    
    SET @cmd2 = ''
    SET @cmd3 = ''
    
   FETCH NEXT FROM Cat_cursor
   INTO @CategoryID,@AxisNo
   
	WHILE @@FETCH_STATUS = 0
	BEGIN
	    IF @bFirst = 1  AND @AxisNo = 0
	    BEGIN
	       SET @cmd2 = @cmd2 + '(' 
	    END
	    
	    IF @AxisNo = 1 AND @PrevNo = 0
		BEGIN
		  SET @cmd2 = @cmd2 + ') /' +  CAST(@A0Count AS VARCHAR(2)) + ' , ('
		  SET @bFirst = 1
		END
		IF @bFirst = 0
		BEGIN
		   SET @cmd2 = @cmd2 + ' + '
		END
		SET @cmd2 = @cmd2 + ' AVG(A' + CAST(@CategoryID AS VARCHAR(8)) + '.value)'
		SET @PrevNo = @AxisNo
		SET @bFirst = 0
		
		SET @cmd3 = @cmd3 + '
		INNER JOIN at.Q_C C' + CAST(@CategoryID AS VARCHAR(8)) +' On C' + CAST(@CategoryID AS VARCHAR(8)) +'.CategoryID = ' + CAST(@CategoryID AS VARCHAR(8)) +'
        LEFT OUTER JOIN ' + @ServerName + '.' + @DbName+ '.dbo.Answer A' + CAST(@CategoryID AS VARCHAR(8)) +' On A' + CAST(@CategoryID AS VARCHAR(8)) +'.ResultID = R.ResultID AND A' + CAST(@CategoryID AS VARCHAR(8)) +'.QuestionID = C' + CAST(@CategoryID AS VARCHAR(8)) +'.QuestionID '
		
		
		FETCH NEXT FROM Cat_cursor
		INTO @CategoryID,@AxisNo
	END
	SET @cmd2 = @cmd2 + ') /' +  CAST(@A1Count AS VARCHAR(4))

	CLOSE Cat_cursor;
	DEALLOCATE Cat_cursor;
	
	SET @cmd = @cmd + @cmd2 + ' FROM ' + @Servername + '.' + @dbname + '.dbo.result R ' + @cmd3 + ' WHERE r.surveyID = ' + CAST(@SurveyID AS VARCHAR(8)) + ' GROUP By R.ResultID ,R.SurveyID '
	--PRINT(@cmd)
    EXEC(@cmd)
        
    -- Slett de med null på aksene
    SET @cmd = 'DELETE ' + @ServerName + '.' + @DbName+ '.dbo.Bubble WHERE ( Axes0 IS NULL OR Axes1 IS NULL) AND  Bubbleid = ' + CAST(@BubbleID AS VARCHAR(8))
	EXEC(@cmd)
    --PRINT(@cmd)
    -- Beregn plassering pr. resultat
    SET @cmd = ' UPDATE ' +  @ServerName + '.' + @DbName+ '.dbo.Bubble
	SET A1 = CASE WHEN Axes0 < ' + CAST(@A0L AS VARCHAR(8)) +' AND Axes1 >= ' + CAST(@A1H AS VARCHAR(8)) +' THEN 1 ELSE 0 END,
	B1 = CASE WHEN Axes0 > ' + CAST(@A0L AS VARCHAR(8)) + '  AND Axes1 >= ' + CAST(@A1H AS VARCHAR(8)) +' AND Axes0 < ' + CAST(@A0H AS VARCHAR(8)) +' THEN 1 ELSE 0 END,
	C1 = CASE WHEN Axes0 >=  '+ CAST(@A0H AS VARCHAR(8)) +'  AND Axes1 >= ' + CAST(@A1H AS VARCHAR(8)) +'  THEN 1 ELSE 0 END,
	D1 = CASE WHEN Axes1 >= ' + CAST(@A0L AS VARCHAR(8)) +' AND Axes1 < ' + CAST(@A1H AS VARCHAR(8)) +' AND Axes0 < ' + CAST(@A0L AS VARCHAR(8)) +'  THEN 1 ELSE 0 END,
	E1 = CASE WHEN Axes0 >= ' + CAST(@A0L AS VARCHAR(8)) +' AND Axes0 < ' + CAST(@A0H AS VARCHAR(8)) +' AND Axes1 >=  ' + CAST(@A1L AS VARCHAR(8)) +' AND Axes1 < ' + CAST(@A1H AS VARCHAR(8)) +' THEN 1 ELSE 0 END,
	F = CASE WHEN Axes0 >= ' + CAST(@A0H AS VARCHAR(8)) +' AND Axes1 >= ' + CAST(@A1L AS VARCHAR(8)) +' AND Axes1 <  ' + CAST(@A1H AS VARCHAR(8)) +'  THEN 1 ELSE 0 END,
	G = CASE WHEN Axes0 < ' + CAST(@A0L AS VARCHAR(8)) +' AND Axes1 < ' + CAST(@A1L AS VARCHAR(8)) +' THEN 1 ELSE 0 END,
	H = CASE WHEN Axes0 >= ' + CAST(@A0L AS VARCHAR(8)) +' AND Axes0 <  ' + CAST(@A0H AS VARCHAR(8)) +' AND Axes1 < ' + CAST(@A1L AS VARCHAR(8)) +' THEN 1 ELSE 0 END,
	I = CASE WHEN Axes0 >= ' + CAST(@A0H AS VARCHAR(8)) +' AND Axes1 <  ' + CAST(@A1L AS VARCHAR(8)) +' THEN 1 ELSE 0 END 
	WHERE surveyID = '  + CAST(@SurveyID AS VARCHAR(8)) + ' AND Bubbleid = ' + CAST(@BubbleID AS VARCHAR(8))
	--PRINT(@cmd)
	EXEC(@cmd)
	
	
	-- Beregen fordeling pr. resultat på de 5 boblene
	SET @cmd = ' UPDATE ' +  @ServerName + '.' + @DbName+ '.dbo.Bubble
	SET a = A1 + Cast(B1 As real)/3 + Cast(D1 As real)/3,
	b = C1 + Cast(B1 As real)/3 + Cast(F As real)/3,
	c = E1 + Cast(B1 As real)/3 + Cast(D1 As real)/3 + Cast(F As real)/3 + Cast(H As real)/3,
	d = G + Cast(D1 As real)/3 + Cast(H As real)/3,
	e = I + Cast(F As real)/3 + Cast(H As real)/3 
	WHERE surveyID = '  +  CAST(@SurveyID AS VARCHAR(8)) + ' AND Bubbleid = ' + CAST(@BubbleID AS VARCHAR(8))
	
	EXEC(@cmd)
	--PRINT(@cmd)
	
END



GO
