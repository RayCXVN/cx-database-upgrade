SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[prc_Invited_Finished_getByBatchList]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [dbo].[prc_Invited_Finished_getByBatchList] AS' 
END
GO
/*
    Sarah - Oct 31, 2016 - Hot fix: Get EntityStatus value on QDB
	Steve - Dec 8, 2016 - Job Fair project: merge Q and R DB, then change how to make a dynamic query
    Ray - 2017-02-09 - Remove cross database reference for Azure compatible
*/
ALTER PROCEDURE [dbo].[prc_Invited_Finished_getByBatchList]
(
    @BatchIDList    varchar(max)
)
AS
BEGIN
    SET NOCOUNT ON
    DECLARE @Err Int, @ActiveEntityStatusID INT = 0, @LinkedDB NVARCHAR(MAX) = ' '
    SELECT @ActiveEntityStatusID = EntityStatusID FROM EntityStatus WHERE CodeName='Active'
    
    DECLARE @BatchID INT, @ReportDB NVARCHAR(MAX), @ReportServer NVARCHAR(MAX), @Sql_command NVARCHAR(MAX)

    CREATE TABLE #T_BatchCouting (BatchID INT, Invited INT, Finished INT)
    DECLARE c_ResultDatabase CURSOR READ_ONLY FOR
    SELECT DISTINCT s.ReportDB,s.ReportServer FROM 
    at.Survey s INNER JOIN 
    at.Batch  b ON b.SurveyID = s.SurveyID WHERE b.BatchID IN (SELECT [ParseValue] FROM dbo.StringToArray(@BatchIDList, ','))
    OPEN c_ResultDatabase
    FETCH NEXT FROM c_ResultDatabase INTO  @ReportDB, @ReportServer
	    WHILE @@FETCH_STATUS=0
	    BEGIN
			IF (@ReportServer = @@servername AND @ReportDB = DB_NAME())
				SET @LinkedDB = ' '
			ELSE 
				SET @LinkedDB = '[' + @ReportServer + '].[' + @ReportDB + '].'

	        SET @Sql_command ='
	        INSERT INTO #T_BatchCouting SELECT  BatchID,
                count(*) as Invited,
                count(EndDate) as Finished
	            FROM ' + @LinkedDB + 'dbo.[Result]
                WHERE [BatchID] IN (SELECT [ParseValue] FROM dbo.StringToArray('''+@BatchIDList+''', '',''))
                  AND [EntityStatusID] = ' + CONVERT(NVARCHAR(14), @ActiveEntityStatusID)+ ' AND Deleted IS NULL
                GROUP BY BatchID
	        '
	       EXECUTE sp_executesql @Sql_command
    	   
	       FETCH NEXT FROM c_ResultDatabase INTO  @ReportDB, @ReportServer
	    END
	    CLOSE c_ResultDatabase
	    DEALLOCATE c_ResultDatabase
    SELECT BatchID, SUM(Invited) as Invited, SUM(Finished) as Finished FROM #T_BatchCouting GROUP BY BatchID ORDER BY BatchID 
    DROP TABLE #T_BatchCouting

	Set @Err = @@Error

	RETURN @Err
END

GO
