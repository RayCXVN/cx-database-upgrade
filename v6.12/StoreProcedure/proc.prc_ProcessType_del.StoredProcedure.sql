SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[proc].[prc_ProcessType_del]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [proc].[prc_ProcessType_del] AS' 
END
GO
-- [proc].[prc_ProcessType_del] 12,0,0
ALTER PROCEDURE [proc].[prc_ProcessType_del]
(
	@ProcessTypeID int,
	@cUserid int,
	@Log smallint = 1
)
AS
BEGIN
	SET NOCOUNT ON
	DECLARE @Err Int

	IF @Log = 1 
	BEGIN 
		INSERT INTO [Log].[AuditLog] ( UserId, TableName, Type, Data, Created) 
		SELECT @cUserid,'ProcessType',2,
		( SELECT * FROM [proc].[ProcessType] 
			WHERE
			[ProcessTypeID] = @ProcessTypeID
			 FOR XML AUTO) as data,
			getdate() 
	END 

    DELETE FROM [proc].[process]
    WHERE [ProcessTypeID] = @ProcessTypeID
		
	DELETE FROM [proc].[ProcessValue]
	WHERE [ProcessTypeID] = @ProcessTypeID

	DELETE FROM [proc].[ProcessType]
	WHERE [ProcessTypeID] = @ProcessTypeID

	Set @Err = @@Error

	RETURN @Err
END


GO
