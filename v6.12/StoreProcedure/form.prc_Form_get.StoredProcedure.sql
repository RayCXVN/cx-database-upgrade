SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[form].[prc_Form_get]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [form].[prc_Form_get] AS' 
END
GO


ALTER PROCEDURE [form].[prc_Form_get]
(
	@OwnerID int
)
AS
BEGIN
	SET NOCOUNT ON
	DECLARE @Err Int

	SELECT
	[FormID],
	[OwnerID],
	[ElementID],
	[TableTypeID],
	ISNULL([ContextFormFieldID], 0) AS 'ContextFormFieldID',
	[Type],
	[FName],
	[CssClass],
	[ExtID],
	[Created]
	FROM [form].[Form]
	WHERE
	[OwnerID] = @OwnerID

	Set @Err = @@Error

	RETURN @Err
END



GO
