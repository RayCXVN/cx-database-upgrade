SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[at].[prc_Alternative_upd]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [at].[prc_Alternative_upd] AS' 
END
GO

ALTER PROCEDURE [at].[prc_Alternative_upd]
(
	@AlternativeID int,
	@ScaleID int,
	@AGID INT=NULL,
	@Type smallint,
	@No smallint,
	@Value float,
	@InvertedValue float,
	@Calc bit,
	@SC bit,
	@MinValue float,
	@MaxValue float,
	@Format varchar(64),
	@Size smallint,
	@CssClass varchar(64),
	@DefaultValue nvarchar(128),
	@Tag nvarchar(128),
	@ExtID nvarchar(256),
	@Width nvarchar(32),
	@OwnerColorID int = null,
	@DefaultCalcType smallint = -1,
    @ParentID INT = NULL,
    @UseEncryption BIT = 0,
	@cUserid int,
	@Log smallint = 1
    
)
AS
BEGIN
	SET NOCOUNT ON
	DECLARE @Err Int

	UPDATE [at].[Alternative]
	SET
		[ScaleID] = @ScaleID,
		[AGID] = @AGID,
		[Type] = @Type,
		[No] = @No,
		[Value] = @Value,
		[InvertedValue] = @InvertedValue,
		[Calc] = @Calc,
		[SC] = @SC,
		[MinValue] = @MinValue,
		[MaxValue] = @MaxValue,
		[Format] = @Format,
		[Size] = @Size,
		[CssClass] = @CssClass,
		[DefaultValue] = @DefaultValue,
		[Tag] = @Tag,
		[ExtID] = @ExtID,
		[Width] = @Width,
		[OwnerColorID] = @OwnerColorID,
		[DefaultCalcType] = @DefaultCalcType,
        [ParentID] = @ParentID,
        UseEncryption = @UseEncryption
	WHERE
		[AlternativeID] = @AlternativeID

	IF @Log = 1 
	BEGIN 
		INSERT INTO [Log].[AuditLog] ( UserId, TableName, Type, Data, Created) 
		SELECT @cUserid,'Alternative',1,
		( SELECT * FROM [at].[Alternative] 
			WHERE
			[AlternativeID] = @AlternativeID			 FOR XML AUTO) as data,
			getdate()
	END

	Set @Err = @@Error

	RETURN @Err
END

GO
