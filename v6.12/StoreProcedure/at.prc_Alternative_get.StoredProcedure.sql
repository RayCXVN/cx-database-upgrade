SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[at].[prc_Alternative_get]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [at].[prc_Alternative_get] AS' 
END
GO
ALTER PROCEDURE [at].[prc_Alternative_get]
(
	@ScaleID int
)
AS
BEGIN
	SET NOCOUNT ON
	DECLARE @Err Int

	SELECT
	[AlternativeID],
	[ScaleID],
	ISNULL([AGID], 0) AS 'AGID',
	[Type],
	[No],
	[Value],
	[InvertedValue],
	[Calc],
	[SC],
	[MinValue],
	[MaxValue],
	[Format],
	[Size],
	[CssClass],
	[Created],
	[DefaultValue],
	[Tag],
	[ExtID],
	[Width],
	ISNULL([OwnerColorID],0) as 'OwnerColorID',
	DefaultCalcType,
    ParentID,
    UseEncryption
	FROM [at].[Alternative]
	WHERE
	[ScaleID] = @ScaleID
	ORDER BY [No]

	Set @Err = @@Error

	RETURN @Err
END

GO
