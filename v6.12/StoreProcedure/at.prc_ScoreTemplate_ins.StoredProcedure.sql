SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[at].[prc_ScoreTemplate_ins]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [at].[prc_ScoreTemplate_ins] AS' 
END
GO

ALTER PROCEDURE [at].[prc_ScoreTemplate_ins]
(
	@ScoreTemplateID int = null output,
	@ActivityID int = null,
	@XCID int = null,
	@Type smallint,
	@No smallint,
	@OwnerColorID int,
	@DefaultState bit=0,
	@cUserid int,
	@Log smallint = 1
)
AS
BEGIN
	SET NOCOUNT ON
	DECLARE @Err Int

	INSERT INTO [at].[ScoreTemplate]
	(
		[ActivityID],
		[XCID],
		[Type],
		[No],
		[OwnerColorID],
		[DefaultState]
	)
	VALUES
	(
		@ActivityID,
		@XCID,
		@Type,
		@No,
		@OwnerColorID,
		@DefaultState
	)

	Set @Err = @@Error
	Set @ScoreTemplateID = scope_identity()

	IF @Log = 1 
	BEGIN 
		INSERT INTO [Log].[AuditLog] ( UserId, TableName, Type, Data, Created) 
		SELECT @cUserid,'ScoreTemplate',0,
		( SELECT * FROM [at].[ScoreTemplate] 
			WHERE
			[ScoreTemplateID] = @ScoreTemplateID				 FOR XML AUTO) as data,
				getdate() 
	 END

	RETURN @Err
END

/****** Object:  StoredProcedure [at].[prc_ScoreTemplate_upd]    Script Date: 12/02/2010 13:40:05 ******/
SET ANSI_NULLS ON


GO
