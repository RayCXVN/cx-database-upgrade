SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[at].[prc_Mail_ins]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [at].[prc_Mail_ins] AS' 
END
GO

ALTER PROCEDURE [at].[prc_Mail_ins]
(
	@MailID int = null output,
	@SurveyID int,
	@BatchID INT=NULL,
	@Type smallint,
	@From nvarchar(256),
	@Bcc nvarchar(max),
	@StatusMail nvarchar(max),
	@cUserid int,
	@Log smallint = 1
)
AS
BEGIN
	SET NOCOUNT ON
	DECLARE @Err Int

	INSERT INTO [at].[Mail]
	(
		[SurveyID],
		[BatchID],
		[Type],
		[From],
		[Bcc],
		[StatusMail]
	)
	VALUES
	(
		@SurveyID,
		@BatchID,
		@Type,
		@From,
		@Bcc,
		@StatusMail
	)

	Set @Err = @@Error
	Set @MailID = scope_identity()

	IF @Log = 1 
	BEGIN 
		INSERT INTO [Log].[AuditLog] ( UserId, TableName, Type, Data, Created) 
		SELECT @cUserid,'Mail',0,
		( SELECT * FROM [at].[Mail] 
			WHERE
			[MailID] = @MailID				 FOR XML AUTO) as data,
				getdate() 
	 END

	RETURN @Err
END


GO
