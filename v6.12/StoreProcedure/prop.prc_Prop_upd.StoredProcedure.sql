SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[prop].[prc_Prop_upd]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [prop].[prc_Prop_upd] AS' 
END
GO

ALTER PROCEDURE [prop].[prc_Prop_upd]
(
	@PropertyID int,
	@PropPageID int,
	@No smallint,
	@MultiValue bit,
	@Type smallint,
	@ValueType smallint,
	@FormatString nvarchar(50),
	@cUserid int,
	@Log smallint = 1
)
AS
BEGIN
	SET NOCOUNT ON
	DECLARE @Err Int

	UPDATE [prop].[Prop]
	SET
		[PropPageID] = @PropPageID,
		[No] = @No,
		[MultiValue] = @MultiValue,
		[Type] = @Type,
		[ValueType] = @ValueType,
		[FormatString] = @FormatString
	WHERE
		[PropertyID] = @PropertyID

	IF @Log = 1 
	BEGIN 
		INSERT INTO [Log].[AuditLog] ( UserId, TableName, Type, Data, Created) 
		SELECT @cUserid,'Prop',1,
		( SELECT * FROM [prop].[Prop] 
			WHERE
			[PropertyID] = @PropertyID			 FOR XML AUTO) as data,
			getdate()
	END

	Set @Err = @@Error

	RETURN @Err
END


GO
