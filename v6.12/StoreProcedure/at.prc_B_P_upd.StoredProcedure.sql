SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[at].[prc_B_P_upd]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [at].[prc_B_P_upd] AS' 
END
GO
ALTER PROCEDURE [at].[prc_B_P_upd]
(
	@BulkID int,
	@PageID int
)
AS
BEGIN
	SET NOCOUNT ON
	DECLARE @Err Int

	UPDATE [at].[B_P]
	SET
		[BulkID] = @BulkID,
		[PageID] = @PageID
	WHERE
		[BulkID] = @BulkID AND
		[PageID] = @PageID

	
	Set @Err = @@Error

	RETURN @Err
END



GO
