SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[at].[prc_Choice_ins]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [at].[prc_Choice_ins] AS' 
END
GO
ALTER PROCEDURE [at].[prc_Choice_ins] (
	@ChoiceID INT = NULL OUTPUT
	,@ActivityID INT = NULL
	,@SurveyID INT = NULL
	,@RoleID INT = NULL
	,@DepartmentID INT = NULL
	,@HDID INT = NULL
	,@DepartmentTypeID INT = NULL
	,@UserGroupID INT = NULL
	,@UserTypeID INT = NULL
	,@Name NVARCHAR(64)
	,@No SMALLINT
	,@Type SMALLINT
	,@cUserid INT
	,@Log SMALLINT = 1
	)
AS
BEGIN
	SET NOCOUNT ON

	DECLARE @Err INT

	INSERT INTO [at].[Choice] (
		[ActivityID]
		,[SurveyID]
		,[RoleID]
		,[DepartmentID]
		,[HDID]
		,[DepartmentTypeID]
		,[UserGroupID]
		,[UserTypeID]
		,[Name]
		,[No]
		,[Type]
		)
	VALUES (
		@ActivityID
		,@SurveyID
		,@RoleID
		,@DepartmentID
		,@HDID
		,@DepartmentTypeID
		,@UserGroupID
		,@UserTypeID
		,@Name
		,@No
		,@Type
		)

	SET @Err = @@Error
	SET @ChoiceID = scope_identity()

	IF @Log = 1
	BEGIN
		INSERT INTO [Log].[AuditLog] (
			UserId
			,TableName
			,Type
			,Data
			,Created
			)
		SELECT @cUserid
			,'Choice'
			,0
			,(
				SELECT *
				FROM [at].[Choice]
				WHERE [ChoiceID] = @ChoiceID
				FOR XML AUTO
				) AS data
			,getdate()
	END

	RETURN @Err
END

GO
