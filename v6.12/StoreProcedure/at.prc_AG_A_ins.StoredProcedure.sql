SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[at].[prc_AG_A_ins]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [at].[prc_AG_A_ins] AS' 
END
GO
ALTER PROCEDURE [at].[prc_AG_A_ins]
(
	@AGID int,
	@AlternativeID int,
	@cUserid int,
	@Log smallint = 1
)
AS
BEGIN
	SET NOCOUNT ON
	DECLARE @Err Int

	INSERT INTO [at].[AG_A]
	(
		[AGID],
		[AlternativeID]
	)
	VALUES
	(
		@AGID,
		@AlternativeID
	)

	Set @Err = @@Error

	IF @Log = 1 
	BEGIN 
		INSERT INTO [Log].[AuditLog] ( UserId, TableName, Type, Data, Created) 
		SELECT @cUserid,'AG_A',0,
		( SELECT * FROM [at].[AG_A] 
			WHERE
			[AGID] = @AGID AND
			[AlternativeID] = @AlternativeID				 FOR XML AUTO) as data,
				getdate() 
	 END

	RETURN @Err
END

GO
