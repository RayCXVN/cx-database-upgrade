SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[res].[prc_MediaType_upd]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [res].[prc_MediaType_upd] AS' 
END
GO
ALTER PROCEDURE [res].[prc_MediaType_upd]
(
	@MediaTypeID int,
	@CodeName nvarchar(256),
	@cUserid int,
	@Log smallint = 1
)
AS  
BEGIN  
	SET NOCOUNT ON  
	DECLARE @Err Int  
  
	UPDATE res.MediaType
	SET	
		CodeName = @CodeName
	WHERE
		[MediaTypeId] = @MediaTypeID
  
	Set @Err = @@Error  
	Set @MediaTypeID = scope_identity()

	IF @Log = 1 
	BEGIN 
		INSERT INTO [Log].[AuditLog] ( UserId, TableName, Type, Data, Created) 
		SELECT @cUserid,'res.MediaType',0,
		( SELECT * FROM [res].[MediaType] 
			WHERE
			[MediaTypeID] = @MediaTypeID	FOR XML AUTO) as data,
				getdate() 
	END

	RETURN @Err
END  

GO
