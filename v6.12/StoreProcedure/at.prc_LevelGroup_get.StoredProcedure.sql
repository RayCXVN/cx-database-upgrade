SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[at].[prc_LevelGroup_get]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [at].[prc_LevelGroup_get] AS' 
END
GO




ALTER PROCEDURE [at].[prc_LevelGroup_get]
(
	@ActivityID int
)
AS
BEGIN
	SET NOCOUNT ON
	DECLARE @Err Int

	SELECT
	[LevelGroupID],
	[ActivityID],
	[Tag],
	[No],
	isNull([CustomerID],0) 'CustomerID',
	ISNULL([Departmentid],0) 'DepartmentID',
	ISNULL([RoleID],0) 'RoleID'
	FROM [at].[LevelGroup]
	WHERE
	[ActivityID] = @ActivityID
	ORDER BY [No]

	Set @Err = @@Error

	RETURN @Err
END



GO
