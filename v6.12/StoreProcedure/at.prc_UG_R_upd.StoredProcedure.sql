SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[at].[prc_UG_R_upd]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [at].[prc_UG_R_upd] AS' 
END
GO

ALTER PROCEDURE [at].[prc_UG_R_upd]
(
	@UserGroupID int,
	@ActivityID int,
	@RoleID int,
	@cUserid int,
	@Log smallint = 1
)
AS
BEGIN
	SET NOCOUNT ON
	DECLARE @Err Int

	UPDATE [at].[UG_R]
	SET
		[UserGroupID] = @UserGroupID,
		[ActivityID] = @ActivityID,
		[RoleID] = @RoleID
	WHERE
		[UserGroupID] = @UserGroupID AND
		[ActivityID] = @ActivityID

	IF @Log = 1 
	BEGIN 
		INSERT INTO [Log].[AuditLog] ( UserId, TableName, Type, Data, Created) 
		SELECT @cUserid,'UG_R',1,
		( SELECT * FROM [at].[UG_R] 
			WHERE
			[UserGroupID] = @UserGroupID AND
			[ActivityID] = @ActivityID			 FOR XML AUTO) as data,
			getdate()
	END

	Set @Err = @@Error

	RETURN @Err
END


GO
