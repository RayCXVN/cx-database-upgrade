SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[rep].[prc_ReportChartType_ins]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [rep].[prc_ReportChartType_ins] AS' 
END
GO

ALTER PROCEDURE [rep].[prc_ReportChartType_ins]
(
	@ReportChartTypeID int,
	@Type smallint,
	@No smallint,
	@DundasID smallint,
	@cUserid int,
	@Log smallint = 1
)
AS
BEGIN
	SET NOCOUNT ON
	DECLARE @Err Int

	INSERT INTO [rep].[ReportChartType]
	(
		[ReportChartTypeID],
		[Type],
		[No],
		[DundasID]
	)
	VALUES
	(
		@ReportChartTypeID,
		@Type,
		@No,
		@DundasID
	)

	Set @Err = @@Error

	IF @Log = 1 
	BEGIN 
		INSERT INTO [Log].[AuditLog] ( UserId, TableName, Type, Data, Created) 
		SELECT @cUserid,'ReportChartType',0,
		( SELECT * FROM [rep].[ReportChartType] 
			WHERE
			[ReportChartTypeID] = @ReportChartTypeID				 FOR XML AUTO) as data,
				getdate() 
	 END

	RETURN @Err
END


GO
