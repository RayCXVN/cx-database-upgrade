SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[proc].[prc_PT_PT_ins]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [proc].[prc_PT_PT_ins] AS' 
END
GO

ALTER PROCEDURE [proc].[prc_PT_PT_ins]
(
	@FromProcessTypeID int,
	@ToProcessTypeID int,
	@cUserid int,
	@Log smallint = 1
)
AS
BEGIN
	SET NOCOUNT ON
	DECLARE @Err Int

	INSERT INTO [proc].[PT_PT]
	(
		[FromProcessTypeID],
		[ToProcessTypeID]
	)
	VALUES
	(
		@FromProcessTypeID,
		@ToProcessTypeID
	)

	Set @Err = @@Error

	IF @Log = 1 
	BEGIN 
		INSERT INTO [Log].[AuditLog] ( UserId, TableName, Type, Data, Created) 
		SELECT @cUserid,'PT_PT',0,
		( SELECT * FROM [proc].[PT_PT] 
			WHERE
			[FromProcessTypeID] = @FromProcessTypeID AND
			[ToProcessTypeID] = @ToProcessTypeID				 FOR XML AUTO) as data,
				getdate() 
	 END

	RETURN @Err
END

GO
