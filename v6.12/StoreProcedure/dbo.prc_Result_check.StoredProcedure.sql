SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[prc_Result_check]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [dbo].[prc_Result_check] AS' 
END
GO
/* ------------------------------------------------------------
   PROCEDURE:    prc_Result_check

   Description:  Selects records from the table 'prc_Result_check'

   AUTHOR:       LockwoodTech 11.07.2006 11:47:11
   ------------------------------------------------------------ */
ALTER PROCEDURE [dbo].[prc_Result_check]
(
	@ResultID		bigint,
	@ts				timestamp OUTPUT,
	@OK				bit OUTPUT
)
As
BEGIN
	SET NOCOUNT ON
	DECLARE	@Err	Int,
			@NewTS	timestamp

	SELECT
		@NewTS = chk
	FROM [Result]
	WHERE 
		[ResultID] = @ResultID

	IF @ts = @NewTS
	BEGIN
		UPDATE [Result] SET PageNo = PageNo WHERE [ResultID] = @ResultID
		SELECT @ts = chk FROM [Result] WHERE [ResultID] = @ResultID
		SET @OK = 1	
	END
	ELSE
		SET @OK = 0


	Set @Err = @@Error

	RETURN @Err
End


GO
