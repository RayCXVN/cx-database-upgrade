SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[prc_Result_ins]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [dbo].[prc_Result_ins] AS' 
END
GO
/*  2016-10-10 Ray: Remove column Status from org.User, org.Department, RDB.dbo.Result
*/
ALTER PROCEDURE [dbo].[prc_Result_ins]
(
	@ResultID bigint = null output,
	@StartDate datetime = null,
	@EndDate datetime = null,
	@RoleID int,
	@UserID int = null,
	@UsergroupID int = null,
	@SurveyID int,
	@BatchID int,
	@LanguageID int,
	@PageNo smallint,
	@DepartmentID int,
	@Anonymous smallint,
	@ShowBack bit,
	@Email varchar(100),
	@ResultKey nvarchar(128) = '',
	@LastUpdatedBy Int = 0,
	@StatusTypeID Int = null,
	@ValidFrom  datetime2(7) =null,
	@ValidTo datetime2(7) =null,
	@DueDate datetime2(7) =null,
	@ParentResultID bigint =null,
	@ParentResultSurveyID int =  null,
	@CustomerID int = NULL,
	@Deleted datetime2= NULL,
	@EntityStatusID int = NULL,
	@EntityStatusReasonID int = NULL  
)
As
BEGIN
	SET NOCOUNT ON
	DECLARE @Err Int 
    Declare @count int
	DECLARE @DBID INT;
	DECLARE @DBNAME NVARCHAR(128);
    Declare @errmsg as nvarchar(200)
    DECLARE @ActiveEntityStatusID INT = (SELECT EntityStatusID FROM EntityStatus WHERE CodeName='Active')

	SET @DBID = DB_ID();
	SET @DBNAME = DB_NAME();

    set @Err = 0
--	if @userid is null
--	begin
--		select @count = count(*) from result where username = @Username and username <> ''
--		if @count > 0
--		begin
--			set @Err = 547
--			PRINT @count
--		end
--	end	

    IF @ResultKey <> '' 
	BEGIN
       SELECT @Count = Count(*) FROM [Result] WHERE ResultKey = @ResultKey AND EntityStatusID = @ActiveEntityStatusID AND Deleted IS NULL
       IF @Count > 0 set @Err = 547
	END

    if @Err = 0
	begin
		INSERT
		INTO [Result]
		(
			[StartDate],
			[EndDate],
			[RoleID],
			[UserID],
			[UserGroupID],
			[SurveyID],
			[BatchID],
			[LanguageID],
			[PageNo],
			[DepartmentID],
			[Anonymous],
			[ShowBack],
			[Email],
			[ResultKey],
			[LastUpdatedBy],
			[StatusTypeID],
			[ValidFrom],  
			[ValidTo], 
			[DueDate], 
			[ParentResultID],
			[ParentResultSurveyID],
			[LastUpdated],
			[CustomerID],
			[Deleted],
			[EntityStatusID],
			[EntityStatusReasonID]
		)
		VALUES
		(
			@StartDate,
			@EndDate,
			@RoleID,
			@UserID,
			@UserGroupID,
			@SurveyID,
			@BatchID,
			@LanguageID,
			@PageNo,
			@DepartmentID,
			@Anonymous,
			@ShowBack,
			@Email,
			@ResultKey,
			@LastUpdatedBy,
			@StatusTypeID,
			@ValidFrom,  
			@ValidTo, 
			@DueDate, 
			@ParentResultID,
			@ParentResultSurveyID,
			GETDATE(),
			@CustomerID,
			@Deleted,
			@EntityStatusID,
			@EntityStatusReasonID 
		)

		Set @Err = @@Error
		set @ResultID = scope_identity()
	end
    ELSE
    BEGIN
        
		set @errmsg = 'Resultkey taken: ' + @ResultKey
        RAISERROR(@errmsg,
			11, -- Severity.
			1, -- State.
			@DBID, -- First substitution argument.
			@DBNAME); -- Second substitution argument.
		Set @Err = 5
		set @ResultID = 0
    END

	RETURN @Err
END


GO
