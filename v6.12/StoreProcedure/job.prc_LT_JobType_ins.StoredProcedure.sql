SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[job].[prc_LT_JobType_ins]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [job].[prc_LT_JobType_ins] AS' 
END
GO

ALTER PROCEDURE [job].[prc_LT_JobType_ins]
(
	@LanguageID int,
	@JobTypeID smallint,
	@Name nvarchar(256),
	@Description nvarchar(max),
	@cUserid int,
	@Log smallint = 1
)
AS
BEGIN
	SET NOCOUNT ON
	DECLARE @Err Int

	INSERT INTO [job].[LT_JobType]
	(
		[LanguageID],
		[JobTypeID],
		[Name],
		[Description]
	)
	VALUES
	(
		@LanguageID,
		@JobTypeID,
		@Name,
		@Description
	)

	Set @Err = @@Error

	IF @Log = 1 
	BEGIN 
		INSERT INTO [Log].[AuditLog] ( UserId, TableName, Type, Data, Created) 
		SELECT @cUserid,'LT_JobType',0,
		( SELECT * FROM [job].[LT_JobType] 
			WHERE
			[LanguageID] = @LanguageID AND
			[JobTypeID] = @JobTypeID				 FOR XML AUTO) as data,
				getdate() 
	 END

	RETURN @Err
END


GO
