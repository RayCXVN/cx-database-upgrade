SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[org].[prc_H_D_Hierarchy_search]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [org].[prc_H_D_Hierarchy_search] AS' 
END
GO
ALTER PROC [org].[prc_H_D_Hierarchy_search]  
(  
 @HierarchyID  int,  
 @HDID    int,  
 @SearchStr  varchar(32),  
 @GetPath   bit = 0  
)  
AS  
  
BEGIN 
DECLARE @ActiveEntityStatusID INT = (SELECT EntityStatusID FROM EntityStatus WHERE CodeName='Active') 
 If @GetPath = 1  
  Begin  
   WITH HDS (HDID, HierarchyID, DepartmentID, ParentID, HD, PathName, Created, Level, pname)  
   AS  
   (  
    --anchor  
    SELECT h.HDID, h.HierarchyID, h.Departmentid, h.Parentid, cast(h.HDID as varchar(128)) as HD , h.PathName, h.Created, 0 as Level, cast((select name  from department where departmentid = h.Departmentid and EntityStatusID = @ActiveEntityStatusID) as varchar(8000))   
    FROM org.H_D  h  
    join department d  on d.departmentid = h.departmentid and ( (d.Name like '%'+ @SearchStr + '%') OR ( d.departmentid like '%'+ @SearchStr + '%') OR ( d.OrgNo like '%'+ @SearchStr + '%')  OR ( d.ExtID like '%'+ @SearchStr + '%') ) 
    where h.Deleted = 0 and d.EntityStatusID = @ActiveEntityStatusID and h.HierarchyID = @HierarchyID and [path] like '%\' + cast(@HDID as varchar(16)) + '\%'  
    UNION ALL  
    --Recursive  
    select h.HDID, h.HierarchyID, h.Departmentid, h.ParentID, cast(cast(d.hd as varchar(128) ) + '\' + cast(h.HDID as varchar(64)) as varchar(128)), h.PathName, h.Created, Level + 1,   
    cast((select name from department where departmentid = h.Departmentid and EntityStatusID = @ActiveEntityStatusID) + '\' + d.pname   as varchar(8000))  as panme  
    FROM org.H_D  h  
    join department dp on dp.departmentid = h.departmentid  
    INNER JOIN HDS AS d  
     ON d.parentid = h.HDID AND h.Deleted = 0 and dp.EntityStatusID = @ActiveEntityStatusID and h.HierarchyID = @HierarchyID  
   )  
   -- Statement that executes the CTE  
   SELECT *  
   FROM HDS   
   order by HD desc  
   --order by PathName asc  
  End  
 Else  
  Begin  
   WITH HDS (HDID, HierarchyID, DepartmentID, ParentID, HD, PathName, Created, Level, pname)  
   AS  
   (  
    --anchor  
    SELECT h.HDID, h.HierarchyID, h.Departmentid, h.Parentid, cast(h.HDID as varchar(128)) as HD , h.PathName, h.Created, 0 as Level, cast((select name  from department where departmentid = h.Departmentid and EntityStatusID = @ActiveEntityStatusID) as varchar(8000))   
    FROM org.H_D  h  
    join department d  on d.departmentid = h.departmentid and ( (d.Name like '%'+ @SearchStr + '%') OR ( d.departmentid like '%'+ @SearchStr + '%') OR ( d.OrgNo like '%'+ @SearchStr + '%')  OR ( d.ExtID like '%'+ @SearchStr + '%') ) 
    where h.Deleted = 0 and d.EntityStatusID = @ActiveEntityStatusID and h.HierarchyID = @HierarchyID and [path] like '%\' + cast(@HDID as varchar(16)) + '\%'  
    UNION ALL  
    --Recursive  
    select h.HDID, h.HierarchyID, h.Departmentid, h.ParentID, cast(cast(d.hd as varchar(128) ) + '\' + cast(h.HDID as varchar(64)) as varchar(128)), h.PathName, h.Created, Level + 1,   
    cast((select name from department where departmentid = h.Departmentid and EntityStatusID = @ActiveEntityStatusID) + '\' + d.pname   as varchar(8000))  as panme  
    FROM org.H_D  h  
    join department dp on dp.departmentid = h.departmentid  
    INNER JOIN HDS AS d  
     ON d.parentid = h.HDID AND h.Deleted = 0 and dp.EntityStatusID = @ActiveEntityStatusID and h.HierarchyID = @HierarchyID  
   )  
   -- Statement that executes the CTE  
   SELECT *  
   FROM HDS where Level = 0  
   order by HD desc  
   --order by PathName asc  
  End  
  
END  

GO
