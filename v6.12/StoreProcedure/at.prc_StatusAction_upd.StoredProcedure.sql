SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[at].[prc_StatusAction_upd]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [at].[prc_StatusAction_upd] AS' 
END
GO

ALTER PROCEDURE [at].[prc_StatusAction_upd]
(
	@StatusActionID int,
	@ActivityID int,
	@FromStatusTypeID INT=NULL,
	@ToStatusTypeID INT=NULL,
	@StatusActionTypeID int,
	@No smallint,
	@Settings XML ='',
	@cUserid int,
	@Log smallint = 1
)
AS
BEGIN
	SET NOCOUNT ON
	DECLARE @Err Int

	UPDATE [at].[StatusAction]
	SET
		[ActivityID] = @ActivityID,
		[FromStatusTypeID] = @FromStatusTypeID,
		[ToStatusTypeID] = @ToStatusTypeID,
		[StatusActionTypeID] = @StatusActionTypeID,
		[No] = @No,
		[Settings] = @Settings
	WHERE
		[StatusActionID] = @StatusActionID

	IF @Log = 1 
	BEGIN 
		INSERT INTO [Log].[AuditLog] ( UserId, TableName, Type, Data, Created) 
		SELECT @cUserid,'StatusAction',1,
		( SELECT * FROM [at].[StatusAction] 
			WHERE
			[StatusActionID] = @StatusActionID			 FOR XML AUTO) as data,
			getdate()
	END

	Set @Err = @@Error

	RETURN @Err
END

GO
