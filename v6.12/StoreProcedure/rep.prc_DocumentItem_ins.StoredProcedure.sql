SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[rep].[prc_DocumentItem_ins]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [rep].[prc_DocumentItem_ins] AS' 
END
GO


ALTER PROCEDURE [rep].[prc_DocumentItem_ins]
(
	@DocumentItemID int = null output,
	@DocumentItemTypeID int,
	@DF_O_ID int,
	@BGColor nvarchar(16),
	@FGColor nvarchar(16),
	@FontSize int,
	@FontName nvarchar(32),
	@FontIsBold int,
	@BorderColor nvarchar(16)='',
	@cUserid int,
	@Log smallint = 1
)
AS
BEGIN
	SET NOCOUNT ON
	DECLARE @Err Int

	INSERT INTO [rep].[DocumentItem]
	(
		[DocumentItemTypeID],
		[DF_O_ID],
		[BGColor],
		[FGColor],
		[FontSize],
		[FontName],
		[FontIsBold],
		[BorderColor]
	)
	VALUES
	(
		@DocumentItemTypeID,
		@DF_O_ID,
		@BGColor,
		@FGColor,
		@FontSize,
		@FontName,
		@FontIsBold,
		@BorderColor
	)

	Set @Err = @@Error
	Set @DocumentItemID = scope_identity()

	IF @Log = 1 
	BEGIN 
		INSERT INTO [Log].[AuditLog] ( UserId, TableName, Type, Data, Created) 
		SELECT @cUserid,'DocumentItem',0,
		( SELECT * FROM [rep].[DocumentItem] 
			WHERE
			[DocumentItemID] = @DocumentItemID				 FOR XML AUTO) as data,
				getdate() 
	 END

	RETURN @Err
END



GO
