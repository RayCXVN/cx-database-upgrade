SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[at].[prc_A_VT_get]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [at].[prc_A_VT_get] AS' 
END
GO
ALTER PROCEDURE [at].[prc_A_VT_get]
(
	@ActivityID int
)
AS
BEGIN
	SET NOCOUNT ON
	DECLARE @Err Int

	SELECT
	[AVTID],
	[ViewTypeID],
	[ActivityID],
	[No],
	[Type],
	[Template],
	[ShowPercentage],
	ISNULL([ActivityViewID],0) AS 'ActivityViewID',
	ISNULL([LevelGroupID],0) AS 'LevelGroupID',
	[CssClass]
	FROM [at].[A_VT]
	WHERE
	[ActivityID] = @ActivityID
	ORDER BY [No]

	Set @Err = @@Error

	RETURN @Err
END

GO
