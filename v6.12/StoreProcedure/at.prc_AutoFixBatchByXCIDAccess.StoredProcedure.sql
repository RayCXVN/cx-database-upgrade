SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[at].[prc_AutoFixBatchByXCIDAccess]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [at].[prc_AutoFixBatchByXCIDAccess] AS' 
END
GO
/*  2018-09-27 Ray:     Exclude paid contents
*/
ALTER PROCEDURE [at].[prc_AutoFixBatchByXCIDAccess]
(   @OwnerID            int,
    @ParentXCIDlist     varchar(max),
    @CustomerID         int,
    @DepartmentTypeID   int,
    @ActivePeriodID     int,
    @NumberOfPeriods    int = 0,
    @PROP_ReadOnly      int = 0,
    @PROP_AccessGroups  int = 0,
    @Log                bit = 1,
    @UserID             int = 0
) AS
BEGIN
    DECLARE @ActivePeriodEndDate datetime, @TT_UserType int, @TT_DepartmentType int, @RT_Standard int = 1, @PropPaymentRequired int;
    DECLARE @ActiveEntityStatusID INT = (SELECT EntityStatusID FROM EntityStatus WHERE CodeName='Active')
    SELECT @ActivePeriodEndDate = [p].[Enddate] FROM [at].[Period] p WHERE [p].[PeriodID] = @ActivePeriodID
    SELECT @TT_UserType       = [TableTypeID] FROM [TableType] WHERE [Name] = 'UserType'
    SELECT @TT_DepartmentType = [TableTypeID] FROM [TableType] WHERE [Name] = 'DepartmentType'
    SELECT @PropPaymentRequired = p.[PropertyID] FROM [prop].[Prop] p WHERE p.[MasterID] = 'A4223A44-5479-4773-9BDE-79066E9A857E';
    
    DECLARE @DepartmentTable TABLE ([DepartmentID] int)
    
    DECLARE @PeriodTable TABLE ([PeriodID] int, [ReadOnlyActivity] nvarchar(16))
    IF @NumberOfPeriods <= 0 SET @NumberOfPeriods = 1
    INSERT INTO @PeriodTable ([PeriodID], [ReadOnlyActivity]) SELECT TOP(@NumberOfPeriods - 1) p.[PeriodID], 'false'
                                                              FROM [at].[Period] p
                                                              WHERE p.[Enddate] <= @ActivePeriodEndDate AND p.[PeriodID] != @ActivePeriodID
                                                              ORDER BY p.[Enddate] DESC
    INSERT INTO @PeriodTable ([PeriodID], [ReadOnlyActivity]) VALUES (@ActivePeriodID, 'true')

    DECLARE @ParentXCIDtable TABLE ([XCID] int)
    INSERT INTO @ParentXCIDtable ([XCID]) SELECT [Value] FROM dbo.funcListToTableInt(@ParentXCIDlist, ',')

    DECLARE @XCIDtable TABLE ([ParentID] int, [XCID] int, [AccessGroupIDs] nvarchar(max))
    INSERT INTO @XCIDtable ([ParentID], [XCID])
    SELECT xc.[ParentID], xc.[XCID]
    FROM [at].[XCategory] xc
    JOIN @ParentXCIDtable pxc ON xc.[ParentID] = pxc.[XCID]
     AND NOT EXISTS (SELECT 1 FROM [prop].[PropValue] pv WHERE pv.[PropertyID] = @PropPaymentRequired AND pv.[ItemID] = xc.[XCID] AND pv.[Value] = '1');
    
    UPDATE xc SET xc.[AccessGroupIDs] = ISNULL(pv.[Value],'')
    FROM @XCIDtable xc LEFT JOIN [prop].[PropValue] pv ON pv.[PropertyID] = @PROP_AccessGroups AND pv.[ItemID] = CAST(xc.[XCID] AS nvarchar(32))

    DECLARE @Activity TABLE ([ActivityID] int, [ReadOnly] nvarchar(16))

    DECLARE @XCID int, @AccessGroupIDs nvarchar(max)
    DECLARE c_XC CURSOR LOCAL FAST_FORWARD READ_ONLY FOR
    SELECT [XCID], [AccessGroupIDs] FROM @XCIDtable

    CREATE TABLE #FixedBatch ([BatchID] int, [XCID] int, [DepartmentID] int)

    OPEN c_XC
    FETCH NEXT FROM c_XC INTO @XCID, @AccessGroupIDs
    WHILE @@FETCH_STATUS = 0
    BEGIN
        DELETE FROM @DepartmentTable
        DELETE FROM @Activity

        -- Get list of departments having access to current XCID
        INSERT INTO @DepartmentTable ([DepartmentID])
        SELECT d.[DepartmentID]
        FROM [org].[Department] d JOIN [org].[DT_D] dtd ON d.[DepartmentID] = dtd.[DepartmentID] AND d.[CustomerID] = @CustomerID AND dtd.[DepartmentTypeID] = @DepartmentTypeID
         AND EXISTS (SELECT 1 FROM [AccessGroupMember] m WHERE m.[AccessGroupID] IN (SELECT [Value] FROM dbo.funcListToTableInt(@AccessGroupIDs, ','))
                                                           AND m.[CustomerID] = d.[CustomerID]
                                                           AND (m.[DepartmentID] IS NULL OR m.[DepartmentID] = d.DepartmentID)
                    )
        
        -- Get list of activities in current XC
        INSERT INTO @Activity ([ActivityID], [ReadOnly])
        SELECT xca.[ActivityID], ISNULL(ltrim(rtrim(pv.[Value])), 'false') [ReadOnly]
        FROM [at].[XC_A] xca LEFT JOIN [prop].[PropValue] pv ON pv.[PropertyID] = @PROP_ReadOnly AND pv.[ItemID] = cast(xca.[ActivityID] as nvarchar(32))
        WHERE xca.[XCID] = @XCID

        -- Fix missing batches and write log temp table
        INSERT INTO [at].[Batch] ([SurveyID], [DepartmentID], [Name], [No], [StartDate], [EndDate], [Status], [Created])
        OUTPUT [INSERTED].[BatchID], @XCID, [INSERTED].[DepartmentID]
        INTO #FixedBatch ([BatchID], [XCID], [DepartmentID])
        SELECT s.[SurveyID], d.[DepartmentID], d.[Name], 0, s.[StartDate], s.[EndDate], 2, GETDATE()
        FROM [at].[Survey] s JOIN @Activity a ON s.[ActivityID] = a.[ActivityID] AND s.[Status] = 2 AND s.[ExtId] != 'Global'
        JOIN @PeriodTable p ON s.[PeriodID] = p.[PeriodID] AND ((a.[ReadOnly] = p.[ReadOnlyActivity] AND a.[ReadOnly] = 'true')
                                                                OR a.[ReadOnly] = 'false')
        CROSS JOIN @DepartmentTable dl
        JOIN [org].[Department] d ON dl.[DepartmentID] = d.[DepartmentID] AND d.EntityStatusID = @ActiveEntityStatusID AND d.Deleted IS NULL
        WHERE EXISTS (SELECT 1 FROM [at].[RoleMapping] rm JOIN [ObjectMapping] om ON s.[ActivityID] = rm.[ActivityID] AND om.[FromTableTypeID] = @TT_UserType AND rm.[UserTypeID] = om.[FromID]
                                                           AND om.[RelationTypeID] = @RT_Standard AND om.[ToTableTypeID] = @TT_DepartmentType
                               JOIN [org].[DT_D] dtd ON dtd.[DepartmentID] = d.[DepartmentID] AND dtd.[DepartmentTypeID] = om.[ToID])
          AND NOT EXISTS (SELECT 1 FROM [at].[Batch] b WHERE b.[SurveyID] = s.[SurveyID] AND b.[Status] = 2 AND b.[DepartmentID] = d.[DepartmentID])    

        FETCH NEXT FROM c_XC INTO @XCID, @AccessGroupIDs
    END
    CLOSE c_XC
    DEALLOCATE c_XC

    IF @Log = 1
    BEGIN
        DECLARE @EventTypeID int, @EventKeyID int, @EventID int, @Value nvarchar(max), @UserData xml,
                @FullName nvarchar(max), @DepartmentName nvarchar(max), @Username nvarchar(max), @PathName nvarchar(max), @DepartmentID int
        
        SELECT @FullName = u.FirstName + ' ' + u.LastName, @DepartmentName = d.[Name], @Username = u.[UserName], @PathName = hd.[PathName], @DepartmentID = u.[DepartmentID]
        FROM org.[User] u
        JOIN org.Department d on u.DepartmentID = d.DepartmentID AND u.[UserID] = @UserID
        JOIN org.H_D hd on hd.DepartmentID = d.DepartmentID

        SET @UserData = CONVERT(xml,N'<UserDataField UserName="' + @Username + '" FullName="' + @FullName + '" DepartmentName="' + @DepartmentName + '" HDPath="' + @PathName + '" />')

        SELECT @EventTypeID = et.[EventTypeID] FROM [log].[EventType] et WHERE et.[OwnerID] = @OwnerID AND et.[CodeName] = 'AUTOFIX_BATCH' AND et.[Active] = 1 

        INSERT INTO [log].[Event] ([EventTypeID], [UserID], [IPNumber], [Created], [ApplicationName], [DepartmentID], [CustomerID], [UserData])
        VALUES (@EventTypeID, @UserID, '::1', GETDATE(), 'By script', @DepartmentID, @CustomerID, @UserData)
        SET @EventID = SCOPE_IDENTITY()
        
        SELECT @Value = STUFF((SELECT ',' + CAST(fb.[XCID] AS nvarchar(32)) AS 'text()' FROM #FixedBatch fb GROUP BY fb.[XCID] ORDER BY fb.[XCID] FOR XML PATH ('')),
                              1, 1, '')
        INSERT INTO [log].[EventInfo] ([EventID], [EventKeyID], [Value], [Created])
        SELECT TOP 1 @EventID, [EventKeyID], ISNULL(@Value,''), GETDATE() FROM [log].[EventKey] WHERE [EventTypeID] = @EventTypeID AND [CodeName] = 'XCIDs'

        SELECT @Value = STUFF((SELECT ',' + CAST(fb.[BatchID] AS nvarchar(32)) AS 'text()' FROM #FixedBatch fb GROUP BY fb.[BatchID] ORDER BY fb.[BatchID] FOR XML PATH ('')),
                              1, 1, '')
        INSERT INTO [log].[EventInfo] ([EventID], [EventKeyID], [Value], [Created])
        SELECT TOP 1 @EventID, [EventKeyID], ISNULL(@Value,''), GETDATE() FROM [log].[EventKey] WHERE [EventTypeID] = @EventTypeID AND [CodeName] = 'BatchIDs'

        SELECT @Value = STUFF((SELECT ',' + CAST(fb.[DepartmentID] AS nvarchar(32)) AS 'text()' FROM #FixedBatch fb GROUP BY fb.[DepartmentID] ORDER BY fb.[DepartmentID] FOR XML PATH ('')),
                              1, 1, '')
        INSERT INTO [log].[EventInfo] ([EventID], [EventKeyID], [Value], [Created])
        SELECT TOP 1 @EventID, [EventKeyID], ISNULL(@Value,''), GETDATE() FROM [log].[EventKey] WHERE [EventTypeID] = @EventTypeID AND [CodeName] = 'DepartmentIDList'

        SELECT @Value = STUFF((SELECT '|' + CAST(fb.[XCID] AS nvarchar(32)) + ',' + CAST(fb.[DepartmentID] AS nvarchar(32)) AS 'text()' FROM #FixedBatch fb
                               GROUP BY fb.[XCID], fb.[DepartmentID]
                               FOR XML PATH ('')),
                              1, 1, '')
        INSERT INTO [log].[EventInfo] ([EventID], [EventKeyID], [Value], [Created])
        SELECT TOP 1 @EventID, [EventKeyID], ISNULL(@Value,''), GETDATE() FROM [log].[EventKey] WHERE [EventTypeID] = @EventTypeID AND [CodeName] = 'Mapping'
    END
    
    SELECT fb.[BatchID], fb.[XCID], fb.[DepartmentID] FROM #FixedBatch fb ORDER BY fb.[XCID], fb.[DepartmentID]
    DROP TABLE #FixedBatch
END

GO
