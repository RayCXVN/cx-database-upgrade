SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[note].[prc_Note_getByAccess]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [note].[prc_Note_getByAccess] AS' 
END
GO
/*
2017-06-14 Ray:     Improve performance; Fix calculation of permissions
2017-09-19 Sarah:   prc_Note_getByAccess get one more column StartDate 
*/
-- EXEC [note].[prc_Note_getByAccess] 12465, 2, 34, 1
ALTER PROCEDURE [note].[prc_Note_getByAccess]
(
    @UserID                     int,
    @NoteTypeID                 int,
    @HDID                       int = 0,
    @NotifyAtNextLogin          bit = 0,
    @NoteIDList                 varchar(max) = '',
    @CheckValidInDateRange      bit = 1,
    @FilterSourceTableTypeID    int = 0,
    @FilterSourceItemID         int = 0
)
AS
BEGIN
  SET NOCOUNT ON
  DECLARE @DepartmentID int, @NoteTypeGranted int = -1,
          @ReadPermission int = 0, @FullPermission int = 1, @DenyPermission int = 2, @Permission int,
          @HD_TT int, @Department_TT int, @DepartmentType_TT int, @Customer_TT int, @UserType_TT int, @User_TT int
  DECLARE @UserTableType AS TABLE (TableTypeID int, ItemID nvarchar(max))
  DECLARE @NoteTypeAccess AS TABLE (AccessRight int)
  DECLARE @NoteIDTable AS TABLE (NoteID int)

  INSERT INTO @NoteIDTable SELECT Value FROM dbo.funcListToTableInt(@NoteIDList,',')
  
  SELECT @HD_TT               = TableTypeID FROM TableType WHERE Name='H_D'
  SELECT @Department_TT       = TableTypeID FROM TableType WHERE Name='Department'
  SELECT @DepartmentType_TT   = TableTypeID FROM TableType WHERE Name='DepartmentType'
  SELECT @Customer_TT         = TableTypeID FROM TableType WHERE Name='Customer'
  SELECT @UserType_TT         = TableTypeID FROM TableType WHERE Name='UserType'
  SELECT @User_TT             = TableTypeID FROM TableType WHERE Name='User'
  
  SELECT @DepartmentID = DepartmentID FROM org.H_D WHERE HDID=@HDID
  
  -- Get all types of current user
  IF ISNULL(@HD_TT,0) > 0
      INSERT INTO @UserTableType (TableTypeID, ItemID)
      SELECT @HD_TT, Value FROM dbo.funcListToTableInt((SELECT STUFF([Path],1,1,'') FROM org.H_D where HDID=@HDID),'\')
  IF ISNULL(@Department_TT,0) > 0
      INSERT INTO @UserTableType (TableTypeID, ItemID)
      VALUES (@Department_TT, @DepartmentID)
  IF ISNULL(@DepartmentType_TT,0) > 0
      INSERT INTO @UserTableType (TableTypeID, ItemID)
      SELECT @DepartmentType_TT, dtd.DepartmentTypeID FROM org.DT_D dtd WHERE dtd.DepartmentID = @DepartmentID
  IF ISNULL(@Customer_TT,0) > 0
      INSERT INTO @UserTableType (TableTypeID, ItemID)
      SELECT @Customer_TT, ISNULL(CustomerID,0) FROM org.Department WHERE DepartmentID = @DepartmentID
  IF ISNULL(@UserType_TT,0) > 0
      INSERT INTO @UserTableType (TableTypeID, ItemID)
      SELECT @UserType_TT, UserTypeID FROM org.UT_U WHERE UserID = @UserID
  IF ISNULL(@User_TT,0) > 0
      INSERT INTO @UserTableType (TableTypeID, ItemID)
      VALUES (@User_TT, @UserID)

  -- Get permissions in note type access
  INSERT INTO @NoteTypeAccess (AccessRight)
  SELECT nta.[Type]
  FROM note.NoteTypeAccess nta JOIN @UserTableType utt ON nta.TableTypeID = utt.TableTypeID AND nta.ItemID = utt.ItemID
  WHERE nta.NoteTypeID = @NoteTypeID
    AND NOT EXISTS (SELECT cnta.TableTypeID, cnta.ItemID FROM note.Combine_NoteTypeAccess cnta WHERE cnta.NoteTypeAccessID = nta.NoteTypeAccessID
                    EXCEPT
                    SELECT utt2.TableTypeID, utt2.ItemID FROM @UserTableType utt2)
  
  IF EXISTS (SELECT 1 FROM @NoteTypeAccess WHERE AccessRight = @DenyPermission)
      SET @NoteTypeGranted = @DenyPermission
  ELSE IF EXISTS (SELECT 1 FROM @NoteTypeAccess WHERE AccessRight = @ReadPermission)
      SET @NoteTypeGranted = @ReadPermission
  ELSE IF EXISTS (SELECT 1 FROM @NoteTypeAccess WHERE AccessRight = @FullPermission)
      SET @NoteTypeGranted = @FullPermission
  ELSE SET @NoteTypeGranted = @DenyPermission
  
  DECLARE @Note TABLE (NoteID int, CreatedBy int, Created datetime, AccessType int, NotifyAtNextLogin bit, Active bit, StartDate datetime)
  -- Access right defined by NoteAccess
  IF @NoteTypeGranted = @DenyPermission
  BEGIN
      -- Get notes of Full and Read access, Read override Full
      INSERT INTO @Note (NoteID, CreatedBy, Created, AccessType, NotifyAtNextLogin, Active, StartDate)
      SELECT n.NoteID, n.CreatedBy, n.Created, min(na.[Type]), n.NotifyAtNextLogin, n.Active, n.StartDate
      FROM note.Note n WITH (NOLOCK) JOIN note.NoteAccess na WITH (NOLOCK) ON na.NoteID = n.NoteID AND n.Deleted = 0 AND na.[Type] IN (@ReadPermission, @FullPermission)
       AND ((@CheckValidInDateRange = 1 AND (n.StartDate <= GETDATE() or n.StartDate IS NULL) AND (GETDATE() <= n.EndDate or n.EndDate IS NULL))
            OR @CheckValidInDateRange = 0)
       AND n.NoteTypeID = @NoteTypeID
       AND (n.NoteID IN (SELECT NoteID FROM @NoteIDTable) OR ISNULL(@NoteIDList,'') = '')
       AND (EXISTS (SELECT 1 FROM [note].[NoteSource] ns WITH (NOLOCK) WHERE ns.[NoteID] = n.[NoteID] AND ns.[TableTypeID] = @FilterSourceTableTypeID AND ns.[ItemID] = @FilterSourceItemID)
            OR ISNULL(@FilterSourceTableTypeID,0) = 0
            OR ISNULL(@FilterSourceItemID,0) = 0)
      JOIN @UserTableType utt ON na.TableTypeID = utt.TableTypeID AND na.ItemID = utt.ItemID    -- Accesses granted to user
            -- Make sure that all combined criteria for Read/Full permissions match with user
       AND NOT EXISTS (SELECT cna.TableTypeID, cna.ItemID FROM note.Combine_NoteAccess cna WITH (NOLOCK) WHERE cna.NoteAccessID = na.NoteAccessID
                       EXCEPT
                       SELECT utt2.TableTypeID, utt2.ItemID FROM @UserTableType utt2)
            -- Search for denied accesses for selected notes
      WHERE NOT EXISTS (SELECT 1 FROM note.NoteAccess da WITH (NOLOCK) JOIN @UserTableType utt1 ON da.TableTypeID = utt1.TableTypeID AND da.ItemID = utt1.ItemID AND da.NoteID = n.NoteID AND da.[Type] = @DenyPermission
                        -- Make sure that all combined criteria for Deny permissions match with user
                        AND NOT EXISTS (SELECT cna.TableTypeID, cna.ItemID FROM note.Combine_NoteAccess cna WITH (NOLOCK) WHERE cna.NoteAccessID = da.NoteAccessID
                                        EXCEPT
                                        SELECT utt2.TableTypeID, utt2.ItemID FROM @UserTableType utt2))
      GROUP BY n.NoteID, n.CreatedBy, n.Created, n.NotifyAtNextLogin, n.Active, n.StartDate
  
      -- Get self created notes
      INSERT INTO @Note (NoteID, CreatedBy, Created, AccessType, NotifyAtNextLogin, Active, StartDate)
      SELECT n.NoteID, n.CreatedBy, n.Created, @FullPermission, n.NotifyAtNextLogin, n.Active, n.StartDate
      FROM note.Note n WITH (NOLOCK)
      WHERE n.Deleted = 0 AND n.CreatedBy = @UserID
        AND ((@CheckValidInDateRange = 1 AND (n.StartDate <= GETDATE() or n.StartDate IS NULL) AND (GETDATE() <= n.EndDate or n.EndDate IS NULL))
            OR @CheckValidInDateRange = 0)
        AND n.NoteTypeID = @NoteTypeID
        AND (n.NoteID IN (SELECT NoteID FROM @NoteIDTable) OR ISNULL(@NoteIDList,'') = '')
            -- Search for denied accesses for selected notes
        AND NOT EXISTS (SELECT 1 FROM note.NoteAccess da WITH (NOLOCK) JOIN @UserTableType utt1 ON da.TableTypeID = utt1.TableTypeID AND da.ItemID = utt1.ItemID AND da.NoteID = n.NoteID AND da.[Type] = @DenyPermission
                        -- Make sure that all combined criteria for Deny permissions match with user
                        AND NOT EXISTS (SELECT cna.TableTypeID, cna.ItemID FROM note.Combine_NoteAccess cna WITH (NOLOCK) WHERE cna.NoteAccessID = da.NoteAccessID
                                        EXCEPT
                                        SELECT utt2.TableTypeID, utt2.ItemID FROM @UserTableType utt2))
        -- Distinct from existing notes in the table
        AND NOT EXISTS (SELECT 2 FROM @Note tn WHERE tn.NoteID = n.NoteID)
        AND (EXISTS (SELECT 1 FROM [note].[NoteSource] ns WITH (NOLOCK) WHERE ns.[NoteID] = n.[NoteID] AND ns.[TableTypeID] = @FilterSourceTableTypeID AND ns.[ItemID] = @FilterSourceItemID)
            OR ISNULL(@FilterSourceTableTypeID,0) = 0
            OR ISNULL(@FilterSourceItemID,0) = 0)
  END
  -- NoteTypeAccess gets involved
  ELSE IF @NoteTypeGranted = @ReadPermission OR @NoteTypeGranted = @FullPermission
  BEGIN
      -- Get notes of Full and Read access, Read override Full, self created notes included
      INSERT INTO @Note (NoteID, CreatedBy, Created, AccessType, NotifyAtNextLogin, Active, StartDate)
      SELECT n.NoteID, n.CreatedBy, n.Created, (CASE WHEN min(na.[Type]) IS NOT NULL THEN min(na.[Type])
                                                     WHEN n.CreatedBy = @UserID THEN @FullPermission
                                                     ELSE @NoteTypeGranted
                                                END) AS AccessRight,
             n.NotifyAtNextLogin, n.Active, n.StartDate
      FROM note.Note n WITH (NOLOCK)
      LEFT JOIN (SELECT nac.* FROM note.NoteAccess nac WITH (NOLOCK) JOIN @UserTableType utt ON nac.TableTypeID = utt.TableTypeID AND nac.ItemID = utt.ItemID AND nac.[Type] IN (@ReadPermission, @FullPermission)) na ON na.NoteID = n.NoteID
      WHERE n.Deleted = 0
        AND ((@CheckValidInDateRange = 1 AND (n.StartDate <= GETDATE() or n.StartDate IS NULL) AND (GETDATE() <= n.EndDate or n.EndDate IS NULL))
            OR @CheckValidInDateRange = 0)
        AND n.NoteTypeID = @NoteTypeID
        AND (n.NoteID IN (SELECT NoteID FROM @NoteIDTable) OR ISNULL(@NoteIDList,'') = '')
            -- Search for denied accesses for selected notes
        AND NOT EXISTS (SELECT 1 FROM note.NoteAccess da WITH (NOLOCK) JOIN @UserTableType utt1 ON da.TableTypeID = utt1.TableTypeID AND da.ItemID = utt1.ItemID AND da.NoteID = n.NoteID AND da.[Type] = @DenyPermission
                        -- Make sure that all combined criteria for Deny permissions match with user
                        AND NOT EXISTS (SELECT cna.TableTypeID, cna.ItemID FROM note.Combine_NoteAccess cna WITH (NOLOCK) WHERE cna.NoteAccessID = da.NoteAccessID
                                        EXCEPT
                                        SELECT utt2.TableTypeID, utt2.ItemID FROM @UserTableType utt2))
        AND (EXISTS (SELECT 1 FROM [note].[NoteSource] ns WITH (NOLOCK) WHERE ns.[NoteID] = n.[NoteID] AND ns.[TableTypeID] = @FilterSourceTableTypeID AND ns.[ItemID] = @FilterSourceItemID)
            OR ISNULL(@FilterSourceTableTypeID,0) = 0
            OR ISNULL(@FilterSourceItemID,0) = 0)
      GROUP BY n.NoteID, n.CreatedBy, n.Created, n.NotifyAtNextLogin, n.Active, n.StartDate
  END

  SELECT * FROM @Note

  -- DECLARE @Note TABLE (NoteID int, CreatedBy int, Created datetime, AccessType int, NotifyAtNextLogin bit, Active bit)
  -- SELECT * FROM @Note

END
GO
