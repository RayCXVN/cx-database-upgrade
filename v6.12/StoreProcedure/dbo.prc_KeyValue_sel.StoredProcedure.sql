SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[prc_KeyValue_sel]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [dbo].[prc_KeyValue_sel] AS' 
END
GO

ALTER PROCEDURE [dbo].[prc_KeyValue_sel]
(
	@Id int,
	@Type tinyint,
	@Key varchar(50),
	@Value nvarchar(1000) output
)
As
BEGIN
	SET NOCOUNT ON
	DECLARE @Err Int
	set @Value = 'Not found'

	Select
	@Id = [Id],
	@Type = [Type],
	@Key = [Key],
	@Value = [Value]
	FROM [KeyValue]
	WHERE
	[Id] = @Id AND
	[Type] = @Type AND
	[Key] = @Key

	Set @Err = @@Error

	RETURN @Err
End

GO
