SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[prop].[prc_PropValue_Get_All_By_TableTypeID_And_DepartmentID]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [prop].[prc_PropValue_Get_All_By_TableTypeID_And_DepartmentID] AS' 
END
GO
ALTER PROCEDURE [prop].[prc_PropValue_Get_All_By_TableTypeID_And_DepartmentID]
(
    @TableTypeID    int,
    @DepartmentID   int,
    @CustomerID     int = 0
)
AS
BEGIN
    SET NOCOUNT ON
    DECLARE @Err Int

    SELECT
        pv.[PropValueID],
        pv.[PropertyID],
        ISNULL(pv.[PropOptionID], 0) AS 'PropOptionID',
        ISNULL(pv.[PropFileID], 0) AS 'PropFileID',
        pv.[No],
        pv.[Created],
        ISNULL(pv.[CreatedBy], 0) AS 'CreatedBy',
        pv.[Updated],
        ISNULL(pv.[UpdatedBy], 0) AS 'UpdatedBy',
        pv.[Value],
        pv.ItemID,
        pv.[CustomerID]
    FROM 
        [prop].[PropPage] pp 
        INNER JOIN [prop].[prop] p ON p.PropPageID = pp.PropPageID
        INNER JOIN [prop].PropValue pv ON pv.PropertyID = p.PropertyId
            AND pv.ItemID IN
            (
	            SELECT UserID FROM Org.[User] WHERE DepartmentID = @DepartmentID
            )
    WHERE pp.TableTypeID = @TableTypeID
      AND (pv.[CustomerID] = @CustomerID OR ISNULL(@CustomerID,0) = 0)
    ORDER BY pv.ItemID, pv.[PropertyID]
    
    Set @Err = @@Error

    RETURN @Err
END

GO
