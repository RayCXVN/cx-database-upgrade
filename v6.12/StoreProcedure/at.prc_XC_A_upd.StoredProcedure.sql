SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[at].[prc_XC_A_upd]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [at].[prc_XC_A_upd] AS' 
END
GO

ALTER PROCEDURE [at].[prc_XC_A_upd]
(
	@XCID int,
	@ActivityID int,
	@No smallint,
	@Listview smallint,
	@Descview smallint,
	@Chartview smallint,
	@FillColor varchar(16),
	@BorderColor varchar(16),
	@cUserid int,
	@Log smallint = 1
)
AS
BEGIN
	SET NOCOUNT ON
	DECLARE @Err Int

	UPDATE [at].[XC_A]
	SET
		[XCID] = @XCID,
		[ActivityID] = @ActivityID,
		[No] = @No,
		[Listview] = @Listview,
		[Descview] = @Descview,
		[Chartview] = @Chartview,
		[FillColor] = @FillColor,
		[BorderColor] = @BorderColor
	WHERE
		[XCID] = @XCID AND
		[ActivityID] = @ActivityID

	IF @Log = 1 
	BEGIN 
		INSERT INTO [Log].[AuditLog] ( UserId, TableName, Type, Data, Created) 
		SELECT @cUserid,'XC_A',1,
		( SELECT * FROM [at].[XC_A] 
			WHERE
			[XCID] = @XCID AND
			[ActivityID] = @ActivityID			 FOR XML AUTO) as data,
			getdate()
	END

	Set @Err = @@Error

	RETURN @Err
END


GO
