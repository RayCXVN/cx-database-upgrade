SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[Logon_STS]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [dbo].[Logon_STS] AS' 
END
GO
/*
    Sarah - Oct 10, 2016 - Remove column Status on User, Department and Result tables
*/
ALTER PROCEDURE [dbo].[Logon_STS]
	@UserName	nvarchar(128),
	@PassWord	nvarchar(64),
	@UserID		int		OUTPUT,
	@LanguageID	int=1		OUTPUT,
	@DepartmentID	int		OUTPUT,
	@Firstname	nvarchar(64) 	OUTPUT,
	@Lastname	nvarchar(64) 	OUTPUT,
	@Email		nvarchar(256) 	OUTPUT,
	@Mobile		nvarchar(16) 	OUTPUT,
	@ExtId		nvarchar(64) 	OUTPUT,
	@EntityStatus int OUTPUT,
	@OwnerId	int		OUTPUT,
	@ErrNo		int		OUTPUT
AS
BEGIN
	DECLARE @EntityStatusID INT = 0
	SELECT @EntityStatusID = EntityStatusID FROM dbo.EntityStatus WHERE CodeName = N'Active'

	SELECT 	
		@DepartmentID = DepartmentID,
		@LanguageID = LanguageID,
		@UserID = UserID,
		@Firstname = Firstname,
		@Lastname = Lastname,
		@Email = Email,
		@Mobile = Mobile,
		@ExtId = ExtId,
		@EntityStatus = EntityStatusID,
		@OwnerId = OwnerId
	FROM 	
		[org].[User] 
	WHERE 	
		[UserName] = @UserName
		AND [Password] = @Password	
		AND [EntityStatusID] = @EntityStatusID
		AND Deleted IS NULL

	IF @@RowCount < 1 
	BEGIN
		SET @ErrNo = 1
	END
	ELSE
	BEGIN	
		SET @ErrNo = 0
	END
END

GO
