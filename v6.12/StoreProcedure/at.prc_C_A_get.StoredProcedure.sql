SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[at].[prc_C_A_get]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [at].[prc_C_A_get] AS' 
END
GO

ALTER PROCEDURE [at].[prc_C_A_get]
(
	@ActivityID int
)
AS
BEGIN
	SET NOCOUNT ON
	DECLARE @Err Int

	SELECT
	[CustomerID],
	[ActivityID],
	[No]
	FROM [at].[C_A]
	WHERE
	[ActivityID] = @ActivityID
	ORDER BY [No]

	Set @Err = @@Error

	RETURN @Err
END


GO
