SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[res].[prc_LT_Resource_get]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [res].[prc_LT_Resource_get] AS' 
END
GO
ALTER PROCEDURE [res].[prc_LT_Resource_get]  
(  
 @ResourceID int  
)  
AS  
BEGIN  
 SET NOCOUNT ON  
 DECLARE @Err Int  
  
 SELECT  
 [ResourceID],  
 [LanguageID],  
 [Name],  
 [Description],
 [URL],
 [Keyword],
 isnull([NoteHeading],'') [NoteHeading],
 isnull([CompletedCheckText],'') [CompletedCheckText],
 isnull([CompletedInfoText],'') [CompletedInfoText],
 isnull([Tooltip],'') [Tooltip]
 FROM [res].[LT_Resource]  
 WHERE  
 [ResourceID] = @ResourceID  
  
 Set @Err = @@Error  
  
 RETURN @Err  
END 

GO
