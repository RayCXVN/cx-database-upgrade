SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[form].[prc_FormCell_ins]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [form].[prc_FormCell_ins] AS' 
END
GO
/*
	2016-12-05 Sarah - Increase Form.CssClass from 256 to 4000, FormCell.CssClass from 128 to 4000
*/
ALTER PROCEDURE [form].[prc_FormCell_ins]
(
	@FormCellID int = null output,
	@FormRowID int,
	@No smallint,
	@CellTypeID smallint,
	@FormFieldID INT=NULL,
	@FormCommandID INT=NULL,
	@CssClass nvarchar(4000),
	@Alignment smallint,
	@Colspan smallint,
	@Width smallint,
	@cUserid int,
	@Log smallint = 1
)
AS
BEGIN
	SET NOCOUNT ON
	DECLARE @Err Int

	INSERT INTO [form].[FormCell]
	(
		[FormRowID],
		[No],
		[CellTypeID],
		[FormFieldID],
		[FormCommandID],
		[CssClass],
		[Alignment],
		[Colspan],
		[Width]
	)
	VALUES
	(
		@FormRowID,
		@No,
		@CellTypeID,
		@FormFieldID,
		@FormCommandID,
		@CssClass,
		@Alignment,
		@Colspan,
		@Width
	)

	Set @Err = @@Error
	Set @FormCellID = scope_identity()

	IF @Log = 1 
	BEGIN 
		INSERT INTO [Log].[AuditLog] ( UserId, TableName, Type, Data, Created) 
		SELECT @cUserid,'FormCell',0,
		( SELECT * FROM [form].[FormCell] 
			WHERE
			[FormCellID] = @FormCellID				 FOR XML AUTO) as data,
				getdate() 
	 END

	RETURN @Err
END


GO
