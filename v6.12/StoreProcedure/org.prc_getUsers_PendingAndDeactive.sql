IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[org].[prc_getUsers_PendingAndDeactive]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [org].[prc_getUsers_PendingAndDeactive] AS' 
END
GO
ALTER PROCEDURE [org].[prc_getUsers_PendingAndDeactive]
(  
    @DepartmentID int  
)  
AS  
BEGIN  
    /*  2017.07.09 by Rickie
    */

    SET NOCOUNT ON  
    DECLARE @Err Int, @EntityStatusID INT = 0  		

    SELECT
        [UserID],  
        [Ownerid],  
        [DepartmentID],  
        [LanguageID],  
        ISNULL([RoleID], 0) AS 'RoleID',  
        [UserName],  
        [Password],  
        [LastName],  
        [FirstName],  
        [Email],  
        [Mobile],  
        [ExtID],  
        [SSN],  
        [Tag],  
        [Locked],  
        [ChangePassword],  
        [HashPassword],  
        [SaltPassword],  
        [OneTimePassword],  
        [OTPExpireTime],  
        [Created],  
        [CountryCode],
        [Gender],
        [DateOfBirth],
        [ForceUserLoginAgain],
        [LastUpdated],
        [LastUpdatedBy],
        [LastSynchronized],
        [ArchetypeID],
        [CustomerID],
        [Deleted],
        [EntityStatusID],
        [EntityStatusReasonID]
    FROM [org].[User]  
    WHERE [DepartmentID] = @DepartmentID  		
      AND [Deleted] IS NULL 
      AND [EntityStatusID] IN
        (
            SELECT EntityStatusID FROM dbo.EntityStatus WHERE CodeName = N'Deactive' OR CodeName = N'Pending'
        )  

    SET @Err = @@Error  

    RETURN @Err  
END
GO