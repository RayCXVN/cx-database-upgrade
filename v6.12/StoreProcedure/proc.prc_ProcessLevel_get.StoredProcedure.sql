SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[proc].[prc_ProcessLevel_get]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [proc].[prc_ProcessLevel_get] AS' 
END
GO

ALTER PROCEDURE [proc].[prc_ProcessLevel_get]
(
	@ProcessID int
)
AS
BEGIN
	SET NOCOUNT ON
	DECLARE @Err Int

	SELECT
	[ProcessLevelID],
	[ProcessValueID],
	ISNULL([ProcessID], 0) AS 'ProcessID',
	[Created]
	FROM [proc].[ProcessLevel]
	WHERE
	[ProcessID] = @ProcessID

	Set @Err = @@Error

	RETURN @Err
END


GO
