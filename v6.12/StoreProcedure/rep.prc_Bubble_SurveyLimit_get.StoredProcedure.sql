SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[rep].[prc_Bubble_SurveyLimit_get]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [rep].[prc_Bubble_SurveyLimit_get] AS' 
END
GO

ALTER PROCEDURE [rep].[prc_Bubble_SurveyLimit_get]
(
	@BubbleID int
)
AS
BEGIN
	SET NOCOUNT ON
	DECLARE @Err Int

	SELECT
	[BubbleID],
	[SurveyID],
	[AxisNo],
	[LimitLow],
	[LimitHigh]
	FROM [rep].[Bubble_SurveyLimit]
	WHERE
	[BubbleID] = @BubbleID

	Set @Err = @@Error

	RETURN @Err
END


GO
