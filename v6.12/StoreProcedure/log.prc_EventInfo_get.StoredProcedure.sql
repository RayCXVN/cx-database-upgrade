SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[log].[prc_EventInfo_get]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [log].[prc_EventInfo_get] AS' 
END
GO
ALTER PROCEDURE [log].[prc_EventInfo_get]
	@EventInfoID	int
AS
BEGIN
	SET NOCOUNT ON;
	DECLARE @Err Int
	
     SELECT [EventInfoID],
		  [EventID],
		  [EventKeyID],
		  [Value],
		  [Created]
	FROM  [log].[EventInfo]
	WHERE [EventInfoID] = @EventInfoID
	
	Set @Err = @@Error

	RETURN @Err
  
END

GO
