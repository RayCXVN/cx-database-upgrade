SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[app].[prc_LT_MenuItem_upd]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [app].[prc_LT_MenuItem_upd] AS' 
END
GO
ALTER PROCEDURE [app].[prc_LT_MenuItem_upd]  
(  
 @LanguageID int,  
 @MenuItemID int,  
 @Name nvarchar(256)='',  
 @Description NTEXT='',  
 @ToolTip nvarchar(512)='',  
 @URL nvarchar(512)='',  
 @cUserid int,  
 @Log smallint = 1  
)  
AS  
BEGIN  
 SET NOCOUNT ON  
 DECLARE @Err Int  
  
 UPDATE [app].[LT_MenuItem]  
 SET  
  [LanguageID] = @LanguageID,  
  [MenuItemID] = @MenuItemID,  
  [Name] = @Name,  
  [Description] = @Description,  
  [ToolTip] = @ToolTip,
  [URL]  = @URL
 WHERE  
  [LanguageID] = @LanguageID AND  
  [MenuItemID] = @MenuItemID  
  
 IF @Log = 1   
 BEGIN   
  INSERT INTO [Log].[AuditLog] ( UserId, TableName, Type, Data, Created)   
  SELECT @cUserid,'LT_MenuItem',1,  
  ( SELECT * FROM [app].[LT_MenuItem]   
   WHERE  
   [LanguageID] = @LanguageID AND  
   [MenuItemID] = @MenuItemID    FOR XML AUTO) as data,  
   getdate()  
 END  
  
 Set @Err = @@Error  
  
 RETURN @Err  
END  
  

GO
