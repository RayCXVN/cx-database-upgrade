SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[app].[prc_Site_Language_ins]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [app].[prc_Site_Language_ins] AS' 
END
GO
ALTER PROCEDURE [app].[prc_Site_Language_ins]
	@LanguageID int,
	@SiteID int,
	@No int,
	@cUserid int,
    @Log smallint = 1
AS
BEGIN
	SET NOCOUNT ON;
	DECLARE @Err Int
    
    INSERT INTO [app].[Site_Language]
           ([LanguageID]
           ,[SiteID]
           ,[No])
     VALUES
           (@LanguageID
           ,@SiteID
           ,@No)
           
    Set @Err = @@Error
    IF @Log = 1 
	BEGIN 
		INSERT INTO [Log].[AuditLog] ( UserId, TableName, Type, Data, Created) 
		SELECT @cUserid,'Site_Language',0,
		( SELECT * FROM [app].[Site_Language]
			WHERE
			[LanguageID] = @LanguageID AND
			[SiteID] = @SiteID				 FOR XML AUTO) as data,
				getdate() 
	END
	
	RETURN @Err       
END

GO
