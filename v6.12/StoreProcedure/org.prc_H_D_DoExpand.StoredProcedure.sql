SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[org].[prc_H_D_DoExpand]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [org].[prc_H_D_DoExpand] AS' 
END
GO
ALTER PROCEDURE [org].[prc_H_D_DoExpand]
(
	@HDIDS		varchar(max),
	@ExpandHdIds	varchar(max)	
)
AS
BEGIN
	SET NOCOUNT ON
	DECLARE @Err Int
    DECLARE @ActiveEntityStatusID INT = (SELECT EntityStatusID FROM EntityStatus WHERE CodeName='Active')
	
	Declare @tblHDIds table (HDID int)
	
	Insert INTO @tblHDIds(HDID)
	Select Value from dbo.funcListToTableInt(@HDIDS, ',')	
	
	Declare @tblExpandHdIds table (HDID int)	
	
	Insert INTO @tblExpandHdIds(HDID)
	Select Value from dbo.funcListToTableInt(@ExpandHdIds, ',')
	
	Select tbl.HDID 
	from @tblHDIds tbl
	Where EXISTS(SELECT TOP 1 * 
			FROM org.H_D hd LEFT JOIN org.Department d ON hd.DepartmentID = d.DepartmentID
			WHERE hd.Path LIKE '%\' + convert(VARCHAR(max),tbl.HDID) + '\%' 
			AND hd.HDID != tbl.HDID
			AND hd.Deleted = 0
			AND hd.HDID IN (SELECT tble.HDID FROM @tblExpandHdIds tble)
			AND (d.DepartmentID IS NULL OR (d.DepartmentID IS NOT NULL AND d.EntityStatusID = @ActiveEntityStatusID AND d.Deleted IS NULL)))
	
	Set @Err = @@Error
	
	RETURN @Err		
		
END

GO
