SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[at].[prc_ResultPage_get]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [at].[prc_ResultPage_get] AS' 
END
GO
 ALTER PROCEDURE [at].[prc_ResultPage_get]
(
	@SurveyID 	int,
	@RoleID 	int,
	@DepartmentID 	int,
	@BatchID	int = null
)
As
    DECLARE @ActivityID INT
    DECLARE @Customerid INT
    SELECT @Customerid = CustomerId FROM org.department WHERE departmentid = @DepartmentID
    SELECT @ActivityID = ActivityID FROM at.survey Where SurveyID = @SurveyID 

	Select
      DISTINCT      
      P.[PageID]      
      FROM [at].[Page] P with (nolock)      
      LEFT OUTER JOIN [at].[Access] A with (nolock)  ON 
      (
         (P.PageID = A.PageID AND A.[QuestionID] IS NULL) 
      OR (A.PageID IS NULL AND A.[QuestionID] IS NULL)  
      ) 
      AND A.SurveyID = @SurveyID
      WHERE 
            A.SurveyID = @SurveyID
            AND (A.BatchID = @BatchID Or A.BatchID IS NULL)
            AND (A.RoleID = @RoleID Or A.RoleID IS NULL)
            AND (A.DepartmentID = @DepartmentID OR A.DepartmentID IS NULL)
            AND (A.DepartmentTypeID IS NULL OR A.DepartmentTypeID IN (SELECT DepartmentTypeID FROM org.DT_D with (nolock)  WHERE DepartmentID = @DepartmentID))
			AND (A.CustomerID is NULL OR  A.Customerid = @Customerid)  




GO
