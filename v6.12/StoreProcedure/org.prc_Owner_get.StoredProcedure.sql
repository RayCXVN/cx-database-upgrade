SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[org].[prc_Owner_get]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [org].[prc_Owner_get] AS' 
END
GO
ALTER PROCEDURE [org].[prc_Owner_get]
AS
BEGIN
	SET NOCOUNT ON
	DECLARE @Err Int

	SELECT
	[OwnerID],
	[LanguageID],
	[Name],
	[ReportServer],
	[ReportDB],
	ISNULL([MainHierarchyId], 0) AS 'MainHierarchyId',
	[OLAPServer],
	[OLAPDB],
	[Css],
	[Url],
	[LoginType],
	[Prefix],
	[Description],
	[Logging],
	ISNULL([Created], 0) AS 'Created',
	OTPLength,
	OTPCharacters,
	OTPAllowLowercase,
	OTPAllowUppercase,
	UseOTPCaseSensitive,
	[UseHashPassword],
    [UseOTP],
    [DefaultHashMethod],
    [OTPDuration]
	FROM [org].[Owner]
	
	Set @Err = @@Error

	RETURN @Err
END

GO
