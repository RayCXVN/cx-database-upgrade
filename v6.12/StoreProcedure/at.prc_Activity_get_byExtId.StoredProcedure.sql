SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[at].[prc_Activity_get_byExtId]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [at].[prc_Activity_get_byExtId] AS' 
END
GO

ALTER PROCEDURE [at].[prc_Activity_get_byExtId]  
(  
 @OwnerID int,  
 @ExtId nvarchar(64)  
)  
AS  
BEGIN  
 SET NOCOUNT ON  
 DECLARE @Err Int  
  
 SELECT  
 [ActivityID],  
 [LanguageID],  
 [OwnerID],  
 [StyleSheet],  
 [Type],  
 [No],  
 [TooltipType],  
 [Listview],  
 [Descview],  
 [Chartview],  
 [OLAPServer],  
 [OLAPDB],  
 [Created],  
 [SelectionHeaderType],  
 [ExtID]  
 FROM [at].[Activity]  
 WHERE  
 [OwnerID] = @OwnerID  
 and [ExtID]= @ExtId  
 ORDER BY [No]  
  
 Set @Err = @@Error  
  
 RETURN @Err  
END  
GO
