SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[at].[prc_StatusAction_del]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [at].[prc_StatusAction_del] AS' 
END
GO




ALTER PROCEDURE [at].[prc_StatusAction_del]
(
	@StatusActionID int,
	@cUserid int,
	@Log smallint = 1
)
AS
BEGIN
	SET NOCOUNT ON
	DECLARE @Err Int

	IF @Log = 1 
	BEGIN 
		INSERT INTO [Log].[AuditLog] ( UserId, TableName, Type, Data, Created) 
		SELECT @cUserid,'StatusAction',2,
		( SELECT * FROM [at].[StatusAction] 
			WHERE
			[StatusActionID] = @StatusActionID
			 FOR XML AUTO) as data,
			getdate() 
	END 


	DELETE FROM [at].[StatusAction]
	WHERE
		[StatusActionID] = @StatusActionID

	Set @Err = @@Error

	RETURN @Err
END


GO
