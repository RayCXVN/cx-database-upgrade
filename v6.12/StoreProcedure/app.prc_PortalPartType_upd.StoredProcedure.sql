SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[app].[prc_PortalPartType_upd]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [app].[prc_PortalPartType_upd] AS' 
END
GO

ALTER PROCEDURE [app].[prc_PortalPartType_upd]
(
	@PortalPartTypeID int,
	@No smallint,
	@cUserid int,
	@Log smallint = 1
)
AS
BEGIN
	SET NOCOUNT ON
	DECLARE @Err Int

	UPDATE [app].[PortalPartType]
	SET
		[No] = @No
	WHERE
		[PortalPartTypeID] = @PortalPartTypeID

	IF @Log = 1 
	BEGIN 
		INSERT INTO [Log].[AuditLog] ( UserId, TableName, Type, Data, Created) 
		SELECT @cUserid,'PortalPartType',1,
		( SELECT * FROM [app].[PortalPartType] 
			WHERE
			[PortalPartTypeID] = @PortalPartTypeID			 FOR XML AUTO) as data,
			getdate()
	END

	Set @Err = @@Error

	RETURN @Err
END


GO
