SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[at].[prc_QA_Calc_del]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [at].[prc_QA_Calc_del] AS' 
END
GO


ALTER PROCEDURE  [at].[prc_QA_Calc_del]
(
	@QAID int,
	@cUserid int,
	@Log smallint = 1
)
AS
BEGIN
	SET NOCOUNT ON
	DECLARE @Err Int

	IF @Log = 1 
	BEGIN 
		INSERT INTO [Log].[AuditLog] ( UserId, TableName, Type, Data, Created) 
		SELECT @cUserid,'QA_Calc',2,
		( SELECT * FROM [at].[QA_Calc] 
			WHERE
			[QAID] = @QAID
			 FOR XML AUTO) as data,
			getdate() 
	END 


	DELETE FROM [at].[QA_Calc]
	WHERE
		[QAID] = @QAID

	Set @Err = @@Error

	RETURN @Err
END


GO
