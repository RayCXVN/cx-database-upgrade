SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[at].[prc_LT_Mail_del]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [at].[prc_LT_Mail_del] AS' 
END
GO


ALTER PROCEDURE [at].[prc_LT_Mail_del]
(
	@LanguageID int,
	@MailID int,
	@cUserid int,
	@Log smallint = 1
)
AS
BEGIN
	SET NOCOUNT ON
	DECLARE @Err Int

	IF @Log = 1 
	BEGIN 
		INSERT INTO [Log].[AuditLog] ( UserId, TableName, Type, Data, Created) 
		SELECT @cUserid,'LT_Mail',2,
		( SELECT * FROM [at].[LT_Mail] 
			WHERE
			[LanguageID] = @LanguageID AND
			[MailID] = @MailID
			 FOR XML AUTO) as data,
			getdate() 
	END 


	DELETE FROM [at].[LT_Mail]
	WHERE
		[LanguageID] = @LanguageID AND
		[MailID] = @MailID

	Set @Err = @@Error

	RETURN @Err
END


GO
