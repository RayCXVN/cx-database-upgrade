SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[prc_SurveysStatusByHDIDAndStatusType]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [dbo].[prc_SurveysStatusByHDIDAndStatusType] AS' 
END
GO
-- select * from H_D where hierarchyid = 21
-- dbo.prc_SurveysStatusByHDIDAndStatusType 17319,0,null,142,null,15578
ALTER PROC [dbo].[prc_SurveysStatusByHDIDAndStatusType]
(
  @HDID AS INT,
  @bSub AS BIT = 0,
  @Roleid AS INT = NULL,
  @SurveyID as INT = NULL,
  @DepartmentID as INT = NULL,
  @UserGroupID as INT = NULL
)
AS

DECLARE @Path AS NVARCHAR(256) 
Declare @HID as INT
DECLARE @ActiveEntityStatusID INT = (SELECT EntityStatusID FROM EntityStatus WHERE CodeName='Active')
SELECT @Path = dbo.GetPath(@HDID)
Select @HID = HierarchyID from h_d where HDID = @HDID

IF @bSub <> 0
BEGIN
	SELECT @HDID AS HDID,surveyid,COUNT(*) AS antall,ISNULL(statustypeid,0) AS statustypeid 
	FROM RESULT  JOIN org.department ON department.departmentid =  RESULT.departmentid and result.EntityStatusID = @ActiveEntityStatusID and result.Deleted IS NULL
			     JOIN org.[User] on [User].UserID = Result.UserID AND [User].EntityStatusID = @ActiveEntityStatusID AND [User].Deleted IS NULL
	AND (RESULT.RoleID = @Roleid OR @RoleID is null )
	AND (SurveyID = @Surveyid OR @SurveyID is null )
	AND (RESULT.DepartmentID = @DepartmentID OR @DepartmentID is null )
	AND (UserGroupID = @UserGroupID OR @UserGroupID is null )
	AND department.departmentid IN (SELECT departmentid FROM org.H_D WHERE PATH LIKE '%\' + CAST(@HDID AS VARCHAR(10)) + '\%')
	GROUP BY  surveyid,ISNULL(statustypeid,0)
	ORDER BY surveyid
END
ELSE
BEGIN
    
    --Select a.parentid,a.surveyid,sum(a.Antall),a.statustypeid from
    --(
	SELECT SUBSTRING(REPLACE(h_d.PATH,@Path,''),0,CHARINDEX('\',REPLACE(h_d.PATH,@Path,''))) HDID,surveyid,COUNT(resultid) AS antall,ISNULL(statustypeid,0) AS statustypeid 
	FROM RESULT  
	JOIN org.department ON department.departmentid =  RESULT.departmentid AND Result.EntityStatusID = @ActiveEntityStatusID AND Result.Deleted IS NULL
	JOIN org.[User] on [User].UserID = Result.UserID AND [User].EntityStatusID = @ActiveEntityStatusID AND [User].Deleted IS NULL
	AND (RESULT.RoleID = @Roleid OR @RoleID is null )
	AND (SurveyID = @Surveyid OR @SurveyID is null )
	AND (RESULT.DepartmentID = @DepartmentID OR @DepartmentID is null )
	AND department.departmentid IN (SELECT departmentid FROM org.H_D WHERE PATH LIKE '%\' + CAST(@HDID AS VARCHAR(10)) + '\%' and HDID <> @HDID)
	JOIN org.h_d ON H_D.departmentid = department.departmentid and H_D.hierarchyID = @HID

	GROUP BY  SUBSTRING(REPLACE(h_d.PATH,@Path,''),0,CHARINDEX('\',REPLACE(h_d.PATH,@Path,''))),surveyid,ISNULL(statustypeid,0)
	
	ORDER BY surveyid
	--) as a
	--Group by a.parentid,a.surveyid,a.statustypeid
	-- order by surveyid
END
-------------------------------------------------------------------------------------


GO
