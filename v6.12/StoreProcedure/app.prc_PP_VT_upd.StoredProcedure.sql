SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[app].[prc_PP_VT_upd]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [app].[prc_PP_VT_upd] AS' 
END
GO

ALTER PROCEDURE [app].[prc_PP_VT_upd]
(
	@PortalPartID int,
	@ViewTypeID int,
	@No smallint,
	@cUserid int,
	@Log smallint = 1
)
AS
BEGIN
	SET NOCOUNT ON
	DECLARE @Err Int

	UPDATE [app].[PP_VT]
	SET
		[PortalPartID] = @PortalPartID,
		[ViewTypeID] = @ViewTypeID,
		[No] = @No
	WHERE
		[PortalPartID] = @PortalPartID AND
		[ViewTypeID] = @ViewTypeID

	IF @Log = 1 
	BEGIN 
		INSERT INTO [Log].[AuditLog] ( UserId, TableName, Type, Data, Created) 
		SELECT @cUserid,'PP_VT',1,
		( SELECT * FROM [app].[PP_VT] 
			WHERE
			[PortalPartID] = @PortalPartID AND
			[ViewTypeID] = @ViewTypeID			 FOR XML AUTO) as data,
			getdate()
	END

	Set @Err = @@Error

	RETURN @Err
END


GO
