SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[note].[prc_NoteAccess_upd]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [note].[prc_NoteAccess_upd] AS' 
END
GO
ALTER PROCEDURE [note].[prc_NoteAccess_upd]
(
	@NoteAccessID int,
	@NoteID int,
	@TableTypeID int,
	@ItemID int,
	@Type smallint,
	@cUserid int,
	@Log smallint = 1
)
AS
BEGIN
	SET NOCOUNT ON
	DECLARE @Err Int

	UPDATE [note].[NoteAccess]
	SET
		[NoteID] = @NoteID,
		[TableTypeID] = @TableTypeID,
		[ItemID] = @ItemID,
		[Type] = @Type
	WHERE
		[NoteAccessID] = @NoteAccessID
	
	Set @Err = @@Error
	
	IF @Log = 1 
	BEGIN 
		INSERT INTO [Log].[AuditLog] ( UserId, TableName, Type, Data, Created) 
		SELECT @cUserid,'NoteAccess',1,
		( SELECT * FROM [note].[NoteAccess] 
			WHERE
			[NoteAccessID] = @NoteAccessID FOR XML AUTO) as data,
				getdate() 
	 END

	RETURN @Err
END

GO
