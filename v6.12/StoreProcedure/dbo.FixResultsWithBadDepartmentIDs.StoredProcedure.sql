SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[FixResultsWithBadDepartmentIDs]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [dbo].[FixResultsWithBadDepartmentIDs] AS' 
END
GO
ALTER proc [dbo].[FixResultsWithBadDepartmentIDs]
AS
BEGIN
DECLARE @DeactiveEntityStatusID INT = (SELECT EntityStatusID FROM EntityStatus WHERE CodeName='Deactive')
DECLARE @ActiveEntityStatusID INT = (SELECT EntityStatusID FROM EntityStatus WHERE CodeName='Active')
--move orphaned results up to school level
update r
 set departmentid = sub.parent_departmentid
from Result r
inner join
(
  --department is soft-deleted
  select r.resultid, hdp.departmentid as parent_departmentid
  from result r 
  inner join at.Survey s on s.SurveyID = r.SurveyID 
  join org.Department d on d.DepartmentID = r.DepartmentID and d.EntityStatusID = @DeactiveEntityStatusID and r.EntityStatusID = @ActiveEntityStatusID AND r.Deleted IS NULL
  inner join org.H_D hd on hd.DepartmentID = r.DepartmentID
  inner join org.H_D hdp on hdp.HDID = hd.ParentID
  
  union all
  
  --department is hard-deleted
  select r.resultid, hdp.departmentid as parent_departmentid
  from result r
  inner join org.[User] u on u.UserID = r.userid
  inner join org.H_D hd on hd.DepartmentID = u.DepartmentID
  inner join org.H_D hdp on hdp.HDID = hd.ParentID
  where r.departmentid not in (
  select departmentid from org.department
  ) and r.EntityStatusID = @ActiveEntityStatusID AND r.Deleted IS NULL
  
) as sub on sub.ResultID = r.resultid

END
-------------------------------------------------------------------------------------


GO
