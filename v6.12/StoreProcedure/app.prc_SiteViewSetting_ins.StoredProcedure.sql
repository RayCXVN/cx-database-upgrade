SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[app].[prc_SiteViewSetting_ins]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [app].[prc_SiteViewSetting_ins] AS' 
END
GO

ALTER PROCEDURE [app].[prc_SiteViewSetting_ins]
	@SiteViewSettingID int = null output,
	@SiteID int,
	@No int,
	@NumOfDecimals smallint,
	@TableIsSelectable bit,
	@TableIsDefaultSelected bit,
	@ChartIsSelectable bit,
	@ChartIsDefaultSelected bit,
	@AverageIsSelectable bit,
	@AverageIsDefaultSelected bit,
	@TrendIsSelectable bit,
	@TrendIsDefaultSelected bit,
	@FrequencyIsSelectable bit,
	@FrequencyIsDefaultSelected bit,
	@NCountIsSelectable bit,
	@NCountIsDefaultSelected bit,
	@StandardDeviationIsSelectable bit,
	@StandardDeviationIsDefaultSelected bit,
	@cUserid int,
    @Log smallint = 1,
    @DefaultMinResultDotted int =0,
    @DefaultMinQuestionCountDotted int =0
AS
BEGIN
	SET NOCOUNT ON;
	DECLARE @Err Int
    
    INSERT INTO [app].[SiteViewSetting]
           ([SiteID]
           ,[No]
           ,[NumOfDecimals]
           ,[TableIsSelectable]
           ,[TableIsDefaultSelected]
           ,[ChartIsSelectable]
           ,[ChartIsDefaultSelected]
           ,[AverageIsSelectable]
           ,[AverageIsDefaultSelected]
           ,[TrendIsSelectable]
           ,[TrendIsDefaultSelected]
           ,[FrequencyIsSelectable]
           ,[FrequencyIsDefaultSelected]
           ,[NCountIsSelectable]
           ,[NCountIsDefaultSelected]
           ,[StandardDeviationIsSelectable]
           ,[StandardDeviationIsDefaultSelected]
           ,[DefaultMinResultDotted]
           ,[DefaultMinQuestionCountDotted])
     VALUES
           (@SiteID
           ,@No
           ,@NumOfDecimals
           ,@TableIsSelectable
           ,@TableIsDefaultSelected
           ,@ChartIsSelectable
           ,@ChartIsDefaultSelected
           ,@AverageIsSelectable
           ,@AverageIsDefaultSelected
           ,@TrendIsSelectable
           ,@TrendIsDefaultSelected
           ,@FrequencyIsSelectable
           ,@FrequencyIsDefaultSelected
           ,@NCountIsSelectable
           ,@NCountIsDefaultSelected
           ,@StandardDeviationIsSelectable
           ,@StandardDeviationIsDefaultSelected
           ,@DefaultMinResultDotted
           ,@DefaultMinQuestionCountDotted)
           
    Set @Err = @@Error
    Set @SiteViewSettingID = scope_identity()
	IF @Log = 1 
	BEGIN 
		INSERT INTO [Log].[AuditLog] ( UserId, TableName, Type, Data, Created) 
		SELECT @cUserid,'SiteViewSetting',0,
		( SELECT * FROM [app].[SiteViewSetting]
			WHERE
			[SiteViewSettingID] = @SiteViewSettingID				 FOR XML AUTO) as data,
				getdate() 
	END
	
	RETURN @Err       
END

GO
