SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[prc_Archetype_upd]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [dbo].[prc_Archetype_upd] AS' 
END
GO
ALTER PROCEDURE [dbo].[prc_Archetype_upd]
(
	@ArchetypeID smallint ,
	@CodeName nvarchar(128),
	@TableTypeID smallint= null,
	@cUserid int,
	@Log smallint = 1
)
AS
BEGIN
	SET NOCOUNT ON
	DECLARE @Err Int

	UPDATE [dbo].[Archetype]
	SET
		[CodeName] = @CodeName,
		[TableTypeID] = @TableTypeID
	WHERE
		[ArchetypeID] = @ArchetypeID

	Set @Err = @@Error

	IF @Log = 1 
	BEGIN 
		INSERT INTO [Log].[AuditLog] ( UserId, TableName, Type, Data, Created) 
		SELECT @cUserid,'Archetype',1,
		( SELECT * FROM [dbo].[Archetype]
			WHERE
			[ArchetypeID] = @ArchetypeID				 FOR XML AUTO) as data,
				getdate() 
	 END

	RETURN @Err
END


GO
