SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[job].[prc_Job_ins]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [job].[prc_Job_ins] AS' 
END
GO

ALTER PROCEDURE [job].[prc_Job_ins]
(
	@JobID int = null output,
	@JobTypeID smallint,
	@JobStatusID smallint,
	@OwnerID int,
	@UserID int,
	@Name nvarchar(256),
	@Priority smallint,
	@Option SMALLINT=NULL,
	@StartDate datetime,
	@EndDate datetime = NULL,
	@Description nvarchar(max),
	@cUserid int,
	@Log smallint = 1
)
AS
BEGIN
	SET NOCOUNT ON
	DECLARE @Err Int

	INSERT INTO [job].[Job]
	(
		[JobTypeID],
		[JobStatusID],
		[OwnerID],
		[UserID],
		[Name],
		[Priority],
		[Option],
		[StartDate],
		[EndDate],
		[Description]
	)
	VALUES
	(
		@JobTypeID,
		@JobStatusID,
		@OwnerID,
		@UserID,
		@Name,
		@Priority,
		@Option,
		@StartDate,
		@EndDate,
		@Description
	)

	Set @Err = @@Error
	Set @JobID = scope_identity()

	IF @Log = 1 
	BEGIN 
		INSERT INTO [Log].[AuditLog] ( UserId, TableName, Type, Data, Created) 
		SELECT @cUserid,'Job',0,
		( SELECT * FROM [job].[Job] 
			WHERE
			[JobID] = @JobID				 FOR XML AUTO) as data,
				getdate() 
	 END

	RETURN @Err
END


GO
