SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[mea].[prc_Measure_Category_GetByMeasureId]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [mea].[prc_Measure_Category_GetByMeasureId] AS' 
END
GO
ALTER PROCEDURE [mea].[prc_Measure_Category_GetByMeasureId]
(
	@MeasureId int
)
AS
BEGIN
	SELECT CategoryId from mea.Measure_Category where MeasureId = @MeasureId
END

GO
