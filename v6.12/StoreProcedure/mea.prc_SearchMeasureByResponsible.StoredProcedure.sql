SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[mea].[prc_SearchMeasureByResponsible]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [mea].[prc_SearchMeasureByResponsible] AS' 
END
GO
/*
    Steve November 14 2016 - Search activities in Competence No - Add more param @MeasureTypeIDs
    Steve November 22 2016 - Search activities in Competence No - Correct 'AND us.EntityStatusID = @ActiveEntityStatusID'
    Ray - 2017-02-09 - Remove cross database reference for Azure compatible
*/
ALTER PROCEDURE [mea].[prc_SearchMeasureByResponsible]
(
    @ListUserID		nvarchar(max),
    @SearchString	nvarchar(max),
    @CurrentUserID	int,
    @RowCount		int output,
    @IsSubject		BIT =0,
    @IsResponsible	BIT=0,
    @PageIndex		INT=1,
    @PageSize		INT =10,
    @MeasureTypeIDs nvarchar(max) = '1'
)
AS
BEGIN
    DECLARE @ActiveEntityStatusID INT = (SELECT EntityStatusID FROM EntityStatus WHERE CodeName='Active')
    DECLARE @TableMeasureType TABLE(MeasureTypeID INT)
    INSERT INTO @TableMeasureType(MeasureTypeID) SELECT Value FROM dbo.funcListToTableInt(@MeasureTypeIDs, ',')
    -- #SearchResult: filter by Responsible
	    ;WITH RecQry AS
				    (
					    SELECT m.MeasureId, m.CopyFrom 
						 FROM mea.Measure m JOIN @TableMeasureType mt ON m.MeasureTypeId = mt.MeasureTypeID
						WHERE( m.Responsible IN (SELECT value From dbo.funcListToTableInt(@ListUserID,',')))  AND Active =1 --AND Deleted =0
					    UNION ALL
					    SELECT a.MeasureId,a.CopyFrom
						 FROM mea.Measure a INNER JOIN RecQry b
						    ON a.MeasureId = b.CopyFrom
				    )
    Select DISTINCT ms.MeasureId,ms.MeasureTypeId, ms.ParentId,ms.CreatedBy,ms.CreatedDate,ms.ChangedBy, ms.ChangedDate, ms.Responsible, ms.StatusId, ms.[Private], ms.[Subject], ms.[Description],ms.[Started], ms.[DueDate], ms.[Completed], ms.CopyFrom, ms.Active, ms.Deleted, us.FirstName,us.LastName,ROW_NUMBER() OVER (ORDER BY ms.MeasureId DESC) AS Row
    INTO #SearchResult
    FROM mea.Measure ms
    LEFT JOIN org.[User] us ON us.UserID = ms.Responsible
    WHERE  EXISTS (SELECT 1 FROM RecQry WHERE MeasureId= ms.MeasureId) AND ms.Deleted =0;

    -- #Result1: filter #SearchResult by SearchString
    Select DISTINCT s.MeasureId,s.MeasureTypeId, s.ParentId,s.CreatedBy,s.CreatedDate,s.ChangedBy, s.ChangedDate, s.Responsible, s.StatusId, s.[Private], s.[Subject], s.[Description],s.[Started], s.[DueDate], s.[Completed], s.CopyFrom, s.Active, s.Deleted, us.FirstName,us.LastName,ROW_NUMBER() OVER (ORDER BY s.MeasureId DESC) AS Row
    INTO #Result1 
    FROM #SearchResult s
    LEFT JOIN org.[User] us ON us.UserID = s.Responsible
    WHERE (
    ((@IsSubject=1 AND @IsResponsible=0) AND s.[Subject] LIKE '%'+@SearchString+'%')
	    OR
    ((@IsSubject=0 AND @IsResponsible=1) AND (us.LastName LIKE '%'+@SearchString+'%' OR us.FirstName LIKE '%'+@SearchString+'%') AND us.EntityStatusID = @ActiveEntityStatusID AND us.Deleted IS NULL)
	    OR
    ((@IsSubject=0 AND @IsResponsible=0) AND (us.LastName LIKE '%'+@SearchString+'%' OR us.FirstName LIKE '%'+@SearchString+'%' OR s.[Subject] LIKE '%'+@SearchString+'%') AND us.EntityStatusID = @ActiveEntityStatusID AND us.Deleted IS NULL)
    );

    -- #Result3: Child of #SearchResult
    WITH RecQry2 AS
				    (
					    SELECT m1.MeasureId, m1.ParentId
						 FROM mea.Measure m1 JOIN @TableMeasureType mt ON m1.MeasureTypeId = mt.MeasureTypeID
					 WHERE m1.MeasureId in (SELECT MeasureId FROM #SearchResult)
        				    UNION ALL
					    SELECT m.MeasureId, m.ParentId
						 FROM mea.Measure m INNER JOIN RecQry2 b
						    ON m.ParentID = b.MeasureID
				    )

    Select DISTINCT ms.MeasureId,ms.MeasureTypeId, ms.ParentId,ms.CreatedBy,ms.CreatedDate,ms.ChangedBy, ms.ChangedDate, ms.Responsible, ms.StatusId, ms.[Private], ms.[Subject], ms.[Description],ms.[Started], ms.[DueDate], ms.[Completed], ms.CopyFrom, ms.Active, ms.Deleted, us.FirstName,us.LastName
    INTO #Result3
    FROM mea.Measure ms
    LEFT JOIN org.[User] us ON us.UserID = ms.Responsible
    WHERE EXISTS (SELECT 1 FROM RecQry2 WHERE MeasureId= ms.MeasureId) AND ms.Deleted =0 AND ms.Active = 1 AND ms.Responsible IS NOT NULL
    AND 
    (
    ((@IsSubject=1 AND @IsResponsible=0) AND ms.[Subject] LIKE '%'+@SearchString+'%')
	    OR
    ((@IsSubject=0 AND @IsResponsible=1) AND (us.LastName LIKE '%'+@SearchString+'%' OR us.FirstName LIKE '%'+@SearchString+'%') AND us.EntityStatusID = @ActiveEntityStatusID AND us.Deleted IS NULL)
	    OR
    ((@IsSubject=0 AND @IsResponsible=0) AND (us.LastName LIKE '%'+@SearchString+'%' OR us.FirstName LIKE '%'+@SearchString+'%' OR ms.[Subject] LIKE '%'+@SearchString+'%') AND us.EntityStatusID = @ActiveEntityStatusID AND us.Deleted IS NULL)
    );

    -- FINAL RESULT
    SELECT v.* INTO #FinalResult FROM
    (SELECT MeasureId,MeasureTypeId, ParentId,CreatedBy,CreatedDate,ChangedBy, ChangedDate, Responsible, StatusId, [Private], [Subject], [Description],[Started], [DueDate], [Completed], CopyFrom, Active, Deleted,FirstName,LastName FROM #Result1
    WHERE MeasureId NOT IN ( SELECT MeasureId FROM #Result1 WHERE Responsible IS NULL AND CopyFrom IS NULL AND CreatedBy<>@CurrentUserID)
    UNION
    SELECT MeasureId,MeasureTypeId, ParentId,CreatedBy,CreatedDate,ChangedBy, ChangedDate, Responsible, StatusId, [Private], [Subject], [Description],[Started], [DueDate], [Completed], CopyFrom, Active, Deleted,FirstName,LastName FROM #Result3
    ) v

    SET @RowCount = (SELECT Count(MeasureId) FROM #FinalResult)

    --SELECT r.* FROM
    --(SELECT row_number() OVER (ORDER BY t.[Subject]) AS RowNum, t.* FROM #FinalResult t) r
    --WHERE r.RowNum between (@PageIndex - 1) * @PageSize + 1 and @PageIndex*@PageSize
    SELECT r.* FROM
    (SELECT row_number() OVER (ORDER BY t.[CreatedDate] DESC) AS RowNum, t.* FROM #FinalResult t) r
    WHERE r.RowNum between (@PageIndex - 1) * @PageSize + 1 and @PageIndex*@PageSize

    DROP TABLE #SearchResult
    DROP TABLE #Result1
    DROP TABLE #Result3
    DROP TABLE #FinalResult
END

GO
