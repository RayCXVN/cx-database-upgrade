SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[at].[prc_LevelGroup_upd]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [at].[prc_LevelGroup_upd] AS' 
END
GO

ALTER PROCEDURE [at].[prc_LevelGroup_upd]
(
	@LevelGroupID int,
	@ActivityID int,
	@Tag nvarchar(128),
	@No smallint,
	@CustomerID int = null,
	@DepartmentID int = null,
	@RoleID int = null,
	@cUserid int,
	@Log smallint = 1
)
AS
BEGIN
	SET NOCOUNT ON
	DECLARE @Err Int

	UPDATE [at].[LevelGroup]
	SET
		[ActivityID] = @ActivityID,
		[Tag] = @Tag,
		[No] = @No,
		[CustomerID] = @CustomerID,
		[DepartmentID] = @DepartmentID,
		[RoleID] = @RoleID
	WHERE
		[LevelGroupID] = @LevelGroupID

	IF @Log = 1 
	BEGIN 
		INSERT INTO [Log].[AuditLog] ( UserId, TableName, Type, Data, Created) 
		SELECT @cUserid,'LevelGroup',1,
		( SELECT * FROM [at].[LevelGroup] 
			WHERE
			[LevelGroupID] = @LevelGroupID			 FOR XML AUTO) as data,
			getdate()
	END

	Set @Err = @@Error

	RETURN @Err
END





GO
