SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[proc].[prc_ProcessSingle_get]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [proc].[prc_ProcessSingle_get] AS' 
END
GO

ALTER PROCEDURE [proc].[prc_ProcessSingle_get]
(
    @ProcessID int

)
AS
BEGIN
	SET NOCOUNT ON
	DECLARE @Err Int

	SELECT
	[ProcessID],
	[ProcessGroupID],
	[DepartmentID],
	[Active],
	[TargetValue],
	[UserID],
	ISNULL([CustomerID], 0) AS 'CustomerID',
	ISNULL([StartDate], 0) AS 'StartDate',
	ISNULL([DueDate], 0) AS 'DueDate',
	[Responsible],
	[URL],
	[No],
	ISNULL([PeriodID],0) as 'PeriodID',
	[Created],
	isnull(ProcessTypeID,0) As 'ProcessTypeID' ,
	isPublic 
	FROM [proc].[Process] P
	WHERE
	 [ProcessID] = @ProcessID
	
	

	Set @Err = @@Error

	RETURN @Err
END


GO
