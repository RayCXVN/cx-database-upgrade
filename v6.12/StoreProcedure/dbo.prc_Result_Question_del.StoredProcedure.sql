SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[prc_Result_Question_del]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [dbo].[prc_Result_Question_del] AS' 
END
GO
ALTER Proc [dbo].[prc_Result_Question_del]

(

@ResultID as bigint,

@QuestionList as varchar(1000)

)

as

begin

set nocount on

Exec ('delete from answer where Resultid = '+ @ResultID +' and QuestionID IN ('+ @QuestionList +')')

end


GO
