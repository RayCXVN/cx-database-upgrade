SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[log].[prc_CubeLog_ins]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [log].[prc_CubeLog_ins] AS' 
END
GO

ALTER PROCEDURE [log].[prc_CubeLog_ins]
(
	@CubLogID int = null output,
	@SurveyID int,
	@Type smallint,
	@Status smallint,
	@ReportServer nvarchar(64)='',
	@ReportDB nvarchar(64)='',
	@Description nvarchar(max)
)
AS
BEGIN
	SET NOCOUNT ON
	DECLARE @Err Int

	INSERT INTO [log].[CubeLog]
	(
		[SurveyID],
		[Type],
		[Status],
		[Description],
		[ReportServer],
		[ReportDB]
	)
	VALUES
	(
		@SurveyID,
		@Type,
		@Status,
		@Description,
		@ReportServer,
		@ReportDB
	)

	Set @Err = @@Error

	RETURN @Err
END


GO
