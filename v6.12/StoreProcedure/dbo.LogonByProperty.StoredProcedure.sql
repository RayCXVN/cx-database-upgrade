SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[LogonByProperty]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [dbo].[LogonByProperty] AS' 
END
GO
ALTER PROCEDURE [dbo].[LogonByProperty]  
(  
 @OwnerID  int ,  
 @PropertyID  int ,  
 @PropertyValue nvarchar(1000), --nvarchar(MAX) /* Begrenset denne til 1000 tegn */  
 @UserID   int  OUTPUT,  
 @DepartmentID int  OUTPUT,  
 @ErrNo   int  OUTPUT  
)  
AS  
BEGIN  
DECLARE @ret as integer, @EntityStatusID INT = 0  

SELECT @EntityStatusID = EntityStatusID FROM dbo.EntityStatus WHERE CodeName = N'Active'

SELECT DISTINCT   
 @DepartmentID = u.DepartmentID,  
 @UserID = u.UserID,  
 @OwnerId = u.OwnerId  
FROM    
 [org].[User] u  
 INNER JOIN [prop].[PropValue] pv ON pv.ItemID = u.UserID AND pv.PropertyID = @PropertyID  
 INNER JOIN [prop].[Prop]p ON p.PropertyID = pv.PropertyID  
 INNER JOIN [prop].[PropPage]ppage ON ppage.PropPageID = p.PropPageID AND ppage.TableTypeID = 13 /* org.User */  
WHERE  
 pv.Value = @PropertyValue  
    AND u.EntityStatusID = @EntityStatusID /* Usikker på hva dette er, antakelig at brukeren er slettet? hilsen Geir */  
    AND u.Deleted IS NULL
	AND u.[Ownerid] = @OwnerId    
      
SET @ret = @@ROWCOUNT  
IF @ret < 1   
BEGIN  
 SET @ErrNo = 1 /* Ingen brukere ble funnet */  
END  
ELSE  
BEGIN   
 IF @ret > 1   
 BEGIN  
  SET @ErrNo = 2 /* Flere brukere med samme propertyvalue ble funnet! */  
 END  
 ELSE  
 BEGIN   
  SET @ErrNo = 0  
 END  
END  
END  


GO
