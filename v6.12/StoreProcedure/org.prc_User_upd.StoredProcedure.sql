SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[org].[prc_User_upd]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [org].[prc_User_upd] AS' 
END
GO
/*  2016-11-02 Steve: Set default CurrentDate for @LastUpdated, @LastSynchronized
	2017-07-27 Steve: Change the length of column ExtID in all related tables from xAPI format
*/
ALTER PROCEDURE [org].[prc_User_upd]
(  
 @UserID INT,  
 @Ownerid INT,  
 @DepartmentID INT,  
 @LanguageID INT,  
 @RoleID INT = null,  
 @UserName NVARCHAR(128),  
 @Password NVARCHAR(64),  
 @LastName NVARCHAR(64),  
 @FirstName NVARCHAR(64),  
 @Email NVARCHAR(256),  
 @Mobile NVARCHAR(16),  
 @ExtID NVARCHAR(256),  
 @SSN NVARCHAR(64),  
 @Tag NVARCHAR(128),  
 @Locked SMALLINT,  
 @ChangePassword bit,  
 @HashPassword nvarchar(128)='',  
 @SaltPassword nvarchar(64)='',  
 @OneTimePassword nvarchar(64)='',  
 @OTPExpireTime smalldatetime='',  
 @cUserid INT,  
 @Log SMALLINT = 1,  
 @CountryCode int = 0,  
 @Gender smallint = 0,
 @DateOfBirth date='',
 @ForceUserLoginAgain bit =0,
 @LastUpdated datetime2=NULL,
 @LastUpdatedBy int = NULL,
 @LastSynchronized datetime2=NULL,
 @ArchetypeID int = NULL,
 @CustomerID int = NULL,
 @Deleted datetime2 = NULL,
 @EntityStatusID int = NULL,
 @EntityStatusReasonID int = NULL  
)  
AS  
BEGIN  
 SET NOCOUNT ON  
 DECLARE @Err INT  
 DECLARE @OldUsername VARCHAR(128)  
 DECLARE @Count INT   
 DECLARE @DBID INT;  
 SET @DBID = DB_ID();  

 IF @LastUpdated IS NULL
    SET @LastUpdated = GETDATE()

 IF @LastSynchronized IS NULL
    SET @LastSynchronized = GETDATE()

 DECLARE @DBNAME NVARCHAR(128);  

    SET @Count = 0  
 SELECT @OldUsername = username FROM [org].[USER] WHERE userid = @UserID  


 IF @OldUsername <> @UserName  
 BEGIN  
  SELECT @Count = COUNT(*) FROM [org].[USER] WHERE UserName = @Username and @userid <> userid  
 END  

 IF @Count = 0  
 BEGIN  
  UPDATE [org].[USER]  
  SET  
	[Ownerid] = @Ownerid,  
	[DepartmentID] = @DepartmentID,  
	[LanguageID] = @LanguageID,  
	[RoleID] = @RoleID,  
	[UserName] = @UserName,  
	[Password] = @Password,  
	[LastName] = @LastName,  
	[FirstName] = @FirstName,  
	[Email] = @Email,  
	[Mobile] = @Mobile,  
	[ExtID] = @ExtID,  
	[SSN] = @SSN,  
	[Tag] = @Tag,  
	[Locked] = @Locked,  
	[ChangePassword] = @ChangePassword,  
	[HashPassword] = @HashPassword,  
	[SaltPassword] = @SaltPassword,  
	[OneTimePassword] = @OneTimePassword,  
	[OTPExpireTime] = @OTPExpireTime,  
	[CountryCode] = @CountryCode,  
	[Gender] = @Gender,
	[DateOfBirth] = @DateOfBirth,
	[ForceUserLoginAgain] = @ForceUserLoginAgain,
	[LastUpdated] = @LastUpdated,
	[LastUpdatedBy] = @LastUpdatedBy,
	[LastSynchronized] = @LastSynchronized,
	[ArchetypeID]= @ArchetypeID,  
	[CustomerID] = @CustomerID ,
	[Deleted]	=	@Deleted ,
	[EntityStatusID] = @EntityStatusID ,
	[EntityStatusReasonID] = @EntityStatusReasonID
  WHERE  
	[UserID] = @UserID  

  IF @Log = 1   
  BEGIN   
   INSERT INTO [LOG].[AuditLog] ( UserId, TableName, TYPE, DATA, Created)   
   SELECT @cUserid,'User',1,  
   ( SELECT * FROM [org].[USER]   
    WHERE  
    [UserID] = @UserID    FOR XML AUTO) AS DATA,  
    GETDATE()  
  END  
 END  
 ELSE  
 BEGIN  
   RAISERROR  
   (N'Username taken',  
   18, -- Severity.  
   1, -- State.  
   @DBID, -- First substitution argument.  
   @DBNAME); -- Second substituti  
 END  

 SET @Err = @@ERROR  

 RETURN @Err  
END


GO
