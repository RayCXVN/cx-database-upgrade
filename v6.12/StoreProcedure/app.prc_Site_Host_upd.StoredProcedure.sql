SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[app].[prc_Site_Host_upd]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [app].[prc_Site_Host_upd] AS' 
END
GO
ALTER PROCEDURE [app].[prc_Site_Host_upd]
(	@SiteHostID int = null output,  
    @SiteID		INT,
	@HostName	NVARCHAR(256), 
	@Active		BIT,
	@cUserid int,  
	@Log smallint = 1
)
AS
BEGIN
	SET NOCOUNT ON;  
	DECLARE @Err Int 
	
	UPDATE app.Site_Host
	SET HostName = @HostName,
	    Active = @Active
	WHERE SiteID = @SiteID
	AND   SiteHostID = @SiteHostID
  
	SET @Err = @@Error  
	SET @SiteHostID = scope_identity() 
	 
	IF @Log = 1
	BEGIN
	  INSERT INTO [Log].[AuditLog] ( UserId, TableName, Type, Data, Created)   
	  SELECT @cUserid,'Site_Host',1,  
	  ( SELECT * FROM [app].Site_Host  
	   WHERE	   
	   SiteHostID = @SiteHostID     FOR XML AUTO) as data,  
		getdate()
	END
	RETURN @Err  
END

GO
