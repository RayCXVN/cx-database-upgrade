SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[note].[prc_NoteReadLog_get]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [note].[prc_NoteReadLog_get] AS' 
END
GO
ALTER PROCEDURE [note].[prc_NoteReadLog_get]
(
	@NoteID int
)
AS
BEGIN
	SET NOCOUNT ON
	DECLARE @Err Int

	SELECT 
		[NoteID],
		[UserID],
		[DateRead]
	FROM [note].[NoteReadLog]
	WHERE [NoteID] = @NoteID
		
	Set @Err = @@Error
	
	RETURN @Err
END

GO
