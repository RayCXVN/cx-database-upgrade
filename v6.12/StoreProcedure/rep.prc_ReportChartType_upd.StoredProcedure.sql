SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[rep].[prc_ReportChartType_upd]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [rep].[prc_ReportChartType_upd] AS' 
END
GO

ALTER PROCEDURE [rep].[prc_ReportChartType_upd]
(
	@ReportChartTypeID int,
	@Type smallint,
	@No smallint,
	@DundasID smallint,
	@cUserid int,
	@Log smallint = 1
)
AS
BEGIN
	SET NOCOUNT ON
	DECLARE @Err Int

	UPDATE [rep].[ReportChartType]
	SET
		[ReportChartTypeID] = @ReportChartTypeID,
		[Type] = @Type,
		[No] = @No,
		[DundasID] = @DundasID
	WHERE
		[ReportChartTypeID] = @ReportChartTypeID

	IF @Log = 1 
	BEGIN 
		INSERT INTO [Log].[AuditLog] ( UserId, TableName, Type, Data, Created) 
		SELECT @cUserid,'ReportChartType',1,
		( SELECT * FROM [rep].[ReportChartType] 
			WHERE
			[ReportChartTypeID] = @ReportChartTypeID			 FOR XML AUTO) as data,
			getdate()
	END

	Set @Err = @@Error

	RETURN @Err
END


GO
