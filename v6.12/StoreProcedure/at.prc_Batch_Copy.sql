SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[at].[prc_Batch_Copy]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [at].[prc_Batch_Copy] AS' 
END
GO
ALTER PROCEDURE [at].[prc_Batch_Copy]
(   @BatchIDList            varchar(max) = '',
    @NewSurveyIDList        varchar(max) = '',
    @AllowDuplicatedBatch   bit = 0
) AS
BEGIN
    DECLARE @BatchTable TABLE ([BatchID] int);
    DECLARE @SurveyTable TABLE ([SurveyID] int, [StartDate] datetime, [EndDate] datetime);

    INSERT INTO @BatchTable ([BatchID]) SELECT [Value] FROM [dbo].[funcListToTableInt] (@BatchIDList, ',');
    INSERT INTO @SurveyTable ([SurveyID], [StartDate], [EndDate])
    SELECT s.[SurveyID], s.[StartDate], s.[EndDate]
    FROM [dbo].[funcListToTableInt] (@NewSurveyIDList, ',') AS fn
    JOIN [at].[Survey] s ON s.[SurveyID] = fn.[Value] AND s.[ExtId] <> 'Global';

    INSERT INTO [at].[Batch] ([SurveyID], [UserID], [DepartmentID], [Name], [No], [StartDate], [EndDate], [Status], [MailStatus], [Created], [ExtID])
    SELECT st.[SurveyID], cb.[UserID], cb.[DepartmentID], cb.[Name], cb.[No],
           DATEADD(DAY, DATEDIFF(DAY, s.[StartDate], cb.[StartDate]), st.[StartDate]) AS [StartDate],
           DATEADD(DAY, DATEDIFF(DAY, s.[EndDate], cb.[EndDate]), st.[EndDate]) AS [EndDate],
           cb.[Status], cb.[MailStatus], GETDATE() AS [Created], cb.[ExtID]
    FROM [at].[Batch] cb
    JOIN @BatchTable bt ON bt.[BatchID] = cb.[BatchID]
    JOIN [at].[Survey] s ON s.[SurveyID] = cb.[SurveyID]
    CROSS JOIN @SurveyTable st
    WHERE (@AllowDuplicatedBatch = 1
          OR (@AllowDuplicatedBatch = 0 AND NOT EXISTS (SELECT 1 FROM [at].[Batch] nb WHERE nb.[SurveyID] = st.[SurveyID] AND nb.[DepartmentID] = cb.[DepartmentID] AND nb.[Status] = 2))
          );
END
GO