SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[prc_CheckDBVersion_QDB]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [dbo].[prc_CheckDBVersion_QDB] AS' 
END
GO

ALTER PROCEDURE [dbo].[prc_CheckDBVersion_QDB] 
AS
BEGIN

DECLARE @ReportServer NVARCHAR(MAX), @ReportDB NVARCHAR(MAX)
SELECT DISTINCT TOP 1 @ReportServer = ReportServer, @ReportDB = ReportDB FROM at.Survey

DECLARE @DatabaseProperties TABLE (Name VARCHAR(MAX), Value VARCHAR(MAX) )

INSERT INTO @DatabaseProperties ( Name, Value )
SELECT 'QDB after deployed' , CONVERT(VARCHAR(MAX),DB_NAME())
UNION ALL
SELECT [Name], CONVERT(VARCHAR(MAX),[Value]) AS Value FROM fn_listextendedproperty(N'cxStudio Database Version', NULL, NULL, NULL, NULL, NULL, NULL)
UNION ALL
SELECT [Name], CONVERT(VARCHAR(MAX),[Value]) AS Value FROM fn_listextendedproperty(N'Concept Name', NULL, NULL, NULL, NULL, NULL, NULL)
UNION ALL
SELECT [Name], CONVERT(VARCHAR(MAX),[Value]) AS Value FROM fn_listextendedproperty(N'Script Name', NULL, NULL, NULL, NULL, NULL, NULL)

INSERT INTO @DatabaseProperties ( Name, Value ) 
VALUES ('--------------------------------------------------', '---------------------------------------------------------------------')

DECLARE @sqlCommand NVARCHAR(MAX)
SET @sqlCommand = '
SELECT ''RDB after deployed'' , ''' + @ReportDB + '''
UNION ALL
SELECT [Name], CONVERT(VARCHAR(MAX),[Value]) AS Value FROM [' +  @ReportDB + '].sys.fn_listextendedproperty(N''cxStudio Database Version'', NULL, NULL, NULL, NULL, NULL, NULL)
UNION ALL
SELECT [Name], CONVERT(VARCHAR(MAX),[Value]) AS Value FROM [' +  @ReportDB + '].sys.fn_listextendedproperty(N''Concept Name'', NULL, NULL, NULL, NULL, NULL, NULL)
UNION ALL
SELECT [Name], CONVERT(VARCHAR(MAX),[Value]) AS Value FROM [' +  @ReportDB + '].sys.fn_listextendedproperty(N''Script Name'', NULL, NULL, NULL, NULL, NULL, NULL)
' 

INSERT INTO @DatabaseProperties ( Name, Value )
EXEC(@sqlCommand)

SELECT * FROM @DatabaseProperties

END


GO
