SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[prc_Result2_get]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [dbo].[prc_Result2_get] AS' 
END
GO
/* ------------------------------------------------------------
   PROCEDURE:    prc_Result2_get

   Description:  Selects records from the table 'prc_Result_get'

   AUTHOR:       LockwoodTech 11.07.2006 11:47:11
   ------------------------------------------------------------
   2016-10-10 Ray: Remove column Status from org.User, org.Department, RDB.dbo.Result
*/
ALTER PROCEDURE [dbo].[prc_Result2_get]
(
	@Resultkey	varchar(128)
)
AS
BEGIN
	SET NOCOUNT ON
	DECLARE @Err Int
    DECLARE @ActiveEntityStatusID INT = (SELECT EntityStatusID FROM EntityStatus WHERE CodeName='Active')

	SELECT
	[ResultID],
	[StartDate],
	[EndDate],
	[RoleID],
	[UserID]=0,
	ISNULL([UserGroupID], 0) [UserGroupID],
	[SurveyID],
	[BatchID],
	[LanguageID],
	[PageNo],
	[DepartmentID],
	[Anonymous],
	[ShowBack],
	[Email],
	[LastUpdated],
	[LastUpdatedBy],
	[StatusTypeID],
    EntityStatusID,
    EntityStatusReasonID, 
    Deleted
	FROM [Result]
	WHERE Resultkey = @Resultkey  AND EndDate IS NULL AND EntityStatusID = @ActiveEntityStatusID AND Deleted IS NULL
	
	SET @Err = @@Error

	RETURN @Err
END


GO
