SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[rep].[prc_LT_ReportPart_upd]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [rep].[prc_LT_ReportPart_upd] AS' 
END
GO


ALTER PROCEDURE [rep].[prc_LT_ReportPart_upd]
(
	@LanguageID int,
	@ReportPartID int,
	@Text nvarchar(max),
	@Title nvarchar(256) = '',
	@cUserid int,
	@Log smallint = 1
)
AS
BEGIN
	SET NOCOUNT ON
	DECLARE @Err Int

	UPDATE [rep].[LT_ReportPart]
	SET
		[LanguageID] = @LanguageID,
		[ReportPartID] = @ReportPartID,
		[Text] = @Text,
		[Title] = @Title
	WHERE
		[LanguageID] = @LanguageID AND
		[ReportPartID] = @ReportPartID

	IF @Log = 1 
	BEGIN 
		INSERT INTO [Log].[AuditLog] ( UserId, TableName, Type, Data, Created) 
		SELECT @cUserid,'LT_ReportPart',1,
		( SELECT * FROM [rep].[LT_ReportPart] 
			WHERE
			[LanguageID] = @LanguageID AND
			[ReportPartID] = @ReportPartID			 FOR XML AUTO) as data,
			getdate()
	END

	Set @Err = @@Error

	RETURN @Err
END



GO
