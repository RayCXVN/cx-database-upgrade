SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[imp].[prc_ImportFile_DataErrorget]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [imp].[prc_ImportFile_DataErrorget] AS' 
END
GO
ALTER PROCEDURE [imp].[prc_ImportFile_DataErrorget]
(
	@ImportFileID	int
)
As
BEGIN
	SET NOCOUNT ON
	DECLARE @Err Int

	SELECT DataError
	FROM [imp].Importfiles
	WHERE [ImportFileID] = @ImportFileID

	Set @Err = @@Error

	RETURN @Err
End

GO
