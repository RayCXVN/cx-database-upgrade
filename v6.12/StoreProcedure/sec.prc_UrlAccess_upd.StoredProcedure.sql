SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[sec].[prc_UrlAccess_upd]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [sec].[prc_UrlAccess_upd] AS' 
END
GO
ALTER PROCEDURE [sec].[prc_UrlAccess_upd]    
(  
   @UrlAccessID int,  
   @SiteId  Integer,
   @Controller  nvarchar(128),
   @Action  nvarchar(128),
   @MenuItemId AS INTEGER,
   @cUserid int,  
   @Log smallint = 1  
)  
AS  
BEGIN  
 SET NOCOUNT ON  
 DECLARE @Err Int  
  
 UPDATE [sec].[UrlAccess]
 SET  
	[SiteId] = @SiteId
   ,[Controller] = @Controller
   ,[Action] = @Action
   ,[MenuItemId] = @MenuItemId
 WHERE  
  [UrlAccessID] = @UrlAccessID  
  
 IF @Log = 1   
 BEGIN   
  INSERT INTO [Log].[AuditLog] ( UserId, TableName, Type, Data, Created)   
  SELECT @cUserid,'UrlAccess',1,  
  ( SELECT * FROM [sec].[UrlAccess]   
   WHERE  
   [UrlAccessID] = @UrlAccessID   FOR XML AUTO) as data,  
   getdate()  
 END  
  
 Set @Err = @@Error  
  
 RETURN @Err  
END


GO
