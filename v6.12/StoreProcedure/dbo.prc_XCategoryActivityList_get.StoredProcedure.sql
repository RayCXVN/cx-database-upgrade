SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[prc_XCategoryActivityList_get]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [dbo].[prc_XCategoryActivityList_get] AS' 
END
GO
--Get XCategory Activity List    
-- exec [dbo].[prc_XCategoryActivityList_get] 2,'2,3'
ALTER PROCEDURE [dbo].[prc_XCategoryActivityList_get]
	@xCategoryId int,	
	@xCategoryIds varchar(MAX)
AS
BEGIN 	
	SELECT * FROM at.XC_A
	WHERE CHARINDEX(',' + CONVERT(varchar(10),XCID) + ',',',' + @xCategoryIds + ',') <> 0
		AND ActivityID in (SELECT ActivityID FROM at.XC_A WHERE XCID = @xCategoryId)		
END
/********************************/


GO
