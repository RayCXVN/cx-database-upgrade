SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[prc_Result_get]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [dbo].[prc_Result_get] AS' 
END
GO
/*  2016-10-10 Ray: Remove column Status from org.User, org.Department, RDB.dbo.Result
*/
ALTER PROCEDURE [dbo].[prc_Result_get]
(
	@BatchID		int = NULL,
	@UserID			int = NULL,
	@UserGroupID	int = NULL
)
As
BEGIN
	SET NOCOUNT ON
	DECLARE @Err Int
    DECLARE @ActiveEntityStatusID INT = (SELECT EntityStatusID FROM EntityStatus WHERE CodeName='Active')

	Select
	[ResultID],
	[StartDate],
	[EndDate],
	[RoleID],
	ISNULL([UserID], 0) [UserID],
	ISNULL([UserGroupID], 0) [UserGroupID],
	[SurveyID],
	[BatchID],
	[LanguageID],
	[PageNo],
	[DepartmentID],
	[Anonymous],
	[ShowBack],
	[Email],
	[chk],
	[ResultKey],
	[Created],
	[LastUpdated],
	[LastUpdatedBy],
	ISNULL([StatusTypeID],0) [StatusTypeID],
	[ValidFrom],  
	[ValidTo], 
	[DueDate], 
	[ParentResultID],
	[ParentResultSurveyID],
	[CustomerID],
	[Deleted],
	[EntityStatusID],
	[EntityStatusReasonID]
	FROM [Result]
	WHERE 
	EntityStatusID = @ActiveEntityStatusID AND Deleted IS NULL AND
    (
		([BatchID] = @BatchID AND @UserID IS NULL AND @UserGroupID IS NULL) 
	    OR  ([UserID] = @UserID AND @BatchID Is NULL AND @UserGroupID IS NULL)
	    OR  ([UserGroupID] = @UserGroupID AND @BatchID Is NULL AND @UserID IS NULL)	
    )
	SET @Err = @@Error

	RETURN @Err
END


GO
