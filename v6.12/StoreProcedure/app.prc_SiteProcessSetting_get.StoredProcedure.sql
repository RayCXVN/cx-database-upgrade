SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[app].[prc_SiteProcessSetting_get]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [app].[prc_SiteProcessSetting_get] AS' 
END
GO
ALTER PROCEDURE [app].[prc_SiteProcessSetting_get]      
 @SiteID int      
AS      
BEGIN      
 SET NOCOUNT ON;      
 DECLARE @Err Int          
 SELECT       
	  [SiteProcessSettingID]
      ,[SiteID]
      ,[No]
      ,[UseLocalProcessGroups]
      ,[AllCanAddAnswers]
      ,[UsePeriod]
      ,[CanChangeuser]
      ,[ShowOnlyAnswersBlowOnAdminPage]
      ,[CanAddProcessGroup]
      ,[CanCreatorEditAnswer]
      ,[ShowEditButtonOnProcessAnswer]
 FROM             
  [SiteProcessSetting]      
 WHERE        
  [SiteProcessSetting].SiteID = @SiteID      
   ORDER BY [No]  
 Set @Err = @@Error      
      
 RETURN @Err      
END 

GO
