SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[proc].[prc_ProcessAnswerLevel_ins]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [proc].[prc_ProcessAnswerLevel_ins] AS' 
END
GO

ALTER PROCEDURE [proc].[prc_ProcessAnswerLevel_ins]
(
	@ProcessAnswerID int,
	@ProcessLevelID int,
	@Description nvarchar(max),
	@cUserid int,
	@Log smallint = 1
)
AS
BEGIN
	SET NOCOUNT ON
	DECLARE @Err Int

	INSERT INTO [proc].[ProcessAnswerLevel]
	(
		[ProcessAnswerID],
		[ProcessLevelID],
		[Description]
	)
	VALUES
	(
		@ProcessAnswerID,
		@ProcessLevelID,
		@Description
	)

	Set @Err = @@Error

	IF @Log = 1 
	BEGIN 
		INSERT INTO [Log].[AuditLog] ( UserId, TableName, Type, Data, Created) 
		SELECT @cUserid,'ProcessAnswerLevel',0,
		( SELECT * FROM [proc].[ProcessAnswerLevel] 
			WHERE
			[ProcessAnswerID] = @ProcessAnswerID AND
			[ProcessLevelID] = @ProcessLevelID				 FOR XML AUTO) as data,
				getdate() 
	 END

	RETURN @Err
END


GO
