SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[at].[prc_OwnerColor_get]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [at].[prc_OwnerColor_get] AS' 
END
GO

ALTER PROCEDURE [at].[prc_OwnerColor_get]
(
	@OwnerID int = null,
	@ThemeID int = null
)
AS
BEGIN
	SET NOCOUNT ON
	DECLARE @Err Int

	SELECT
	[OwnerColorID],
	ISNULL([ThemeID],0) as 'ThemeID',
	ISNULL([OwnerID],0) as 'OwnerID',
	ISNULL([CustomerID], 0) AS 'CustomerID',
	[FriendlyName],
	[FillColor],
	[TextColor],
	[BorderColor],
	[No],
	[Created]
	FROM [at].[OwnerColor]
	WHERE
	( [OwnerID] = @OwnerID or @Ownerid is null)
	and ([ThemeID] = @ThemeID or @ThemeID is null)
	ORDER BY [No]

	Set @Err = @@Error

	RETURN @Err
END

GO
