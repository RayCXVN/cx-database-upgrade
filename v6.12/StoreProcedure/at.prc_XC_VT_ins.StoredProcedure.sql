SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[at].[prc_XC_VT_ins]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [at].[prc_XC_VT_ins] AS' 
END
GO

ALTER PROCEDURE [at].[prc_XC_VT_ins]
(
	@XCVTID int = null output,
	@ViewTypeID int,
	@XCID int,
	@No smallint,
	@Type smallint,
	@Template varchar(64),
	@ShowPercentage BIT,
	@cUserid int,
	@Log smallint = 1
)
AS
BEGIN
	SET NOCOUNT ON
	DECLARE @Err Int

	INSERT INTO [at].[XC_VT]
	(
		[ViewTypeID],
		[XCID],
		[No],
		[Type],
		[Template],
		[ShowPercentage]
	)
	VALUES
	(
		@ViewTypeID,
		@XCID,
		@No,
		@Type,
		@Template,
		@ShowPercentage
	)

	Set @Err = @@Error
	Set @XCVTID = scope_identity()

	IF @Log = 1 
	BEGIN 
		INSERT INTO [Log].[AuditLog] ( UserId, TableName, Type, Data, Created) 
		SELECT @cUserid,'XC_VT',0,
		( SELECT * FROM [at].[XC_VT] 
			WHERE
			[XCVTID] = @XCVTID				 FOR XML AUTO) as data,
				getdate() 
	 END

	RETURN @Err
END


GO
