SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[list].[prc_ItemListCommand_del]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [list].[prc_ItemListCommand_del] AS' 
END
GO
ALTER PROCEDURE [list].[prc_ItemListCommand_del]
	@ItemListCommandID int,
	@cUserid int,
	@Log smallint = 1
AS
BEGIN
	SET NOCOUNT ON;
	DECLARE @Err Int
	
	IF @Log = 1 
	BEGIN 
		INSERT INTO [Log].[AuditLog] ( UserId, TableName, Type, Data, Created) 
		SELECT @cUserid,'ItemListCommand',2,
		( SELECT * FROM [list].[ItemListCommand] 
			WHERE
			[ItemListCommandID] = @ItemListCommandID
			 FOR XML AUTO) as data,
			getdate() 
	END 
	
    DELETE FROM [list].[ItemListCommand]
    WHERE
		[ItemListCommandID] = @ItemListCommandID
	
	Set @Err = @@Error

	RETURN @Err
  
END

GO
