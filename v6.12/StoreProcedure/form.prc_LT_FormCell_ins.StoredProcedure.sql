SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[form].[prc_LT_FormCell_ins]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [form].[prc_LT_FormCell_ins] AS' 
END
GO

ALTER PROCEDURE [form].[prc_LT_FormCell_ins]
(
	@LanguageID int,
	@FormCellID int,
	@Name nvarchar(256),
	@ToolTip nvarchar(max),
	@Description nvarchar(max),
	@cUserid int,
	@Log smallint = 1
)
AS
BEGIN
	SET NOCOUNT ON
	DECLARE @Err Int

	INSERT INTO [form].[LT_FormCell]
	(
		[LanguageID],
		[FormCellID],
		[Name],
		[ToolTip],
		[Description]
	)
	VALUES
	(
		@LanguageID,
		@FormCellID,
		@Name,
		@ToolTip,
		@Description
	)

	Set @Err = @@Error

	IF @Log = 1 
	BEGIN 
		INSERT INTO [Log].[AuditLog] ( UserId, TableName, Type, Data, Created) 
		SELECT @cUserid,'LT_FormCell',0,
		( SELECT * FROM [form].[LT_FormCell] 
			WHERE
			[LanguageID] = @LanguageID AND
			[FormCellID] = @FormCellID				 FOR XML AUTO) as data,
				getdate() 
	 END

	RETURN @Err
END


GO
