SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[org].[prc_Hierarchy_get]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [org].[prc_Hierarchy_get] AS' 
END
GO

ALTER PROCEDURE [org].[prc_Hierarchy_get]
(
	@OwnerID int
)
AS
BEGIN
	SET NOCOUNT ON
	DECLARE @Err Int

	SELECT
	[HierarchyID],
	[OwnerID],
	[Name],
	[Description],
	[Type],
	[Deleted],
	[Created]
	FROM [org].[Hierarchy]
	WHERE
	[OwnerID] = @OwnerID

	Set @Err = @@Error

	RETURN @Err
END


GO
