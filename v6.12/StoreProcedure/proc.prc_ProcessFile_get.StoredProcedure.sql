SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[proc].[prc_ProcessFile_get]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [proc].[prc_ProcessFile_get] AS' 
END
GO

ALTER PROCEDURE [proc].[prc_ProcessFile_get]
(
	@ProcessID int
)
AS
BEGIN
	SET NOCOUNT ON
	DECLARE @Err Int

	SELECT
	[ProcessFileId],
	[ProcessId],
	[ContentType],
	[Size],
	[Title],
	[FileName],
	[Description],
	[No],
	[Created],
	ISNULL([CustomerID],0) as 'CustomerID'
	FROM [proc].[ProcessFile]
	WHERE
	[ProcessID] = @ProcessID

	Set @Err = @@Error

	RETURN @Err
END

GO
