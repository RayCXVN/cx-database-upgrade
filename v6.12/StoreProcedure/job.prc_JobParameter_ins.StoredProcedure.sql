SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[job].[prc_JobParameter_ins]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [job].[prc_JobParameter_ins] AS' 
END
GO


ALTER PROCEDURE [job].[prc_JobParameter_ins]
(
	@JobParameterID int = null output,
	@JobID int,
	@No smallint,
	@Name nvarchar(256),
	@Value nvarchar(max),
	@Type smallint,
	@cUserid int,
	@Log smallint = 1
)
AS
BEGIN
	SET NOCOUNT ON
	DECLARE @Err Int

	INSERT INTO [job].[JobParameter]
	(
		[JobID],
		[No],
		[Name],
		[Value],
		[Type]
	)
	VALUES
	(
		@JobID,
		@No,
		@Name,
		@Value,
		@Type
	)

	Set @Err = @@Error
	Set @JobParameterID = scope_identity()

	IF @Log = 1 
	BEGIN 
		INSERT INTO [Log].[AuditLog] ( UserId, TableName, Type, Data, Created) 
		SELECT @cUserid,'JobParameter',0,
		( SELECT * FROM [job].[JobParameter] 
			WHERE
			[JobParameterID] = @JobParameterID				 FOR XML AUTO) as data,
				getdate() 
	 END

	RETURN @Err
END



GO
