SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[at].[prc_LT_StatusType_ins]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [at].[prc_LT_StatusType_ins] AS' 
END
GO

ALTER PROCEDURE [at].[prc_LT_StatusType_ins]
(
	@LanguageID int,
	@StatusTypeID int,
	@Name varchar(50),
	@Description varchar(250),
	@DefaultActionName Nvarchar(512) ='',
	@DefaultActionDescription NVARCHAR(MAX) ='',
	@cUserid int,
	@Log smallint = 1
)
AS
BEGIN
	SET NOCOUNT ON
	DECLARE @Err Int

	INSERT INTO [at].[LT_StatusType]
	(
		[LanguageID],
		[StatusTypeID],
		[Name],
		[Description],
		[DefaultActionName],
		[DefaultActionDescription]
	)
	VALUES
	(
		@LanguageID,
		@StatusTypeID,
		@Name,
		@Description,
		@DefaultActionName,
		@DefaultActionDescription
	)

	Set @Err = @@Error

	IF @Log = 1 
	BEGIN 
		INSERT INTO [Log].[AuditLog] ( UserId, TableName, Type, Data, Created) 
		SELECT @cUserid,'LT_StatusType',0,
		( SELECT * FROM [at].[LT_StatusType] 
			WHERE
			[LanguageID] = @LanguageID AND
			[StatusTypeID] = @StatusTypeID				 FOR XML AUTO) as data,
				getdate() 
	 END

	RETURN @Err
END

GO
