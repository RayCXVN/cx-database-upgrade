SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[prc_RelationType_del]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [dbo].[prc_RelationType_del] AS' 
END
GO

ALTER PROCEDURE [dbo].[prc_RelationType_del]
(
	@RelationTypeID int,
	@cUserid int,
	@Log smallint = 1
)
AS
BEGIN
	SET NOCOUNT ON
	DECLARE @Err Int

	IF @Log = 1 
	BEGIN 
		INSERT INTO [Log].[AuditLog] ( UserId, TableName, Type, Data, Created) 
		SELECT @cUserid,'RelationType',2,
		( SELECT * FROM [dbo].[RelationType] 
			WHERE
			[RelationTypeID] = @RelationTypeID
			 FOR XML AUTO) as data,
			getdate() 
	END 


	DELETE FROM [dbo].[RelationType]
	WHERE
		[RelationTypeID] = @RelationTypeID

	Set @Err = @@Error

	RETURN @Err
END


GO
