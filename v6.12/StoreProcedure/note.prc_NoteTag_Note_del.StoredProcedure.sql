SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[note].[prc_NoteTag_Note_del]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [note].[prc_NoteTag_Note_del] AS' 
END
GO
ALTER PROCEDURE [note].[prc_NoteTag_Note_del]
(
	@NoteID int,
	@NoteTagID int,
	@cUserid int,
	@Log smallint = 1
)
AS
BEGIN
	SET NOCOUNT ON
	DECLARE @Err Int

	IF @Log = 1 
	BEGIN 
		INSERT INTO [Log].[AuditLog] ( UserId, TableName, Type, Data, Created) 
		SELECT @cUserid,'NoteTag_Note',2,
		( SELECT * FROM [note].[NoteTag_Note] 
			WHERE
			[NoteID] = @NoteID
			AND [NoteTagID] = @NoteTagID FOR XML AUTO) as data,
				getdate() 
	 END

	DELETE FROM [note].[NoteTag_Note]
	WHERE
		[NoteID] = @NoteID
		AND [NoteTagID] = @NoteTagID
		
	Set @Err = @@Error
	
	
	RETURN @Err
END

GO
