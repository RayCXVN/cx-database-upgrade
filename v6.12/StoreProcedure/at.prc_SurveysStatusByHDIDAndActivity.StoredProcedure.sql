SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[at].[prc_SurveysStatusByHDIDAndActivity]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [at].[prc_SurveysStatusByHDIDAndActivity] AS' 
END
GO
/*
    Steve - Dec 19 2016 - Add a new param @GetCurrentSurvey
    Ray - May 25 2017 - Count within school; Remove parameter @GlobalId and @DepartmentIds because of no use
	EXEC [at].[prc_SurveysStatusByHDIDAndActivity] @HDID = 17319,@bSub = 1,@RoleID = '',@ActivityIDList = N'696',@PeriodID = 9,@DepartmentID = '',@UserGroupID = '',@DepTypeIDList = '',@SurveyID = NULL,@Debug = 0,@GetCurrentSurvey = 1
*/
/* Test case and usage guide
    -- General activity status
    EXEC at.[prc_SurveysStatusByHDIDAndActivity] 22968,1,null,'466,458,68,167,174,179,184,180,185,181,182,186,183,187',5,'','',''

    -- Detail activity status by activity/active survey
    EXEC at.[prc_SurveysStatusByHDIDAndActivity] 22968,0,null,'68',5,'','',''
    EXEC at.[prc_SurveysStatusByHDIDAndActivity] 24422,0,null,'51',5,'','',''
    EXEC at.[prc_SurveysStatusByHDIDAndActivity] 78733,0,null,'51',5,'','',''
    EXEC at.[prc_SurveysStatusByHDIDAndActivity] 16910,0,null,'51',5,'','',''

    -- Expanding activity list by activity/all survey
    EXEC at.[prc_SurveysStatusByHDIDAndActivity] 22968,1,null,'68',0,'','',''
    EXEC at.[prc_SurveysStatusByHDIDAndActivity] 16905,1,null,'68',0,'','',''
    EXEC at.[prc_SurveysStatusByHDIDAndActivity] 22968,1,null,'167',0,'','',''
    EXEC at.[prc_SurveysStatusByHDIDAndActivity] 22765,1,null,'51',0,'','',''

    -- Expanding detail by activity/all survey
    EXEC at.[prc_SurveysStatusByHDIDAndActivity] 22968,0,null,'68',5,'','','',862
    EXEC at.[prc_SurveysStatusByHDIDAndActivity] 22765,0,null,'51',4,'','25178,25179,25180','',331
    EXEC at.[prc_SurveysStatusByHDIDAndActivity] 22968,0,null,'68',4,'','20172,20173','',416
    EXEC at.[prc_SurveysStatusByHDIDAndActivity] 22968,0,null,'68',4,'','','40',416
    EXEC at.[prc_SurveysStatusByHDIDAndActivity] 16905,0,null,'68',4,'','','40',416
    EXEC at.[prc_SurveysStatusByHDIDAndActivity] 22968,0,null,'68',4,'','20172,20173','',337
*/
ALTER PROCEDURE [at].[prc_SurveysStatusByHDIDAndActivity]
(
    @HDID               int,
    @bSub               bit = 0,
    @RoleID	            varchar(max) = NULL,
    @ActivityIDList     varchar(max),
    @PeriodID           int = NULL,
    @DepartmentID       varchar(max) = NULL,
    @UserGroupID        varchar(max) = NULL,
    @DepTypeIDList      varchar(max) = NULL,
    @SurveyID           int = NULL, 
    @Debug              int = 0,
    @GetCurrentSurvey   bit = 1
) WITH RECOMPILE
AS 
BEGIN
    SET NOCOUNT ON

    CREATE TABLE #ActivityList (ActivityID int)
    CREATE TABLE #SurveyList (ActivityID int, SurveyID int, ReportServer nvarchar(64), ReportDB nvarchar(64))
    CREATE TABLE #StatusTable (ActivityID int, HDID int, SurveyID int, DepartmentName nvarchar(256), StatusTypeID int, ResultCount int)
    
    DECLARE c_ReportDB CURSOR FORWARD_ONLY READ_ONLY FOR
    SELECT DISTINCT ReportServer, ReportDB FROM #SurveyList
    
    DECLARE @exclude_parentHD nvarchar(150) = '', @ParentHD_clause nvarchar(512) = '',
            @ReportServer nvarchar(64) = '', @ReportDB nvarchar(64) = '',
            @sqlCommand nvarchar(max) = '', @parameters nvarchar(max) = '', @HD_columns nvarchar(256) = '', @GroupByHD nvarchar(256) = '',
            @LinkedDB NVARCHAR(MAX) = ''
    DECLARE @ActiveEntityStatusID VARCHAR(5) = (SElECT EntityStatusID FROM dbo.EntityStatus WHERE CodeName = N'Active')

    INSERT INTO #ActivityList (ActivityID) SELECT Value FROM dbo.funcListToTableInt(@ActivityIDList, ',')

    IF @PeriodID > 0 AND @GetCurrentSurvey = 1
    BEGIN
        INSERT INTO #SurveyList (ActivityID, SurveyID, ReportServer, ReportDB)
        SELECT ActivityID, SurveyID, ReportServer, ReportDB
        FROM (SELECT s.ActivityID, s.SurveyID, s.ReportServer, s.ReportDB,
                     ROW_NUMBER() OVER (PARTITION BY s.[ActivityID] ORDER BY s.[EndDate] DESC, s.[StartDate] DESC) AS [RowNum]
                FROM at.Survey s
                JOIN #ActivityList a ON s.[ActivityID] = a.[ActivityID]
                 AND s.PeriodID = @PeriodID
                 AND s.SurveyID = ISNULL(@SurveyID, s.SurveyID)
                 AND s.[Status] > 0
                 AND (s.StartDate <= GETDATE() AND GETDATE() <= s.EndDate)
                 AND s.ExtId <> N'global') v
        WHERE v.[RowNum] = 1
    END
    ELSE
	BEGIN
		INSERT INTO #SurveyList (ActivityID, SurveyID, ReportServer, ReportDB)
		SELECT s.ActivityID, s.SurveyID, s.ReportServer, s.ReportDB
		FROM at.Survey s
        JOIN #ActivityList a ON s.[ActivityID] = a.[ActivityID]
         AND (s.PeriodID = @PeriodID OR ISNULL(@PeriodID,0) = 0)
         AND s.SurveyID = ISNULL(@SurveyID,s.SurveyID)
         AND s.[Status] > 0
         AND s.ExtId <> N'global'
	END

	OPEN c_ReportDB
	FETCH c_ReportDB INTO @ReportServer, @ReportDB
	WHILE @@FETCH_STATUS = 0
	BEGIN
	    IF @ReportDB=DB_NAME() AND @ReportServer=@@servername
		    BEGIN
		    SET @LinkedDB=' '
		    END
	    ELSE
		    BEGIN
		    SET @LinkedDB= '['+ @ReportServer + '].['+ @ReportDB + '].'
		    END
	    IF @bSub <> 0
	    BEGIN
		    SET @HD_columns = CONVERT(nvarchar(16), @HDID) + ' AS HDID, ' + CONVERT(nvarchar(16), @HDID) + ' AS DepartmentName,'
		    SET @GroupByHD = ''
		    SET @exclude_parentHD = ''
		    SET @ParentHD_clause = ''
	    END
	    ELSE
	    BEGIN
		    SET @HD_columns = 'ISNULL(parent_hd.HDID, hd.HDID) AS HDID, ISNULL(parent_hd.Name, d.Name) AS DepartmentName,'
		    SET @GroupByHD = 'ISNULL(parent_hd.HDID, hd.HDID), ISNULL(parent_hd.Name, d.Name),'
		    SET @exclude_parentHD = ' AND hd.HDID <> ' + CONVERT(nvarchar(16), @HDID)
		    SET @ParentHD_clause = ' LEFT JOIN (SELECT phd.HDID, phd.[Path], d1.DepartmentID, d1.Name FROM org.H_D phd JOIN org.Department d1 ON d1.DepartmentID = phd.DepartmentID WHERE phd.ParentID=' + CONVERT(nvarchar(16), @HDID) + ') AS parent_hd ON hd.[Path] LIKE ''%'' + [parent_hd].[Path] + ''%'''
	    END

		IF ISNULL(@UserGroupID,'') = '' AND ISNULL(@DepTypeIDList,'') = ''
		BEGIN
			SET @sqlCommand = N'INSERT INTO #StatusTable (HDID, DepartmentName, SurveyID, StatusTypeID, ResultCount)
			SELECT ' + @HD_columns + ' r.SurveyID, ISNULL(r.StatusTypeID,0) AS StatusTypeID, COUNT(r.[ResultID]) AS ResultCount
			FROM  ' + @LinkedDB + '[dbo].[Result] r
			JOIN #SurveyList sl ON r.[SurveyID] = sl.[SurveyID] AND sl.[ReportServer] = @p_ReportServer AND sl.[ReportDB] = @p_ReportDB AND r.EntityStatusID = ' + @ActiveEntityStatusID + ' AND r.Deleted IS NULL
            JOIN org.[H_D] hd ON hd.[DepartmentID] = r.[DepartmentID] AND hd.[Deleted] = 0 AND hd.[Path] LIKE ''%\' + CAST(@HDID AS VARCHAR(32)) + '\%''    -- Count within school
            JOIN org.[Department] d ON d.DepartmentID = r.DepartmentID AND d.EntityStatusID = ' + @ActiveEntityStatusID + ' AND d.Deleted IS NULL '
    	   
			IF ISNULL(@RoleID,'') != ''
			BEGIN
				SET @sqlCommand += N' AND r.RoleID IN (' + @RoleID + ')'
			END
    	   
			IF ISNULL(@DepartmentID,'') != ''
			BEGIN
				SET @sqlCommand += N' AND r.DepartmentID IN (' + @DepartmentID + ')'
			END
	   	   
            IF(@bSub = 0)
            BEGIN  
			SET @sqlCommand += @exclude_parentHD + @ParentHD_clause 
            END

			SET @sqlCommand += N' GROUP BY ' + @GroupByHD + ' r.SurveyID, ISNULL(r.StatusTypeID,0)'
			SET @parameters = N'@p_ReportServer nvarchar(64),@p_ReportDB nvarchar(64)'
			  
			EXEC sp_executesql @sqlCommand, @parameters, @p_ReportServer = @ReportServer, @p_ReportDB = @ReportDB
		END
		ELSE
		BEGIN
			SET @sqlCommand = N'INSERT INTO #StatusTable (HDID, DepartmentName, SurveyID, StatusTypeID, ResultCount)
			SELECT ISNULL(parent_hd.HDID, r.UserGroupID) AS HDID, ISNULL(parent_hd.Name, ug.Name) AS DepartmentName, r.SurveyID, ISNULL(r.StatusTypeID,0) AS StatusTypeID, COUNT(r.[ResultID]) AS ResultCount
			FROM '+ @LinkedDB +'[dbo].[Result] r
            JOIN #SurveyList sl ON r.[SurveyID] = sl.[SurveyID] AND sl.[ReportServer] = @p_ReportServer AND sl.[ReportDB] = @p_ReportDB AND r.EntityStatusID = ' + @ActiveEntityStatusID + ' AND r.Deleted IS NULL
			JOIN org.[User] u on u.UserID = r.UserID AND u.EntityStatusID = ' + @ActiveEntityStatusID + ' AND u.Deleted IS NULL '
    	   
			IF ISNULL(@RoleID,'') != ''
			BEGIN
				SET @sqlCommand += N' AND r.RoleID IN (' + @RoleID + ')'
			END
    	   
			IF ISNULL(@UserGroupID,'') != ''
			BEGIN
				SET @sqlCommand += N' AND r.UserGroupID IN (' + @UserGroupID + ')'
			END
		  
			SET @sqlCommand += N' JOIN org.UserGroup ug ON ug.UserGroupID = r.UserGroupID AND ug.EntityStatusID = ' + @ActiveEntityStatusID + ' AND ug.Deleted IS NULL AND ug.PeriodID = ' + CONVERT(nvarchar(16), @PeriodID) + '
			JOIN org.H_D hd on ug.DepartmentID = hd.DepartmentID AND hd.[Path] LIKE ''%\' + CONVERT(nvarchar(16), @HDID) + '\%''' + @ParentHD_clause
		  
			IF ISNULL(@DepTypeIDList,'') != ''
			BEGIN
				SET @sqlCommand += N' JOIN org.DT_UG dtug ON dtug.UserGroupID = ug.UserGroupID AND dtug.DepartmentTypeID IN (' + @DepTypeIDList + ')'
			END
	   	   
			SET @sqlCommand += N' GROUP BY ISNULL(parent_hd.HDID, r.UserGroupID), ISNULL(parent_hd.Name, ug.Name), r.SurveyID, ISNULL(r.StatusTypeID,0)'
			SET @parameters = N'@p_ReportServer nvarchar(64),@p_ReportDB nvarchar(64)'
			  
			EXEC sp_executesql @sqlCommand, @parameters, @p_ReportServer = @ReportServer, @p_ReportDB = @ReportDB 
		END

	    IF(@Debug = 1)
        BEGIN
            PRINT @sqlCommand
        END

		FETCH c_ReportDB INTO @ReportServer, @ReportDB
	END
	CLOSE c_ReportDB
	DEALLOCATE c_ReportDB
    
	UPDATE #StatusTable SET ActivityID = (SELECT s.ActivityID FROM at.Survey s WHERE s.SurveyID = st.SurveyID) FROM #StatusTable st
    
	INSERT INTO #StatusTable (ActivityID, SurveyID, HDID, DepartmentName, StatusTypeID, ResultCount) 
	SELECT sl.ActivityID, sl.SurveyID, 0, '0', 1, 0
	FROM   #SurveyList sl 
	WHERE  NOT EXISTS (SELECT 1 FROM #StatusTable st WHERE st.ActivityID = sl.ActivityID AND st.SurveyID = sl.SurveyID)
    
	SELECT * FROM #StatusTable ORDER BY ActivityID, SurveyID, DepartmentName
    
    DROP TABLE #ActivityList
    DROP TABLE #SurveyList
    DROP TABLE #StatusTable
END

GO
