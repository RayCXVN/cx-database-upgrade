SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[list].[prc_ItemListField_upd]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [list].[prc_ItemListField_upd] AS' 
END
GO
/*
    2017-02-09 Steve:  Fix bug 64587 - [Mass-registration]- UI of user list is broken when changing status in header
    2017-09-26 Ray:    Increase CssClass to 256
*/
ALTER PROCEDURE [list].[prc_ItemListField_upd]
    @ItemListFieldID int,
    @ItemListID int,
    @No int,
    @TableTypeID smallint = null,
    @FieldType int,
    @ValueType int,
    @FieldName nvarchar(128),
    @PropID int = null,
    @QuestionID int = null,
    @OverrideLocked bit,
    @ShowInUserList bit,
    @ShowInTableView bit,
    @ShowInExport bit,
    @Mandatory bit,
    @Sortable bit,
    @SortNo smallint,
    @SortDirection bit,
    @Width int,
    @Align int,
    @ItemPrefix nvarchar(32),
    @ItemSuffix nvarchar(32),
    @HeaderCssClass nvarchar(256),
    @ItemCssClass nvarchar(256),
    @ColumnCssClass nvarchar(256),
    @HeaderGroupCSSClass nvarchar(256),
    @HeaderGroupLastItem bit,
    @cUserid int,
    @Log smallint = 1
AS
BEGIN
    SET NOCOUNT ON;
    DECLARE @Err Int
    
    UPDATE [list].[ItemListField]
    SET 
        [ItemListID] = @ItemListID,
        [No] = @No,
        [TableTypeID] = @TableTypeID,
        [FieldType] = @FieldType,
        [ValueType] = @ValueType,
        [FieldName] = @FieldName,
        [PropID] = @PropID,
        [QuestionID] = @QuestionID,
        [OverrideLocked] = @OverrideLocked,
        [ShowInUserList] = @ShowInUserList,
        [ShowInTableView] = @ShowInTableView,
        [ShowInExport] = @ShowInExport,
        [Mandatory] = @Mandatory,
        [Sortable] = @Sortable,
        [SortNo] = @SortNo,
        [SortDirection] = @SortDirection,
        [Width] = @Width,
        [Align] = @Align,
        [ItemPrefix] = @ItemPrefix,
        [ItemSuffix] = @ItemSuffix,
        [HeaderCssClass] = @HeaderCssClass,
        [ItemCssClass] = @ItemCssClass,
        [ColumnCssClass] = @ColumnCssClass,
        [HeaderGroupCSSClass] = @HeaderGroupCSSClass,
        [HeaderGroupLastItem] = @HeaderGroupLastItem
     WHERE 
        [ItemListFieldID] = @ItemListFieldID
        
    IF @Log = 1 
    BEGIN 
        INSERT INTO [Log].[AuditLog] ( UserId, TableName, Type, Data, Created) 
        SELECT @cUserid,'ItemListField',1,
        ( SELECT * FROM [list].[ItemListField] 
            WHERE
            [ItemListFieldID] = @ItemListFieldID             FOR XML AUTO) as data,
            getdate()
    END

    Set @Err = @@Error

    RETURN @Err    
END
GO