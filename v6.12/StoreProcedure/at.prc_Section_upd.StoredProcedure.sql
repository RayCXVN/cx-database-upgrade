SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[at].[prc_Section_upd]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [at].[prc_Section_upd] AS' 
END
GO

ALTER PROCEDURE [at].[prc_Section_upd]
(
	@SectionID int,
	@ActivityID int,
	@Type smallint,
	@MinOccurance smallint,
	@MaxOccurance smallint,
	@Moveable bit,
	@CssClass nvarchar(64) = '',
	@cUserid int,
	@Log smallint = 1
)
AS
BEGIN
	SET NOCOUNT ON
	DECLARE @Err Int

	UPDATE [at].[Section]
	SET
		[ActivityID] = @ActivityID,
		[Type] = @Type,
		[MinOccurance] = @MinOccurance,
		[MaxOccurance] = @MaxOccurance,
		[Moveable] = @Moveable,
		[CssClass] = @CssClass
	WHERE
		[SectionID] = @SectionID

	IF @Log = 1 
	BEGIN 
		INSERT INTO [Log].[AuditLog] ( UserId, TableName, Type, Data, Created) 
		SELECT @cUserid,'Section',1,
		( SELECT * FROM [at].[Section] 
			WHERE
			[SectionID] = @SectionID			 FOR XML AUTO) as data,
			getdate()
	END

	Set @Err = @@Error

	RETURN @Err
END



GO
