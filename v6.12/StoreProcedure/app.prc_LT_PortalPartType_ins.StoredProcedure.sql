SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[app].[prc_LT_PortalPartType_ins]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [app].[prc_LT_PortalPartType_ins] AS' 
END
GO

ALTER PROCEDURE [app].[prc_LT_PortalPartType_ins]
(
	@LanguageID int,
	@PortalPartTypeID int,
	@Name nvarchar(256),
	@Description nvarchar(max),
	@cUserid int,
	@Log smallint = 1
)
AS
BEGIN
	SET NOCOUNT ON
	DECLARE @Err Int

	INSERT INTO [app].[LT_PortalPartType]
	(
		[LanguageID],
		[PortalPartTypeID],
		[Name],
		[Description]
	)
	VALUES
	(
		@LanguageID,
		@PortalPartTypeID,
		@Name,
		@Description
	)

	Set @Err = @@Error

	IF @Log = 1 
	BEGIN 
		INSERT INTO [Log].[AuditLog] ( UserId, TableName, Type, Data, Created) 
		SELECT @cUserid,'LT_PortalPartType',0,
		( SELECT * FROM [app].[LT_PortalPartType] 
			WHERE
			[LanguageID] = @LanguageID AND
			[PortalPartTypeID] = @PortalPartTypeID				 FOR XML AUTO) as data,
				getdate() 
	 END

	RETURN @Err
END


GO
