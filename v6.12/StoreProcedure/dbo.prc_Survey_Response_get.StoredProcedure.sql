SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[prc_Survey_Response_get]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [dbo].[prc_Survey_Response_get] AS' 
END
GO
ALTER proc [dbo].[prc_Survey_Response_get]
(
    @SurveyId INT, 
	@Invited	INT = 0 OUTPUT,
	@Finished	INT = 0 OUTPUT
)
AS
BEGIN
    DECLARE @ActiveEntityStatusID INT = (SELECT EntityStatusID FROM EntityStatus WHERE CodeName='Active')

	SELECT @Invited = COUNT(*), @Finished = COUNT(Enddate) 
	FROM Result
	WHERE Surveyid =  @SurveyId AND EntityStatusID = @ActiveEntityStatusID AND Deleted IS NULL
END
---------------------------------------------------------------------


GO
