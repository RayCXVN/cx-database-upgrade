SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[sec].[prc_AccessControlList_ins]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [sec].[prc_AccessControlList_ins] AS' 
END
GO
ALTER PROCEDURE [sec].[prc_AccessControlList_ins]
(
	@AccessControlListID int = NULL output,
	@GroupAccessRuleID int,
   @AccessGroupID int, 
   @No int,
   @RuleType nvarchar(512),
	@cUserid int,
	@Log smallint = 1
)
AS
BEGIN
SET NOCOUNT ON
DECLARE @Err int
INSERT INTO [sec].[AccessControlList] ([GroupAccessRuleID]
, [AccessGroupID]
, [No]
, [RuleType])
    VALUES (@GroupAccessRuleID, @AccessGroupID, @No, @RuleType)
SET @Err = @@Error
SET @AccessControlListID = SCOPE_IDENTITY()
IF @Log = 1 BEGIN
INSERT INTO [Log].[AuditLog] (UserId, TableName, Type, Data, Created)
    SELECT
        @cUserid,
        'AccessControlList',
        0,
        (SELECT
            *
        FROM [sec].[AccessControlList]
        WHERE [AccessControlListID] = @AccessControlListID
        FOR xml AUTO)
        AS data,
        GETDATE()
END
RETURN @Err
END

GO
