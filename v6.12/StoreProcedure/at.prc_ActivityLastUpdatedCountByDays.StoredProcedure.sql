SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[at].[prc_ActivityLastUpdatedCountByDays]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [at].[prc_ActivityLastUpdatedCountByDays] AS' 
END
GO
/*
    Steve - Nov 31, 2016 - UPDATE org.Department SET LastUpdatedBy = NULL to delete users and add script of deleting measures in subdepartments
	Johnny - Des 07 2016 - Update store procedure for dynamic SQL 
    EXEC [at].[prc_ActivityLastUpdatedCountByDays] '1035,1031,1032,583,1120,1129,1111,47',-14,22765
*/
ALTER PROCEDURE [at].[prc_ActivityLastUpdatedCountByDays]
(
  @ActivityIDList	varchar(max),
  @Period			int,
  @HDID			int = 0
)
AS
BEGIN
  SET NOCOUNT ON
  DECLARE @ReportDBTable TABLE (ReportServer  nvarchar(64), ReportDB nvarchar(64))
  DECLARE @ActiveEntityStatusID VARCHAR(2) = (SELECT EntityStatusID FROM EntityStatus WHERE CodeName='Active')
  DECLARE @ActivityTable TABLE (ActivityID int)
  CREATE TABLE #SurveyList (SurveyID int)
  CREATE TABLE #Result (ActivityID int, SurveyID int, ResultCount int)
  DECLARE @LinkedDB NVARCHAR(MAX)
  DECLARE c_ReportDB CURSOR FORWARD_ONLY READ_ONLY FOR
  SELECT ReportServer, ReportDB FROM @ReportDBTable
  
  DECLARE @ReportServer nvarchar(64), @ReportDB nvarchar(64), @RetValue int,
		@sql_command nvarchar(max), @parameter nvarchar(max) = N''
  
  INSERT INTO @ActivityTable (ActivityID) SELECT Value FROM funcListToTableInt(@ActivityIDList, ',')
  
  INSERT INTO #SurveyList (SurveyID)
  SELECT s.SurveyID FROM at.Survey s
  WHERE s.ActivityID IN (SELECT ActivityID FROM @ActivityTable)
    AND s.StartDate < GETDATE()
    AND s.EndDate > GETDATE()
    AND s.[Status] > 0
  
  INSERT INTO @ReportDBTable (ReportServer, ReportDB)
  SELECT DISTINCT ReportServer, ReportDB
  FROM   at.Survey
  WHERE  SurveyID IN (SELECT SurveyID FROM #SurveyList)
  
  SELECT @ReportServer=ReportServer, @ReportDB=ReportDB FROM @ReportDBTable
  IF @ReportDB=DB_NAME() AND @ReportServer=@@servername
  BEGIN
	SET @LinkedDB=' '
  END
  ELSE
  BEGIN
	SET @LinkedDB= '['+ @ReportServer + '].['+ @ReportDB + '].'
  END

  IF EXISTS(SELECT TOP 1 * FROM @ReportDBTable)
  BEGIN
    OPEN c_ReportDB
      FETCH NEXT FROM c_ReportDB INTO @ReportServer, @ReportDB
      WHILE @@FETCH_STATUS = 0
      BEGIN
        IF @HDID = 0
	   BEGIN
		SET @sql_command = N'INSERT INTO #Result (SurveyID, ResultCount)
		SELECT SurveyID, Count(ResultID)
		FROM  '+ @LinkedDB +' [dbo].Result WITH (NOLOCK)
		WHERE  LastUpdated > dateadd("d",@p_Period,getdate())'
		  
		IF EXISTS (SELECT TOP 1 SurveyID FROM #SurveyList)
		BEGIN
		  SET @sql_command += ' AND SurveyID IN (SELECT SurveyID FROM #SurveyList)'
		END
		
		SET @sql_command += ' AND EntityStatusID = '+@ActiveEntityStatusID+' AND Deleted IS NULL GROUP BY SurveyID'

		SET @parameter = N'@p_Period int'
		EXECUTE sp_executesql @sql_command, @parameter, @p_Period = @Period
	   END
	   ELSE
	   BEGIN
	     SET @sql_command = N'INSERT INTO #Result (SurveyID, ResultCount)
		SELECT r.SurveyID, Count(r.ResultID)
		FROM  '+ @LinkedDB +'[dbo].Result r WITH (NOLOCK)
		JOIN   [org].H_D hd WITH (NOLOCK) ON hd.DepartmentID = r.DepartmentID AND hd.path like ''%\' + convert(nvarchar,@HDID) + '\%''
		WHERE  r.LastUpdated > dateadd("d",@p_Period,getdate())'
		
		IF EXISTS (SELECT TOP 1 SurveyID FROM #SurveyList)
		BEGIN
		  SET @sql_command += ' AND r.SurveyID IN (SELECT SurveyID FROM #SurveyList)'
		END
		
		SET @sql_command += ' AND EntityStatusID = '+@ActiveEntityStatusID+' AND r.Deleted IS NULL GROUP BY r.SurveyID'
		SET @parameter = N'@p_Period int'
		EXECUTE sp_executesql @sql_command, @parameter, @p_Period = @Period
	   END
        FETCH NEXT FROM c_ReportDB INTO @ReportServer, @ReportDB
      END
    CLOSE c_ReportDB
    DEALLOCATE c_ReportDB
    
    UPDATE #Result SET ActivityID = (SELECT s.ActivityID FROM at.Survey s WHERE s.SurveyID = r.SurveyID) FROM #Result r
    SET @RetValue = 1
  END
  ELSE
  BEGIN
    SET @RetValue = 0
  END
  
  SELECT ActivityID, ISNULL(SUM(ResultCount),0) ResultCount FROM #Result GROUP BY ActivityID
  
  DROP TABLE #SurveyList
  DROP TABLE #Result
  Return @RetValue
END

GO
