SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[prc_DeleteCustomersAndData]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [dbo].[prc_DeleteCustomersAndData] AS' 
END
GO
/*
    Steve - Nov 31, 2016 - UPDATE org.Department SET LastUpdatedBy = NULL to delete users and add script of deleting measures in subdepartments
    Steve - Dec 8, 2016 - Job Fair project: merge Q and R DB, then change how to make a dynamic query
    2018-07-09 Ray:     Replace UG_U by UGMember
*/
ALTER PROCEDURE [dbo].[prc_DeleteCustomersAndData] (@CustomerIDList nvarchar(max))
AS
BEGIN
    SET NOCOUNT ON
    SET XACT_ABORT ON
    BEGIN TRANSACTION
    
    DECLARE @CustomerIDTable TABLE (id int)
    DECLARE @HierarchyIDTable TABLE (id int)
    DECLARE @SubDepartmentIDTable TABLE (id int)
    INSERT INTO @CustomerIDTable (id) SELECT Value FROM dbo.funcListToTableInt(@CustomerIDList, ',')

    RAISERROR ('Deleting Batch, UserGroup, U_D, mea.Category', 0, 1)
    DELETE tbd FROM at.Batch tbd JOIN [org].[Department] d ON tbd.[DepartmentID] = d.[DepartmentID] AND d.[CustomerID] IN (SELECT id FROM @CustomerIDTable)
    DELETE tbd FROM org.DT_UG tbd JOIN [org].UserGroup g ON tbd.UserGroupID = g.UserGroupID JOIN [org].[User] u ON g.[UserID] = u.[UserID] JOIN [org].[Department] d ON u.[DepartmentID] = d.[DepartmentID] AND d.[CustomerID] IN (SELECT id FROM @CustomerIDTable)
    DELETE tbd FROM org.DT_UG tbd JOIN [org].UserGroup g ON tbd.UserGroupID = g.UserGroupID JOIN [org].[Department] d ON g.[DepartmentID] = d.[DepartmentID] AND d.[CustomerID] IN (SELECT id FROM @CustomerIDTable)
    DELETE tbd FROM org.[UGMember] tbd JOIN [org].[User] u ON tbd.[UserID] = u.[UserID] JOIN [org].[Department] d ON u.[DepartmentID] = d.[DepartmentID] AND d.[CustomerID] IN (SELECT id FROM @CustomerIDTable)
    DELETE tbd FROM org.[UGMember] tbd JOIN [org].[UserGroup] ug ON tbd.UserGroupID = ug.UserGroupID JOIN [org].[Department] d ON ug.[DepartmentID] = d.[DepartmentID] AND d.[CustomerID] IN (SELECT id FROM @CustomerIDTable)
    DELETE tbd FROM org.[UGMember] tbd JOIN [org].[UserGroup] ug ON tbd.UserGroupID = ug.UserGroupID JOIN [org].[User] u ON ug.[UserID] = u.[UserID] JOIN [org].[Department] d ON u.[DepartmentID] = d.[DepartmentID] AND d.[CustomerID] IN (SELECT id FROM @CustomerIDTable)
    DELETE tbd FROM org.[UserGroup] tbd JOIN [org].[Department] d ON tbd.[DepartmentID] = d.[DepartmentID] AND d.[CustomerID] IN (SELECT id FROM @CustomerIDTable)
    DELETE tbd FROM org.[UserGroup] tbd JOIN [org].[User] u ON tbd.[UserID] = u.[UserID] JOIN [org].[Department] d ON u.[DepartmentID] = d.[DepartmentID] AND d.[CustomerID] IN (SELECT id FROM @CustomerIDTable)
    DELETE tbd FROM org.U_D tbd JOIN [org].[Department] d ON tbd.[DepartmentID] = d.[DepartmentID] AND d.[CustomerID] IN (SELECT id FROM @CustomerIDTable)
    DELETE tbd FROM org.U_D tbd JOIN [org].[User] u ON tbd.[UserID] = u.[UserID] JOIN [org].[Department] d ON u.[DepartmentID] = d.[DepartmentID] AND d.[CustomerID] IN (SELECT id FROM @CustomerIDTable)
    DELETE tbd FROM mea.Category tbd WHERE CustomerId IN (SELECT id FROM @CustomerIDTable)
    
    RAISERROR ('Deleting mea.CheckListItem, mea.Measure, mea.CheckListItemTemplate, mea.MeasureTemplate', 0, 1) 
    DECLARE @MeasureIDTable TABLE (id int IDENTITY(1,1), MeasureId INT)
    DECLARE @i INT = 0, @maxid INT
    INSERT INTO @MeasureIDTable ([MeasureId])
    SELECT tbd.MeasureId FROM mea.Measure tbd JOIN [org].[User] u ON tbd.[CreatedBy] = u.[UserID] JOIN [org].[Department] d ON u.[DepartmentID] = d.[DepartmentID] AND d.[CustomerID] IN (SELECT id FROM @CustomerIDTable)
    UNION
    SELECT tbd.MeasureId FROM mea.Measure tbd JOIN [org].[User] u ON tbd.[Responsible] = u.[UserID] JOIN [org].[Department] d ON u.[DepartmentID] = d.[DepartmentID] AND d.[CustomerID] IN (SELECT id FROM @CustomerIDTable)
    UNION
    SELECT tbd.MeasureId FROM mea.Measure tbd JOIN [org].[User] u ON tbd.[ChangedBy] = u.[UserID] JOIN [org].[Department] d ON u.[DepartmentID] = d.[DepartmentID] AND d.[CustomerID] IN (SELECT id FROM @CustomerIDTable)
    UNION 
    SELECT tbd.MeasureId FROM mea.Measure tbd JOIN mea.MeasureTemplate mt ON tbd.MeasureTemplateID = mt.MeasureTemplateID AND mt.CustomerID IN (SELECT id FROM @CustomerIDTable)

    SELECT @maxid = ISNULL(MAX(id),0) FROM @MeasureIDTable
    SELECT @i = ISNULL(MIN(id),0) - 1 FROM @MeasureIDTable
    DECLARE @MeasureID INT
    DECLARE @TempMeasure TABLE (MeasureID INT)
    WHILE (@i < @maxid)
    BEGIN    
        SET @i = @i + 1    
        SELECT @MeasureID = MeasureId FROM @MeasureIDTable WHERE id = @i;
        WITH temp(ID) AS 
        (
            SELECT @MeasureID
            UNION ALL
            SELECT m.MeasureId FROM mea.Measure m JOIN temp ON m.ParentId = temp.ID OR m.CopyFrom = temp.ID
        )
        INSERT INTO @TempMeasure SELECT ID FROM temp;

        DELETE cl FROM mea.CheckListItem cl JOIN @TempMeasure t ON cl.MeasureId = t.MeasureID
        DELETE m FROM mea.Measure m JOIN @TempMeasure t ON m.MeasureId = t.MeasureID
        DELETE FROM @TempMeasure
    END

    DELETE tbl FROM mea.CheckListItemTemplate tbl JOIN mea.MeasureTemplate mt ON tbl.MeasureTemplateID = mt.MeasureTemplateID AND mt.CustomerID IN (SELECT id FROM @CustomerIDTable)
    DELETE tbl FROM mea.MeasureTemplate tbl WHERE tbl.CustomerID IN (SELECT id FROM @CustomerIDTable)
    DELETE FROM @MeasureIDTable    

    RAISERROR ('Deleting Access, LoginService_User, NoteSubscription, User, Event, H_D, Department, Resource, res.Category, res.Provider, AccessGroupMember, Customer', 0, 1)
    DELETE tbd FROM at.access tbd WHERE CustomerId IN (SELECT id FROM @CustomerIDTable)
    DELETE tbd FROM at.access tbd JOIN [org].[H_D] hd ON tbd.[HDID] = hd.[HDID] JOIN [org].[Department] d ON hd.[DepartmentID] = d.[DepartmentID] AND d.[CustomerID] IN (SELECT id FROM @CustomerIDTable)
    DELETE tbd FROM at.access tbd JOIN [org].[Department] d ON tbd.[DepartmentID] = d.[DepartmentID] AND d.[CustomerID] IN (SELECT id FROM @CustomerIDTable)
    DELETE tbd FROM [app].[LoginService_User] tbd JOIN [org].[User] u ON tbd.[UserID] = u.[UserID] JOIN [org].[Department] d ON u.[DepartmentID] = d.[DepartmentID] AND d.[CustomerID] IN (SELECT id FROM @CustomerIDTable)
    DELETE tbd FROM mea.Category tbd JOIN [org].[User] u ON tbd.[CreatedBy] = u.[UserID] JOIN [org].[Department] d ON u.[DepartmentID] = d.[DepartmentID] AND d.[CustomerID] IN (SELECT id FROM @CustomerIDTable)
    DELETE tbd FROM mea.CheckListItem tbd JOIN res.Resource r ON tbd.ResourceID = r.ResourceId JOIN res.[Category] c ON r.[CategoryId] = c.[CategoryId] AND c.[CustomerId] IN (SELECT id FROM @CustomerIDTable)
    DELETE tbd FROM res.Resource tbd JOIN res.[Category] c ON tbd.[CategoryId] = c.[CategoryId] AND c.[CustomerId] IN (SELECT id FROM @CustomerIDTable)
    DELETE tbd FROM res.Resource tbd JOIN [org].[User] u ON tbd.[CreatedBy] = u.[UserID] JOIN [org].[Department] d ON u.[DepartmentID] = d.[DepartmentID] AND d.[CustomerID] IN (SELECT id FROM @CustomerIDTable)        
    DELETE tbd FROM note.NoteSubscription tbd JOIN org.[User] u ON tbd.UserID = u.UserID JOIN [org].[Department] d ON u.[DepartmentID] = d.[DepartmentID] AND d.[CustomerID] IN (SELECT id FROM @CustomerIDTable)    
    
    UPDATE tbd 
    SET tbd.LastUpdatedBy = NULL
    FROM org.Department tbd JOIN org.[user] u ON tbd.LastUpdatedBy = u.UserID 
    JOIN [org].[Department] d1 ON u.[DepartmentID] = d1.[DepartmentID] AND d1.[CustomerID] IN (SELECT id FROM @CustomerIDTable)
    
    DELETE tbd FROM org.[user] tbd JOIN [org].[Department] d ON tbd.[DepartmentID] = d.[DepartmentID] AND d.[CustomerID] IN (SELECT id FROM @CustomerIDTable)
    DELETE tbd FROM log.Event tbd JOIN [org].[Department] d ON tbd.[DepartmentID] = d.[DepartmentID] AND d.[CustomerID] IN (SELECT id FROM @CustomerIDTable)
    DELETE tbd FROM log.Event tbd WHERE CustomerID IN (SELECT id FROM @CustomerIDTable)
    DELETE tbd FROM res.Provider tbd WHERE CustomerId IN (SELECT id FROM @CustomerIDTable)
    DELETE tbd FROM res.Category tbd WHERE CustomerId IN (SELECT id FROM @CustomerIDTable)
    DELETE tbd FROM dbo.AccessGroupMember tbd JOIN [org].[Department] d ON tbd.[DepartmentID] = d.[DepartmentID] AND d.[CustomerID] IN (SELECT id FROM @CustomerIDTable)

    RAISERROR ('Delete all subdepartments based on CustomerIDList', 0, 1)
    INSERT INTO @HierarchyIDTable(id)
    SELECT hd.HDID FROM org.Department d JOIN org.H_D hd ON d.DepartmentID = hd.DepartmentID 
    WHERE d.CustomerID IN (SELECT id FROM @CustomerIDTable)

    DECLARE @Id INT, @ParentID INT
    DECLARE @TempHD TABLE (HDID INT, DepartmentID INT)
    DECLARE Cur CURSOR FOR SELECT h.id FROM @HierarchyIDTable h
    OPEN Cur
    FETCH NEXT FROM Cur INTO @Id 
    WHILE(@@FETCH_STATUS = 0)
    BEGIN
        SELECT @ParentID = hd.ParentID FROM org.H_D hd WHERE hd.HDID = @Id
        -- UPDATE ParentID for Hierachy below without relation to these customers
        UPDATE org.H_D SET ParentID = @ParentID WHERE ParentID = @Id AND DepartmentID NOT IN (SELECT d.DepartmentID FROM org.Department d JOIN org.Customer c ON d.CustomerID = c.CustomerID AND c.CustomerID IN (SELECT id FROM @CustomerIDTable));

        WITH temp(ID) AS 
        (
        SELECT @Id
        UNION ALL
        SELECT hd.HDID FROM org.H_D hd JOIN temp ON hd.ParentID = temp.ID
        )
        INSERT INTO @TempHD (HDID, DepartmentID) SELECT t.ID, h.DepartmentID FROM temp t JOIN org.H_D h ON t.ID = h.HDID;

        DELETE tbd FROM org.H_D tbd JOIN @TempHD t ON tbd.HDID = t.HDID
        
        DELETE tbd FROM at.Batch tbd JOIN [org].[Department] d ON tbd.[DepartmentID] = d.[DepartmentID] JOIN @TempHD t ON tbd.DepartmentID = t.DepartmentID
        DELETE tbd FROM org.DT_UG tbd JOIN [org].UserGroup g ON tbd.UserGroupID = g.UserGroupID JOIN [org].[User] u ON g.[UserID] = u.[UserID] JOIN [org].[Department] d ON u.[DepartmentID] = d.[DepartmentID] JOIN @TempHD t ON t.DepartmentID = d.DepartmentID
        DELETE tbd FROM org.DT_UG tbd JOIN [org].UserGroup g ON tbd.UserGroupID = g.UserGroupID JOIN [org].[Department] d ON g.[DepartmentID] = d.[DepartmentID] JOIN @TempHD t ON t.DepartmentID = d.DepartmentID
        DELETE tbd FROM org.[UGMember] tbd JOIN [org].[User] u ON tbd.[UserID] = u.[UserID] JOIN [org].[Department] d ON u.[DepartmentID] = d.[DepartmentID] JOIN @TempHD t ON t.DepartmentID = d.DepartmentID
        DELETE tbd FROM org.[UGMember] tbd JOIN [org].[UserGroup] ug ON tbd.UserGroupID = ug.UserGroupID JOIN [org].[Department] d ON ug.[DepartmentID] = d.[DepartmentID] JOIN @TempHD t ON t.DepartmentID = d.DepartmentID
        DELETE tbd FROM org.[UGMember] tbd JOIN [org].[UserGroup] ug ON tbd.UserGroupID = ug.UserGroupID JOIN [org].[User] u ON ug.[UserID] = u.[UserID] JOIN [org].[Department] d ON u.[DepartmentID] = d.[DepartmentID] JOIN @TempHD t ON t.DepartmentID = d.DepartmentID
        DELETE tbd FROM org.[UserGroup] tbd JOIN [org].[Department] d ON tbd.[DepartmentID] = d.[DepartmentID] JOIN @TempHD t ON t.DepartmentID = d.DepartmentID
        DELETE tbd FROM org.[UserGroup] tbd JOIN [org].[User] u ON tbd.[UserID] = u.[UserID] JOIN [org].[Department] d ON u.[DepartmentID] = d.[DepartmentID] JOIN @TempHD t ON t.DepartmentID = d.DepartmentID
        DELETE tbd FROM org.U_D tbd JOIN [org].[Department] d ON tbd.[DepartmentID] = d.[DepartmentID] JOIN @TempHD t ON t.DepartmentID = d.DepartmentID
        DELETE tbd FROM org.U_D tbd JOIN [org].[User] u ON tbd.[UserID] = u.[UserID] JOIN [org].[Department] d ON u.[DepartmentID] = d.[DepartmentID] JOIN @TempHD t ON t.DepartmentID = d.DepartmentID
        
        -- Delete all measures related to all subdepartments
        INSERT INTO @MeasureIDTable ([MeasureId])
        SELECT tbd.MeasureId FROM mea.Measure tbd JOIN [org].[User] u ON tbd.[CreatedBy] = u.[UserID] JOIN [org].[Department] d ON u.[DepartmentID] = d.[DepartmentID] JOIN @TempHD t ON t.DepartmentID = d.DepartmentID
        UNION
        SELECT tbd.MeasureId FROM mea.Measure tbd JOIN [org].[User] u ON tbd.[Responsible] = u.[UserID] JOIN [org].[Department] d ON u.[DepartmentID] = d.[DepartmentID] JOIN @TempHD t ON t.DepartmentID = d.DepartmentID
        UNION
        SELECT tbd.MeasureId FROM mea.Measure tbd JOIN [org].[User] u ON tbd.[ChangedBy] = u.[UserID] JOIN [org].[Department] d ON u.[DepartmentID] = d.[DepartmentID] JOIN @TempHD t ON t.DepartmentID = d.DepartmentID
        UNION 
        SELECT tbd.MeasureId FROM mea.Measure tbd JOIN mea.MeasureTemplate mt ON tbd.MeasureTemplateID = mt.MeasureTemplateID JOIN [org].[Department] d ON mt.CustomerID = d.CustomerID JOIN @TempHD t ON t.DepartmentID = d.DepartmentID
        
        SELECT @maxid = ISNULL(MAX(id),0) FROM @MeasureIDTable
        SELECT @i = ISNULL(MIN(id),0) - 1 FROM @MeasureIDTable
        WHILE (@i < @maxid)
        BEGIN    
            SET @i = @i + 1    
            SELECT @MeasureID = MeasureId FROM @MeasureIDTable WHERE id = @i;
            WITH temp(ID) AS 
            (
                SELECT @MeasureID
                UNION ALL
                SELECT m.MeasureId FROM mea.Measure m JOIN temp ON m.ParentId = temp.ID OR m.CopyFrom = temp.ID
            )
            INSERT INTO @TempMeasure SELECT ID FROM temp;

            DELETE cl FROM mea.CheckListItem cl JOIN @TempMeasure t ON cl.MeasureId = t.MeasureID
            DELETE m FROM mea.Measure m JOIN @TempMeasure t ON m.MeasureId = t.MeasureID    
            DELETE FROM @TempMeasure    
        END

        DELETE tbl FROM mea.CheckListItemTemplate tbl JOIN mea.MeasureTemplate mt ON tbl.MeasureTemplateID = mt.MeasureTemplateID JOIN [org].[Department] d ON mt.CustomerID = d.CustomerID JOIN @TempHD t ON t.DepartmentID = d.DepartmentID
        DELETE tbl FROM mea.MeasureTemplate tbl JOIN [org].[Department] d ON tbl.CustomerID = d.CustomerID JOIN @TempHD t ON t.DepartmentID = d.DepartmentID
        DELETE FROM @MeasureIDTable       
        -- End (Delete all measures related to all subdepartments)

        DELETE tbd FROM at.access tbd JOIN [org].[H_D] hd ON tbd.[HDID] = hd.[HDID] JOIN [org].[Department] d ON hd.[DepartmentID] = d.[DepartmentID] JOIN @TempHD t ON t.DepartmentID = d.DepartmentID
        DELETE tbd FROM at.access tbd JOIN [org].[Department] d ON tbd.[DepartmentID] = d.[DepartmentID] JOIN @TempHD t ON t.DepartmentID = d.DepartmentID
        DELETE tbd FROM [app].[LoginService_User] tbd JOIN [org].[User] u ON tbd.[UserID] = u.[UserID] JOIN [org].[Department] d ON u.[DepartmentID] = d.[DepartmentID] JOIN @TempHD t ON t.DepartmentID = d.DepartmentID
        DELETE tbd FROM mea.Category tbd JOIN [org].[User] u ON tbd.[CreatedBy] = u.[UserID] JOIN [org].[Department] d ON u.[DepartmentID] = d.[DepartmentID] JOIN @TempHD t ON t.DepartmentID = d.DepartmentID
        DELETE tbd FROM res.Resource tbd JOIN [org].[User] u ON tbd.[CreatedBy] = u.[UserID] JOIN [org].[Department] d ON u.[DepartmentID] = d.[DepartmentID] JOIN @TempHD t ON t.DepartmentID = d.DepartmentID    
        DELETE tbd FROM note.NoteSubscription tbd JOIN org.[User] u ON tbd.UserID = u.UserID JOIN [org].[Department] d ON u.[DepartmentID] = d.[DepartmentID] JOIN @TempHD t ON t.DepartmentID = d.DepartmentID    
        
        UPDATE tbd 
        SET tbd.LastUpdatedBy = NULL
        FROM org.Department tbd JOIN org.[user] u ON tbd.LastUpdatedBy = u.UserID 
        JOIN [org].[Department] d1 ON u.[DepartmentID] = d1.[DepartmentID] AND d1.[CustomerID] IN (SELECT id FROM @CustomerIDTable)
        
        DELETE tbd FROM org.[user] tbd JOIN [org].[Department] d ON tbd.[DepartmentID] = d.[DepartmentID] JOIN @TempHD t ON t.DepartmentID = d.DepartmentID
        DELETE tbd FROM log.Event tbd JOIN [org].[Department] d ON tbd.[DepartmentID] = d.[DepartmentID] JOIN @TempHD t ON t.DepartmentID = d.DepartmentID
        DELETE tbd FROM dbo.AccessGroupMember tbd JOIN [org].[Department] d ON tbd.[DepartmentID] = d.[DepartmentID] JOIN @TempHD t ON tbd.DepartmentID = t.DepartmentID 

        DELETE tbd FROM org.Department tbd JOIN @TempHD t ON tbd.DepartmentID = t.DepartmentID 
        FETCH NEXT FROM Cur INTO @Id 
    END
    CLOSE Cur
    DEALLOCATE Cur

    DELETE tbd FROM AccessGroupMember tbd JOIN org.Customer c ON tbd.CustomerID = c.CustomerID WHERE c.CustomerID IN (SELECT id FROM @CustomerIDTable)
    DELETE tbd FROM prop.PropValue tbd JOIN prop.PropFile pf ON tbd.PropFileID = pf.PropFileId AND pf.[CustomerID] IN (SELECT id FROM @CustomerIDTable)
    DELETE tbd FROM prop.PropFile tbd WHERE CustomerID IN (SELECT id FROM @CustomerIDTable)
    DELETE tbd FROM org.Customer tbd WHERE CustomerID IN (SELECT id FROM @CustomerIDTable)

     -- Update Department 25 which has Customer 'N-Norge'
    -- UPDATE org.Department SET CustomerID = NULL WHERE DepartmentID = 25
    
    DECLARE @ReportDBTable TABLE (ReportServer nvarchar(max), ReportDB nvarchar(max))
    INSERT INTO @ReportDBTable ([ReportServer], [ReportDB]) SELECT DISTINCT s.[ReportServer], s.[ReportDB] FROM [at].[Survey] s

    DECLARE c_Database CURSOR LOCAL FAST_FORWARD READ_ONLY FOR
    SELECT [ReportServer], [ReportDB] FROM @ReportDBTable

    DECLARE @ReportServer nvarchar(max), @ReportDB nvarchar(max), @sqlCommand nvarchar(max), @Msg nvarchar(max), @LinkedDB NVARCHAR(MAX) = ' '

    OPEN c_Database
    FETCH NEXT FROM c_Database INTO @ReportServer, @ReportDB
    WHILE @@FETCH_STATUS = 0
    BEGIN
        IF (@ReportServer = @@servername AND @ReportDB = DB_NAME())
        BEGIN
            SET @LinkedDB = ' '
            SET @Msg = N'Deleting results in database [' + @ReportServer + '].[' + @ReportDB + ']'
        END
        ELSE 
        BEGIN
            SET @LinkedDB = '[' + @ReportServer + '].[' + @ReportDB + '].'
            SET @Msg = N'Deleting results in database ' + @LinkedDB
        END
        
        RAISERROR (@Msg, 0, 1)
        SET @sqlCommand = N'DELETE tbd FROM ' + @LinkedDB +'dbo.Result tbd
                            WHERE NOT EXISTS (SELECT 1 FROM at.Batch b WHERE tbd.BatchID = b.BatchID)
                               OR NOT EXISTS (SELECT 1 FROM org.Department d WHERE tbd.DepartmentID = d.DepartmentID)'
        EXEC sp_executesql @sqlCommand

        FETCH NEXT FROM c_Database INTO @ReportServer, @ReportDB
    END
    CLOSE c_Database
    DEALLOCATE c_Database

    COMMIT TRANSACTION
    RAISERROR ('Completed', 0, 1)
END

GO
