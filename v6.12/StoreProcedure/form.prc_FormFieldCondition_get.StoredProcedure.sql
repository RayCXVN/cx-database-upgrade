SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[form].[prc_FormFieldCondition_get]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [form].[prc_FormFieldCondition_get] AS' 
END
GO

ALTER PROCEDURE [form].[prc_FormFieldCondition_get] @FormFieldID INT
AS
BEGIN
	SET NOCOUNT ON;

	DECLARE @Err INT

	SELECT [FormFieldConditionID]
		,[FormFieldID]
		,[Type]
		,[Param]
		,[Value]
		,[Created]
	FROM [form].[FormFieldCondition]
	WHERE [FormFieldID] = @FormFieldID

	SET @Err = @@Error

	RETURN @Err
END

GO
