SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[app].[prc_LT_Site_ins]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [app].[prc_LT_Site_ins] AS' 
END
GO
ALTER PROCEDURE [app].[prc_LT_Site_ins]  
 @LanguageID int,  
 @SiteID int,  
 @Name nvarchar(256),  
 @Description nvarchar(max),  
 @LoginText nvarchar(max),  
 @FooterText nvarchar(max),  
 @SupportText nvarchar(max),  
 @ClosedText nvarchar(max),  
 @ExtraFooterTextAfterAuthentication nvarchar(max),  
 @HeadData nvarchar(max), 
 @cUserid int,  
 @Log smallint = 1,
 @BaseURL nvarchar(256)='',
 @ShortName nvarchar(256)='',
 @ProductURL			NVARCHAR(max),
 @ProductURLTitle		NVARCHAR(max),
 @BlogURL				NVARCHAR(max),
 @BlogURLTitle			NVARCHAR(max),
 @BodyData			    NVARCHAR(max) ='' 
AS  
BEGIN  
 SET NOCOUNT ON;  
 DECLARE @Err Int  
      
    INSERT INTO [app].[LT_Site]  
           ([LanguageID]  
           ,[SiteID]  
           ,[Name]  
           ,[Description]  
           ,[LoginText]  
           ,[FooterText]  
           ,[SupportText]  
           ,[ClosedText]  
           ,[ExtraFooterTextAfterAuthentication]
           ,[HeadData]
		   ,[BaseURL]
		   ,[ShortName]
		   ,[ProductURL]		
		   ,[ProductURLTitle]	
		   ,[BlogURL]			
		   ,[BlogURLTitle]
		   ,[BodyData]	)  
    VALUES  
           (@LanguageID  
           ,@SiteID  
           ,@Name  
           ,@Description  
           ,@LoginText  
           ,@FooterText  
           ,@SupportText  
           ,@ClosedText  
           ,@ExtraFooterTextAfterAuthentication
           ,@HeadData
		   ,@BaseURL
		   ,@ShortName
		   ,@ProductURL
		   ,@ProductURLTitle
		   ,@BlogURL 
           ,@BlogURLTitle
		   ,@BodyData )
    Set @Err = @@Error  
 IF @Log = 1
 BEGIN
  INSERT INTO [Log].[AuditLog] ( UserId, TableName, Type, Data, Created)   
  SELECT @cUserid,'LT_Site',0,  
  ( SELECT * FROM [app].[LT_Site]  
   WHERE
   [LanguageID] = @LanguageID AND  
   [SiteID] = @SiteID     FOR XML AUTO) as data,  
    getdate()
 END
   
 RETURN @Err   
END

GO
