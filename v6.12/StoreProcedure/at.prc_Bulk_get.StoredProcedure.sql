SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[at].[prc_Bulk_get]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [at].[prc_Bulk_get] AS' 
END
GO
ALTER PROCEDURE [at].[prc_Bulk_get]
(
	@BulkGroupID int
)
AS
BEGIN
	SET NOCOUNT ON
	DECLARE @Err Int

	SELECT
	[BulkID],
	ISNULL([BulkGroupID], 0) AS 'BulkGroupID',
	ISNULL([No], 0) AS 'No',
	ISNULL([Css], '') AS 'Css',
	ISNULL([Icon], '') AS 'Icon',
	ISNULL([Tag], '') AS 'Tag'
	FROM [at].[Bulk]
	WHERE
	[BulkGroupID] = @BulkGroupID
	ORDER BY [No]

	Set @Err = @@Error

	RETURN @Err
END



GO
