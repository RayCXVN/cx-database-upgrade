SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[res].[prc_Status_ins]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [res].[prc_Status_ins] AS' 
END
GO
ALTER PROCEDURE [res].[prc_Status_ins]    
(
	@StatusID int = null output,
	@No int,
	@cUserid int,  
	@Log smallint = 1
)
AS    
BEGIN    
 SET NOCOUNT ON;    
 DECLARE @Err Int    

 INSERT INTO [res].[Status]
    ([No]
	)
 VALUES
	(@No
	)
 
 Set @Err = @@Error  
 Set @StatusID = scope_identity()  
  
 IF @Log = 1   
 BEGIN   
  INSERT INTO [Log].[AuditLog] ( UserId, TableName, Type, Data, Created)   
  SELECT @cUserid,'res.Status',0,  
  ( SELECT * FROM [res].[Status]   
   WHERE  
   [StatusID] = @StatusID     FOR XML AUTO) as data,  
    getdate()   
  END  
  
 RETURN @Err  

END 

GO
