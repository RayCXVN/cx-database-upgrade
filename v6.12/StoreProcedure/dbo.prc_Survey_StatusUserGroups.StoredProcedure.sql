SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[prc_Survey_StatusUserGroups]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [dbo].[prc_Survey_StatusUserGroups] AS' 
END
GO
--prc_Survey_StatusRapport 7, '592,594'
ALTER proc [dbo].[prc_Survey_StatusUserGroups]
(
    @SurveyID AS INT,
    @DepID AS INT
)
AS
BEGIN
DECLARE @ActiveEntityStatusID INT = (SELECT EntityStatusID FROM EntityStatus WHERE CodeName='Active')

SELECT COUNT(*), COUNT(enddate), usergroupid
FROM dbo.Result
WHERE surveyid = @SurveyID AND departmentid = @DepID AND EntityStatusID = @ActiveEntityStatusID AND Deleted IS NULL
GROUP BY usergroupid

END
---------------------------------------------------------------------


GO
