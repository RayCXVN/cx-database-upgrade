SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[prc_H_D_get_single]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [dbo].[prc_H_D_get_single] AS' 
END
GO
ALTER PROCEDURE [dbo].[prc_H_D_get_single]
(
	@HDID		int 
)
As
BEGIN
	SET NOCOUNT ON
	DECLARE @Err Int
    DECLARE @ActiveEntityStatusID INT = (SELECT EntityStatusID FROM EntityStatus WHERE CodeName='Active')
	
		Select
		hd.[HDID],
		hd.[HierarchyID],
		hd.[DepartmentID],
		ISNULL([ParentID], 0) ParentID,
		hd.[Path],
		hd.[PathName],
		hd.[Created]
		FROM [H_D] hd
		join department d  on d.departmentid = hd.departmentid
		WHERE hd.[HDID] = @HDID AND hd.[Deleted] = 0 and d.EntityStatusID = @ActiveEntityStatusID

	Set @Err = @@Error

	RETURN @Err
End

GO
