SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[at].[prc_A_VT_ins]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [at].[prc_A_VT_ins] AS' 
END
GO
ALTER PROCEDURE [at].[prc_A_VT_ins]
(
	@AVTID int = null output,
	@ViewTypeID int,
	@ActivityID int,
	@No smallint,
	@Type smallint,
	@Template varchar(64),
	@ShowPercentage BIT,
	@ActivityViewID int=null,
	@LevelGroupID int=null,
	@CssClass nvarchar(64)='',
	@cUserid int,
	@Log smallint = 1
)
AS
BEGIN
	SET NOCOUNT ON
	DECLARE @Err Int

	INSERT INTO [at].[A_VT]
	(
		[ViewTypeID],
		[ActivityID],
		[No],
		[Type],
		[Template],
		[ShowPercentage],
		[ActivityViewID],
		[LevelGroupID],
		[CssClass]
	)
	VALUES
	(
		@ViewTypeID,
		@ActivityID,
		@No,
		@Type,
		@Template,
		@ShowPercentage,
		@ActivityViewID,
		@LevelGroupID,
		@CssClass
	)

	Set @Err = @@Error
	Set @AVTID = scope_identity()

	IF @Log = 1 
	BEGIN 
		INSERT INTO [Log].[AuditLog] ( UserId, TableName, Type, Data, Created) 
		SELECT @cUserid,'A_VT',0,
		( SELECT * FROM [at].[A_VT] 
			WHERE
			[AVTID] = @AVTID				 FOR XML AUTO) as data,
				getdate() 
	 END

	RETURN @Err
END

GO
