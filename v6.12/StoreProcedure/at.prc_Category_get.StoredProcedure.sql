SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[at].[prc_Category_get]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [at].[prc_Category_get] AS' 
END
GO
---

ALTER PROCEDURE [at].[prc_Category_get]    
(    
 @ActivityID int,    
 @ParentID int = 0    
)    
AS    
BEGIN    
 SET NOCOUNT ON    
 DECLARE @Err Int    
    
 IF @ParentID is null    
    Begin    
      Set @ParentID = 0    
 End    
    
 SELECT    
 [CategoryID],    
 [ActivityID],    
 ISNULL([ParentID], 0) AS 'ParentID',    
 [Tag],    
 [Type],    
 [No],    
 [Fillcolor],    
 [BorderColor],    
 [Weight],    
 [NType],    
 [ProcessCategory],    
 [ExtID],  
 [Created],
 [AnswerItemID],
 [RenderAsDefault],
 [AvgType]   
 FROM [at].[Category]    
 WHERE    
 [ActivityID] = @ActivityID AND ISNULL([ParentID],0) = @ParentID    
 ORDER BY [No]    
    
 Set @Err = @@Error    
    
 RETURN @Err    
END    

GO
