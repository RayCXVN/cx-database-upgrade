SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[exp].[prc_exType_del]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [exp].[prc_exType_del] AS' 
END
GO


ALTER PROCEDURE [exp].[prc_exType_del]
(
	@TypeID smallint,
	@cUserid int,
	@Log smallint = 1
)
AS
BEGIN
	SET NOCOUNT ON
	DECLARE @Err Int

	IF @Log = 1 
	BEGIN 
		INSERT INTO [Log].[AuditLog] ( UserId, TableName, Type, Data, Created) 
		SELECT @cUserid,'exType',2,
		( SELECT * FROM [exp].[exType] 
			WHERE
			[TypeID] = @TypeID
			 FOR XML AUTO) as data,
			getdate() 
	END 


	DELETE FROM [exp].[exType]
	WHERE
		[TypeID] = @TypeID

	Set @Err = @@Error

	RETURN @Err
END


GO
