SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[mea].[prc_SearchMeasureByCreatedOrResponsible]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [mea].[prc_SearchMeasureByCreatedOrResponsible] AS' 
END
GO
/*
    Steve November 14 2016 - Search activities in Competence No - Add more param @MeasureTypeIDs
    Steve November 22 2016 - Search activities in Competence No - Correct 'AND us.EntityStatusID = @ActiveEntityStatusID'
    Ray - 2017-02-09 - Remove cross database reference for Azure compatible
*/
ALTER PROCEDURE [mea].[prc_SearchMeasureByCreatedOrResponsible]
(
    @UserID			INT,
    @SearchString	nvarchar(max),
    @RowCount		int output,
    @IsSubject		BIT =0,
    @IsResponsible	BIT=0,
    @PageIndex		INT=1,
    @PageSize		INT =10,
    @MeasureTypeIDs nvarchar(max) = '1'
)
AS
BEGIN
    DECLARE @ActiveEntityStatusID INT = (SELECT EntityStatusID FROM EntityStatus WHERE CodeName='Active')
    DECLARE @TableMeasureType TABLE(MeasureTypeID INT)
    INSERT INTO @TableMeasureType(MeasureTypeID) SELECT Value FROM dbo.funcListToTableInt(@MeasureTypeIDs, ',')

	    ;WITH RecQry AS
				    (
					    SELECT m.MeasureId 
						 FROM mea.Measure m JOIN @TableMeasureType mt ON m.MeasureTypeId = mt.MeasureTypeID
						WHERE( m.CreatedBy =@UserID OR m.Responsible = @UserID)  AND Active =1 --AND Deleted =0
					    UNION ALL
					    SELECT a.MeasureId
						 FROM mea.Measure a INNER JOIN RecQry b
						    ON a.ParentID = b.MeasureId
				    )
    Select ms.MeasureId,ms.MeasureTypeId, ms.ParentId,ms.CreatedBy,ms.CreatedDate,ms.ChangedBy, ms.ChangedDate, ms.Responsible, ms.StatusId, ms.[Private], ms.[Subject], ms.[Description],ms.[Started], ms.[DueDate], ms.[Completed], ms.CopyFrom, ms.Active, ms.Deleted, us.FirstName,us.LastName
    INTO #SearchResult
    FROM mea.Measure ms
	    INNER JOIN org.[User] us ON us.UserID = ms.Responsible
    WHERE  EXISTS (SELECT 1 FROM RecQry WHERE MeasureId= ms.MeasureId) AND ms.Deleted =0
    AND 
    (
    ((@IsSubject=1 AND @IsResponsible=0) AND ms.[Subject] LIKE '%'+@SearchString+'%')
	    OR
    ((@IsSubject=0 AND @IsResponsible=1) AND (us.LastName LIKE '%'+@SearchString+'%' OR us.FirstName LIKE '%'+@SearchString+'%') AND us.EntityStatusID = @ActiveEntityStatusID AND us.Deleted IS NULL)
	    OR
    ((@IsSubject=0 AND @IsResponsible=0) AND (us.LastName LIKE '%'+@SearchString+'%' OR us.FirstName LIKE '%'+@SearchString+'%' OR ms.[Subject] LIKE '%'+@SearchString+'%') AND us.EntityStatusID = @ActiveEntityStatusID AND us.Deleted IS NULL)
    OR EXISTS (SELECT 1 FROM org.[User] us1 WHERE ms.CreatedBy = us1.UserID AND (us1.LastName LIKE '%'+@SearchString+'%' OR us1.FirstName LIKE '%'+@SearchString+'%') AND us1.EntityStatusID = @ActiveEntityStatusID AND us.Deleted IS NULL)
    );

    SELECT @RowCount=Count(MeasureId) FROM #SearchResult
    SELECT r.* FROM
    (SELECT row_number() OVER (ORDER BY t.[CreatedDate] DESC) AS RowNum, t.* FROM #SearchResult t) r
    WHERE r.RowNum between (@PageIndex - 1) * @PageSize + 1 and @PageIndex*@PageSize

    DROP TABLE #SearchResult
END

GO
