SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[log].[prc_GetTopDisctinctEvent_ByEventType]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [log].[prc_GetTopDisctinctEvent_ByEventType] AS' 
END
GO
/*
    Created By:     Sarah
    Created Date:   Aug 19, 2016
    Description:    Task #60464: [VOKAL2][Landing page][Code] Write event logs for recent student

    EXEC [log].[prc_GetTopDisctinctEvent_ByEventType] @UserID=17225, @ItemId=80202, @EventTypeID = 1043, @NumberOfRow = 10
*/

ALTER PROCEDURE [log].[prc_GetTopDisctinctEvent_ByEventType]
    @EventTypeID    nvarchar(128)	= '13,14',
    @HDID           int = 0,
    @FromDate       date = NULL,
    @ToDate         date = NULL,
    @ItemId			INT = 0,
    @UserID         INT = 0,
    @NumberOfRow	INT = 10
AS
BEGIN
    DECLARE @ActiveEntityStatusID INT = (SELECT EntityStatusID FROM EntityStatus WHERE CodeName='Active')

	;WITH EventEntries AS 
	(
		SELECT ROW_NUMBER() OVER (PARTITION BY e.ItemID, e.UserID ORDER BY e.Created DESC) AS [Row], 
              e.EventID, e.ItemID, CASE WHEN u.EntityStatusID = @ActiveEntityStatusID THEN 0 ELSE 1 END AS [DeletedUser], e.Created
		FROM [log].[Event] e WITH (NOLOCK)
		    INNER JOIN org.H_D hd WITH (NOLOCK) ON hd.DepartmentID = e.DepartmentID 
		    AND (@HDID = 0 OR hd.[Path] LIKE '%\' + CONVERT(nvarchar(16),@HDID) + '\%')
		    AND  e.EventTypeID IN (SELECT value FROM dbo.funcListToTableInt(@EventTypeID,','))
            AND (CAST(e.Created AS DATE) >= @FromDate OR @FromDate IS NULL)
            AND (CAST(e.Created AS DATE) <= @ToDate OR @ToDate IS NULL)
            AND (@ItemId = 0 OR e.ItemID= @ItemId)
            AND (@UserID = 0 OR e.UserID = @UserID)
            LEFT JOIN org.[User] u WITH (NOLOCK) ON e.UserID = u.UserID
        --ORDER BY e.Created DESC
	)
	SELECT TOP (@NumberOfRow) EE.EventID, E.EventTypeID, E.UserID, E.IPNumber, E.TableTypeID, E.ItemID, E.Created, E.ApplicationName, E.UserData, E.DepartmentID, EE.[DeletedUser]
	FROM EventEntries EE
        INNER JOIN [log].[Event] E WITH (NOLOCK) ON EE.EventID = E.EventID
    WHERE EE.[Row] = 1
    ORDER BY e.Created DESC
END
GO
