SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[org].[prc_Owner_upd]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [org].[prc_Owner_upd] AS' 
END
GO


ALTER PROCEDURE [org].[prc_Owner_upd]
(
	@OwnerID int,
	@LanguageID int,
	@Name nvarchar(256),
	@ReportServer varchar(64),
	@ReportDB varchar(64),
	@MainHierarchyID INT=NULL,
	@OLAPServer varchar(64),
	@OLAPDB varchar(64),
	@Css nvarchar(128),
	@Url nvarchar(128),
	@LoginType int,
	@Prefix nvarchar(8),
	@Description nvarchar(max),
	@Logging smallint,
	@cUserid int,
	@Log smallint = 1,
	@OTPLength		INT =5,
	@OTPCharacters nvarchar(512) = '',
	@OTPAllowLowercase BIT =1,
	@OTPAllowUppercase BIT = 1,
	@UseOTPCaseSensitive BIT =0,
	@UseHashPassword smallint =0,
	@UseOTP bit =1,
	@DefaultHashMethod int =2,
	@OTPDuration int =10
	
)
AS
BEGIN
	SET NOCOUNT ON
	DECLARE @Err Int

	UPDATE [org].[Owner]
	SET
		[LanguageID] = @LanguageID,
		[Name] = @Name,
		[ReportServer] = @ReportServer,
		[ReportDB] = @ReportDB,
		[MainHierarchyID] = @MainHierarchyID,
		[OLAPServer] = @OLAPServer,
		[OLAPDB] = @OLAPDB,
		[Css] = @Css,
		[Url] = @Url,
		[LoginType] = @LoginType,
		[Prefix] = @Prefix,
		[Description] = @Description,
		[Logging] = @Logging,
		OTPLength =@OTPLength,
		OTPCharacters=@OTPCharacters,
		OTPAllowLowercase=@OTPAllowLowercase,
		OTPAllowUppercase=@OTPAllowUppercase,
		UseOTPCaseSensitive=@UseOTPCaseSensitive,
		UseHashPassword=@UseHashPassword,
		UseOTP=@UseOTP,
		DefaultHashMethod=@DefaultHashMethod,
		OTPDuration=@OTPDuration
		
	WHERE
		[OwnerID] = @OwnerID

	IF @Log = 1 
	BEGIN 
		INSERT INTO [Log].[AuditLog] ( UserId, TableName, Type, Data, Created) 
		SELECT @cUserid,'Owner',1,
		( SELECT * FROM [org].[Owner] 
			WHERE
			[OwnerID] = @OwnerID			 FOR XML AUTO) as data,
			getdate()
	END

	Set @Err = @@Error

	RETURN @Err
END

GO
