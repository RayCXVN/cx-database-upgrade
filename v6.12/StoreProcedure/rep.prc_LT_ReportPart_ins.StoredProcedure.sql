SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[rep].[prc_LT_ReportPart_ins]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [rep].[prc_LT_ReportPart_ins] AS' 
END
GO


ALTER PROCEDURE [rep].[prc_LT_ReportPart_ins]
(
	@LanguageID int,
	@ReportPartID int,
	@Text nvarchar(max),
	@Title nvarchar(256) = '',
	@cUserid int,
	@Log smallint = 1
)
AS
BEGIN
	SET NOCOUNT ON
	DECLARE @Err Int

	INSERT INTO [rep].[LT_ReportPart]
	(
		[LanguageID],
		[ReportPartID],
		[Text],
		[Title]
	)
	VALUES
	(
		@LanguageID,
		@ReportPartID,
		@Text,
		@Title
	)

	Set @Err = @@Error

	IF @Log = 1 
	BEGIN 
		INSERT INTO [Log].[AuditLog] ( UserId, TableName, Type, Data, Created) 
		SELECT @cUserid,'LT_ReportPart',0,
		( SELECT * FROM [rep].[LT_ReportPart] 
			WHERE
			[LanguageID] = @LanguageID AND
			[ReportPartID] = @ReportPartID				 FOR XML AUTO) as data,
				getdate() 
	 END

	RETURN @Err
END



GO
