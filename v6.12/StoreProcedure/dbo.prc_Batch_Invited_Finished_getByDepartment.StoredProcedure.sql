SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[prc_Batch_Invited_Finished_getByDepartment]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [dbo].[prc_Batch_Invited_Finished_getByDepartment] AS' 
END
GO
/*
	2016-12-06 Sarah - Update for add new EntityStatusID column in Result table
    2017-02-13 Ray   - Count number of started results
*/

--EXEC [dbo].[prc_Batch_Invited_Finished_getByDepartment] 328,19
ALTER PROC [dbo].[prc_Batch_Invited_Finished_getByDepartment]
(
	@SurveyID	INT,
	@DepartmentID INT
)
As
BEGIN
	SET NOCOUNT ON
	DECLARE @Err INT

	DECLARE @EntityStatus_Active INT 
	SELECT @EntityStatus_Active = E.EntityStatusID FROM EntityStatus E WHERE E.CodeName = 'Active'

	SELECT
	r.BatchID,
	COUNT(*) AS Invited,
    COUNT(r.EndDate) AS Finished,
	COUNT(r.StartDate) AS [Started]
	FROM [Result] r
	WHERE r.[SurveyID] = @SurveyID AND r.[DepartmentID] = @DepartmentID
		AND r.EntityStatusID = @EntityStatus_Active AND r.Deleted IS NULL
    GROUP BY r.BatchID

	SET @Err = @@Error

	RETURN @Err
END

GO
