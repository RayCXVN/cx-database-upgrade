SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[at].[prc_Mail_upd]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [at].[prc_Mail_upd] AS' 
END
GO

ALTER PROCEDURE [at].[prc_Mail_upd]
(
	@MailID int,
	@SurveyID int,
	@BatchID INT=NULL,
	@Type smallint,
	@From nvarchar(256),
	@Bcc nvarchar(max),
	@StatusMail nvarchar(max),
	@cUserid int,
	@Log smallint = 1
)
AS
BEGIN
	SET NOCOUNT ON
	DECLARE @Err Int

	UPDATE [at].[Mail]
	SET
		[SurveyID] = @SurveyID,
		[BatchID] = @BatchID,
		[Type] = @Type,
		[From] = @From,
		[Bcc] = @Bcc,
		[StatusMail] = @StatusMail
	WHERE
		[MailID] = @MailID

	IF @Log = 1 
	BEGIN 
		INSERT INTO [Log].[AuditLog] ( UserId, TableName, Type, Data, Created) 
		SELECT @cUserid,'Mail',1,
		( SELECT * FROM [at].[Mail] 
			WHERE
			[MailID] = @MailID			 FOR XML AUTO) as data,
			getdate()
	END

	Set @Err = @@Error

	RETURN @Err
END


GO
