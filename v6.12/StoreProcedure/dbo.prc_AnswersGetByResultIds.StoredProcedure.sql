SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[prc_AnswersGetByResultIds]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [dbo].[prc_AnswersGetByResultIds] AS' 
END
GO
/*
	Improve performance for external data import
    Johnny - Des 07 2016 - Update store procedure for dynamic SQL
*/
ALTER PROCEDURE [dbo].[prc_AnswersGetByResultIds]
 @ResultIds NVARCHAR(MAX), 
 @SurveyId INT
AS
BEGIN
 DECLARE @ReportServer NVARCHAR(64),@ReportDB NVARCHAR(64)
 SELECT @ReportServer=ReportServer,@ReportDB=ReportDB FROM at.Survey
 WHERE SurveyID=@SurveyId
 DECLARE @LinkedDB NVARCHAR(MAX)
	IF @ReportDB=DB_NAME() AND @ReportServer=@@servername
		BEGIN
		SET @LinkedDB=' '
		END
	ELSE
		BEGIN
		SET @LinkedDB= '['+ @ReportServer + '].['+ @ReportDB + '].'
		END
 DECLARE @sqlCommand NVARCHAR(max),@sqlParameters NVARCHAR(Max)
 SET @sqlCommand='SELECT a.AnswerID, a.ItemID, a.ResultID, a.QuestionID, a.AlternativeID, a.Value, a.DateValue, a.Free, a.No'+ 
  ' FROM '+ @LinkedDB +'dbo.Answer a'+
     ' WHERE a.ResultID IN ('+@ResultIds+')'     
 SET @sqlParameters = N'@p_SurveyID int'
 EXECUTE sp_executesql @sqlCommand, @sqlParameters, @p_SurveyID = @SurveyId
END

GO
