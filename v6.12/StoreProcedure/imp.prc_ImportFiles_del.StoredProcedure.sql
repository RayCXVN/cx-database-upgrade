SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[imp].[prc_ImportFiles_del]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [imp].[prc_ImportFiles_del] AS' 
END
GO


ALTER PROCEDURE [imp].[prc_ImportFiles_del]
(
	@ImportFileID int,
	@cUserid int,
	@Log smallint = 1
)
AS
BEGIN
	SET NOCOUNT ON
	DECLARE @Err Int

	IF @Log = 1 
	BEGIN 
		INSERT INTO [Log].[AuditLog] ( UserId, TableName, Type, Data, Created) 
		SELECT @cUserid,'ImportFiles',2,
		( SELECT * FROM [imp].[ImportFiles] 
			WHERE
			[ImportFileID] = @ImportFileID
			 FOR XML AUTO) as data,
			getdate() 
	END 


	DELETE FROM [imp].[ImportFiles]
	WHERE
		[ImportFileID] = @ImportFileID

	Set @Err = @@Error

	RETURN @Err
END


GO
