SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[note].[prc_NoteType_ins]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [note].[prc_NoteType_ins] AS' 
END
GO
ALTER PROCEDURE [note].[prc_NoteType_ins]
(
	@NoteTypeID int = null output,
	@OwnerID int,
	@Name nvarchar(128),
	@UseVersioning bit,
	@UseReadLog bit,
	@UseHTML bit,
	@UseEncryption bit,
	@cUserid int,
	@Log smallint = 1
)
AS
BEGIN
	SET NOCOUNT ON
	DECLARE @Err Int

	INSERT INTO [note].[NoteType]
	(
		[OwnerID],
		[Name],
		[UseVersioning],
		[UseReadLog],
		[UseHTML],
		[UseEncryption]
	)
	VALUES
	(
		@OwnerID,
		@Name,
		@UseVersioning,
		@UseReadLog,
		@UseHTML,
		@UseEncryption
	)

	Set @Err = @@Error
	Set @NoteTypeID = scope_identity()

	IF @Log = 1 
	BEGIN 
		INSERT INTO [Log].[AuditLog] ( UserId, TableName, Type, Data, Created) 
		SELECT @cUserid,'NoteType',0,
		( SELECT * FROM [note].[NoteType] 
			WHERE
			[NoteTypeID] = @NoteTypeID FOR XML AUTO) as data,
				getdate() 
	 END

	RETURN @Err
END

GO
