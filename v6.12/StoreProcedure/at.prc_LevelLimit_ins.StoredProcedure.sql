SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[at].[prc_LevelLimit_ins]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [at].[prc_LevelLimit_ins] AS' 
END
GO
/*
	2017-06-13 Steve: Change the length of column ExtID in all related tables from xAPI format
*/
ALTER PROCEDURE [at].[prc_LevelLimit_ins]  
(  
 @LevelLimitID int = null output,  
 @LevelGroupID int = null,  
 @CategoryID int = null,  
 @QuestionID int = null,  
 @AlternativeID int = null,  
 @MinValue float,  
 @MaxValue float,  
 @SigChange float,  
 @OwnerColorID int,  
 @NegativeTrend bit = 0,  
 @cUserid int,  
 @Log smallint = 1,
 @ItemID int = NULL,
 @ExtID nvarchar(256)='',
 @MatchingType int = 0
)  
AS  
BEGIN  
 SET NOCOUNT ON  
 DECLARE @Err Int  
  
 INSERT INTO [at].[LevelLimit]  
 (  
  [LevelGroupID],  
  [CategoryID],  
  [QuestionID],  
  [AlternativeID],  
  [MinValue],  
  [MaxValue],  
  [Sigchange],  
  [OwnerColorID],  
  [NegativeTrend],
  [ItemID],
  [ExtID],
  [MatchingType]  
 )  
 VALUES  
 (  
  @LevelGroupID,  
  @CategoryID,  
  @QuestionID,  
  @AlternativeID,  
  @MinValue,  
  @MaxValue,  
  @SigChange,  
  @OwnerColorID ,  
  @NegativeTrend,
  @ItemID,
  @ExtID,
  @MatchingType  
 )  
  
 Set @Err = @@Error  
 Set @LevelLimitID = scope_identity()  
  
 IF @Log = 1   
 BEGIN   
  INSERT INTO [Log].[AuditLog] ( UserId, TableName, Type, Data, Created)   
  SELECT @cUserid,'LevelLimit',0,  
  ( SELECT * FROM [at].[LevelLimit]   
   WHERE  
   [LevelLimitID] = @LevelLimitID     FOR XML AUTO) as data,  
    getdate()   
  END  
  
 RETURN @Err  
END

GO
