SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[list].[prc_LT_ItemListDataParameter_ins]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [list].[prc_LT_ItemListDataParameter_ins] AS' 
END
GO
ALTER PROCEDURE [list].[prc_LT_ItemListDataParameter_ins]
(
	@LanguageID int,
	@ItemListDataParameterID int,
	@Name nvarchar(256),
	@Description nvarchar(max),
    @cUserid int,
    @Log smallint = 1
)    
AS
BEGIN
	SET NOCOUNT ON;
	DECLARE @Err Int
	
	INSERT INTO [list].[LT_ItemListDataParameter]
           ([LanguageID]
           ,[ItemListDataParameterID]
           ,[Name]
           ,[Description])
     VALUES
           (@LanguageID
           ,@ItemListDataParameterID
           ,@Name
           ,@Description)
           
     Set @Err = @@Error
	 IF @Log = 1 
	 BEGIN 
		INSERT INTO [Log].[AuditLog] ( UserId, TableName, Type, Data, Created) 
		SELECT @cUserid,'LT_ItemListDataParameter',0,
		( SELECT * FROM [list].[LT_ItemListDataParameter] 
			WHERE
			[LanguageID] = @LanguageID AND
			[ItemListDataParameterID] = @ItemListDataParameterID				 FOR XML AUTO) as data,
				getdate() 
	  END

	
	 RETURN @Err
END

GO
