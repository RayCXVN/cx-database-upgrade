SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[at].[prc_QA_Calc_upd]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [at].[prc_QA_Calc_upd] AS' 
END
GO
ALTER PROCEDURE  [at].[prc_QA_Calc_upd]  
(  
 @QAID int,  
 @QuestionID int,  
 @AlternativeID int,  
 @CFID int,  
 @MinValue float,  
 @MaxValue float,  
 @No smallint,  
 @NewValue float,  
 @UseNewValue smallint,  
 @UseCalculatedValue smallint,  
 @RoleID int = null,  
 @cUserid int,  
 @Log smallint = 1, 
 @ItemID int = NULL
)  
AS  
BEGIN  
 SET NOCOUNT ON  
 DECLARE @Err Int  
  
 UPDATE [at].[QA_Calc]  
 SET  
  [QuestionID] = @QuestionID,  
  [AlternativeID] = @AlternativeID,  
  [CFID] = @CFID,  
  [MinValue] = @MinValue,  
  [MaxValue] = @MaxValue,  
  [No] = @No,  
  [NewValue] = @NewValue,  
  [UseNewValue] = @UseNewValue,  
  [UseCalculatedValue] = @UseCalculatedValue,  
  [RoleID] = @RoleID,
  [ItemID]  = @ItemID
 WHERE  
  [QAID] = @QAID  
  
 IF @Log = 1   
 BEGIN   
  INSERT INTO [Log].[AuditLog] ( UserId, TableName, Type, Data, Created)   
  SELECT @cUserid,'QA_Calc',1,  
  ( SELECT * FROM [at].[QA_Calc]   
   WHERE  
   [QAID] = @QAID    FOR XML AUTO) as data,  
   getdate()  
 END  
  
 Set @Err = @@Error  
  
 RETURN @Err  
END  
  
  

GO
