SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[prop].[prc_PropValue_upd]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [prop].[prc_PropValue_upd] AS' 
END
GO

ALTER PROCEDURE [prop].[prc_PropValue_upd]
(
	@PropValueID int,
	@PropertyID int,
	@PropOptionID int = null,
	@PropFileID int = null,
	@No smallint,
	@Created datetime,
	@CreatedBy INT=NULL,
	@Updated datetime = null output,
	@UpdatedBy INT=NULL,
	@ItemID int,
	@Value ntext,
	@cUserid int,
	@Log smallint = 1
)
AS
BEGIN
	SET NOCOUNT ON
	DECLARE @Err Int

	SET @Updated = getdate()
	UPDATE [prop].[PropValue]
	SET
		[PropertyID] = @PropertyID,
		[PropOptionID] = @PropOptionID,
		[PropFileID] = @PropFileID,
		[No] = @No,
		[Updated] = @Updated,
		[UpdatedBy] = @UpdatedBy,
		[ItemID] = @ItemID,
		[Value] = @Value
	WHERE
		[PropValueID] = @PropValueID

	IF @Log = 1 
	BEGIN 
		INSERT INTO [Log].[AuditLog] ( UserId, TableName, Type, Data, Created) 
		SELECT @cUserid,'PropValue',1,
		( SELECT * FROM [prop].[PropValue] 
			WHERE
			[PropValueID] = @PropValueID			 FOR XML AUTO) as data,
			getdate()
	END

	Set @Err = @@Error

	RETURN @Err
END

GO
