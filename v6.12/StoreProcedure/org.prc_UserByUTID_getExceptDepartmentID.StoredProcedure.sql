SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[org].[prc_UserByUTID_getExceptDepartmentID]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [org].[prc_UserByUTID_getExceptDepartmentID] AS' 
END
GO
/*  2016-10-10 Ray: Remove column Status from org.User, org.Department, RDB.dbo.Result
*/
ALTER PROC [org].[prc_UserByUTID_getExceptDepartmentID]
(
  @HDID INT,
  @UsertypeID int,
  @UsertypeID2 INT,
  @ListDepartmentID nvarchar(32)
)
AS
BEGIN
	SET NOCOUNT ON
    DECLARE @ActiveEntityStatusID INT = (SELECT EntityStatusID FROM EntityStatus WHERE CodeName='Active')

	SELECT u.[UserID],
		u.[Ownerid],
		u.[DepartmentID],
		u.[LanguageID],
		ISNULL(u.[RoleID], 0) AS 'RoleID',
		u.[UserName],
		u.[Password],
		u.[LastName],
		u.[FirstName],
		u.[Email],
		u.[Mobile],
		u.[ExtID],
		u.[SSN],
		u.[Tag],
		u.[Locked],
		u.[ChangePassword],
		u.[HashPassword],
		u.[SaltPassword],
		u.[OneTimePassword],
		u.[OTPExpireTime],
		u.[Created],
		d.Name as DepartmentName,
        u.EntityStatusID,
        u.Deleted,
        u.EntityStatusReasonID
	FROM org.H_D hd 
	JOIN org.[USER] u ON u.departmentid = hd.departmentid
	JOIN org.[Department] d ON u.departmentid = d.departmentid and d.EntityStatusID = @ActiveEntityStatusID
	JOIN org.[UT_U] utu on utu.userid = u.userid and (utu.usertypeid = @Usertypeid)
	JOIN org.[UT_U] utu2 on utu2.userid = u.userid and (utu2.usertypeid = @Usertypeid2)
	WHERE hd.PATH LIKE '%\' + cast(@HDID as nvarchar(16)) + '\%'
	AND u.EntityStatusID = @ActiveEntityStatusID AND u.Deleted IS NULL AND u.DepartmentID NOT IN (SELECT value FROM dbo.funcListToTableInt(@ListDepartmentID,','))
END

GO
