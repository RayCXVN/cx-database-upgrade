SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[sec].[prc_RuleGroup_ins]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [sec].[prc_RuleGroup_ins] AS' 
END
GO
ALTER PROCEDURE [sec].[prc_RuleGroup_ins]
(
	@RuleGroupID int = NULL output,
	@GroupAccessRuleID int,
    @AccessRuleID int,
	@cUserid int,
	@Log smallint = 1
)
AS
BEGIN
SET NOCOUNT ON
DECLARE @Err int
INSERT INTO [sec].[RuleGroup] ([GroupAccessRuleID], [AccessRuleID])
    VALUES (@GroupAccessRuleID, @AccessRuleID)
SET @Err = @@Error
SET @RuleGroupID = SCOPE_IDENTITY()
IF @Log = 1 BEGIN
INSERT INTO [Log].[AuditLog] (UserId, TableName, Type, Data, Created)
  SELECT
        @cUserid,
        'RuleGroup',
        0,
        (SELECT
            *
        FROM [sec].[RuleGroup]
        WHERE [RuleGroupID] = @RuleGroupID
        FOR xml AUTO)
        AS data,
        GETDATE()
END
RETURN @Err
END

GO
