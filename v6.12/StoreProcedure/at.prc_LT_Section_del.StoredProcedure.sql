SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[at].[prc_LT_Section_del]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [at].[prc_LT_Section_del] AS' 
END
GO
ALTER PROCEDURE [at].[prc_LT_Section_del]
(
	@LanguageID int,
	@SectionID int,
	@cUserid int,
	@Log smallint = 1
)
AS
BEGIN
	SET NOCOUNT ON
	DECLARE @Err Int

	IF @Log = 1 
	BEGIN 
		INSERT INTO [Log].[AuditLog] ( UserId, TableName, Type, Data, Created) 
		SELECT @cUserid,'LT_Section',2,
		( SELECT * FROM [at].[LT_Section] 
			WHERE
			[LanguageID] = @LanguageID AND
			[SectionID] = @SectionID
			 FOR XML AUTO) as data,
			getdate() 
	END 


	DELETE FROM [at].[LT_Section]
	WHERE
		[LanguageID] = @LanguageID AND
		[SectionID] = @SectionID

	Set @Err = @@Error

	RETURN @Err
END


GO
