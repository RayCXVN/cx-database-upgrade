SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[prc_KeyValue_upd]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [dbo].[prc_KeyValue_upd] AS' 
END
GO

ALTER PROCEDURE [dbo].[prc_KeyValue_upd]
(
	@Id int,
	@Type tinyint,
	@Key varchar(50),
	@Value nvarchar(1000),
	@cUserid int,
	@Log smallint = 1
)
AS
BEGIN
	SET NOCOUNT ON
	DECLARE @Err Int

	UPDATE [dbo].[KeyValue]
	SET
		[Id] = @Id,
		[Type] = @Type,
		[Key] = @Key,
		[Value] = @Value
	WHERE
		[Id] = @Id AND
		[Type] = @Type AND
		[Key] = @Key

	IF @Log = 1 
	BEGIN 
		INSERT INTO [Log].[AuditLog] ( UserId, TableName, Type, Data, Created) 
		SELECT @cUserid,'KeyValue',1,
		( SELECT * FROM [dbo].[KeyValue] 
			WHERE
			[Id] = @Id AND
			[Type] = @Type AND
			[Key] = @Key			 FOR XML AUTO) as data,
			getdate()
	END

	Set @Err = @@Error

	RETURN @Err
END


GO
