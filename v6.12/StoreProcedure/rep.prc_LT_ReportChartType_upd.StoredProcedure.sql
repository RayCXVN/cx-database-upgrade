SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[rep].[prc_LT_ReportChartType_upd]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [rep].[prc_LT_ReportChartType_upd] AS' 
END
GO

ALTER PROCEDURE [rep].[prc_LT_ReportChartType_upd]
(
	@LanguageID int,
	@ReportChartTypeID int,
	@Name nvarchar(256),
	@cUserid int,
	@Log smallint = 1
)
AS
BEGIN
	SET NOCOUNT ON
	DECLARE @Err Int

	UPDATE [rep].[LT_ReportChartType]
	SET
		[LanguageID] = @LanguageID,
		[ReportChartTypeID] = @ReportChartTypeID,
		[Name] = @Name
	WHERE
		[LanguageID] = @LanguageID AND
		[ReportChartTypeID] = @ReportChartTypeID

	IF @Log = 1 
	BEGIN 
		INSERT INTO [Log].[AuditLog] ( UserId, TableName, Type, Data, Created) 
		SELECT @cUserid,'LT_ReportChartType',1,
		( SELECT * FROM [rep].[LT_ReportChartType] 
			WHERE
			[LanguageID] = @LanguageID AND
			[ReportChartTypeID] = @ReportChartTypeID			 FOR XML AUTO) as data,
			getdate()
	END

	Set @Err = @@Error

	RETURN @Err
END


GO
