SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[org].[prc_UGMember_MoveUsersIntoGroup]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [org].[prc_UGMember_MoveUsersIntoGroup] AS' 
END
GO
-- =============================================
-- Author:		Tom Lee
-- Create date: 03/12/2012
-- Description:	Select Leader for +Competence
-- =============================================
/*  2018-07-11 Ray:     Replace UG_U by UGMember
*/
ALTER PROCEDURE [org].[prc_UGMember_MoveUsersIntoGroup]
	@UserIds           varchar(max),
	@LeaderGroupId     int,
	@UserGroupTypeId   int,
	@Message           nvarchar(max) OUTPUT,
     @ExecutedByUserID  int = NULL
AS
BEGIN
	SET XACT_ABORT ON
     DECLARE @UserId int, @IsLeader bit = 0, @LeaderID int, @EntityStatus_Active int, @EntityStatusReason_ActiveNone int
	DECLARE @UserIDTable TABLE (UserID int)	

	SELECT @LeaderID = ug.UserID FROM org.UserGroup ug WHERE UserGroupID = @LeaderGroupId
     SELECT @EntityStatus_Active = [EntityStatusID] FROM [dbo].[EntityStatus] WHERE [CodeName] = 'Active';
     SELECT @EntityStatusReason_ActiveNone = [EntityStatusReasonID] FROM [dbo].[EntityStatusReason] WHERE [CodeName] = 'Active_None';

	INSERT INTO @UserIDTable (UserID) SELECT ParseValue FROM dbo.StringToArray(@UserIds,',')

	SET @Message = ' '
	SELECT @IsLeader = 1 FROM @UserIDTable WHERE UserID = @LeaderID
	IF @IsLeader = 1	
	BEGIN		
		SELECT @Message = FirstName + ' '+ LastName FROM org.[User] where UserID = @LeaderID
		DELETE FROM @UserIDTable WHERE UserID = @LeaderID
	END
		
	BEGIN TRANSACTION
	    -- Remove users from original groups
	    DELETE ugm
         FROM org.[UGMember] ugm
         JOIN @UserIDTable u ON u.[UserID] = ugm.[UserID]
         JOIN org.UserGroup ug ON ugm.UserGroupID = ug.UserGroupID AND ug.UserGroupTypeID = @UserGroupTypeId;

	    -- Move users to new group
	    INSERT INTO org.[UGMember] ([UserGroupID], [UserID], [Created], [CreatedBy], [LastUpdated], [LastUpdatedBy],
                                     [EntityStatusID], [EntityStatusReasonID], [CustomerID], [PeriodID], [ExtID], [Deleted])
         SELECT @LeaderGroupId AS [UserGroupID], u.[UserID], GETDATE() AS [Created], @ExecutedByUserID AS [CreatedBy], GETDATE() AS [LastUpdated], @ExecutedByUserID AS [LastUpdatedBy],
                @EntityStatus_Active AS [EntityStatusID], @EntityStatusReason_ActiveNone AS [EntityStatusReasonID], d.[CustomerID], ug.[PeriodID], '' AS [ExtID], NULL AS [Deleted]
         FROM @UserIDTable u
         JOIN [org].[UserGroup] ug ON ug.[UserGroupID] = @LeaderGroupId
         JOIN [org].[Department] d ON d.[DepartmentID] = ug.[DepartmentID];

	COMMIT TRANSACTION
END

GO
