SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[res].[prc_LT_Provider_ins]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [res].[prc_LT_Provider_ins] AS' 
END
GO
  
ALTER PROCEDURE [res].[prc_LT_Provider_ins]
(
	@ProviderID int,
	@LanguageID int,
	@Name nvarchar(max),
	@Description nvarchar(max),
	@cUserid int,
	@Log smallint = 1
)
AS  
BEGIN  
	SET NOCOUNT ON  
	DECLARE @Err Int  
  
	INSERT INTO res.LT_Provider
	(
		[LanguageID],
		[ProviderID],
		[Name],
		[Description]
	)
	VALUES
	(
		@LanguageID,
		@ProviderID,
		@Name,
		@Description
	)
  
	Set @Err = @@Error  
	IF @Log = 1 
	BEGIN 
		INSERT INTO [Log].[AuditLog] ( UserId, TableName, Type, Data, Created) 
		SELECT @cUserid,'res.LT_Provider',0,
		( SELECT * FROM [res].[LT_Provider] 
			WHERE
			[ProviderID] = @ProviderID AND [LanguageID] = @LanguageID	FOR XML AUTO) as data,
				getdate() 
	END

	RETURN @Err
END  

GO
