SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[at].[prc_SentMailFiles_del]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [at].[prc_SentMailFiles_del] AS' 
END
GO


ALTER PROCEDURE [at].[prc_SentMailFiles_del]
(
	@SentMailFileID int,
	@cUserid int,
	@Log smallint = 1
)
AS
BEGIN
	SET NOCOUNT ON
	DECLARE @Err Int

	IF @Log = 1 
	BEGIN 
		INSERT INTO [Log].[AuditLog] ( UserId, TableName, Type, Data, Created) 
		SELECT @cUserid,'SentMailFiles',2,
		( SELECT * FROM [at].[SentMailFiles] 
			WHERE
			[SentMailFileID] = @SentMailFileID
			 FOR XML AUTO) as data,
			getdate() 
	END 


	DELETE FROM [at].[SentMailFiles]
	WHERE
		[SentMailFileID] = @SentMailFileID

	Set @Err = @@Error

	RETURN @Err
END


GO
