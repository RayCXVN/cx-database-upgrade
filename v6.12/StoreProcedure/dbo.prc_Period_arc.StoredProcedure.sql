SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[prc_Period_arc]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [dbo].[prc_Period_arc] AS' 
END
GO
/*  2018-07-09 Ray:     Replace UG_U by UGMember
*/
-- [dbo].[prc_Period_arc] 3, 17319, 37, 2, 3
ALTER proc [dbo].[prc_Period_arc]
(
    @PeriodID           int,
    @HDID               int,
    @DepartmentTypeID   int,
    @DT_RTID            int,
    @UT_RTID            int,
    @ExecutedByUserID   int = NULL
)
AS
BEGIN

    DECLARE @DepartmentID int, @Name nvarchar(256), @UGID int, @ParentDepID int, @UserID int, @OName varchar(10), @NName varchar(10), @Trinn varchar(200),
            @EntityStatusID_Active int = 0, @EntityStatusReasonID_Active int;

    CREATE TABLE #Map
    (
        OID        int,
        OName    varchar(10),
        NID        int,
        NName    varchar(10)
    )

    SELECT @ParentDepID = DepartmentID FROM org.H_D WHERE HDID = @HDID
    SELECT @EntityStatusID_Active = EntityStatusID FROM dbo.EntityStatus WHERE CodeName = N'Active'
    SELECT @EntityStatusReasonID_Active = EntityStatusReasonID FROM dbo.EntityStatusReason WHERE CodeName = N'Active_None'

    BEGIN TRANSACTION
    DECLARE curDep CURSOR LOCAL FAST_FORWARD READ_ONLY FOR 
    SELECT DISTINCT H.DepartmentID, Name FROM org.H_D H
    INNER JOIN org.Department D on H.DepartmentID = D.DepartmentID
    WHERE ParentID = @HDID AND H.DepartmentID IN (SELECT DepartmentID FROM org.DT_D WHERE DepartmentTypeID = @DepartmentTypeID)
    
    OPEN curDep
    FETCH NEXT FROM curDep INTO @DepartmentID, @Name

    WHILE (@@FETCH_STATUS = 0)
    BEGIN
        DELETE FROM #Map  
        INSERT org.Usergroup(OwnerID, DepartmentID, Name, Description, ExtID, PeriodID)
        SELECT OwnerID, DepartmentID, Name, Description, ExtID, @PeriodID From org.Department
        WHERE DepartmentID = @DepartmentID
        SET @UGID = scope_identity()

        INSERT INTO [org].[UGMember] ([UserGroupID], [UserID], [Created], [CreatedBy], [LastUpdated], [LastUpdatedBy],
                                      [EntityStatusID], [EntityStatusReasonID], [CustomerID], [PeriodID], [ExtID], [Deleted])
        SELECT @UGID AS [UserGroupID], u.[UserID], GETDATE() AS [Created], @ExecutedByUserID AS [CreatedBy], GETDATE() AS [LastUpdated], @ExecutedByUserID AS [LastUpdatedBy],
               @EntityStatusID_Active AS [EntityStatusID], @EntityStatusReasonID_Active AS [EntityStatusReasonID], u.[CustomerID], @PeriodID, '' AS [ExtID], NULL AS [Deleted]
        FROM org.[User] u WHERE u.[DepartmentID] = @DepartmentID;
        
        SET @Trinn=''
        SELECT @Trinn = @Trinn + ', ' + DT1.ExtID FROM ObjectMapping O
        INNER JOIN org.DT_D DT ON DT.DepartmentTypeID = O.FromID AND DT.DepartmentID = @DepartmentID
        INNER JOIN org.DepartmentType DT1 ON O.FromID = DT1.DepartmentTypeID
        WHERE O.RelationTypeID = @DT_RTID
        
        SET @Trinn=SUBSTRING(@Trinn, 3, 1000)
        
        UPDATE org.UserGroup
        SET Description = @Trinn
        WHERE UserGroupID = @UGID
        
        INSERT #Map
        SELECT FromID, DT1.ExtID, ToID, DT2.ExtID
        FROM ObjectMapping O
        INNER JOIN org.DT_D DT ON DT.DepartmentTypeID = O.FromID AND DT.DepartmentID = @DepartmentID
        INNER JOIN org.DepartmentType DT1 ON O.FromID = DT1.DepartmentTypeID
        INNER JOIN org.DepartmentType DT2 ON O.ToID = DT2.DepartmentTypeID
        WHERE O.RelationTypeID = @DT_RTID AND 
            O.ToID IN (SELECT DepartmentTypeID FROM org.DT_D WHERE DepartmentID = @ParentDepID)
        
        DELETE org.DT_D WHERE DepartmentID = @DepartmentID 
            AND (DepartmentTypeID IN (SELECT FromID FROM ObjectMapping WHERE RelationTypeID = @DT_RTID)
            OR  DepartmentTypeID IN (SELECT ToID FROM ObjectMapping WHERE RelationTypeID = @DT_RTID))
        
        INSERT org.DT_D
        SELECT NID, @DepartmentID FROM #Map            
        
        /* Fix Name */
        SELECT TOP 1 @Nname = NName, @OName = OName FROM #Map
        IF CHARINDEX(@OName, @Name) = 1
        BEGIN
            SET @Name = REPLACE(LEFT(@Name, LEN(@OName)), @OName, @Nname) + SUBSTRING(@Name, LEN(@OName) + 1, 1000)
            UPDATE org.Department
            SET Name = @Name
            WHERE DepartmentID = @DepartmentID
        END
            
        DECLARE curUser CURSOR LOCAL FAST_FORWARD READ_ONLY FOR 
        SELECT DISTINCT UserID FROM org.[User]
        WHERE DepartmentID = @DepartmentID
        
        OPEN curUser
        FETCH NEXT FROM curUser INTO @UserID

        WHILE (@@FETCH_STATUS = 0)
        BEGIN
            DELETE FROM #Map  
            INSERT #Map
            SELECT FromID, '', ToID, ''
            FROM ObjectMapping O
            INNER JOIN org.UT_U DT ON DT.UserTypeID = O.FromID AND DT.UserID = @UserID
            WHERE O.RelationTypeID = @UT_RTID AND 
                O.ToID IN (SELECT ToID FROM ObjectMapping O
                    INNER JOIN org.DT_D DT ON O.FromID = DT.DepartmentTypeID AND DT.DepartmentID = @DepartmentID 
                    WHERE RelationTypeID = 1 AND O.FromTableTypeID = 27 AND O.ToTableTypeID = 26)
                    
            
            DELETE org.UT_U WHERE UserID = @UserID
                AND (UserTypeID IN (SELECT FromID FROM ObjectMapping WHERE RelationTypeID = @UT_RTID)
                OR  UserTypeID IN (SELECT ToID FROM ObjectMapping WHERE RelationTypeID = @UT_RTID))
            
            INSERT org.UT_U
            SELECT NID, @UserID FROM #Map            
                    
            FETCH NEXT FROM curUser INTO @UserID
        END
        CLOSE curUser
        DEALLOCATE curUser
        
        FETCH NEXT FROM curDep INTO @DepartmentID, @Name
    END
    CLOSE curDep
    DEALLOCATE curDep
    COMMIT TRANSACTION

END 
GO
