SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[form].[prc_Form_copy]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [form].[prc_Form_copy] AS' 
END
GO

    
  --sp_helptext '[app].[prc_PortalPage_copy]'
ALTER PROCEDURE [form].[prc_Form_copy]  
(  
 @FormID int,
 @NewFormID int = null output  
)  
AS  
BEGIN  

 SET XACT_ABORT ON  
 BEGIN TRANSACTION  
 DECLARE  
 @OwnerID int,  
 @ElementID int,  
 @TableTypeID smallint,  
 @ContextFormFieldID INT=NULL,  
 @Type smallint,  
 @FName nvarchar(64),  
 @CssClass nvarchar(128),  
 @ExtID nvarchar(64) = '',
 @FormFieldID AS INT,  
 @NewFormFieldId AS INT,  
 @FormCommandID AS INT,  
 @NewFormCommandId AS INT,  
 @FormRowId AS INT,
 @NewFormRowId AS INT,
 @FormCellId AS INT,
 @NewFormCellId AS INT            
 
 SELECT  
    @OwnerID =  [OwnerID]
      ,@ElementID = [ElementID]
      ,@TableTypeID =  [TableTypeID]
      ,@ContextFormFieldID = [ContextFormFieldID]
      ,@Type = [Type]
      ,@FName = [FName]
      ,@CssClass = [CssClass]
      ,@ExtID = [ExtID]
 FROM [form].[Form]   
 WHERE  
  [FormID] = @FormID  
  
   INSERT INTO [form].[Form]  
	 ([OwnerID]
      ,[ElementID]
      ,[TableTypeID]
      ,[ContextFormFieldID]
      ,[Type]
      ,[FName]
      ,[CssClass]
      ,[ExtID]
      )    
	 VALUES  
	 (  @OwnerID,  
  @ElementID,  
  @TableTypeID,  
  @ContextFormFieldID,  
  @Type,  
  @FName,  
  @CssClass,  
  @ExtID  )    
 
  Set @NewFormID = scope_identity()  
  
   --insert LT_Form
  INSERT INTO [form].[LT_Form]
  ([LanguageID]
	  ,[FormID]
      ,[Name]
      ,[Description])
  (SELECT [LanguageID]
	   ,@NewFormID
      ,[Name]
      ,[Description]
   FROM [form].[LT_Form]
   WHERE ([FormID] = @FormID))
 
	--insert Form Field
	DECLARE curFormField CURSOR LOCAL FAST_FORWARD READ_ONLY FOR      
	SELECT FormFieldID        
	FROM [form].[FormField]    
	WHERE  ([FormID] = @FormID)

	OPEN curFormField    
	FETCH NEXT FROM curFormField INTO @FormFieldID    
	    
	WHILE (@@FETCH_STATUS = 0)    
	BEGIN    
		INSERT INTO [form].[FormField]  
		([FormID]
      ,[No]
      ,[TableTypeID]
      ,[RelatedTableTypeID]
      ,[FieldTypeID]
      ,[ValueType]
      ,[FormMode]
      ,[RenderType]
      ,[DefaultValue]
      ,[AvailableValues]
      ,[Mandatory]
      ,[Multiple]
      ,[PropertyID]
      ,[FieldName]
      ,[Parameters]
      ,[MappingFromFieldId]
      ,[MappingTableTypeID]
      ,[Created]
      ,[OverrideLocked])  
		 (SELECT @NewFormID  
	  ,[No]
      ,[TableTypeID]
      ,[RelatedTableTypeID]
      ,[FieldTypeID]
      ,[ValueType]
      ,[FormMode]
      ,[RenderType]
      ,[DefaultValue]
      ,[AvailableValues]
      ,[Mandatory]
      ,[Multiple]
      ,[PropertyID]
      ,[FieldName]
      ,[Parameters]
      ,[MappingFromFieldId]
      ,[MappingTableTypeID]
      ,[Created]
      ,[OverrideLocked]
	   FROM [form].[FormField]  
	   WHERE ([FormFieldID] = @FormFieldID))  
	     
	      
	   SET @NewFormFieldId = SCOPE_IDENTITY()    
	     
	   --insert LT_FormField  
	   INSERT INTO [form].[LT_FormField]  
			   ( [LanguageID]
				  ,[FormFieldID]
				  ,[Name]
				  ,[RegularExpression]
				  ,[RegularExpressionText]
				  ,[Description] 
			   )  
	   (SELECT  
		   [LanguageID]
		  ,@NewFormFieldId
		  ,[Name]
		  ,[RegularExpression]
		  ,[RegularExpressionText]
		  ,[Description]
		FROM [form].[LT_FormField]  
		WHERE [FormFieldID] = @FormFieldID)  
	        
		FETCH NEXT FROM curFormField INTO @FormFieldID    
	END    
	CLOSE curFormField    
	DEALLOCATE curFormField    
	
	--insert FormCommand
	DECLARE curFormCommand CURSOR LOCAL FAST_FORWARD READ_ONLY FOR      
	SELECT FormCommandID        
	FROM [form].[FormCommand]    
	WHERE  ([FormID] = @FormID)
	
	OPEN curFormCommand    
	FETCH NEXT FROM curFormCommand INTO @FormCommandID    
	    
	WHILE (@@FETCH_STATUS = 0)    
	BEGIN    
		INSERT INTO [form].[FormCommand]  
		( [FormID]
      ,[No]
      ,[FormMode]
      ,[CssClass]
      ,[ClientFunction]
      ,[Parameters]
      ,[FormCommandType]
      ,[Created]
      ,[CommandGroupClass])  
		 (SELECT @NewFormID  
      ,[No]
      ,[FormMode]
      ,[CssClass]
      ,[ClientFunction]
      ,[Parameters]
      ,[FormCommandType]
      ,[Created]
      ,[CommandGroupClass]
	   FROM [form].[FormCommand]  
	   WHERE ([FormCommandID] = @FormCommandID))  
	     
	   SET @NewFormCommandId = SCOPE_IDENTITY()    
	     
	   --insert LT_FormCommand  
	   INSERT INTO [form].[LT_FormCommand]  
			   ( [LanguageID]
			  ,[FormCommandID]
			  ,[Name]
			  ,[ErrorMsg]
			  ,[ConfirmMsg]
			  ,[Description]
			   )  
	   (SELECT  
		 [LanguageID]
		  ,@NewFormCommandId
		  ,[Name]
		  ,[ErrorMsg]
		  ,[ConfirmMsg]
		  ,[Description]
		FROM [form].[LT_FormCommand]  
		WHERE [FormCommandID] = @FormCommandID)  
	        
		FETCH NEXT FROM curFormCommand INTO @FormCommandID    
	END    
	CLOSE curFormCommand    
	DEALLOCATE curFormCommand 
	
	
	
  --insert Form Row
  	DECLARE curFormRow CURSOR LOCAL FAST_FORWARD READ_ONLY FOR      
	SELECT FormRowID        
	FROM [form].[FormRow]    
	WHERE  ([FormID] = @FormID)
	
	OPEN curFormRow    
	FETCH NEXT FROM curFormRow INTO @FormRowID    
	    
	WHILE (@@FETCH_STATUS = 0)    
	BEGIN    
		INSERT INTO [form].[FormRow]  
		([FormID]
      ,[No]
      ,[RowTypeID]
      ,[CssClass]
      ,[Created])  
		 (SELECT @NewFormID  
      ,[No]
      ,[RowTypeID]
      ,[CssClass]
      ,[Created]
	   FROM [form].[FormRow]  
	   WHERE ([FormRowID] = @FormRowID))  
	     
	      
	   SET @NewFormRowId = SCOPE_IDENTITY()    

		--insert FormCell 
		DECLARE curFormCell CURSOR LOCAL FAST_FORWARD READ_ONLY FOR      
		SELECT FormCellID        
		FROM [form].[FormCell]    
		WHERE  ([FormRowID] = @FormRowID)
	
		OPEN curFormCell    
		FETCH NEXT FROM curFormCell INTO @FormCellID   
		     WHILE (@@FETCH_STATUS = 0)    
			 BEGIN    
				INSERT INTO [form].[FormCell]
			   ([FormRowID]
			  ,[No]
			  ,[CellTypeID]
			  ,[FormFieldID]
			  ,[FormCommandID]
			  ,[CssClass]
			  ,[Alignment]
			  ,[Colspan]
			  ,[Width]
			   )  
			   (SELECT  
						@NewFormRowId
					  ,[No]
					  ,[CellTypeID]
					  ,[FormFieldID]
					  ,[FormCommandID]
					  ,[CssClass]
					  ,[Alignment]
					  ,[Colspan]
					  ,[Width]
				FROM [form].[FormCell]  
				WHERE [FormCellID] = @FormCellID)  
		
				SET @NewFormCellId = SCOPE_IDENTITY()    
				--insert LT_FormCell
				
				 INSERT INTO [form].[LT_FormCell]  
			   ( [LanguageID]
				  ,[FormCellID]
				  ,[Name]
				  ,[ToolTip]
				  ,[Description]
			   )  
			   (SELECT  
				 [LanguageID]
				  ,@NewFormCellId
				  ,[Name]
				  ,[ToolTip]
				  ,[Description]
				FROM [form].[LT_FormCell]  
				WHERE [FormCellID] = @FormCellID)  
				
				FETCH NEXT FROM curFormCell INTO @FormCellID  
			 END
	  
		CLOSE curFormCell
		DEALLOCATE curFormCell
	        
		FETCH NEXT FROM curFormRow INTO @FormRowID    
	END    
	CLOSE curFormRow    
	DEALLOCATE curFormRow  
	
 COMMIT TRANSACTION  
    
END  
  
  

GO
