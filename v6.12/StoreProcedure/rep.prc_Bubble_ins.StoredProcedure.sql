SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[rep].[prc_Bubble_ins]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [rep].[prc_Bubble_ins] AS' 
END
GO

ALTER PROCEDURE [rep].[prc_Bubble_ins]
(
	@BubbleID int = null output,
	@OwnerID int,
	@ActivityID int,
	@No smallint,
	@Status smallint,
	@Created datetime,
	@cUserid int,
	@Log smallint = 1
)
AS
BEGIN
	SET NOCOUNT ON
	DECLARE @Err Int

	INSERT INTO [rep].[Bubble]
	(
		[OwnerID],
		[ActivityID],
		[No],
		[Status],
		[Created]
	)
	VALUES
	(
		@OwnerID,
		@ActivityID,
		@No,
		@Status,
		@Created
	)

	Set @Err = @@Error
	Set @BubbleID = scope_identity()

	IF @Log = 1 
	BEGIN 
		INSERT INTO [Log].[AuditLog] ( UserId, TableName, Type, Data, Created) 
		SELECT @cUserid,'Bubble',0,
		( SELECT * FROM [rep].[Bubble] 
			WHERE
			[BubbleID] = @BubbleID				 FOR XML AUTO) as data,
				getdate() 
	 END

	RETURN @Err
END


GO
