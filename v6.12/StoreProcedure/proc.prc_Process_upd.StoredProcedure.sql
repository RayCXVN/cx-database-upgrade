SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[proc].[prc_Process_upd]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [proc].[prc_Process_upd] AS' 
END
GO
/*
	2017-07-20 Steve: Change the length of column ExtID in all related tables from xAPI format
*/
ALTER PROCEDURE [proc].[prc_Process_upd]
(
	@ProcessID int,
	@ProcessGroupID int,
	@DepartmentID int,
	@Active smallint,
	@TargetValue float,
	@UserID int,
	@CustomerID int,
	@StartDate datetime,
	@DueDate datetime,
	@Responsible nvarchar(128),
	@URL nvarchar(512),
	@No smallint,
	@ProcessTypeID int = NULL,
	@isPublic smallint = 0,
	@PeriodID int = NULL,
	@cUserid int,
	@Log smallint = 1,
	@Created DATETIME = NULL,
	@ExtID nvarchar(256)=''
)
AS
BEGIN
	SET NOCOUNT ON
	DECLARE @Err Int

	SET @Created = ISNULL(@Created, GETDATE())

	UPDATE [proc].[Process]
	SET
		[ProcessGroupID] = @ProcessGroupID,
		[DepartmentID] = @DepartmentID,
		[Active] = @Active,
		[TargetValue] = @TargetValue,
		[UserID] = @UserID,
		[CustomerID] = @CustomerID,
		[StartDate] = @StartDate,
		[DueDate] = @DueDate,
		[Responsible] = @Responsible,
		[URL] = @URL,
		[No] = @No,
		[ProcessTypeID] = @ProcessTypeID ,
		[isPublic] = @isPublic,
		[PeriodID] = @PeriodID,
		[Created] = @Created,
		[ExtID] = @ExtID
	WHERE
		[ProcessID] = @ProcessID

	IF @Log = 1 
	BEGIN 
		INSERT INTO [Log].[AuditLog] ( UserId, TableName, Type, Data, Created) 
		SELECT @cUserid,'Process',1,
		( SELECT * FROM [proc].[Process] 
			WHERE
			[ProcessID] = @ProcessID			 FOR XML AUTO) as data,
			getdate()
	END

	Set @Err = @@Error

	RETURN @Err
END


GO
