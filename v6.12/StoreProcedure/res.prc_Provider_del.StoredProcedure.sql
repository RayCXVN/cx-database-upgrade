SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[res].[prc_Provider_del]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [res].[prc_Provider_del] AS' 
END
GO
  
ALTER PROCEDURE [res].[prc_Provider_del]
(
	@ProviderID int,
	@cUserid int,
	@Log smallint = 1
)
AS  
BEGIN  
	SET NOCOUNT ON  
	DECLARE @Err Int  
  
	IF @Log = 1 
	BEGIN 
		INSERT INTO [Log].[AuditLog] ( UserId, TableName, Type, Data, Created) 
		SELECT @cUserid,'res.Provider',0,
		( SELECT * FROM [res].[Provider] 
			WHERE
			[ProviderID] = @ProviderID FOR XML AUTO) as data,
				getdate() 
	END

	DELETE FROM res.Provider
	WHERE
		[ProviderID] = @ProviderID
	
	Set @Err = @@Error  
	
	RETURN @Err
END  

GO
