SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[log].[prc_LatestEventGetByKeyValue]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [log].[prc_LatestEventGetByKeyValue] AS' 
END
GO
/*  2016-10-10 Ray: Remove column Status from org.User, org.Department, RDB.dbo.Result
*/
ALTER PROCEDURE [log].[prc_LatestEventGetByKeyValue]
    @EventTypeID    nvarchar(128)	='6',
    @EventKeyID     nvarchar(128)	='PasswordType',
    @Value          nvarchar(128)	='Hash',
    @EventKeyID2    int = 0,
    @Value2         nvarchar(128) = '',
    @HDID           int = 0,
    @FromDate       date = NULL,
    @ToDate         date = NULL,
    @PageIndex		INT =1,
	@PageSize		INT= 10,
	@UserIDList		NVARCHAR(MAX) ='0'
WITH RECOMPILE
AS
BEGIN
    SET NOCOUNT ON
    DECLARE @sqlCommand nvarchar(max), @Path nvarchar(max), @EntityStatusID_Active INT = 0, @EntityStatusID_Deactive INT = 0

	SET @Path = '%\' + CONVERT(nvarchar(32),@HDID) + '\%'
	SELECT @EntityStatusID_Active = EntityStatusID FROM dbo.EntityStatus WHERE CodeName = N'Active'
	SELECT @EntityStatusID_Deactive = EntityStatusID FROM dbo.EntityStatus WHERE CodeName = N'Deactive'
    
	DECLARE @EventTypeTable TABLE (EventTypeID int)
    DECLARE @UserList TABLE (UserID int)

    CREATE TABLE #EventKeyTable (EventKeyID int)
	CREATE CLUSTERED INDEX IDX_C_EventKeyTable_EventKeyID ON #EventKeyTable(EventKeyID)
    CREATE TABLE #UserIDTable (UserID int, [EntityStatus] int, [Deleted] DATETIME)
	CREATE CLUSTERED INDEX IDX_C_UserIDTable_UserID ON #UserIDTable(UserID)
    CREATE TABLE #Event (EventID int, EventTypeID int, UserID int, IPNumber nvarchar(64), TableTypeID int, ItemID int, Created datetime, ApplicationName nvarchar(max), DepartmentID int, Value nvarchar(max), [DeletedUser] INT)
	CREATE CLUSTERED INDEX IDX_C_Event_EventID ON #Event(EventID)
	CREATE NONCLUSTERED INDEX IDX_C_Event_EventTypeID ON #Event(EventTypeID, UserID, DepartmentID, Created)

    INSERT INTO @EventTypeTable (EventTypeID) SELECT value FROM dbo.funcListToTableInt(@EventTypeID,',')

	DECLARE @code varchar(32)
	SET @code = cast(@EventKeyID as varchar)
   	INSERT INTO #EventKeyTable (EventKeyID) SELECT EventKeyID FROM log.EventKey WHERE EventTypeID IN (SELECT EventTypeID FROM @EventTypeTable) AND CodeName = @code
	
    IF @HDID = 0 AND (@UserIDList = '0' OR @UserIDList = '')
    BEGIN
        -- Get all users have this type of event registered
        INSERT INTO #UserIDTable (UserID, [EntityStatus], [Deleted])
		SELECT u.[UserID], CASE WHEN u.EntityStatusID = @EntityStatusID_Deactive THEN NULL ELSE u.EntityStatusID END, CASE WHEN u.Deleted IS NOT NULL THEN NULL ELSE u.Deleted END
		FROM [org].[User] u
		WHERE EXISTS (SELECT 1 FROM [log].[Event] e 
		JOIN [log].[EventInfo] ei ON e.EventID = ei.EventID AND e.UserID = u.UserID
		JOIN @EventTypeTable et ON et.EventTypeID = e.EventTypeID
		JOIN #EventKeyTable ek ON ek.EventKeyID = ei.EventKeyID)
    END
    ELSE IF (@UserIDList != '0' AND @UserIDList != '')
    BEGIN
        INSERT INTO @UserList (UserID) SELECT value FROM dbo.funcListToTableInt(@UserIDList,',')
        
        INSERT INTO #UserIDTable (UserID, [EntityStatus], [Deleted])
        SELECT u.[UserID], u.[EntityStatusID], u.[Deleted] FROM [org].[User] u JOIN @UserList ul ON u.[UserID] = ul.[UserID]
        --JOIN [org].[H_D] hd ON u.[DepartmentID] = hd.[DepartmentID] AND (@HDID = 0 OR hd.[Path] LIKE ' %\' + CONVERT(nvarchar(32),@HDID) + '\%')
		JOIN [org].[H_D] hd ON u.[DepartmentID] = hd.[DepartmentID] AND (@HDID = 0 OR hd.[Path] LIKE @Path)
    END
    ELSE IF @HDID != 0
    BEGIN
        INSERT INTO #UserIDTable (UserID, [EntityStatus], [Deleted])
        SELECT u.[UserID], u.[EntityStatusID], u.[Deleted] FROM [org].[User] u
        --JOIN [org].[H_D] hd ON u.[DepartmentID] = hd.[DepartmentID] AND hd.[Path] LIKE ' %\' + CONVERT(nvarchar(32),@HDID) + '\%'
		JOIN [org].[H_D] hd ON u.[DepartmentID] = hd.[DepartmentID] AND hd.[Path] LIKE @Path
    END

    SET @sqlCommand = 'WITH ev AS (
        SELECT ROW_NUMBER() OVER (PARTITION BY e.[UserID] ORDER BY e.EventID DESC) as [ROW],
               e.EventID, e.EventTypeID, e.UserID, e.IPNumber, e.TableTypeID, e.ItemID, e.Created, e.ApplicationName, e.DepartmentID, ei.Value, CASE WHEN uit.[EntityStatus] = ' + CONVERT(NVARCHAR(14), @EntityStatusID_Active) + ' THEN 0 ELSE 1 END as [DeletedUser]
        FROM log.Event e
        JOIN #UserIDTable uit ON e.[UserID] = uit.[UserID]
        JOIN log.EventInfo ei ON e.EventID = ei.EventID AND e.[EventTypeID] IN (' + @EventTypeID + ')
        JOIN #EventKeyTable ek ON ei.[EventKeyID] = ek.[EventKeyID] AND ei.Value IN (''' + replace(@Value, ',', ''',''') + ''')
        '

    IF @HDID > 0
    BEGIN
        --SET @sqlCommand = @sqlCommand + ' AND EXISTS (SELECT 1 FROM org.H_D hd WHERE hd.DepartmentID = e.DepartmentID AND hd.[Path] LIKE ''%\' + CONVERT(nvarchar(16),@HDID) + '\%'')
        --'
		SET @sqlCommand = @sqlCommand + ' AND EXISTS (SELECT 1 FROM org.H_D hd WHERE hd.DepartmentID = e.DepartmentID AND hd.[Path] LIKE ''' + @Path + ''')
        '
    END

	DECLARE @from nvarchar(32), @to nvarchar(32)
    IF @FromDate IS NOT NULL
    BEGIN
		SET @from = CONVERT(nvarchar(32),@FromDate,120)
        --SET @sqlCommand = @sqlCommand + ' AND cast(e.Created as date) >= ''' + CONVERT(nvarchar(32),@FromDate,120) + '''
        --'
		SET @sqlCommand = @sqlCommand + ' AND e.Created >= ''' + @from + '''
        '
    END

    IF @ToDate IS NOT NULL
    BEGIN
		SET @to = CONVERT(nvarchar(32),DATEADD(DD, 1, @ToDate),120)
        --SET @sqlCommand = @sqlCommand + ' AND cast(e.Created as date) <= ''' + CONVERT(nvarchar(32),@ToDate,120) + '''
        --'
		SET @sqlCommand = @sqlCommand + ' AND e.Created < ''' + @to + '''
        '
    END

	DECLARE @keyID2 nvarchar(32)
    IF @EventKeyID2 > 0
    BEGIN
		SET @keyID2 = CONVERT(nvarchar(32),@EventKeyID2)
    --    SET @sqlCommand = @sqlCommand + ' JOIN log.EventInfo evi2 ON e.EventID = evi2.EventID AND evi2.EventKeyID = ' + CONVERT(nvarchar(32),@EventKeyID2)
    --                                  + ' AND evi2.Value = ''' + @Value2 + '''
    --'
		SET @sqlCommand = @sqlCommand + ' JOIN log.EventInfo evi2 ON e.EventID = evi2.EventID AND evi2.EventKeyID = ' + @keyID2
										  + ' AND evi2.Value = ''' + @Value2 + '''
		'
    END

    SET @sqlCommand = @sqlCommand + ')
    INSERT INTO #Event (EventID, EventTypeID, UserID, IPNumber, TableTypeID, ItemID, Created, ApplicationName, DepartmentID, Value, [DeletedUser])
    SELECT ev.EventID, ev.EventTypeID, ev.UserID, ev.IPNumber, ev.TableTypeID, ev.ItemID, ev.Created, ev.ApplicationName, ev.DepartmentID, ev.Value, ev.[DeletedUser]
    FROM ev
    WHERE [ROW] = 1'

    EXEC sp_executesql @sqlCommand;
    
    WITH ev AS (
    SELECT ROW_NUMBER() OVER (ORDER BY [EventID] DESC) AS rn,
           EventID, EventTypeID, UserID, IPNumber, TableTypeID, ItemID, Created, ApplicationName, DepartmentID, Value, [DeletedUser]
    FROM #Event)
    SELECT ev.EventID, ev.EventTypeID, ev.UserID, ev.IPNumber, ev.TableTypeID, ev.ItemID, ev.Created, ev.ApplicationName, ev.DepartmentID, ev.Value, ev.[DeletedUser]
    FROM ev
    WHERE ev.rn BETWEEN (@PageIndex-1)*@PageSize+1 AND @PageIndex*@PageSize OR @PageSize <= 0
    ORDER BY ev.EventID DESC
   
    DROP TABLE #EventKeyTable
    DROP TABLE #UserIDTable
    DROP TABLE #Event
END

GO
