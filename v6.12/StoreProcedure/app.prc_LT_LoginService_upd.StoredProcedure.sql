SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[app].[prc_LT_LoginService_upd]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [app].[prc_LT_LoginService_upd] AS' 
END
GO
ALTER PROCEDURE [app].[prc_LT_LoginService_upd]
	@LanguageID int,
	@LoginServiceID int,
	@Name nvarchar(512) = NULL,
	@Description nvarchar(max) = NULL,
	@ToolTip nvarchar(512) = NULL,
	@cUserid int,
    @Log smallint = 1
AS
BEGIN
	SET NOCOUNT ON;
	DECLARE @Err Int

    UPDATE [app].[LT_LoginService]
    SET    
         [Name] = @Name,
		 [Description] = @Description,
		 [ToolTip] = @ToolTip
    WHERE
		[LanguageID] = @LanguageID AND
		[LoginServiceID] = @LoginServiceID

    Set @Err = @@Error
	IF @Log = 1 
	BEGIN 
		INSERT INTO [Log].[AuditLog] ( UserId, TableName, Type, Data, Created) 
		SELECT @cUserid,'LT_LoginService',0,
		( SELECT * FROM [app].[LoginService]
			WHERE
			[LoginServiceID] = @LoginServiceID				 FOR XML AUTO) as data,
				getdate() 
	END

	RETURN @Err       
END

GO
