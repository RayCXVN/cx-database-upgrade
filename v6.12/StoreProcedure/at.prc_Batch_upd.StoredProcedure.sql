SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[at].[prc_Batch_upd]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [at].[prc_Batch_upd] AS' 
END
GO
/*  
	2017-06-13 Steve: Change the length of column ExtID in all related tables from xAPI format
*/
ALTER PROCEDURE [at].[prc_Batch_upd]
(
	@BatchID int,
	@SurveyID int,
	@UserID INT=NULL,
	@DepartmentID INT=NULL,
	@Name nvarchar(256),
	@No smallint,
	@StartDate DATETIME=NULL,
	@EndDate DATETIME=NULL,
	@Status smallint,
	@MailStatus smallint,
	@cUserid int,
	@Log smallint = 1,
	@Created DATETIME = NULL,
	@ExtID nvarchar(256)=''
)
AS
BEGIN
	SET NOCOUNT ON
	DECLARE @Err Int

	SET @Created = ISNULL(@Created, GETDATE())

	UPDATE [at].[Batch]
	SET
		[SurveyID] = @SurveyID,
		[UserID] = @UserID,
		[DepartmentID] = @DepartmentID,
		[Name] = @Name,
		[No] = @No,
		[StartDate] = @StartDate,
		[EndDate] = @EndDate,
		[Status] = @Status,
		[MailStatus] = @MailStatus,
		[Created] = @Created,
		[ExtID] = @ExtID
	WHERE
		[BatchID] = @BatchID

	IF @Log = 1 
	BEGIN 
		INSERT INTO [Log].[AuditLog] ( UserId, TableName, Type, Data, Created) 
		SELECT @cUserid,'Batch',1,
		( SELECT * FROM [at].[Batch] 
			WHERE
			[BatchID] = @BatchID			 FOR XML AUTO) as data,
			getdate()
	END

	Set @Err = @@Error

	RETURN @Err
END

GO
