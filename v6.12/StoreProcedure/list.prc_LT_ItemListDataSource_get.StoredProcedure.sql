SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[list].[prc_LT_ItemListDataSource_get]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [list].[prc_LT_ItemListDataSource_get] AS' 
END
GO
ALTER PROCEDURE [list].[prc_LT_ItemListDataSource_get]
	@ItemListDataSourceID int
AS
BEGIN
	SET NOCOUNT ON;
	DECLARE @Err Int
	
    SELECT 
		[LanguageID],
		[ItemListDataSourceID],
		[Name],
		[Description]
	FROM [list].[LT_ItemListDataSource]
	WHERE [ItemListDataSourceID] = @ItemListDataSourceID
	
	Set @Err = @@Error

	RETURN @Err
  
END

GO
