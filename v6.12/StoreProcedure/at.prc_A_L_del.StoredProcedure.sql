SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[at].[prc_A_L_del]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [at].[prc_A_L_del] AS' 
END
GO

ALTER PROCEDURE [at].[prc_A_L_del]
(
	@ActivityID int,
	@LanguageID int,
	@cUserid int,
	@Log smallint = 1
)
AS
BEGIN
	SET NOCOUNT ON
	DECLARE @Err Int

	IF @Log = 1 
	BEGIN 
		INSERT INTO [Log].[AuditLog] ( UserId, TableName, Type, Data, Created) 
		SELECT @cUserid,'A_L',2,
		( SELECT * FROM [at].[A_L] 
			WHERE
			[ActivityID] = @ActivityID AND
			[LanguageID] = @LanguageID
			 FOR XML AUTO) as data,
			getdate() 
	END 


	DELETE FROM [at].[A_L]
	WHERE
		[ActivityID] = @ActivityID AND
		[LanguageID] = @LanguageID

	Set @Err = @@Error

	RETURN @Err
END


GO
