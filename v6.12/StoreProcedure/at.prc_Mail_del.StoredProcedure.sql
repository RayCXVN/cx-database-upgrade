SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[at].[prc_Mail_del]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [at].[prc_Mail_del] AS' 
END
GO


ALTER PROCEDURE [at].[prc_Mail_del]
(
	@MailID int,
	@cUserid int,
	@Log smallint = 1
)
AS
BEGIN
	SET NOCOUNT ON
	DECLARE @Err Int

	IF @Log = 1 
	BEGIN 
		INSERT INTO [Log].[AuditLog] ( UserId, TableName, Type, Data, Created) 
		SELECT @cUserid,'Mail',2,
		( SELECT * FROM [at].[Mail] 
			WHERE
			[MailID] = @MailID
			 FOR XML AUTO) as data,
			getdate() 
	END 


	DELETE FROM [at].[Mail]
	WHERE
		[MailID] = @MailID

	Set @Err = @@Error

	RETURN @Err
END


GO
