SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[sec].[prc_UrlAccess_del]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [sec].[prc_UrlAccess_del] AS' 
END
GO
ALTER PROCEDURE [sec].[prc_UrlAccess_del]  
(  
 @UrlAccessId int,  
 @cUserid int,  
 @Log smallint = 1  
)  
AS  
BEGIN  
 SET NOCOUNT ON  
 DECLARE @Err Int  
  
 IF @Log = 1   
 BEGIN   
  INSERT INTO [Log].[AuditLog] ( UserId, TableName, Type, Data, Created)   
  SELECT @cUserid,'UrlAccess',2,  
  ( SELECT * FROM [sec].[UrlAccess]   
   WHERE  
   [UrlAccessId] = @UrlAccessId  
    FOR XML AUTO) as data,  
   getdate()   
 END   
  
  
 DELETE FROM [sec].[UrlAccess]   
 WHERE  
 [UrlAccessId] = @UrlAccessId  
  
 Set @Err = @@Error  
  
 RETURN @Err  
END  

GO
