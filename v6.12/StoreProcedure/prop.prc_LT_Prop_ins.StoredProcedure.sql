SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[prop].[prc_LT_Prop_ins]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [prop].[prc_LT_Prop_ins] AS' 
END
GO

ALTER PROCEDURE [prop].[prc_LT_Prop_ins]
(
	@LanguageID int,
	@PropertyID int,
	@Name nvarchar(256),
	@Description nvarchar(max),
	@cUserid int,
	@Log smallint = 1
)
AS
BEGIN
	SET NOCOUNT ON
	DECLARE @Err Int

	INSERT INTO [prop].[LT_Prop]
	(
		[LanguageID],
		[PropertyID],
		[Name],
		[Description]
	)
	VALUES
	(
		@LanguageID,
		@PropertyID,
		@Name,
		@Description
	)

	Set @Err = @@Error

	IF @Log = 1 
	BEGIN 
		INSERT INTO [Log].[AuditLog] ( UserId, TableName, Type, Data, Created) 
		SELECT @cUserid,'LT_Prop',0,
		( SELECT * FROM [prop].[LT_Prop] 
			WHERE
			[LanguageID] = @LanguageID AND
			[PropertyID] = @PropertyID				 FOR XML AUTO) as data,
				getdate() 
	 END

	RETURN @Err
END


GO
