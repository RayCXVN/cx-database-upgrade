SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[proc].[prc_ProcessType_get]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [proc].[prc_ProcessType_get] AS' 
END
GO
ALTER PROCEDURE [proc].[prc_ProcessType_get]
(
	@OwnerID int
)
AS
BEGIN
	SET NOCOUNT ON
	DECLARE @Err Int

	SELECT
	[ProcessTypeID],
	[No],
	[OwnerID],
	[CssClass],
	[DepartmentSelect],
	[UserSelect],
	[FilesAbove] ,
	[Created]
	FROM [proc].[ProcessType]
	WHERE
	[OwnerID] = @OwnerID
	ORDER BY [No]

	Set @Err = @@Error

	RETURN @Err
END

GO
