SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[log].[prc_EventGetByEventType]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [log].[prc_EventGetByEventType] AS' 
END
GO
ALTER PROCEDURE [log].[prc_EventGetByEventType]
    @EventTypeID    nvarchar(128)	= '13,14',
    @HDID           int = 0,
    @FromDate       date = NULL,
    @ToDate         date = NULL,
    @ItemId			INT = 0,
    @UserID         INT = 0,
    @PageIndex		INT = 1,
	@PageSize		INT= 10
AS
BEGIN
    DECLARE @ActiveEntityStatusID INT = (SELECT EntityStatusID FROM EntityStatus WHERE CodeName='Active')

	;WITH EventEntries AS 
	(
		
		SELECT ROW_NUMBER()  OVER (ORDER BY e.EventID DESC) as [ROW], e.EventID, e.EventTypeID, e.UserID, e.IPNumber, e.TableTypeID, e.ItemID, e.Created, e.ApplicationName, e.UserData, e.DepartmentID,CASE WHEN u.EntityStatusID = @ActiveEntityStatusID THEN 0 ELSE 1 END as [DeletedUser]
		FROM log.Event e
		INNER JOIN  org.H_D hd ON hd.DepartmentID = e.DepartmentID 
		AND ((@HDID =0 AND e.UserID =@UserID) OR hd.[Path] LIKE '%\' + CONVERT(nvarchar(16),@HDID) + '\%')
		AND  e.EventTypeID IN (select value from  dbo.funcListToTableInt(@EventTypeID,','))
        AND (cast(e.Created as date) >= @FromDate OR @FromDate IS NULL)
        AND (cast(e.Created as date) <= @ToDate OR @ToDate IS NULL)
        AND (@ItemId =0 OR e.ItemID= @ItemId)
        LEFT JOIN org.[User] u ON e.UserID = u.UserID
	)
	SELECT EventID, EventTypeID, UserID, IPNumber, TableTypeID, ItemID, Created, ApplicationName, UserData, DepartmentID,[DeletedUser]
	FROM EventEntries
	WHERE Row BETWEEN
    (@PageIndex - 1) * @PageSize + 1 and @PageIndex*@PageSize
    OR (@PageIndex = 0 AND @PageSize = 0)
END

GO
