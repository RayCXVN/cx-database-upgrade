SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[at].[prc_LevelLimit_get]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [at].[prc_LevelLimit_get] AS' 
END
GO
ALTER PROCEDURE [at].[prc_LevelLimit_get]  
(  
 @CategoryID int = null,  
 @QuestionID int = null,  
 @LevelGroupID int = null  
)  
AS  
BEGIN  
 SET NOCOUNT ON  
 DECLARE @Err Int  
  
 SELECT  
 [LevelLimitID],  
 ISNULL([LevelGroupID], 0) AS 'LevelGroupID',  
 ISNULL([CategoryID], 0) AS 'CategoryID',  
 ISNULL([QuestionID], 0) AS 'QuestionID',  
 ISNULL([AlternativeID], 0) AS 'AlternativeID',  
 [MinValue],  
 [MaxValue],  
 [SigChange],  
 [OwnerColorID],  
 [ItemID],  
 [Created],  
 [NegativeTrend],
 [ExtID],
 [MatchingType]
 FROM [at].[LevelLimit]  
 WHERE  
 (NOT @CategoryID IS NULL AND [CategoryID] = @CategoryID AND [LevelGroupID] IS NULL)  
 OR (NOT @QuestionID IS NULL AND [QuestionID] = @QuestionID AND [LevelGroupID] IS NULL)  
 OR (NOT @LevelGroupID IS NULL AND [LevelGroupID] = @LevelGroupID)  
  
 Set @Err = @@Error  
  
 RETURN @Err  
END

GO
