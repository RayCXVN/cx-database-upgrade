SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[at].[prc_RoleMapping_get]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [at].[prc_RoleMapping_get] AS' 
END
GO

-- [at].[prc_RoleMapping_get] null,33
ALTER PROCEDURE [at].[prc_RoleMapping_get]
(
	@SurveyID int = null,
	@ActivityID int = null
)
AS
BEGIN
	SET NOCOUNT ON
	DECLARE @Err Int

	SELECT
	RMID,
	[RoleID],
	ISNULL([ActivityID],0) as ActivityID,
	ISNULL([SurveyID],0) as SurveyID,
	[UserTypeID]
	FROM [at].[RoleMapping]
	WHERE
	( [SurveyID] = @SurveyID AND @ActivityID IS NULL ) 
	OR ([ActivityID] = @ActivityID AND @SurveyID IS NULL)

	Set @Err = @@Error

	RETURN @Err
END

GO
