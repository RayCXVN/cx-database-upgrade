SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[prc_Result_del]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [dbo].[prc_Result_del] AS' 
END
GO
/* ------------------------------------------------------------
   PROCEDURE:    prc_Result_del

   Description:  Deletes a record from table 'prc_Result_del'

   AUTHOR:       LockwoodTech 11.07.2006 11:47:11
   ------------------------------------------------------------ */

ALTER PROCEDURE [dbo].[prc_Result_del]
(
	@ResultID bigint
)
As
BEGIN
	SET NOCOUNT ON
	DECLARE @Err Int

	DELETE
	FROM [Result]
	WHERE
	[ResultID] = @ResultID

	Set @Err = @@Error

	RETURN @Err
End


GO
