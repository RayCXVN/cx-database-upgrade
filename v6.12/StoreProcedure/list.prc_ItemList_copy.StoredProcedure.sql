SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[list].[prc_ItemList_copy]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [list].[prc_ItemList_copy] AS' 
END
GO

    
  --sp_helptext '[app].[prc_PortalPage_copy]'
ALTER PROCEDURE [list].[prc_ItemList_copy]  
(  
 @ItemListID int,
 @NewItemListID int = null output  
)  
AS  
BEGIN  

 SET XACT_ABORT ON  
 BEGIN TRANSACTION  
 DECLARE  
	@OwnerID int,  
    @ItemListDataSourceID int = null,  
    @ListTableCSSClass nvarchar(64),  
    @ListTableCSSCStyle nvarchar(64),  
    @ListHeaderRowCSSClass nvarchar(64),  
    @ListRowCSSClass nvarchar(64),  
    @ListAltRowCSSClass nvarchar(64),  
    @ExpandingItemListDatasourceID int = null,  
    @ExpandingListRowCSSClass nvarchar(64),  
    @ExpandingListAltRowCSSClass nvarchar(64),  
    @ExpandingExpression nvarchar(64),  
    @InsertMultiCheckColumn bit,  
    @DisableCheckColumnExpression nvarchar(1),  
    @DisplayUpperCmdBarThreshold int,  
    @MaxRows int,  
	@ExtID nvarchar(64) = '',
	@ItemListFieldID INT,
	@NewItemListFieldId INT,
	@ItemListCommandID INT,
	@NewItemListCommandID INT
 SELECT  
		@OwnerID = [OwnerID]  
           ,@ItemListDataSourceID =  [ItemListDataSourceID]  
           ,@ListTableCSSClass = [ListTableCSSClass]  
           ,@ListTableCSSCStyle = [ListTableCSSCStyle]  
           ,@ListHeaderRowCSSClass = [ListHeaderRowCSSClass]  
           ,@ListRowCSSClass = [ListRowCSSClass]  
           ,@ListAltRowCSSClass = [ListAltRowCSSClass]  
           ,@ExpandingItemListDatasourceID = [ExpandingItemListDatasourceID]  
           ,@ExpandingListRowCSSClass = [ExpandingListRowCSSClass]  
           ,@ExpandingListAltRowCSSClass = [ExpandingListAltRowCSSClass]  
           ,@ExpandingExpression = [ExpandingExpression]  
           ,@InsertMultiCheckColumn = [InsertMultiCheckColumn]  
           ,@DisableCheckColumnExpression = [DisableCheckColumnExpression]  
           ,@DisplayUpperCmdBarThreshold = [DisplayUpperCmdBarThreshold]  
           ,@MaxRows = [MaxRows]  
     ,@ExtID = [ExtID]
 FROM [list].[ItemList]   
 WHERE  
  [ItemListID] = @ItemListID  
  
   INSERT INTO [list].[ItemList]  
	 ([OwnerID]  
           ,[ItemListDataSourceID]  
           ,[ListTableCSSClass]  
           ,[ListTableCSSCStyle]  
           ,[ListHeaderRowCSSClass]  
           ,[ListRowCSSClass]  
           ,[ListAltRowCSSClass]  
           ,[ExpandingItemListDatasourceID]  
           ,[ExpandingListRowCSSClass]  
           ,[ExpandingListAltRowCSSClass]  
           ,[ExpandingExpression]  
           ,[InsertMultiCheckColumn]  
           ,[DisableCheckColumnExpression]  
           ,[DisplayUpperCmdBarThreshold]  
           ,[MaxRows]  
     ,[ExtID]
      )    
	 VALUES  
	 ( @OwnerID  
           ,@ItemListDataSourceID  
           ,@ListTableCSSClass  
           ,@ListTableCSSCStyle  
           ,@ListHeaderRowCSSClass  
           ,@ListRowCSSClass  
           ,@ListAltRowCSSClass  
           ,@ExpandingItemListDatasourceID  
           ,@ExpandingListRowCSSClass  
           ,@ExpandingListAltRowCSSClass  
           ,@ExpandingExpression  
           ,@InsertMultiCheckColumn  
           ,@DisableCheckColumnExpression  
           ,@DisplayUpperCmdBarThreshold  
           ,@MaxRows  
     ,@ExtID )    
 
  Set @NewItemListID = scope_identity()  
  
   --insert LT_ItemList
  INSERT INTO [list].[LT_ItemList]
  ([LanguageID]
      ,[ItemListID]
      ,[Name]
      ,[Description]
      ,[RowTooltipText]
      ,[EmptyListText])
  (SELECT [LanguageID]
      ,@NewItemListID
      ,[Name]
      ,[Description]
      ,[RowTooltipText]
      ,[EmptyListText]
   FROM [list].[LT_ItemList]
   WHERE ([ItemListID] = @ItemListID))
 
	--insert ItemList Field
	DECLARE curItemListField CURSOR LOCAL FAST_FORWARD READ_ONLY FOR      
	SELECT ItemListFieldID        
	FROM [list].[ItemListField]    
	WHERE  ([ItemListID] = @ItemListID)

	OPEN curItemListField    
	FETCH NEXT FROM curItemListField INTO @ItemListFieldID    
	    
	WHILE (@@FETCH_STATUS = 0)    
	BEGIN    
		INSERT INTO [list].[ItemListField]  
		([ItemListID]
      ,[No]
      ,[TableTypeID]
      ,[FieldType]
      ,[ValueType]
      ,[FieldName]
      ,[PropID]
      ,[QuestionID]
      ,[OverrideLocked]
      ,[ShowInUserList]
      ,[ShowInTableView]
      ,[ShowInExport]
      ,[Mandatory]
      ,[Sortable]
      ,[SortNo]
      ,[SortDirection]
      ,[Width]
      ,[Align]
      ,[ItemPrefix]
      ,[ItemSuffix]
      ,[HeaderCssClass]
      ,[ItemCssClass]
      ,[Created]
      ,[ColumnCssClass]
      ,[HeaderGroupCSSClass]
      ,[HeaderGroupLastItem])  
		 (SELECT @NewItemListID  
      ,[No]
      ,[TableTypeID]
      ,[FieldType]
      ,[ValueType]
      ,[FieldName]
      ,[PropID]
      ,[QuestionID]
      ,[OverrideLocked]
      ,[ShowInUserList]
      ,[ShowInTableView]
      ,[ShowInExport]
      ,[Mandatory]
      ,[Sortable]
      ,[SortNo]
      ,[SortDirection]
      ,[Width]
      ,[Align]
      ,[ItemPrefix]
      ,[ItemSuffix]
      ,[HeaderCssClass]
      ,[ItemCssClass]
      ,[Created]
      ,[ColumnCssClass]
      ,[HeaderGroupCSSClass]
      ,[HeaderGroupLastItem]
	   FROM [list].[ItemListField]  
	   WHERE ([ItemListFieldID] = @ItemListFieldID))  
	     
	      
	   SET @NewItemListFieldId = SCOPE_IDENTITY()    
	     
	   --insert LT_ItemListField  
	   INSERT INTO [list].[LT_ItemListField]  
			   ( [LanguageID]
      ,[ItemListFieldID]
      ,[Name]
      ,[Description]
      ,[HeaderText]
      ,[HeaderGroupText]
      ,[HeaderTooltipText]
      ,[ItemTooltipText]
      ,[ItemText]
      ,[ExpandingItemText]
      ,[DefaultValues]
      ,[AvailableValues]
			   )  
	   (SELECT  
		   [LanguageID]
		  ,@NewItemListFieldId
		  ,[Name]
		  ,[Description]
		  ,[HeaderText]
		  ,[HeaderGroupText]
		  ,[HeaderTooltipText]
		  ,[ItemTooltipText]
		  ,[ItemText]
		  ,[ExpandingItemText]
		  ,[DefaultValues]
		  ,[AvailableValues]
		FROM [list].[LT_ItemListField]  
		WHERE [ItemListFieldID] = @ItemListFieldID)  
		
		
		--insert List FieldAttribute
	   INSERT INTO [list].[ItemListFieldAttribute]
			   ( [ItemListFieldID]
			  ,[key]
			  ,[Value]
			   )  
	   (SELECT  
			   @NewItemListFieldId
			  ,[key]
			  ,[Value]
		FROM [list].[ItemListFieldAttribute]  
		WHERE [ItemListFieldID] = @ItemListFieldID)  
		
		--insert List FieldAttribute
	   INSERT INTO [list].[ItemListFieldCondition]
			   ( [ItemListFieldID]
				  ,[Type]
				  ,[Param]
				  ,[Value]
			   )  
	   (SELECT  
			   @NewItemListFieldId
			  ,[Type]
			  ,[Param]
			  ,[Value]
		FROM [list].[ItemListFieldCondition]  
		WHERE [ItemListFieldID] = @ItemListFieldID)  
		
			--insert List Field Expanding Attribute
	   INSERT INTO [list].[ItemListFieldExpandingAttribute]
			   (  [ItemListFieldID]
				  ,[key]
				  ,[Value]
			   )  
	   (SELECT  
			   @NewItemListFieldId
			  ,[key]
			  ,[Value]
		FROM [list].[ItemListFieldExpandingAttribute]  
		WHERE [ItemListFieldID] = @ItemListFieldID)  
		
		FETCH NEXT FROM curItemListField INTO @ItemListFieldID    
	END    
	CLOSE curItemListField    
	DEALLOCATE curItemListField    
	
	--insert ItemListCommand
	DECLARE curItemListCommand CURSOR LOCAL FAST_FORWARD READ_ONLY FOR      
	SELECT ItemListCommandID        
	FROM [list].[ItemListCommand]    
	WHERE  ([ItemListID] = @ItemListID)
	
	OPEN curItemListCommand    
	FETCH NEXT FROM curItemListCommand INTO @ItemListCommandID    
	    
	WHILE (@@FETCH_STATUS = 0)    
	BEGIN    
		INSERT INTO [list].[ItemListCommand]  
		([ItemListID]
      ,[PlaceAbove]
      ,[PlaceBelow]
      ,[MultiCheckCommand]
      ,[ClientFunction]
      ,[DisabledInEmptyList]
      ,[CssClass]
      ,[Created])  
		 (SELECT @NewItemListID  
      ,[PlaceAbove]
      ,[PlaceBelow]
      ,[MultiCheckCommand]
      ,[ClientFunction]
      ,[DisabledInEmptyList]
      ,[CssClass]
      ,[Created]
	   FROM [list].[ItemListCommand]  
	   WHERE ([ItemListCommandID] = @ItemListCommandID))  
	     
	   SET @NewItemListCommandId = SCOPE_IDENTITY()    
	     
	   --insert LT_ItemListCommand  
	   INSERT INTO [list].[LT_ItemListCommand]  
			   ([LanguageID]
      ,[ItemListCommandID]
      ,[Name]
      ,[Description]
      ,[Text]
      ,[TooltipText]
			   )  
	   (SELECT  
		 [LanguageID]
		  ,@NewItemListCommandId
		  ,[Name]
		  ,[Description]
		  ,[Text]
		  ,[TooltipText]
		FROM [list].[LT_ItemListCommand]  
		WHERE [ItemListCommandID] = @ItemListCommandID)  
	        
		FETCH NEXT FROM curItemListCommand INTO @ItemListCommandID    
	END    
	CLOSE curItemListCommand    
	DEALLOCATE curItemListCommand 
	
	
	
  --insert ItemList RowAttribute
  	 INSERT INTO [list].[ItemListRowAttribute]
			   ([ItemListID]
      ,[Key]
      ,[Value]
			   )  
	   (SELECT  
	  @NewItemListID
      ,[Key]
      ,[Value]
		FROM [list].[ItemListRowAttribute]  
		WHERE [ItemListID] = @ItemListID)  
	
 COMMIT TRANSACTION  
    
END  
  
  

GO
