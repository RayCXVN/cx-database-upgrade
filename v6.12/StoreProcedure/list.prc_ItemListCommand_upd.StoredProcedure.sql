SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[list].[prc_ItemListCommand_upd]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [list].[prc_ItemListCommand_upd] AS' 
END
GO
ALTER PROCEDURE [list].[prc_ItemListCommand_upd]
	@ItemListCommandID int,
	@ItemListID int,
    @PlaceAbove bit,
    @PlaceBelow bit,
    @MultiCheckCommand bit,
    @DisabledInEmptyList bit,
    @CssClass nvarchar(256)='',
    @ClientFunction nvarchar(128),
    @cUserid int,
    @Log smallint = 1,
    @No int =0
AS
BEGIN
	SET NOCOUNT ON;
	DECLARE @Err Int
	
    UPDATE [list].[ItemListCommand]
    SET 
		[ItemListID] = @ItemListID,
        [PlaceAbove] = @PlaceAbove,
        [PlaceBelow] = @PlaceBelow,
        [MultiCheckCommand] = @MultiCheckCommand,
        [DisabledInEmptyList] = @DisabledInEmptyList,
        [CssClass] = @CssClass,
        [ClientFunction] = @ClientFunction,
        [No]=@No
     WHERE 
		[ItemListCommandID] = @ItemListCommandID
		
	IF @Log = 1 
	BEGIN 
		INSERT INTO [Log].[AuditLog] ( UserId, TableName, Type, Data, Created) 
		SELECT @cUserid,'ItemListCommand',1,
		( SELECT * FROM [list].[ItemListCommand] 
			WHERE
			[ItemListCommandID] = @ItemListCommandID			 FOR XML AUTO) as data,
			getdate()
	END

	Set @Err = @@Error

	RETURN @Err	
END

GO
