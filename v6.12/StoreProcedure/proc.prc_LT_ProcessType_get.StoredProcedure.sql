SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[proc].[prc_LT_ProcessType_get]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [proc].[prc_LT_ProcessType_get] AS' 
END
GO
ALTER PROCEDURE [proc].[prc_LT_ProcessType_get]
(
	@ProcessTypeID int
)
AS
BEGIN
	SET NOCOUNT ON
	DECLARE @Err Int

	SELECT
	[ProcessTypeID],
	[LanguageID],
	[Name],
	[Description],
	[DescriptionFieldName] ,
	[Helptext],
	[WizardStep1],
	[WizardStep2],
	[WizardStep3],
	[WizardStep4],
	[WizardStep5],
	[AnswerHeader],
	[WizardStep1Desc],
	[WizardStep2Desc],
	[WizardStep3Desc],
	[WizardStep4Desc],
	[WizardStep5Desc]
	FROM [proc].[LT_ProcessType]
	WHERE
	[ProcessTypeID] = @ProcessTypeID

	Set @Err = @@Error

	RETURN @Err
END

GO
