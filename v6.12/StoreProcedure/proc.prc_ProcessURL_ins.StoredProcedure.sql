SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[proc].[prc_ProcessURL_ins]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [proc].[prc_ProcessURL_ins] AS' 
END
GO

ALTER PROCEDURE [proc].[prc_ProcessURL_ins]
(
	@ProcessURLID int = null output,
	@ProcessID int,
	@URL nvarchar(512),
	@Name nvarchar(128),
	@Target nvarchar(32),
	@No smallint,
	@cUserid int,
	@Log smallint = 1
)
AS
BEGIN
	SET NOCOUNT ON
	DECLARE @Err Int

	INSERT INTO [proc].[ProcessURL]
	(
		[ProcessID],
		[URL],
		[Name],
		[Target],
		[No]
	)
	VALUES
	(
		@ProcessID,
		@URL,
		@Name,
		@Target,
		@No
	)

	Set @Err = @@Error
	Set @ProcessURLID = scope_identity()

	IF @Log = 1 
	BEGIN 
		INSERT INTO [Log].[AuditLog] ( UserId, TableName, Type, Data, Created) 
		SELECT @cUserid,'ProcessURL',0,
		( SELECT * FROM [proc].[ProcessURL] 
			WHERE
			[ProcessURLID] = @ProcessURLID				 FOR XML AUTO) as data,
				getdate() 
	 END

	RETURN @Err
END

GO
