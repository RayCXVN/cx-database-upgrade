SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[at].[prc_Page_getByOwner]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [at].[prc_Page_getByOwner] AS' 
END
GO
ALTER PROCEDURE [at].[prc_Page_getByOwner]  
(  
 @OwnerID int  
)  
AS  
BEGIN  
 SET NOCOUNT ON  
 DECLARE @Err Int  
  
 SELECT  
 [PageID],  
 ISNULL([ActivityID], 0) AS 'ActivityID',  
 [No],  
 ISNULL([Created], 0) AS 'Created',  
 [Type],  
 [CssClass],
 [ExtID],
 [Tag]
 FROM [at].[Page]  
 WHERE  
 [ActivityID] IN (select ActivityID from at.Activity where OwnerID = @OwnerID)
 ORDER BY [No]  
  
 Set @Err = @@Error  
  
 RETURN @Err  
END
GO
