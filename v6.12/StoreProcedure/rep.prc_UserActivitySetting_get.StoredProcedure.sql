SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[rep].[prc_UserActivitySetting_get]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [rep].[prc_UserActivitySetting_get] AS' 
END
GO
ALTER PROCEDURE [rep].[prc_UserActivitySetting_get]
 @UserID int,
 @ActivityID int
AS      
BEGIN      
 SET NOCOUNT ON;      
 DECLARE @Err Int          
 SELECT       
	 [UserActivitySettingID]
	,[UserID]
	,[ActivityID]
	,[ShowTable]
	,[ShowChart]
	,[ChartTypeID]
	,[ChartTypeIDAvg]
	,[ShowPage]
	,[ShowCategory]
	,[ShowCategoryType]
 FROM             
  [rep].[UserActivitySetting]     
 WHERE        
  [UserID] = @UserID      
  AND [ActivityID] = @ActivityID
 Set @Err = @@Error      
      
 RETURN @Err      
END 

GO
