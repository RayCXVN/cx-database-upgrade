SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[app].[prc_PortalPage_get]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [app].[prc_PortalPage_get] AS' 
END
GO

ALTER PROCEDURE [app].[prc_PortalPage_get]
(
	@OwnerID int
)
AS
BEGIN
	SET NOCOUNT ON
	DECLARE @Err Int

	SELECT
	[PortalPageID],
	[OwnerID],
	[No],
	[Parameter]
	FROM [app].[PortalPage]
	WHERE
	[OwnerID] = @OwnerID
	ORDER BY [No]

	Set @Err = @@Error

	RETURN @Err
END


GO
