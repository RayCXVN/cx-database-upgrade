SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[org].[prc_Department_del]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [org].[prc_Department_del] AS' 
END
GO


ALTER PROCEDURE [org].[prc_Department_del]
(
	@DepartmentID int,
	@cUserid int,
	@Log smallint = 1
)
AS
BEGIN
	SET NOCOUNT ON
	DECLARE @Err Int

	IF @Log = 1 
	BEGIN 
		INSERT INTO [Log].[AuditLog] ( UserId, TableName, Type, Data, Created) 
		SELECT @cUserid,'Department',2,
		( SELECT * FROM [org].[Department] 
			WHERE
			[DepartmentID] = @DepartmentID
			 FOR XML AUTO) as data,
			getdate() 
	END 


	DELETE FROM [org].[Department]
	WHERE
	[DepartmentID] = @DepartmentID

	Set @Err = @@Error

	RETURN @Err
END


GO
