SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[at].[prc_LT_Activity_upd]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [at].[prc_LT_Activity_upd] AS' 
END
GO
ALTER PROCEDURE [at].[prc_LT_Activity_upd]
(
	@LanguageID int,
	@ActivityID int,
	@Name nvarchar(256),
	@Info nvarchar(max),
	@StartText nvarchar(max),
	@Description nvarchar(max),
	@RoleName nvarchar(128) = '',
	@SurveyName nvarchar(128) = '',
	@BatchName nvarchar(128) = '',
	@DisplayName    nvarchar (256)='',
	@ShortName      nvarchar (256)='',
	@cUserid int,
	@Log smallint = 1
)
AS
BEGIN
	SET NOCOUNT ON
	DECLARE @Err Int

	UPDATE [at].[LT_Activity]
	SET
		[LanguageID] = @LanguageID,
		[ActivityID] = @ActivityID,
		[Name] = @Name,
		[Info] = @Info,
		[StartText] = @StartText,
		[Description] = @Description,
		[RoleName] = @RoleName,
		[SurveyName] = @SurveyName , 
		[BatchName] = @BatchName,
		[DisplayName] =@DisplayName,
		[ShortName] = @ShortName
	WHERE
		[LanguageID] = @LanguageID AND
		[ActivityID] = @ActivityID

	IF @Log = 1 
	BEGIN 
		INSERT INTO [Log].[AuditLog] ( UserId, TableName, Type, Data, Created) 
		SELECT @cUserid,'LT_Activity',1,
		( SELECT * FROM [at].[LT_Activity] 
			WHERE
			[LanguageID] = @LanguageID AND
			[ActivityID] = @ActivityID			 FOR XML AUTO) as data,
			getdate()
	END

	Set @Err = @@Error

	RETURN @Err
END

GO
