SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[org].[prc_H_D_ins]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [org].[prc_H_D_ins] AS' 
END
GO
ALTER PROCEDURE [org].[prc_H_D_ins]
(
	@HDID int = null output,
	@HierarchyID int,
	@DepartmentID int,
	@ParentID INT=NULL,
	@Path nvarchar(900),
	@PathName nvarchar(4000),
	@Deleted smallint,
	@cUserid int,
	@Log smallint = 1
)
AS
BEGIN
	SET NOCOUNT ON
	DECLARE @Err Int
	IF @ParentID = 0  SET @ParentID = NULL
	INSERT INTO [org].[H_D]
	(
		[HierarchyID],
		[DepartmentID],
		[ParentID],
		[Path],
		[PathName],
		[Deleted]
	)
	VALUES
	(
		@HierarchyID,
		@DepartmentID,
		@ParentID,
		@Path,
		@PathName,
		@Deleted
	)

	Set @Err = @@Error
	Set @HDID = scope_identity()

	IF @Log = 1 
	BEGIN 
		INSERT INTO [Log].[AuditLog] ( UserId, TableName, Type, Data, Created) 
		SELECT @cUserid,'H_D',0,
		( SELECT * FROM [org].[H_D] 
			WHERE
			[HDID] = @HDID				 FOR XML AUTO) as data,
				getdate() 
	 END

	RETURN @Err
END

GO
