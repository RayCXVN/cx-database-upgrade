SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[prop].[prc_PropFile_sel]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [prop].[prc_PropFile_sel] AS' 
END
GO


ALTER PROCEDURE [prop].[prc_PropFile_sel]
(
	@PropFileId int,
	@OwnerId int
)
AS
BEGIN
	SET NOCOUNT ON
	DECLARE @Err Int

	SELECT [PropFileId],[GUID],[OwnerId],[CustomerId],[ContentType],[Size],[Title],[FileName],[Description]
	FROM [prop].[PropFile]
	WHERE [PropFileId] = @PropFileId AND OwnerId  = @OwnerId AND Deleted  = 0

	Set @Err = @@Error

	RETURN @Err
END


GO
