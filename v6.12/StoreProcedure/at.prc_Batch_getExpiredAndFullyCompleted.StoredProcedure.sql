SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[at].[prc_Batch_getExpiredAndFullyCompleted]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [at].[prc_Batch_getExpiredAndFullyCompleted] AS' 
END
GO
/*
    Johnny - Des 07 2016 - Update store procedure for dynamic SQL
*/
ALTER PROCEDURE [at].[prc_Batch_getExpiredAndFullyCompleted]
(	@SurveyID int, 
	@DepartmentID int
)
AS
BEGIN
	DECLARE
	@ReportServer nvarchar(64),
	@ReportDB nvarchar(64),
	@Sqlcommand nvarchar(max),
	@Parameters	nvarchar(max) = N'',
	@ActiveEntityStatusID INT = 0
	SELECT @ActiveEntityStatusID=EntityStatusID FROM  EntityStatus WHERE CodeName='Active'
	SELECT @ReportServer = ReportServer, @ReportDB = ReportDB FROM at.Survey WHERE SurveyID = @SurveyID
	DECLARE @LinkedDB NVARCHAR(MAX)
	IF @ReportDB=DB_NAME() AND @ReportServer=@@servername
		BEGIN
		SET @LinkedDB=' '
		END
	ELSE
		BEGIN
		SET @LinkedDB= '['+ @ReportServer + '].['+ @ReportDB + '].'
		END

	SET @sqlCommand = N'SELECT @p_ActiveEntityStatusID = EntityStatusID FROM '+ @LinkedDB +'dbo.EntityStatus WHERE CodeName = ''Active'''
	EXECUTE sp_executesql @sqlCommand, N'@p_ActiveEntityStatusID INT OUTPUT', @p_ActiveEntityStatusID = @ActiveEntityStatusID OUTPUT

	SET @Sqlcommand = 'SELECT b.*
	FROM at.Batch b 
	WHERE b.DepartmentID = @p_DepartmentID AND b.SurveyID = @p_SurveyID AND b.status > 0 AND b.Startdate is not null AND b.EndDate is not null 
	AND EXISTS (SELECT 1 FROM '+ @LinkedDB +'[dbo].Result r2
	            WHERE r2.BatchID = b.BatchID AND r2.SurveyID = b.SurveyID AND r2.[EntityStatusID] = '+CONVERT(NVARCHAR(14),@ActiveEntityStatusID)+' AND r2.Deleted IS NULL)
    AND (CONVERT(date, b.EndDate) < CONVERT(date, getdate())
	     OR 	
	     NOT EXISTS (SELECT 1 FROM '+ @LinkedDB +'[dbo].Result r 
	                 WHERE r.BatchID = b.BatchID AND r.SurveyID = b.SurveyID AND r.[EntityStatusID] = '+CONVERT(NVARCHAR(14),@ActiveEntityStatusID)+' AND r.Deleted IS NULL AND r.EndDate is null)
	    )
	ORDER BY b.Created desc'

	SET @Parameters = N'@p_DepartmentID int, @p_SurveyID int'
	EXECUTE sp_executesql @sqlCommand, @Parameters, @p_DepartmentID = @DepartmentID, @p_SurveyID = @SurveyID
END

GO
