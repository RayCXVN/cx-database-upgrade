SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[log].[prc_BubbleLog_del]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [log].[prc_BubbleLog_del] AS' 
END
GO

ALTER PROCEDURE [log].[prc_BubbleLog_del]
(
	@BubbleLogID int,
	@cUserid int,
	@Log smallint = 1
)
AS
BEGIN
	SET NOCOUNT ON
	DECLARE @Err Int

	IF @Log = 1 
	BEGIN 
		INSERT INTO [Log].[AuditLog] ( UserId, TableName, Type, Data, Created) 
		SELECT @cUserid,'BubbleLog',2,
		( SELECT * FROM [log].[BubbleLog] 
			WHERE
			[BubbleLogID] = @BubbleLogID
			 FOR XML AUTO) as data,
			getdate() 
	END 


	DELETE FROM [log].[BubbleLog]
	WHERE
		[BubbleLogID] = @BubbleLogID

	Set @Err = @@Error

	RETURN @Err
END


GO
