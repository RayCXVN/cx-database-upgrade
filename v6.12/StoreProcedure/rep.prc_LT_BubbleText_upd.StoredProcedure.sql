SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[rep].[prc_LT_BubbleText_upd]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [rep].[prc_LT_BubbleText_upd] AS' 
END
GO

ALTER PROCEDURE [rep].[prc_LT_BubbleText_upd]
(
	@LanguageID int,
	@BubbleTextID int,
	@Name nvarchar(256),
	@Description nvarchar(max),
	@cUserid int,
	@Log smallint = 1
)
AS
BEGIN
	SET NOCOUNT ON
	DECLARE @Err Int

	UPDATE [rep].[LT_BubbleText]
	SET
		[LanguageID] = @LanguageID,
		[BubbleTextID] = @BubbleTextID,
		[Name] = @Name,
		[Description] = @Description
	WHERE
		[LanguageID] = @LanguageID AND
		[BubbleTextID] = @BubbleTextID

	IF @Log = 1 
	BEGIN 
		INSERT INTO [Log].[AuditLog] ( UserId, TableName, Type, Data, Created) 
		SELECT @cUserid,'LT_BubbleText',1,
		( SELECT * FROM [rep].[LT_BubbleText] 
			WHERE
			[LanguageID] = @LanguageID AND
			[BubbleTextID] = @BubbleTextID			 FOR XML AUTO) as data,
			getdate()
	END

	Set @Err = @@Error

	RETURN @Err
END


GO
