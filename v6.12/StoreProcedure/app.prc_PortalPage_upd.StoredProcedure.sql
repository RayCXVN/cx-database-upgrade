SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[app].[prc_PortalPage_upd]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [app].[prc_PortalPage_upd] AS' 
END
GO
/*
	2017-06-13 Steve: Change the length of column ExtID in all related tables from xAPI format
*/
ALTER PROCEDURE [app].[prc_PortalPage_upd]
(
	@PortalPageID int,
	@OwnerID int,
	@No smallint,
	@Parameter nvarchar(1024) = '',
	@cUserid int,
	@Log smallint = 1,
	@Created DATETIME = NULL,
	@ExtID nvarchar(256)=''
)
AS
BEGIN
	SET NOCOUNT ON
	DECLARE @Err Int

	SET @Created = ISNULL(@Created, GETDATE())

	UPDATE [app].[PortalPage]
	SET
		[OwnerID] = @OwnerID,
		[No] = @No,
		[Parameter] = @Parameter ,
		[Created] = @Created,
		[ExtID] = @ExtID
	WHERE
		[PortalPageID] = @PortalPageID

	IF @Log = 1 
	BEGIN 
		INSERT INTO [Log].[AuditLog] ( UserId, TableName, Type, Data, Created) 
		SELECT @cUserid,'PortalPage',1,
		( SELECT * FROM [app].[PortalPage] 
			WHERE
			[PortalPageID] = @PortalPageID			 FOR XML AUTO) as data,
			getdate()
	END

	Set @Err = @@Error

	RETURN @Err
END


GO
