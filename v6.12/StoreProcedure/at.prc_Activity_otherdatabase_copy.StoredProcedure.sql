/****** Object:  StoredProcedure [at].[prc_Activity_otherdatabase_copy]    Script Date: 4/18/2017 11:27:28 AM ******/
DROP PROCEDURE [at].[prc_Activity_otherdatabase_copy]
GO
/****** Object:  StoredProcedure [at].[prc_Activity_otherdatabase_copy]    Script Date: 4/18/2017 11:27:30 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- [at].[prc_Activity_otherdatabase_copy] 28,null,7,6,20,1 --Select * from org.owner , 

create PROC [at].[prc_Activity_otherdatabase_copy]
(	
	@ActivityID	INT,
	@NewAID		INT = NULL OUTPUT,
	@OldOwnerID INT = 0,
	@NewOwnerID INT = 0,
	@CopyMenuID INT = 0,
	@CopyPortalParts INT = 0,
	@CopyBubbles INT = 1,
	@CopyScoreTemplate INT = 1,
	@CopyChoice int = 1,
	@CopyCVView int = 1
)
AS
BEGIN
SET NOCOUNT ON
SET XACT_ABORT ON

BEGIN TRANSACTION

CREATE TABLE #Page
(
	ID	BIGINT,
	NEWID BIGINT
)
CREATE TABLE #Scale
(
	ID	BIGINT,
	NEWID BIGINT
)
CREATE TABLE #Question
(
	ID	BIGINT,
	NEWID BIGINT
)

CREATE TABLE #Alternative
(
	oldID	BIGINT,
	NEWID BIGINT
)

CREATE TABLE #Category
(
	ID	BIGINT,
	NEWID BIGINT
)
CREATE TABLE #LevelGroup
(
 ID	BIGINT,
 NEWID BIGINT
)
CREATE TABLE #LevelLimit
(
 ID	BIGINT,
 NEWID BIGINT
)

CREATE TABLE #PortalPage
(
 ID	BIGINT,
 NEWID BIGINT
)

CREATE TABLE #PortalPart
(
 ID	BIGINT,
 NEWID BIGINT
)

CREATE TABLE #Menu
(
 ID	BIGINT,
 NEWID BIGINT
)

CREATE TABLE #MenuItem
(
 ID	BIGINT,
 NEWID BIGINT
)

CREATE TABLE #Bubble
(
 ID	BIGINT,
 NEWID BIGINT
)

CREATE TABLE #BubbleText
(
 ID	BIGINT,
 NEWID BIGINT
)

CREATE TABLE #CalcField
(
 ID INT,
 NEWID INT
)

CREATE TABLE #ScoreTemplate
(
 ID INT,
 NEWID INT
)

CREATE TABLE #Section
(
 ID INT,
 NEWID INT
)

CREATE TABLE #A_VT
(
 ID INT,
 NEWID INT
)

CREATE TABLE #CVView
(
 ID INT,
 NEWID INT
)
CREATE TABLE #CVV_Q
(
 ID INT,
 NEWID INT
)

DECLARE
@ID	INT,
@NewID INT,
@CurOwnerID INT

IF @NewOwnerid > 0
BEGIN
  SET @CurOwnerID = @NewOwnerid
END
ELSE IF @OldOwnerid > 0
BEGIN
  SET @CurOwnerID = @OldOwnerid
END
ELSE 
BEGIN
  SELECT @CurOwnerID = Ownerid FROM at5q_common/*Sourcedatabase*/.AT.Activity WHERE ActivityID = @ActivityID
END

-- Activity
INSERT AT.Activity(LanguageID, OwnerID, StyleSheet, [TYPE], NO, TooltipType, Listview, Descview, Chartview, OLAPServer, OLAPDB, Created,SelectionHeaderType )  
SELECT  LanguageID, @CurOwnerID, StyleSheet, [TYPE], NO, TooltipType, Listview, Descview, Chartview, OLAPServer, OLAPDB, getdate(),SelectionHeaderType  
FROM at5q_common/*Sourcedatabase*/.at.Activity WHERE ActivityID = @ActivityID
SET @NewAID = SCOPE_IDENTITY()

INSERT INTO AT.LT_Activity(LanguageID, ActivityID, Name, Info, StartText, Description)
SELECT LanguageID, @NewAID, Name, Info, StartText, Description
FROM at5q_common/*Sourcedatabase*/.AT.LT_Activity WHERE ActivityID = @ActivityID

INSERT at.A_L(ActivityID, LanguageID)
SELECT @NewAID, LanguageID FROM at5q_common/*Sourcedatabase*/.at.A_L WHERE ActivityID = @ActivityID


 -- A_CalcType
   INSERT INTO AT.A_CalcType (ReportCalcTypeID, ActivityID, NO, DefaultSelected, Format, Created)
   SELECT ReportCalcTypeID, @NewAID, NO, DefaultSelected, Format, GETDATE()
   FROM   at5q_common/*Sourcedatabase*/.AT.A_CalcType WHERE ActivityID = @ActivityID
   
   
   -- A_GraphType
   INSERT INTO AT.A_GraphType (ReportGraphTypeID, ActivityID, NO, DefaultSelected, Created)
   SELECT  ReportGraphTypeID, @NewAID, NO, DefaultSelected, GETDATE()
   FROM at5q_common/*Sourcedatabase*/.AT.A_GraphType WHERE ActivityID = @ActivityID
   

--Role
IF @NewOwnerID = 0
BEGIN
    INSERT at.XC_A(XCID, ActivityID)
    SELECT XCID, @NewAID FROM at5q_common/*Sourcedatabase*/.at.XC_A WHERE ActivityID = @ActivityID

	INSERT INTO AT.A_R(ActivityID, RoleID, NO)
	SELECT @NewAID, RoleID, NO
	FROM at5q_common/*Sourcedatabase*/.AT.A_R WHERE ActivityID = @ActivityID
	
	INSERT INTO AT.A_S(ActivityID, StatusTypeID, NO)
	SELECT @NewAID, StatusTypeID, NO
	FROM at5q_common/*Sourcedatabase*/.AT.A_S WHERE ActivityID = @ActivityID
	
		
	DECLARE curA_VT CURSOR LOCAL FAST_FORWARD READ_ONLY FOR
	SELECT AVTID
	FROM at5q_common/*Sourcedatabase*/.at.A_VT
	WHERE ActivityID = @ActivityID

	OPEN curA_VT
	FETCH NEXT FROM curA_VT INTO @ID

	WHILE (@@FETCH_STATUS = 0)
	BEGIN
		INSERT at.A_VT(ViewTypeID, ActivityID, No, Type, Template)
		SELECT ViewTypeID,@NewAID, [NO],Type,Template
		FROM at5q_common/*Sourcedatabase*/.AT.A_VT	
		WHERE  AVTID= @ID
		SET @NewID = SCOPE_IDENTITY()

		INSERT #A_VT(ID, NEWID) VALUES(@ID, @NewID)

		FETCH NEXT FROM curA_VT INTO @ID
	END
	CLOSE curA_VT
	DEALLOCATE curA_VT
	INSERT AT.LT_A_VT(LanguageID, AVTID, Name, Description, TextAbove, TextBelow)
	SELECT LanguageID, P.[NEWID], Name, Description, TextAbove, TextBelow
	FROM #A_VT P
	INNER JOIN AT.LT_A_VT L ON P.ID = L.AVTID
		
		
END

-- RoleMapping
INSERT INTO AT.RoleMapping(RoleID, SurveyID, ActivityID, UserTypeID)
SELECT RoleID, SurveyID, @NewAID, UserTypeID
FROM at5q_common/*Sourcedatabase*/.AT.RoleMapping WHERE ActivityID = @ActivityID AND SurveyID IS NULL


-- Page
DECLARE curSec CURSOR LOCAL FAST_FORWARD READ_ONLY FOR
SELECT PageID
FROM at5q_common/*Sourcedatabase*/.AT.Page 
WHERE ActivityID = @ActivityID

OPEN curSec
FETCH NEXT FROM curSec INTO @ID

WHILE (@@FETCH_STATUS = 0)
BEGIN
	INSERT AT.Page(ActivityID, [NO],[Type],[CssClass])
	SELECT @NewAID, [NO],[Type],CssClass
	FROM at5q_common/*Sourcedatabase*/.AT.Page	
	WHERE PageID = @ID
	SET @NewID = SCOPE_IDENTITY()

	INSERT #Page(ID, NEWID) VALUES(@ID, @NewID)

	FETCH NEXT FROM curSec INTO @ID
END
CLOSE curSec
DEALLOCATE curSec
INSERT AT.LT_Page(LanguageID, PageID, Name, Info, Description)
SELECT LanguageID, P.[NEWID], Name, Info, Description
FROM #Page P
INNER JOIN at5q_common/*Sourcedatabase*/.AT.LT_Page L ON P.ID = L.PageID


-- Scale
DECLARE curAG CURSOR LOCAL FAST_FORWARD READ_ONLY FOR 
SELECT ScaleID
FROM at5q_common/*Sourcedatabase*/.AT.Scale 
WHERE ActivityID = @ActivityID

OPEN curAG
FETCH NEXT FROM curAG INTO @ID

WHILE (@@FETCH_STATUS = 0)
BEGIN
	INSERT AT.Scale(ActivityID, MinSelect, MaxSelect, TYPE, MinValue, MaxValue,Tag,Extid )
	SELECT @NewAID, MinSelect, MaxSelect, TYPE, MinValue, MaxValue, Tag,ScaleID 
	FROM at5q_common/*Sourcedatabase*/.at.Scale
	WHERE ScaleID = @ID
	SET @NewID = SCOPE_IDENTITY()
		
	INSERT AT.LT_Scale(LanguageID, ScaleID, Name, Title, Description)
	SELECT LanguageID, @NewID, Name, Title, Description
	FROM at5q_common/*Sourcedatabase*/.AT.LT_Scale
	WHERE ScaleID = @ID 

	INSERT #Scale(ID, NEWID) VALUES(@ID, @NewID)

	FETCH NEXT FROM curAG INTO @ID
END
CLOSE curAG
DEALLOCATE curAG

-- Alternative
DECLARE curAlt CURSOR LOCAL FAST_FORWARD READ_ONLY FOR 
SELECT AlternativeID
FROM at5q_common/*Sourcedatabase*/.AT.Alternative A
INNER JOIN #Scale T ON A.ScaleID = T.ID

OPEN curAlt
FETCH NEXT FROM curAlt INTO @ID

WHILE (@@FETCH_STATUS = 0)
BEGIN
	INSERT AT.Alternative(ScaleID, TYPE, NO, VALUE, InvertedValue, Calc, SC, MinValue, MaxValue, Format, SIZE, CssClass, AGID, DefaultValue,EXTID,width,OwnerColorID,DefaultCalcType)
	SELECT T.NEWID, TYPE, NO, VALUE, InvertedValue, Calc, SC, MinValue, MaxValue, Format, SIZE, CssClass, AGID, DefaultValue,a.AlternativeID  ,width,OwnerColorID,DefaultCalcType
	FROM at5q_common/*Sourcedatabase*/.AT.Alternative A
	INNER JOIN #Scale T ON A.ScaleID = T.ID
	WHERE AlternativeID = @ID

	SET @NewID = SCOPE_IDENTITY()
	INSERT #Alternative(oldID, NEWID) VALUES(@ID, @NewID)
		
	INSERT AT.LT_Alternative(LanguageID, AlternativeID, Name, Label,Description )
	SELECT LanguageID, @NewID, Name, Label, Description 
	FROM at5q_common/*Sourcedatabase*/.AT.LT_Alternative
	WHERE AlternativeID = @ID 

	FETCH NEXT FROM curAlt INTO @ID
END
CLOSE curAlt
DEALLOCATE curAlt

-- Section
DECLARE curSection CURSOR LOCAL FAST_FORWARD READ_ONLY FOR
SELECT SectionID
FROM at5q_common/*Sourcedatabase*/.AT.Section
WHERE ActivityID = @ActivityID

OPEN curSection
FETCH NEXT FROM curSection INTO @ID

WHILE (@@FETCH_STATUS = 0)
BEGIN
	INSERT AT.Section(ActivityID, Type, MinOccurance, MaxOccurance, Moveable, CssClass, Created )
	SELECT @NewAID, Type, MinOccurance, MaxOccurance, Moveable, CssClass, getdate()
	FROM at5q_common/*Sourcedatabase*/.AT.Section	
	WHERE SectionID = @ID
	SET @NewID = SCOPE_IDENTITY()

	INSERT #Section(ID, NEWID) VALUES(@ID, @NewID)

	FETCH NEXT FROM curSection INTO @ID
END
CLOSE curSection
DEALLOCATE curSection

INSERT AT.LT_Section(LanguageID, SectionID, Name, Title, Description)
SELECT LanguageID, P.[NEWID], Name, Title, Description
FROM #Section P
INNER JOIN at5q_common/*Sourcedatabase*/.AT.LT_Section S ON P.ID = S.SectionID


-- Questions
DECLARE curQuest CURSOR LOCAL FAST_FORWARD READ_ONLY FOR 
SELECT QuestionID
FROM at5q_common/*Sourcedatabase*/.AT.Question Q
INNER JOIN #Page P ON Q.PageID = P.ID 

OPEN curQuest
FETCH NEXT FROM curQuest INTO @ID

WHILE (@@FETCH_STATUS = 0)
BEGIN
	INSERT AT.Question( PageID, ScaleID, NO, TYPE, Inverted, Mandatory, Status, CssClass, ExtId, Tag, Created, SectionID)
	SELECT  P.NEWID, S.NEWID, NO, TYPE, Inverted, Mandatory, Status, CssClass, q.QuestionID , Tag, created, Se.NewID
	FROM at5q_common/*Sourcedatabase*/.AT.Question Q
	INNER JOIN #Page P ON Q.PageID = P.ID
	LEFT OUTER JOIN #Scale S ON Q.ScaleID = S.ID
	LEFT OUTER JOIN #Section Se ON Q.SectionID = Se.ID
	WHERE QuestionID = @ID

	SET @NewID = SCOPE_IDENTITY()
	INSERT #Question(ID, NEWID) VALUES(@ID, @NewID)
	FETCH NEXT FROM curQuest INTO @ID
END
CLOSE curQuest
DEALLOCATE curQuest

INSERT AT.LT_Question(LanguageID, QuestionID, Name,Title, ReportText,Description,ShortName )
SELECT LanguageID, Q.NEWID, Name, Title, ReportText, Description,ShortName
FROM #Question Q
INNER JOIN at5q_common/*Sourcedatabase*/.AT.LT_Question LTQ ON Q.ID = LTQ.QuestionID




-- Category
DECLARE @ParentID INT
DECLARE @NewParentID INT
DECLARE @Tag NVARCHAR(128)
DECLARE @Type SMALLINT
DECLARE @No INT
DECLARE @Fillcolor VARCHAR(16)
DECLARE @BorderColor VARCHAR(16)
DECLARE @Weight FLOAT

DECLARE curCat CURSOR LOCAL FAST_FORWARD READ_ONLY FOR 
WITH Cat (CategoryID,ParentID,Tag,TYPE,NO,Fillcolor,BorderColor,Weight)
AS
(
  --Anchor members
	  SELECT CategoryID,ParentID,Tag,TYPE,NO,Fillcolor,BorderColor,Weight
	  FROM at5q_common/*Sourcedatabase*/.AT.Category c
	  WHERE c.ActivityID = @ActivityID AND c.Parentid IS NULL
  UNION ALL
  -- Recursive member def
  SELECT c.CategoryID,c.ParentID,c.Tag,c.TYPE,c.NO,c.Fillcolor,c.BorderColor,c.Weight
	  FROM at5q_common/*Sourcedatabase*/.AT.Category c
	  INNER JOIN Cat AS c1 ON  c.Parentid = c1.categoryid
	 )
SELECT CategoryID,ParentID,Tag,TYPE,NO,Fillcolor,BorderColor,Weight FROM Cat


OPEN curCat
FETCH NEXT FROM curCat INTO @ID,@ParentID,@Tag,@Type,@No,@Fillcolor,@BorderColor,@Weight

WHILE (@@FETCH_STATUS = 0)
BEGIN
    SET @NewParentID = @Parentid
    IF NOT @Parentid IS NULL
    BEGIN
       SELECT @NewParentID = [NEWID] FROM #Category WHERE ID = @Parentid
    END
	INSERT AT.Category(ActivityID, ParentID, Tag, TYPE, NO, Fillcolor, BorderColor, Weight, Created) 
	VALUES(@NewAID,@NewParentID,@Tag,@Type,@No,@Fillcolor,@BorderColor,@Weight,GETDATE())	
	SET @NewID = SCOPE_IDENTITY()

	INSERT #Category(ID, NEWID) VALUES(@ID, @NewID)

	FETCH NEXT FROM curCat INTO @ID,@ParentID,@Tag,@Type,@No,@Fillcolor,@BorderColor,@Weight
END
CLOSE curCat
DEALLOCATE curCat

INSERT AT.LT_Category(LanguageID, CategoryID, Name,Description )
SELECT LanguageID, C.NEWID, Name, Description
FROM #Category C
INNER JOIN at5q_common/*Sourcedatabase*/.AT.LT_Category LTC ON C.ID = LTC.CategoryID

-- Q_C

INSERT AT.Q_C(QuestionID, CategoryID, Weight,NO,Visible)
SELECT Q.NEWID, C.NEWID, Weight,NO,Visible
FROM at5q_common/*Sourcedatabase*/.AT.Q_C QC
INNER JOIN #Category C ON QC.CategoryID = C.ID
INNER JOIN #Question Q ON QC.QuestionID = Q.ID

-- LevelGroup
DECLARE curLG CURSOR LOCAL FAST_FORWARD READ_ONLY FOR
SELECT LevelGroupID
FROM  at5q_common/*Sourcedatabase*/.AT.LevelGroup
WHERE ActivityID = @ActivityID

OPEN curLG
FETCH NEXT FROM curLG INTO @ID

WHILE (@@FETCH_STATUS = 0)
BEGIN
	INSERT AT.LevelGroup(ActivityID, Tag,[NO])
	SELECT @NewAID, Tag,[NO]
	FROM at5q_common/*Sourcedatabase*/.AT.LevelGroup
	WHERE  LevelGroupID = @ID
	SET @NewID = SCOPE_IDENTITY()

	INSERT #LevelGroup(ID, NEWID) VALUES(@ID, @NewID)

	FETCH NEXT FROM curLG INTO @ID
END
CLOSE curLG
DEALLOCATE curLG

INSERT AT.LT_LevelGroup(LanguageID, LevelGroupID, Name, Description)
SELECT LanguageID, LG.[NEWID], Name, Description
FROM #LevelGroup LG
INNER JOIN AT.LT_LevelGroup LG2 ON LG.ID = LG2.LevelGroupID


-- LevelLimits
DECLARE curLL CURSOR LOCAL FAST_FORWARD READ_ONLY FOR
SELECT LevelLimitID
FROM at5q_common/*Sourcedatabase*/.AT.LevelLimit
WHERE CategoryID IN (SELECT CategoryID FROM at5q_common/*Sourcedatabase*/.AT.Category WHERE ActivityID = @ActivityID)
OR QuestionID IN (SELECT QuestionID FROM at5q_common/*Sourcedatabase*/.AT.Question Q JOIN at5q_common/*Sourcedatabase*/.AT.Page P ON Q.PageID = P.PageID AND P.ActivityID = @ActivityID)
OR LevelGroupID IN (SELECT LevelGroupID FROM at5q_common/*Sourcedatabase*/.AT.LevelGroup WHERE ActivityID = @ActivityID)


OPEN curLL
FETCH NEXT FROM curLL INTO @ID

WHILE (@@FETCH_STATUS = 0)
BEGIN
	INSERT AT.LevelLimit(LevelGroupID, CategoryID, QuestionID, MinValue, MaxValue, Sigchange, Fillcolor, BorderColor, Created)
	SELECT (SELECT [NEWID] FROM #Levelgroup WHERE ID = LL.LevelGroupID), 
	        (SELECT [NEWID] FROM #Category WHERE ID = LL.CategoryID), 
	        (SELECT [NEWID] FROM #Question WHERE ID = LL.QuestionID) , 
	        MinValue, MaxValue, Sigchange, Fillcolor, BorderColor, GETDATE()
	FROM at5q_common/*Sourcedatabase*/.AT.LevelLimit ll
	WHERE  LevelLimitID = @ID
	SET @NewID = SCOPE_IDENTITY()

	INSERT #LevelLimit(ID, NEWID) VALUES(@ID, @NewID)

	FETCH NEXT FROM curLL INTO @ID
END
CLOSE curLL
DEALLOCATE curLL

INSERT AT.LT_LevelLimit(LanguageID, LevelLimitID, Name, Description)
SELECT LanguageID, LL.[NEWID], Name, Description
FROM #LevelLimit LL
INNER JOIN at5q_common/*Sourcedatabase*/.AT.LT_LevelLimit LL2 ON LL.ID = LL2.LevelLimitID

------------------------------------- Bobbler --------------------------
IF @CopyBubbles = 1
BEGIN
  
  DECLARE curBubble CURSOR LOCAL FAST_FORWARD READ_ONLY FOR
  SELECT BubbleID
  FROM at5q_common/*Sourcedatabase*/.rep.Bubble WHERE ActivityID = @ActivityID

OPEN curBubble
FETCH NEXT FROM curBubble INTO @ID

WHILE (@@FETCH_STATUS = 0)
BEGIN
	INSERT Rep.Bubble(Ownerid,ActivityID, [NO],status)
	SELECT @CurOwnerID,@NewAID, [NO],status
	FROM at5q_common/*Sourcedatabase*/.Rep.Bubble	
	WHERE BubbleID = @ID
	SET @NewID = SCOPE_IDENTITY()

	INSERT #Bubble(ID, NEWID) VALUES(@ID, @NewID)

	FETCH NEXT FROM curBubble INTO @ID
END
CLOSE curBubble
DEALLOCATE curBubble

INSERT rep.LT_Bubble(LanguageID, BubbleID, Name,  Description)
SELECT LanguageID, B.[NEWID], Name,  Description
FROM #Bubble B
INNER JOIN at5q_common/*Sourcedatabase*/.rep.LT_Bubble L ON B.ID = L.BubbleID

INSERT INTO rep.Bubble_Category(BubbleID, CategoryID, AxisNo)
SELECT B.[NEWID],
       (SELECT [NEWID] FROM #Category WHERE ID = BC.CategoryID)
      ,BC.AxisNo
FROM #Bubble B
INNER JOIN at5q_common/*Sourcedatabase*/.rep.Bubble_Category BC ON B.ID = BC.BubbleID
  
Declare @BubbleID int
DECLARE curBubbleText CURSOR LOCAL FAST_FORWARD READ_ONLY FOR
  SELECT BubbleTextID,BubbleID
  FROM at5q_common/*Sourcedatabase*/.rep.BubbleText Where Bubbleid IN (Select ID FROM #Bubble)

OPEN curBubbleText
FETCH NEXT FROM curBubbleText INTO @ID,@BubbleID

WHILE (@@FETCH_STATUS = 0)
BEGIN
	INSERT Rep.BubbleText(BubbleID, No, FillColor)
	SELECT (Select NewID from #Bubble where ID = @BubbleID), [NO],FillColor
	FROM at5q_common/*Sourcedatabase*/.Rep.BubbleText	
	WHERE BubbleTextID = @ID
	SET @NewID = SCOPE_IDENTITY()

	INSERT #BubbleText(ID, NEWID) VALUES(@ID, @NewID)

	FETCH NEXT FROM curBubbleText INTO @ID,@BubbleID
END
CLOSE curBubbleText
DEALLOCATE curBubbleText

INSERT rep.LT_BubbleText(LanguageID, BubbleTextID , Name,  Description)
SELECT LanguageID, BT.[NEWID], Name,  Description
FROM #BubbleText BT
INNER JOIN at5q_common/*Sourcedatabase*/.rep.LT_BubbleText L ON BT.ID = L.BubbleTextID  
  
  
END

IF @CopyPortalParts = 1
BEGIN
    --Portalpage
	DECLARE curPP CURSOR LOCAL FAST_FORWARD READ_ONLY FOR
	SELECT PortalPageID
	FROM at5q_common/*Sourcedatabase*/.app.PortalPage 
	WHERE Ownerid = @OldOwnerID

	OPEN curPP
	FETCH NEXT FROM curPP INTO @ID

	WHILE (@@FETCH_STATUS = 0)
	BEGIN
		INSERT app.PortalPage(OwnerID, [NO])
		SELECT @NewOwnerID, [NO]
		FROM at5q_common/*Sourcedatabase*/.app.PortalPage	
		WHERE PortalPageID = @ID
		SET @NewID = SCOPE_IDENTITY()

		INSERT #PortalPage(ID, NEWID) VALUES(@ID, @NewID)

		FETCH NEXT FROM curPP INTO @ID
	END
	CLOSE curPP
	DEALLOCATE curPP
	INSERT app.LT_PortalPage(LanguageID, PortalPageID, Name, Description)
	SELECT LanguageID, P.[NEWID], Name,  Description
	FROM #PortalPage P
	INNER JOIN at5q_common/*Sourcedatabase*/.app.LT_PortalPage L ON P.ID = L.PortalPageID
  
    --PortalPart
    DECLARE @PortalPageID INT
    DECLARE @CategoryID INT
    DECLARE curPPart CURSOR LOCAL FAST_FORWARD READ_ONLY FOR
	SELECT PortalPartID,PortalPageID,CategoryID
	FROM at5q_common/*Sourcedatabase*/.app.PortalPart 
	WHERE PortalPageID IN (SELECT ID FROM #PortalPage)

	OPEN curPPart
	FETCH NEXT FROM curPPart INTO @ID,@PortalPageID,@CategoryID

	WHILE (@@FETCH_STATUS = 0)
	BEGIN
		INSERT app.PortalPart(PortalPageID, PortalPartTypeID, NO, CategoryID, Settings, BubbleID, ReportPartID,ReportID )
		SELECT (SELECT [NEWID] FROM #PortalPage WHERE ID = @PortalPageID), 
		       PortalPartTypeID, NO, 
		       (SELECT [NEWID] FROM #Category WHERE ID = @CategoryID), 
		       Settings, 
		       NULL, 
		       NULL,
		       NULL
		FROM at5q_common/*Sourcedatabase*/.app.PortalPart	
		WHERE PortalPartID = @ID
		SET @NewID = SCOPE_IDENTITY()

		INSERT #PortalPart(ID, NEWID) VALUES(@ID, @NewID)

		FETCH NEXT FROM curPPart INTO @ID,@PortalPageID,@CategoryID
	END
	CLOSE curPPart
	DEALLOCATE curPPart
	
	INSERT app.LT_PortalPart(LanguageID, PortalPartID, Name, Description,TEXT,TextAbove,TextBelow)
	SELECT LanguageID, P.[NEWID], Name,  Description,TEXT,TextAbove,TextBelow
	FROM #PortalPart P
	INNER JOIN at5q_common/*Sourcedatabase*/.app.LT_PortalPart L ON P.ID = L.PortalPartID
	
END


IF @CopyMenuID > 0
BEGIN
  DECLARE @NewMenuID AS INTEGER
  
  INSERT INTO app.Menu(OwnerID, NO, TYPE, Created)
  SELECT   @NewOwnerID, NO, TYPE, GETDATE()
  FROM at5q_common/*Sourcedatabase*/.app.Menu WHERE MenuID = @CopyMenuID
  
  SET @NewMenuID = SCOPE_IDENTITY()
  INSERT #Menu(ID, NEWID) VALUES(@CopyMenuID, @NewMenuID)
  
    INSERT app.LT_Menu(LanguageID, MenuID, Name, Description,Title)
	SELECT LanguageID, M.[NEWID],  Name, Description,Title
	FROM #Menu M
	INNER JOIN at5q_common/*Sourcedatabase*/.app.LT_Menu L ON M.ID = L.MenuID
  
  --MenuItems
  DECLARE @MenuItemTypeID AS INT
  DECLARE @CssClass AS NVARCHAR(64)
  DECLARE @URL AS NVARCHAR(128)
  DECLARE @Target AS NVARCHAR(64)
  DECLARE @IsDefault AS BIT
  DECLARE @NewPortalPageID AS INT
  
  DECLARE curMI CURSOR LOCAL FAST_FORWARD READ_ONLY FOR 
WITH MenuI (MenuItemID, MenuID, ParentID, MenuItemTypeID, PortalPageID, NO, CssClass, URL, Target, IsDefault)
AS
(
  --Anchor members
	  SELECT MenuItemID, @NewMenuID, ParentID, MenuItemTypeID, PortalPageID, NO, CssClass, URL, Target, IsDefault
	  FROM at5q_common/*Sourcedatabase*/.app.MenuItem MI
	  WHERE MI.MenuID = @CopyMenuID AND MI.Parentid IS NULL
   UNION ALL
  -- Recursive member def
     SELECT MI.MenuItemID, @NewMenuID, MI.ParentID,MI.MenuItemTypeID, MI.PortalPageID, MI.NO, MI.CssClass, MI.URL, MI.Target, MI.IsDefault
	  FROM at5q_common/*Sourcedatabase*/.app.MenuItem MI
	  INNER JOIN MenuI AS M1 ON  MI.Parentid = M1.MenuItemid
	 )
    SELECT MenuItemID,  ParentID, MenuItemTypeID, PortalPageID, NO, CssClass, URL, Target, IsDefault FROM MenuI

   
  OPEN curMI
FETCH NEXT FROM curMI INTO @ID,@ParentID,@MenuItemTypeID, @PortalPageID, @No, @CssClass, @URL, @Target, @IsDefault

WHILE (@@FETCH_STATUS = 0)
BEGIN
    SET @NewParentID = @Parentid
    IF NOT @Parentid IS NULL
    BEGIN
       SELECT @NewParentID = [NEWID] FROM #MenuITem WHERE ID = @Parentid
    END
    
    SELECT @NewPortalPageID = [NEWID] FROM #PortalPage WHERE ID =  @PortalPageID
    
	INSERT app.MenuItem( MenuID, ParentID, MenuItemTypeID, PortalPageID, NO, CssClass, URL, Target, IsDefault, Created) 
	VALUES(@NewMenuID,@NewParentID,@MenuItemTypeID,
	       @NewPortalPageID, 
	       @No, @CssClass, @URL, @Target, @IsDefault,GETDATE())	
	
	SET @NewID = SCOPE_IDENTITY()

	INSERT #MenuITem(ID, NEWID) VALUES(@ID, @NewID)

	FETCH NEXT FROM curMI INTO @ID,@ParentID,@MenuItemTypeID, @PortalPageID, @No, @CssClass, @URL, @Target, @IsDefault
END
CLOSE curMI
DEALLOCATE curMI


INSERT App.LT_MenuItem(LanguageID, MenuItemID, Name,Description, ToolTip)
SELECT LanguageID, MI.NEWID, Name, Description, ToolTip
FROM #MenuItem MI
INNER JOIN at5q_common/*Sourcedatabase*/.app.LT_MenuItem LMI ON MI.ID = LMI.MenuItemID
  
END

-- CalcFields
	DECLARE curCF CURSOR LOCAL FAST_FORWARD READ_ONLY FOR
	SELECT CFID
	FROM at5q_common/*Sourcedatabase*/.at.CalcField 
	WHERE ActivityID = @ActivityID

	OPEN curCF
	FETCH NEXT FROM curCF INTO @ID

	WHILE (@@FETCH_STATUS = 0)
	BEGIN
		INSERT at.CalcField(ActivityID, Name, Formula, Format, Created)
		SELECT @NewAID, name,formula,format,getdate()
		FROM at5q_common/*Sourcedatabase*/.at.CalcField	
		WHERE CFID = @ID
		
		SET @NewID = SCOPE_IDENTITY()

		INSERT #CalcField(ID, NEWID) VALUES(@ID, @NewID)

		FETCH NEXT FROM curCF INTO @ID
	END
	CLOSE curCF
	DEALLOCATE curCF

   --CalcFieldParameter
   INSERT INTO AT.CalcParameter(CFID, NO, Name, QuestionID, AlternativeID, CategoryID, CalcType, Format, Created)
   SELECT ncf.[NEWID],cp.NO,cp.name,
    (SELECT [NEWID] FROM #Question WHERE ID = cp.Questionid), 
    (SELECT [NEWID] FROM #Alternative WHERE oldID = cp.AlternativeID),
    (SELECT [NEWID]FROM #Category WHERE ID = cp.categoryid),
    cp.calctype,
    cp.format,
    GETDATE() 
   FROM at5q_common/*Sourcedatabase*/.AT.CalcParameter cp
   JOIN at5q_common/*Sourcedatabase*/.AT.CalcField cf ON cp.cfid = cf.cfid
   JOIN #CalcField ncf ON ncf.id = cf.cfid
   
    --QA_Calc
    INSERT INTO AT.QA_Calc(QuestionID, AlternativeID, CFID, MinValue, MaxValue, NO, NewValue, UseNewValue, Created, UseCalculatedValue)
    SELECT  
    (SELECT NEWID FROM #Question WHERE id = qac.Questionid), 
    (SELECT NEWID FROM #Alternative WHERE oldid = qac.Alternativeid),
    cf.NEWID,
    qac.Minvalue,
    qac.Maxvalue,
    qac.NO,
    qac.NewValue,
    qac.UseNewValue,
    GETDATE(),
    qac.UseCalculatedValue
    FROM at5q_common/*Sourcedatabase*/.AT.QA_Calc qac
    JOIN #CalcField  cf ON qac.cfid = cf.id
    
    ---  Choice    
    IF @CopyChoice = 1
    BEGIN
		Declare @ChoiceID as int 
		Declare @NewChoiceID as int 
		Declare @ActionID as int
		Declare @NewActionID as int

		declare ChoiceList cursor for
		select ChoiceID from at5q_common/*Sourcedatabase*/.at.Choice where activityid = @ActivityID 
		OPEN ChoiceList
		FETCH NEXT FROM ChoiceList 
		INTO @ChoiceID
			
		WHILE @@FETCH_STATUS = 0
		BEGIN
			Insert into at.choice(ActivityID, Name, No, DepartmentID, DepartmentTypeID, RoleID, Type, Created)
			select @NewAID, Name, No, DepartmentID, DepartmentTypeID, RoleID, Type,getdate() 
			from at5q_common/*Sourcedatabase*/.at.choice where choiceid = @ChoiceID
		
			select @NewChoiceID = SCOPE_IDENTITY()
		    
			insert into at.Combi(ChoiceID, QuestionID, AlternativeID, ValueFormula)
			select @NewChoiceID , q.newid, a.newid, ValueFormula
			from at5q_common/*Sourcedatabase*/.at.Combi c
			Join #Question q on q.id = c.Questionid
			Join #Alternative a on a.oldid = c.Alternativeid
			where choiceid = @ChoiceID
		    
			declare ActionList cursor for
			select ActionID from at5q_common/*Sourcedatabase*/.at.[Action] where ChoiceID = @ChoiceID
			OPEN ActionList
			FETCH NEXT FROM ActionList 
			INTO @ActionID

			WHILE @@FETCH_STATUS = 0
			BEGIN
				insert into at.Action(ChoiceID, No, PageID, QuestionID, AlternativeID, Type)
				select	  @NewChoiceID, No, p.newid, q.newid, a.newid, Type
				from at5q_common/*Sourcedatabase*/.at.Action ac 
				left outer Join #Page p on p.id = ac.pageid
				left outer Join #Question q on q.id = ac.Questionid
				left outer Join #Alternative a on a.oldid = ac.Alternativeid
				where ActionID = @ActionID
		    
				select @NewActionID = SCOPE_IDENTITY()

				insert into at.LT_Action(LanguageID, ActionID, Response)
				select	  LanguageID, @NewActionID, Response
				from at5q_common/*Sourcedatabase*/.at.LT_Action where ActionID = @ActionID

				FETCH NEXT FROM ActionList 
				INTO @ActionID
			END
			CLOSE ActionList
			DEALLOCATE ActionList

			FETCH NEXT FROM ChoiceList 
			INTO @ChoiceID
		   
		END
		CLOSE ChoiceList
		DEALLOCATE ChoiceList
	END

   --


    
  
   
 
IF @CopyScoreTemplate = 1
BEGIN
   -- ScoreTemplate
	DECLARE curST CURSOR LOCAL FAST_FORWARD READ_ONLY FOR
	SELECT ScoreTemplateID
	FROM at5q_common/*Sourcedatabase*/.at.ScoreTemplate
	WHERE ActivityID = @ActivityID

	OPEN curST
	FETCH NEXT FROM curST INTO @ID

	WHILE (@@FETCH_STATUS = 0)
	BEGIN
		INSERT AT.ScoreTemplate(ActivityID,XCID,Type ,[NO],BorderColor,Fillcolor,Created)
		SELECT @NewAID, XCID,Type ,[NO],BorderColor,Fillcolor,getdate()
		FROM at5q_common/*Sourcedatabase*/.AT.ScoreTemplate	
		WHERE ScoreTemplateID = @ID
		SET @NewID = SCOPE_IDENTITY()

		INSERT #ScoreTemplate(ID, NEWID) VALUES(@ID, @NewID)

		FETCH NEXT FROM curST INTO @ID
	END
	CLOSE curST
	DEALLOCATE curST

	INSERT INTO at.LT_ScoreTemplate(ScoreTemplateID, LanguageID, Name, Description)
	SELECT [NewID], LanguageID, Name, Description FROM #ScoreTemplate S
	INNER JOIN at5q_common/*Sourcedatabase*/.at.LT_ScoreTemplate L ON S.ID = L.ScoreTemplateID

	INSERT INTO AT.ST_CQ(ScoreTemplateID, CategoryID, QuestionID, MinValue, MaxValue)
	SELECT st.NEWID, 
	 (SELECT NEWID FROM #Category WHERE id = stcq.CategoryID),
	 (SELECT NEWID FROM #Question WHERE id = stcq.QuestionID),
	  stcq.MinValue,
	  stcq.MaxValue
	FROM at5q_common/*Sourcedatabase*/.AT.ST_CQ stcq
	JOIN #ScoreTemplate st ON st.id = stcq.ScoreTemplateID
END

IF @CopyCVView = 1 
BEGIN
	DECLARE curCVV CURSOR LOCAL FAST_FORWARD READ_ONLY FOR
	SELECT CVVID
	FROM at5q_common/*Sourcedatabase*/.at.CVView
	WHERE ActivityID = @ActivityID

	OPEN curCVV
	FETCH NEXT FROM curCVV INTO @ID

	WHILE (@@FETCH_STATUS = 0)
	BEGIN
		INSERT AT.CVView(DepartmentID, CustomerID, ActivityID, Created)
		SELECT  Departmentid,Customerid, @NewAID, getdate()
		FROM at5q_common/*Sourcedatabase*/.AT.CVView	
		WHERE CVVID = @ID
		SET @NewID = SCOPE_IDENTITY()

		INSERT #CVView(ID, NEWID) VALUES(@ID, @NewID)

		FETCH NEXT FROM curCVV INTO @ID
	END
	CLOSE curCVV
	DEALLOCATE curCVV
	
    INSERT at.LT_CVView(LanguageID, CVVID, Name, Description)
	SELECT LanguageID, P.[NEWID], Name,  Description
	FROM #CVView P
	INNER JOIN at.LT_CVView L ON P.ID = L.CVVID
	
	DECLARE @CVVID as int 
	DECLARE curCVV_Q CURSOR LOCAL FAST_FORWARD READ_ONLY FOR
	SELECT vq.CVVQID, v.CVVID
	FROM at5q_common/*Sourcedatabase*/.at.CVV_Q vq
	JOIN at5q_common/*Sourcedatabase*/.at.CVView  v on v.CVVID = vq.CVVID
	WHERE ActivityID = @ActivityID

	OPEN curCVV_Q
	FETCH NEXT FROM curCVV_Q INTO @ID,@CVVID

	WHILE (@@FETCH_STATUS = 0)
	BEGIN
		INSERT AT.CVV_Q(CVVID, QuestionID, SectionID, No)
		SELECT  C.NewID,Q.NewID,S.NewID, No
		FROM at5q_common/*Sourcedatabase*/.AT.CVV_Q CV
		INNER JOIN #CVView C ON CV.CVVID = C.ID
		LEFT OUTER JOIN #Question Q on CV.QuestionID = Q.ID
		LEFT OUTER JOIN #Section S on CV.SectionID = S.ID
		WHERE CVVQID = @ID
		SET @NewID = SCOPE_IDENTITY()

		INSERT #CVV_Q(ID, NEWID) VALUES(@ID, @NewID)

		FETCH NEXT FROM curCVV_Q INTO @ID,@CVVID
	END
	CLOSE curCVV_Q
	DEALLOCATE curCVV_Q
	
	Insert into at.CVV_QA (CVVQID, AlternativeID, No)
	Select QA2.newid, A.NewID, No
	FROM at5q_common/*Sourcedatabase*/.at.CVV_QA QA
	JOIN #CVV_Q QA2 ON QA.CVVQID = QA2.id
	INNER JOIN #Alternative A ON QA.AlternativeID = A.oldID
	
	
END


COMMIT TRANSACTION


END



	

GO
