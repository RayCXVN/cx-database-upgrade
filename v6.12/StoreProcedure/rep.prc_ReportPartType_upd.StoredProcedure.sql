SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[rep].[prc_ReportPartType_upd]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [rep].[prc_ReportPartType_upd] AS' 
END
GO

ALTER PROCEDURE [rep].[prc_ReportPartType_upd]
(
	@ReportPartTypeID int,
	@No smallint,
	@cUserid int,
	@Log smallint = 1
)
AS
BEGIN
	SET NOCOUNT ON
	DECLARE @Err Int

	UPDATE [rep].[ReportPartType]
	SET
		[No] = @No
	WHERE
		[ReportPartTypeID] = @ReportPartTypeID

	IF @Log = 1 
	BEGIN 
		INSERT INTO [Log].[AuditLog] ( UserId, TableName, Type, Data, Created) 
		SELECT @cUserid,'ReportPartType',1,
		( SELECT * FROM [rep].[ReportPartType] 
			WHERE
			[ReportPartTypeID] = @ReportPartTypeID			 FOR XML AUTO) as data,
			getdate()
	END

	Set @Err = @@Error

	RETURN @Err
END


GO
