SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[at].[prc_XC_P_del]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [at].[prc_XC_P_del] AS' 
END
GO
ALTER PROCEDURE [at].[prc_XC_P_del]
(
	@XCID int,
	@PageID int,
	@cUserid int,
	@Log smallint = 1
)
AS
BEGIN
	SET NOCOUNT ON
	DECLARE @Err Int
	IF @Log = 1 
	BEGIN 
		INSERT INTO [Log].[AuditLog] ( UserId, TableName, Type, Data, Created) 
		SELECT @cUserid,'XC_P',2,
		( SELECT * FROM [at].[XC_P] 
			WHERE
			[XCID] = @XCID AND
			[PageID] = @PageID
			 FOR XML AUTO) as data,
			getdate() 
	END 
	DELETE FROM [at].[XC_P]
	WHERE
		[XCID] = @XCID AND
		[PageID] = @PageID
	Set @Err = @@Error
	RETURN @Err
END

GO
