SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[org].[prc_Hierarchy_clone]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [org].[prc_Hierarchy_clone] AS' 
END
GO



/* ------------------------------------------------------------
   PROCEDURE:    prc_Hierarchy_clone

   Description:  Inserts a record into table 'Hierarchy'

   AUTHOR:       LockwoodTech 07.07.2006 12:52:51
   ------------------------------------------------------------ */

ALTER PROCEDURE [org].[prc_Hierarchy_clone]
(
	@HierarchyID int,
	@HierarchyIDNew int = null output
)
As
BEGIN
	SET NOCOUNT ON

	DECLARE @Err Int

	INSERT
	INTO [Hierarchy]
	(
		[Name],
		[Description],
		[Type],
		[OwnerID]
	)
	SELECT 'Kopi av ' + [Name],[Description], [Type],[OwnerID] FROM [org].[Hierarchy] WHERE [HierarchyID] = @HierarchyID

	Set @Err = @@Error
	set @HierarchyIDNew = scope_identity()

    exec org.prc_Hierarchy_clone_HDs @HierarchyID,null,@HierarchyIDNew,null
	RETURN @Err
End





GO
