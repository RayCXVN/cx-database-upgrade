SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[rep].[prc_DF_O_ins]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [rep].[prc_DF_O_ins] AS' 
END
GO


ALTER PROCEDURE [rep].[prc_DF_O_ins]
(
	@DF_O_ID int = null output,
	@OwnerID int,
	@DocumentFormatID int,
	@cUserid int,
	@Log smallint = 1
)
AS
BEGIN
	SET NOCOUNT ON
	DECLARE @Err Int

	INSERT INTO [rep].[DF_O]
	(
		[OwnerID],
		[DocumentFormatID]
	)
	VALUES
	(
		@OwnerID,
		@DocumentFormatID
	)

	Set @Err = @@Error
	Set @DF_O_ID = scope_identity()

	IF @Log = 1 
	BEGIN 
		INSERT INTO [Log].[AuditLog] ( UserId, TableName, Type, Data, Created) 
		SELECT @cUserid,'DF_O',0,
		( SELECT * FROM [rep].[DF_O] 
			WHERE
			[DF_O_ID] = @DF_O_ID				 FOR XML AUTO) as data,
				getdate() 
	 END

	RETURN @Err
END



GO
