SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[org].[prc_U_D_UT_del]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [org].[prc_U_D_UT_del] AS' 
END
GO


ALTER PROCEDURE [org].[prc_U_D_UT_del]
(
	@U_DID int,
	@UsertypeID int,
	@cUserid int,
	@Log smallint = 1
)
AS
BEGIN
	SET NOCOUNT ON
	DECLARE @Err Int

	IF @Log = 1 
	BEGIN 
		INSERT INTO [Log].[AuditLog] ( UserId, TableName, Type, Data, Created) 
		SELECT @cUserid,'U_D_UT',2,
		( SELECT * FROM [org].[U_D_UT] 
			WHERE
			[U_DID] = @U_DID AND
			[UsertypeID] = @UsertypeID
			 FOR XML AUTO) as data,
			getdate() 
	END 


	DELETE FROM [org].[U_D_UT]
	WHERE
		[U_DID] = @U_DID AND
		[UsertypeID] = @UsertypeID

	Set @Err = @@Error

	RETURN @Err
END


GO
