SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[mea].[prc_LT_MeasureTemplate_get]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [mea].[prc_LT_MeasureTemplate_get] AS' 
END
GO
ALTER PROCEDURE [mea].[prc_LT_MeasureTemplate_get]  
(  
 @MeasureTemplateID int  
)  
AS  
BEGIN  
 SET NOCOUNT ON  
 DECLARE @Err Int  
  
 SELECT  
 [LanguageID]  
      ,[MeasureTemplateID]  
      ,[Name]  
      ,[Description]  
      ,[Subject]  
 FROM [mea].[LT_MeasureTemplate]  
 WHERE  
 [MeasureTemplateID] = @MeasureTemplateID  
  
 Set @Err = @@Error  
  
 RETURN @Err  
END  

GO
