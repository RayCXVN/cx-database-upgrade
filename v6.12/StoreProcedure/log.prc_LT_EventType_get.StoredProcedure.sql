SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[log].[prc_LT_EventType_get]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [log].[prc_LT_EventType_get] AS' 
END
GO
ALTER PROCEDURE [log].[prc_LT_EventType_get]
	@EventTypeID	int,
	@LanguageID	int
AS
BEGIN
	SET NOCOUNT ON;
	DECLARE @Err Int
	
     SELECT [EventTypeID],
		  [LanguageID],
		  [Name],
		  [Description]
	FROM  [log].[LT_EventType]
	WHERE [EventTypeID] = @EventTypeID
	  AND [LanguageID]  = @LanguageID
	
	Set @Err = @@Error

	RETURN @Err
  
END

GO
