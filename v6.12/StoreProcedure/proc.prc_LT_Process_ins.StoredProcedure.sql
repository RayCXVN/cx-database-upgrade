SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[proc].[prc_LT_Process_ins]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [proc].[prc_LT_Process_ins] AS' 
END
GO

ALTER PROCEDURE [proc].[prc_LT_Process_ins]
(
	@ProcessID int,
	@LanguageID int,
	@Name nvarchar(256),
	@Description nvarchar(max),
	@cUserid int,
	@Log smallint = 1
)
AS
BEGIN
	SET NOCOUNT ON
	DECLARE @Err Int

	INSERT INTO [proc].[LT_Process]
	(
		[ProcessID],
		[LanguageID],
		[Name],
		[Description],
		[Created]
	)
	VALUES
	(
		@ProcessID,
		@LanguageID,
		@Name,
		@Description,
		getdate()
	)

	Set @Err = @@Error

	IF @Log = 1 
	BEGIN 
		INSERT INTO [Log].[AuditLog] ( UserId, TableName, Type, Data, Created) 
		SELECT @cUserid,'LT_Process',0,
		( SELECT * FROM [proc].[LT_Process] 
			WHERE
			[ProcessID] = @ProcessID AND
			[LanguageID] = @LanguageID				 FOR XML AUTO) as data,
				getdate() 
	 END

	RETURN @Err
END


GO
