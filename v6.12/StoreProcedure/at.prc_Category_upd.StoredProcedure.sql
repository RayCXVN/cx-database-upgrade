SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[at].[prc_Category_upd]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [at].[prc_Category_upd] AS' 
END
GO
/*  
	2017-06-13 Steve: Change the length of column ExtID in all related tables from xAPI format
*/
ALTER PROCEDURE [at].[prc_Category_upd]      
(      
 @CategoryID int,      
 @ActivityID int,      
 @ParentID INT=NULL,      
 @Tag nvarchar(128),      
 @Type smallint,      
 @No smallint,      
 @Fillcolor varchar(16),      
 @BorderColor varchar(16),      
 @Weight float,      
 @NType smallint,      
 @ProcessCategory bit,      
 @cUserid int,      
 @Log smallint = 1,  
 @ExtID nvarchar(256),
 @AnswerItemID int = null,
 @RenderAsDefault bit =1,
 @AvgType smallint = 0
)      
AS      
BEGIN      
 SET NOCOUNT ON      
 DECLARE @Err Int      
      
 UPDATE [at].[Category]      
 SET      
  [ActivityID] = @ActivityID,      
  [ParentID] = @ParentID,      
  [Tag] = @Tag,      
  [Type] = @Type,      
  [No] = @No,      
  [Fillcolor] = @Fillcolor,      
  [BorderColor] = @BorderColor,      
  [Weight] = @Weight,      
  [NType] = @NType,      
  [ProcessCategory]=@ProcessCategory,  
  [ExtID]  = @ExtID,
  [AnswerItemID]  = @AnswerItemID,
  [RenderAsDefault] = @RenderAsDefault,
  [AvgType] = @AvgType
 WHERE      
  [CategoryID] = @CategoryID      
      
 IF @Log = 1       
 BEGIN       
  INSERT INTO [Log].[AuditLog] ( UserId, TableName, Type, Data, Created)       
  SELECT @cUserid,'Category',1,      
  ( SELECT * FROM [at].[Category]       
   WHERE      
   [CategoryID] = @CategoryID    FOR XML AUTO) as data,      
   getdate()      
 END      
      
 Set @Err = @@Error      
      
 RETURN @Err      
END 

GO
