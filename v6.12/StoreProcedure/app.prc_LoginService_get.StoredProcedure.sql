SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[app].[prc_LoginService_get]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [app].[prc_LoginService_get] AS' 
END
GO
/*
	2018-10-31	Sarah	CXPLAT-848
*/
ALTER PROCEDURE [app].[prc_LoginService_get]  
(  
 @SiteID int  
)  
AS  
BEGIN  
 SET NOCOUNT ON  
 DECLARE @Err Int  
  
 SELECT  
      [LoginServiceID]  
      ,[SiteID]  
      ,[LoginServiceType]  
      ,[IconUrl]  
      ,[Authority] 
      ,[MetadataAddress]  
      ,[RedirectUri]  
      ,[SecondaryClaimType]
      ,[ClientId]
      ,[ClientSecret]
      ,[Scope]
      ,[ResponseType]
      ,[PrimaryClaimType]  
      ,[PostLogoutUri]
      ,[Disabled]
      ,[CreatedDate]
      ,[MasterID]
 FROM [app].[LoginService]  
 WHERE  
 [SiteID] = @SiteID  
  
 Set @Err = @@Error  
  
 RETURN @Err  
END

GO
