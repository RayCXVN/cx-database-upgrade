SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[at].[prc_Activity_ins]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [at].[prc_Activity_ins] AS' 
END
GO
/*  
	2017-06-13 Steve: Change the length of column ExtID in all related tables from xAPI format
*/
ALTER PROCEDURE [at].[prc_Activity_ins]
(
	@ActivityID int = null output,
	@LanguageID int,
	@OwnerID int,
	@StyleSheet nvarchar(128),
	@Type smallint,
	@No smallint,
	@TooltipType smallint,
	@Listview smallint,
	@Descview smallint,
	@Chartview smallint,
	@OLAPServer nvarchar(64),
	@OLAPDB nvarchar(64),
	@SelectionHeaderType smallint = 0,
	@ExtID nvarchar(256)='',
	@cUserid int,
	@Log smallint = 1,
	@UseOLAP bit = 1,
	@ArchetypeID int = NULL
)
AS
BEGIN
	SET NOCOUNT ON
	DECLARE @Err Int

	INSERT INTO [at].[Activity]
	(
		[LanguageID],
		[OwnerID],
		[StyleSheet],
		[Type],
		[No],
		[TooltipType],
		[Listview],
		[Descview],
		[Chartview],
		[OLAPServer],
		[OLAPDB],
		[SelectionHeaderType],
		[ExtID],
		[UseOLAP],
		[ArchetypeID]
	)
	VALUES
	(
		@LanguageID,
		@OwnerID,
		@StyleSheet,
		@Type,
		@No,
		@TooltipType,
		@Listview,
		@Descview,
		@Chartview,
		@OLAPServer,
		@OLAPDB,
		@SelectionHeaderType,
		@ExtID,
		@UseOLAP,
		@ArchetypeID
	)

	Set @Err = @@Error
	Set @ActivityID = scope_identity()

	IF @Log = 1 
	BEGIN 
		INSERT INTO [Log].[AuditLog] ( UserId, TableName, Type, Data, Created) 
		SELECT @cUserid,'Activity',0,
		( SELECT * FROM [at].[Activity] 
			WHERE
			[ActivityID] = @ActivityID				 FOR XML AUTO) as data,
				getdate() 
	 END

	RETURN @Err
END

GO
