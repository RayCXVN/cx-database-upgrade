SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[at].[prc_Alternative_ins]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [at].[prc_Alternative_ins] AS' 
END
GO
ALTER PROCEDURE [at].[prc_Alternative_ins]
(
	@AlternativeID int = null output,
	@ScaleID int,
	@AGID INT=NULL,
	@Type smallint,
	@No smallint,
	@Value float,
	@InvertedValue float,
	@Calc bit,
	@SC bit,
	@MinValue float,
	@MaxValue float,
	@Format varchar(64),
	@Size smallint,
	@CssClass varchar(64),
	@DefaultValue nvarchar(128),
	@Tag nvarchar(128),
	@ExtID nvarchar(256),
	@Width nvarchar(32),
	@OwnerColorID int = null,
	@DefaultCalcType smallint = -1,
    @ParentID INT = NULL,
    @UseEncryption BIT = 0,
	@cUserid int,
	@Log smallint = 1
    
)
AS
BEGIN
	SET NOCOUNT ON
	DECLARE @Err Int

	INSERT INTO [at].[Alternative]
	(
		[ScaleID],
		[AGID],
		[Type],
		[No],
		[Value],
		[InvertedValue],
		[Calc],
		[SC],
		[MinValue],
		[MaxValue],
		[Format],
		[Size],
		[CssClass],
		[DefaultValue],
		[Tag],
		[ExtID],
		[Width],
		[OwnerColorID],
		[DefaultCalcType],
        [ParentID],
        [UseEncryption]
	)
	VALUES
	(
		@ScaleID,
		@AGID,
		@Type,
		@No,
		@Value,
		@InvertedValue,
		@Calc,
		@SC,
		@MinValue,
		@MaxValue,
		@Format,
		@Size,
		@CssClass,
		@DefaultValue,
		@Tag,
		@ExtID,
		@Width,
		@OwnerColorid,
		@DefaultCalcType,
        @ParentID,
        @UseEncryption
	)

	Set @Err = @@Error
	Set @AlternativeID = scope_identity()

	IF @Log = 1 
	BEGIN 
		INSERT INTO [Log].[AuditLog] ( UserId, TableName, Type, Data, Created) 
		SELECT @cUserid,'Alternative',0,
		( SELECT * FROM [at].[Alternative] 
			WHERE
			[AlternativeID] = @AlternativeID				 FOR XML AUTO) as data,
				getdate() 
	 END

	RETURN @Err
END


GO
