SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[rep].[prc_SelectionGroup_get]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [rep].[prc_SelectionGroup_get] AS' 
END
GO

ALTER PROCEDURE [rep].[prc_SelectionGroup_get]
(
	@UserID int
)
AS
BEGIN
	SET NOCOUNT ON
	DECLARE @Err Int

	SELECT
	[SelectionGroupID],
	[Name],
	[UserID],
	[Created]
	FROM [rep].[SelectionGroup]
	WHERE
	[UserID] = @UserID

	Set @Err = @@Error

	RETURN @Err
END


GO
