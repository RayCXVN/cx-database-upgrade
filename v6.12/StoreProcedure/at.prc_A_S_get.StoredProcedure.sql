SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[at].[prc_A_S_get]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [at].[prc_A_S_get] AS' 
END
GO

ALTER PROCEDURE [at].[prc_A_S_get] 
(
	@ActivityID INT
)
AS
BEGIN
	SET NOCOUNT ON

	DECLARE @Err INT

	SELECT [ASID],
		[ActivityID],
		[StatusTypeID],
		[No]
	FROM [at].[A_S]
	WHERE [ActivityID] = @ActivityID
	ORDER BY [No]

	SET @Err = @@Error

	RETURN @Err
END


GO
