SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[rep].[prc_DocumentFormat_upd]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [rep].[prc_DocumentFormat_upd] AS' 
END
GO


ALTER PROCEDURE [rep].[prc_DocumentFormat_upd]
(
	@DocumentFormatID int,
	@Name nvarchar(256),
	@cUserid int,
	@Log smallint = 1
)
AS
BEGIN
	SET NOCOUNT ON
	DECLARE @Err Int

	UPDATE [rep].[DocumentFormat]
	SET
		[Name] = @Name
	WHERE
		[DocumentFormatID] = @DocumentFormatID

	IF @Log = 1 
	BEGIN 
		INSERT INTO [Log].[AuditLog] ( UserId, TableName, Type, Data, Created) 
		SELECT @cUserid,'DocumentFormat',1,
		( SELECT * FROM [rep].[DocumentFormat] 
			WHERE
			[DocumentFormatID] = @DocumentFormatID			 FOR XML AUTO) as data,
			getdate()
	END

	Set @Err = @@Error

	RETURN @Err
END



GO
