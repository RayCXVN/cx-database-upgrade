SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[mea].[prc_Measure_Category_GetByMeasureIds]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [mea].[prc_Measure_Category_GetByMeasureIds] AS' 
END
GO
 ALTER PROCEDURE [mea].[prc_Measure_Category_GetByMeasureIds]
(
 @MeasureIds nvarchar(max)
)
AS
BEGIN
 SELECT mc.CategoryId,lc.LanguageID, mc.MeasureId , lc.Name, lc.Description
 FROM mea.Measure_Category mc INNER JOIN mea.LT_Category lc ON lc.CategoryId = mc.CategoryId
 WHERE mc.MeasureId IN (SELECT value FROM dbo.funcListToTableInt(@MeasureIds,',')) 
END

GO
