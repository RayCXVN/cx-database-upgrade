SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[at].[prc_LT_Section_upd]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [at].[prc_LT_Section_upd] AS' 
END
GO

ALTER PROCEDURE [at].[prc_LT_Section_upd]
(
	@LanguageID int,
	@SectionID int,
	@Name nvarchar(256),
	@Title nvarchar(256),
	@Description nvarchar(max),
	@cUserid int,
	@Log smallint = 1
)
AS
BEGIN
	SET NOCOUNT ON
	DECLARE @Err Int

	UPDATE [at].[LT_Section]
	SET
		[LanguageID] = @LanguageID,
		[SectionID] = @SectionID,
		[Name] = @Name,
		[Title] = @Title,
		[Description] = @Description
	WHERE
		[LanguageID] = @LanguageID AND
		[SectionID] = @SectionID

	IF @Log = 1 
	BEGIN 
		INSERT INTO [Log].[AuditLog] ( UserId, TableName, Type, Data, Created) 
		SELECT @cUserid,'LT_Section',1,
		( SELECT * FROM [at].[LT_Section] 
			WHERE
			[LanguageID] = @LanguageID AND
			[SectionID] = @SectionID			 FOR XML AUTO) as data,
			getdate()
	END

	Set @Err = @@Error

	RETURN @Err
END


GO
