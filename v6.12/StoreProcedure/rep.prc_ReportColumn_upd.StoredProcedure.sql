SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[rep].[prc_ReportColumn_upd]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [rep].[prc_ReportColumn_upd] AS' 
END
GO
ALTER PROCEDURE [rep].[prc_ReportColumn_upd]  
(  
 @ReportColumnID int,  
 @ReportRowID int,  
 @No smallint,  
 @ReportColumnTypeID int,  
 @Formula varchar(1024),  
 @Width float,  
 @Format varchar(16),  
 @URL nvarchar(256),  
 @IsNegative smallint = 0,  
 @FormulaText nvarchar(32) = '',  
 @UseLevelLimit bit = 0,  
 @UseLevelLimitText bit = 0,  
 @OwnerColorID int = null,  
 @QuestionID int = null,  
 @AlternativeID int = null,  
 @CellAlign smallint = -1,  
 @cUserid int,  
 @Log smallint = 1,
 @ItemID int = NULL
)  
AS  
BEGIN  
 SET NOCOUNT ON  
 DECLARE @Err Int  
  
 UPDATE [rep].[ReportColumn]  
 SET  
  [ReportRowID] = @ReportRowID,  
  [No] = @No,  
  [ReportColumnTypeID] = @ReportColumnTypeID,  
  [Formula] = @Formula,  
  [Width] = @Width,  
  [Format] = @Format,  
  [URL] = @URL,  
  [IsNegative] = @IsNegative,  
  [FormulaText] = @FormulaText,  
  [UseLevellimit] = @UseLevelLimit,  
  [UseLevellimitText] = @UseLevelLimitText,  
  [OwnerColorID] = @OwnerColorID,  
  [QuestionID] = @QuestionID,  
  [AlternativeID] = @AlternativeID,  
  [CellAlign] = @CellAlign,
  [ItemID] = @ItemID
 WHERE  
  [ReportColumnID] = @ReportColumnID  
  
 IF @Log = 1   
 BEGIN   
  INSERT INTO [Log].[AuditLog] ( UserId, TableName, Type, Data, Created)   
  SELECT @cUserid,'ReportColumn',1,  
  ( SELECT * FROM [rep].[ReportColumn]   
   WHERE  
   [ReportColumnID] = @ReportColumnID    FOR XML AUTO) as data,  
   getdate()  
 END  
  
 Set @Err = @@Error  
  
 RETURN @Err  
END  

GO
