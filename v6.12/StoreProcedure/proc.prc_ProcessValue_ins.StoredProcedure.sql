SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[proc].[prc_ProcessValue_ins]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [proc].[prc_ProcessValue_ins] AS' 
END
GO
ALTER PROCEDURE [proc].[prc_ProcessValue_ins]
(
	@ProcessValueID int = null output,
	@No smallint,
	@Value smallint,
	@BackGroundColor nvarchar(16),
	@OwnerID int,
	@ProcessTypeID int = NULL,
	@URL nvarchar(256) = '',
	@ReadOnly bit = 0,
	@cUserid int,
	@Log smallint = 1
)
AS
BEGIN
	SET NOCOUNT ON
	DECLARE @Err Int

	INSERT INTO [proc].[ProcessValue]
	(
		[No],
		[Value],
		[BackGroundColor],
		[OwnerID],
		[ProcessTypeID],
		[URL],
		[ReadOnly] 
	)
	VALUES
	(
		@No,
		@Value,
		@BackGroundColor,
		@OwnerID,
		@ProcessTypeID,
		@URL,
		@ReadOnly 
	)

	Set @Err = @@Error
	Set @ProcessValueID = scope_identity()

	IF @Log = 1 
	BEGIN 
		INSERT INTO [Log].[AuditLog] ( UserId, TableName, Type, Data, Created) 
		SELECT @cUserid,'ProcessValue',0,
		( SELECT * FROM [proc].[ProcessValue] 
			WHERE
			[ProcessValueID] = @ProcessValueID				 FOR XML AUTO) as data,
				getdate() 
	 END

	RETURN @Err
END

GO
