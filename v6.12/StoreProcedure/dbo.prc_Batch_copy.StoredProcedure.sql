SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[prc_Batch_copy]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [dbo].[prc_Batch_copy] AS' 
END
GO
ALTER PROC [dbo].[prc_Batch_copy] 
(
		@OldSurveyID int,
		@NewSurveyID int
)
AS
BEGIN
INSERT at.Batch(SurveyID, UserID, DepartmentID, Name, No, StartDate, EndDate, Status, MailStatus)
SELECT @NewSurveyID, B.UserID, B.DepartmentID, B.Name, B.No, S.StartDate, S.EndDate, B.Status, MailStatus
FROM at.Batch B 
INNER JOIN at.Survey S on S.SurveyID = @NewSurveyID
WHERE B.SurveyID = @OldSurveyID
AND Not B.DepartmentID IN (SELECT DepartmentID FROM at.Batch WHERE SurveyID = @NewSurveyID)
end


GO
