SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[prc_AccessGroup_getByCustomerID]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [dbo].[prc_AccessGroup_getByCustomerID] AS' 
END
GO
-- exec [dbo].[prc_AccessGroup_getByCustomerID]  15, 29 
-- exec [dbo].[prc_AccessGroup_getByCustomerID]  15, 22, 6, null, null
-- exec [dbo].[prc_AccessGroup_getByCustomerID]  9, 107824, 23824, 0, 61
ALTER PROCEDURE [dbo].[prc_AccessGroup_getByCustomerID]
(
@OwnerId INT,
@CustomerID INT = NULL
)
AS
BEGIN
    SET NOCOUNT ON

    DECLARE  @NestedLevel int = 0
    DECLARE @CheckedAGMember TABLE (AccessGroupMemberID int)
    CREATE TABLE #AccessGroupByUser (AccessGroupID int, OwnerID int, [No] smallint, Created smalldatetime)
    CREATE TABLE #RecursiveAGMember (AccessGroupMemberID int, AccessGroupID int, UserID int, UserTypeID int, DepartmentID int, HDID int, UserGroupID int, RoleID int, DepartmentTypeID int, DepartmentGroupID int, CustomerID int, ExtGroupID int)

    -- Get all leaf level Access group members
    INSERT INTO #AccessGroupByUser
    SELECT AG.AccessGroupID, AG.OwnerID, AG.[No], AG.Created
    FROM	 dbo.AccessGroup AG
    JOIN   dbo.AccessGroupMember AGM ON AG.AccessGroupID = AGM.AccessGroupID AND AG.OwnerId = @OwnerID
    WHERE
    AGM.CustomerID = @CustomerID  AND
    AGM.ExtGroupID IS NULL

    INSERT INTO #RecursiveAGMember (AccessGroupMemberID, AccessGroupID, UserID, UserTypeID, DepartmentID, HDID, UserGroupID, RoleID, DepartmentTypeID, DepartmentGroupID, CustomerID, ExtGroupID)
    SELECT  AccessGroupMemberID, AccessGroupID, UserID, UserTypeID, DepartmentID, HDID, UserGroupID, RoleID, DepartmentTypeID, DepartmentGroupID, CustomerID, ExtGroupID
    FROM	  dbo.AccessGroupMember AGM
    WHERE	  AGM.ExtGroupID > 0

    -- Get all nested level Access group members
    WHILE EXISTS (SELECT 1 FROM #RecursiveAGMember AGM WHERE EXISTS (SELECT 1 FROM #AccessGroupByUser AG1 WHERE AG1.AccessGroupID = AGM.ExtGroupID)
										   AND NOT EXISTS (SELECT 1 FROM #AccessGroupByUser AG2 WHERE AG2.AccessGroupID = AGM.AccessGroupID)
			  ) AND @NestedLevel < 32
    BEGIN
	   SET @NestedLevel += 1

	   INSERT INTO @CheckedAGMember(AccessGroupMemberID)
	   SELECT AccessGroupMemberID FROM #RecursiveAGMember AGM
	   WHERE  EXISTS  (SELECT 1 FROM #AccessGroupByUser AG1 WHERE AG1.AccessGroupID = AGM.ExtGroupID)
	   AND NOT EXISTS (SELECT 1 FROM #AccessGroupByUser AG2 WHERE AG2.AccessGroupID = AGM.AccessGroupID)

	   INSERT INTO #AccessGroupByUser
	   SELECT AG.AccessGroupID, AG.OwnerID, AG.[No], AG.Created
	   FROM   dbo.AccessGroup AG
	   JOIN   #RecursiveAGMember AGM ON AG.AccessGroupID = AGM.AccessGroupID AND AG.OwnerId = @OwnerID
	   WHERE

	   ( AGM.CustomerID = @CustomerID OR AGM.CustomerID IS NULL) AND
	   EXISTS		   (SELECT 1 FROM #AccessGroupByUser AG1 WHERE AG1.AccessGroupID = AGM.ExtGroupID)
	   AND NOT EXISTS (SELECT 1 FROM #AccessGroupByUser AG2 WHERE AG2.AccessGroupID = AGM.AccessGroupID)

	   DELETE AGM FROM #RecursiveAGMember AGM
	   WHERE AccessGroupMemberID IN (SELECT AccessGroupMemberID FROM @CheckedAGMember)

	   DELETE FROM @CheckedAGMember
    END

    --PRINT 'Nested level: ' + convert(nvarchar(5),@NestedLevel)
    SELECT DISTINCT * FROM #AccessGroupByUser
    DROP TABLE #AccessGroupByUser
    DROP TABLE #RecursiveAGMember
END

GO
