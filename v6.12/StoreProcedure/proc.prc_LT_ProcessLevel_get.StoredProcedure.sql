SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[proc].[prc_LT_ProcessLevel_get]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [proc].[prc_LT_ProcessLevel_get] AS' 
END
GO

ALTER PROCEDURE [proc].[prc_LT_ProcessLevel_get]
(
	@ProcessLevelID int
)
AS
BEGIN
	SET NOCOUNT ON
	DECLARE @Err Int

	SELECT
	[ProcessLevelID],
	[LanguageID],
	[Name],
	[Description],
	[Created]
	FROM [proc].[LT_ProcessLevel]
	WHERE
	[ProcessLevelID] = @ProcessLevelID

	Set @Err = @@Error

	RETURN @Err
END


GO
