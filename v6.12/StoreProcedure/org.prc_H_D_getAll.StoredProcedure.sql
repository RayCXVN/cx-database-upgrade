SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[org].[prc_H_D_getAll]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [org].[prc_H_D_getAll] AS' 
END
GO
/*  2016-10-10 Ray: Remove column Status from org.User, org.Department, RDB.dbo.Result
	2017-02-15 Steve: Add Conditions hd.Deleted = 0 AND d.Deleted IS NULL
*/
ALTER PROCEDURE [org].[prc_H_D_getAll]
(
	@ParentID		int = NULL,
	@HierarchyID	int = NULL,
	@DepartmentID int = NULL
)
AS
BEGIN
	SET NOCOUNT ON
	DECLARE @Err Int
	
	IF NOT @ParentID IS NULL
		Select
		hd.[HDID],
		hd.[HierarchyID],
		hd.[DepartmentID],
		ISNULL([ParentID], 0) ParentID,
		hd.[Path],
		hd.[PathName],
		hd.[Created],
		hd.[Deleted],
        d.[EntityStatusID],
        d.[Deleted] AS [DepartmentDeleted]
		FROM org.[H_D] hd
		join org.department d  on d.departmentid = hd.departmentid AND hd.Deleted = 0 AND d.Deleted IS NULL
		WHERE hd.[ParentID] = @ParentID
		--order by d.Name asc
	ELSE
		Select
		hd.[HDID],
		hd.[HierarchyID],
		hd.[DepartmentID],
		ISNULL([ParentID], 0) ParentID,
		hd.[Path],
		hd.[PathName],
		hd.[Created],
		hd.[Deleted],
		d.[EntityStatusID],
        d.[Deleted] AS [DepartmentDeleted]
		FROM org.[H_D] hd
		join org.department d  on d.departmentid = hd.departmentid AND hd.Deleted = 0 AND d.Deleted IS NULL
		WHERE hd.[HierarchyID] = @HierarchyID
		--order by d.Name asc
	Set @Err = @@Error

	RETURN @Err
END


GO
