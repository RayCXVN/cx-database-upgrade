SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[at].[prc_Access_AddByBulks]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [at].[prc_Access_AddByBulks] AS' 
END
GO
ALTER PROCEDURE [at].[prc_Access_AddByBulks]
(   @BatchID        int,
    @BulkIDList     varchar(max)
) AS
BEGIN
    SET NOCOUNT ON
    DECLARE @SurveyID int, @DepartmentID int
    SELECT @SurveyID = [SurveyID], @DepartmentID = [DepartmentID] FROM [at].[Batch] WHERE [BatchID] = @BatchID

    DECLARE @BulkIDTable TABLE (BulkID int)
    INSERT INTO @BulkIDTable (BulkID) SELECT Value FROM dbo.funcListToTableInt(@BulkIDList, ',')

    INSERT INTO [at].[Access] ([SurveyID], [BatchID], [RoleID], [PageID], [Type], [Mandatory], [Created])
    SELECT @SurveyID, @BatchID, br.[RoleID], bp.[PageID], 0, 0, GETDATE()
    FROM [at].[B_P] bp LEFT JOIN [at].[B_R] br ON bp.[BulkID] = br.[BulkID]
    WHERE bp.[BulkID] IN (SELECT BulkID FROM @BulkIDTable)

    INSERT INTO [at].[Access] ([SurveyID], [BatchID], [RoleID], [QuestionID], [Type], [Mandatory], [Created])
    SELECT @SurveyID, @BatchID, br.[RoleID], bq.[QuestionID], 0, 0, GETDATE()
    FROM [at].[B_Q] bq LEFT JOIN [at].[B_R] br ON bq.[BulkID] = br.[BulkID]
    WHERE bq.[BulkID] IN (SELECT BulkID FROM @BulkIDTable)
END

GO
