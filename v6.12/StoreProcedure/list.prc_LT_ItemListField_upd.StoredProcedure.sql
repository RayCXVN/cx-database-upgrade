SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[list].[prc_LT_ItemListField_upd]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [list].[prc_LT_ItemListField_upd] AS' 
END
GO
ALTER PROCEDURE [list].[prc_LT_ItemListField_upd]
	@LanguageID int,
	@ItemListFieldID int,
	@Name nvarchar(256),
	@Description nvarchar(max),
	@HeaderText nvarchar(max),
	@HeaderGroupText nvarchar(256),
	@HeaderTooltipText nvarchar(512),
	@ItemTooltipText nvarchar(512),
	@ItemText nvarchar(max),
	@ExpandingItemText nvarchar(max),
    @DefaultValues nvarchar(128),
    @AvailableValues nvarchar(128),
    @cUserid int,
    @Log smallint = 1
AS
BEGIN
	SET NOCOUNT ON;
	DECLARE @Err Int
	
    UPDATE [list].[LT_ItemListField]
    SET 
		[Name] = @Name,
        [Description] = @Description,
        [HeaderText] = @HeaderText,
        [HeaderGroupText] = @HeaderGroupText,
        [HeaderTooltipText] = @HeaderTooltipText,
        [ItemTooltipText] = @ItemTooltipText,
        [ItemText] = @ItemText,
        [ExpandingItemText] = @ExpandingItemText,
        [DefaultValues] = @DefaultValues,
        [AvailableValues] = @AvailableValues
     WHERE 
		[ItemListFieldID] = @ItemListFieldID AND
		[LanguageID] = @LanguageID
		
	IF @Log = 1 
	BEGIN 
		INSERT INTO [Log].[AuditLog] ( UserId, TableName, Type, Data, Created) 
		SELECT @cUserid,'LT_ItemListField',1,
		( SELECT * FROM [list].[LT_ItemListField] 
			WHERE
				[ItemListFieldID] = @ItemListFieldID AND
				[LanguageID] = @LanguageID			 FOR XML AUTO) as data,
			getdate()
	END

	Set @Err = @@Error

	RETURN @Err	
END

GO
