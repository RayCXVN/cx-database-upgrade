SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[at].[prc_Access_upd]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [at].[prc_Access_upd] AS' 
END
GO
ALTER PROCEDURE [at].[prc_Access_upd](
    @AccessID         int,
    @SurveyID         int,
    @BatchID          int      = NULL,
    @RoleID           int      = NULL,
    @DepartmentID     int      = NULL,
    @DepartmentTypeID int      = NULL,
    @HDID             int      = NULL,
    @CustomerID       int      = NULL,
    @PageID           int      = NULL,
    @QuestionID       int      = NULL,
    @Type             smallint,
    @Mandatory        bit,
    @cUserid          int,
    @Log              smallint = 1,
    @ArchetypeID      int      = NULL)
AS
BEGIN
    SET NOCOUNT ON;
    DECLARE @Err int;

    UPDATE [at].[Access] SET [SurveyID] = @SurveyID, [BatchID] = @BatchID, [RoleID] = @RoleID, [DepartmentID] = @DepartmentID,
                             [DepartmentTypeID] = @DepartmentTypeID, [HDID] = @HDID, [CustomerID] = @CustomerID, [PageID] = @PageID,
                             [QuestionID] = @QuestionID, [Type] = @Type, [Mandatory] = @Mandatory, [ArchetypeID] = @ArchetypeID
    WHERE [AccessID] = @AccessID;

    IF @Log = 1
    BEGIN
        INSERT INTO [Log].[AuditLog] ([UserId], [TableName], [Type], [Data], [Created])
        SELECT @cUserid, 'Access', 1, (SELECT * FROM [at].[Access] WHERE [AccessID] = @AccessID FOR XML AUTO) AS [data], GETDATE();
    END;

    SET @Err = @@Error;

    RETURN @Err;
END;
GO