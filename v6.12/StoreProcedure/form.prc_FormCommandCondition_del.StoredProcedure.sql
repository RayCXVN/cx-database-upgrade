SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[form].[prc_FormCommandCondition_del]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [form].[prc_FormCommandCondition_del] AS' 
END
GO

ALTER PROCEDURE [form].[prc_FormCommandCondition_del] @FormCommandConditionID INT
	,@cUserid INT
	,@Log SMALLINT = 1
AS
BEGIN
	SET NOCOUNT ON;

	DECLARE @Err INT

	IF @Log = 1
	BEGIN
		INSERT INTO [Log].[AuditLog] (
			UserId
			,TableName
			,Type
			,Data
			,Created
			)
		SELECT @cUserid
			,'FormCommandCondition'
			,2
			,(
				SELECT *
				FROM [form].[FormCommandCondition]
				WHERE [FormCommandConditionID] = @FormCommandConditionID
				FOR XML AUTO
				) AS data
			,getdate()
	END

	DELETE
	FROM [form].[FormCommandCondition]
	WHERE [FormCommandConditionID] = @FormCommandConditionID

	SET @Err = @@Error

	RETURN @Err
END

GO
