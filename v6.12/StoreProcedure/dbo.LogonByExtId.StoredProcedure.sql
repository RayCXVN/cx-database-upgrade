SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[LogonByExtId]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [dbo].[LogonByExtId] AS' 
END
GO
ALTER PROCEDURE [dbo].[LogonByExtId]
(
	@ExtId		nvarchar(64),
	@UserName	nvarchar(128) = NULL, /* OPTIONAL */
	@UserID		int		OUTPUT,
	@DepartmentID	int		OUTPUT,
	@OwnerId	int	,
	@ErrNo		int		OUTPUT
)
AS
BEGIN

DECLARE @EntityStatusID INT = 0
SELECT @EntityStatusID = EntityStatusID FROM dbo.EntityStatus WHERE CodeName = N'Active'

SELECT 	
	@DepartmentID = DepartmentID,
	@UserID = UserID,
	@OwnerId = OwnerId
FROM 	
	[org].[User] 
WHERE 	
	((@UserName IS NULL) OR (UserName = @UserName))
	AND [ExtId] = @ExtId
	AND [ExtId] IS NOT NULL
	AND [ExtId] <> ''
    AND EntityStatusID = @EntityStatusID
	AND Deleted IS NULL
    AND [Ownerid] = @OwnerId

IF @@RowCount < 1 
BEGIN
	SET @ErrNo = 1 /* Ingen brukere ble funnet */
END
ELSE
BEGIN	
	IF @@RowCount > 1 
	BEGIN
		SET @ErrNo = 2 /* Flere brukere med samme propertyvalue ble funnet! */
	END
	ELSE
	BEGIN	
		SET @ErrNo = 0
	END
END
END


GO
