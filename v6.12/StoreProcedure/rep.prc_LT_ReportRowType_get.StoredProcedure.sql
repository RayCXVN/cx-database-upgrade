SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[rep].[prc_LT_ReportRowType_get]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [rep].[prc_LT_ReportRowType_get] AS' 
END
GO

ALTER PROCEDURE [rep].[prc_LT_ReportRowType_get]
(
	@ReportRowTypeID int
)
AS
BEGIN
	SET NOCOUNT ON
	DECLARE @Err Int

	SELECT
	[ReportRowTypeID],
	[LanguageID],
	[Name],
	[Description]
	FROM [rep].[LT_ReportRowType]
	WHERE
	[ReportRowTypeID] = @ReportRowTypeID

	Set @Err = @@Error

	RETURN @Err
END


GO
