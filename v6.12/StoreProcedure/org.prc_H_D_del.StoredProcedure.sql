SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[org].[prc_H_D_del]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [org].[prc_H_D_del] AS' 
END
GO
/*  
    2016-10-10 Ray:     Remove column Status from org.User, org.Department, RDB.dbo.Result
    2016-10-31 Sarah:   Hot fix: Set limit length of string which is used to update UserName
*/
ALTER PROCEDURE [org].[prc_H_D_del]
(
	@HDID int,
	@cUserid int,
	@Log smallint = 1
)
AS
BEGIN
	SET NOCOUNT ON
	DECLARE @Err Int, @DeactiveEntityStatusID int, @StatusReason_ManuallySetDeactive int, @StatusReason_DeactivatedByRelatedObject int

    SELECT @DeactiveEntityStatusID = EntityStatusID FROM EntityStatus WHERE CodeName='Deactive'
    SELECT @StatusReason_ManuallySetDeactive = [EntityStatusReasonID] FROM [EntityStatusReason] WHERE [CodeName] = 'Deactive_ManuallySetDeactive'
    SELECT @StatusReason_DeactivatedByRelatedObject = [EntityStatusReasonID] FROM [EntityStatusReason] WHERE [CodeName] = 'Deactive_SetByRelatedObject'

	IF @Log = 1 
	BEGIN 
		INSERT INTO [Log].[AuditLog] ( UserId, TableName, Type, Data, Created) 
		SELECT @cUserid,'H_D',2,
		( SELECT * FROM [org].[H_D] 
			WHERE
			[HDID] = @HDID
			 FOR XML AUTO) as data,
			getdate() 
	END 

--	DELETE FROM [org].[H_D]
--	WHERE
--	[HDID] = @HDID

    Declare @Ownerid as int
    Declare @HCount as int 
    
    Select @Ownerid = isnull(h.Ownerid,0)
    FROM org.H_D hd
    JOIN org.Hierarchy h on hd.hdid = @HDID and hd.HierarchyID = h.HierarchyID
    
    Select  @HCount = Count(HierarchyID) FROM org.Hierarchy where Ownerid = @Ownerid
     
    IF  @HCount = 1
    BEGIN
      UPDATE org.department SET EntityStatusID = @DeactiveEntityStatusID, [EntityStatusReasonID] = @StatusReason_ManuallySetDeactive
      WHERE departmentid IN (
         SELECT departmentid FROM [org].[H_D] WHERE PATH LIKE '%\' + CAST(@HDID AS NVARCHAR(32)) + '\%'
      )
      
      Update org.[user] set EntityStatusID = @DeactiveEntityStatusID, username = SUBSTRING('DEL_' + cast(userid as nvarchar(16)) + '_' + username,1,128),
                            [EntityStatusReasonID] = @StatusReason_DeactivatedByRelatedObject
      WHERE departmentid IN (
         SELECT departmentid FROM [org].[H_D] WHERE PATH LIKE '%\' + CAST(@HDID AS NVARCHAR(32)) + '\%'
      )
    END

	UPDATE [org].[H_D]
	Set
	[Deleted] = 1
	WHERE
	Path like '%\' + cast(@HDID as nvarchar(32)) + '\%'

	Set @Err = @@Error

	RETURN @Err
END

GO
