SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[org].[prc_U_D_upd]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [org].[prc_U_D_upd] AS' 
END
GO

ALTER PROCEDURE [org].[prc_U_D_upd]
(
	@U_DID int,
	@DepartmentID int,
	@UserID int,
	@Selected bit,
	@cUserid int,
	@Log smallint = 1
)
AS
BEGIN
	SET NOCOUNT ON
	DECLARE @Err Int

	UPDATE [org].[U_D]
	SET
		[DepartmentID] = @DepartmentID,
		[UserID] = @UserID,
		[Selected] = @Selected
	WHERE
		[U_DID] = @U_DID

	IF @Log = 1 
	BEGIN 
		INSERT INTO [Log].[AuditLog] ( UserId, TableName, Type, Data, Created) 
		SELECT @cUserid,'U_D',1,
		( SELECT * FROM [org].[U_D] 
			WHERE
			[U_DID] = @U_DID			 FOR XML AUTO) as data,
			getdate()
	END

	Set @Err = @@Error

	RETURN @Err
END


GO
