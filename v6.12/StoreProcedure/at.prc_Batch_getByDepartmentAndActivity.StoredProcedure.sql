SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[at].[prc_Batch_getByDepartmentAndActivity]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [at].[prc_Batch_getByDepartmentAndActivity] AS' 
END
GO
/*
	Johnny - Des 07 2016 - Update store procedure for dynamic SQL 
*/
ALTER PROCEDURE [at].[prc_Batch_getByDepartmentAndActivity]
(   @HDID                   int,
    @ActivityID             int,
    @GetLastResultUpdate    bit = 0
) AS
BEGIN
    SET NOCOUNT ON;

    DECLARE @DepartmentID int
    CREATE TABLE #BatchTable (SurveyID int, BatchID int, BatchStartDate datetime, BatchEndDate datetime, Created datetime, LastResultUpdate datetime, ReportServer nvarchar(64), ReportDB nvarchar(64))

    SELECT @DepartmentID = hd.[DepartmentID] FROM [org].[H_D] hd WHERE hd.[HDID] = @HDID
    DECLARE @ActiveEntityStatusID VARCHAR(2) = (SELECT EntityStatusID FROM EntityStatus WHERE CodeName='Active')

    INSERT INTO #BatchTable ([SurveyID], [BatchID], [BatchStartDate], [BatchEndDate], [Created], [LastResultUpdate], [ReportServer], [ReportDB])
    SELECT b.[SurveyID], b.[BatchID], b.[StartDate], b.[EndDate], b.[Created], NULL, s.[ReportServer], s.[ReportDB]
    FROM [at].[Batch] b JOIN [at].[Survey] s ON b.[SurveyID] = s.[SurveyID] AND b.[Status] > 0 AND s.[Status] > 0 AND s.[ActivityID] = @ActivityID AND b.[DepartmentID] = @DepartmentID

    IF @GetLastResultUpdate = 1
    BEGIN
        DECLARE c_ReportDB CURSOR LOCAL FAST_FORWARD READ_ONLY FOR
        SELECT DISTINCT [ReportServer], [ReportDB] FROM #BatchTable

        DECLARE @ReportServer nvarchar(64), @ReportDB nvarchar(64), @sqlCommand nvarchar(max)
		DECLARE @LinkedDB NVARCHAR(MAX)

        OPEN c_ReportDB
        FETCH NEXT FROM c_ReportDB INTO @ReportServer, @ReportDB
        WHILE @@FETCH_STATUS = 0
        BEGIN			
			IF @ReportDB=DB_NAME() AND @ReportServer=@@servername
				BEGIN
				SET @LinkedDB=' '
				END
			ELSE
				BEGIN
				SET @LinkedDB= '['+ @ReportServer + '].['+ @ReportDB + '].'
				END
            SET @sqlCommand = N'UPDATE b SET [b].[LastResultUpdate] = [v].[LastResultUpdate]
            FROM #BatchTable b JOIN (SELECT r.[SurveyID], r.[BatchID], MAX(r.[LastUpdated]) [LastResultUpdate]
                                     FROM #BatchTable bt JOIN '+ @LinkedDB +'[dbo].[Result] r ON bt.[BatchID] = r.[BatchID] AND r.[SurveyID] = bt.[SurveyID] AND r.[EntityStatusID] = '+@ActiveEntityStatusID+' AND r.Deleted IS NULL
                                     GROUP BY r.[SurveyID], r.[BatchID]) v ON b.[BatchID] = v.[BatchID] AND b.[SurveyID] = v.[SurveyID]'
            EXEC sp_executesql @sqlCommand

            FETCH NEXT FROM c_ReportDB INTO @ReportServer, @ReportDB
        END
        CLOSE c_ReportDB
        DEALLOCATE c_ReportDB
    END

    SELECT [SurveyID], [BatchID], [BatchStartDate], [BatchEndDate], [Created], [LastResultUpdate] FROM #BatchTable
    DROP TABLE #BatchTable
END

GO
