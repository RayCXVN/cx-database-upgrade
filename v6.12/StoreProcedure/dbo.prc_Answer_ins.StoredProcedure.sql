SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[prc_Answer_ins]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [dbo].[prc_Answer_ins] AS' 
END
GO
ALTER PROCEDURE [dbo].[prc_Answer_ins]
(
    @AnswerID bigint = null output,
    @ResultID bigint,
    @QuestionID int,
    @AlternativeID int,
    @Value float,
    @Free nvarchar(max),
    @No as smallint,
    @DateValue as datetime = null,
    @itemId as int = null,
    @LastUpdatedBy int = 0
)
AS
BEGIN
    SET NOCOUNT ON
    DECLARE @Err Int

    INSERT
    INTO [Answer]
    (
        [ResultID],
        [QuestionID],
        [AlternativeID],
        [Value],
        [Free],
        [No],
        [DateValue],
        [ItemID],
        [LastUpdated],
        [LastUpdatedBy]
    )
    VALUES
    (
        @ResultID,
        @QuestionID,
        @AlternativeID,
        @Value,
        @Free,
        @No,
        @DateValue,
        @itemId,
        GETDATE(),
        @LastUpdatedBy
    )

    Set @Err = @@Error
    set @AnswerID = @@identity

    RETURN @Err
End


GO
