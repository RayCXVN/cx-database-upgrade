SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[list].[prc_LT_ItemListCommand_ins]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [list].[prc_LT_ItemListCommand_ins] AS' 
END
GO
ALTER PROCEDURE [list].[prc_LT_ItemListCommand_ins]
(
	@LanguageID int,
	@ItemListCommandID int,
	@Name nvarchar(256),
	@Description nvarchar(max),
	@Text nvarchar(256),
	@TooltipText nvarchar(512),
    @cUserid int,
    @Log smallint = 1,
    @HotKey nvarchar(5)
)    
AS
BEGIN
	SET NOCOUNT ON;
	DECLARE @Err Int
	
	INSERT INTO [list].[LT_ItemListCommand]
           ([LanguageID]
           ,[ItemListCommandID]
           ,[Name]
           ,[Description]
           ,[Text]
           ,[TooltipText]
           ,[HotKey])
     VALUES
           (@LanguageID
           ,@ItemListCommandID
           ,@Name
           ,@Description
           ,@Text
           ,@TooltipText
           ,@HotKey)
           
     Set @Err = @@Error
	 IF @Log = 1 
	 BEGIN 
		INSERT INTO [Log].[AuditLog] ( UserId, TableName, Type, Data, Created) 
		SELECT @cUserid,'LT_ItemListCommand',0,
		( SELECT * FROM [list].[LT_ItemListCommand] 
			WHERE
			[LanguageID] = @LanguageID AND
			[ItemListCommandID] = @ItemListCommandID				 FOR XML AUTO) as data,
				getdate() 
	  END

	
	 RETURN @Err
END

GO
