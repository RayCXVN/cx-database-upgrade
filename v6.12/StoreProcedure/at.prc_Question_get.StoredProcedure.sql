SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[at].[prc_Question_get]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [at].[prc_Question_get] AS' 
END
GO

ALTER PROCEDURE [at].[prc_Question_get](
    @PageID int)
AS
BEGIN
    SET NOCOUNT ON;
    DECLARE @Err int;

    SELECT [QuestionID], [PageID], ISNULL([ScaleID], 0) AS 'ScaleID', [No], [Type], [Inverted], [Mandatory], [Status], [CssClass], [ExtId], [Tag], [Created],
           ISNULL([SectionID], 0) AS 'SectionID', [Width]
    FROM [at].[Question]
    WHERE [PageID] = @PageID
    ORDER BY [No];

    SET @Err = @@Error;

    RETURN @Err;
END;

GO