SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[list].[prc_LT_ItemListCommand_get]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [list].[prc_LT_ItemListCommand_get] AS' 
END
GO
ALTER PROCEDURE [list].[prc_LT_ItemListCommand_get]
	@ItemListCommandID int
AS
BEGIN
	SET NOCOUNT ON;
	DECLARE @Err Int
	
    SELECT 
		[LanguageID],
		[ItemListCommandID],
		[Name],
		[Description],
		[Text],
		[TooltipText],
		[HotKey]
	FROM [list].[LT_ItemListCommand]
	WHERE [ItemListCommandID] = @ItemListCommandID
	
	Set @Err = @@Error

	RETURN @Err
  
END

GO
