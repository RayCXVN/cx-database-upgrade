IF NOT EXISTS (SELECT 1 FROM sys.objects WHERE name = N'prc_MemberRole_get' AND type IN ( N'P', N'PC' )  AND SCHEMA_NAME(schema_id) = 'org')
	EXEC ('CREATE PROC [org].[prc_MemberRole_get] AS ')
GO
/*  
    2018-07-19 Sarah    Create procedures for Insert, Update, Delete, Get of org.MemberRole
    2019-02-21 Ray      Get by OwnerID
*/
ALTER PROCEDURE [org].[prc_MemberRole_get]
(
    @OwnerID int
)
AS
BEGIN
    SET NOCOUNT ON
    DECLARE @Err Int

    SELECT [MemberRoleID], [OwnerID], [ExtID], [No], [Created], [EntityStatusID], [EntityStatusReasonID], [MasterID]
    FROM [org].[MemberRole]
    WHERE [OwnerID] = @OwnerID;

    SET @Err = @@Error

    RETURN @Err
END