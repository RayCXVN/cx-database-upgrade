SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[proc].[prc_LT_ProcessValue_upd]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [proc].[prc_LT_ProcessValue_upd] AS' 
END
GO
ALTER PROCEDURE [proc].[prc_LT_ProcessValue_upd]
(
	@ProcessValueID int,
	@LanguageID int,
	@Name nvarchar(256),
	@Description nvarchar(max),
	@URLName nvarchar(256),
	@LeadText nvarchar(max), 
	@LeadAnswerText nvarchar(max), 
	@cUserid int,
	@Log smallint = 1
)
AS
BEGIN
	SET NOCOUNT ON
	DECLARE @Err Int

	UPDATE [proc].[LT_ProcessValue]
	SET
		[ProcessValueID] = @ProcessValueID,
		[LanguageID] = @LanguageID,
		[Name] = @Name,
		[Description] = @Description,
		[URLName] = @URLName,
		[LeadText] = @LeadText,
		[LeadAnswerText] = @LeadAnswerText   
		
	WHERE
		[ProcessValueID] = @ProcessValueID AND
		[LanguageID] = @LanguageID

	IF @Log = 1 
	BEGIN 
		INSERT INTO [Log].[AuditLog] ( UserId, TableName, Type, Data, Created) 
		SELECT @cUserid,'LT_ProcessValue',1,
		( SELECT * FROM [proc].[LT_ProcessValue] 
			WHERE
			[ProcessValueID] = @ProcessValueID AND
			[LanguageID] = @LanguageID			 FOR XML AUTO) as data,
			getdate()
	END

	Set @Err = @@Error

	RETURN @Err
END

GO
