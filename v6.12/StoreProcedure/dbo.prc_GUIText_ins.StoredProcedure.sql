SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[prc_GUIText_ins]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [dbo].[prc_GUIText_ins] AS' 
END
GO

ALTER PROCEDURE [dbo].[prc_GUIText_ins]
(
	@LanguageID int,
	@ItemID int,
	@Text nvarchar(max),
	@cUserid int,
	@Log smallint = 1
)
AS
BEGIN
	SET NOCOUNT ON
	DECLARE @Err Int

	INSERT INTO [dbo].[GUIText]
	(
		[LanguageID],
		[ItemID],
		[Text]
	)
	VALUES
	(
		@LanguageID,
		@ItemID,
		@Text
	)

	Set @Err = @@Error

	IF @Log = 1 
	BEGIN 
		INSERT INTO [Log].[AuditLog] ( UserId, TableName, Type, Data, Created) 
		SELECT @cUserid,'GUIText',0,
		( SELECT * FROM [dbo].[GUIText] 
			WHERE
			[LanguageID] = @LanguageID AND
			[ItemID] = @ItemID				 FOR XML AUTO) as data,
				getdate() 
	 END

	RETURN @Err
END


GO
