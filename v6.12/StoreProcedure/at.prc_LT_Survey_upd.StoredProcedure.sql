SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[at].[prc_LT_Survey_upd]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [at].[prc_LT_Survey_upd] AS' 
END
GO




ALTER PROCEDURE [at].[prc_LT_Survey_upd]
(
	@LanguageID int,
	@SurveyID int,
	@Name nvarchar(256),
	@Description nvarchar(max),
	@Info nvarchar(max),
	@FinishText nvarchar(max),
	@DisplayName nvarchar(256) = '',
	@cUserid int,
	@Log smallint = 1
)
AS
BEGIN
	SET NOCOUNT ON
	DECLARE @Err Int

	UPDATE [at].[LT_Survey]
	SET
		[LanguageID] = @LanguageID,
		[SurveyID] = @SurveyID,
		[Name] = @Name,
		[Description] = @Description,
		[Info] = @Info,
		[FinishText] = @FinishText,
		[DisplayName] = @DisplayName
	WHERE
		[LanguageID] = @LanguageID AND
		[SurveyID] = @SurveyID

	IF @Log = 1 
	BEGIN 
		INSERT INTO [Log].[AuditLog] ( UserId, TableName, Type, Data, Created) 
		SELECT @cUserid,'LT_Survey',1,
		( SELECT * FROM [at].[LT_Survey] 
			WHERE
			[LanguageID] = @LanguageID AND
			[SurveyID] = @SurveyID			 FOR XML AUTO) as data,
			getdate()
	END

	Set @Err = @@Error

	RETURN @Err
END



GO
