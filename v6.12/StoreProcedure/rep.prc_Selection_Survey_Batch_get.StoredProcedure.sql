SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[rep].[prc_Selection_Survey_Batch_get]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [rep].[prc_Selection_Survey_Batch_get] AS' 
END
GO

ALTER PROCEDURE [rep].[prc_Selection_Survey_Batch_get]
(
	@S_SurveyID int
)
AS
BEGIN
	SET NOCOUNT ON
	DECLARE @Err Int

	SELECT
	[S_SurveyID],
	[BatchID]
	FROM [rep].[Selection_Survey_Batch]
	WHERE
	[S_SurveyID] = @S_SurveyID

	Set @Err = @@Error

	RETURN @Err
END


GO
