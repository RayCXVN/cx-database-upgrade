SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[prc_CountResultByDepartmentID]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [dbo].[prc_CountResultByDepartmentID] AS' 
END
GO
ALTER PROCEDURE [dbo].[prc_CountResultByDepartmentID] 
(
	@DepartmentID		int
)
AS
BEGIN
	SET NOCOUNT ON
	DECLARE @Err Int
    DECLARE @ActiveEntityStatusID INT = (SELECT EntityStatusID FROM EntityStatus WHERE CodeName='Active')

	SELECT COUNT(ResultID) FROM [dbo].[Result] WHERE DepartmentID=@DepartmentID AND Result.EntityStatusID = @ActiveEntityStatusID AND Result.Deleted IS NULL
	
	SET @Err = @@Error

	RETURN @Err
END



GO
