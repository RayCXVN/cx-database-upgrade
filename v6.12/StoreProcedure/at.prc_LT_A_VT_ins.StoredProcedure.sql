SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[at].[prc_LT_A_VT_ins]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [at].[prc_LT_A_VT_ins] AS' 
END
GO

ALTER PROCEDURE [at].[prc_LT_A_VT_ins]
(
	@LanguageID int,
	@AVTID int,
	@Name nvarchar(256),
	@Description ntext,
	@TextAbove ntext,
	@TextBelow ntext,
	@cUserid int,
	@Log smallint = 1
)
AS
BEGIN
	SET NOCOUNT ON
	DECLARE @Err Int

	INSERT INTO [at].[LT_A_VT]
	(
		[LanguageID],
		[AVTID],
		[Name],
		[Description],
		[TextAbove],
		[TextBelow]
	)
	VALUES
	(
		@LanguageID,
		@AVTID,
		@Name,
		@Description,
		@TextAbove,
		@TextBelow
	)

	Set @Err = @@Error

	IF @Log = 1 
	BEGIN 
		INSERT INTO [Log].[AuditLog] ( UserId, TableName, Type, Data, Created) 
		SELECT @cUserid,'LT_A_VT',0,
		( SELECT * FROM [at].[LT_A_VT] 
			WHERE
			[LanguageID] = @LanguageID AND
			[AVTID] = @AVTID				 FOR XML AUTO) as data,
				getdate() 
	 END

	RETURN @Err
END


GO
