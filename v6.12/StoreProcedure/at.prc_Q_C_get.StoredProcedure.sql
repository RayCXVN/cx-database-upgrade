SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[at].[prc_Q_C_get]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [at].[prc_Q_C_get] AS' 
END
GO

ALTER PROCEDURE [at].[prc_Q_C_get]
(
	@CategoryID int
)
AS
BEGIN
	SET NOCOUNT ON
	DECLARE @Err Int

	SELECT
	[QuestionID],
	[CategoryID],
	[Weight],
	[No],
	[ItemID],
	[Visible]
	FROM [at].[Q_C]
	WHERE
	[CategoryID] = @CategoryID
	ORDER BY [No]

	Set @Err = @@Error

	RETURN @Err
END


GO
