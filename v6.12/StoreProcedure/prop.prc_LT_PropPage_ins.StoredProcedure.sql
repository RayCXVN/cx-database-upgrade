SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[prop].[prc_LT_PropPage_ins]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [prop].[prc_LT_PropPage_ins] AS' 
END
GO

ALTER PROCEDURE [prop].[prc_LT_PropPage_ins]
(
	@LanguageID int,
	@PropPageID int,
	@Name nvarchar(256),
	@Description nvarchar(max),
	@cUserid int,
	@Log smallint = 1
)
AS
BEGIN
	SET NOCOUNT ON
	DECLARE @Err Int

	INSERT INTO [prop].[LT_PropPage]
	(
		[LanguageID],
		[PropPageID],
		[Name],
		[Description]
	)
	VALUES
	(
		@LanguageID,
		@PropPageID,
		@Name,
		@Description
	)

	Set @Err = @@Error

	IF @Log = 1 
	BEGIN 
		INSERT INTO [Log].[AuditLog] ( UserId, TableName, Type, Data, Created) 
		SELECT @cUserid,'LT_PropPage',0,
		( SELECT * FROM [prop].[LT_PropPage] 
			WHERE
			[LanguageID] = @LanguageID AND
			[PropPageID] = @PropPageID				 FOR XML AUTO) as data,
				getdate() 
	 END

	RETURN @Err
END


GO
