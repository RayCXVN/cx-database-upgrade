SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[log].[prc_LT_EventType_ins]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [log].[prc_LT_EventType_ins] AS' 
END
GO

ALTER PROCEDURE [log].[prc_LT_EventType_ins]
(
	@EventTypeID	int,
	@LanguageID	int,
	@Name		nvarchar(256),
	@Description	nvarchar(max),
	@cUserid		int,
	@Log			smallint = 1
)
AS
BEGIN
	SET NOCOUNT ON
	DECLARE @Err Int

	INSERT INTO [log].[LT_EventType]
	(
		[LanguageID],
		[EventTypeID],
		[Name],
		[Description]
	)
	VALUES
	(
		@LanguageID,
		@EventTypeID,
		@Name,
		@Description
	)

	Set @Err = @@Error
	
	IF @Log = 1 
	BEGIN 
		INSERT INTO [Log].[AuditLog] ( UserId, TableName, Type, Data, Created) 
		SELECT @cUserid,'LT_EventType',0,
		( SELECT * FROM [log].[LT_EventType]
			WHERE
			[EventTypeID] = @EventTypeID AND [LanguageID] = @LanguageID FOR XML AUTO) as data,
				getdate() 
	END

	RETURN @Err
END


GO
