SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[at].[prc_LT_ActivityView_get]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [at].[prc_LT_ActivityView_get] AS' 
END
GO


ALTER PROCEDURE [at].[prc_LT_ActivityView_get]
(
	@ActivityViewID int
)
AS
BEGIN
	SET NOCOUNT ON
	DECLARE @Err Int

	SELECT
	[LanguageID],
	[ActivityViewID],
	[Name],
	[Description]
	FROM [at].[LT_ActivityView]
	WHERE
	[ActivityViewID] = @ActivityViewID

	Set @Err = @@Error

	RETURN @Err
END



GO
