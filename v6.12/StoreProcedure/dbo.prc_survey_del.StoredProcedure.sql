SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[prc_survey_del]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [dbo].[prc_survey_del] AS' 
END
GO
ALTER proc [dbo].[prc_survey_del]
(
  @Surveyid as integer
)
as

 Delete from result where surveyid = @Surveyid


GO
