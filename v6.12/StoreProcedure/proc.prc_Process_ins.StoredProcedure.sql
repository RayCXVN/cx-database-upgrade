SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[proc].[prc_Process_ins]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [proc].[prc_Process_ins] AS' 
END
GO
/*
	2017-07-26 Steve: Change the length of column ExtID in all related tables from xAPI format
*/
ALTER PROCEDURE [proc].[prc_Process_ins]
(
	@ProcessID int = null output,
	@ProcessGroupID int,
	@DepartmentID int,
	@Active smallint,
	@TargetValue float,
	@UserID int,
	@CustomerID INT=NULL,
	@StartDate DATETIME=NULL,
	@DueDate DATETIME=NULL,
	@Responsible nvarchar(128),
	@URL nvarchar(512),
	@No smallint,
	@ProcessTypeID int = NULL,
	@isPublic smallint = 0,
	@PeriodID int = NULL,
	@cUserid int,
	@Log smallint = 1,
	@Created DATETIME = NULL,
	@ExtID nvarchar(256)=''
)
AS
BEGIN
	SET NOCOUNT ON
	DECLARE @Err Int

	SET @Created = ISNULL(@Created, GETDATE())

	INSERT INTO [proc].[Process]
	(
		[ProcessGroupID],
		[DepartmentID],
		[Active],
		[TargetValue],
		[UserID],
		[CustomerID],
		[StartDate],
		[DueDate],
		[Responsible],
		[URL],
		[No],
		[ProcessTypeID],
		[isPublic],
		[PeriodID],
		[Created],
		[ExtID]
	)
	VALUES
	(
		@ProcessGroupID,
		@DepartmentID,
		@Active,
		@TargetValue,
		@UserID,
		@CustomerID,
		@StartDate,
		@DueDate,
		@Responsible,
		@URL,
		@No,
		@ProcessTypeID,
		@isPublic,
		@PeriodID,
		@Created,
		@ExtID
	)

	Set @Err = @@Error
	Set @ProcessID = scope_identity()

	IF @Log = 1 
	BEGIN 
		INSERT INTO [Log].[AuditLog] ( UserId, TableName, Type, Data, Created) 
		SELECT @cUserid,'Process',0,
		( SELECT * FROM [proc].[Process] 
			WHERE
			[ProcessID] = @ProcessID				 FOR XML AUTO) as data,
				getdate() 
	 END

	RETURN @Err
END

GO
