SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[at].[prc_ReportCalcType_ins]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [at].[prc_ReportCalcType_ins] AS' 
END
GO

ALTER PROCEDURE [at].[prc_ReportCalcType_ins]
(
	@ReportCalcTypeID int,
	@No smallint,
	@cUserid int,
	@Log smallint = 1
)
AS
BEGIN
	SET NOCOUNT ON
	DECLARE @Err Int

	INSERT INTO [at].[ReportCalcType]
	(
		[ReportCalcTypeID],
		[No]
	)
	VALUES
	(
		@ReportCalcTypeID,
		@No
	)

	Set @Err = @@Error

	IF @Log = 1 
	BEGIN 
		INSERT INTO [Log].[AuditLog] ( UserId, TableName, Type, Data, Created) 
		SELECT @cUserid,'ReportCalcType',0,
		( SELECT * FROM [at].[ReportCalcType] 
			WHERE
			[ReportCalcTypeID] = @ReportCalcTypeID				 FOR XML AUTO) as data,
				getdate() 
	 END

	RETURN @Err
END


GO
