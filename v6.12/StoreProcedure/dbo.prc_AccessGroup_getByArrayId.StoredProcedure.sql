SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[prc_AccessGroup_getByArrayId]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [dbo].[prc_AccessGroup_getByArrayId] AS' 
END
GO

/*
19:1054,
exec prc_AccessGroup_getByArrayId '17:2,17:3,17:4,17:5,17:6,17:7,17:8,17:9,17:10,17:11,17:12,17:13,17:14,17:15,17:16,17:17,17:18,17:19,17:20,17:21,17:22,17:23,17:25,17:26,17:27,17:55,17:56,17:57,17:59,17:60,17:61,17:63,17:65,17:66', 9 , 36268
*/

ALTER PROC [dbo].[prc_AccessGroup_getByArrayId]
(  
  @arrayId varchar(max),
  @OwnerID int,
  @UserId int  
)
AS
	--Tao bang tu @arrayId  
	Declare @tableTE Table
	(
		ElementID int,
		TableTypeID int,		
		IsAccess bit default 0		
	)
	
	Insert into @tableTE(ElementID, TableTypeID)  
	Select  ElementID, TableTypeID 
	from SplitString(@arrayId)
		
	--ket gom @tableTE, AccessGroup, AccessGeneric:listAccessGenerics	
	Update @tableTE
	Set	IsAccess = IsNull((SELECT 0
					WHERE EXISTS (SELECT 1
					FROM  dbo.AccessGeneric a JOIN dbo.AccessGroup ag ON a.AccessGroupID = ag.AccessGroupID							
					WHERE a.TableTypeID = TE.TableTypeID AND a.Elementid = TE.ElementID
					)),1)
	From @tableTE TE
	
	Declare @DepartmentId int
	Declare @RoleId int
	Declare @CustomerId int

	Select @DepartmentId = u.DepartmentID, @RoleId = u.RoleID, @CustomerId = d.CustomerID
	From	org.[User] u Join org.Department d On (u.DepartmentID = d.DepartmentID)
	Where u.UserID = @UserId
	OPTION(RECOMPILE)

	Declare @tableAccessGroup Table
	(
	AccessGroupID int,
	OwnerID int,
	[No] smallint,
	Created smalldatetime
	)
		
	If Exists (Select * From @tableTE Where IsAccess = 0)
	Begin
	  Insert Into @tableAccessGroup(AccessGroupID,OwnerID,[No],Created)
	  Exec [dbo].[prc_AccessGroup_getByUserid] @OwnerId ,@UserId ,@Departmentid ,@RoleId ,@CustomerID
	
	  Update @tableTE  
	  Set	IsAccess = IsNull((SELECT 1
					WHERE EXISTS (SELECT 1
					FROM  dbo.AccessGeneric a JOIN @tableAccessGroup tag ON a.AccessGroupID = tag.AccessGroupID							
					WHERE a.TableTypeID = TE.TableTypeID AND a.Elementid = TE.ElementID
					)),0)
	  From	@tableTE TE
	  Where TE.IsAccess = 0	  
	  	  
	End

	Select TableTypeID,ElementID
	From @tableTE
	Where IsAccess = 1
	
	

GO
