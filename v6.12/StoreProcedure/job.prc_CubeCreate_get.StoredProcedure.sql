SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[job].[prc_CubeCreate_get]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [job].[prc_CubeCreate_get] AS' 
END
GO
ALTER PROC [job].[prc_CubeCreate_get]
AS
  SELECT top 5 S.SurveyID,s.ActivityID,COALESCE(S.OLAPDB,A.OLAPDB,O.OLAPDB) AS OLAPDB,
  COALESCE(S.OLAPServer,A.OLAPServer,O.OLAPServer) AS OLAPServer,
  COALESCE(S.ReportDB,O.ReportDB) AS ReportDB,
  COALESCE(S.ReportServer,O.ReportServer) AS ReportServer,
  S.LastProcessed
  FROM AT.Survey S
  JOIN AT.Activity A on A.ActivityID = S.ActivityID AND A.UseOLAP = 1
  JOIN ORG.[Owner] O on O.Ownerid = A.Ownerid 
  WHERE S.LastProcessed = '1900-01-01' and Datepart(hh,getdate()) IN (1,2,3)
GO
