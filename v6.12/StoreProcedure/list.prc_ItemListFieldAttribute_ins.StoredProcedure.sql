SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[list].[prc_ItemListFieldAttribute_ins]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [list].[prc_ItemListFieldAttribute_ins] AS' 
END
GO
ALTER PROCEDURE [list].[prc_ItemListFieldAttribute_ins]
(
	@ItemListFieldAttributeID int = null output,
	@ItemListFieldID int,
	@key nvarchar(64),
	@Value nvarchar(max)='',
    @cUserid int,
    @Log smallint = 1
)    
AS
BEGIN
	SET NOCOUNT ON;
	DECLARE @Err Int
	
	INSERT INTO [list].[ItemListFieldAttribute]
           ([ItemListFieldID]
           ,[key]
           ,[Value])
     VALUES
           (@ItemListFieldID
           ,@key
           ,@Value)
           
     Set @Err = @@Error
	 Set @ItemListFieldAttributeID = scope_identity()
	 IF @Log = 1 
	 BEGIN 
		INSERT INTO [Log].[AuditLog] ( UserId, TableName, Type, Data, Created) 
		SELECT @cUserid,'ItemListFieldAttribute',0,
		( SELECT * FROM [list].[ItemListFieldAttribute]
			WHERE
			[ItemListFieldAttributeID] = @ItemListFieldAttributeID				 FOR XML AUTO) as data,
				getdate() 
	 END

	
	 RETURN @Err
END

GO
