SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[prc_Language_upd]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [dbo].[prc_Language_upd] AS' 
END
GO

ALTER PROCEDURE [dbo].[prc_Language_upd]
(
	@LanguageID int,
	@LanguageCode varchar(5),
	@Dir varchar(3),
	@Name nvarchar(64),
	@cUserid int,
	@Log smallint = 1
)
AS
BEGIN
	SET NOCOUNT ON
	DECLARE @Err Int

	UPDATE [dbo].[Language]
	SET
		[LanguageID] = @LanguageID,
		[LanguageCode] = @LanguageCode,
		[Dir] = @Dir,
		[Name] = @Name
	WHERE
		[LanguageID] = @LanguageID

	IF @Log = 1 
	BEGIN 
		INSERT INTO [Log].[AuditLog] ( UserId, TableName, Type, Data, Created) 
		SELECT @cUserid,'Language',1,
		( SELECT * FROM [dbo].[Language] 
			WHERE
			[LanguageID] = @LanguageID			 FOR XML AUTO) as data,
			getdate()
	END

	Set @Err = @@Error

	RETURN @Err
END


GO
