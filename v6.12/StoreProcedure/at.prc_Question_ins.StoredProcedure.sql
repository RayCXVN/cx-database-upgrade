SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[at].[prc_Question_ins]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [at].[prc_Question_ins] AS' 
END
GO
ALTER PROCEDURE [at].[prc_Question_ins](
    @QuestionID int           = NULL OUTPUT,
    @PageID     int,
    @ScaleID    int           = NULL,
    @No         [smallint],
    @Type       [smallint],
    @Inverted   bit,
    @Mandatory  bit,
    @Status     [smallint],
    @CssClass   nvarchar(64),
    @ExtId      nvarchar(256),
    @Tag        nvarchar(128),
    @SectionID  int           = NULL,
    @cUserid    int,
    @Log        [smallint]    = 1,
    @Width      int           = NULL)
AS
BEGIN
    SET NOCOUNT ON;
    DECLARE @Err int;

    INSERT INTO [at].[Question] ([PageID], [ScaleID], [No], [Type], [Inverted], [Mandatory], [Status], [CssClass], [ExtId], [Tag], [SectionID], [Width])
    VALUES (@PageID, @ScaleID, @No, @Type, @Inverted, @Mandatory, @Status, @CssClass, @ExtId, @Tag, @SectionID, @Width);

    SET @Err = @@Error;
    SET @QuestionID = SCOPE_IDENTITY();

    IF @Log = 1
    BEGIN
        INSERT INTO [Log].[AuditLog] ([UserId], [TableName], [Type], [Data], [Created])
        SELECT @cUserid, 'Question', 0, (SELECT * FROM [at].[Question] WHERE [QuestionID] = @QuestionID FOR XML AUTO) AS [data], GETDATE();
    END;

    RETURN @Err;
END;

GO