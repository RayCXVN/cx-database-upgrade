SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[rep].[prc_LT_ReportColumnType_del]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [rep].[prc_LT_ReportColumnType_del] AS' 
END
GO


ALTER PROCEDURE [rep].[prc_LT_ReportColumnType_del]
(
	@ReportColumnTypeID int,
	@LanguageID int,
	@cUserid int,
	@Log smallint = 1
)
AS
BEGIN
	SET NOCOUNT ON
	DECLARE @Err Int

	IF @Log = 1 
	BEGIN 
		INSERT INTO [Log].[AuditLog] ( UserId, TableName, Type, Data, Created) 
		SELECT @cUserid,'LT_ReportColumnType',2,
		( SELECT * FROM [rep].[LT_ReportColumnType] 
			WHERE
			[ReportColumnTypeID] = @ReportColumnTypeID AND
			[LanguageID] = @LanguageID
			 FOR XML AUTO) as data,
			getdate() 
	END 


	DELETE FROM [rep].[LT_ReportColumnType]
	WHERE
		[ReportColumnTypeID] = @ReportColumnTypeID AND
		[LanguageID] = @LanguageID

	Set @Err = @@Error

	RETURN @Err
END


GO
