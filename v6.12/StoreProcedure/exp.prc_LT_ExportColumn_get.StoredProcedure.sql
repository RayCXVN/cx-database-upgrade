SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[exp].[prc_LT_ExportColumn_get]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [exp].[prc_LT_ExportColumn_get] AS' 
END
GO
ALTER PROCEDURE [exp].[prc_LT_ExportColumn_get]
(
	@ExportColumnID smallint
)
AS
BEGIN
	SET NOCOUNT ON
	DECLARE @Err Int

	SELECT
	[LanguageID],
	[ExportColumnID],
	[Name],
	[Description]
	FROM [exp].[LT_ExportColumn]
	WHERE
	[ExportColumnID] = @ExportColumnID

	Set @Err = @@Error

	RETURN @Err
END

GO
