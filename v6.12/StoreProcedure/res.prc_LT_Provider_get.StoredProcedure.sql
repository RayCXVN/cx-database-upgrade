SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[res].[prc_LT_Provider_get]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [res].[prc_LT_Provider_get] AS' 
END
GO
  
ALTER PROCEDURE [res].[prc_LT_Provider_get]  
(  
	@ProviderID int  
)  
AS  
BEGIN  
 SET NOCOUNT ON  
 DECLARE @Err Int  
  
 SELECT  
	[ProviderID],  
	[LanguageID],  
	[Name],  
	[Description]  
 FROM [res].[LT_Provider]  
 WHERE  
	[ProviderID] = @ProviderID  
  
 Set @Err = @@Error  
  
 RETURN @Err  
END  

GO
