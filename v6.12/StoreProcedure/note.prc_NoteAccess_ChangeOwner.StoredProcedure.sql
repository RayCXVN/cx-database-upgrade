SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[note].[prc_NoteAccess_ChangeOwner]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [note].[prc_NoteAccess_ChangeOwner] AS' 
END
GO
ALTER PROCEDURE [note].[prc_NoteAccess_ChangeOwner]

@Err int out,
@NoteIDs varchar (max),
@OldOwnerID int,
@NewOwnerID int
AS
BEGIN
SET NOCOUNT ON
SET @Err = 0

declare @tableType int;

DECLARE @tbNoteId table (Nid int)

SELECT TOP 1 @tableType = tt.TableTypeID 
FROM TableType tt
WHERE tt.Name = 'User'

INSERT INTO @tbNoteId (NId)
SELECT ParseValue
FROM dbo.StringToArray(@NoteIDs, ',')
IF @@ERROR <> 0
BEGIN
SET @Err = @@ERROR
GOTO Finish
END

BEGIN TRANSACTION

--Case: old counselor DIDN'T CREATE note and new counselor IS NOT EXISTS in NoteAccess
UPDATE na
SET na.ItemID = @NewOwnerId
FROM note.NoteAccess na
WHERE na.NoteID IN (SELECT	Nid FROM @tbNoteId)
AND na.TableTypeID = @tableType AND na.ItemID = @OldOwnerID
AND NOT EXISTS (SELECT TOP 1 1 FROM note.Note n 
				WHERE n.NoteID = na.NoteID AND (n.CreatedBy = @OldOwnerID OR n.CreatedBy = @NewOwnerId))
AND NOT EXISTS (SELECT TOP 1 1 FROM note.NoteAccess na1
				WHERE na1.NoteID = na.NoteID AND na1.TableTypeID = @tableType AND na1.ItemID = @NewOwnerId)

IF @@ERROR <> 0 AND @@TRANCOUNT > 0
BEGIN
SET @Err = @@ERROR
ROLLBACK TRANSACTION
GOTO Finish
END

--Case: old counselor CREATED note and new counselor IS NOT EXISTS in NoteAccess 
--(insert noteacess with note which is not created by new counselor)


INSERT INTO note.NoteAccess (NoteID, TableTypeID, ItemID, [Type], Created)
SELECT n.NoteID,@tableType,	@NewOwnerID,1,	GETDATE()
FROM note.Note n
WHERE n.NoteID IN (SELECT Nid FROM @tbNoteId)
AND n.CreatedBy = @OldOwnerID
AND NOT EXISTS (SELECT TOP 1 1 FROM note.NoteAccess na
WHERE n.NoteID = na.NoteID AND na.TableTypeID = @tableType AND na.ItemID = @NewOwnerID)

IF @@ERROR <> 0 AND @@TRANCOUNT > 0
BEGIN
SET @Err = @@ERROR
ROLLBACK TRANSACTION
GOTO Finish
END

-- Case: old counselor CREATED  and new counselor IS EXISTS in NoteAccess (1: Full)
update na
SET na.[Type]=1 
from note.NoteAccess na
where na.NoteID in (SELECT Nid from @tbNoteId)
and na.ItemID=@NewOwnerID
and na.TableTypeID=@tableType
and EXISTS (SELECT TOP 1 1 from  note.Note n 
where n.NoteID=na.NoteID and n.CreatedBy=@OldOwnerID ) 

	
IF @@ERROR <> 0 AND @@TRANCOUNT > 0
BEGIN
SET @Err = @@ERROR
ROLLBACK TRANSACTION
GOTO Finish
END

--Case: Both old and new counselor is exists in NoteAcess
UPDATE newNa
SET newNa.[Type] = oldNa.[Type]
FROM note.NoteAccess newNa
INNER JOIN note.NoteAccess oldNa ON oldNa.NoteID = newNa.NoteID AND newNa.TableTypeID = oldNa.TableTypeID
AND newNa.[Type] <> oldNa.[Type]
WHERE newNa.NoteID IN (SELECT Nid FROM @tbNoteId)
AND newNa.ItemID = @NewOwnerID 
AND oldNa.ItemID = @OldOwnerID
AND newNa.TableTypeID = @tableType

IF @@ERROR <> 0 AND @@TRANCOUNT > 0
BEGIN
SET @Err = @@ERROR
ROLLBACK TRANSACTION
GOTO Finish
END

--REMOVE old counselor's right
DELETE FROM note.NoteAccess
WHERE TableTypeID = @tableType and NoteID IN (SELECT Nid	FROM @tbNoteId)
AND ( ItemID = @OldOwnerID
or (ItemID =@NewOwnerID AND Type=1
AND EXISTS(SELECT TOP 1 1 from note.Note where CreatedBy=@NewOwnerID and NoteID=note.NoteAccess.NoteID)))


--Insert denied right to CREATER
INSERT INTO note.NoteAccess (NoteID, TableTypeID, ItemID, [Type], Created)
SELECT n.NoteID,@tableType,	@OldOwnerID,2,	GETDATE()
FROM note.Note n
WHERE n.NoteID IN (SELECT Nid FROM @tbNoteId)
AND n.CreatedBy = @OldOwnerID

IF @@ERROR <> 0 AND @@TRANCOUNT > 0
BEGIN
SET @Err = @@ERROR
ROLLBACK TRANSACTION
GOTO Finish
END

COMMIT TRANSACTION

Finish:
	RETURN @Err

END

GO
