SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[proc].[prc_ProcessValue_upd]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [proc].[prc_ProcessValue_upd] AS' 
END
GO
ALTER PROCEDURE [proc].[prc_ProcessValue_upd]
(
	@ProcessValueID int,
	@No smallint,
	@Value smallint,
	@BackGroundColor nvarchar(16),
	@OwnerID int,
	@ProcessTypeID int = NULL,
	@URL nvarchar(256) = '',
	@ReadOnly bit = 0,
	@cUserid int,
	@Log smallint = 1
)
AS
BEGIN
	SET NOCOUNT ON
	DECLARE @Err Int

	UPDATE [proc].[ProcessValue]
	SET
		[No] = @No,
		[Value] = @Value,
		[BackGroundColor] = @BackGroundColor,
		[OwnerID] = @OwnerID,
		[ProcessTypeID] = @ProcessTypeID,
		[URL] = @URL,
		[ReadOnly] = @ReadOnly
	WHERE
		[ProcessValueID] = @ProcessValueID

	IF @Log = 1 
	BEGIN 
		INSERT INTO [Log].[AuditLog] ( UserId, TableName, Type, Data, Created) 
		SELECT @cUserid,'ProcessValue',1,
		( SELECT * FROM [proc].[ProcessValue] 
			WHERE
			[ProcessValueID] = @ProcessValueID			 FOR XML AUTO) as data,
			getdate()
	END

	Set @Err = @@Error

	RETURN @Err
END

GO
