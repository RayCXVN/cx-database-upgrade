SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[prc_Answer_upd]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [dbo].[prc_Answer_upd] AS' 
END
GO
ALTER PROCEDURE [dbo].[prc_Answer_upd]
(
    @AnswerID bigint,
    @ResultID bigint,
    @QuestionID int,
    @AlternativeID int,
    @Value float,
    @Free nvarchar(max),
    @No smallint,
    @DateValue as datetime = null,
    @itemID as int = null,
    @LastUpdatedBy int = 0
)
AS
BEGIN
    SET NOCOUNT ON
    DECLARE @Err Int

    UPDATE [Answer]
    SET
    [ResultID] = @ResultID,
    [QuestionID] = @QuestionID,
    [AlternativeID] = @AlternativeID,
    [Value] = @Value,
    [Free] = @Free,
    [No] = @No,
    [DateValue] = @DateValue,
    [ItemID] = @itemID,
    [LastUpdated] = GETDATE(),
    [LastUpdatedBy] = @LastUpdatedBy
    WHERE
    [AnswerID] = @AnswerID

    Set @Err = @@Error

    RETURN @Err
End


GO
