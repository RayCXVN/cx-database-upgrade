SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[CreateJobForPDFCreation]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [dbo].[CreateJobForPDFCreation] AS' 
END
GO
/*  
	Steve - May 19, 2017 - Change text Vokal --> Engage, SiteID 3 --> 5
*/
ALTER PROCEDURE [dbo].[CreateJobForPDFCreation]( 
	@userid as int, 
	@hdid_school as int, 
	@username as nvarchar(128), 
	@password as nvarchar(64),
	@server as varchar(100)=@@servername,
	@database as varchar(100),
	@userids as nvarchar(max) = '',
	@LanguageID as int = 0,
    @SiteID as int = 5,
    @FileName nvarchar(max) = '',
    @FileTitle nvarchar(max) = '',
    @FileDescription nvarchar(max) = ''
)
AS
BEGIN
    -- This procedure is used by integration
	IF ISNULL(@database,'')=''
	BEGIN
		SELECT @database = DB_NAME()
	END

	declare @jobid as int, @OwnerID int, @PROP_ZipFile nvarchar(32) = '', @UT_Graduate nvarchar(32) = ''

	 --Add logic get language from department base on hdid  
	if ISNULL(@LanguageID, 0) = 0 
	begin
		SELECT @LanguageID=d.LanguageID from org.H_D h
		JOIN org.Department d ON d.DepartmentID=h.DepartmentID
		WHERE h.HDID=@hdid_school
	end

    SELECT @OwnerID = [OwnerID] FROM [app].[Site] WHERE [SiteID] = @SiteID
    SELECT @PROP_ZipFile = [Value] FROM [app].[SiteParameter] WHERE [SiteID] = @SiteID AND [Key] = 'Platform.NewSchoolYear.ZipProp'
    SELECT @UT_Graduate = [Value] FROM [app].[SiteParameter] WHERE [SiteID] = @SiteID AND [Key] = 'Platform.Admin.GraduatesUserType'
	--creates job with temporary status
	insert into job.Job (JobTypeID, JobStatusID, OwnerID, UserID, Name, Priority, [Option], EndDate, Description)
	select 24, 3, 9, @userid, 'Export results for HDID:' + CAST(@hdid_school as nvarchar(10)), 0, 0, NULL, 'Engage - MANUELT GENERERT jobb for produsering av zip-fil med resultpdfs for avgangselever ifbm. nytt skoleår'

	set @jobid = SCOPE_IDENTITY()

	insert into job.jobParameter (JobID, [No], Name, Value, [Type])
	select @jobid, 1, 'Server',@server , 0

	insert into job.jobParameter (JobID, [No], Name, Value, [Type])
	select @jobid, 2, 'Database', @database, 0

	insert into job.jobParameter (JobID, [No], Name, Value, [Type])
	select @jobid, 3, 'Username', @username, 0

	insert into job.jobParameter (JobID, [No], Name, Value, [Type])
	select @jobid, 4, 'Password', @password, 0

	insert into job.jobParameter (JobID, [No], Name, Value, [Type])
	select @jobid, 5, 'OwnerID', CAST(@OwnerID as nvarchar(32)), 0

	insert into job.jobParameter (JobID, [No], Name, Value, [Type])
	select @jobid, 6, 'LanguageID', CAST(@LanguageID as nvarchar(32)), 0

	insert into job.jobParameter (JobID, [No], Name, Value, [Type])
	select @jobid, 7, 'HDID', CAST(@hdid_school as nvarchar(max)), 0

    IF @FileName = '' SET @FileName = N'Avgangselever.zip'
	insert into job.jobParameter (JobID, [No], Name, Value, [Type])
	select @jobid, 8, 'FileName', @FileName, 0

    IF @FileTitle = '' SET @FileTitle = N'Avgangselever'
	insert into job.jobParameter (JobID, [No], Name, Value, [Type])
	select @jobid, 9, 'FileTitle', @FileTitle, 0

    IF @FileDescription = '' SET @FileDescription = N'Samling av alle kartlegginger og vurderinger registrert for avgangselevene'
	insert into job.jobParameter (JobID, [No], Name, Value, [Type])
	select @jobid, 10, 'Description', @FileDescription, 0

	insert into job.jobParameter (JobID, [No], Name, Value, [Type])
	select @jobid, 11, 'PropertyId', @PROP_ZipFile, 0

	insert into job.jobParameter (JobID, [No], Name, Value, [Type])
	select @jobid, 12, 'UserTypeID', @UT_Graduate, 0

	insert into job.jobParameter (JobID, [No], Name, Value, [Type])
	select @jobid, 13, 'PropIdBirthDay', '8', 0

	insert into job.jobParameter (JobID, [No], Name, Value, [Type])
	select @jobid, 14, 'GraduatedUserIds', @userids, 0

	--update job with ready-for-pickup status
	update job.job set jobstatusid = 0 where jobid = @jobid
END

GO
