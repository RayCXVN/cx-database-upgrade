SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[at].[prc_A_ChartType_ins]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [at].[prc_A_ChartType_ins] AS' 
END
GO

ALTER PROCEDURE [at].[prc_A_ChartType_ins]
(
	@AChartTypeID int = null output,
	@ReportChartTypeID int,
	@ActivityID int,
	@No smallint,
	@DefaultSelected smallint,
	@cUserid int,
	@Log smallint = 1
)
AS
BEGIN
	SET NOCOUNT ON
	DECLARE @Err Int

	INSERT INTO [at].[A_ChartType]
	(
		[ReportChartTypeID],
		[ActivityID],
		[No],
		[DefaultSelected]
	)
	VALUES
	(
		@ReportChartTypeID,
		@ActivityID,
		@No,
		@DefaultSelected
	)

	Set @Err = @@Error
	Set @AChartTypeID = scope_identity()

	IF @Log = 1 
	BEGIN 
		INSERT INTO [Log].[AuditLog] ( UserId, TableName, Type, Data, Created) 
		SELECT @cUserid,'A_ChartType',0,
		( SELECT * FROM [at].[A_ChartType] 
			WHERE
			[AChartTypeID] = @AChartTypeID				 FOR XML AUTO) as data,
				getdate() 
	 END

	RETURN @Err
END


GO
