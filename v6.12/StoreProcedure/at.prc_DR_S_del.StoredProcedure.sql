SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[at].[prc_DR_S_del]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [at].[prc_DR_S_del] AS' 
END
GO

ALTER PROCEDURE [at].[prc_DR_S_del]
(
	@DottedRuleID int,
	@ScaleID int,
	@cUserid int,
	@Log smallint = 1
)
AS
BEGIN
	SET NOCOUNT ON
	DECLARE @Err Int

	IF @Log = 1 
	BEGIN 
		INSERT INTO [Log].[AuditLog] ( UserId, TableName, Type, Data, Created) 
		SELECT @cUserid,'DR_S',2,
		( SELECT * FROM [at].[DR_S] 
			WHERE
			[DottedRuleID] = @DottedRuleID AND
			[ScaleID] = @ScaleID
			 FOR XML AUTO) as data,
			getdate() 
	END 


	DELETE FROM [at].[DR_S]
	WHERE
		[DottedRuleID] = @DottedRuleID AND
		[ScaleID] = @ScaleID

	Set @Err = @@Error

	RETURN @Err
END


GO
