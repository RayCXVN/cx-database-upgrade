SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[prc_Batch_Invited_Finished_get]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [dbo].[prc_Batch_Invited_Finished_get] AS' 
END
GO
ALTER proc [dbo].[prc_Batch_Invited_Finished_get]
(
	@SurveyID	INT
)
AS
BEGIN
	SET NOCOUNT ON
	DECLARE @Err INT
    DECLARE @ActiveEntityStatusID INT = (SELECT EntityStatusID FROM EntityStatus WHERE CodeName='Active')

	SELECT
	    batchid,
	    COUNT(*) AS invited,
        COUNT(enddate) AS finished
	FROM [Result]
	WHERE [SurveyID] = @SurveyID
        AND EntityStatusID = @ActiveEntityStatusID AND Deleted IS NULL
    GROUP BY batchid

	SET @Err = @@Error

	RETURN @Err
END
---------------------------------------------------------------------


GO
