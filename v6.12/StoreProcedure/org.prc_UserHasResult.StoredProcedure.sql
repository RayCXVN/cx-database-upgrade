SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[org].[prc_UserHasResult]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [org].[prc_UserHasResult] AS' 
END
GO
/*
	exec [org].[prc_UserHasResult] 15,'143,146,147,149,150,151,152,80',1
	Johnny - Des 07 2016 - Update store procedure for dynamic SQL
*/
ALTER PROC [org].[prc_UserHasResult]
(
  @OwnerID		    int,
  @UserList		    varchar(max),
  @ExcludeStatusTypeID  int
)
AS
    SET NOCOUNT ON
    DECLARE @sqlCommand		  nvarchar(max) = N'',
		  @Parameters		  nvarchar(max) = N'@p_UserID int,@p_StatusTypeID int,@p_HasResult bit OUTPUT',
		  @UserID			  int,
		  @SurveyID		  int,
		  @ReportServer	  nvarchar(64),
		  @ReportDB		  nvarchar(64),
		  @HasResult		  bit
	DECLARE @LinkedDB NVARCHAR(MAX)=' '	  

    DECLARE c_ReportDB CURSOR SCROLL READ_ONLY FOR
    SELECT  DISTINCT s.ReportServer, s.ReportDB 
    FROM	  at.Survey s JOIN at.Activity a ON a.ActivityID = s.ActivityID AND a.OwnerID = @OwnerID
    
	IF @ReportDB=DB_NAME() AND @ReportServer=@@servername
		BEGIN
		SET @LinkedDB=' '
		END
	ELSE
		BEGIN
		SET @LinkedDB= '['+ @ReportServer + '].['+ @ReportDB + '].'
		END
    DECLARE c_User CURSOR READ_ONLY FOR
    SELECT ParseValue FROM dbo.StringToArray(@UserList,',')
    
    CREATE TABLE #UserResult (UserID int, HasResult bit)
    
    OPEN c_ReportDB
    OPEN c_User
	   FETCH NEXT FROM c_User INTO @UserID
	   WHILE @@FETCH_STATUS=0
	   BEGIN
		  SET @HasResult = 0
		  FETCH FIRST FROM c_ReportDB INTO @ReportServer, @ReportDB
		  WHILE @@FETCH_STATUS=0
		  BEGIN
			 SET @sqlCommand = N'SELECT @p_HasResult = 1
							 WHERE EXISTS (SELECT 1
										FROM '+ @LinkedDB +'dbo.Result
										WHERE UserID = @p_UserID AND StatusTypeID != @p_StatusTypeID)'
			 EXECUTE sp_executesql @sqlCommand, @Parameters, @p_UserID = @UserID, @p_StatusTypeID = @ExcludeStatusTypeID, @p_HasResult = @HasResult OUTPUT
			 IF ISNULL(@HasResult,0) = 1 BREAK
    		  
			 FETCH NEXT FROM c_ReportDB INTO @ReportServer, @ReportDB
		  END
			 
		  INSERT INTO #UserResult (UserID, HasResult)
		  VALUES (@UserID, ISNULL(@HasResult,0))
		  
		  FETCH NEXT FROM c_User INTO @UserID
	   END
    CLOSE c_User
    DEALLOCATE c_User
    CLOSE c_ReportDB
    DEALLOCATE c_ReportDB

    SELECT * FROM #UserResult  

    DROP TABLE #UserResult

GO
