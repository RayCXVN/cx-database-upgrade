SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[rep].[prc_LT_ReportColumn_upd]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [rep].[prc_LT_ReportColumn_upd] AS' 
END
GO

ALTER PROCEDURE [rep].[prc_LT_ReportColumn_upd]
(
	@LanguageID int,
	@ReportColumnID int,
	@Text nvarchar(max),
	@ToolTip nvarchar(512) = '',
	@cUserid int,
	@Log smallint = 1
)
AS
BEGIN
	SET NOCOUNT ON
	DECLARE @Err Int

	UPDATE [rep].[LT_ReportColumn]
	SET
		[LanguageID] = @LanguageID,
		[ReportColumnID] = @ReportColumnID,
		[Text] = @Text,
		[ToolTip] = @ToolTip
	WHERE
		[LanguageID] = @LanguageID AND
		[ReportColumnID] = @ReportColumnID

	IF @Log = 1 
	BEGIN 
		INSERT INTO [Log].[AuditLog] ( UserId, TableName, Type, Data, Created) 
		SELECT @cUserid,'LT_ReportColumn',1,
		( SELECT * FROM [rep].[LT_ReportColumn] 
			WHERE
			[LanguageID] = @LanguageID AND
			[ReportColumnID] = @ReportColumnID			 FOR XML AUTO) as data,
			getdate()
	END

	Set @Err = @@Error

	RETURN @Err
END



GO
