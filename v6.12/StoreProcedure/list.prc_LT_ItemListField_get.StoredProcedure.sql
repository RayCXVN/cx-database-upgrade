SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[list].[prc_LT_ItemListField_get]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [list].[prc_LT_ItemListField_get] AS' 
END
GO
ALTER PROCEDURE [list].[prc_LT_ItemListField_get]
	@ItemListFieldID int
AS
BEGIN
	SET NOCOUNT ON;
	DECLARE @Err Int
	
    SELECT 
		[LanguageID],
		[ItemListFieldID],
		[Name],
		[Description],
		[HeaderText],
		[HeaderGroupText],
		[HeaderTooltipText],
		[ItemTooltipText],
		[ItemText],
		[ExpandingItemText],
		[DefaultValues],
		[AvailableValues]
	FROM [list].[LT_ItemListField]
	WHERE [ItemListFieldID] = @ItemListFieldID
	
	Set @Err = @@Error

	RETURN @Err
  
END

GO
