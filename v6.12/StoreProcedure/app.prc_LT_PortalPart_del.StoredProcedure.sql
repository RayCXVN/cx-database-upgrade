SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[app].[prc_LT_PortalPart_del]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [app].[prc_LT_PortalPart_del] AS' 
END
GO
ALTER PROCEDURE [app].[prc_LT_PortalPart_del]
(
	@LanguageID int,
	@PortalPartID int,
	@cUserid int,
	@Log smallint = 1
)
AS
BEGIN
	SET NOCOUNT ON
	DECLARE @Err Int

	IF @Log = 1 
	BEGIN 
		INSERT INTO [Log].[AuditLog] ( UserId, TableName, Type, Data, Created) 
		SELECT @cUserid,'LT_PortalPart',2,
		( SELECT * FROM [app].[LT_PortalPart] 
			WHERE
			[LanguageID] = @LanguageID AND
			[PortalPartID] = @PortalPartID
			 FOR XML AUTO) as data,
			getdate() 
	END 


	DELETE FROM [app].[LT_PortalPart]
	WHERE
		[LanguageID] = @LanguageID AND
		[PortalPartID] = @PortalPartID

	Set @Err = @@Error

	RETURN @Err
END


GO
