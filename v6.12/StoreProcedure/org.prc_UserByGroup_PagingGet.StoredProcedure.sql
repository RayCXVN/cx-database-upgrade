SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[org].[prc_UserByGroup_PagingGet]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [org].[prc_UserByGroup_PagingGet] AS' 
END
GO
/*  2018-07-12 Ray:     Replace UG_U by UGMember
*/
ALTER PROCEDURE [org].[prc_UserByGroup_PagingGet](
    @UserGroupID  int,
    @RowCount     int OUTPUT,
    @RowIndex     int,
    @PageSize     int,
    @DenyUTIDList nvarchar(max) = '',
    @UTIDList     nvarchar(max) = ''
) AS
BEGIN
    SET NOCOUNT ON;
    DECLARE @Err int, @ToRow int, @EntityStatusID int= 0;

    SELECT value AS [UserTypeID] INTO [#DenyUTtable] FROM [dbo].[funcListToTableInt] (@DenyUTIDList, ',');
    SELECT value AS [UserTypeID] INTO [#UTTable] FROM [dbo].[funcListToTableInt] (@UTIDList, ',');

    SELECT @EntityStatusID = [EntityStatusID] FROM [dbo].[EntityStatus] WHERE [CodeName] = N'Active';

    SELECT @RowCount = COUNT([ugm].[UserID])
    FROM [org].[UGMember] [ugm]
    JOIN [org].[User] [u] ON [ugm].[UserID] = [u].[UserID] AND ugm.[UserGroupID] = @UserGroupID
     AND [u].[EntityStatusID] = @EntityStatusID AND [u].[Deleted] IS NULL
     AND ugm.[EntityStatusID] = @EntityStatusID AND ugm.[Deleted] IS NULL
    WHERE (@UTIDList = ''
           OR EXISTS (SELECT 1 FROM [org].[UT_U] [utu2] JOIN [#UTTable] ut ON utu2.[UserTypeID] = [utu2].[UserTypeID] AND [utu2].[UserID] = [ugm].[UserID])
          )
      AND (@DenyUTIDList = ''
           OR NOT EXISTS (SELECT 1 FROM [org].[UT_U] [utu] JOIN [#DenyUTtable] dut ON dut.[UserTypeID] = [utu].[UserTypeID] AND [utu].[UserID] = [ugm].[UserID])
          );

    IF @RowIndex > @RowCount
    BEGIN
        RETURN;
    END;

    SET @ToRow = @RowIndex + @PageSize - 1;
    IF @ToRow > @RowCount
    BEGIN
        SET @ToRow = @RowCount;
    END;

    SELECT [v].[UserGroupID], [v].[UserID], [v].[DepartmentID], [v].[FirstName], [v].[LastName]
    FROM (SELECT ROW_NUMBER() OVER(ORDER BY [u].[FirstName], [u].[LastName]) AS [RowNum],
                 [ugm].[UserGroupID], [ugm].[UserID], [u].[DepartmentID], [u].[FirstName], [u].[LastName]
          FROM [org].[UGMember] [ugm]
          JOIN [org].[User] [u] ON [ugm].[UserID] = [u].[UserID] AND ugm.[UserGroupID] = @UserGroupID
           AND [u].[EntityStatusID] = @EntityStatusID AND [u].[Deleted] IS NULL
           AND ugm.[EntityStatusID] = @EntityStatusID AND ugm.[Deleted] IS NULL
         WHERE (@UTIDList = ''
                OR EXISTS (SELECT 1 FROM [org].[UT_U] [utu2] JOIN [#UTTable] ut ON utu2.[UserTypeID] = [utu2].[UserTypeID] AND [utu2].[UserID] = [ugm].[UserID])
               )
           AND (@DenyUTIDList = ''
                OR NOT EXISTS (SELECT 1 FROM [org].[UT_U] [utu] JOIN [#DenyUTtable] dut ON dut.[UserTypeID] = [utu].[UserTypeID] AND [utu].[UserID] = [ugm].[UserID])
               )
         ) AS [v]
    WHERE [v].[RowNum] BETWEEN @RowIndex AND @ToRow;

    DROP TABLE [#DenyUTtable];
    DROP TABLE [#UTtable];

    SET @Err = @@Error;

    RETURN @Err;
END;
GO