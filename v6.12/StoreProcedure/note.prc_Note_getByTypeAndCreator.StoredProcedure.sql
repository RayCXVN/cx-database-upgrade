SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[note].[prc_Note_getByTypeAndCreator]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [note].[prc_Note_getByTypeAndCreator] AS' 
END
GO
/*
2017-06-15 Ray:     Fix missing note accesses
*/
-- EXEC [note].[prc_Note_getByTypeAndCreator] 3,1,2, '1018556'
ALTER PROCEDURE [note].[prc_Note_getByTypeAndCreator]
(
    @NoteTypeID	        int,
    @LanguageID         int,
    @FallbackLanguageID int,
    @UserIDList         varchar(max) = '',
    @GetDeniedNote      bit = 0
)
AS
BEGIN
    SET NOCOUNT ON
    DECLARE @DenyPermission int = 2, @HD_TT int, @UserType_TT int, @DepartmentType_TT int
    DECLARE @NoteAccess AS TABLE (NoteID int, NoteAccessID int, TableTypeID int, ItemID nvarchar(max), AccessRight int, Name nvarchar(max))
    DECLARE @UserIDTable AS TABLE (UserID int)
    DECLARE @Note AS TABLE ([NoteID] int, [CreatedBy] int, [CreatedByName] nvarchar(max))

    SELECT @HD_TT       = TableTypeID FROM TableType WHERE Name='H_D'
    SELECT @UserType_TT = TableTypeID FROM TableType WHERE Name='UserType'
    SELECT @DepartmentType_TT = TableTypeID FROM TableType WHERE Name='DepartmentType'

    INSERT INTO @UserIDTable (UserID) SELECT Value FROM dbo.funcListToTableInt(@UserIDList, ',')
    
    INSERT INTO @Note ([NoteID], [CreatedBy], [CreatedByName])
    SELECT n.NoteID, n.[CreatedBy], (u.FirstName + ' ' + u.LastName) AS CreatedByName
    FROM note.Note n WITH (NOLOCK)
    JOIN [org].[User] u WITH (NOLOCK) ON n.NoteTypeID = @NoteTypeID AND n.Deleted = 0 AND n.[CreatedBy] = u.[UserID]
     AND (n.[CreatedBy] IN (SELECT UserID FROM @UserIDTable) OR ISNULL(@UserIDList,'') = '')

    -- Get all accesses granted
    INSERT INTO @NoteAccess ([NoteID], [NoteAccessID], [TableTypeID], [ItemID], [AccessRight])
    SELECT n.NoteID, na.NoteAccessID, na.TableTypeID, na.ItemID, na.[Type] AccessRight
    FROM note.NoteAccess na WITH (NOLOCK)
    JOIN @Note n ON na.[NoteID] = n.[NoteID] AND na.[Type] != @DenyPermission
 
    -- Get all combination criteria
    INSERT INTO @NoteAccess ([NoteID], [NoteAccessID], [TableTypeID], [ItemID], [AccessRight])
    SELECT na.[NoteID], na.[NoteAccessID], cna.[TableTypeID], cna.[ItemID], na.[AccessRight]
    FROM @NoteAccess na JOIN [note].[Combine_NoteAccess] cna WITH (NOLOCK) ON na.[NoteAccessID] = cna.[NoteAccessID]

    UPDATE na SET na.[Name] = lt.[Name] FROM @NoteAccess na JOIN [org].[LT_UserType] lt WITH (NOLOCK) ON na.[TableTypeID] = @UserType_TT AND lt.[UserTypeID] = na.[ItemID] AND lt.[LanguageID] = @LanguageID
    UPDATE na SET na.[Name] = lt.[Name] FROM @NoteAccess na JOIN [org].[LT_DepartmentType] lt WITH (NOLOCK) ON na.[TableTypeID] = @DepartmentType_TT AND lt.[DepartmentTypeID] = na.[ItemID] AND lt.[LanguageID] = @LanguageID
    UPDATE na SET na.[Name] = d.[Name]  FROM @NoteAccess na JOIN [org].[H_D] hd WITH (NOLOCK) ON na.[TableTypeID] = @HD_TT AND hd.[HDID] = na.[ItemID]
                                                            JOIN [org].[Department] d WITH (NOLOCK) ON hd.[DepartmentID] = d.[DepartmentID]

    SELECT n.NoteID, ISNULL(ltn.[Subject],fblt.[Subject]) [Subject], ISNULL(ltn.Note,fblt.Note) [Note], nt.[Active],
           n.[CreatedByName], nt.[Created], nt.[StartDate], nt.[EndDate], nt.[NotifyAtNextLogin], nac.[TableTypeID], nac.[Receiver]
    FROM (SELECT [na].[NoteID], MIN(na.[TableTypeID]) AS [TableTypeID],
                 STUFF((SELECT '-' + ac.[Name] AS [text()] FROM @NoteAccess ac WHERE ac.[NoteAccessID] = na.[NoteAccessID]
                       FOR XML PATH ('')),
                       1, 1, '') AS [Receiver]
          FROM @NoteAccess na
          GROUP BY na.[NoteID], na.[NoteAccessID]) nac
    JOIN @Note n ON nac.[NoteID] = n.[NoteID]
    JOIN [note].[Note] nt WITH (NOLOCK) ON nt.[NoteID] = n.[NoteID]
    LEFT JOIN note.LT_Note ltn WITH (NOLOCK) ON n.NoteID = ltn.NoteID AND ltn.LanguageID = @LanguageID  
	LEFT JOIN note.LT_Note fblt WITH (NOLOCK) ON n.NoteID = fblt.NoteID AND fblt.LanguageID = @FallbackLanguageID
END


GO
