SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[org].[prc_Hierarchy_ins]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [org].[prc_Hierarchy_ins] AS' 
END
GO

ALTER PROCEDURE [org].[prc_Hierarchy_ins]
(
	@HierarchyID int = null output,
	@OwnerID int,
	@Name varchar(256),
	@Description nvarchar(max),
	@Type smallint,
	@Deleted bit,
	@cUserid int,
	@Log smallint = 1
)
AS
BEGIN
	SET NOCOUNT ON
	DECLARE @Err Int

	INSERT INTO [org].[Hierarchy]
	(
		[OwnerID],
		[Name],
		[Description],
		[Type],
		[Deleted]
	)
	VALUES
	(
		@OwnerID,
		@Name,
		@Description,
		@Type,
		@Deleted
	)

	Set @Err = @@Error
	Set @HierarchyID = scope_identity()

	IF @Log = 1 
	BEGIN 
		INSERT INTO [Log].[AuditLog] ( UserId, TableName, Type, Data, Created) 
		SELECT @cUserid,'Hierarchy',0,
		( SELECT * FROM [org].[Hierarchy] 
			WHERE
			[HierarchyID] = @HierarchyID				 FOR XML AUTO) as data,
				getdate() 
	 END

	RETURN @Err
END


GO
