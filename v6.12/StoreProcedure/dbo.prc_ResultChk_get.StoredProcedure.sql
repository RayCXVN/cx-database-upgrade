SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[prc_ResultChk_get]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [dbo].[prc_ResultChk_get] AS' 
END
GO
/* ------------------------------------------------------------
   PROCEDURE:    prc_Result_get

   Description:  Selects records from the table 'prc_Result_get'

   AUTHOR:       LockwoodTech 11.07.2006 11:47:11
   ------------------------------------------------------------ */
ALTER PROCEDURE [dbo].[prc_ResultChk_get]
(
	@ResultID		bigint,
	@Chk			bigint output
)
As
BEGIN
	SET NOCOUNT ON
	DECLARE @Err Int

	Select
	@Chk = Cast([chk] As bigint)
	FROM [Result]
	WHERE [ResultID] = @ResultID
	
	Set @Err = @@Error

	RETURN @Err
End


GO
