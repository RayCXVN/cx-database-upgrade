SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[at].[prc_LT_Action_upd]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [at].[prc_LT_Action_upd] AS' 
END
GO

ALTER PROCEDURE [at].[prc_LT_Action_upd]
(
	@LanguageID int,
	@ActionID int,
	@Response nvarchar(max)=NULL,
	@cUserid int,
	@Log smallint = 1
)
AS
BEGIN
	SET NOCOUNT ON
	DECLARE @Err Int

	UPDATE [at].[LT_Action]
	SET
		[LanguageID] = @LanguageID,
		[ActionID] = @ActionID,
		[Response] = @Response
	WHERE
		[LanguageID] = @LanguageID AND
		[ActionID] = @ActionID

	IF @Log = 1 
	BEGIN 
		INSERT INTO [Log].[AuditLog] ( UserId, TableName, Type, Data, Created) 
		SELECT @cUserid,'LT_Action',1,
		( SELECT * FROM [at].[LT_Action] 
			WHERE
			[LanguageID] = @LanguageID AND
			[ActionID] = @ActionID			 FOR XML AUTO) as data,
			getdate()
	END

	Set @Err = @@Error

	RETURN @Err
END


GO
