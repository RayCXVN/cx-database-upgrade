SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[res].[prc_LT_Resource_ins]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [res].[prc_LT_Resource_ins] AS' 
END
GO
ALTER PROCEDURE [res].[prc_LT_Resource_ins]
(
	@ResourceID int,
	@LanguageID int,
	@Name nvarchar(max),
	@Description nvarchar(max),
	@URL nvarchar(max),
	@Keyword nvarchar(512),
	@NoteHeading nvarchar(max),
	@CompletedCheckText nvarchar(max),
	@CompletedInfoText nvarchar(max),
    @Tooltip nvarchar(max) = '',
	@cUserid int,
	@Log smallint = 1
)
AS  
BEGIN  
	SET NOCOUNT ON  
	DECLARE @Err Int  
  
	INSERT INTO res.LT_Resource
	(
		[LanguageID],
		[ResourceID],
		[Name],
		[Description],
		[URL],
		[Keyword],
		[NoteHeading],
		[CompletedCheckText],
		[CompletedInfoText],
        [Tooltip]
	)
	VALUES
	(
		@LanguageID,
		@ResourceID,
		@Name,
		@Description,
		@URL,
		@Keyword,
		@NoteHeading,
		@CompletedCheckText,
		@CompletedInfoText,
        @Tooltip
	)
  
	Set @Err = @@Error  
	IF @Log = 1 
	BEGIN 
		INSERT INTO [Log].[AuditLog] ( UserId, TableName, Type, Data, Created) 
		SELECT @cUserid,'res.LT_Resource',0,
		( SELECT * FROM [res].[LT_Resource] 
			WHERE
			[ResourceID] = @ResourceID AND [LanguageID] = @LanguageID	FOR XML AUTO) as data,
				getdate() 
	END

	RETURN @Err
END 

GO
