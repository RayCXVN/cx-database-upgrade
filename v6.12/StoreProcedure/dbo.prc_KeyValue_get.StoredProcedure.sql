SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[prc_KeyValue_get]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [dbo].[prc_KeyValue_get] AS' 
END
GO

ALTER PROCEDURE [dbo].[prc_KeyValue_get]
AS
BEGIN
	SET NOCOUNT ON
	DECLARE @Err Int

	SELECT
	[Id],
	[Type],
	[Key],
	[Value]
	FROM [dbo].[KeyValue]

	Set @Err = @@Error

	RETURN @Err
END


GO
