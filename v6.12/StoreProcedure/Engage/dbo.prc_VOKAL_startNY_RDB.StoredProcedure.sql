SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT 1 FROM sys.objects WHERE name = N'prc_VOKAL_startNY_RDB' AND type IN ( N'P', N'PC' )  AND SCHEMA_NAME(schema_id) = 'dbo')
EXEC ('CREATE PROC [dbo].[prc_VOKAL_startNY_RDB] AS SELECT 1')
GO
/*  2016-10-10 Ray:     Remove column Status from org.User, org.Department, RDB.dbo.Result
    2017-08-11 Ray:     Copy missing columns from Result and Answer: StartDate, EndDate, CustomerID,...
    2018-07-06 Ray:     Replace UG_U by UGMember
    2018-07-23 Ray:     Remove hint as it is not allowed with columnstored index
    2018-11-07 Ray:     Do not copy StartDate and EndDate to new survey, rewrite for merged QR
                        Filter active batch for copy target
    2019-04-08 Ray:     Copy StartDate, Created, CreatedBy to new survey
    2019-06-13 Ray:     Filter UserGroup by archetype "ArchivedClass"
*/
ALTER PROCEDURE [dbo].[prc_VOKAL_startNY_RDB]
(
    @HDID           int,
    @PeriodID       int,
    @SurveyID       int,
    @BatchID        int,
    @NewSurveyId    int, -- oppgis kun ved kartleggingsverktøy, da resultatet kopieres til denne survey
    @RTID           int,
    @StatusTypeID   int
) --WITH RECOMPILE
AS
BEGIN
    DECLARE @ActiveEntityStatusID INT = (SELECT EntityStatusID FROM EntityStatus WHERE CodeName='Active')
    DECLARE @DepartmentID int, @ActivityID int, @NewBatchID int, @ArchetypeID_Class int,
            @RowCount int, @TRANS bit, @FromID int, @ToID int
    
    SELECT @ArchetypeID_Class = ArchetypeID FROM dbo.Archetype WHERE CodeName = N'ArchivedClass';
    -- hent skolens departmentid        
    SELECT @DepartmentID = DepartmentID FROM H_D WHERE HDID = @HDID
    -- hent survey's activityid
    SELECT @ActivityID = ActivityID FROM Survey WHERE SurveyID = @SurveyID

    SET @TRANS = 0
    SELECT @TRANS = 1 FROM TRANS_Question WHERE ActivityID = @ActivityID
        
    print 'Archive results'
    -- flytter alle skolens resultater opp på skolen, og knytter det til UserGroup som er snapshot av klassen ved gammel periodes slutt
    UPDATE Result
    SET UserGroupID = UG.UserGroupID, DepartmentID = @DepartmentID
    FROM Result R    
    JOIN UserGroup UG ON UG.DepartmentID = @DepartmentID AND UG.UserGroupTypeID IS NULL AND UG.PeriodID = @PeriodID AND UG.[ArchetypeID] = @ArchetypeID_Class
     AND R.BatchID = @BatchID AND R.EntityStatusID = @ActiveEntityStatusID AND R.Deleted IS NULL
    JOIN [UGMember] ugm ON R.UserID = ugm.UserID AND UG.UsergroupID = ugm.UsergroupID AND ugm.[EntityStatusID] = @ActiveEntityStatusID AND ugm.[Deleted] IS NULL
            
    SELECT @RowCount = @@ROWCOUNT 
    
    -- @NewSurveyID oppgis bare ved kartleggingsverktøy
    IF @NewSurveyID > 0 AND @RowCount > 0
    BEGIN
        -- finner batchid for ny survey
        SELECT TOP (1) @NewBatchID = BatchID FROM Batch WITH (NOLOCK)
        WHERE SurveyID = @NewSurveyID AND DepartmentID = @DepartmentID AND [Status] = 2
        ORDER BY BatchID
        
        print 'Copy results into new batch'
        -- oppretter kopi av resultat inn i ny survey og batch, og tar vare på gammel resultatid i e-mail feltet
        INSERT Result(RoleID, UserID, UserGroupID, SurveyID, BatchID, LanguageID, PageNo, DepartmentID, [Anonymous], ShowBack, ResultKey ,
                      LastUpdated, LastUpdatedBy, StatusTypeID, [ParentResultID], [ParentResultSurveyID], EntityStatusID, Deleted, [EntityStatusReasonID],
                      [CustomerID], [StartDate], [Created], [CreatedBy])
        SELECT RM.RoleID, U.UserID, 0 AS [UserGroupID], @NewSurveyID, B.BatchID, R.LanguageID, R.PageNo, U.DepartmentID, R.[Anonymous], R.ShowBack, CAST(R.ResultID As varchar(16)) AS [ResultKey],
               R.LastUpdated, R.LastUpdatedBy, @StatusTypeID, R.ResultID AS [ParentResultID], R.SurveyID AS [ParentResultSurveyID], R.EntityStatusID, R.Deleted, R.[EntityStatusReasonID],
               R.[CustomerID], r.[StartDate], r.[Created], r.[CreatedBy]
        FROM Result R WITH (NOLOCK)
        JOIN Batch BO WITH (NOLOCK) ON R.BatchID = BO.BatchID AND R.BatchID = @BatchID AND R.StatusTypeID IS NOT NULL AND R.EntityStatusID = @ActiveEntityStatusID AND R.Deleted IS NULL
        JOIN Batch B WITH (NOLOCK) ON BO.DepartmentID = B.DepartmentID AND B.SurveyID = @NewSurveyID
        JOIN [User] U WITH (NOLOCK) ON R.UserID = U.UserID
        JOIN UT_U UT WITH (NOLOCK) ON U.UserID = UT.UserID 
        JOIN ObjectMapping OM WITH (NOLOCK) ON OM.RelationTypeID = @RTID AND OM.ToID = UT.UserTypeID
        JOIN RoleMapping RM WITH (NOLOCK) ON RM.ActivityID = @ActivityID AND RM.UserTypeId = UT.UserTypeID
         
        IF @TRANS = 0
        BEGIN                
            INSERT Answer(ResultID, QuestionID, AlternativeID, Value, DateValue, Free, [No],
                          [ItemID], [CustomerID], [LastUpdated], [LastUpdatedBy], [Created], [CreatedBy]) 
            SELECT RN.ResultID, A.QuestionID, A.AlternativeID, A.Value, A.DateValue, A.Free, A.[No],
                   A.[ItemID], A.[CustomerID], A.[LastUpdated], A.[LastUpdatedBy], A.[Created], A.[CreatedBy]
            FROM Answer A WITH (NOLOCK)
            JOIN Result R WITH (NOLOCK) ON A.ResultID = R.ResultID AND R.BatchID = @BatchID
            JOIN Result RN WITH (NOLOCK) ON RN.BatchID = @NewBatchID AND ISNUMERIC(RN.ResultKey) = 1 AND CAST(RN.ResultKey As bigint) = R.ResultID
        END
        ELSE
        BEGIN
            /*
                In LUS only a few answers should be copied and some of these answers should be copied to different questions.
                I have created two tables, dbo.LUS_Question and dbo.Alternative, with FromID and ToID, to transfer the answers.
                The "Type" column in dbo.LUS_Question is used as follows:
                    0 - Copies answers to the same question
                    1 - Copies answers to a different question
                    2 - Copy the first answer sortert by "No" to a different question            
            */
                
            -- This copies "Gender" and "SNO" questions.
            INSERT Answer(ResultID, QuestionID, AlternativeID, Value, DateValue, Free, [No]) 
            SELECT RN.ResultID, A.QuestionID, A.AlternativeID, A.Value, A.DateValue, A.Free, A.[No]
            FROM Answer A WITH (NOLOCK)
            JOIN dbo.TRANS_Question LQ WITH (NOLOCK) ON A.QuestionID = LQ.FromID AND LQ.[Type] = 0
            JOIN Result R WITH (NOLOCK) ON A.ResultID = R.ResultID AND R.BatchID = @BatchID
            JOIN Result RN WITH (NOLOCK) ON CAST(RN.ResultKey As bigint) = R.ResultID        
                AND RN.BatchID = @NewBatchID AND ISNUMERIC(RN.ResultKey) = 1
                
            -- This copies from question "Første, andre eller tredje" to "Siste kartlegging forrige skoleår ble utført:"
            INSERT Answer(ResultID, QuestionID, AlternativeID, Value, DateValue, Free, [No]) 
            SELECT RN.ResultID, LQ.ToID, LA.ToID, ALT.Value, A.DateValue, A.Free, A.[No]
            FROM Answer A WITH (NOLOCK)
            JOIN dbo.TRANS_Question LQ WITH (NOLOCK) ON A.QuestionID = LQ.FromID AND LQ.[Type] = 1
            JOIN dbo.TRANS_Alternative LA WITH (NOLOCK) ON A.AlternativeID = LA.FromID
            JOIN Alternative ALT WITH (NOLOCK) ON LA.ToID = ALT.AlternativeID
            JOIN Result R WITH (NOLOCK) ON A.ResultID = R.ResultID
            JOIN Result RN WITH (NOLOCK) ON CAST(RN.ResultKey As bigint) = R.ResultID        
            WHERE R.BatchID = @BatchID AND RN.BatchID = @NewBatchID AND ISNUMERIC(RN.ResultKey) = 1
                
            DECLARE curB CURSOR LOCAL FAST_FORWARD READ_ONLY FOR
            SELECT ToID, FromID
            FROM dbo.TRANS_Question WITH (NOLOCK)
            WHERE ActivityID = @ActivityID AND [Type] = 2
            ORDER By [No]

            OPEN curB
            FETCH NEXT FROM curB INTO @ToID, @FromID
            WHILE (@@FETCH_STATUS = 0)
            BEGIN
                -- This copies from last evaluation (Kartlegging 1,2 or 3) to "Siste kartlegging forrige skoleår:"
                INSERT Answer(ResultID, QuestionID, AlternativeID, Value, DateValue, Free, [No]) 
                SELECT RN.ResultID, @ToID, LA.ToID, ALT.Value, A.DateValue, A.Free, A.[No]
                FROM Answer A WITH (NOLOCK)                       
                JOIN dbo.TRANS_Alternative LA WITH (NOLOCK) ON A.AlternativeID = LA.FromID AND A.QuestionID = @FromID
                JOIN Alternative ALT WITH (NOLOCK) ON LA.ToID = ALT.AlternativeID
                JOIN Result R WITH (NOLOCK) ON A.ResultID = R.ResultID AND R.BatchID = @BatchID
                JOIN Result RN WITH (NOLOCK) ON CAST(RN.ResultKey  As int) = R.ResultID AND RN.BatchID = @NewBatchID     
                WHERE ISNUMERIC(RN.ResultKey) = 1
                  AND RN.ResultID NOT IN (SELECT ResultID FROM Answer WHERE QuestionID = @ToID)
                FETCH NEXT FROM curB INTO @ToID, @FromID
            END
            CLOSE curB
            DEALLOCATE curB
        END

        print 'Clear ResultKey'
        -- nullstiller result feltet på nytt resultat
        UPDATE Result
        SET ResultKey  = ''
        FROM Result R         
        WHERE R.BatchID = @NewBatchID
    END
END
GO