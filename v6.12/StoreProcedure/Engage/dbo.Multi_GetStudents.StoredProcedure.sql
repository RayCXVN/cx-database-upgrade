SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[Multi_GetStudents]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [dbo].[Multi_GetStudents] AS' 
END
GO
ALTER PROCEDURE [dbo].[Multi_GetStudents] 
    @DepartmentID   int,
    @BatchID        int,
    @UsertypeID     int,
    @LanguageID     int = 1
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

	DECLARE @EntityStatusID INT = 0
	SELECT @EntityStatusID = EntityStatusID FROM dbo.EntityStatus WHERE CodeName = N'Active'

    -- Insert statements for procedure here
	select u.UserID,d.DepartmentID,d.name 'DepartmentName',ut.name 'UTName',utu.UserTypeID,u.FirstName,u.LastName,isnull(r.Resultid,0) 'ResultID', ISNULL(CONVERT(nvarchar(32),u.DateOfBirth,102),'') AS [BirthDate]
	from org.Department d
	join org.[User] u on  u.DepartmentID=d.DepartmentID
	join org.UT_U utu on u.UserID=utu.UserID and utu.UserTypeID = @UsertypeID --between 40 and 49
	join org.LT_userType ut on utu.UserTypeID=ut.UserTypeID AND ut.[LanguageID] = @LanguageID
	left outer join [Synonym_VOKAL_Result] r on u.UserID=r.userid and r.BatchID=@BatchID AND r.EntityStatusID = @EntityStatusID AND r.Deleted IS NULL
	where u.EntityStatusID=@EntityStatusID and u.Deleted IS NULL and d.DepartmentID in (select DepartmentID from org.H_D where ParentID in (select HDID from org.H_D where DepartmentID=@DepartmentID))
	order by d.name,u.LastName,u.FirstName

END

GO
