SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[prc_VOKAL_MoveGraduate]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [dbo].[prc_VOKAL_MoveGraduate] AS' 
END
GO
/*
    2016-10-10 Sarah:   Remove column Status on User, Department and Result tables
    2018-07-23 Ray:     Remove hint as it is not allowed with columnstored index
    2018-11-27 Ray:     Anonymize results move out of private schools
*/
ALTER PROCEDURE [dbo].[prc_VOKAL_MoveGraduate]
(
  @CurrentHDID          int,
  @NewDepartmentID      int = 0,
  @UserIDList           varchar(max),
  @MoveResult           bit,
  @SiteID               int,
  @ActivePeriodID       int,
  @FromPeriodID         int,
  @MovedByUserID        int,
  @OwnerID              int,
  @ErrorMessage         varchar(max)='',
  @DeniedStatusTypeIDs  varchar(64)='2,3,4',
  @returnrows           int = 1,
  @Integration          int = 0
)
AS
BEGIN
    SET NOCOUNT ON

    SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED
    SET @ErrorMessage = ''

    DECLARE @ErrorList TABLE (UserID int, ErrorMessage varchar(max))
    DECLARE @UserListTable TABLE (UserID int, NewDepartmentID int)
    DECLARE @PropValueTable TABLE (PropValueID int)
	DECLARE @DeniedStatusTypeIDList TABLE (StatusTypeID int)
    
    DECLARE @PROP_Receiving_School int, @ActivityTypePROVE int = 2, @ActivityTypeKART int = 3, @DenyTransferStatusTypeIDs nvarchar(max) = '',
            @ActiveEntityStatusID int, @EntityStatusID_Deactive int, @EntityStatusReasonID_DisconnectedObject int

    SELECT @PROP_Receiving_School = Value FROM app.SiteParameter WHERE SiteID = @SiteID AND [Key] = 'Platform.NewSchoolYear.ReceivingDepartmentProp'
    SELECT @ActiveEntityStatusID = EntityStatusID FROM dbo.EntityStatus WHERE CodeName = N'Active'
    SELECT @EntityStatusID_Deactive = EntityStatusID FROM dbo.EntityStatus WHERE CodeName = N'Deactive'
    SELECT @EntityStatusReasonID_DisconnectedObject = [EntityStatusReasonID] FROM [EntityStatusReason] WHERE [CodeName] = 'Deactive_SetByDisconnectedObject'

    IF ISNULL(@PROP_Receiving_School,0) = 0 AND ISNULL(@NewDepartmentID,0) = 0
    BEGIN
	   SET @ErrorMessage = 'No receiving department'
	   Return 1
    END

    IF @NewDepartmentID > 0
    BEGIN
	   INSERT INTO @UserListTable (UserID, NewDepartmentID)
	   SELECT Value, @NewDepartmentID
	   FROM   dbo.funcListToTableInt(@UserIDList,',')
    END 
    ELSE
    BEGIN
	   INSERT INTO @UserListTable (UserID)
	   SELECT Value
	   FROM   dbo.funcListToTableInt(@UserIDList,',')

	   UPDATE @UserListTable
	   SET	NewDepartmentID = (SELECT TOP 1 pv.Value FROM prop.PropValue pv 
						        WHERE pv.PropertyID = @PROP_Receiving_School AND pv.ItemID = ul.UserID
						          AND ISNULL(pv.Value,'') <> '')
	   FROM	@UserListTable ul
    END

    INSERT INTO @ErrorList (UserID, ErrorMessage)
    SELECT UserID, 'No receiving department'
    FROM	 @UserListTable
    WHERE	 NewDepartmentID IS NULL

    DELETE FROM @UserListTable WHERE NewDepartmentID IS NULL

    IF NOT EXISTS (SELECT TOP 1 * FROM @UserListTable)
    BEGIN
	   SELECT * FROM @ErrorList
	   SET @ErrorMessage = 'No user to be moved'
	   Return 2
    END

    CREATE TABLE #ResultTable (ResultID int)

    DECLARE @UT_Graduate int, @UT_NewStudent int, @STATUSTYPE_TRANSFERRED int, @UserTableType int, @DT_Municipality int,
            @UserID int, @NewDepID int, @DenyTransfer int, @ValidDep bit, @ToPeriodID int, @PROP_ActivePeriod int,
            @PROP_SAM int, @PROP_OPT_SAM int, @PROP_DIAGNOSTIC_TEST int, @PROP_EVALUATION_TEST int,
            @UserPropOptSAM int, @CurrentDepartmentID  int, @ParentID int, @ParentHDID int, @GlobalDepartmentID int,
            @MoveProve bit = 0, @MoveKart bit = 0, @UserMoveProve bit, @UserMoveKart bit, @PID int

    SELECT @UT_Graduate = Value	            FROM app.SiteParameter WHERE SiteID = @SiteID AND [Key] = 'Platform.Admin.GraduatesUserType'
    SELECT @UT_NewStudent = Value           FROM app.SiteParameter WHERE SiteID = @SiteID AND [Key] = 'Platform.NewSchoolYear.FresherUserType'
    SELECT @STATUSTYPE_TRANSFERRED = ISNULL(Value,0)  FROM app.SiteParameter WHERE SiteID = @SiteID AND [Key] = 'Platform.NewSchoolYear.TransferredStatusType'
    SELECT @PROP_SAM = ISNULL(Value,0)	    FROM app.SiteParameter WHERE SiteID = @SiteID AND [Key] = 'PROP_SAM'
    SELECT @PROP_OPT_SAM = ISNULL(Value,0)  FROM app.SiteParameter WHERE SiteID = @SiteID AND [Key] = 'Platform.NewSchoolYear.OptionDenyTransferResultProp'
    SELECT @PROP_DIAGNOSTIC_TEST = ISNULL(Value,0)    FROM app.SiteParameter WHERE SiteID = @SiteID AND [Key] = 'Platform.NewSchoolYear.DiagnosticTestProp'
    SELECT @PROP_EVALUATION_TEST = ISNULL(Value,0)    FROM app.SiteParameter WHERE SiteID = @SiteID AND [Key] = 'Platform.Admin.EvaluationTestProp'
    SELECT @PROP_ActivePeriod = Value	    FROM app.SiteParameter WHERE SiteID = @SiteID AND [Key] = 'Platform.NewSchoolYear.CurrentPeriodProp'
    SELECT @PID = ISNULL(Value,0)		    FROM app.SiteParameter WHERE SiteID = @SiteID AND [Key] = 'Platform.NewSchoolYear.CurrentPeriodId'
    SELECT @DT_Municipality = Value         FROM app.SiteParameter WHERE SiteID = @SiteID AND [Key] = 'Platform.Admin.MunicipalityDepartmentTypeId'
    SELECT @DenyTransferStatusTypeIDs = Value         FROM app.SiteParameter WHERE SiteID = @SiteID AND [Key] = 'Platform.NewSchoolYear.DenyTransferStatusTypeList'
    SELECT @GlobalDepartmentID = Value      FROM app.SiteParameter WHERE SiteID = @SiteID AND [Key] = 'Platform.Common.GlobalId'

    SELECT @UserTableType = TableTypeID FROM TableType WHERE Name = 'UserType'
    SELECT @CurrentDepartmentID = DepartmentID, @ParentHDID = ISNULL(ParentID,0) FROM org.H_D WHERE HDID = @CurrentHDID
    -- Check the moving result permission from the nearest Municipality level
    SET @ParentID = (SELECT TOP 1 phd.[DepartmentID] FROM [org].[H_D] phd JOIN [org].[H_D] hd ON hd.[HDID] = @CurrentHDID AND hd.[Path] LIKE '%\' + CAST(phd.[HDID] AS nvarchar(32)) + '\%'
                                                     JOIN [org].[DT_D] dtd ON phd.[DepartmentID] = dtd.[DepartmentID] AND dtd.[DepartmentTypeID] = @DT_Municipality
                     ORDER BY LEN(phd.[Path]) DESC)

	INSERT INTO @DeniedStatusTypeIDList (StatusTypeID) SELECT Value FROM dbo.funcListToTableInt(@DenyTransferStatusTypeIDs,',')

    IF @ParentID > 0
    BEGIN
	   IF EXISTS (SELECT 1 FROM prop.PropValue WHERE PropertyID = @PROP_DIAGNOSTIC_TEST AND ItemID = @ParentID AND Value = 1)
	   BEGIN
		  SET @MoveProve = 1
	   END
	   IF EXISTS (SELECT 1 FROM prop.PropValue WHERE PropertyID = @PROP_EVALUATION_TEST AND ItemID = @ParentID AND Value = 1)
	   BEGIN
		  SET @MoveKart = 1
	   END
    END

    DECLARE @NewUserType TABLE (UserID int, UserTypeID int)

    CREATE TABLE #SurveyBatchList (ActivityType int, SurveyID int, BatchID int, DepartmentID int, ReportServer nvarchar(64), ReportDB nvarchar(64), EndDate datetime)
    DECLARE c_User CURSOR FORWARD_ONLY READ_ONLY FOR SELECT * FROM @UserListTable

    IF @MoveResult > 0
    BEGIN
	   INSERT INTO #SurveyBatchList (ActivityType, SurveyID, DepartmentID, ReportServer, ReportDB, EndDate)
	   SELECT a.[Type], s.SurveyID, ult.NewDepartmentID, s.ReportServer, s.ReportDB, s.EndDate
	   FROM at.Survey s JOIN at.Activity a ON s.ActivityID = a.ActivityID AND a.OwnerID = @OwnerID
	   CROSS JOIN (SELECT DISTINCT NewDepartmentID FROM @UserListTable) ult

	   UPDATE #SurveyBatchList
	   SET BatchID = (SELECT TOP 1 b.BatchID FROM at.Batch b WHERE b.SurveyID = sbl.SurveyID AND b.DepartmentID = sbl.DepartmentID ORDER BY b.Created DESC)
	   FROM #SurveyBatchList sbl
	  
	   IF @Integration= 0
		   UPDATE #SurveyBatchList
		   SET BatchID = (SELECT TOP 1 b.BatchID FROM at.Batch b WHERE b.SurveyID = sbl.SurveyID AND b.DepartmentID = sbl.DepartmentID ORDER BY b.Created DESC)
		   FROM #SurveyBatchList sbl 
       ELSE 
		   UPDATE #SurveyBatchList
		   SET BatchID = b.BatchID
		   FROM #SurveyBatchList sbl
		   join org.H_D hd on hd.DepartmentID=sbl.DepartmentID
		   join org.H_D hdp on hdp.hdID=hd.ParentID
		   join at.Batch b on b.SurveyID = sbl.SurveyID AND b.DepartmentID = hdp.DepartmentID
    END

    OPEN c_User
    FETCH c_User INTO @UserID, @NewDepID
    WHILE @@FETCH_STATUS = 0
    BEGIN
        DELETE FROM @NewUserType
        SELECT @ValidDep = 1 FROM org.Department WHERE DepartmentID = @NewDepID AND EntityStatusID = @ActiveEntityStatusID AND Deleted IS NULL
        IF ISNULL(@ValidDep,0) = 1
        BEGIN
		  BEGIN TRANSACTION
		  BEGIN TRY

			 UPDATE org.[User] SET DepartmentID = @NewDepID WHERE UserID = @UserID

			 SELECT @ToPeriodID = ISNULL(Value,@PID) FROM prop.PropValue WHERE PropertyID = @PROP_ActivePeriod AND ItemID = @NewDepID
			 IF ISNULL(@ToPeriodID,0) = 0
			 BEGIN
				SET @ToPeriodID = @ActivePeriodID
			 END

             -- Check current PeriodID of receiving department
			 IF @FromPeriodID != @ToPeriodID
			 BEGIN
				-- If receiving department has done NSY and source department has not, upgrade moving student to higher level
                IF @FromPeriodID = @PID
				BEGIN
				    INSERT INTO @NewUserType (UserID, UserTypeID)
				    SELECT @UserID, om.ToID
				    FROM   ObjectMapping om JOIN org.UT_U utu ON utu.UserTypeID = om.FromID AND utu.UserID = @UserID
				     AND  om.FromTableTypeID = @UserTableType AND om.ToTableTypeID = @UserTableType 
				     AND  om.RelationTypeID=3

                    DELETE utu FROM org.UT_U utu 
				    WHERE utu.UserID = @UserID 
				      AND EXISTS (SELECT 1 FROM ObjectMapping om WHERE om.FromID = utu.UserTypeID
						             AND om.FromTableTypeID = @UserTableType AND om.ToTableTypeID = @UserTableType 
						             AND om.RelationTypeID=3)
				END
				-- If source department has done NSY and receiving department has not, downgrade moving student to lower level
                ELSE IF @ToPeriodID = @PID
				BEGIN
				    INSERT INTO @NewUserType (UserID, UserTypeID)
				    SELECT @UserID, om.FromID
				    FROM   ObjectMapping om JOIN org.UT_U utu ON utu.UserTypeID = om.ToID AND utu.UserID = @UserID
				     AND  om.FromTableTypeID = @UserTableType AND om.ToTableTypeID = @UserTableType 
				     AND  om.RelationTypeID=3
                    
                    DELETE utu FROM org.UT_U utu 
				    WHERE utu.UserID = @UserID 
				      AND EXISTS (SELECT 1 FROM ObjectMapping om WHERE om.ToID = utu.UserTypeID
						             AND om.FromTableTypeID = @UserTableType AND om.ToTableTypeID = @UserTableType 
						             AND om.RelationTypeID=3)
				END

				INSERT INTO org.UT_U (UserID, UserTypeID) SELECT UserID, UserTypeID FROM @NewUserType
			 END

             -- Mark moved student as new student in the receiving school, no longer graduated student
			 DELETE FROM org.UT_U WHERE UserID = @UserID AND UserTypeID = @UT_Graduate
			 INSERT INTO org.UT_U (UserID, UserTypeID) SELECT @UserID, @UT_NewStudent WHERE NOT EXISTS (SELECT 1 FROM org.UT_U WHERE UserID=@UserID AND UserTypeID=@UT_NewStudent)

             -- Save the info of source school for later references
			 IF EXISTS (SELECT 1 FROM prop.PropValue WHERE PropertyID = @PROP_Receiving_School AND ItemID = @UserID)
			 BEGIN
				UPDATE prop.PropValue SET Value = @CurrentDepartmentID, Updated = GETDATE()
				WHERE PropertyID = @PROP_Receiving_School AND ItemID = @UserID

				INSERT INTO @PropValueTable (PropValueID) SELECT PropValueID FROM prop.PropValue WHERE PropertyID = @PROP_Receiving_School AND ItemID = @UserID
			 END
			 ELSE
			 BEGIN
				INSERT INTO prop.PropValue (PropertyID, ItemID, Value, CreatedBy, UpdatedBy)
				VALUES (@PROP_Receiving_School, @UserID, @CurrentDepartmentID, @MovedByUserID, @MovedByUserID)

				INSERT INTO @PropValueTable (PropValueID) VALUES (SCOPE_IDENTITY())
			 END

			 IF @MoveResult > 0
			 BEGIN
				-- Checking moving result permission from individual student
                IF EXISTS (SELECT 1 FROM prop.PropValue WHERE PropertyID = @PROP_SAM AND ItemID = @UserID AND [PropOptionID] != @PROP_OPT_SAM)
				BEGIN
				    SET @UserMoveProve = 0
				    SET @UserMoveKart = 0
				END
				ELSE
				BEGIN
				    SET @UserMoveProve = @MoveProve
				    SET @UserMoveKart = @MoveKart
				END
                    
                    INSERT INTO #ResultTable (ResultID)
				    SELECT ResultID FROM [dbo].[Synonym_VOKAL_Result] WHERE UserID = @UserID

                    -- Move non-historical results of current surveys (not expired) to new department when:
                    -- 1. Allow moving and not global batch results (outside reporting results)
                    -- 2. Evaluation activities
                    -- 3. Other activities' results with no EndDate and not denied in list of status types
                    -- Set to new BatchID if receiving department has a batch
				    UPDATE  Result
				    SET DepartmentID = @NewDepID,
                        BatchID = sbl.BatchID,
                        StatusTypeID = @STATUSTYPE_TRANSFERRED
				    FROM [dbo].[Synonym_VOKAL_Result] result, #SurveyBatchList sbl 
				    WHERE Result.SurveyID = sbl.SurveyID
					AND Result.UserID = @UserID
					AND Result.UserGroupID = 0
					AND ((Result.[EndDate] IS NULL AND Result.StatusTypeID NOT IN (SELECT StatusTypeID FROM @DeniedStatusTypeIDList))
                         OR sbl.ActivityType = @ActivityTypeKART)
					AND ((sbl.ActivityType = @ActivityTypePROVE AND @UserMoveProve = 1)
						    OR (sbl.ActivityType = @ActivityTypeKART AND @UserMoveKart = 1)
						    OR (sbl.ActivityType != @ActivityTypePROVE AND sbl.ActivityType != @ActivityTypeKART))
					AND sbl.EndDate > GETDATE()
					AND sbl.DepartmentID = @NewDepID
					AND sbl.BatchID IS NOT NULL
                    AND Result.[DepartmentID] != @GlobalDepartmentID

                    -- Otherwise, only set to new DepartmentID
				    UPDATE  result
				    SET   DepartmentID = @NewDepID
				    FROM  [dbo].[Synonym_VOKAL_Result] result ,#SurveyBatchList sbl 
				    WHERE Result.SurveyID = sbl.SurveyID
					AND Result.UserID = @UserID
					AND Result.UserGroupID = 0
					AND ((Result.[EndDate] IS NULL AND Result.StatusTypeID NOT IN (SELECT StatusTypeID FROM @DeniedStatusTypeIDList))
                         OR sbl.ActivityType = @ActivityTypeKART)
					AND ((sbl.ActivityType = @ActivityTypePROVE AND @UserMoveProve = 1)
						    OR (sbl.ActivityType = @ActivityTypeKART AND @UserMoveKart = 1)
						    OR (sbl.ActivityType != @ActivityTypePROVE AND sbl.ActivityType != @ActivityTypeKART))
					AND sbl.EndDate > GETDATE()
					AND sbl.DepartmentID = @NewDepID
					AND sbl.BatchID IS NULL
                    AND Result.[DepartmentID] != @GlobalDepartmentID

                    -- Soft delete non-historical evaluation activities' results if not allowed to move
				    UPDATE result
				    SET   [EntityStatusID] = @EntityStatusID_Deactive, [result].[EntityStatusReasonID] = @EntityStatusReasonID_DisconnectedObject
				    FROM  [dbo].[Synonym_VOKAL_Result] result ,#SurveyBatchList sbl 
				    WHERE Result.SurveyID = sbl.SurveyID
					AND Result.UserID = @UserID
					AND Result.UserGroupID = 0
					AND (sbl.ActivityType = @ActivityTypeKART AND @UserMoveKart != 1)
					AND sbl.EndDate > GETDATE()
					AND sbl.DepartmentID = @NewDepID
                    
                    -- Anonymize all results of the activity types which are not allowed to move, including global batch results
				    UPDATE  result 
				    SET  [Email] = convert(nvarchar(16),UserID), UserID = 0
				    FROM [dbo].[Synonym_VOKAL_Result] result ,  #SurveyBatchList sbl 
				    WHERE Result.SurveyID = sbl.SurveyID
					AND Result.UserID = @UserID
					AND ((sbl.ActivityType = @ActivityTypePROVE AND @UserMoveProve != 1)
					    OR (sbl.ActivityType = @ActivityTypeKART AND @UserMoveKart != 1))
					--AND Result.DepartmentID != @NewDepID
					AND sbl.DepartmentID = @NewDepID
			 END

			 COMMIT TRANSACTION
		  END TRY
		  BEGIN CATCH
			 ROLLBACK TRANSACTION
			 INSERT INTO @ErrorList (UserID, ErrorMessage) VALUES (@UserID, ERROR_MESSAGE())
			  
		  END CATCH
        END
        ELSE
        BEGIN
		  INSERT INTO @ErrorList (UserID, ErrorMessage) VALUES (@UserID, 'Invalid receiving department')
        END

        FETCH c_User INTO @UserID, @NewDepID
      END
    CLOSE c_User
    DEALLOCATE c_User
    
    -- Return rows for clear cache purpose
    IF @returnrows=1
    BEGIN
        SELECT * FROM @ErrorList
        SELECT UserID FROM @UserListTable
        SELECT * FROM @PropValueTable
        SELECT * FROM #ResultTable
    END
	DROP TABLE #SurveyBatchList
    DROP TABLE #ResultTable
END

GO
