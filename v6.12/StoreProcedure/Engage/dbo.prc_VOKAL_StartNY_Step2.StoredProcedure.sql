SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[prc_VOKAL_StartNY_Step2]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [dbo].[prc_VOKAL_StartNY_Step2] AS' 
END
GO
/***************************************************************
Run startNY in R-database for each school that has Integration

2017-10-11 Ray:     Refactor
***************************************************************/
ALTER PROCEDURE [dbo].[prc_VOKAL_StartNY_Step2](
    @SiteId int)
AS
BEGIN
    DECLARE @HDID_School int, @ActivityID int, @SurveyID int, @NewSurveyID int, @BatchID int, @PeriodID int, @NewPeriodID int, @OwnerID AS int,
            @DepartmentType_School int, @error_message AS nvarchar(max),
            @EntityStatusID_Active int= 0, @EntityStatusID_Inactive int= 0;

    SELECT @EntityStatusID_Active = [EntityStatusID] FROM [EntityStatus] WHERE [CodeName] = 'Active';
    SELECT @EntityStatusID_Inactive = [EntityStatusID] FROM [EntityStatus] WHERE [CodeName] = 'Inactive';

    SELECT @DepartmentType_School = [sp].value FROM [app].[SiteParameter] AS [sp] WHERE [sp].[Key] = 'Platform.Common.SchoolDepartmentTypeId' AND [sp].[SiteID] = @SiteId;
    IF (@DepartmentType_School IS NULL)
    BEGIN
        RAISERROR(N'Site param for Platform.Common.SchoolDepartmentTypeId not defined', 10, 1);
    END;

    SELECT @PeriodID = [sp].value FROM [app].[SiteParameter] AS [sp] WHERE [sp].[Key] = 'Platform.NewSchoolYear.CurrentPeriodId' AND [sp].[SiteID] = @SiteId;
    IF (@PeriodID IS NULL)
    BEGIN
        RAISERROR(N'Site param for Platform.NewSchoolYear.CurrentPeriodId not defined', 10, 1);
    END;

    SELECT @NewPeriodID = [sp].value FROM [app].[SiteParameter] AS [sp] WHERE [sp].[Key] = 'Platform.NewSchoolYear.NextPeriodId' AND [sp].[SiteID] = @SiteId;
    IF (@NewPeriodID IS NULL)
    BEGIN
        RAISERROR(N'Site param for Platform.NewSchoolYear.NextPeriodId not defined', 10, 1);
    END;

    SELECT @OwnerID = [s].[OwnerID] FROM [app].[Site] AS [s] WHERE [s].[SiteID] = @SiteId;
    IF (@OwnerID IS NULL)
    BEGIN
        DECLARE @errorMsg AS varchar(max)= 'Owner-id not defined for site '+CAST(@siteId AS varchar(3));
        RAISERROR(@errorMsg, 10, 1);
    END;

    DECLARE @XCID_DenyTransferResultTable TABLE ([XCID] int);
    DECLARE @XCID_DenyTransferResult nvarchar(max);

    SELECT @XCID_DenyTransferResult = [Value] FROM [app].[SiteParameter] WHERE [SiteID] = @SiteID AND [Key] = 'XCID_DenyTransferResult';
    IF ISNULL(@XCID_DenyTransferResult, '') != ''
    BEGIN
        INSERT INTO @XCID_DenyTransferResultTable ([XCID])
        SELECT [Value] FROM [dbo].[funcListToTableINT] (@XCID_DenyTransferResult, ',');
    END;

    IF (@DepartmentType_School IS NOT NULL AND @PeriodID IS NOT NULL AND @OwnerID IS NOT NULL)
    BEGIN
        SET NOCOUNT ON;
        SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED;

        DECLARE curBatches CURSOR LOCAL FAST_FORWARD READ_ONLY FOR
        SELECT DISTINCT [b].[batchid], [s].[surveyid],
                        CASE
                            WHEN ([a].[type] = 3
                              AND [s].[surveyid] = (SELECT TOP 1 [sur].[SurveyID]
                                                    FROM [at].[Survey] [sur]
                                                    WHERE [sur].[ActivityID] = [s].[ActivityID] AND [sur].[PeriodID] = @periodid
                                                    ORDER BY [sur].[EndDate] DESC, [sur].[StartDate] DESC
                                                    )   -- last survey of period
                                 ) THEN (SELECT TOP 1 [sur].[SurveyID]
                                         FROM [at].[Survey] [sur]
                                         WHERE [sur].[ActivityID] = [s].[ActivityID] AND [sur].[PeriodID] = @newperiodid
                                         ORDER BY [sur].[StartDate], [sur].[EndDate]
                                        )     -- first survey of new period
                            ELSE 0
                        END  -- type 3 = kartleggingsverktøy
                        , [hd].[hdid], [s].[ActivityID]
        FROM [at].[batch] [b]
        JOIN [at].[survey] [s] ON [s].[surveyid] = [b].[surveyid] AND [s].[periodid] = @periodid
        JOIN [at].[activity] [a] ON [a].[activityid] = [s].[activityid]
        JOIN [org].[department] [d] ON [d].[departmentid] = [b].[departmentid]
        JOIN [org].[h_d] [hd] ON [hd].[departmentid] = [d].[departmentid] AND [hd].[Deleted] = 0
        JOIN [org].[dt_d] [dtd] ON [dtd].[departmentid] = [d].[departmentid] AND [dtd].[departmenttypeid] = @DepartmentType_School
        WHERE [d].[ownerid] = @OwnerID
          AND [d].[customerid] IN (SELECT [customerid] FROM [org].[Customer] WHERE [HasUserINTegration] = 1)
          AND 0 < (SELECT COUNT(*) FROM [dbo].[Synonym_VOKAL_Result] WHERE [batchid] = [b].[batchid]) -- only with results
          AND [d].[EntityStatusID] IN (@EntityStatusID_Active, @EntityStatusID_Inactive)
          AND [d].[Deleted] IS NULL; -- only not deleted schools

        BEGIN TRANSACTION;
        BEGIN TRY
            OPEN curBatches;
            FETCH NEXT FROM curBatches INTO @BatchID, @SurveyID, @NewSurveyID, @HDID_School, @ActivityID;

            WHILE(@@FETCH_STATUS = 0)
            BEGIN
                -- only with at least 1 batch
                IF EXISTS (SELECT 1 FROM [at].[XC_A] [xca]
                                    JOIN @XCID_DenyTransferResultTable [xt] ON [xca].[XCID] = [xt].[XCID] AND [xca].[ActivityID] = @ActivityID)
                   OR NOT EXISTS (SELECT 1 FROM [at].[Batch] [b] WHERE [b].[SurveyID] = @NewSurveyID)
                BEGIN
                    SET @NewSurveyID = 0;
                END;

                EXEC [dbo].[Synonym_prc_VOKAL_startNY] @HDID_School, @Periodid, @SurveyID, @BatchID, @NewSurveyID, 3, 5;

                FETCH NEXT FROM curBatches INTO @BatchID, @SurveyID, @NewSurveyID, @HDID_School, @ActivityID;
            END;
            CLOSE curBatches;
            DEALLOCATE curBatches;
            COMMIT TRANSACTION;
        END TRY
        BEGIN CATCH
            ROLLBACK TRANSACTION;
            IF CURSOR_STATUS('global', 'curBatches') = 1
            BEGIN
                CLOSE curBatches;
                DEALLOCATE curBatches;
            END;
        END CATCH;
    END;
END;
GO