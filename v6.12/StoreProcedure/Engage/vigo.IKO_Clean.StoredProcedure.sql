SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[vigo].[IKO_Clean]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [vigo].[IKO_Clean] AS' 
END
GO
ALTER PROCEDURE [vigo].[IKO_Clean](
    @VOKALSiteId     int          = 5,
    @AvsenderFylkeNr nvarchar(50)
) AS
BEGIN

    WITH YearPeriod
    AS ( --declare @VOKALSiteId as int = 5
    SELECT CAST(YEAR([startdate]) AS char(4)) + CAST(YEAR([enddate]) AS char(4)) AS [period]
    FROM [app].[SiteParameter] AS [sp]
    JOIN [at].[Period] AS [p] ON [sp].Value = [p].[periodId]
    WHERE [sp].[Key] = 'Platform.NewSchoolYear.CurrentPeriodId' AND [sp].[SiteID] = @VOKALSiteId /* There are multiple sites in app.Site so we need to select the one to get the parameters from */
    )

    --GFP: The delete below makes it so we keep just one row for vokal_soker_fag for a person and a subject (for each schoolyear and county), we don't need multiple
    --declare @AvsenderFylkeNr as nvarchar(50) = '04';    with YearPeriod as (select period = '20162017')
    DELETE
    --declare @AvsenderFylkeNr as nvarchar(50) = '04';    with YearPeriod as (select period = '20162017')    select *
    FROM [vigo].[vokal_soker_fag]
    WHERE [id] IN ( --declare @AvsenderFylkeNr as nvarchar(50) = '04';    with YearPeriod as (select period = '20162017')
                    SELECT [id]
                    FROM (SELECT *, ROW_NUMBER() OVER(PARTITION BY [fødselsnummer], [fagkode] ORDER BY [fagkode], [karakter - standpunkt] DESC) AS [rn] /* GFP: I added sorting by karakter so that the selection of what to delete would be more consistent, and hopefully keep the top karakter of the learner */
                          FROM [vigo].[vokal_soker_fag]
                          JOIN [YearPeriod] AS [yp] ON [skoleår] = [yp].[period]
                          WHERE [fødselsnummer] IN (SELECT DISTINCT [fødselsnummer]
                                                    FROM (--declare @AvsenderFylkeNr as nvarchar(50) = '04';    with YearPeriod as (select period = '20162017')
                                                        SELECT [fødselsnummer], [fagkode], COUNT(*) AS [cnt]
                                                        FROM [vigo].[vokal_soker_fag]
                                                        JOIN [YearPeriod] AS [yp] ON [skoleår] = [yp].[period]
                                                        WHERE [fylkesnr - avsender] = @AvsenderFylkeNr
                                                        GROUP BY [fødselsnummer], [fagkode]
                                                        HAVING COUNT(*) > 1
                                                        ) AS [d]
                                                   )
                            AND [fylkesnr - avsender] = @AvsenderFylkeNr
                        ) AS [dd]
                    WHERE [dd].[rn] > 1
                  );

    --GFP: I have added the logic to calculate the number of grades (Karakterantall) at this position in the logic, because the number is used in queries further down
    --declare @AvsenderFylkeNr as nvarchar(50) = '04'
    UPDATE [vigo].[vokal_soker_program] SET [karakterAntall] = (SELECT COUNT(*)
                                                                FROM [vigo].[vokal_soker_fag] [f]
                                                                WHERE [f].[fødselsnummer] = [vigo].[vokal_soker_program].[fødselsnummer]
                                                                  AND [f].[skolenr] = [vigo].[vokal_soker_program].[skolenr]
                                                                  AND [f].[skoleår] = [vigo].[vokal_soker_program].[skoleår]
                                                                  AND [f].[programområde] = [vigo].[vokal_soker_program].[programområde]
                                                                  AND [f].[fylkesnr - avsender] = [vigo].[vokal_soker_program].[fylkesnr - avsender]
                                                                  AND [f].[fylkesnr - avsender] = @AvsenderFylkeNr
                                                                )
    WHERE [vigo].[vokal_soker_program].[fylkesnr - avsender] = @AvsenderFylkeNr;

    --GFP: The delete below seem to remove duplicates in vokal_soker_program
    --declare @AvsenderFylkeNr as nvarchar(50) = '04'
    DELETE [p]
    --declare @AvsenderFylkeNr as nvarchar(50) = '04'    select *
    FROM [vigo].[vokal_soker_program] [p]
    JOIN ( --declare @AvsenderFylkeNr as nvarchar(50) = '04'
            SELECT *
            FROM (SELECT *, ROW_NUMBER() OVER(PARTITION BY [fødselsnummer] ORDER BY [Karakterantall] DESC, [karakterpoengsum] DESC /* Added by GFP */, [fravær - timer] ASC /*WHY???*/ ) AS [rn]
                  FROM [vigo].[vokal_soker_program]
                  WHERE [fødselsnummer] IN (SELECT [fødselsnummer]
                                            FROM (--declare @AvsenderFylkeNr as nvarchar(50) = '04'
                                                  SELECT [fødselsnummer], [skoleår], COUNT(*) AS [cnt]
                                                  FROM [vigo].[vokal_soker_program]
                                                  WHERE [nivå] = 1 AND [fylkesnr - avsender] = @AvsenderFylkeNr
                                                  GROUP BY [fødselsnummer], [skoleår]
                                                  HAVING COUNT(*) > 1
                                                 ) AS [d]
                                           )
                    AND [nivå] = 1
                    AND [fylkesnr - avsender] = @AvsenderFylkeNr
                ) AS [dd]
            WHERE [dd].[rn] > 1
        ) AS [ddd] ON [p].[fødselsnummer] = [ddd].[fødselsnummer]
                  AND [p].[skoleår] = [ddd].[skoleår]
                  AND [p].[skolenr] = [ddd].[skolenr]
                  AND [p].[fylkesnr - avsender] = @AvsenderFylkeNr /* Added by GFP */
                  AND [ddd].[fylkesnr - avsender] = @AvsenderFylkeNr /* Added by GFP */
                  AND [p].[nivå] = [ddd].[nivå] /* Added by GFP, but unsure! */
                  AND [p].[programområde] = [ddd].[programområde]; /* Added by GFP, but unsure! */
    --order by p.[fødselsnummer], p.skoleår, p.programområde

    -- IKO_VG1
    --declare @AvsenderFylkeNr as nvarchar(50) = '04'
    DELETE
    --declare @AvsenderFylkeNr as nvarchar(50) = '04'    select *
    FROM [vigo].[vokal_soker_fag]
    WHERE [karakter - standpunkt] IS NULL OR [karakter - standpunkt] = '' /* Added by GFP */ AND [fylkesnr - avsender] = @AvsenderFylkeNr;

    DELETE
    --declare @AvsenderFylkeNr as nvarchar(50) = '02'    select *
    FROM [vigo].[vokal_soker_program]
    WHERE [fødselsnummer] IN (  --declare @AvsenderFylkeNr as nvarchar(50) = '04'
                                SELECT [fødselsnummer]
                                FROM [vigo].[vokal_soker_program]
                                WHERE [programområde] = 'GSGSK0----' AND [fylkesnr - avsender] = @AvsenderFylkeNr
                                GROUP BY [fødselsnummer]
                                HAVING COUNT(*) > 1
                             )
      AND [fylkesnr - avsender] = @AvsenderFylkeNr
      AND [programområde] = 'GSGSK0----'
      AND [karakterantall] < 4; /* if NULL then this criteria is not true */

    -- Delete first GSK schoolyear row(s) if student has results from more than one GSK school year
    --declare @AvsenderFylkeNr as nvarchar(50) = '04'
    DELETE [p]
    --declare @AvsenderFylkeNr as nvarchar(50) = '04'    select *
    FROM [vigo].[vokal_soker_program] [p]
    JOIN (
            --declare @AvsenderFylkeNr as nvarchar(50) = '04'
            SELECT [fødselsnummer], MIN([skoleår]) [skoleår]
            FROM [vigo].[vokal_soker_program]
            WHERE [programområde] = 'GSGSK0----' AND [fylkesnr - avsender] = @AvsenderFylkeNr
            GROUP BY [fødselsnummer]
            HAVING COUNT(*) > 1 AND MAX([skoleår]) <> MIN([skoleår])
         ) AS [pg] ON [pg].[fødselsnummer] = [p].[fødselsnummer] AND [pg].[skoleår] = [p].[skoleår] AND [p].[fylkesnr - avsender] = @AvsenderFylkeNr;
    --order by p.fødselsnummer, p.skoleår, p.skolenr

    DELETE [f]
    --declare @AvsenderFylkeNr as nvarchar(50) = '04'    select *
    FROM [vigo].[vokal_soker_fag] [f]
    LEFT OUTER JOIN [vigo].[vokal_soker_program] [p] ON [p].[fødselsnummer] = [f].[fødselsnummer] AND [p].[skolenr] = [f].[skolenr] AND [p].[skoleår] = [f].[skoleår]
    WHERE [p].[skolenr] IS NULL AND [f].[fylkesnr - avsender] = @AvsenderFylkeNr AND [p].[fylkesnr - avsender] = @AvsenderFylkeNr;

    /* Comment from GFP: The logic below seems to trust that the latest ID is the one that should be kept... I am not sure about this... */
    --declare @AvsenderFylkeNr as nvarchar(50) = '04'
    DELETE
    --declare @AvsenderFylkeNr as nvarchar(50) = '04'    select *
    FROM [vigo].[vokal_soker_fag]
    WHERE [ID] IN (
                    --declare @AvsenderFylkeNr as nvarchar(50) = '04'
                    SELECT MIN([id])
                    FROM [vigo].[vokal_soker_fag]
                    WHERE [programområde] = 'GSGSK0----' AND [fylkesnr - avsender] = @AvsenderFylkeNr
                    GROUP BY [fødselsnummer], [fagkode], [skoleår]
                    HAVING COUNT(*) > 1
                  );

    -- IKO_VG2  (Comment from GFP: ...but it seems like the deletion below is affecting all levels, not just VG2)
    DELETE
    --declare @AvsenderFylkeNr as nvarchar(50) = '04'    select *
    FROM [vigo].[vokal_soker_fag]
    WHERE ([karakter - standpunkt] IS NULL OR [karakter - standpunkt] = '') AND [fylkesnr - avsender] = @AvsenderFylkeNr;

    DELETE
    --declare @AvsenderFylkeNr as nvarchar(50) = '04'    select *
    FROM [vigo].[vokal_soker_program]
    WHERE [skolenr] = '02099' /* Eksamen in AFK */ AND [fylkesnr - avsender] = '02' AND [fylkesnr - avsender] = @AvsenderFylkeNr /* See above, it only will execute for 02' */ AND [karakterantall] = 0;

    --select * from tmp.vokal_soker_program where skolenr in (select skolenummer  from tmp.vokal_skole where skolenavn like '%eksamen%')

    DELETE
    --declare @AvsenderFylkeNr as nvarchar(50) = '04'    select *
    FROM [vigo].[vokal_soker_program] WHERE [karakterantall] = 0 AND [fylkesnr - avsender] = @AvsenderFylkeNr;

    --declare @AvsenderFylkeNr as nvarchar(50) = '04'
    DECLARE @RCount AS int;
    SET @RCount = 1;

    WHILE @RCount <> 0
    BEGIN
        --declare @AvsenderFylkeNr as nvarchar(50) = '04'
        DELETE [p]
        --declare @AvsenderFylkeNr as nvarchar(50) = '04'    select *
        FROM [vigo].[vokal_soker_program] [p]
        JOIN (--declare @AvsenderFylkeNr as nvarchar(50) = '04'
                SELECT [fødselsnummer], MIN([skoleår]) [skoleår]
                FROM [vigo].[vokal_soker_program]
                WHERE [nivå] = 1 AND [fylkesnr - avsender] = @AvsenderFylkeNr
                GROUP BY [fødselsnummer]
                HAVING COUNT(*) > 1 AND MAX([skoleår]) <> MIN([skoleår])
             ) AS [pg] ON [pg].[fødselsnummer] = [p].[fødselsnummer] AND [pg].[skoleår] = [p].[skoleår] AND [p].[fylkesnr - avsender] = @AvsenderFylkeNr AND [p].[nivå] = 1; /* Added by GFP, not sure! */

        SET @RCount = @@ROWCOUNT;
    END;

    --declare @AvsenderFylkeNr as nvarchar(50) = '04'
    DELETE
    --declare @AvsenderFylkeNr as nvarchar(50) = '04'    select *
    FROM [vigo].[vokal_soker_fag]
    WHERE [id] IN (--declare @AvsenderFylkeNr as nvarchar(50) = '04'
                    SELECT [f2].[id]
                    FROM [vigo].[vokal_soker_fag] [f]
                    JOIN [vigo].[vokal_soker_fag] [f2] ON [f].[fagkode] = 'NAT1001'
                                                      AND [f2].[fagkode] = 'NAT1002'
                                                      AND [f].[fødselsnummer] = [f2].[fødselsnummer]
                                                      AND [f].[Nivå] = [f2].[nivå]
                                                      AND [f].[skoleår] = [f2].[skoleår]
                                                      AND [f].[fylkesnr - avsender] = @AvsenderFylkeNr
                                                      AND [f2].[fylkesnr - avsender] = @AvsenderFylkeNr
                  )
      AND [fylkesnr - avsender] = @AvsenderFylkeNr;

    DELETE
    --declare @AvsenderFylkeNr as nvarchar(50) = '04'    select *
    FROM [vigo].[vokal_soker_fag]
    WHERE [id] IN (--declare @AvsenderFylkeNr as nvarchar(50) = '04'
                    SELECT [f2].[id]
                    FROM [vigo].[vokal_soker_fag] [f]
                    JOIN [vigo].[vokal_soker_fag] [f2] ON [f].[fagkode] = 'NOR1201'
                                                      AND [f2].[fagkode] = 'NOR1204'
                                                      AND [f].[fødselsnummer] = [f2].[fødselsnummer]
                                                      AND [f].[Nivå] = [f2].[nivå]
                                                      AND [f].[skoleår] = [f2].[skoleår]
                                                      AND [f].[fylkesnr - avsender] = @AvsenderFylkeNr
                                                      AND [f2].[fylkesnr - avsender] = @AvsenderFylkeNr
                    )
      AND [fylkesnr - avsender] = @AvsenderFylkeNr;

    DELETE
    --declare @AvsenderFylkeNr as nvarchar(50) = '04'    select *
    FROM [vigo].[vokal_soker_fag]
    WHERE [id] IN (--declare @AvsenderFylkeNr as nvarchar(50) = '04'
                    SELECT [f2].[id]
                    FROM [vigo].[vokal_soker_fag] [f]
                    JOIN [vigo].[vokal_soker_fag] [f2] ON [f].[fagkode] = 'NOR1203'
                                                      AND [f2].[fagkode] = 'NOR1205'
                                                      AND [f].[fødselsnummer] = [f2].[fødselsnummer]
                                                      AND [f].[Nivå] = [f2].[nivå]
                                                      AND [f].[skoleår] = [f2].[skoleår]
                                                      AND [f].[fylkesnr - avsender] = @AvsenderFylkeNr
                                                      AND [f2].[fylkesnr - avsender] = @AvsenderFylkeNr
                  )
      AND [fylkesnr - avsender] = @AvsenderFylkeNr;

    -- IKO_VG3
    SET @RCount = 1;

    WHILE @RCount <> 0
    BEGIN
        --declare @AvsenderFylkeNr as nvarchar(50) = '04'
        DELETE [p]
        --declare @AvsenderFylkeNr as nvarchar(50) = '04'    select *
        FROM [vigo].[vokal_soker_program] [p]
        JOIN (--declare @AvsenderFylkeNr as nvarchar(50) = '04'
                SELECT [fødselsnummer], MIN([skoleår]) [skoleår]
                FROM [vigo].[vokal_soker_program]
                WHERE [nivå] = 2 AND [fylkesnr - avsender] = @AvsenderFylkeNr
                GROUP BY [fødselsnummer]
                HAVING COUNT(*) > 1 AND MAX([skoleår]) <> MIN([skoleår])
             ) AS [pg] ON [pg].[fødselsnummer] = [p].[fødselsnummer] AND [pg].[skoleår] = [p].[skoleår] AND [p].[fylkesnr - avsender] = @AvsenderFylkeNr AND [p].[nivå] = 2; /* Added by GFP, not sure! */

        SET @RCount = @@ROWCOUNT;
    END;

    DELETE [p]
    --declare @AvsenderFylkeNr as nvarchar(50) = '04'    select *
    FROM [vigo].[vokal_soker_program] [p]
    JOIN (--declare @AvsenderFylkeNr as nvarchar(50) = '04'
            SELECT [fødselsnummer], MIN([Karakterantall]) [karantall]
            FROM [vigo].[vokal_soker_program]
            WHERE [nivå] = 2 AND [fylkesnr - avsender] = @AvsenderFylkeNr
            GROUP BY [fødselsnummer]
            HAVING COUNT(*) > 1 AND MAX([Karakterantall]) <> MIN([Karakterantall])
         ) AS [pg] ON [pg].[fødselsnummer] = [p].[fødselsnummer]
     AND [pg].[karantall] = [p].[Karakterantall]
     AND [p].[fylkesnr - avsender] = @AvsenderFylkeNr
     AND [p].[nivå] = 2; /* Added by GFP, not sure! */

END;
GO