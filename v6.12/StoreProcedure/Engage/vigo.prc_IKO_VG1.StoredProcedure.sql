SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[vigo].[prc_IKO_VG1]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [vigo].[prc_IKO_VG1] AS' 
END
GO
ALTER PROC [vigo].[prc_IKO_VG1](
    @SurveyID        integer,
    @GradeScaleID    integer      = 3980,
    @YesNoScaleID    integer      = 3978,
    @GradeQID        integer      = 28075,
    @FirstChoiceQID  integer      = 28076,
    @FirstChoiceAID  integer      = 54300,
    @SpeEdQID        integer      = 28077,
    @moduleid        int          = 1,
    @ActivityID      int          = 321,
    @AvsenderFylkeNr nvarchar(50),
    @CustomerID      int,
    @RunAutocalc     int          = 0
) AS
BEGIN

    -- delete from VOKAL.SynonymResult where SurveyID = @SurveyID
    --SELECT * FROM VOKAL.SynonymResult WHERE SurveyID = @SurveyID --@SurveyID 
    DECLARE @QDB nvarchar(64), @QServer nvarchar(64);

    SELECT @QDB = [QBase], @QServer = [Qserver] FROM [dbo].[Modules] WHERE [moduleid] = @moduleid;

    --select * from [vigo].[vokal_soker_onske] where len(skolenr) = 4

    --declare @AvsenderFylkeNr as nvarchar(50) = '04'
    UPDATE [vigo].[vokal_soker_onske] SET [skolenr] = '0'+[skolenr] WHERE LEN([skolenr]) = 4 AND [fylkesnr - avsender] = @AvsenderFylkeNr;
    UPDATE [vigo].[vokal_soker_program] SET [skolenr] = '0'+[skolenr] WHERE LEN([skolenr]) = 4 AND [fylkesnr - avsender] = @AvsenderFylkeNr;
    UPDATE [vigo].[vokal_soker_fag] SET [skolenr] = '0'+[skolenr] WHERE LEN([skolenr]) = 4 AND [fylkesnr - avsender] = @AvsenderFylkeNr;
    UPDATE [vigo].[vokal_soker_onske] SET [skolenr] = '00'+[skolenr] WHERE LEN([skolenr]) = 3 AND [fylkesnr - avsender] = @AvsenderFylkeNr;
    UPDATE [vigo].[vokal_soker_program] SET [skolenr] = '00'+[skolenr] WHERE LEN([skolenr]) = 3 AND [fylkesnr - avsender] = @AvsenderFylkeNr;
    UPDATE [vigo].[vokal_soker_fag] SET [skolenr] = '00'+[skolenr] WHERE LEN([skolenr]) = 3 AND [fylkesnr - avsender] = @AvsenderFylkeNr;

    DELETE FROM [vigo].[vokal_soker_fag]
    WHERE [karakter - standpunkt] IS NULL OR [karakter - standpunkt] = '' AND [fylkesnr - avsender] = @AvsenderFylkeNr;
    --Delete  tmp.vokal_soker_program where skolenr = '02099' and karakterantall = 0 and [fylkesnr - avsender] = @AvsenderFylkeNr and @AvsenderFylkeNr = '02' /* Only for AFK */
    --Delete from tmp.vokal_soker_program where karakterantall = 0 and [fylkesnr - avsender] = @AvsenderFylkeNr

    --declare @AvsenderFylkeNr as nvarchar(50) = '04'
    UPDATE [vigo].[vokal_soker_program] SET [karakterAntall] = (SELECT COUNT(*)
                                                                FROM [vigo].[vokal_soker_fag] [f]
                                                                WHERE [f].[fødselsnummer] = [vigo].[vokal_soker_program].[fødselsnummer]
                                                                  AND [f].[skolenr] = [vigo].[vokal_soker_program].[skolenr]
                                                                  AND [f].[skoleår] = [vigo].[vokal_soker_program].[skoleår]
                                                                  AND [f].[programområde] = [vigo].[vokal_soker_program].[programområde]
                                                                  AND [f].[fylkesnr - avsender] = [vigo].[vokal_soker_program].[fylkesnr - avsender]
                                                                  AND [f].[fylkesnr - avsender] = @AvsenderFylkeNr
                                                                )
    WHERE [fylkesnr - avsender] = @AvsenderFylkeNr;

    DELETE [vigo].[vokal_soker_program]
    WHERE [skolenr] = '02099' AND [karakterantall] = 0 AND [fylkesnr - avsender] = @AvsenderFylkeNr AND @AvsenderFylkeNr = '02'; /* Only for AFK */

    /* The delete below has been removed by GFP */
    --Delete from tmp.vokal_soker_program where karakterantall = 0 and [karakterpoengsum] = 0 and [fylkesnr - avsender] = @AvsenderFylkeNr

    ----declare @AvsenderFylkeNr as nvarchar(50) = '04'
    ----select * ,
    ----  karakterAntall = (Select count(*) from  [vigo].[vokal_soker_fag] f 
    ----     where f.fødselsnummer = [vigo].[vokal_soker_program].fødselsnummer and f.skolenr = [vigo].[vokal_soker_program].skolenr and f.skoleår = [vigo].[vokal_soker_program].skoleår 
    ----	  and f.programområde =  [vigo].[vokal_soker_program].programområde  and f.[fylkesnr - avsender] = [vigo].[vokal_soker_program].[fylkesnr - avsender] and f.[fylkesnr - avsender] = @AvsenderFylkeNr)
    ----  from [vigo].[vokal_soker_program]
    ----  where [fylkesnr - avsender] = @AvsenderFylkeNr

    --declare @AvsenderFylkeNr as nvarchar(50) = '04'
    DELETE
    --declare @AvsenderFylkeNr as nvarchar(50) = '04'   select *
    FROM [vigo].[vokal_soker_program]
    WHERE [fødselsnummer] IN (SELECT [fødselsnummer]
                              FROM [vigo].[vokal_soker_program]
                              WHERE [programområde] = 'GSGSK0----' AND [fylkesnr - avsender] = @AvsenderFylkeNr
                              GROUP BY [fødselsnummer]
                              HAVING COUNT(*) > 1
                             )
      AND [programområde] = 'GSGSK0----'
      AND [karakterantall] < 4
      AND [fylkesnr - avsender] = @AvsenderFylkeNr;

    -- Delete first GSK row if student has more than one
    --declare @AvsenderFylkeNr as nvarchar(50) = '04'
    DELETE FROM [vigo].[vokal_soker_program]
    FROM [vigo].[vokal_soker_program] [p]
    JOIN (
            --declare @AvsenderFylkeNr as nvarchar(50) = '09'
            SELECT [fødselsnummer], MIN([skoleår]) [skoleår]
            FROM [vigo].[vokal_soker_program]
            WHERE [programområde] = 'GSGSK0----' AND [fylkesnr - avsender] = @AvsenderFylkeNr
            GROUP BY [fødselsnummer]
            HAVING COUNT(*) > 1 AND MAX([skoleår]) <> MIN([skoleår])
         ) AS [pg] ON [pg].[fødselsnummer] = [p].[fødselsnummer] AND [pg].[skoleår] = [p].[skoleår] AND [fylkesnr - avsender] = @AvsenderFylkeNr;

    /* Rows for subjects (vokal_soker_fag) are deleted if there is no matching program (vokal_soker_program), but those may have been deleted above */
    --declare @AvsenderFylkeNr as nvarchar(50) = '17'
    DELETE FROM [vigo].[vokal_soker_fag]
    --declare @AvsenderFylkeNr as nvarchar(50) = '09'  select *
    FROM [vigo].[vokal_soker_fag] [f]
    LEFT OUTER JOIN [vigo].[vokal_soker_program] [p] ON [p].[fødselsnummer] = [f].[fødselsnummer]
                                                    AND [p].[skolenr] = [f].[skolenr]
                                                    AND [p].[skoleår] = [f].[skoleår]
                                                    AND [p].[fylkesnr - avsender] = [f].[fylkesnr - avsender]
                                                    AND [f].[fylkesnr - avsender] = @AvsenderFylkeNr
    WHERE [p].[skolenr] IS NULL AND [f].[fylkesnr - avsender] = @AvsenderFylkeNr AND [f].[nivå] = '0' AND [f].[programområde] = 'GSGSK0----'; /* Criteria added by GFP */
    --and f.fødselsnummer = 'xxxx'

    --declare @AvsenderFylkeNr as nvarchar(50) = '04'
    DELETE FROM [vigo].[vokal_soker_fag]
    WHERE [ID] IN (--declare @AvsenderFylkeNr as nvarchar(50) = '04'
                    SELECT MIN([id])
                    FROM [vigo].[vokal_soker_fag]
                    WHERE [programområde] = 'GSGSK0----' AND [fylkesnr - avsender] = @AvsenderFylkeNr
                    GROUP BY [fødselsnummer], [fagkode], [skoleår]
                    HAVING COUNT(*) > 1
                  )
      AND [fylkesnr - avsender] = @AvsenderFylkeNr;

    -- Insert Grades
    INSERT INTO [dbo].[Answer] ([ResultID], [QuestionID], [AlternativeID], value, [customerID], [createdby], [lastupdatedby], [created], [lastupdated])
    --declare @AvsenderFylkeNr as nvarchar(50) = '04', @CustomerID int = 497, @SurveyID as integer = 2874, @GradeScaleID  integer = 3980, @YesNoScaleID integer = 3978, @GradeQID  integer = 28075
    SELECT DISTINCT [r].[resultid], @GradeQID, [a].[AlternativeID],
                    CASE
                        WHEN ISNUMERIC([f].[karakter - standpunkt]) = 1 THEN [f].[karakter - standpunkt]
                        ELSE 0
                    END, @CustomerID, 883174, 883174, GETDATE(), GETDATE()
    FROM [vigo].[vokal_soker] [s]
    JOIN [org].[User] [u] WITH (nolock) ON [s].[fødselsnummer] = [u].[ssn] AND [u].[EntityStatusID] IN (1, 2) AND [u].[CustomerID] = @CustomerID AND u.[Deleted] IS NULL
    JOIN [vigo].[vokal_soker_fag] [f] ON [f].[fødselsnummer] = [s].[fødselsnummer]
                                     AND [karakter - standpunkt] IS NOT NULL
                                     AND [f].[programområde] = 'GSGSK0----'
                                     AND [f].[fylkesnr - avsender] = @AvsenderFylkeNr
    JOIN [at].[Alternative] [a] WITH (nolock) ON [a].[scaleid] = @GradeScaleID AND [a].[extid] LIKE '%'+[f].[fagkode]+'%'
    JOIN [dbo].[Result] [r] WITH (nolock) ON [r].[SurveyID] = @SurveyID AND [r].[UserID] = [u].[userid] AND [r].[EntityStatusID] = 2 AND [r].[Created] > GETDATE() - 3 AND r.[Deleted] IS NULL
    WHERE [s].[fylkesnr - avsender] = @AvsenderFylkeNr
      AND NOT EXISTS (SELECT 1
                      FROM [dbo].[Answer] [a2]
                      WHERE [a2].[ResultID] = [r].[ResultID] AND [a2].[QuestionID] = @GradeQID AND [a2].[AlternativeID] = [a].[AlternativeID]
                     );

    -- Insert Absence and grade. avg
    INSERT INTO [dbo].[Answer] ([ResultID], [QuestionID], [AlternativeID], value, [customerID], [createdby], [lastupdatedby], [created], [lastupdated])
    --declare @AvsenderFylkeNr as nvarchar(50) = '04', @CustomerID int = 497, @SurveyID as integer = 2874, @GradeScaleID  integer = 3980, @YesNoScaleID integer = 3978, @GradeQID  integer = 28075
    SELECT DISTINCT [r].[resultid], @GradeQID, [a].[AlternativeID], ISNULL([p].[fravær - dager], 0), @CustomerID, 883174, 883174, GETDATE(), GETDATE()
    FROM [vigo].[vokal_soker_program] [p]
    JOIN [org].[User] [u] ON [p].[fødselsnummer] = [u].[ssn] AND [u].[EntityStatusID] IN (1, 2) AND [p].[programområde] = 'GSGSK0----' AND [u].[CustomerID] = @CustomerID AND u.[Deleted] IS NULL
    JOIN [at].[Alternative] [a] ON [a].[scaleid] = @GradeScaleID AND [a].[extid] = 'Absence_D'
    JOIN [dbo].[Result] [r] ON [r].[SurveyID] = @SurveyID AND [r].[UserID] = [u].[userid] AND [r].[EntityStatusID] = 2 AND [r].[Created] > GETDATE() - 3 AND r.[Deleted] IS NULL
    WHERE [p].[fylkesnr - avsender] = @AvsenderFylkeNr
      AND NOT EXISTS (SELECT 1
                      FROM [dbo].[Answer] [a2]
                      WHERE [a2].[ResultID] = [r].[ResultID] AND [a2].[QuestionID] = @GradeQID AND [a2].[AlternativeID] = [a].[AlternativeID]
                     );

    INSERT INTO [dbo].[Answer] ([ResultID], [QuestionID], [AlternativeID], value, [customerID], [createdby], [lastupdatedby], [created], [lastupdated])
    --declare @AvsenderFylkeNr as nvarchar(50) = '04', @CustomerID int = 497, @SurveyID as integer = 2874, @GradeScaleID  integer = 3980, @YesNoScaleID integer = 3978, @GradeQID  integer = 28075
    SELECT [r].[resultid], @GradeQID, [a].[AlternativeID], isnull([p].[fravær - timer], 0), @CustomerID, 883174, 883174, GETDATE(), GETDATE()
    FROM [vigo].[vokal_soker_program] [p]
    JOIN [org].[User] [u] ON [p].[fødselsnummer] = [u].[ssn] AND [u].[EntityStatusID] IN (1, 2) AND [p].[programområde] = 'GSGSK0----' AND [u].[CustomerID] = @CustomerID AND u.[Deleted] IS NULL
    JOIN [at].[Alternative] [a] ON [a].[scaleid] = @GradeScaleID AND [a].[extid] = 'Absence_T'
    JOIN [dbo].[Result] [r] ON [r].[SurveyID] = @SurveyID AND [r].[UserID] = [u].[userid] AND [r].[EntityStatusID] = 2 AND [r].[Created] > GETDATE() - 3 AND r.[Deleted] IS NULL
    WHERE [p].[fylkesnr - avsender] = @AvsenderFylkeNr
      AND NOT EXISTS (SELECT 1
                      FROM [dbo].[Answer] [a2]
                      WHERE [a2].[ResultID] = [r].[ResultID] AND [a2].[QuestionID] = @GradeQID AND [a2].[AlternativeID] = [a].[AlternativeID]
                     );

    INSERT INTO [dbo].[Answer] ([ResultID], [QuestionID], [AlternativeID], value, [customerID], [createdby], [lastupdatedby], [created], [lastupdated])
    --declare @AvsenderFylkeNr as nvarchar(50) = '04', @CustomerID int = 497, @SurveyID as integer = 2874, @GradeScaleID  integer = 3980, @YesNoScaleID integer = 3978, @GradeQID  integer = 28075
    SELECT [r].[resultid], @GradeQID, [a].[AlternativeID], REPLACE([p].[karakterpoengsum], ',', '.'), @CustomerID, 883174, 883174, GETDATE(), GETDATE()
    FROM [vigo].[vokal_soker_program] [p]
    JOIN [org].[User] [u] ON [p].[fødselsnummer] = [u].[ssn] AND [u].[EntityStatusID] IN (1, 2) AND [p].[programområde] = 'GSGSK0----' AND [u].[CustomerID] = @CustomerID AND u.[Deleted] IS NULL
    JOIN [at].[Alternative] [a] ON [a].[scaleid] = @GradeScaleID AND [a].[extid] = 'AvgGrade'
    JOIN [dbo].[Result] [r] ON [r].[SurveyID] = @SurveyID AND [r].[UserID] = [u].[userid] AND [r].[EntityStatusID] = 2 AND [r].[Created] > GETDATE() - 3 AND r.[Deleted] IS NULL
    WHERE [p].[fylkesnr - avsender] = @AvsenderFylkeNr
            AND NOT EXISTS (SELECT 1
                            FROM [dbo].[Answer] [a2]
                            WHERE [a2].[ResultID] = [r].[ResultID] AND [a2].[QuestionID] = @GradeQID AND [a2].[AlternativeID] = [a].[AlternativeID]
                            );

    -- Choice number
    INSERT INTO [dbo].[Answer] ([ResultID], [QuestionID], [AlternativeID], value, [customerID], [createdby], [lastupdatedby], [created], [lastupdated])
    --declare @AvsenderFylkeNr as nvarchar(50) = '04', @CustomerID int = 497, @SurveyID as integer = 2874, @GradeScaleID  integer = 3980, @YesNoScaleID integer = 3978, @GradeQID  integer = 28075
    SELECT [r].[resultid], @GradeQID, [a].[alternativeid], [o].ønskenr, @CustomerID, 883174, 883174, GETDATE(), GETDATE()
    FROM [vigo].[vokal_soker_onske] [o]
    JOIN [vigo].[vokal_skole] [s] ON [s].[skolenummer] = [o].[skolenr]
    JOIN [org].[User] [u] ON [o].[fødselsnummer] = [u].[ssn] AND [u].[EntityStatusID] IN (1, 2) AND [u].[tag] = [o].[programområde] /* TAG!!! */ AND [u].[CustomerID] = @CustomerID AND u.[Deleted] IS NULL
    JOIN [org].[Department] [d] ON [d].[DepartmentID] = [u].[DepartmentID]
    JOIN [org].[H_D] [hd] ON [hd].[departmentid] = [d].[DepartmentID]
    JOIN [org].[H_D] [hd2] ON [hd2].[hdid] = [hd].[ParentID]
    JOIN [org].[Department] [d2] ON [hd2].[departmentid] = [d2].[DepartmentID] AND [d2].[orgno] = [s].[orgnr]
    JOIN [at].[Alternative] [a] ON [a].[scaleid] = @GradeScaleID AND [a].[extid] = 'Priority'
    JOIN [dbo].[Result] [r] ON [r].[SurveyID] = @SurveyID AND [r].[UserID] = [u].[userid]
     AND [r].[EntityStatusID] = 2 AND r.[Deleted] IS NULL AND [r].[Created] > GETDATE() - 3 AND [r].[CustomerID] = @CustomerID
    WHERE [o].[fylkesnr - avsender] = @AvsenderFylkeNr
      AND NOT EXISTS (SELECT 1
                      FROM [dbo].[Answer] [a2]
                      WHERE [a2].[ResultID] = [r].[ResultID] AND [a2].[QuestionID] = @GradeQID AND [a2].[AlternativeID] = [a].[AlternativeID]
                     );

    --First choice

    INSERT INTO [dbo].[Answer] ([ResultID], [QuestionID], [AlternativeID], value, [customerID], [createdby], [lastupdatedby], [created], [lastupdated])
    --declare @AvsenderFylkeNr as nvarchar(50) = '05', @CustomerID int = 495, @SurveyID as integer = 4206, @GradeScaleID  integer = 3980, @YesNoScaleID integer = 3978, @GradeQID  integer = 28075, @FirstChoiceQID integer = 28076, @FirstChoiceAID integer = 54300
    SELECT [r].[resultid], @FirstChoiceQID,
                            CASE
                                WHEN isnull([a].value, 0) = 1 THEN 16487
                                ELSE 16488
                            END,
                            CASE
                                WHEN isnull([a].value, 0) = 1 THEN 1
                                ELSE 2
                            END, @CustomerID, 883174, 883174, GETDATE(), GETDATE()
    FROM [dbo].[Result] [r]
    LEFT OUTER JOIN [dbo].[Answer] [a] ON [a].[ResultID] = [r].[resultid] AND [a].[QuestionID] = @GradeQID AND [a].[AlternativeID] = @FirstChoiceAID
    WHERE [r].[SurveyID] = @SurveyID
      AND [r].[CustomerID] = @CustomerID --and r.EntityStatusID = 2 and r.Created  > getdate() -3 
      AND NOT EXISTS (SELECT 1 FROM [dbo].[Answer] [a2] WHERE [a2].[ResultID] = [r].[ResultID] AND [a2].[QuestionID] = @FirstChoiceQID);

    --Special 
    INSERT INTO [dbo].[Answer] ([ResultID], [QuestionID], [AlternativeID], value, [customerID], [createdby], [lastupdatedby], [created], [lastupdated])
    --declare @AvsenderFylkeNr as nvarchar(50) = '04', @CustomerID integer = 497, @SurveyID as integer = 2874, @GradeScaleID integer = 3980, @YesNoScaleID integer = 3978, @GradeQID integer = 28075, @FirstChoiceQID integer = 28076, @FirstChoiceAID integer = 54300, @SpeEdQID integer = 28077
    SELECT [r].[resultid], @SpeEdQID, [a].[AlternativeID], [a].Value, @CustomerID, 883174, 883174, GETDATE(), GETDATE()
    FROM [vigo].[vokal_soker] [s]
    JOIN [org].[User] [u] ON [s].[fødselsnummer] = [u].[ssn] AND [u].[EntityStatusID] IN (1, 2) AND [u].[CustomerID] = @CustomerID AND u.[Deleted] IS NULL
    JOIN [at].[Alternative] [a] ON [a].[scaleid] = @YesNoScaleID
     AND [a].[extid] = (CASE
                         WHEN ( (
                                --          Akershus (02): Elever med M i internkodefelt 2 skal identifiseres som «har tidligere hatt spesialundervisning»
                                --          Aust-Agder (09): registrerer ikke om elevene tidligere har hatt spesialundervisning, og kan derfor ikke identifisere elever etter dette kriteriet
                                --          Hedmark (04): ?
                                --          Oppland (05): Elever med internkode 11 skal identifiseres som «har tidligere hatt spesialundervisning». 
                                --          Nord-Trøndelag (17): Elever med S i internkodefelt 10 skal identifiseres som «har tidligere hatt spesialundervisning»

                                [s].[spesialundervisning vurderes] = 'M' AND @AvsenderFylkeNr = '02'
                                ) -- Akershus
                                OR ([s].[spesialundervisning vurderes] <> '' AND @AvsenderFylkeNr = '05'
                                    ) --Oppland
                                OR ([s].[spesialundervisning vurderes] = 'S' AND @AvsenderFylkeNr = '50'
                                    ) --Nord-Trøndelag
                                ) THEN 'J'
                        ELSE 'N'
                       END)
    JOIN [dbo].[Result] [r] ON [r].[SurveyID] = @SurveyID AND [r].[UserID] = [u].[userid]
     AND [r].[EntityStatusID] = 2 AND r.[Deleted] IS NULL AND [r].[Created] > GETDATE() - 3 AND [r].[CustomerID] = @CustomerID
    WHERE [s].[fylkesnr - avsender] = @AvsenderFylkeNr
      AND NOT EXISTS (SELECT 1
                      FROM [dbo].[Answer] [a2]
                      WHERE [a2].[ResultID] = [r].[ResultID] AND [a2].[QuestionID] = @SpeEdQID AND [a2].[AlternativeID] = [a].[AlternativeID]
                     );

    ----/* Check Grunnleggende ferdigheter activity and set checkboxes under "Grunnleggende ferdigheter" section in IKO*/
    ----exec [vigo].[prc_IKO_GrunnleggendeFerdigheter] 2044,2380,28092,3982
/*
--Conduct
insert into VOKAL.SynonymAnswer(ResultID,QuestionID,AlternativeID,Free, customerID, createdby, lastupdatedby, created, lastupdated)   
--declare @AvsenderFylkeNr as nvarchar(50) = '04', @CustomerID int = 497, @SurveyID as integer = 4206, @GradeScaleID  integer = 3980, @YesNoScaleID integer = 3978, @GradeQID  integer = 28075
SELECT r.resultid,@GradeQID,a.AlternativeID, p.[karakter - atferd], @CustomerID, 883174, 883174, getdate(), getdate()
FROM [vigo].[vokal_soker_program] p
join  VOKAL.Synonymuser u on p.fødselsnummer = u.ssn and u.EntityStatusID IN (1,2) and p.programområde = 'GSGSK0----' and u.CustomerID = @CustomerID
Join  VOKAL.SynonymAlternative a on a.scaleid = @GradeScaleID and a.extid = 'GradeConduct'
Join VOKAL.SynonymResult r on r.SurveyID = @SurveyID and r.UserID = u.userid and r.EntityStatusID = 2 and r.Created  > getdate() -3 
where p.[fylkesnr - avsender] = @AvsenderFylkeNr

 
--Order
insert into VOKAL.SynonymAnswer(ResultID,QuestionID,AlternativeID,Free, customerID, createdby, lastupdatedby, created, lastupdated)   
--declare @AvsenderFylkeNr as nvarchar(50) = '04', @CustomerID int = 497, @SurveyID as integer = 4206, @GradeScaleID  integer = 3980, @YesNoScaleID integer = 3978, @GradeQID  integer = 28075
SELECT r.resultid,@GradeQID,a.AlternativeID, p.[karakter - orden], @CustomerID, 883174, 883174, getdate(), getdate()
FROM [vigo].[vokal_soker_program] p
join  VOKAL.Synonymuser u on p.fødselsnummer = u.ssn and u.EntityStatusID IN (1,2) and p.programområde = 'GSGSK0----' and u.CustomerID = @CustomerID
Join  VOKAL.SynonymAlternative a on a.scaleid = @GradeScaleID and a.extid = 'GradeOrder'
Join VOKAL.SynonymResult r on r.SurveyID = @SurveyID and r.UserID = u.userid and r.EntityStatusID = 2 and r.Created  > getdate() -3 
where p.[fylkesnr - avsender] = @AvsenderFylkeNr

*/
    --Count IV
    INSERT INTO [dbo].[Answer] ([ResultID], [QuestionID], [AlternativeID], value, [customerID], [createdby], [lastupdatedby], [created], [lastupdated])
    --declare @AvsenderFylkeNr as nvarchar(50) = '09', @CustomerID int = 497, @SurveyID as integer = 4206, @GradeScaleID  integer = 3980, @YesNoScaleID integer = 3978, @GradeQID  integer = 28075
    SELECT [r].[resultid], @GradeQID, [a].[AlternativeID], (SELECT SUM(CASE
                                                                            WHEN [vsf].[karakter - standpunkt] = 'IV' THEN 1
                                                                            ELSE 0
                                                                        END)
                                                            FROM [vigo].[vokal_soker_fag] [vsf]
                                                            WHERE [p].[fylkesnr - avsender] = [vsf].[fylkesnr - avsender]
                                                              AND [p].[fødselsnummer] = [vsf].[fødselsnummer]
                                                              AND [p].[programområde] = [vsf].[programområde]
                                                            GROUP BY [vsf].[fødselsnummer]
                                                            ), @CustomerID, 883174, 883174, GETDATE(), GETDATE()
    FROM [vigo].[vokal_soker_program] [p]
    JOIN [org].[User] [u] ON [p].[fødselsnummer] = [u].[ssn] AND [u].[EntityStatusID] IN (1, 2) AND [p].[programområde] = 'GSGSK0----' AND [u].[CustomerID] = @CustomerID AND u.[Deleted] IS NULL
    JOIN [at].[Alternative] [a] ON [a].[scaleid] = @GradeScaleID AND [a].[extid] = 'Count_IV'
    JOIN [dbo].[Result] [r] ON [r].[SurveyID] = @SurveyID AND [r].[UserID] = [u].[userid] AND [r].[EntityStatusID] = 2 AND [r].[Created] > GETDATE() - 3 AND r.[Deleted] IS NULL
    WHERE [p].[fylkesnr - avsender] = @AvsenderFylkeNr
      AND NOT EXISTS (SELECT 1
                      FROM [dbo].[Answer] [a2]
                      WHERE [a2].[ResultID] = [r].[ResultID] AND [a2].[QuestionID] = @GradeQID AND [a2].[AlternativeID] = [a].[AlternativeID]
                     );

    --Count 1
    INSERT INTO [dbo].[Answer] ([ResultID], [QuestionID], [AlternativeID], value, [customerID], [createdby], [lastupdatedby], [created], [lastupdated])
    --declare @AvsenderFylkeNr as nvarchar(50) = '09', @CustomerID int = 497, @SurveyID as integer = 4206, @GradeScaleID  integer = 3980, @YesNoScaleID integer = 3978, @GradeQID  integer = 28075
    SELECT [r].[resultid], @GradeQID, [a].[AlternativeID], (SELECT SUM(CASE
                                                                            WHEN [vsf].[karakter - standpunkt] = '1' THEN 1
                                                                            ELSE 0
                                                                        END)
                                                            FROM [vigo].[vokal_soker_fag] [vsf]
                                                            WHERE [p].[fylkesnr - avsender] = [vsf].[fylkesnr - avsender]
                                                              AND [p].[fødselsnummer] = [vsf].[fødselsnummer]
                                                              AND [p].[programområde] = [vsf].[programområde]
                                                            GROUP BY [vsf].[fødselsnummer]
                                                            ), @CustomerID, 883174, 883174, GETDATE(), GETDATE()
    FROM [vigo].[vokal_soker_program] [p]
    JOIN [org].[User] [u] ON [p].[fødselsnummer] = [u].[ssn] AND [u].[EntityStatusID] IN (1, 2) AND [p].[programområde] = 'GSGSK0----' AND [u].[CustomerID] = @CustomerID AND u.[Deleted] IS NULL
    JOIN [at].[Alternative] [a] ON [a].[scaleid] = @GradeScaleID AND [a].[extid] = 'Count_1'
    JOIN [dbo].[Result] [r] ON [r].[SurveyID] = @SurveyID AND [r].[UserID] = [u].[userid] AND [r].[EntityStatusID] = 2 AND [r].[Created] > GETDATE() - 3 AND r.[Deleted] IS NULL
    WHERE [p].[fylkesnr - avsender] = @AvsenderFylkeNr
      AND NOT EXISTS (SELECT 1
                      FROM [dbo].[Answer] [a2]
                      WHERE [a2].[ResultID] = [r].[ResultID] AND [a2].[QuestionID] = @GradeQID AND [a2].[AlternativeID] = [a].[AlternativeID]
                     );

    --Count 2
    INSERT INTO [dbo].[Answer] ([ResultID], [QuestionID], [AlternativeID], value, [customerID], [createdby], [lastupdatedby], [created], [lastupdated])
    --declare @AvsenderFylkeNr as nvarchar(50) = '09', @CustomerID int = 497, @SurveyID as integer = 4206, @GradeScaleID  integer = 3980, @YesNoScaleID integer = 3978, @GradeQID  integer = 28075
    SELECT [r].[resultid], @GradeQID, [a].[AlternativeID], (SELECT SUM(CASE
                                                                            WHEN [vsf].[karakter - standpunkt] = '2' THEN 1
                                                                            ELSE 0
                                                                        END)
                                                            FROM [vigo].[vokal_soker_fag] [vsf]
                                                            WHERE [p].[fylkesnr - avsender] = [vsf].[fylkesnr - avsender]
                                                              AND [p].[fødselsnummer] = [vsf].[fødselsnummer]
                                                              AND [p].[programområde] = [vsf].[programområde]
                                                            GROUP BY [vsf].[fødselsnummer]
                                                            ), @CustomerID, 883174, 883174, GETDATE(), GETDATE()
    FROM [vigo].[vokal_soker_program] [p]
    JOIN [org].[User] [u] ON [p].[fødselsnummer] = [u].[ssn] AND [u].[EntityStatusID] IN (1, 2) AND [p].[programområde] = 'GSGSK0----' AND [u].[CustomerID] = @CustomerID AND u.[Deleted] IS NULL
    JOIN [at].[Alternative] [a] ON [a].[scaleid] = @GradeScaleID AND [a].[extid] = 'Count_2'
    JOIN [dbo].[Result] [r] ON [r].[SurveyID] = @SurveyID AND [r].[UserID] = [u].[userid] AND [r].[EntityStatusID] = 2 AND [r].[Created] > GETDATE() - 3 AND r.[Deleted] IS NULL
    WHERE [p].[fylkesnr - avsender] = @AvsenderFylkeNr
      AND NOT EXISTS (SELECT 1
                      FROM [dbo].[Answer] [a2]
                      WHERE [a2].[ResultID] = [r].[ResultID] AND [a2].[QuestionID] = @GradeQID AND [a2].[AlternativeID] = [a].[AlternativeID]
                     );

    --declare @SurveyID as integer = 2874, @CustomerID int = 497
    UPDATE [dbo].[Result] SET [EntityStatusID] = 1, [EndDate] = GETDATE()
    WHERE [SurveyID] = @SurveyID AND [EntityStatusID] = 2 AND [Created] > GETDATE() - 3 AND [CustomerID] = @CustomerID AND [Deleted] IS NULL;

    --Create AutoCalc job for given activity and survey
    IF @RunAutocalc = 1
    BEGIN
        PRINT 'Creating BPS calc job (but deleting some calculated D-answers first, Q=28097 A=16489)';
        DELETE FROM [dbo].[Answer]
        WHERE [questionid] = 28097 AND [AlternativeID] = 16489 AND [resultid] IN (SELECT [resultid] FROM [dbo].[Result] WHERE [surveyid] = @SurveyID);

        EXEC [job].[CreateAutoCalcJob] 9, 883174, @QServer, @QDB, @ActivityID, @SurveyID, 'vokalint', '';
    END;
END;
GO