SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[prc_UpdateDepartmentTypeOnTeachingroups]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [dbo].[prc_UpdateDepartmentTypeOnTeachingroups] AS' 
END
GO
/*  2018-07-06 Ray:     Replace UG_U by UGMember
*/
ALTER PROCEDURE [dbo].[prc_UpdateDepartmentTypeOnTeachingroups]
AS
BEGIN
    SET NOCOUNT ON
    DECLARE @PeriodID INT, @TableType_User INT, @TableType_Department INT, @EntityStatus_Active INT;

    SELECT @TableType_User = T.TableTypeID FROM TableType T WHERE T.Name = 'UserType'
    SELECT @TableType_Department = T.TableTypeID FROM TableType T WHERE T.Name = 'DepartmentType'

    SELECT TOP 1 @PeriodID = P.PeriodID
    FROM at.Period P
    WHERE P.Startdate <= GETDATE() AND GETDATE() <= P.Enddate
    ORDER BY P.Enddate DESC

    SELECT @EntityStatus_Active = es.EntityStatusID FROM dbo.EntityStatus es WHERE es.CodeName LIKE 'Active';

    SELECT ug.[UserGroupID], ug.[Name], dtug.[DepartmentTypeID], lt.[Name] AS [DepartmentType], 'Before Fix' AS [Description]
    FROM [org].[UserGroup] ug
    LEFT JOIN [org].[DT_UG] dtug ON dtug.[UserGroupID] = ug.[UserGroupID]
    LEFT JOIN [org].[LT_DepartmentType] lt ON lt.[DepartmentTypeID] = dtug.[DepartmentTypeID] AND lt.[LanguageID] = 1
    WHERE ug.[PeriodID] = @PeriodID AND ug.[UsergroupTypeID] = 1
    ORDER BY ug.[UserGroupID]
 
 
    RAISERROR ('Remove department types of teaching groups where no learner''s levels match', 0, 1) WITH NOWAIT
    DELETE dtug
    FROM [org].[DT_UG] dtug
    JOIN [org].[UserGroup] ug ON dtug.[UserGroupID] = ug.[UserGroupID] AND ug.[PeriodID] = @PeriodID AND ug.[UsergroupTypeID] = 1
     AND ug.[EntityStatusID] = @EntityStatus_Active AND ug.[Deleted] IS NULL
    WHERE NOT EXISTS (SELECT 1 FROM [ObjectMapping] om
                               JOIN [org].[UserType] ut ON om.[FromTableTypeID] = @TableType_Department AND om.[ToTableTypeID] = @TableType_User
                                AND om.[FromID] = dtug.[DepartmentTypeID] AND ut.[UserTypeID] = om.[ToID]
                               JOIN [org].[UT_U] utu ON ut.[UserTypeID] = utu.[UserTypeID]
                               JOIN [org].[UGMember] ugm ON utu.[UserID] = ugm.[UserID] AND ugm.[UserGroupID] = ug.[UserGroupID]
                                AND ugm.[EntityStatusID] = @EntityStatus_Active AND ugm.[Deleted] IS NULL
                               JOIN [org].[User] u ON ugm.[UserID] = u.[UserID] AND u.[EntityStatusID] = @EntityStatus_Active AND u.[Deleted] IS NULL)
    RAISERROR ('%d rows deleted', 0, 1, @@rowcount) WITH NOWAIT
 
    RAISERROR ('Add department types for teaching groups based on learner''s levels', 0, 1) WITH NOWAIT
    INSERT INTO [org].[DT_UG] ([DepartmentTypeID], [UserGroupID])
    SELECT DISTINCT om.[ToID], ug.[UserGroupID]
    FROM [org].[UserGroup] ug
    JOIN [org].[UGMember] ugm ON ug.[UserGroupID] = ugm.[UserGroupID] AND ug.[PeriodID] = @PeriodID AND ug.[UsergroupTypeID] = 1
     AND ugm.[EntityStatusID] = @EntityStatus_Active AND ugm.[Deleted] IS NULL
     AND ug.[EntityStatusID] = @EntityStatus_Active AND ug.[Deleted] IS NULL
    JOIN [org].[UT_U] utu ON ugm.[UserID] = utu.[UserID]
    JOIN [org].[User] u ON ugm.[UserID] = u.[UserID] AND u.[EntityStatusID] = @EntityStatus_Active AND u.[Deleted] IS NULL
    JOIN [ObjectMapping] om ON om.[FromTableTypeID] = @TableType_User AND om.[ToTableTypeID] = @TableType_Department AND om.[FromID] = utu.[UserTypeID]
    WHERE NOT EXISTS (SELECT 1 FROM [org].[DT_UG] dtug WHERE dtug.[DepartmentTypeID] = om.[ToID] AND dtug.[UserGroupID] = ug.[UserGroupID])
    RAISERROR ('%d rows added', 0, 1, @@rowcount) WITH NOWAIT
 
    SELECT ug.[UserGroupID], ug.[Name], dtug.[DepartmentTypeID], lt.[Name] AS [DepartmentType], 'After Fix' AS [Description], hd.[PathName]
    FROM [org].[UserGroup] ug
    JOIN [org].[H_D] hd ON ug.[DepartmentID] = hd.[DepartmentID]
    LEFT JOIN [org].[DT_UG] dtug ON dtug.[UserGroupID] = ug.[UserGroupID]
    LEFT JOIN [org].[LT_DepartmentType] lt ON lt.[DepartmentTypeID] = dtug.[DepartmentTypeID] AND lt.[LanguageID] = 1
    WHERE ug.[PeriodID] = @PeriodID AND ug.[UsergroupTypeID] = 1
    ORDER BY ug.[UserGroupID]
END

GO
