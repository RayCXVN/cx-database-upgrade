SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[vigo].[prc_IKO_VG3]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [vigo].[prc_IKO_VG3] AS' 
END
GO
ALTER PROC [vigo].[prc_IKO_VG3](
    @SurveyID        integer,
    @GradeScaleID    integer      = 77537,
    @YesNoScaleID    integer      = 77535,
    @GradeQID        integer      = 335769,
    @FirstChoiceQID  integer      = 335770,
    @FirstChoiceAID  integer      = 7421850,
    @SpeEdQID        integer      = 335771,
    @moduleid        int          = 1,
    @ActivityID      int          = 1769,
    @AvsenderFylkeNr nvarchar(50),
    @CustomerID      int,
    @RunAutocalc     int          = 0
) AS
BEGIN

    -- delete from VOKAL.SynonymResult where SurveyID = @SurveyID
    --SELECT * FROM VOKAL.SynonymResult WHERE SurveyID = @SurveyID --@SurveyID 
    DECLARE @QDB nvarchar(64), @QServer nvarchar(64);

    SELECT @QDB = [QBase], @QServer = [Qserver] FROM [dbo].[Modules] WHERE [moduleid] = @moduleid;

    --declare @AvsenderFylkeNr as nvarchar(50) = '04'
    UPDATE [vigo].[vokal_soker_onske] SET [skolenr] = '0'+[skolenr] WHERE LEN([skolenr]) = 4 AND [fylkesnr - avsender] = @AvsenderFylkeNr;
    UPDATE [vigo].[vokal_soker_program] SET [skolenr] = '0'+[skolenr] WHERE LEN([skolenr]) = 4 AND [fylkesnr - avsender] = @AvsenderFylkeNr;
    UPDATE [vigo].[vokal_soker_fag] SET [skolenr] = '0'+[skolenr] WHERE LEN([skolenr]) = 4 AND [fylkesnr - avsender] = @AvsenderFylkeNr;
    UPDATE [vigo].[vokal_soker_onske] SET [skolenr] = '00'+[skolenr] WHERE LEN([skolenr]) = 3 AND [fylkesnr - avsender] = @AvsenderFylkeNr;
    UPDATE [vigo].[vokal_soker_program] SET [skolenr] = '00'+[skolenr] WHERE LEN([skolenr]) = 3 AND [fylkesnr - avsender] = @AvsenderFylkeNr;
    UPDATE [vigo].[vokal_soker_fag] SET [skolenr] = '00'+[skolenr] WHERE LEN([skolenr]) = 3 AND [fylkesnr - avsender] = @AvsenderFylkeNr;

    DELETE FROM [vigo].[vokal_soker_fag]
    WHERE [karakter - standpunkt] IS NULL OR [karakter - standpunkt] = '' AND [fylkesnr - avsender] = @AvsenderFylkeNr;
    --Delete  tmp.vokal_soker_program where skolenr = '02099' and karakterantall = 0 and [fylkesnr - avsender] = @AvsenderFylkeNr and @AvsenderFylkeNr = '02' /* Only for AFK */
    --Delete from tmp.vokal_soker_program where karakterantall = 0 and [fylkesnr - avsender] = @AvsenderFylkeNr

    --declare @AvsenderFylkeNr as nvarchar(50) = '04'
    UPDATE [vigo].[vokal_soker_fag] SET [karakter - standpunkt] = (SELECT TOP 1 [karakter - standpunkt]
                                                                  FROM [vigo].[vokal_soker_fag] [f3]
                                                                  WHERE [karakter - standpunkt] NOT IN ('IV', 'D')
                                                                    AND [f3].[skoleår] < [vigo].[vokal_soker_fag].[skoleår]
                                                                    AND [f3].[fagkode] = [vigo].[vokal_soker_fag].[fagkode]
                                                                    AND [f3].[fødselsnummer] = [vigo].[vokal_soker_fag].[fødselsnummer]
                                                                    AND [f3].[fylkesnr - avsender] = @AvsenderFylkeNr
                                                                  ORDER BY [skoleår] DESC
                                                                 )
    WHERE [karakter - standpunkt] = 'IV' AND [programområde] NOT IN ('GSGSK0----')
    AND (SELECT COUNT(*)
        FROM [vigo].[vokal_soker_fag] [f2]
        WHERE [karakter - standpunkt] NOT IN ('IV', 'D')
        AND [f2].[skoleår] < [vigo].[vokal_soker_fag].[skoleår]
        AND [f2].[fagkode] = [vigo].[vokal_soker_fag].[fagkode]
        AND [f2].[fødselsnummer] = [vigo].[vokal_soker_fag].[fødselsnummer]
        AND [f2].[fylkesnr - avsender] = @AvsenderFylkeNr
        ) > 0
    AND [vigo].[vokal_soker_fag].[fylkesnr - avsender] = @AvsenderFylkeNr;

    UPDATE [vigo].[vokal_soker_program] SET [karakterAntall] = (SELECT COUNT(*)
                                                                FROM [vigo].[vokal_soker_fag] [f]
                                                                WHERE [f].[fødselsnummer] = [vigo].[vokal_soker_program].[fødselsnummer]
                                                                AND [f].[skolenr] = [vigo].[vokal_soker_program].[skolenr]
                                                                AND [f].[skoleår] = [vigo].[vokal_soker_program].[skoleår]
                                                                AND [f].[karakter - standpunkt] <> ''
                                                                AND [f].[programområde] = [vigo].[vokal_soker_program].[programområde]
                                                                AND [f].[fylkesnr - avsender] = [vigo].[vokal_soker_program].[fylkesnr - avsender]
                                                                AND [f].[fylkesnr - avsender] = @AvsenderFylkeNr
                                                                )
    WHERE [fylkesnr - avsender] = @AvsenderFylkeNr;

    DECLARE @RCount AS int;
    SET @RCount = 1;

    WHILE @RCount <> 0
    BEGIN
        DELETE FROM [vigo].[vokal_soker_program]
        FROM [vigo].[vokal_soker_program] [p]
        JOIN (SELECT [fødselsnummer], MIN([skoleår]) [skoleår]
                FROM [vigo].[vokal_soker_program]
                WHERE [nivå] = 2 AND [fylkesnr - avsender] = @AvsenderFylkeNr
                GROUP BY [fødselsnummer]
                HAVING COUNT(*) > 1 AND MAX([skoleår]) <> MIN([skoleår])
                ) AS [pg] ON [pg].[fødselsnummer] = [p].[fødselsnummer] AND [pg].[skoleår] = [p].[skoleår]
        WHERE [p].[fylkesnr - avsender] = @AvsenderFylkeNr;

        SET @RCount = @@ROWCOUNT;
    END;

    DELETE FROM [vigo].[vokal_soker_program]
    FROM [vigo].[vokal_soker_program] [p]
    JOIN (SELECT [fødselsnummer], MIN([Karakterantall]) [karantall]
            FROM [vigo].[vokal_soker_program]
            WHERE [nivå] = 2 AND [fylkesnr - avsender] = @AvsenderFylkeNr
            GROUP BY [fødselsnummer]
            HAVING COUNT(*) > 1 AND MAX([Karakterantall]) <> MIN([Karakterantall])
            ) AS [pg] ON [pg].[fødselsnummer] = [p].[fødselsnummer] AND [pg].[karantall] = [p].[Karakterantall]
    WHERE [p].[fylkesnr - avsender] = @AvsenderFylkeNr;

    -- Insert Grades
    INSERT INTO [dbo].[Answer] ([ResultID], [QuestionID], [AlternativeID], value)
    --declare @AvsenderFylkeNr as nvarchar(50) = '09', @CustomerID int = 497, @SurveyID as integer = 4249, @GradeScaleID  integer = 18527, @YesNoScaleID integer = 18525, @GradeQID  integer = 63181
    SELECT DISTINCT [r].[resultid], @GradeQID, [a].[AlternativeID],
                    CASE
                        WHEN ISNUMERIC([f].[karakter - standpunkt]) = 1 THEN [f].[karakter - standpunkt]
                        ELSE 0
                    END
    FROM [vigo].[vokal_soker] [s]
    JOIN [org].[User] [u] ON [s].[fødselsnummer] = [u].[ssn] AND [u].[EntityStatusID] IN (1, 2) AND [u].[CustomerID] = @CustomerID AND u.[Deleted] IS NULL
    JOIN [vigo].[vokal_soker_program] [p] ON [p].[fødselsnummer] = [s].[fødselsnummer] AND [p].[nivå] = 2 AND [p].[fylkesnr - avsender] = @AvsenderFylkeNr
    JOIN [vigo].[vokal_soker_fag] [f] ON [f].[fødselsnummer] = [p].[fødselsnummer] AND [f].[programområde] = [p].[programområde] AND [f].[skoleår] = [p].[skoleår]
     AND [karakter - standpunkt] IS NOT NULL
     AND CASE
             WHEN ISNUMERIC([f].[Nivå]) = 1 THEN CAST([f].[Nivå] AS int)
             ELSE 0
         END = 2
     AND [f].[fylkesnr - avsender] = @AvsenderFylkeNr
    JOIN [at].[Alternative] [a] ON [a].[scaleid] = @GradeScaleID AND [a].[extid] LIKE '%'+[f].[fagkode]+'%'
    JOIN [dbo].[Result] [r] WITH (nolock) ON [r].[SurveyID] = @SurveyID AND [r].[UserID] = [u].[userid] AND [r].[EntityStatusID] = 2 AND [r].[Created] > GETDATE() - 3 AND r.[Deleted] IS NULL
    WHERE [s].[fylkesnr - avsender] = @AvsenderFylkeNr;

    -- Insert Absence and grade. avg
    INSERT INTO [dbo].[Answer] ([ResultID], [QuestionID], [AlternativeID], value)
    SELECT DISTINCT [r].[resultid], @GradeQID, [a].[AlternativeID], ISNULL([p].[fravær - dager], 0)
    FROM [vigo].[vokal_soker_program] [p]
    JOIN [org].[User] [u] ON [p].[fødselsnummer] = [u].[ssn] AND [u].[EntityStatusID] IN (1, 2) AND [p].[nivå] = 2 AND [u].[CustomerID] = @CustomerID AND u.[Deleted] IS NULL
    JOIN [at].[Alternative] [a] ON [a].[scaleid] = @GradeScaleID AND [a].[extid] = 'Absence_D'
    JOIN [dbo].[Result] [r] ON [r].[SurveyID] = @SurveyID AND [r].[UserID] = [u].[userid] AND [r].[EntityStatusID] = 2 AND [r].[Created] > GETDATE() - 3 AND r.[Deleted] IS NULL
    WHERE [p].[fylkesnr - avsender] = @AvsenderFylkeNr;

    INSERT INTO [dbo].[Answer] ([ResultID], [QuestionID], [AlternativeID], value)
    SELECT [r].[resultid], @GradeQID, [a].[AlternativeID], isnull([p].[fravær - timer], 0)
    FROM [vigo].[vokal_soker_program] [p]
    JOIN [org].[User] [u] ON [p].[fødselsnummer] = [u].[ssn] AND [u].[EntityStatusID] IN (1, 2) AND [p].[nivå] = 2 AND [u].[CustomerID] = @CustomerID AND u.[Deleted] IS NULL
    JOIN [at].[Alternative] [a] ON [a].[scaleid] = @GradeScaleID AND [a].[extid] = 'Absence_T'
    JOIN [dbo].[Result] [r] ON [r].[SurveyID] = @SurveyID AND [r].[UserID] = [u].[userid] AND [r].[EntityStatusID] = 2 AND [r].[Created] > GETDATE() - 3 AND r.[Deleted] IS NULL
    WHERE [p].[fylkesnr - avsender] = @AvsenderFylkeNr;

    INSERT INTO [dbo].[Answer] ([ResultID], [QuestionID], [AlternativeID], value)
    SELECT [r].[resultid], @GradeQID, [a].[AlternativeID], REPLACE([p].[karakterpoengsum], ',', '.')
    FROM [vigo].[vokal_soker_program] [p]
    JOIN [org].[User] [u] ON [p].[fødselsnummer] = [u].[ssn] AND [u].[EntityStatusID] IN (1, 2) AND [p].[nivå] = 2 AND [u].[CustomerID] = @CustomerID AND u.[Deleted] IS NULL
    JOIN [at].[Alternative] [a] ON [a].[scaleid] = @GradeScaleID AND [a].[extid] = 'AvgGrade'
    JOIN [dbo].[Result] [r] ON [r].[SurveyID] = @SurveyID AND [r].[UserID] = [u].[userid] AND [r].[EntityStatusID] = 2 AND [r].[Created] > GETDATE() - 3 AND r.[Deleted] IS NULL
    WHERE [p].[fylkesnr - avsender] = @AvsenderFylkeNr;

    -- Choice number
    INSERT INTO [dbo].[Answer] ([ResultID], [QuestionID], [AlternativeID], value)
    SELECT [r].[resultid], @GradeQID, [a].[alternativeid], [o].ønskenr
    FROM [vigo].[vokal_soker_onske] [o]
    JOIN [vigo].[vokal_skole] [s] ON [s].[skolenummer] = [o].[skolenr]
    JOIN [org].[User] [u] ON [o].[fødselsnummer] = [u].[ssn] AND [u].[EntityStatusID] IN (1, 2) AND [u].[tag] = [o].[programområde] /* TAG!!! */ AND [u].[CustomerID] = @CustomerID AND u.[Deleted] IS NULL
    JOIN [org].[Department] [d] ON [d].[DepartmentID] = [u].[DepartmentID]
    JOIN [org].[H_D] [hd] ON [hd].[departmentid] = [d].[DepartmentID]
    JOIN [org].[H_D] [hd2] ON [hd2].[hdid] = [hd].[ParentID]
    JOIN [org].[Department] [d2] ON [hd2].[departmentid] = [d2].[DepartmentID] AND [d2].[orgno] = [s].[orgnr]
    JOIN [at].[Alternative] [a] ON [a].[scaleid] = @GradeScaleID AND [a].[extid] = 'Priority'
    JOIN [dbo].[Result] [r] ON [r].[SurveyID] = @SurveyID AND [r].[UserID] = [u].[userid] AND [r].[EntityStatusID] = 2 AND r.[Deleted] IS NULL
     AND [r].[Created] > GETDATE() - 3 AND [r].[CustomerID] = @CustomerID
    WHERE [o].[fylkesnr - avsender] = @AvsenderFylkeNr;

    --First choice

    INSERT INTO [dbo].[Answer] ([ResultID], [QuestionID], [AlternativeID], value)
    SELECT [r].[resultid], @FirstChoiceQID,
                            CASE
                                WHEN isnull([a].value, 0) = 1 THEN 94276
                                ELSE 94277
                            END,
                            CASE
                                WHEN isnull([a].value, 0) = 1 THEN 1
                                ELSE 2
                            END
    FROM [dbo].[Result] [r]
    LEFT OUTER JOIN [dbo].[Answer] [a] ON [a].[ResultID] = [r].[resultid] AND [a].[QuestionID] = @GradeQID AND [a].[AlternativeID] = @FirstChoiceAID
    WHERE [r].[EntityStatusID] = 2 AND [r].[SurveyID] = @SurveyID AND [r].[Created] > GETDATE() - 3 AND [r].[CustomerID] = @CustomerID AND r.[Deleted] IS NULL;

    --Special 
    INSERT INTO [dbo].[Answer] ([ResultID], [QuestionID], [AlternativeID], value)
    SELECT [r].[resultid], @SpeEdQID, [a].[AlternativeID], [a].Value
    FROM [vigo].[vokal_soker] [s]
    JOIN [org].[User] [u] ON [s].[fødselsnummer] = [u].[ssn] AND [u].[EntityStatusID] IN (1, 2) AND [u].[CustomerID] = @CustomerID AND u.[Deleted] IS NULL
    JOIN [at].[Alternative] [a] ON [a].[scaleid] = @YesNoScaleID
    AND [a].[extid] = (CASE
                        WHEN ( (
                                --          Akershus (02): Elever med M i internkodefelt 2 skal identifiseres som «har tidligere hatt spesialundervisning»
                                --          Aust-Agder (09): registrerer ikke om elevene tidligere har hatt spesialundervisning, og kan derfor ikke identifisere elever etter dette kriteriet
                                --          Hedmark (04): ?
                                --          Oppland (05): Elever med internkode 11 skal identifiseres som «har tidligere hatt spesialundervisning». 
                                --          Nord-Trøndelag (17): Elever med S i internkodefelt 10 skal identifiseres som «har tidligere hatt spesialundervisning»

                                [s].[spesialundervisning vurderes] = 'M' AND @AvsenderFylkeNr = '02'
                                ) -- Akershus
                                OR ([s].[spesialundervisning vurderes] <> '' AND @AvsenderFylkeNr = '05'
                                    ) --Oppland
                                OR ([s].[spesialundervisning vurderes] = 'S' AND @AvsenderFylkeNr = '17'
                                    ) --Nord-Trøndelag
                                ) THEN 'J'
                        ELSE 'N'
                    END)
    JOIN [dbo].[Result] [r] ON [r].[SurveyID] = @SurveyID AND [r].[UserID] = [u].[userid] AND [r].[EntityStatusID] = 2 AND r.[Deleted] IS NULL
     AND [r].[Created] > GETDATE() - 3 AND [r].[CustomerID] = @CustomerID
    WHERE [s].[fylkesnr - avsender] = @AvsenderFylkeNr;

    --Count IV
    INSERT INTO [dbo].[Answer] ([ResultID], [QuestionID], [AlternativeID], value, [customerID], [createdby], [lastupdatedby], [created], [lastupdated])
    --declare @AvsenderFylkeNr as nvarchar(50) = '09', @CustomerID int = 497, @SurveyID as integer = 4249, @GradeScaleID  integer = 18527, @YesNoScaleID integer = 18525, @GradeQID  integer = 63181
    SELECT [r].[resultid], @GradeQID, [a].[AlternativeID], (SELECT SUM(CASE
                                                                            WHEN [vsf].[karakter - standpunkt] = 'IV' THEN 1
                                                                            ELSE 0
                                                                        END)
                                                            FROM [vigo].[vokal_soker_fag] [vsf]
                                                            WHERE [p].[fylkesnr - avsender] = [vsf].[fylkesnr - avsender]
                                                              AND [p].[fødselsnummer] = [vsf].[fødselsnummer]
                                                              AND [p].[programområde] = [vsf].[programområde]
                                                              AND [vsf].[skoleår] = [p].[skoleår]
                                                            GROUP BY [vsf].[fødselsnummer]
                                                            ), @CustomerID, 883174, 883174, GETDATE(), GETDATE()
    FROM [vigo].[vokal_soker_program] [p]
    JOIN [org].[User] [u] ON [p].[fødselsnummer] = [u].[ssn] AND [u].[EntityStatusID] IN (1, 2) AND [u].[CustomerID] = @CustomerID AND u.[Deleted] IS NULL
    JOIN [at].[Alternative] [a] ON [a].[scaleid] = @GradeScaleID AND [a].[extid] = 'Count_IV'
    JOIN [dbo].[Result] [r] ON [r].[SurveyID] = @SurveyID AND [r].[UserID] = [u].[userid] AND [r].[EntityStatusID] = 2 AND [r].[Created] > GETDATE() - 3 AND r.[Deleted] IS NULL
    WHERE [p].[fylkesnr - avsender] = @AvsenderFylkeNr AND CASE
                                                                WHEN ISNUMERIC([p].[Nivå]) = 1 THEN CAST([p].[Nivå] AS int)
                                                                ELSE 0
                                                            END = 2;

    --Count 1
    INSERT INTO [dbo].[Answer] ([ResultID], [QuestionID], [AlternativeID], value, [customerID], [createdby], [lastupdatedby], [created], [lastupdated])
    --declare @AvsenderFylkeNr as nvarchar(50) = '09', @CustomerID int = 497, @SurveyID as integer = 4249, @GradeScaleID  integer = 18527, @YesNoScaleID integer = 18525, @GradeQID  integer = 63181
    SELECT [r].[resultid], @GradeQID, [a].[AlternativeID], (SELECT SUM(CASE
                                                                            WHEN [vsf].[karakter - standpunkt] = '1' THEN 1
                                                                            ELSE 0
                                                                        END)
                                                            FROM [vigo].[vokal_soker_fag] [vsf]
                                                            WHERE [p].[fylkesnr - avsender] = [vsf].[fylkesnr - avsender]
                                                              AND [p].[fødselsnummer] = [vsf].[fødselsnummer]
                                                              AND [p].[programområde] = [vsf].[programområde]
                                                              AND [vsf].[skoleår] = [p].[skoleår]
                                                            GROUP BY [vsf].[fødselsnummer]
                                                            ), @CustomerID, 883174, 883174, GETDATE(), GETDATE()
    FROM [vigo].[vokal_soker_program] [p]
    JOIN [org].[User] [u] ON [p].[fødselsnummer] = [u].[ssn] AND [u].[EntityStatusID] IN (1, 2) AND [u].[CustomerID] = @CustomerID AND u.[Deleted] IS NULL
    JOIN [at].[Alternative] [a] ON [a].[scaleid] = @GradeScaleID AND [a].[extid] = 'Count_1'
    JOIN [dbo].[Result] [r] ON [r].[SurveyID] = @SurveyID AND [r].[UserID] = [u].[userid] AND [r].[EntityStatusID] = 2 AND [r].[Created] > GETDATE() - 3 AND r.[Deleted] IS NULL
    WHERE [p].[fylkesnr - avsender] = @AvsenderFylkeNr AND CASE
                                                                WHEN ISNUMERIC([p].[Nivå]) = 1 THEN CAST([p].[Nivå] AS int)
                                                                ELSE 0
                                                            END = 2;

    --Count 2
    INSERT INTO [dbo].[Answer] ([ResultID], [QuestionID], [AlternativeID], value, [customerID], [createdby], [lastupdatedby], [created], [lastupdated])
    --declare @AvsenderFylkeNr as nvarchar(50) = '09', @CustomerID int = 497, @SurveyID as integer = 4249, @GradeScaleID  integer = 18527, @YesNoScaleID integer = 18525, @GradeQID  integer = 63181
    SELECT [r].[resultid], @GradeQID, [a].[AlternativeID], (SELECT SUM(CASE
                                                                            WHEN [vsf].[karakter - standpunkt] = '2' THEN 1
                                                                            ELSE 0
                                                                        END)
                                                            FROM [vigo].[vokal_soker_fag] [vsf]
                                                            WHERE [p].[fylkesnr - avsender] = [vsf].[fylkesnr - avsender]
                                                              AND [p].[fødselsnummer] = [vsf].[fødselsnummer]
                                                              AND [p].[programområde] = [vsf].[programområde]
                                                              AND [vsf].[skoleår] = [p].[skoleår]
                                                            GROUP BY [vsf].[fødselsnummer]
                                                            ), @CustomerID, 883174, 883174, GETDATE(), GETDATE()
    FROM [vigo].[vokal_soker_program] [p]
    JOIN [org].[User] [u] ON [p].[fødselsnummer] = [u].[ssn] AND [u].[EntityStatusID] IN (1, 2) AND [u].[CustomerID] = @CustomerID AND u.[Deleted] IS NULL
    JOIN [at].[Alternative] [a] ON [a].[scaleid] = @GradeScaleID AND [a].[extid] = 'Count_2'
    JOIN [dbo].[Result] [r] ON [r].[SurveyID] = @SurveyID AND [r].[UserID] = [u].[userid] AND [r].[EntityStatusID] = 2 AND [r].[Created] > GETDATE() - 3 AND r.[Deleted] IS NULL
    WHERE [p].[fylkesnr - avsender] = @AvsenderFylkeNr AND CASE
                                                                WHEN ISNUMERIC([p].[Nivå]) = 1 THEN CAST([p].[Nivå] AS int)
                                                                ELSE 0
                                                            END = 2;

    UPDATE [dbo].[Result] SET [EntityStatusID] = 1, [EndDate] = GETDATE()
    WHERE [SurveyID] = @SurveyID AND [EntityStatusID] = 2 AND [Created] > GETDATE() - 3 AND [CustomerID] = @CustomerID AND [Deleted] IS NULL;

    --Create AutoCalc job for given activity and survey
    IF @RunAutocalc = 1
    BEGIN
        PRINT 'Creating BPS calc job (but deleting some calculated D-answers first, Q=63204 A=94278)';
        DELETE FROM [dbo].[Answer]
        WHERE [questionid] = 63204 AND [AlternativeID] = 94278 AND [resultid] IN (SELECT [resultid] FROM [dbo].[Result] WHERE [surveyid] = @SurveyID);

        EXEC [job].[CreateAutoCalcJob] 9, 883174, @QServer, @QDB, @ActivityID, @SurveyID, 'vokalint', '';
    END;

END;
GO