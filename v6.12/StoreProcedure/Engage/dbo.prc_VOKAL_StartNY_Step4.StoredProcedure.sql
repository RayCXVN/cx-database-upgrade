SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[prc_VOKAL_StartNY_Step4]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [dbo].[prc_VOKAL_StartNY_Step4] AS' 
END
GO
/***********************************************
Run after PDF creation AND new students created

2017-10-11 Ray:     Refactor
***********************************************/
ALTER PROCEDURE [dbo].[prc_VOKAL_StartNY_Step4](
    @SiteId int)
AS
BEGIN
    DECLARE @NewPeriodID int, @DT_Skole int, @OwnerId int, @AG_FinishedNSY int = 0, @PROP_ActivePeriodID int,
            @EntityStatusID_Active int = 0, @EntityStatusID_Inactive int = 0;

    SELECT @EntityStatusID_Active = [EntityStatusID] FROM [EntityStatus] WHERE [CodeName] = 'Active';
    SELECT @EntityStatusID_Inactive = [EntityStatusID] FROM [EntityStatus] WHERE [CodeName] = 'Inactive';

    SELECT @NewPeriodID = [sp].value FROM [app].[SiteParameter] AS [sp] WHERE [sp].[Key] = 'Platform.NewSchoolYear.NextPeriodId' AND [sp].[SiteID] = @SiteId;
    IF (@NewPeriodID IS NULL)
    BEGIN
        RAISERROR(N'Site param for Platform.NewSchoolYear.NextPeriodId not defined', 10, 1);
    END;

    SELECT @AG_FinishedNSY = ISNULL([Value], 0) FROM [app].[SiteParameter] WHERE [SiteID] = @SiteID AND [Key] = 'AG_FinishedNSY';

    SELECT @DT_Skole = [sp].value FROM [app].[SiteParameter] AS [sp] WHERE [sp].[Key] = 'Platform.Common.SchoolDepartmentTypeId' AND [sp].[SiteID] = @SiteId;
    IF (@DT_Skole IS NULL)
    BEGIN
        RAISERROR(N'Site param for Platform.Common.SchoolDepartmentTypeId not defined', 10, 1);
    END;

    SELECT @PROP_ActivePeriodID = [sp].value FROM [app].[SiteParameter] AS [sp] WHERE [sp].[Key] = 'Platform.NewSchoolYear.CurrentPeriodProp' AND [sp].[SiteID] = @SiteId;
    IF (@PROP_ActivePeriodID IS NULL)
    BEGIN
        RAISERROR(N'Site param for Platform.NewSchoolYear.CurrentPeriodProp not defined', 10, 1);
    END;

    SELECT @OwnerId = [s].[OwnerID] FROM [app].[Site] AS [s] WHERE [s].[SiteID] = @SiteId;
    IF (@OwnerId IS NULL)
    BEGIN
        DECLARE @errorMsg AS varchar(max)= 'Owner-id not defined for site '+CAST(@siteId AS varchar(3));
        RAISERROR(@errorMsg, 10, 1);
    END;

    IF (@NewPeriodID IS NOT NULL AND @DT_Skole IS NOT NULL AND @OwnerId IS NOT NULL)
    BEGIN
        DECLARE @DepartmentID int;
        
        -- List out all Integration schools belonging to the school owner
        DECLARE cur_department CURSOR FOR
        SELECT [d].[departmentid]
        FROM [org].[department] [d]
        JOIN [org].[dt_d] [dtd] ON [dtd].[departmentid] = [d].[departmentid] AND [dtd].[departmenttypeid] = @DT_Skole
        JOIN [org].[H_D] [hd] ON [hd].[DepartmentID] = [d].[DepartmentID] AND [hd].[Deleted] = 0
        WHERE [d].[ownerid] = @OwnerID
          AND [d].[customerid] IN (SELECT [customerid] FROM [org].[Customer] WHERE [HasUserIntegration] = 1)
          AND [d].[EntityStatusID] IN (@EntityStatusID_Active, @EntityStatusID_Inactive)
          AND [d].[Deleted] IS NULL; -- only not deleted schools

        OPEN cur_department;
        FETCH NEXT FROM cur_department INTO @DepartmentID;
        WHILE @@FETCH_STATUS = 0
        BEGIN
            UPDATE [pv] SET [pv].Value = @NewPeriodID, [pv].[Updated] = GETDATE()
            FROM [prop].[PropValue] [pv]
            WHERE [pv].[PropertyID] = @PROP_ActivePeriodID AND [pv].[itemid] = @DepartmentID;

            FETCH NEXT FROM cur_department INTO @DepartmentID;
        END;
        CLOSE cur_department;
        DEALLOCATE cur_department;
    END;

    -- Hide the menu "Nasjonale prøver" for all Integration schools after run NSY
    INSERT INTO [AccessGroupMember] ([AccessGroupID], [HDID])
    SELECT @AG_FinishedNSY, [hd].[HDID]
    FROM [org].[department] [d]
    JOIN [org].[dt_d] [dtd] ON [dtd].[departmentid] = [d].[departmentid] AND [dtd].[departmenttypeid] = @DT_Skole
    JOIN [org].[H_D] [hd] ON [hd].[DepartmentID] = [d].[DepartmentID] AND [hd].[Deleted] = 0
    WHERE [d].[ownerid] = @OwnerID
      AND [d].[customerid] IN (SELECT [customerid] FROM [org].[Customer] WHERE [HasUserIntegration] = 1)
      AND [d].[EntityStatusID] IN (@EntityStatusID_Active, @EntityStatusID_Inactive)
      AND [d].[Deleted] IS NULL; -- only not deleted schools
END;
GO