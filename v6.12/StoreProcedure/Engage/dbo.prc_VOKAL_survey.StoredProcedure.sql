SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[prc_VOKAL_survey]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [dbo].[prc_VOKAL_survey] AS' 
END
GO

ALTER proc [dbo].[prc_VOKAL_survey]
(
  @XCategoryID as int,
  @Ownerid as int,
  @PeriodID as int,
  @Name as nvarchar(64),
  @Startdate as datetime,
  @Enddate as datetime
)
AS 
  --Insert Into at.survey( ActivityID, StartDate, EndDate, Anonymous, Status, ShowBack, LanguageID, ButtonPlacement, UsePageNo, LinkURL, FinishURL, CreateResult, DefaultMinResults, ReportDB, ReportServer, StyleSheet, Type, Created, LastProcessed, ReProcessOLAP, No, OLAPServer, OLAPDB, PeriodID)
  Select a.activityid,@Startdate,@Enddate,0,2,0,1,0,1,'','',0,0,o.reportdb,o.reportserver,'',1,getdate(),'1900-01-01',0,0,a.Olapserver,a.olapdb,@Periodid from at.activity a
  join org.owner o on a.ownerid = o.ownerid 
  join at.XC_A xca on xca.activityid = a.activityid and xca.XCID = @XCategoryID
  

GO
