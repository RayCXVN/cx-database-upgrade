SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[prc_VOKAL_startNY]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [dbo].[prc_VOKAL_startNY] AS' 
END
GO
/*
    2016-10-10 Sarah:   Remove column Status on User, Department and Result tables
    2016-10-31 Sarah:   Hot fix: Set limit length of string which is used to update UserName
    2017-05-24 Sarah:   Insert ArchetypeID, EntityStatusID, EntityStatusReasonID when insert a new UserGroup
    2018-04-24 Ray:     Switch to use archetype learner instead of usertype student
    2018-06-19 Ray:     Clear UserFavorite
    2018-07-06 Ray:     Replace UG_U by UGMember
    2018-07-19 Ray:     Improve performance for Clear UserFavorite
    2018-08-10 Ray:     Fix UserFavourite left over after NSY, fix for class and school
                        Fix DT_UG of classes with multiple levels
    2019-02-28 Ray:     Set archetype of UserGroups to Archived Class
    2019-04-01 Ray:     Remove all rights of external users (handled U_D)
    2019-04-10 Ray:     Copy external usergroups and its UGMembers related to Department
    2019-06-18 Ray:     Fix deadlocks when delete PropFile
*/
ALTER PROCEDURE [dbo].[prc_VOKAL_startNY]
(
    @PeriodID           int,
    @HDID               int,
    @DepartmentTypeID   int,
    @DT_RTID            int,
    @UT_RTID            int,
    @UT_AVG             int,
    @ErrorMessage       nvarchar(MAX) out,
    @Sync               bit = 0,
    @SiteID             int = 5,
    @ExecutedByUserID   int = NULL
)
AS
BEGIN TRAN
    SET NOCOUNT ON
    SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED
    DECLARE @DepartmentID int, @Name nvarchar(256), @UGID int, @ParentDepID int, @UserID int, @OName varchar(10), @NName varchar(10), @Trinn varchar(200), @Count int,
            @NextPeriodID int, @CustomerID int, @C1 int, @C2 int, @Result int= 0, -- no errors
            @PropFileID int, @PROP_ZipFile int= NULL, @PROP_SinglePDF int= NULL, @EntityStatusID_Active int= 0, @EntityStatusID_Deactive int= 0,
            @EntityStatusReasonID_RelatedObject int, @EntityStatusReasonID_Active int, @ArchetypeID_Class int= 0, @ArchetypeID_Learner int = 0,
            @Archetype_ExternalUser int = 0, @UserType_Registration_Full int = 0, @UserType_Registration_Limited int = 0, @Archetype_ExternalUserGroup int = 0;
    DECLARE @EventType_ForCleanUp int;
    DECLARE @CleanupEventType TABLE ([EventTypeID] int);

    --DECLARE @StartTime datetime = getdate()
    SELECT @PROP_ZipFile    = [sp].[Value] FROM [app].[SiteParameter] sp WHERE sp.[SiteID] = @SiteID AND [sp].[Key] = 'Platform.NewSchoolYear.ZipProp';
    SELECT @PROP_SinglePDF  = [sp].[Value] FROM [app].[SiteParameter] sp WHERE sp.[SiteID] = @SiteID AND [sp].[Key] = 'Platform.NewSchoolYear.ZipSLGProp';
    SELECT @NextPeriodID    = [sp].[Value] FROM [app].[SiteParameter] sp WHERE sp.[SiteID] = @SiteID AND [sp].[Key] = 'Platform.NewSchoolYear.NextPeriodId';
    SELECT @UserType_Registration_Full      = [sp].[Value] FROM [app].[SiteParameter] sp WHERE sp.[SiteID] = @SiteID AND [sp].[Key] = 'Platform.Registration.ResultRegistrationForAllUserType';
    SELECT @UserType_Registration_Limited   = [sp].[Value] FROM [app].[SiteParameter] sp WHERE sp.[SiteID] = @SiteID AND [sp].[Key] = 'Platform.Registration.ResultRegistrationForSelectedUserType';

    SELECT @EntityStatusID_Active = EntityStatusID FROM dbo.EntityStatus WHERE CodeName = N'Active';
    SELECT @EntityStatusID_Deactive = EntityStatusID FROM dbo.EntityStatus WHERE CodeName = N'Deactive';
    SELECT @EntityStatusReasonID_RelatedObject = [EntityStatusReasonID] FROM [EntityStatusReason] WHERE [CodeName] = 'Deactive_SetByRelatedObject';
    SELECT @EntityStatusReasonID_Active = EntityStatusReasonID FROM dbo.EntityStatusReason WHERE CodeName = N'Active_None';
    SELECT @ArchetypeID_Class = ArchetypeID FROM dbo.Archetype WHERE CodeName = N'ArchivedClass';
    SELECT @ArchetypeID_Learner = ArchetypeID FROM dbo.Archetype WHERE CodeName = N'Learner';
    SELECT @Archetype_ExternalUser = [ArchetypeID] FROM [dbo].[Archetype] WHERE [CodeName] = 'ExternalUser';
    SELECT @Archetype_ExternalUserGroup = [ArchetypeID] FROM [dbo].[Archetype] WHERE [CodeName] = 'ExternalUserGroup';
    SELECT @EventType_ForCleanUp = et.[EventTypeID] FROM [log].[EventType] et WHERE et.[MasterID] = '72150482-FDBC-4425-8317-E9EB472D07EE'; -- ForCleanUp
    
    INSERT INTO @CleanupEventType ([EventTypeID])
    SELECT et.[EventTypeID]
    FROM [log].[EventType] et
    WHERE et.[CodeName] IN ('VIEW_USER_INFORMATION','USER_CREATED','RESULT_CREATED','RESULT_STATUS_CHANGE','RESULT_IN_PROGRESS','RESULT_DELETED','RESULT_VIEWED','RESULT_UPDATED');

    BEGIN TRY
        -- temporary table with old and new type's Department and User Type's. Name fields are used to change the class name from, for example. 2A to 3A.
        CREATE TABLE #Map
        (
            OID        int,
            OName    varchar(10),
            NID        int,
            NName    varchar(10),
            EntityStatusID int,
            Deleted DATETIME
        )

        -- loading DepartmentId to school according to HDID.
        SELECT @ParentDepID = hd.[DepartmentID], @CustomerID = [d].[CustomerID]
        FROM [org].[H_D] [hd]
        JOIN [org].[Department] [d] ON [d].[DepartmentID] = [hd].[DepartmentID] AND [hd].[HDID] = @HDID;
        
        -- Delete previous schoolyear zip files
        DECLARE @PropFileTable TABLE ([PropFileID] int)
        DELETE pv OUTPUT [DELETED].[PropFileID] INTO @PropFileTable ([PropFileID])
        FROM [prop].[PropFile] pf
        JOIN [prop].[PropValue] pv ON pf.[PropFileId] = pv.[PropFileID] AND pv.[PropertyID] IN (@PROP_ZipFile, @PROP_SinglePDF)
         AND pv.[ItemID] = @ParentDepID;

        UPDATE pf SET pf.[Deleted] = 1
        FROM [prop].[PropFile] pf JOIN @PropFileTable t ON pf.[PropFileId] = t.[PropFileID];
        
        -- Remove graduated student from previous schoolyear
        UPDATE org.[User] SET EntityStatusReasonID = @EntityStatusReasonID_RelatedObject, EntityStatusID = @EntityStatusID_Deactive, [UserName] = SUBSTRING('DEL_' + cast([UserID] AS VARCHAR(32)) + '_'+ [UserName],1,128)
        FROM  org.[User] u
        WHERE DepartmentID = @ParentDepID 
          AND EXISTS (SELECT 1 FROM org.UT_U utu WHERE utu.UserTypeID = @UT_AVG AND utu.UserID = u.UserID)
        
        --#region Copy external usergroups and members related to departments
        DECLARE @ExternalUserGroup TABLE ([FromUserGroupID] int, [ToUserGroupID] int);

        INSERT INTO [org].[UserGroup] ([OwnerID], [DepartmentID], [SurveyId], [Name], [Description], [Created], [ExtID], [PeriodID], [UserID], [UsergroupTypeID],
                                       [Tag], [LastUpdated], [LastUpdatedBy], [ArchetypeID], [Deleted], [EntityStatusID], [EntityStatusReasonID],
                                       [ReferrerToken], [ReferrerResource], [ReferrerArchetypeID])
        OUTPUT [INSERTED].[ExtID], [INSERTED].[UserGroupID]
        INTO @ExternalUserGroup ([FromUserGroupID], [ToUserGroupID])
        SELECT ug.[OwnerID], ug.[DepartmentID], ug.[SurveyId], ug.[Name], ug.[Description], GETDATE() AS [Created], ug.[UserGroupID] AS [ExtID], @NextPeriodID AS [PeriodID], ug.[UserID], ug.[UsergroupTypeID],
               ug.[Tag], GETDATE() AS [LastUpdated], @ExecutedByUserID AS [LastUpdatedBy], ug.[ArchetypeID], ug.[Deleted], ug.[EntityStatusID], ug.[EntityStatusReasonID],
               ug.[ReferrerToken], ug.[ReferrerResource], ug.[ReferrerArchetypeID]
        FROM [org].[UserGroup] ug
        WHERE ug.[DepartmentID] = @ParentDepID AND ug.[ArchetypeID] = @Archetype_ExternalUserGroup AND ug.[PeriodID] = @PeriodID
          AND ug.[EntityStatusID] = @EntityStatusID_Active AND ug.[Deleted] IS NULL;
        
        INSERT INTO [org].[UGMember] ([UserGroupID], [UserID], [Created], [CreatedBy], [MemberRoleID], [ValidFrom], [ValidTo], [LastUpdated], [LastUpdatedBy],
                                      [EntityStatusID], [EntityStatusReasonID], [CustomerID], [PeriodID], [ExtID], [Deleted], [DisplayName],
                                      [ReferrerResource], [ReferrerToken], [ReferrerArchetypeID])
        SELECT eug.[ToUserGroupID] AS [UserGroupID], ugm.[UserID], GETDATE() AS [Created], @ExecutedByUserID AS [CreatedBy], ugm.[MemberRoleID], NULL AS [ValidFrom], NULL AS [ValidTo], GETDATE() AS [LastUpdated], @ExecutedByUserID AS [LastUpdatedBy],
               ugm.[EntityStatusID], ugm.[EntityStatusReasonID], @CustomerID, @NextPeriodID AS [PeriodID], ugm.[ExtID], ugm.[Deleted], ugm.[DisplayName],
               ugm.[ReferrerResource], ugm.[ReferrerToken], ugm.[ReferrerArchetypeID]
        FROM [org].[UGMember] ugm
        JOIN @ExternalUserGroup eug ON eug.[FromUserGroupID] = ugm.[UserGroupID] AND ugm.[EntityStatusID] = @EntityStatusID_Active AND ugm.[Deleted] IS NULL
         AND ugm.[ExtID] <> '';
        --#endregion Copy external usergroups and members related to departments

        -- stock cursor that runs through all school classes
        DECLARE department_cursor CURSOR LOCAL FAST_FORWARD READ_ONLY FOR        
        -- get list of child active department id and name of current department and must have department type
        SELECT DISTINCT H.DepartmentID, Name FROM org.H_D H
        INNER JOIN org.Department D on H.DepartmentID = D.DepartmentID
        WHERE ParentID = @HDID AND H.DepartmentID IN (SELECT DepartmentID FROM org.DT_D WHERE DepartmentTypeID = @DepartmentTypeID)
        AND D.EntityStatusID = @EntityStatusID_Active AND D.Deleted IS NULL AND H.Deleted = 0
        
        OPEN department_cursor
        FETCH NEXT FROM department_cursor INTO @DepartmentID, @Name

        WHILE (@@FETCH_STATUS = 0)
        BEGIN
            -- clear the temporary table        
            DELETE FROM #Map  
            
            -- create a User Group with class id and current periodeID (created also for deleted branches)
            -- print 'create a User Group with class id and current periodeID (created also for deleted branches)'
            INSERT org.Usergroup(OwnerID, DepartmentID, Name, Description, ExtID, PeriodID, ArchetypeID, EntityStatusID, EntityStatusReasonID)
            SELECT OwnerID, @ParentDepID, Name, Description, CAST(DepartmentID As varchar(12)), @PeriodID, @ArchetypeID_Class, @EntityStatusID_Active, @EntityStatusReasonID_Active From org.Department
            WHERE DepartmentID = @DepartmentID
            SET @UGID = scope_identity()
            
            -- add all users into created User Group for its class.
            -- Adds even users who are soft-deleted with status <0
            -- print 'legger alle brukerne i opprettet userGroup for sin klasse'
            INSERT INTO [org].[UGMember] ([UserGroupID], [UserID], [Created], [CreatedBy], [LastUpdated], [LastUpdatedBy],
                                          [EntityStatusID], [EntityStatusReasonID], [CustomerID], [PeriodID], [ExtID], [Deleted])
            SELECT @UGID AS [UserGroupID], u.[UserID], GETDATE() AS [Created], @ExecutedByUserID AS [CreatedBy], GETDATE() AS [LastUpdated], @ExecutedByUserID AS [LastUpdatedBy],
                   @EntityStatusID_Active AS [EntityStatusID], @EntityStatusReasonID_Active AS [EntityStatusReasonID], u.[CustomerID], @PeriodID, '' AS [ExtID], NULL AS [Deleted]
            FROM org.[User] u WHERE u.[DepartmentID] = @DepartmentID;
            
            -- create the link between class User Group and class Department type before class has new type Department's
            -- the type of relationship to object mapping table is for new school year for the Department Type (opprykksmappinger from eg.
            -- 2A to 3A.
            -- print 'create the link between class User Group and class Department type before class has new type Department's'
            INSERT org.DT_UG(DepartmentTypeID, UserGroupID)
            SELECT dtd.[DepartmentTypeID], @UGID
            FROM org.[DT_D] dtd
            WHERE dtd.[DepartmentID] = @DepartmentID
              AND dtd.[DepartmentTypeID] IN (SELECT om.[FromID] FROM [dbo].[ObjectMapping] om WHERE om.[RelationTypeID] = @DT_RTID
                                             UNION
                                             SELECT om.[ToID] FROM [dbo].[ObjectMapping] om WHERE om.[RelationTypeID] = @DT_RTID); 

            -- Clear UserFavorite
            DELETE uf
            FROM [org].[UserFavorite] uf
            WHERE uf.[DepartmentID] = @DepartmentID;
        
            IF @EventType_ForCleanUp > 0
            BEGIN
                UPDATE e SET e.[EventTypeID] = @EventType_ForCleanUp
                FROM [log].[Event] e
                JOIN @CleanupEventType et ON et.[EventTypeID] = e.[EventTypeID] AND e.[DepartmentID] = @DepartmentID;
            END
        
            -- If you do not want to block the move as putting one @ Sync = 0 
            IF @Sync = 0
            BEGIN                
                SELECT @C1 = COUNT(*) FROM org.DT_D DT
                WHERE DT.DepartmentID = @DepartmentID
                    AND (DepartmentTypeID IN (SELECT FromID FROM ObjectMapping WHERE RelationTypeID = @DT_RTID)
                    OR  DepartmentTypeID IN (SELECT ToID FROM ObjectMapping WHERE RelationTypeID = @DT_RTID))
                
                SELECT @C2 = COUNT(*) FROM org.DT_D DT
                WHERE DT.DepartmentID = @ParentDepID
                    AND (DepartmentTypeID IN (SELECT FromID FROM ObjectMapping WHERE RelationTypeID = @DT_RTID)
                    OR  DepartmentTypeID IN (SELECT ToID FROM ObjectMapping WHERE RelationTypeID = @DT_RTID))
                
                IF @C1 <> @C2
                BEGIN
                    -- put into the temp-table mappings from the current step to the next step promotion, ex. 2 to 3 steps,
                    -- and ExtID'ene the old and the new Department Type of class.
                    -- NB! Steps must be checked to school at all be picked out from the mapping table.
                    --     Classes that are being tried moved the school steps, losing Department Type of step,
                    --     and be soft-deleted.
                    -- print 'put into the temp-table mappings from the current step to the next step promotion, ex. 2 to 3 steps'
                    INSERT #Map (OID, OName, NID, NName)
                    SELECT FromID, DT1.ExtID, ToID, DT2.ExtID
                    FROM ObjectMapping O
                    INNER JOIN org.DT_D DT ON DT.DepartmentTypeID = O.FromID AND DT.DepartmentID = @DepartmentID
                    INNER JOIN org.DepartmentType DT1 ON O.FromID = DT1.DepartmentTypeID
                    INNER JOIN org.DepartmentType DT2 ON O.ToID = DT2.DepartmentTypeID
                    WHERE O.RelationTypeID = @DT_RTID AND 
                        O.ToID IN (SELECT DepartmentTypeID FROM org.DT_D WHERE DepartmentID = @ParentDepID)
                                        
                    -- Delete all the Department's type of class that is mapped as a step
                    -- print 'Delete all the Department's type of class that is mapped as a step'
                    DELETE org.DT_D WHERE DepartmentID = @DepartmentID 
                        AND (DepartmentTypeID IN (SELECT FromID FROM ObjectMapping WHERE RelationTypeID = @DT_RTID)
                        OR  DepartmentTypeID IN (SELECT ToID FROM ObjectMapping WHERE RelationTypeID = @DT_RTID))
                                                
                    -- there are mappings to the next level in the class at all?
                    SELECT @Count = Count(*) FROM #Map
                    -- If not
                    IF @Count = 0
                    BEGIN
                        -- no mappings, set-class soft-deleted
                        -- 10B -> new step does not exist when there is a departure class, so soft-delete class)
                        -- NB! should also H_D soft-delete?
                        -- print 'no mappings, set-class soft-deleted'
                        UPDATE org.Department SET EntityStatusReasonID = @EntityStatusReasonID_RelatedObject, EntityStatusID = @EntityStatusID_Deactive WHERE DepartmentID = @DepartmentID            
                        UPDATE org.H_D SET Deleted = 1 WHERE DepartmentID = @DepartmentID
                    END
                    ELSE
                    BEGIN
                        -- enter new stage in class from temporary table
                        -- print 'enter new stage in class from temporary table'
                        INSERT org.DT_D
                        SELECT NID, @DepartmentID FROM #Map
                    
                        /* Fix Name */
                        -- or old and new names as defined in the mapping table
                        SELECT TOP 1 @Nname = NName, @OName = OName FROM #Map
                        
                        -- if the old name from the mapping table for the current class
                        IF CHARINDEX(@OName, @Name) = 1 AND dbo.IsInteger(SUBSTRING(@Name,LEN(@OName) + 1,1)) <> 1
                        BEGIN
                            -- replacing old example 9 by 10 and add the rest of the old class name,
                            -- it is assumed that the class names starting with digits.
                            SET @Name = REPLACE(LEFT(@Name, LEN(@OName)), @OName, @Nname) + SUBSTRING(@Name, LEN(@OName) + 1, 1000)
                            
                            -- sets new class name of Department
                            -- print 'sets new class name of Department'
                            UPDATE org.Department
                            SET Name = @Name
                            WHERE DepartmentID = @DepartmentID
                            
                            -- ExtID also update with a new class name, very important for classes that have integration (Locked = 1)
                            UPDATE org.Department
                            SET ExtID = SUBSTRING(ExtID,1,CHARINDEX(':',ExtID,1)) + [Name]
                            WHERE DepartmentID = @DepartmentID AND Locked = 1 AND CHARINDEX(':',ExtID,0) > 0

                            UPDATE org.Department
                            SET ExtID = SUBSTRING([ExtID], 1, LEN([ExtID]) - CHARINDEX('_', REVERSE([ExtID])) + 1) + [Name]
                            WHERE DepartmentID = @DepartmentID AND Locked = 1 AND CHARINDEX('_',ExtID,0) > 0
                        END
                    END
                END
            END    
            
            FETCH NEXT FROM department_cursor INTO @DepartmentID, @Name            
        END        
        
        -- Clear UserFavorite
        DELETE uf
        FROM [org].[UserFavorite] uf
        WHERE uf.[DepartmentID] = @ParentDepID;
        
        IF @EventType_ForCleanUp > 0
        BEGIN
            UPDATE e SET e.[EventTypeID] = @EventType_ForCleanUp
            FROM [log].[Event] e
            JOIN @CleanupEventType et ON et.[EventTypeID] = e.[EventTypeID] AND e.[DepartmentID] = @ParentDepID;
        END

        SELECT @DepartmentID = DepartmentID FROM org.H_D WHERE HDID = @HDID
        
        DELETE FROM #Map  
        
        INSERT #Map (OID, OName, NID, NName, EntityStatusID, [Deleted])
        SELECT U.UserID, '', UT.UserTypeID, '', U.EntityStatusID, U.Deleted
        FROM org.[User] U
        INNER JOIN org.H_D HD ON U.DepartmentID = HD.DepartmentID AND U.[ArchetypeID] = @ArchetypeID_Learner
        INNER JOIN org.UT_U UT ON U.UserID = UT.UserID 
        WHERE HD.Path LIKE '%\' + CAST(@HDID as varchar(12)) + '\%' 
        AND (UT.UserTypeID IN (SELECT FromID FROM ObjectMapping WHERE RelationTypeID = @UT_RTID)
            OR  UT.UserTypeID IN (SELECT ToID FROM ObjectMapping WHERE RelationTypeID = @UT_RTID))
        AND NOT EXISTS (SELECT 1 FROM org.UT_U utu WHERE utu.UserID = u.UserID AND utu.UserTypeID = @UT_AVG)
        
        -- Assign graduated usertype to last grade users of the department
        INSERT org.UT_U    
        SELECT DISTINCT @UT_AVG, M.OID  
        FROM #Map M 
        INNER JOIN ObjectMapping O ON M.NID = O.FromID  AND RelationTypeID = @UT_RTID
        WHERE NOT O.ToID IN (SELECT ToID FROM ObjectMapping O
                             INNER JOIN org.DT_D DT ON O.FromID = DT.DepartmentTypeID AND DT.DepartmentID = @DepartmentID 
                             WHERE RelationTypeID = 1 AND O.FromTableTypeID = 27 AND O.ToTableTypeID = 26)
            AND NOT M.OID IN (SELECT UserID FROM org.UT_U WHERE UserTypeID = @UT_AVG)
                            
        -- Assign graduated usertype to last grade (10th grade) users
        INSERT org.UT_U    
        SELECT DISTINCT @UT_AVG, M.OID  
        FROM #Map M 
        WHERE NOT M.NID IN (SELECT FromID FROM ObjectMapping WHERE RelationTypeID = @UT_RTID)
        AND NOT EXISTS (SELECT 1 FROM org.UT_U UTU WHERE UTU.UserTypeID=@UT_AVG AND UTU.UserID=M.OID)
        
        -- Upgrade not deleted users to higher grade
        INSERT org.UT_U    
        SELECT DISTINCT O.ToID, U.UserID
        FROM org.[User] U
        INNER JOIN org.H_D HD ON U.DepartmentID = HD.DepartmentID AND u.EntityStatusID = @EntityStatusID_Active AND u.Deleted IS NULL AND U.[ArchetypeID] = @ArchetypeID_Learner
        INNER JOIN org.UT_U UT ON U.UserID = UT.UserID 
        INNER JOIN ObjectMapping O ON O.RelationTypeID = @UT_RTID AND UT.UserTypeID = O.FromID
        WHERE HD.Path LIKE '%\' + CAST(@HDID as varchar(12)) + '\%' 
        AND NOT U.UserID IN (SELECT UserID FROM org.UT_U WHERE UserTypeID = O.ToID)

        -- Move graduated users to parent department
        UPDATE org.[User] 
        SET DepartmentID = @DepartmentID 
        FROM org.[User] U
        INNER JOIN org.H_D HD ON U.DepartmentID = HD.DepartmentID
        INNER JOIN org.UT_U UTE ON U.UserID = UTE.UserID AND UTE.UserTypeID = @UT_AVG
        WHERE HD.Path LIKE '%\' + CAST(@HDID as varchar(12)) + '\%'
        
        DECLARE @GraduatedUser TABLE (UserID int)
        INSERT INTO @GraduatedUser(UserID) SELECT u.UserID FROM org.[User] u 
        WHERE u.DepartmentID = @DepartmentID AND EXISTS (SELECT 1 FROM org.UT_U utu WHERE utu.UserID = u.UserID AND utu.UserTypeID = @UT_AVG)
        
        -- Clean original school property of graduated students
        DECLARE @PROP_Move_School int = 17
        SELECT @PROP_Move_School = [sp].[Value] FROM [app].[SiteParameter] sp WHERE sp.[SiteID] = @SiteID AND [sp].[Key] = 'Platform.NewSchoolYear.ReceivingDepartmentProp'
        DELETE pv FROM [prop].[PropValue] pv WHERE [pv].[PropertyID] = @PROP_Move_School AND [pv].[ItemID] IN (SELECT UserID FROM @GraduatedUser)        

        SELECT * FROM @GraduatedUser
        
        -- Remove graduated usertype for those who were soft deleted
        DELETE org.UT_U WITH (ROWLOCK)
        FROM org.UT_U utu
        WHERE utu.UserID IN (SELECT m.OID FROM #Map m WHERE m.EntityStatusID = @EntityStatusID_Deactive OR m.Deleted IS NOT NULL)
          AND utu.UserTypeID = @UT_AVG
        
        -- Remove users' old grade except deleted ones
        DELETE org.UT_U WITH (ROWLOCK)
        FROM org.UT_U UT
        INNER JOIN #Map M ON UT.UserID = M.OID AND UT.UserTypeID = M.NID AND M.EntityStatusID = @EntityStatusID_Active AND M.Deleted IS NULL
        WHERE UT.UserID IN (SELECT M.OID FROM #Map M)

        --#region Remove all rights of external users
        DELETE utu
        FROM [org].[H_D] hd
        JOIN [org].[User] u ON u.[DepartmentID] = hd.[DepartmentID] AND u.[ArchetypeID] = @Archetype_ExternalUser
         AND hd.[Path] LIKE '%\' + CAST(@HDID AS nvarchar(32)) + '\%'
        JOIN [org].[UT_U] utu ON utu.[UserID] = u.[UserID] AND utu.[UserTypeID] IN (@UserType_Registration_Full, @UserType_Registration_Limited);

        DELETE udut
        FROM [org].[H_D] hd
        JOIN [org].[U_D] ud ON ud.[DepartmentID] = hd.[DepartmentID] AND hd.[Path] LIKE '%\' + CAST(@HDID AS nvarchar(32)) + '\%'
        JOIN [org].[User] u ON u.[UserID] = ud.[UserID] AND u.[ArchetypeID] = @Archetype_ExternalUser
        JOIN [org].[U_D_UT] udut ON udut.[U_DID] = ud.[U_DID] AND udut.[UserTypeID] IN (@UserType_Registration_Full, @UserType_Registration_Limited);
        --#endregion Remove all rights of external users
    END TRY
    BEGIN CATCH        
        Set @Result = @@Error
        SET @ErrorMessage = ERROR_MESSAGE()
        GOTO RollbackResult
    END CATCH

CommitResult:
    IF CURSOR_STATUS('global','department_cursor') = 1
    BEGIN
        CLOSE department_cursor
        DEALLOCATE department_cursor
    END
    
    IF Object_ID('tempdb..#Map') IS NOT NULL DROP TABLE #Map
    COMMIT TRAN    
    SET @ErrorMessage = ''
    RETURN @Result
    
RollbackResult:        
    IF CURSOR_STATUS('global','department_cursor') = 1
    BEGIN
        CLOSE department_cursor
        DEALLOCATE department_cursor
    END    
    --print 'Update child department : ' + rtrim(cast(datediff(ms,@StartTime,getdate()) as char(10))) + ' mili seconds!'            
    IF Object_ID('tempdb..#Map') IS NOT NULL DROP TABLE #Map
    ROLLBACK TRAN    
    RETURN @Result

GO
