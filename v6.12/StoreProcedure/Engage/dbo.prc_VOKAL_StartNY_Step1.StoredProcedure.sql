SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[prc_VOKAL_StartNY_Step1]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [dbo].[prc_VOKAL_StartNY_Step1] AS' 
END
GO
/*  Target: Update classes and students to 1 grade up

    2017-10-06 Ray:     Refactor
    2018-07-06 Ray:     Switch to use archetype learner instead of usertype student
*/
ALTER PROCEDURE [dbo].[prc_VOKAL_StartNY_Step1]
(   @SiteId     int,
    @Sync       BIT = 0
)
AS
BEGIN
    DECLARE @OwnerID int, @OldPeriodID int,
            @DepartmentType_School int, @DepartmentType_Class int, @UserType_Graduated int, @ExecutedByUserID int = NULL,
            @RelationTypeID_DT int, @RelationTypeID_UT int, @PROP_MaintenanceMode int,
            @HDID_School int, @error_message nvarchar(max),
            @EntityStatusID_Active INT = 0, @EntityStatusID_Inactive INT = 0

    SELECT @EntityStatusID_Active   = EntityStatusID FROM EntityStatus WHERE CodeName = 'Active'
    SELECT @EntityStatusID_Inactive = EntityStatusID FROM EntityStatus WHERE CodeName = 'Inactive'
    SELECT @ExecutedByUserID = u.[UserID] FROM [org].[User] u WHERE u.[UserName] = 'vokalint';

    SELECT @DepartmentType_School = sp.value FROM app.SiteParameter sp WHERE sp.[Key] = 'Platform.Common.SchoolDepartmentTypeId' AND sp.SiteID =  @SiteId;
    IF (@DepartmentType_School IS NULL)
    BEGIN
        RAISERROR (N'Site param for Platform.Common.SchoolDepartmentTypeId not defined', 10, 1);
    END

    SELECT @OldPeriodID = sp.value FROM app.SiteParameter sp WHERE sp.[Key] = 'Platform.NewSchoolYear.CurrentPeriodId' AND sp.SiteID = @SiteId;
    IF(@OldPeriodID IS NULL)
    BEGIN
        RAISERROR(N'Site param for Platform.NewSchoolYear.CurrentPeriodId not defined', 10, 1);
    END;

    SELECT @DepartmentType_Class = [sp].value FROM [app].[SiteParameter] AS [sp] WHERE [sp].[Key] = 'Platform.Common.ClassDepartmentTypeId' AND [sp].[SiteID] = @SiteId;
    IF(@DepartmentType_Class IS NULL)
    BEGIN
        RAISERROR(N'Site param for Platform.Common.ClassDepartmentTypeId not defined', 10, 1);
    END;

    SELECT @RelationTypeID_DT = [sp].value FROM [app].[SiteParameter] AS [sp] WHERE [sp].[Key] = 'Platform.NewSchoolYear.RelationTypeIdDepartmentType' AND [sp].[SiteID] = @SiteId;
    IF(@RelationTypeID_DT IS NULL)
    BEGIN
        RAISERROR(N'Site param for Platform.NewSchoolYear.RelationTypeIdDepartmentType not defined', 10, 1);
    END;

    SELECT @RelationTypeID_UT = [sp].value FROM [app].[SiteParameter] AS [sp] WHERE [sp].[Key] = 'Platform.NewSchoolYear.RelationTypeIdUserType' AND [sp].[SiteID] = @SiteId;
    IF(@RelationTypeID_UT IS NULL)
    BEGIN
        RAISERROR(N'Site param for Platform.NewSchoolYear.RelationTypeIdUserType not defined', 10, 1);
    END;

    SELECT @UserType_Graduated = [sp].value FROM [app].[SiteParameter] AS [sp] WHERE [sp].[Key] = 'Platform.Admin.GraduatesUserType' AND [sp].[SiteID] = @SiteId;
    IF(@UserType_Graduated IS NULL)
    BEGIN
        RAISERROR(N'Site param for Platform.Admin.GraduatesUserType not defined', 10, 1);
    END;

    SELECT @PROP_MaintenanceMode = [sp].value FROM [app].[SiteParameter] AS [sp] WHERE [sp].[Key] = 'Platform.NewSchoolYear.MaintenanceModeProp' AND [sp].[SiteID] = @SiteId;
    IF(@PROP_MaintenanceMode IS NULL)
    BEGIN
        RAISERROR(N'Site param for Platform.NewSchoolYear.MaintenanceModeProp not defined', 10, 1);
    END;

    SELECT @OwnerID = [s].[OwnerID] FROM [app].[Site] AS [s] WHERE [s].[SiteID] = @SiteId;
    IF(@OwnerID IS NULL)
    BEGIN
        DECLARE @errorMsg AS varchar(max)= 'Owner-id not defined for site '+CAST(@siteId AS varchar(3));
        RAISERROR(@errorMsg, 10, 1);
    END;

    IF (@DepartmentType_School IS NOT NULL
        AND @OldPeriodID IS NOT NULL
        AND @DepartmentType_Class IS NOT NULL
        AND @RelationTypeID_DT IS NOT NULL
        AND @RelationTypeID_UT IS NOT NULL
        AND @UserType_Graduated IS NOT NULL
        AND @OwnerID IS NOT NULL)
    BEGIN
        -- updates status for all schools for a given customer to 6=Underveis
        UPDATE [pv] SET [pv].[propoptionid] = 6, [pv].[Updated] = GETDATE()
        FROM [prop].[propvalue] [pv]
        JOIN [org].[Department] [d] ON [d].[DepartmentID] = [pv].[ItemID] AND [d].[OwnerID] = @OwnerID
         AND [d].[CustomerID] IN (SELECT [customerid] FROM [org].[Customer] WHERE [HasUserIntegration] = 1)
        WHERE [pv].[propertyid] = @PROP_MaintenanceMode AND [pv].[propoptionid] IN (5, 7)
          AND [d].[EntityStatusID] IN (@EntityStatusID_Active, @EntityStatusID_Inactive) AND [d].[Deleted] IS NULL;

        DECLARE curSchools CURSOR LOCAL FAST_FORWARD READ_ONLY FOR
        SELECT DISTINCT [hd].[hdid]
        FROM [org].[department] [d]
        JOIN [org].[dt_d] [dtd] ON [dtd].[departmentid] = [d].[departmentid] AND [dtd].[departmenttypeid] = @DepartmentType_School
        JOIN [org].[h_d] [hd] ON [hd].[departmentid] = [d].[departmentid] AND [hd].[Deleted] = 0
        WHERE [d].[ownerid] = @OwnerID
          AND [d].[customerid] IN (SELECT [customerid] FROM [org].[Customer] WHERE [HasUserIntegration] = 1)
          AND [d].[EntityStatusID] IN (@EntityStatusID_Active, @EntityStatusID_Inactive) AND [d].[Deleted] IS NULL;

        OPEN curSchools;
        FETCH NEXT FROM curSchools INTO @HDID_School;
        WHILE(@@FETCH_STATUS = 0)
        BEGIN
            EXEC [dbo].[prc_VOKAL_startNY] @PeriodID = @OldPeriodID, @HDID = @HDID_School, @DepartmentTypeID = @DepartmentType_Class,
                                           @DT_RTID = @RelationTypeID_DT, @UT_RTID = @RelationTypeID_UT, @UT_AVG = @UserType_Graduated,
                                           @ErrorMessage = @error_message OUTPUT, @Sync = @Sync, @SiteID = @SiteId, @ExecutedByUserID = @ExecutedByUserID;

            FETCH NEXT FROM curSchools INTO @HDID_School;
        END;
        CLOSE curSchools;
        DEALLOCATE curSchools;
    END;
END
GO