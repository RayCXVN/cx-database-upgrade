SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[Multi_GetTests]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [dbo].[Multi_GetTests] AS' 
END
GO
ALTER PROCEDURE [dbo].[Multi_GetTests] 
    @DepartmentID   int,
    @XCID           int,
    @LanguageID     int = 1
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	select a.name,s.activityid,b.batchid, b.StartDate, b.EndDate
	from at.Batch b
	join at.Survey s on b.SurveyID=s.SurveyID
	join at.LT_Activity a on s.ActivityID=a.ActivityID and a.LanguageID = @LanguageID
	join at.XC_A x on a.ActivityID=x.ActivityID and x.XCID=@xcid
	where b.DepartmentID=@DepartmentID and b.StartDate<GETDATE() and b.EndDate>GETDATE()
END

GO
