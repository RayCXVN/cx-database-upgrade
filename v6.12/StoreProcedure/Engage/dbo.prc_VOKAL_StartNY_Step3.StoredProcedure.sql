SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[prc_VOKAL_StartNY_Step3]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [dbo].[prc_VOKAL_StartNY_Step3] AS' 
END
GO
/*****************************************************************************************************************************************************
NOTE:The elevkart-user must have e-mail to you (rsa@conexus.no) because the email regarding zip-generated for the school will be sent to this address.

Make one procedure to trigger the right job for a school. Remember that I must be receiver, not the school admins.
(Has created stored procedrue at [CreateJobForPDFCreation], which is used by the cursor below:) 

CURSOR running for a customer which parse all schools, create temporary users AND register one job per school for PDF-archive for graduates.

2017-10-11 Ray:     Refactor
*****************************************************************************************************************************************************/
ALTER PROCEDURE [dbo].[prc_VOKAL_StartNY_Step3](
    @SiteId          int,
    @LanguageID      int           = 0,
    @FileName        nvarchar(max) = '',
    @FileTitle       nvarchar(max) = '',
    @FileDescription nvarchar(max) = '')
AS
BEGIN
    DECLARE @HDID_School int, @DepartmentID_School int, @database varchar(100), @UserString nvarchar(max), @OwnerID int, @DepartmentType_School int, 
            @UserType_Graduated int, @UsernameForPDFCreation varchar(32) = 'vokalint', @UserIdForPDFCreation varchar(max) = NULL;

    SELECT @database = DB_NAME();

    SELECT @DepartmentType_School = [sp].value FROM [app].[SiteParameter] AS [sp] WHERE [sp].[Key] = 'Platform.Common.SchoolDepartmentTypeId' AND [sp].[SiteID] = @SiteId;
    IF (@DepartmentType_School IS NULL)
    BEGIN
        RAISERROR(N'Site param for Platform.Common.SchoolDepartmentTypeId not defined', 10, 1);
    END;

    SELECT @UserType_Graduated = [sp].value FROM [app].[SiteParameter] AS [sp] WHERE [sp].[Key] = 'Platform.Admin.GraduatesUserType' AND [sp].[SiteID] = @SiteId;
    IF (@UserType_Graduated IS NULL)
    BEGIN
        RAISERROR(N'Site param for Platform.Admin.GraduatesUserType not defined', 10, 1);
    END;

    SELECT @OwnerID = [s].[OwnerID] FROM [app].[Site] AS [s] WHERE [s].[SiteID] = @SiteId;
    IF (@OwnerID IS NULL)
    BEGIN
        DECLARE @errorMsg AS varchar(max) = 'Owner-id not defined for site ' + CAST(@siteId AS varchar(32));
        RAISERROR(@errorMsg, 10, 1);
    END;

    SELECT @UserIdForPDFCreation = [u].[UserID] FROM [org].[User] AS [u] WHERE [u].[UserName] = @UsernameForPDFCreation;
    IF (@UserIdForPDFCreation IS NULL)
    BEGIN
        RAISERROR('User for creating PDF jobs IS NOT defined', 10, 1);
    END;

    IF (@DepartmentType_School IS NOT NULL AND @UserType_Graduated IS NOT NULL AND @OwnerID IS NOT NULL AND @UserIdForPDFCreation IS NOT NULL)
    BEGIN
        DECLARE @ActiveEntityStatusID int = (SELECT [EntityStatusID] FROM [EntityStatus] WHERE [CodeName] = 'Active');

        DECLARE curSchools CURSOR LOCAL FAST_FORWARD READ_ONLY FOR
        SELECT DISTINCT [hd].[hdid], [d].[departmentid]
        FROM [org].[department] [d]
        JOIN [org].[dt_d] [dtd] ON [dtd].[departmentid] = [d].[departmentid] AND [dtd].[departmenttypeid] = @DepartmentType_School
        JOIN [org].[h_d] [hd] ON [hd].[departmentid] = [d].[departmentid] AND [hd].[Deleted] = 0
        WHERE [d].[ownerid] = @OwnerID
          AND [d].[customerid] IN (SELECT [customerid] FROM [org].[Customer] WHERE [HasUserINTegration] = 1)
          AND [d].[EntityStatusID] = @ActiveEntityStatusID AND [d].[Deleted] IS NULL; -- only active schools

        OPEN curSchools;
        FETCH NEXT FROM curSchools INTO @HDID_School, @DepartmentID_School;
        WHILE(@@FETCH_STATUS = 0)
        BEGIN
            SET @UserString = NULL;

            SELECT @UserString = COALESCE(@UserString+', ', '') + CAST([u].[userid] AS varchar)
            FROM [org].[user] [u]
            JOIN [org].[UT_U] [utu] ON [u].[userid] = [utu].[UserID] AND [utu].[UserTypeID] = @UserType_Graduated AND [departmentid] = @DepartmentID_School
             AND [u].[EntityStatusID] = @ActiveEntityStatusID AND u.[Deleted] IS NULL;

            IF @UserString IS NULL
            BEGIN
                SET @UserString = '';
            END;
            PRINT @DepartmentID_School;
            PRINT @UserString;

            EXEC [dbo].[CreateJobForPDFCreation] @UserIdForPDFCreation, @HDID_School, @UsernameForPDFCreation, '', @@SERVERNAME, @database, @UserString, @LanguageID, @SiteID, @FileName, @FileTitle, @FileDescription;

            FETCH NEXT FROM curSchools INTO @HDID_School, @DepartmentID_School;
        END;
        CLOSE curSchools;
        DEALLOCATE curSchools;
    END;
END;
GO