SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[prc_VOKAL]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [dbo].[prc_VOKAL] AS' 
END
GO
ALTER PROCEDURE [dbo].[prc_VOKAL]
(
  @HDID as int,
  @PeriodID as int,
  @XCategoryID as int,
  @DepartmentTypeID as int,
  @Ownerid as int,
  @AllowMultiBatchPerDepartment bit=0
)
AS
IF @DepartmentTypeID>0
 Insert into at.Batch (SurveyID, UserID, DepartmentID, Name, No, StartDate, EndDate, Status, MailStatus, Created)
  Select s.surveyid,null,d.departmentid,d.name,0,s.startdate,s.enddate,2,0,getdate() from org.H_D HD 
  Join org.DT_D DTD on DTD.Departmentid = HD.Departmentid and DTD.departmenttypeid = @DepartmentTypeID
  Join org.Department d on d.departmentid = hd.departmentid
  Cross Join at.Activity act 
  join at.XC_A xca on xca.activityid = act.activityid and xca.XCID = @XCategoryID
  Join at.survey s on s.activityid = act.activityID and s.periodid= @PeriodID AND s.[ExtId] != 'Global'
  where HD.path like '%\' + cast(@HDID as nvarchar(16)) + '\%'
  and act.ownerid = @ownerid
  and not d.DepartmentID in (Select DepartmentID from at.Batch bb where bb.SurveyID=s.SurveyID and not bb.DepartmentID is null)
  and d.DepartmentID in (SELECT DISTINCT DepartmentID FROM org.DT_D DT2
  INNER JOIN dbo.ObjectMapping OM2 ON DT2.DepartmentTypeID = OM2.FromID AND OM2.RelationTypeID = 1 AND OM2.FromTableTypeID = 27
  INNER JOIN at.RoleMapping RM2 ON RM2.ActivityID = act.ActivityID AND RM2.UserTypeID = OM2.ToID)
  AND (@AllowMultiBatchPerDepartment=1 OR NOT EXISTS (SELECT 1 FROM at.Batch b1 WHERE b1.SurveyID=s.SurveyID AND b1.DepartmentID=d.DepartmentID))
ELSE
 Insert into at.Batch (SurveyID, UserID, DepartmentID, Name, No, StartDate, EndDate, Status, MailStatus, Created)
  Select s.surveyid,null,d.DepartmentID,d.Name,0,s.startdate,s.enddate,2,0,getdate() from at.Activity act 
  join at.XC_A xca on xca.activityid = act.activityid and xca.XCID = @XCategoryID
  Join at.survey s on s.activityid = act.activityID and s.periodid= @PeriodID AND s.[ExtId] != 'Global'
  inner join org.H_D hd on hd.HDID=@HDID
  inner join org.Department d on hd.DepartmentID=d.DepartmentID
  where act.ownerid = @ownerid  
  and not d.DepartmentID in (Select DepartmentID from at.Batch bb where bb.SurveyID=s.SurveyID and not bb.DepartmentID is null)
  AND (@AllowMultiBatchPerDepartment=1 OR NOT EXISTS (SELECT 1 FROM at.Batch b1 WHERE b1.SurveyID=s.SurveyID AND b1.DepartmentID=d.DepartmentID))

GO
