SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[prc_VOKAL_FinishNewSchoolYear]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [dbo].[prc_VOKAL_FinishNewSchoolYear] AS' 
END
GO
/*
    Sarah - Oct 31, 2016 - Hot fix: Get EntityStatus value on QDB
    2017-05-19 Steve:   SiteID 3 --> 5
    2017-05-25 Steve:   Change how to list all last period surveys for copying results
    2017-08-11 Ray:     New logic for copying evaluation activities' results to first survey of new period

    [prc_VOKAL_FinishNewSchoolYear] @OwnerID = 9, @HDID = 22945, @OldPeriodID = 7, @NewPeriodID = 8, @EvaluationActivityType = 3, @RTID = 3, @StatusTypeID = 5, @SiteID = 5
*/
ALTER PROCEDURE [dbo].[prc_VOKAL_FinishNewSchoolYear]
(
    @OwnerID                int,
    @HDID                   int,
    @OldPeriodID            int,
    @NewPeriodID            int,
    @EvaluationActivityType int,
    @RTID                   int,
    @StatusTypeID           int,
    @SiteID                 int = 5
)
AS
BEGIN
    SET NOCOUNT ON

    DECLARE @XCID_DenyTransferResultTable TABLE (XCID int)
    DECLARE @ActiveEntityStatusID INT = 0
    SELECT @ActiveEntityStatusID = EntityStatusID FROM EntityStatus WHERE CodeName='Active'
    DECLARE @SurveyListTable TABLE (ActivityID int, ActivityType int, SurveyID int, ReportServer nvarchar(64), ReportDB nvarchar(64), BatchID int, NewSurveyID int, ResultCount int)
    DECLARE c_Survey CURSOR FORWARD_ONLY READ_ONLY FOR SELECT ReportServer, ReportDB, SurveyID, BatchID, NewSurveyID,ResultCount FROM @SurveyListTable

    CREATE TABLE #ResultIDTable (ResultID int)

    DECLARE @DepartmentID int, @ProcCall nvarchar(max), @ErrorMsg nvarchar(max)='', @XCID_DenyTransferResult nvarchar(max), @AG_FinishedNSY int = 0

    SELECT @DepartmentID = DepartmentID FROM org.H_D WHERE HDID = @HDID
    
    SELECT @XCID_DenyTransferResult = [Value] FROM [app].[SiteParameter] WHERE [SiteID] = @SiteID AND [Key] = 'XCID_DenyTransferResult'
    IF ISNULL(@XCID_DenyTransferResult, '') != ''
    BEGIN
        INSERT INTO @XCID_DenyTransferResultTable ([XCID]) SELECT [Value] FROM dbo.funcListToTableInt(@XCID_DenyTransferResult, ',')
    END
    SELECT @AG_FinishedNSY = ISNULL([Value], 0) FROM [app].[SiteParameter] WHERE [SiteID] = @SiteID AND [Key] = 'AG_FinishedNSY'

    DECLARE @p_HDID int = @HDID, @p_RTID int = @RTID, @p_StatusTypeID int = @StatusTypeID,
            @ReportServer nvarchar(64), @ReportDB nvarchar(64), @p_SurveyID int, @p_BatchID int, @p_NewSurveyID int,
            @sqlCommand nvarchar(max), @Parameters nvarchar(max), @ResultCount int

    -- List all last period surveys
    INSERT INTO @SurveyListTable (ActivityID, ActivityType, SurveyID, ReportServer, ReportDB, BatchID, NewSurveyID, ResultCount)
    SELECT a.ActivityID, a.[Type], s.SurveyID, s.ReportServer, s.ReportDB, b.BatchID, 0,0
    FROM at.Survey s
    JOIN at.Activity a ON a.ActivityID = s.ActivityID AND a.OwnerID = @OwnerID AND s.PeriodID = @OldPeriodID
    JOIN at.Batch b ON b.SurveyID = s.SurveyID AND b.DepartmentID = @DepartmentID

    -- Get new surveys for evaluation activities, except some XCIDs in the list XCID_DenyTransferResult
    UPDATE @SurveyListTable SET NewSurveyID = ISNULL((SELECT TOP 1 s.SurveyID FROM at.Survey s WHERE s.ActivityID = slt.ActivityID AND s.PeriodID = @NewPeriodID ORDER BY s.StartDate),0)
    FROM @SurveyListTable slt
    WHERE slt.ActivityType = @EvaluationActivityType
      AND slt.[SurveyID] = (SELECT TOP 1 s.SurveyID FROM at.Survey s WHERE s.ActivityID = slt.ActivityID AND s.PeriodID = @OldPeriodID ORDER BY s.[EndDate] DESC, s.[StartDate] DESC)
      AND NOT EXISTS (SELECT 1 FROM [at].[XC_A] xca JOIN @XCID_DenyTransferResultTable xt ON xca.[XCID] = xt.[XCID] AND slt.[ActivityID] = xca.[ActivityID])

    -- Count results per survey
    UPDATE @SurveyListTable SET ResultCount = ISNULL((SELECT isnull(Count(*),0) FROM [dbo].[Synonym_VOKAL_Result] r (NOLOCK) WHERE r.batchid = slt.batchid AND r.EntityStatusID = @ActiveEntityStatusID AND r.Deleted IS NULL),0)
    FROM @SurveyListTable slt

    BEGIN TRANSACTION
    BEGIN TRY

       OPEN c_Survey
       FETCH NEXT FROM c_Survey INTO @ReportServer, @ReportDB, @p_SurveyID, @p_BatchID, @p_NewSurveyID, @ResultCount
       WHILE @@FETCH_STATUS = 0
       BEGIN
          -- Only process if results exist
          IF @ResultCount > 0 
          BEGIN
             -- Call to RDB StartNY procedure
             --SET @ProcCall = '[' + @ReportServer + '].[' + @ReportDB + '].[dbo].[prc_VOKAL_startNY]'
             EXEC [dbo].[Synonym_prc_VOKAL_startNY] @HDID=@p_HDID, @PeriodID=@OldPeriodID, @SurveyID=@p_SurveyID, @BatchID=@p_BatchID, @NewSurveyId=@p_NewSurveyID, @RTID=@p_RTID, @StatusTypeID=@p_StatusTypeID

             -- Save ResultIDs for clear cache
             INSERT INTO #ResultIDTable(ResultID)
             SELECT ResultID FROM [dbo].[Synonym_VOKAL_Result] WITH (NOLOCK)
             WHERE BatchID = @p_BatchID

          END
          FETCH NEXT FROM c_Survey INTO @ReportServer, @ReportDB, @p_SurveyID, @p_BatchID, @p_NewSurveyID,@ResultCount
       END
       CLOSE c_Survey
       DEALLOCATE c_Survey

       COMMIT TRANSACTION
    END TRY
    BEGIN CATCH
       SET @ErrorMsg = ERROR_MESSAGE()
       ROLLBACK TRANSACTION

       IF CURSOR_STATUS('global','c_Survey') = 1
       BEGIN
          CLOSE c_Survey
          DEALLOCATE c_Survey
       END
    END CATCH

    -- Order OLAP processing service to reprocess cubes
    UPDATE at.Survey SET ReProcessOLAP = 2 WHERE SurveyID IN (SELECT SurveyID FROM @SurveyListTable WHERE ResultCount > 0) AND ReProcessOLAP != 2
    
    IF @AG_FinishedNSY > 0 AND NOT EXISTS (SELECT 1 FROM [AccessGroupMember] WHERE [AccessGroupID] = @AG_FinishedNSY AND [HDID] = @HDID)
    BEGIN
        INSERT INTO [AccessGroupMember] ([AccessGroupID], [HDID]) VALUES (@AG_FinishedNSY, @HDID)
    END

    -- Return lists for clear cache
    SELECT SurveyID, NewSurveyID FROM @SurveyListTable
    SELECT ResultID FROM #ResultIDTable
    SELECT @ErrorMsg as ErrorMsg

    DROP TABLE #ResultIDTable
END

GO
