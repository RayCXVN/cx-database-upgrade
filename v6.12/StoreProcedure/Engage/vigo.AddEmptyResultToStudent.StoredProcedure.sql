SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[vigo].[AddEmptyResultToStudent]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [vigo].[AddEmptyResultToStudent] AS' 
END
GO

ALTER PROCEDURE [vigo].[AddEmptyResultToStudent](
    @RoleID     int,
    @SurveyID   int,
    @usertype   int,
    @customerid int)
AS
BEGIN
    INSERT INTO [dbo].[Result]([StartDate]
          ,[RoleID] --vg1
          ,[UserID]
          ,[UserGroupID]--0
          ,[SurveyID]
          ,[BatchID]
          ,[LanguageID] --1
          ,[PageNo] --0
          ,[DepartmentID]
          ,[Anonymous] --0
          ,[ShowBack] --0
          ,[Email] --''
          ,[EntityStatusID]--0
          ,[LastUpdatedBy] --0
          ,[StatusTypeID] --1
          ,[CustomerID]
    )
    --declare @surveyid as int  = 2874 /* VG1 2016/2017 */,  @RoleID as int = 43 ,@usertype as int = 55 ,@customerid as int = 497 --VG1
    SELECT distinct convert(smalldatetime, getdate()) [StartDate]
          ,@RoleID [RoleID] --vg1
          ,u.UserId [UserID]
          ,0 [UserGroupID]--0
          ,@SurveyID [SurveyID]
          ,b.batchid [BatchID]
          ,1 [LanguageID] --1
          ,0 [PageNo] --0
          ,u.DepartmentID [DepartmentID]
          ,0 [Anonymous] --0
          ,0 [ShowBack] --0
          ,'' [Email] --''
          ,2 [EntityStatusID]--0 --this was Status -1 before
          ,0 [LastUpdatedBy] --0
          ,1 [StatusTypeID] --1
          ,@customerid [CustomerID]
    FROM [org].[User] u
    JOIN [org].[Department] d on d.departmentid = u.departmentid and d.customerid = u.customerid and d.customerid = @customerid
     AND d.EntityStatusID IN (1,2) and u.EntityStatusID IN (1,2) AND d.[Deleted] IS NULL AND u.[Deleted] IS NULL
    JOIN [org].[UT_U] ut on ut.userid=u.UserID and ut.usertypeid = @usertype 
    JOIN [org].[H_D] hd on hd.DepartmentID=u.DepartmentID and Hd.deleted = 0
    JOIN [org].[H_D] hdp on hd.parentid=hdp.hdid
    JOIN [at].[Batch] b on b.departmentid=hdp.departmentid and surveyid =@surveyid    and b.Status = 2 /* Added by GFP */
     AND u.DepartmentID is not null and u.userid is not null and u.customerId = @customerid /* Added by GFP */
    WHERE NOT EXISTS (SELECT * FROM [dbo].[Result] [r] WHERE [u].[userid] = [r].[userid] AND [r].[surveyid] = @surveyid AND [r].[roleid] = @RoleID);
       
END
GO
