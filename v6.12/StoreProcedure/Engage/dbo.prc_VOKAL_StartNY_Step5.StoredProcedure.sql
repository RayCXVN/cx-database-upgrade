SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[prc_VOKAL_StartNY_Step5]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [dbo].[prc_VOKAL_StartNY_Step5] AS' 
END
GO
/*  Set all Integration-schools to completed NSY process

2017-10-11 Ray:     Refactor
*/
ALTER PROCEDURE [dbo].[prc_VOKAL_StartNY_Step5](
    @SiteId int)
AS
BEGIN
    DECLARE @OwnerID AS int, @DepartmentType_School AS int, @PROP_MaintenanceMode int,
            @EntityStatusID_Active int = 0, @EntityStatusID_Inactive int = 0;

    SELECT @EntityStatusID_Active = [EntityStatusID] FROM [EntityStatus] WHERE [CodeName] = 'Active';
    SELECT @EntityStatusID_Inactive = [EntityStatusID] FROM [EntityStatus] WHERE [CodeName] = 'Inactive';

    SELECT @DepartmentType_School = [sp].value FROM [app].[SiteParameter] AS [sp] WHERE [sp].[Key] = 'Platform.Common.SchoolDepartmentTypeId' AND [sp].[SiteID] = @SiteId;
    IF (@DepartmentType_School IS NULL)
    BEGIN
        RAISERROR(N'Site param for Platform.Common.SchoolDepartmentTypeId NOT defined', 10, 1);
    END;

    SELECT @PROP_MaintenanceMode = [sp].value FROM [app].[SiteParameter] AS [sp] WHERE [sp].[Key] = 'Platform.NewSchoolYear.MaintenanceModeProp' AND [sp].[SiteID] = @SiteId;
    IF (@PROP_MaintenanceMode IS NULL)
    BEGIN
        RAISERROR(N'Site param for Platform.NewSchoolYear.MaintenanceModeProp NOT defined', 10, 1);
    END;

    SELECT @OwnerID = [s].[OwnerID] FROM [app].[Site] AS [s] WHERE [s].[SiteID] = @SiteId;
    IF (@OwnerID IS NULL)
    BEGIN
        DECLARE @errorMsg AS varchar(max)= 'Owner-id NOT defined for site '+CAST(@siteId AS varchar(3));
        RAISERROR(@errorMsg, 10, 1);
    END;

    IF (@OwnerID IS NOT NULL AND @DepartmentType_School IS NOT NULL)
    BEGIN
        DECLARE @DepartmentID int;

        DECLARE cur_department CURSOR FOR
        SELECT [d].[departmentid]
        FROM [org].[department] [d]
        JOIN [org].[dt_d] [dtd] ON [dtd].[departmentid] = [d].[departmentid] AND [dtd].[departmenttypeid] = @DepartmentType_School
        JOIN [org].[H_D] [hd] ON [hd].[DepartmentID] = [d].[DepartmentID] AND [hd].[Deleted] = 0
        WHERE [d].[ownerid] = @OwnerID
          AND [d].[customerid] IN (SELECT [customerid] FROM [org].[Customer] WHERE [HasUserINTegration] = 1)
          AND [d].[EntityStatusID] IN (@EntityStatusID_Active, @EntityStatusID_Inactive)
          AND [d].[Deleted] IS NULL; -- only not deleted schools

        OPEN cur_department;
        FETCH NEXT FROM cur_department INTO @DepartmentID;
        WHILE @@FETCH_STATUS = 0
        BEGIN
            UPDATE [pv] SET [propoptionid] = 7,  -- Fullført
                            [updated] = GETDATE()
            FROM [prop].[propvalue] [pv]
            WHERE [pv].[propertyid] = @PROP_MaintenanceMode AND [pv].[itemid] = @DepartmentID AND [propoptionid] = 6;  -- Underveis
            
            FETCH NEXT FROM cur_department INTO @DepartmentID;
        END;
        CLOSE cur_department;
        DEALLOCATE cur_department;
    END;
END;
GO