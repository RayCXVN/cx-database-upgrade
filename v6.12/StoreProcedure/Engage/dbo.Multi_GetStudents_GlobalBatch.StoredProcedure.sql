SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[Multi_GetStudents_GlobalBatch]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [dbo].[Multi_GetStudents_GlobalBatch] AS' 
END
GO
/*
    Sarah - Oct 10, 2016 - Remove column Status on User, Department and Result tables
    2017-11-23 Ray:     switch to use archetype instead of usertype
*/
ALTER PROCEDURE [dbo].[Multi_GetStudents_GlobalBatch]
    @DepartmentID int,
    @BatchID      int,
    @UserTypeID   int,
    @LanguageID   int = 1
AS
BEGIN
    SET NOCOUNT ON;
    DECLARE @ActiveEntityStatusID int = (SELECT [EntityStatusID] FROM [EntityStatus] WHERE [CodeName] = 'Active');
    DECLARE @Archetype_Learner int
    SELECT @Archetype_Learner = [ArchetypeID] FROM [dbo].[Archetype] WHERE [CodeName] = 'Learner';

    -- Insert statements for procedure here
    SELECT [u].[UserID], [d].[DepartmentID], [d].[name] AS [DepartmentName], ar.[CodeName] AS [UTName], 0 AS [UserTypeID], [u].[FirstName], [u].[LastName],
           isnull(MAX([r].[Resultid]), 0) 'ResultID', ISNULL(CONVERT(nvarchar(32), [u].[DateOfBirth], 102), '') AS [BirthDate]
    FROM [org].[Department] [d]
    JOIN [org].[User] [u] ON [u].[DepartmentID] = [d].[DepartmentID] AND u.[ArchetypeID] = @Archetype_Learner
    JOIN [dbo].[Archetype] ar ON ar.[ArchetypeID] = u.[ArchetypeID]
    LEFT JOIN [dbo].[Synonym_VOKAL_Result] [r] ON [u].[UserID] = [r].[userid] AND [r].[BatchID] = @BatchID AND [r].[EntityStatusID] = @ActiveEntityStatusID AND [r].[Deleted] IS NULL
    WHERE [u].[EntityStatusID] = @ActiveEntityStatusID AND [u].[Deleted] IS NULL
      AND [d].[DepartmentID] IN (SELECT [DepartmentID] FROM [org].[H_D] WHERE [ParentID] IN (SELECT [HDID] FROM [org].[H_D] WHERE [DepartmentID] = @DepartmentID) )
    GROUP BY [u].[UserID], [d].[DepartmentID], [d].[name], ar.[CodeName], [u].[FirstName], [u].[LastName], [u].[DateOfBirth]
    ORDER BY [d].[name], [u].[LastName], [u].[FirstName];
END;
GO