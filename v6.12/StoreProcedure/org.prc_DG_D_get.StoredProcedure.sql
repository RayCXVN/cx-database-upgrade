SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[org].[prc_DG_D_get]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [org].[prc_DG_D_get] AS' 
END
GO

ALTER PROCEDURE [org].[prc_DG_D_get]
(
	@DepartmentID int = null,
	@DepartmentGroupID int = null
)
AS
BEGIN
	SET NOCOUNT ON
	DECLARE @Err Int

	IF @DepartmentGroupID IS NULL
		SELECT
		[DepartmentGroupID],
		[DepartmentID]
		FROM [org].[DG_D]
		WHERE
		[DepartmentID] = @DepartmentID
	ELSE
		SELECT
		[DepartmentGroupID],
		[DepartmentID]
		FROM [org].[DG_D]
		WHERE
		[DepartmentGroupID] = @DepartmentGroupID
		
	Set @Err = @@Error

	RETURN @Err
END


GO
