SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[rep].[prc_BubbleText_upd]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [rep].[prc_BubbleText_upd] AS' 
END
GO

ALTER PROCEDURE [rep].[prc_BubbleText_upd]
(
	@BubbleTextID int,
	@BubbleID int,
	@No smallint,
	@FillColor varchar(16),
	@cUserid int,
	@Log smallint = 1
)
AS
BEGIN
	SET NOCOUNT ON
	DECLARE @Err Int

	UPDATE [rep].[BubbleText]
	SET
		[BubbleID] = @BubbleID,
		[No] = @No,
		[FillColor] = @FillColor
	WHERE
		[BubbleTextID] = @BubbleTextID

	IF @Log = 1 
	BEGIN 
		INSERT INTO [Log].[AuditLog] ( UserId, TableName, Type, Data, Created) 
		SELECT @cUserid,'BubbleText',1,
		( SELECT * FROM [rep].[BubbleText] 
			WHERE
			[BubbleTextID] = @BubbleTextID			 FOR XML AUTO) as data,
			getdate()
	END

	Set @Err = @@Error

	RETURN @Err
END


GO
