SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[prc_AG_AG_get]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [dbo].[prc_AG_AG_get] AS' 
END
GO

ALTER PROCEDURE [dbo].[prc_AG_AG_get]
(
	@AccessGroupID int
)
AS
BEGIN
	SET NOCOUNT ON
	DECLARE @Err Int

	SELECT
	[AG_AGID],
	[AccessGroupID],
	[ToAccessGroupID],
	[Type]
	FROM [dbo].[AG_AG]
	WHERE
	[AccessGroupID] = @AccessGroupID

	Set @Err = @@Error

	RETURN @Err
END


GO
