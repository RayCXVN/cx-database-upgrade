SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[prc_ResultQuestionOTH_get]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [dbo].[prc_ResultQuestionOTH_get] AS' 
END
GO
-- Stored Procedure

ALTER PROCEDURE [dbo].[prc_ResultQuestionOTH_get] 
(
       @SurveyID    int,   
       @DepartmentID       int
)
As 
       Select
      DISTINCT
      S.[QuestionID]      
      FROM at.[Question] S WITH (NOLOCK) 
      INNER JOIN at.Survey Su WITH (NOLOCK) ON Su.SurveyID = @SurveyID      
      LEFT OUTER JOIN at.[Access] I with (nolock) ON 
      (
         (S.[QuestionID] = I.[QuestionID]) 
      OR (S.PageID = I.PageID AND I.QuestionID IS NULL) 
      OR (I.PageID IS NULL AND I.[QuestionID] IS NULL)  
      ) 
      AND I.SurveyID = @SurveyID
      WHERE 
            I.SurveyID = @SurveyID            
            AND (I.DepartmentID = @DepartmentID OR I.DepartmentID IS NULL)
            AND (I.DepartmentTypeID IS NULL OR I.DepartmentTypeID IN (SELECT DepartmentTypeID FROM org.DT_D with (nolock)  WHERE DepartmentID = @DepartmentID))


GO
