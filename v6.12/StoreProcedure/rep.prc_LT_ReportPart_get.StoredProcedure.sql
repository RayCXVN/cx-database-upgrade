SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[rep].[prc_LT_ReportPart_get]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [rep].[prc_LT_ReportPart_get] AS' 
END
GO



ALTER PROCEDURE [rep].[prc_LT_ReportPart_get]
(
	@ReportPartID int
)
AS
BEGIN
	SET NOCOUNT ON
	DECLARE @Err Int

	SELECT
	[LanguageID],
	[ReportPartID],
	[Text],
	[Title]
	FROM [rep].[LT_ReportPart]
	WHERE
	[ReportPartID] = @ReportPartID

	Set @Err = @@Error

	RETURN @Err
END



GO
