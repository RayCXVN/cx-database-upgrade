SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[at].[prc_ResultCheckByUser]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [at].[prc_ResultCheckByUser] AS' 
END
GO
/*
    Sarah - Oct 31, 2016 - Hot fix: Get EntityStatus value on QDB
	Johnny - Des 07 2016 - Update store procedure for dynamic SQL
    Ray - 2017-02-09 - Remove cross database reference for Azure compatible
*/
ALTER PROCEDURE [at].[prc_ResultCheckByUser]
(
    @ListUserID     nvarchar(max),
    @OwnerID        int,
    @StatusTypeID	int
)
AS
BEGIN
	DECLARE @ActiveEntityStatusID INT = 0, @sqlCommand nvarchar(max)
    DECLARE @ReportServer   nvarchar(84),
            @ReportDB       nvarchar(84),
            @cmd            nvarchar(512)   
	DECLARE @LinkedDB NVARCHAR(MAX)

    SELECT @ActiveEntityStatusID = EntityStatusID FROM dbo.EntityStatus WHERE CodeName = 'Active'

    CREATE TABLE #Result_temp (UserID int,res int Default (0))
    INSERT INTO #Result_temp (UserID) SELECT value FROM dbo.funcListToTableInt(@ListUserID,',')

    DECLARE cur CURSOR READ_ONLY FOR 
    SELECT DISTINCT ReportServer, ReportDB FROM at.Survey WHERE ActivityID IN (SELECT ActivityID from at.Activity WHERE OwnerID = @OwnerID)

    OPEN cur
    FETCH NEXT FROM cur INTO @ReportServer, @ReportDB
        WHILE @@FETCH_STATUS=0
        BEGIN			
			IF @ReportDB=DB_NAME() AND @ReportServer=@@servername
				BEGIN
				SET @LinkedDB=' '
				END
			ELSE
				BEGIN
				SET @LinkedDB= '['+ @ReportServer + '].['+ @ReportDB + '].'
				END
            SET @cmd ='UPDATE #Result_temp  SET res =1 FROM #Result_temp r WHERE EXISTS (SELECT 1 FROM  '+ @LinkedDB +'dbo.Result where StatusTypeID='+CONVERT(varchar(max), @StatusTypeID)+' AND UserID =r.UserID AND r.UserID >0 AND [EntityStatusID] = '+CONVERT(NVARCHAR(14), @ActiveEntityStatusID)+' AND Deleted IS NULL)'
            EXECUTE sp_executesql @cmd
        FETCH NEXT FROM cur INTO @ReportServer, @ReportDB
        END
    CLOSE cur
    DEALLOCATE cur
    SELECT UserID,res FROM #Result_temp
    DROP TABLE #Result_temp
END

GO
