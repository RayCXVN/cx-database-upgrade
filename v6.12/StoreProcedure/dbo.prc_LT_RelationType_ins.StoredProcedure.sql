SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[prc_LT_RelationType_ins]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [dbo].[prc_LT_RelationType_ins] AS' 
END
GO

ALTER PROCEDURE [dbo].[prc_LT_RelationType_ins]
(
	@LanguageID int,
	@RelationTypeID int,
	@Name nvarchar(256),
	@Description nvarchar(max),
	@cUserid int,
	@Log smallint = 1
)
AS
BEGIN
	SET NOCOUNT ON
	DECLARE @Err Int

	INSERT INTO [dbo].[LT_RelationType]
	(
		[LanguageID],
		[RelationTypeID],
		[Name],
		[Description]
	)
	VALUES
	(
		@LanguageID,
		@RelationTypeID,
		@Name,
		@Description
	)

	Set @Err = @@Error

	IF @Log = 1 
	BEGIN 
		INSERT INTO [Log].[AuditLog] ( UserId, TableName, Type, Data, Created) 
		SELECT @cUserid,'LT_RelationType',0,
		( SELECT * FROM [dbo].[LT_RelationType] 
			WHERE
			[LanguageID] = @LanguageID AND
			[RelationTypeID] = @RelationTypeID				 FOR XML AUTO) as data,
				getdate() 
	 END

	RETURN @Err
END


GO
