SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[at].[prc_Access_get]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [at].[prc_Access_get] AS' 
END
GO
ALTER PROCEDURE [at].[prc_Access_get](
    @SurveyID int)
AS
BEGIN
    SET NOCOUNT ON;
    DECLARE @Err int;

    SELECT [AccessID], [SurveyID], ISNULL([BatchID], 0) AS 'BatchID', ISNULL([RoleID], 0) AS 'RoleID', ISNULL([DepartmentID], 0) AS 'DepartmentID',
            ISNULL([DepartmentTypeID], 0) AS 'DepartmentTypeID', ISNULL([HDID], 0) AS 'HDID', ISNULL([PageID], 0) AS 'PageID',
            ISNULL([QuestionID], 0) AS 'QuestionID', ISNULL([CustomerID], 0) AS 'CustomerID', [Type], [Mandatory], [Created], [ArchetypeID]
    FROM [at].[Access]
    WHERE [SurveyID] = @SurveyID;

    SET @Err = @@Error;

    RETURN @Err;
END;
GO