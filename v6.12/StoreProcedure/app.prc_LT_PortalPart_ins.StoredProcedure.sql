SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[app].[prc_LT_PortalPart_ins]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [app].[prc_LT_PortalPart_ins] AS' 
END
GO


ALTER PROCEDURE [app].[prc_LT_PortalPart_ins]
(
	@LanguageID int,
	@PortalPartID int,
	@Name nvarchar(256),
	@Description nvarchar(max),
	@Text ntext,
	@TextAbove nvarchar(max),
	@TextBelow nvarchar(max),
	@HeaderText nvarchar(max) = '',
	@FooterText nvarchar(max) = '',
	@BeforeContentText nvarchar(max) = '',
	@AfterContentText nvarchar(max) = '',
	@cUserid int,
	@Log smallint = 1
)
AS
BEGIN
	SET NOCOUNT ON
	DECLARE @Err Int

	INSERT INTO [app].[LT_PortalPart]
	(
		[LanguageID],
		[PortalPartID],
		[Name],
		[Description],
		[Text],
		[TextAbove],
		[TextBelow],
		[HeaderText],
		[FooterText],
		[BeforeContentText],
		[AfterContentText]
	)
	VALUES
	(
		@LanguageID,
		@PortalPartID,
		@Name,
		@Description,
		@Text,
		@TextAbove,
		@TextBelow,
		@HeaderText,
		@FooterText,
		@BeforeContentText,
		@AfterContentText
	)

	Set @Err = @@Error

	IF @Log = 1 
	BEGIN 
		INSERT INTO [Log].[AuditLog] ( UserId, TableName, Type, Data, Created) 
		SELECT @cUserid,'LT_PortalPart',0,
		( SELECT * FROM [app].[LT_PortalPart] 
			WHERE
			[LanguageID] = @LanguageID AND
			[PortalPartID] = @PortalPartID				 FOR XML AUTO) as data,
				getdate() 
	 END

	RETURN @Err
END



GO
