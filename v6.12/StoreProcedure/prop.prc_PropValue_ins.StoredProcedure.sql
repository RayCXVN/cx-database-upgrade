SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[prop].[prc_PropValue_ins]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [prop].[prc_PropValue_ins] AS' 
END
GO

ALTER PROCEDURE [prop].[prc_PropValue_ins]
(
	@PropValueID int = null output,
	@PropertyID int,
	@PropOptionID int = null,
	@PropFileID int = null,
	@No smallint,
	@Created datetime output,
	@CreatedBy INT=NULL,
	@Updated DATETIME output,
	@UpdatedBy INT=NULL,
	@ItemID int,
	@Value ntext,
	@cUserid int,
	@Log smallint = 1
)

AS
BEGIN
	SET NOCOUNT ON
	DECLARE @Err Int

	SET @Created = getdate()
	SET @Updated = getdate()
	
	INSERT INTO [prop].[PropValue]
	(
		[PropertyID],
		[PropOptionID],
		[PropFileID],
		[No],
		[Created],
		[CreatedBy],
		[Updated],
		[UpdatedBy],
		[ItemID],
		[Value]
	)
	VALUES
	(
		@PropertyID,
		@PropOptionID,
		@PropFileID,
		@No,
		@Created,
		@CreatedBy,
		@Updated,
		@UpdatedBy,
		@ItemID,
		@Value
	)

	Set @Err = @@Error
	Set @PropValueID = scope_identity()

	IF @Log = 1 
	BEGIN 
		INSERT INTO [Log].[AuditLog] ( UserId, TableName, Type, Data, Created) 
		SELECT @cUserid,'PropValue',0,
		( SELECT * FROM [prop].[PropValue] 
			WHERE
			[PropValueID] = @PropValueID				 FOR XML AUTO) as data,
				getdate() 
	 END

	RETURN @Err
END


GO
