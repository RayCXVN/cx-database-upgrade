SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[app].[prc_LoginService_User_get]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [app].[prc_LoginService_User_get] AS' 
END
GO


ALTER PROCEDURE [app].[prc_LoginService_User_get]  
(  
 @UserID int  
)  
AS  
BEGIN  
 SET NOCOUNT ON  
 DECLARE @Err Int  
  
 SELECT  
 [LoginServiceID]  
    ,[UserID]  
    ,[PrimaryClaimValue]  
    ,[Created]  
 FROM [app].[LoginService_User]  
 WHERE  
 [UserID] = @UserID  
  
 Set @Err = @@Error  
  
 RETURN @Err  
END  


GO
