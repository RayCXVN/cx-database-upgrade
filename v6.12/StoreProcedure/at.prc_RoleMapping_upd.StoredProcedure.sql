SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[at].[prc_RoleMapping_upd]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [at].[prc_RoleMapping_upd] AS' 
END
GO


ALTER PROCEDURE [at].[prc_RoleMapping_upd]
(
    @RMID int,
	@RoleID int,
	@ActivityID int = null,
	@SurveyID int = null,
	@UserTypeID int,
	@cUserid int,
	@Log smallint = 1
)
AS
BEGIN
	SET NOCOUNT ON
	DECLARE @Err Int

	UPDATE [at].[RoleMapping]
	SET
		[RoleID] = @RoleID,
	    [ActivityID] = @ActivityID,	
		[SurveyID] = @SurveyID,
		[UserTypeID] = @UserTypeID
	WHERE
		[RMID] = @RMID 
	IF @Log = 1 
	BEGIN 
		INSERT INTO [Log].[AuditLog] ( UserId, TableName, Type, Data, Created) 
		SELECT @cUserid,'RoleMapping',1,
		( SELECT * FROM [at].[RoleMapping] 
			WHERE
			[RoleID] = @RoleID AND
			[SurveyID] = @SurveyID			 FOR XML AUTO) as data,
			getdate()
	END

	Set @Err = @@Error

	RETURN @Err
END



GO
