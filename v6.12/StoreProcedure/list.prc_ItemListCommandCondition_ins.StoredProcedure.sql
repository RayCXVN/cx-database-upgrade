SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[list].[prc_ItemListCommandCondition_ins]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [list].[prc_ItemListCommandCondition_ins] AS' 
END
GO
ALTER PROCEDURE [list].[prc_ItemListCommandCondition_ins]
(
	@ItemListCommandConditionID int = null output,
	@ItemListCommandID int,
	@Type nvarchar(32),
	@Param nvarchar(max),
	@Value nvarchar(32),
    @cUserid int,
    @Log smallint = 1
)    
AS
BEGIN
	SET NOCOUNT ON;
	DECLARE @Err Int

	INSERT INTO [list].[ItemListCommandCondition]
           ([ItemListCommandID]
           ,[Type]
           ,[Param]
           ,[Value])
     VALUES
           (@ItemListCommandID
           ,@Type
           ,@Param
           ,@Value)

     Set @Err = @@Error
	 Set @ItemListCommandConditionID = scope_identity()
	 IF @Log = 1 
	 BEGIN 
		INSERT INTO [Log].[AuditLog] ( UserId, TableName, Type, Data, Created) 
		SELECT @cUserid,'ItemListCommandCondition',0,
		( SELECT * FROM [list].[ItemListCommandCondition]
			WHERE
			[ItemListCommandConditionID] = @ItemListCommandConditionID				 FOR XML AUTO) as data,
				getdate() 
	 END

	 RETURN @Err
END

GO
