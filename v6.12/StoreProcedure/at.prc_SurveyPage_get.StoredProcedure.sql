SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[at].[prc_SurveyPage_get]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [at].[prc_SurveyPage_get] AS' 
END
GO
/* ------------------------------------------------------------
   PROCEDURE:    prc_SurveyPage_get

   Description: 

   AUTHOR:       MOD
   ------------------------------------------------------------ */

ALTER PROCEDURE [at].[prc_SurveyPage_get]
(
	@SurveyID	int
)
AS
BEGIN
	SET NOCOUNT ON
	DECLARE @Err Int

	Select DISTINCT
	P.[PageID],
	P.[ActivityID],
	P.[No],
	P.[Created]
	FROM [Page] P
	INNER JOIN at.Survey S ON P.ActivityID = S.ActivityID
	WHERE [SurveyID] = @SurveyID AND
	(@SurveyID IN (Select SurveyID FROM at.Access WHERE PageID IS NULL AND QuestionID IS NULL))
	OR
	(PageID IN (SELECT PageID FROM at.Access WHERE SurveyID = @SurveyID))
	OR
	(PageID IN (SELECT Q.PageID FROM at.Access A INNER JOIN Question Q ON A.QuestionID = Q.QuestionID WHERE SurveyID = @SurveyID))
	order by P.[No]
	Set @Err = @@Error

	RETURN @Err
End


GO
