SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[note].[prc_Note_PagingGet]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [note].[prc_Note_PagingGet] AS' 
END
GO
/*
2017-06-14 Ray:     Improve performance
2017-09-19 Sarah:   prc_Note_getByAccess get one more column StartDate 
*/
ALTER PROCEDURE [note].[prc_Note_PagingGet]
(
	@UserID                     int,
	@ListNoteTypeID             nvarchar(max)='',
	@LanguageID                 int,
	@FallbackLanguageID         int,
	@RowCount                   int output,
	@RowIndex                   int = 0,
	@PageSize                   int = 0,
	@HDID                       int = 0,
	@NotifyAtNextLogin          bit = 0,
    @NoteIDList                 varchar(max) = '',
    @FilterSourceTableTypeID    int = 0,
    @FilterSourceItemID         int = 0
)
AS
BEGIN
    SET NOCOUNT ON
    DECLARE @Err Int, @ToRow int
    DECLARE @NoteTypeID int, @CheckValidInDateRange bit = 1
    DECLARE @Note TABLE (NoteID int, CreatedBy int, Created datetime, AccessType int, NotifyAtNextLogin bit, Active bit, StartDate datetime) 
    
    DECLARE cur CURSOR READ_ONLY FOR
    SELECT * FROM dbo.funcListToTableInt(@ListNoteTypeID,',')
    OPEN cur
    FETCH NEXT FROM cur INTO  @NoteTypeID
        WHILE @@FETCH_STATUS =0
        BEGIN
            INSERT INTO @Note EXEC [note].[prc_Note_getByAccess] @UserID, @NoteTypeID, @HDID, @NotifyAtNextLogin, @NoteIDList, @CheckValidInDateRange, @FilterSourceTableTypeID, @FilterSourceItemID
            FETCH NEXT FROM cur INTO  @NoteTypeID
        END
    CLOSE cur
    DEALLOCATE cur
    
    SELECT @RowCount = COUNT(NoteID) FROM @Note WHERE Active = 1
    IF @RowIndex > @RowCount
    BEGIN
	   RETURN
    END

    SET @ToRow = @RowIndex + @PageSize - 1
    IF @ToRow > @RowCount OR (@RowIndex = 0 AND @PageSize = 0)  -- Exceed total number of rows or get all notes
    BEGIN
	   SET @ToRow = @RowCount
    END
    
    SELECT v.NoteID, ISNULL(ltn.[Subject],fblt.[Subject]) AS [Subject], ISNULL(ltn.Note,fblt.Note) AS [Note], v.CreatedBy, v.Created, v.AccessType, n2.Changed,
           (SELECT TOP 1 rl.DateRead FROM note.NoteReadLog rl WITH (NOLOCK) WHERE rl.NoteID = v.NoteID AND rl.UserID = @UserID) DateRead,
           v.StartDate
    FROM (
	   SELECT row_number() OVER (ORDER BY n.Created DESC) AS RowNum, n.NoteID, n.AccessType, n.CreatedBy, n.Created, n.StartDate
	   FROM @Note n
	   WHERE (n.NotifyAtNextLogin = 1 OR @NotifyAtNextLogin = 0) AND n.Active = 1
    ) v  JOIN note.Note n2 WITH (NOLOCK) ON v.NoteID = n2.NoteID AND v.RowNum BETWEEN @RowIndex AND @ToRow
    LEFT JOIN note.LT_Note ltn WITH (NOLOCK) ON v.NoteID = ltn.NoteID AND ltn.LanguageID = @LanguageID  
	LEFT JOIN note.LT_Note fblt WITH (NOLOCK) ON v.NoteID = fblt.NoteID AND fblt.LanguageID = @FallbackLanguageID
	ORDER BY  v.Created DESC

    Set @Err = @@Error
    RETURN @Err
END

GO
