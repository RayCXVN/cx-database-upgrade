SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[at].[prc_A_S_upd]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [at].[prc_A_S_upd] AS' 
END
GO
ALTER PROCEDURE [at].[prc_A_S_upd] (
	@ASID INT,
	@ActivityID INT,
	@StatusTypeID INT,
	@No SMALLINT,
	@cUserid INT,
	@Log SMALLINT = 1
	)
AS
BEGIN
	SET NOCOUNT ON

	DECLARE @Err INT

	UPDATE [at].[A_S]
	SET [ActivityID] = @ActivityID,
		[StatusTypeID] = @StatusTypeID,
		[No] = @No
	WHERE [ASID] = @ASID

	IF @Log = 1
	BEGIN
		INSERT INTO [Log].[AuditLog] (
			UserId,
			TableName,
			Type,
			Data,
			Created
			)
		SELECT @cUserid,
			'A_S',
			1,
			(
				SELECT *
				FROM [at].[A_S]
				WHERE [ASID] = @ASID
				FOR XML AUTO
				) AS data,
			getdate()
	END

	SET @Err = @@Error

	RETURN @Err
END


GO
