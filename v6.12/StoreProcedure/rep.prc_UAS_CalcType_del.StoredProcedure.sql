SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[rep].[prc_UAS_CalcType_del]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [rep].[prc_UAS_CalcType_del] AS' 
END
GO
ALTER PROCEDURE [rep].[prc_UAS_CalcType_del]       
(        
 @UserActivitySettingID int,
 @ReportCalcTypeID int,
 @cUserid int,
 @Log smallint = 1
)        
AS        
BEGIN        
 SET NOCOUNT ON        
 DECLARE @Err Int        
        
 IF @Log = 1         
 BEGIN         
  INSERT INTO [Log].[AuditLog] ( UserId, TableName, Type, Data, Created)         
  SELECT @cUserid,'UAS_CalcType',2,        
  (SELECT * FROM [rep].[UAS_CalcType]
   WHERE        
		[UserActivitySettingID] = @UserActivitySettingID
		AND [ReportCalcTypeID] = @ReportCalcTypeID
    FOR XML AUTO) as data,        
   getdate()         
 END         
        
        
 DELETE FROM [rep].[UAS_CalcType]
 WHERE        
	[UserActivitySettingID] = @UserActivitySettingID
	AND [ReportCalcTypeID] = @ReportCalcTypeID
 Set @Err = @@Error        
        
 RETURN @Err        
END 

GO
