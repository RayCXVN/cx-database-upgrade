SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[app].[prc_SiteViewSetting_get]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [app].[prc_SiteViewSetting_get] AS' 
END
GO
ALTER PROCEDURE [app].[prc_SiteViewSetting_get]      
 @SiteID int      
AS      
BEGIN      
 SET NOCOUNT ON;      
 DECLARE @Err Int          
 SELECT       
	 [SiteViewSettingID]
      ,[SiteID]
      ,[No]
      ,[NumOfDecimals]
      ,[TableIsSelectable]
      ,[TableIsDefaultSelected]
      ,[ChartIsSelectable]
      ,[ChartIsDefaultSelected]
      ,[AverageIsSelectable]
      ,[AverageIsDefaultSelected]
      ,[TrendIsSelectable]
      ,[TrendIsDefaultSelected]
      ,[FrequencyIsSelectable]
      ,[FrequencyIsDefaultSelected]
      ,[NCountIsSelectable]
      ,[NCountIsDefaultSelected]
      ,[StandardDeviationIsSelectable]
      ,[StandardDeviationIsDefaultSelected]
      ,[Created]
      ,[DefaultMinResultDotted]
      ,[DefaultMinQuestionCountDotted]
 FROM             
  [SiteViewSetting]      
 WHERE        
  [SiteViewSetting].SiteID = @SiteID      
   ORDER BY [No]  
 Set @Err = @@Error      
      
 RETURN @Err      
END 

GO
