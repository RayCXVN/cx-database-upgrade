SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[at].[prc_Choice_copy]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [at].[prc_Choice_copy] AS' 
END
GO

--prc_CopyChoice 7,173

ALTER proc [at].[prc_Choice_copy]
(
  @FromSurveyID int,
  @ToSurveyID int
) as
	Declare @ChoiceID as int 
	Declare @NewChoiceID as int 
	Declare @ActionID as int
	Declare @NewActionID as int

	declare ChoiceList cursor for
	select ChoiceID from at.Choice where surveyid = @FromSurveyID 
	OPEN ChoiceList
	FETCH NEXT FROM ChoiceList 
	INTO @ChoiceID
		
	WHILE @@FETCH_STATUS = 0
	BEGIN
	    Insert into choice(SurveyID, Name, No, DepartmentID, DepartmentTypeID, RoleID, Type, Created)
	    select @ToSurveyID, Name, No, DepartmentID, DepartmentTypeID, RoleID, Type,getdate() 
	    from choice where choiceid = @ChoiceID
	
	    select @NewChoiceID = SCOPE_IDENTITY()
	    
	    insert into at.Combi(ChoiceID, QuestionID, AlternativeID, ValueFormula)
	    select @NewChoiceID , QuestionID, AlternativeID, ValueFormula
	    from at.Combi where choiceid = @ChoiceID
	    
		declare ActionList cursor for
		select ActionID from [Action] where ChoiceID = @ChoiceID
		OPEN ActionList
		FETCH NEXT FROM ActionList 
		INTO @ActionID

		WHILE @@FETCH_STATUS = 0
		BEGIN
			insert into at.Action(ChoiceID, No, PageID, QuestionID, AlternativeID, Type)
			select	  @NewChoiceID, No, PageID, QuestionID, AlternativeID, Type
			from at.Action where ActionID = @ActionID
	    
			select @NewActionID = SCOPE_IDENTITY()

			insert into at.LT_Action(LanguageID, ActionID, Response)
			select	  LanguageID, @NewActionID, Response
			from at.LT_Action where ActionID = @ActionID

			FETCH NEXT FROM ActionList 
			INTO @ActionID
		END
		CLOSE ActionList
		DEALLOCATE ActionList

	    FETCH NEXT FROM ChoiceList 
	    INTO @ChoiceID
	   
	END
	CLOSE ChoiceList
	DEALLOCATE ChoiceList
	


	

GO
