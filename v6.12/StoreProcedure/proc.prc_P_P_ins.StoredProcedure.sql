SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[proc].[prc_P_P_ins]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [proc].[prc_P_P_ins] AS' 
END
GO


ALTER PROCEDURE [proc].[prc_P_P_ins]
(
	@ProcessID int,
	@ToProcessID int,
	@cUserid int,
	@Log smallint = 1
)
AS
BEGIN
	SET NOCOUNT ON
	DECLARE @Err Int

	INSERT INTO [proc].[P_P]
	(
		[ProcessID],
		[ToProcessID]
	)
	VALUES
	(
		@ProcessID,
		@ToProcessID
	)

	Set @Err = @@Error

	IF @Log = 1 
	BEGIN 
		INSERT INTO [Log].[AuditLog] ( UserId, TableName, Type, Data, Created) 
		SELECT @cUserid,'P_P',0,
		( SELECT * FROM [proc].[P_P] 
			WHERE
			[ProcessID] = @ProcessID AND
			[ToProcessID] = @ToProcessID				 FOR XML AUTO) as data,
				getdate() 
	 END

	RETURN @Err
END

GO
