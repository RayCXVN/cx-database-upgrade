SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[proc].[prc_ProcessAccess_upd]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [proc].[prc_ProcessAccess_upd] AS' 
END
GO

ALTER PROCEDURE [proc].[prc_ProcessAccess_upd]
(
	@ProcessAccessID int,
	@ProcessID int,
	@DepartmentID int,
	@cUserid int,
	@Log smallint = 1
)
AS
BEGIN
	SET NOCOUNT ON
	DECLARE @Err Int

	UPDATE [proc].[ProcessAccess]
	SET
		[ProcessID] = @ProcessID,
		[DepartmentID] = @DepartmentID
	WHERE
		[ProcessAccessID] = @ProcessAccessID

	IF @Log = 1 
	BEGIN 
		INSERT INTO [Log].[AuditLog] ( UserId, TableName, Type, Data, Created) 
		SELECT @cUserid,'ProcessAccess',1,
		( SELECT * FROM [proc].[ProcessAccess] 
			WHERE
			[ProcessAccessID] = @ProcessAccessID			 FOR XML AUTO) as data,
			getdate()
	END

	Set @Err = @@Error

	RETURN @Err
END


GO
