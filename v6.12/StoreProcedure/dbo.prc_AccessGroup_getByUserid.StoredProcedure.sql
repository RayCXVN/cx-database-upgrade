SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[prc_AccessGroup_getByUserid]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [dbo].[prc_AccessGroup_getByUserid] AS' 
END
GO
/*
    exec [dbo].[prc_AccessGroup_getByUserid]  15, 22, 6, null, 5 
    exec [dbo].[prc_AccessGroup_getByUserid]  15, 22, 6, null, null
    exec [dbo].[prc_AccessGroup_getByUserid]  9, 107824, 23824, 0, 61
    2017-10-11 Johnny:  add ArchetypeID for AccessGroupMember
    2017-12-01 Ray:     Fix missing column ArchetypeID in temp table #RecursiveAGMember
    2018-07-09 Ray:     Replace UG_U by UGMember
*/
ALTER PROCEDURE [dbo].[prc_AccessGroup_getByUserid](
    @OwnerId      int,
    @UserId       int,
    @Departmentid int,
    @RoleId       int,
    @CustomerID   int = NULL)
AS
BEGIN
    SET NOCOUNT ON;

    DECLARE @Path nvarchar(2048), @UserDepID int, @NestedLevel int = 0, @EntityStatus_Active int;
    DECLARE @HDIDS TABLE
    (
        [HDID] int
    );
    DECLARE @UserTypeTable TABLE
    (
        [UserTypeID] int
    );
    DECLARE @ArchetypeTable TABLE
    (
        [ArchetypeID] int
    );
    DECLARE @CheckedAGMember TABLE
    (
        [AccessGroupMemberID] int
    );

    SELECT @UserDepID = [Departmentid] FROM [org].[user] WHERE [userid] = @Userid;
    SELECT @EntityStatus_Active = [EntityStatusID] FROM [dbo].[EntityStatus] WHERE [CodeName] = 'Active';

    DECLARE Table_Cursor CURSOR READ_ONLY
    FOR SELECT RIGHT([PATH], LEN([PATH]) - 1) FROM [org].[H_D] WHERE [Departmentid] = @Departmentid;

    CREATE TABLE [#AccessGroupByUser]
    (
        [AccessGroupID] int,
        [OwnerID]       int,
        [No]            smallint,
        [Created]       smalldatetime
    );
    CREATE TABLE [#RecursiveAGMember]
    (
        [AccessGroupMemberID] int,
        [AccessGroupID]       int,
        [UserID]              int,
        [UserTypeID]          int,
        [DepartmentID]        int,
        [HDID]                int,
        [UserGroupID]         int,
        [RoleID]              int,
        [DepartmentTypeID]    int,
        [DepartmentGroupID]   int,
        [CustomerID]          int,
        [ExtGroupID]          int,
        [ArchetypeID]         int
    );

    OPEN Table_Cursor;
    FETCH NEXT FROM Table_Cursor INTO @Path;
    WHILE(@@FETCH_STATUS = 0)
    BEGIN
        PRINT @Path;
        INSERT INTO @HDIDS ([HDID])
        SELECT VALUE FROM [dbo].[funcListToTableInt] (@Path, '\');
        FETCH NEXT FROM Table_Cursor INTO @Path;
    END;
    CLOSE Table_Cursor;
    DEALLOCATE Table_Cursor;

    SET @Path = '';
    IF @UserDepID = @Departmentid
    BEGIN
        INSERT INTO @UserTypeTable ([UserTypeID])
        SELECT [UserTypeID] FROM [org].[UT_U] WHERE [UserId] = @UserId;
    END;
    ELSE
    BEGIN
        INSERT INTO @UserTypeTable ([UserTypeID])
        SELECT [udut].[UserTypeID]
        FROM [org].[U_D] [ud]
        JOIN [org].[U_D_UT] [udut] ON [ud].[userid] = @userid AND [ud].[departmentid] = @Departmentid AND [ud].[u_did] = [udut].[u_did];
    END;

    INSERT INTO @ArchetypeTable ([ArchetypeID])
    SELECT [ArchetypeID] FROM [org].[Department] WHERE [DepartmentID] = @Departmentid;
    INSERT INTO @ArchetypeTable ([ArchetypeID])
    SELECT [ArchetypeID] FROM [org].[User] WHERE [UserID] = @UserId;

    -- Get all leaf level Access group members
    INSERT INTO [#AccessGroupByUser]
    SELECT [AG].[AccessGroupID], [AG].[OwnerID], [AG].[No], [AG].[Created]
    FROM [dbo].[AccessGroup] [AG]
    JOIN [dbo].[AccessGroupMember] [AGM] ON [AG].[AccessGroupID] = [AGM].[AccessGroupID] AND [AG].[OwnerId] = @OwnerID
    WHERE ([AGM].[UserId] IS NULL OR [AGM].[userid] = @Userid)
    AND ([AGM].[RoleId] = @RoleID OR [AGM].[RoleId] IS NULL)
    AND ([AGM].[DepartmentTypeID] IN (SELECT [DepartmentTypeID] FROM [org].[DT_D] WHERE [DepartmentId] = @Departmentid) OR [AGM].[DepartmentTypeID] IS NULL)
    AND ([AGM].[UserGroupID] IN (SELECT [UserGroupID] FROM [org].[UGMember] WHERE [UserId] = @UserId AND [EntityStatusID] = @EntityStatus_Active AND [Deleted] IS NULL)
         OR [AGM].[UserGroupID] IS NULL)
    AND ([AGM].[UserTypeID] IN (SELECT [UserTypeID] FROM @UserTypeTable) OR [AGM].[UserTypeID] IS NULL)
    AND ([AGM].[ArchetypeID] IN (SELECT [ArchetypeID] FROM @ArchetypeTable) OR [AGM].[ArchetypeID] IS NULL)
    AND ([AGM].[DepartmentGroupID] IN (SELECT [DepartmentGroupID] FROM [org].[DG_D] WHERE [DepartmentId] = @Departmentid) OR [AGM].[DepartmentGroupID] IS NULL)
    AND ([AGM].[HDID] IN (SELECT [HDID] FROM @HDIDS) OR [AGM].[HDID] IS NULL)
    AND ([AGM].[Departmentid] = @Departmentid OR [AGM].[Departmentid] IS NULL)
    AND ([AGM].[CustomerID] = @CustomerID OR [AGM].[CustomerID] IS NULL)
    AND [AGM].[ExtGroupID] IS NULL;

    INSERT INTO [#RecursiveAGMember] ([AccessGroupMemberID], [AccessGroupID], [UserID], [UserTypeID], [DepartmentID], [HDID], [UserGroupID], [RoleID],
                                      [DepartmentTypeID], [DepartmentGroupID], [CustomerID], [ExtGroupID], [ArchetypeID])
    SELECT [AccessGroupMemberID], [AccessGroupID], [UserID], [UserTypeID], [DepartmentID], [HDID], [UserGroupID], [RoleID],
           [DepartmentTypeID], [DepartmentGroupID], [CustomerID], [ExtGroupID], [ArchetypeID]
    FROM [dbo].[AccessGroupMember] [AGM]
    WHERE [AGM].[ExtGroupID] > 0;

    -- Get all nested level Access group members
    WHILE EXISTS (SELECT 1 FROM [#RecursiveAGMember] [AGM]
                           WHERE EXISTS (SELECT 1 FROM [#AccessGroupByUser] [AG1] WHERE [AG1].[AccessGroupID] = [AGM].[ExtGroupID])
                             AND NOT EXISTS (SELECT 1 FROM [#AccessGroupByUser] [AG2] WHERE [AG2].[AccessGroupID] = [AGM].[AccessGroupID])
                 )
      AND @NestedLevel < 32
    BEGIN
        SET @NestedLevel+=1;

        INSERT INTO @CheckedAGMember ([AccessGroupMemberID])
        SELECT [AccessGroupMemberID]
        FROM [#RecursiveAGMember] [AGM]
        WHERE EXISTS (SELECT 1 FROM [#AccessGroupByUser] [AG1] WHERE [AG1].[AccessGroupID] = [AGM].[ExtGroupID])
        AND NOT EXISTS (SELECT 1 FROM [#AccessGroupByUser] [AG2] WHERE [AG2].[AccessGroupID] = [AGM].[AccessGroupID]);

        INSERT INTO [#AccessGroupByUser]
        SELECT [AG].[AccessGroupID], [AG].[OwnerID], [AG].[No], [AG].[Created]
        FROM [dbo].[AccessGroup] [AG]
        JOIN [#RecursiveAGMember] [AGM] ON [AG].[AccessGroupID] = [AGM].[AccessGroupID] AND [AG].[OwnerId] = @OwnerID
        WHERE ([AGM].[UserId] IS NULL OR [AGM].[userid] = @Userid)
        AND ([AGM].[RoleId] = @RoleID OR [AGM].[RoleId] IS NULL)
        AND ([AGM].[DepartmentTypeID] IN (SELECT [DepartmentTypeID] FROM [org].[DT_D] WHERE [DepartmentId] = @Departmentid) OR [AGM].[DepartmentTypeID] IS NULL)
        AND ([AGM].[UserGroupID] IN (SELECT [UserGroupID] FROM [org].[UGMember] WHERE [UserId] = @UserId AND [EntityStatusID] = @EntityStatus_Active AND [Deleted] IS NULL)
             OR [AGM].[UserGroupID] IS NULL)
        AND ([AGM].[UserTypeID] IN (SELECT [UserTypeID] FROM @UserTypeTable) OR [AGM].[UserTypeID] IS NULL)
        AND ([AGM].[ArchetypeID] IN (SELECT [ArchetypeID] FROM @ArchetypeTable) OR [AGM].[ArchetypeID] IS NULL)
        AND ([AGM].[DepartmentGroupID] IN (SELECT [DepartmentGroupID] FROM [org].[DG_D] WHERE [DepartmentId] = @Departmentid) OR [AGM].[DepartmentGroupID] IS NULL)
        AND ([AGM].[HDID] IN (SELECT [HDID] FROM @HDIDS) OR [AGM].[HDID] IS NULL)
        AND ([AGM].[Departmentid] = @Departmentid OR [AGM].[Departmentid] IS NULL)
        AND ([AGM].[CustomerID] = @CustomerID OR [AGM].[CustomerID] IS NULL)
        AND EXISTS (SELECT 1 FROM [#AccessGroupByUser] [AG1] WHERE [AG1].[AccessGroupID] = [AGM].[ExtGroupID])
        AND NOT EXISTS (SELECT 1 FROM [#AccessGroupByUser] [AG2] WHERE [AG2].[AccessGroupID] = [AGM].[AccessGroupID]);

        DELETE [AGM] FROM [#RecursiveAGMember] [AGM] WHERE [AccessGroupMemberID] IN (SELECT [AccessGroupMemberID] FROM @CheckedAGMember);

        DELETE FROM @CheckedAGMember;
    END;

    PRINT 'Nested level: '+CONVERT(nvarchar(5), @NestedLevel);
    SELECT DISTINCT * FROM [#AccessGroupByUser];
    DROP TABLE [#AccessGroupByUser];
    DROP TABLE [#RecursiveAGMember];
END;
GO