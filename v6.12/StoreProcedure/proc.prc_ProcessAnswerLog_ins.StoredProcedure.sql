SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[proc].[prc_ProcessAnswerLog_ins]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [proc].[prc_ProcessAnswerLog_ins] AS' 
END
GO

ALTER PROCEDURE [proc].[prc_ProcessAnswerLog_ins]
(
	@ProcessAnswerLogID int = null output,
	@UserID int,
	@ProcessAnswerID int,
	@ProcessLevelID int,
	@Comment nvarchar(max),
	@cUserid int,
	@Log smallint = 1
)
AS
BEGIN
	SET NOCOUNT ON
	DECLARE @Err Int

	INSERT INTO [proc].[ProcessAnswerLog]
	(
		[UserID],
		[ProcessAnswerID],
		[ProcessLevelID],
		[Comment]
	)
	VALUES
	(
		@UserID,
		@ProcessAnswerID,
		@ProcessLevelID,
		@Comment
	)

	Set @Err = @@Error
	Set @ProcessAnswerLogID = scope_identity()

	IF @Log = 1 
	BEGIN 
		INSERT INTO [Log].[AuditLog] ( UserId, TableName, Type, Data, Created) 
		SELECT @cUserid,'ProcessAnswerLog',0,
		( SELECT * FROM [proc].[ProcessAnswerLog] 
			WHERE
			[ProcessAnswerLogID] = @ProcessAnswerLogID				 FOR XML AUTO) as data,
				getdate() 
	 END

	RETURN @Err
END


GO
