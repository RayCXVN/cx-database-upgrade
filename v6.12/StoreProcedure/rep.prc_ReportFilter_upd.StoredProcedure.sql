SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[rep].[prc_ReportFilter_upd]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [rep].[prc_ReportFilter_upd] AS' 
END
GO


ALTER PROCEDURE [rep].[prc_ReportFilter_upd]
(
	@ActivityID int,
	@QuestionID int,
	@FilterDim bit,
	@cUserid int,
	@Log smallint = 1
)
AS
BEGIN
	SET NOCOUNT ON
	DECLARE @Err Int

	UPDATE [rep].[ReportFilter]
	SET
		[ActivityID] = @ActivityID,
		[QuestionID] = @QuestionID,
		[FilterDim] = @FilterDim 
	WHERE
		[ActivityID] = @ActivityID AND
		[QuestionID] = @QuestionID

	IF @Log = 1 
	BEGIN 
		INSERT INTO [Log].[AuditLog] ( UserId, TableName, Type, Data, Created) 
		SELECT @cUserid,'ReportFilter',1,
		( SELECT * FROM [rep].[ReportFilter] 
			WHERE
			[ActivityID] = @ActivityID AND
			[QuestionID] = @QuestionID			 FOR XML AUTO) as data,
			getdate()
	END

	Set @Err = @@Error

	RETURN @Err
END



GO
