SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[mea].[prc_LT_MeasureTemplate_del]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [mea].[prc_LT_MeasureTemplate_del] AS' 
END
GO
ALTER PROCEDURE [mea].[prc_LT_MeasureTemplate_del]
	@LanguageID int,
	@MeasureTemplateID int,
	@cUserid int,
    @Log smallint = 1
AS
BEGIN
	SET NOCOUNT ON;
	DECLARE @Err Int

    DELETE [mea].[LT_MeasureTemplate]
    WHERE     [MeasureTemplateID] = @MeasureTemplateID

    Set @Err = @@Error
	IF @Log = 1 
	BEGIN 
		INSERT INTO [Log].[AuditLog] ( UserId, TableName, Type, Data, Created) 
		SELECT @cUserid,'LT_MeasureTemplate',0,
		( SELECT * FROM [mea].[LT_MeasureTemplate]
			WHERE
			([MeasureTemplateID] = @MeasureTemplateID AND [LanguageID] = @LanguageID) 				 FOR XML AUTO) as data,
				getdate() 
	END

	RETURN @Err       
END

GO
