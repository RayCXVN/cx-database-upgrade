SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[rep].[prc_Selection_upd]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [rep].[prc_Selection_upd] AS' 
END
GO
ALTER PROCEDURE [rep].[prc_Selection_upd]
(
@SelectionID int,
@SelectionGroupID int,
@Active bit,
@CustomName bit,
@Name nvarchar(256),
@ActivityID INT=NULL,
@Status smallint,
@StartDate DATETIME=NULL,
@EndDate DATETIME=NULL,
@LevelGroupID int = null,
@No smallint = 0,
@HierarchyID INT=NULL,
@cUserid int,
@Log smallint = 1,
@SaveRec bit = 0
)
AS
BEGIN
SET NOCOUNT ON
DECLARE @Err Int
UPDATE [rep].[Selection]
SET
[SelectionGroupID] = @SelectionGroupID,
[Active] = @Active,
[CustomName] = @CustomName,
[Name] = @Name,
[ActivityID] = @ActivityID,
[Status] = @Status,
[StartDate] = @StartDate,
[EndDate] = @EndDate,
[LevelGroupID] = @LevelGroupID,
[No] = @No,
[HierarchyID] = @HierarchyID
WHERE
[SelectionID] = @SelectionID
IF @Log = 1
BEGIN
INSERT INTO [Log].[AuditLog] ( UserId, TableName, Type, Data, Created)
SELECT @cUserid,'Selection',1,
( SELECT * FROM [rep].[Selection]
WHERE
[SelectionID] = @SelectionID FOR XML AUTO) as data,
getdate()
END
IF @SaveRec = 1
BEGIN
DELETE rep.Selection_D WHERE SelectionID = @SelectionID
DELETE rep.Selection_DG WHERE SelectionID = @SelectionID
DELETE rep.Selection_DT WHERE SelectionID = @SelectionID
DELETE rep.Selection_QFilter WHERE SelectionID = @SelectionID
DELETE rep.Selection_Result WHERE SelectionID = @SelectionID
DELETE rep.Selection_Role WHERE SelectionID = @SelectionID
DELETE rep.Selection_Survey WHERE SelectionID = @SelectionID
DELETE rep.Selection_User WHERE SelectionID = @SelectionID
END
Set @Err = @@Error
RETURN @Err
END

GO
