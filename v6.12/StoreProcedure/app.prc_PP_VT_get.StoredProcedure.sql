SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[app].[prc_PP_VT_get]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [app].[prc_PP_VT_get] AS' 
END
GO

ALTER PROCEDURE [app].[prc_PP_VT_get]
(
	@PortalPartID int
)
AS
BEGIN
	SET NOCOUNT ON
	DECLARE @Err Int

	SELECT
	[PortalPartID],
	[ViewTypeID],
	[No]
	FROM [app].[PP_VT]
	WHERE
	[PortalPartID] = @PortalPartID
	ORDER BY [No]

	Set @Err = @@Error

	RETURN @Err
END


GO
