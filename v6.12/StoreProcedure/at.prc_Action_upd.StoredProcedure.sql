SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[at].[prc_Action_upd]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [at].[prc_Action_upd] AS' 
END
GO
ALTER PROCEDURE [at].[prc_Action_upd]
(
	@ActionID int,
	@ChoiceID int,
	@No smallint,
	@PageID INT=NULL,
	@QuestionID int = null,
	@AlternativeID int = null,
	@Type smallint,
	@AVTID  int =null,
	@cUserid int,
	@Log smallint = 1
)
AS
BEGIN
	SET NOCOUNT ON
	DECLARE @Err Int

	UPDATE [at].[Action]
	SET
		[ChoiceID] = @ChoiceID,
		[No] = @No,
		[PageID] = @PageID,
		[QuestionID] = @QuestionID,
		[AlternativeID] = @AlternativeID,
		[Type] = @Type,
		[AVTID]=@AVTID
	WHERE
		[ActionID] = @ActionID

	IF @Log = 1 
	BEGIN 
		INSERT INTO [Log].[AuditLog] ( UserId, TableName, Type, Data, Created) 
		SELECT @cUserid,'Action',1,
		( SELECT * FROM [at].[Action] 
			WHERE
			[ActionID] = @ActionID			 FOR XML AUTO) as data,
			getdate()
	END

	Set @Err = @@Error

	RETURN @Err
END

GO
