SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[at].[prc_StatusTypeMapping_get]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [at].[prc_StatusTypeMapping_get] AS' 
END
GO

ALTER PROCEDURE [at].[prc_StatusTypeMapping_get]
(
	@ActivityID int
)
AS
BEGIN
	SET NOCOUNT ON
	DECLARE @Err Int

	SELECT
	[StatusTypeMappingID],
	[ActivityID],
	ISNULL([CustomerID], 0) AS 'CustomerID',
	ISNULL([FromStatusTypeID],0) AS 'FromStatusTypeID',
	[ToStatusTypeID],
	[No],
	[Created],
	[ShowActionInUI],
	[Settings]
	FROM [at].[StatusTypeMapping]
	WHERE
	[ActivityID] = @ActivityID
	ORDER BY [No]

	Set @Err = @@Error

	RETURN @Err
END

GO
