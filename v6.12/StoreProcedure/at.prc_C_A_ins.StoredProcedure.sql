SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[at].[prc_C_A_ins]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [at].[prc_C_A_ins] AS' 
END
GO

ALTER PROCEDURE [at].[prc_C_A_ins]
(
	@CustomerID int,
	@ActivityID int,
	@No smallint,
	@cUserid int,
	@Log smallint = 1
)
AS
BEGIN
	SET NOCOUNT ON
	DECLARE @Err Int

	INSERT INTO [at].[C_A]
	(
		[CustomerID],
		[ActivityID],
		[No]
	)
	VALUES
	(
		@CustomerID,
		@ActivityID,
		@No
	)

	Set @Err = @@Error

	IF @Log = 1 
	BEGIN 
		INSERT INTO [Log].[AuditLog] ( UserId, TableName, Type, Data, Created) 
		SELECT @cUserid,'C_A',0,
		( SELECT * FROM [at].[C_A] 
			WHERE
			[CustomerID] = @CustomerID AND
			[ActivityID] = @ActivityID				 FOR XML AUTO) as data,
				getdate() 
	 END

	RETURN @Err
END


GO
