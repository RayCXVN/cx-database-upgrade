SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[rep].[prc_LT_ReportColumn_ins]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [rep].[prc_LT_ReportColumn_ins] AS' 
END
GO


ALTER PROCEDURE [rep].[prc_LT_ReportColumn_ins]
(
	@LanguageID int,
	@ReportColumnID int,
	@Text nvarchar(max),
	@ToolTip nvarchar(512) = '',
	@cUserid int,
	@Log smallint = 1
)
AS
BEGIN
	SET NOCOUNT ON
	DECLARE @Err Int

	INSERT INTO [rep].[LT_ReportColumn]
	(
		[LanguageID],
		[ReportColumnID],
		[Text],
		[ToolTip]
	)
	VALUES
	(
		@LanguageID,
		@ReportColumnID,
		@Text,
		@ToolTip
	)

	Set @Err = @@Error

	IF @Log = 1 
	BEGIN 
		INSERT INTO [Log].[AuditLog] ( UserId, TableName, Type, Data, Created) 
		SELECT @cUserid,'LT_ReportColumn',0,
		( SELECT * FROM [rep].[LT_ReportColumn] 
			WHERE
			[LanguageID] = @LanguageID AND
			[ReportColumnID] = @ReportColumnID				 FOR XML AUTO) as data,
				getdate() 
	 END

	RETURN @Err
END



GO
