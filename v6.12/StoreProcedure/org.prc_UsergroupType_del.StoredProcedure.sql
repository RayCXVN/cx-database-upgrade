SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[org].[prc_UsergroupType_del]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [org].[prc_UsergroupType_del] AS' 
END
GO
ALTER PROCEDURE [org].[prc_UsergroupType_del]
(
	@UsergroupTypeID int,
	@cUserid int,
	@Log smallint = 1
)
AS
BEGIN
	SET NOCOUNT ON
	DECLARE @Err Int

	IF @Log = 1 
	BEGIN 
		INSERT INTO [Log].[AuditLog] ( UserId, TableName, Type, Data, Created) 
		SELECT @cUserid,'UsergroupType',2,
		( SELECT * FROM [org].[UsergroupType] 
			WHERE
			[UsergroupTypeID] = @UsergroupTypeID
			 FOR XML AUTO) as data,
			getdate() 
	END 


	DELETE FROM [org].[UsergroupType]
	WHERE
		[UsergroupTypeID] = @UsergroupTypeID

	Set @Err = @@Error

	RETURN @Err
END


GO
