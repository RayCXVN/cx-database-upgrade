SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[rep].[prc_ReportRow_get]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [rep].[prc_ReportRow_get] AS' 
END
GO

ALTER PROCEDURE [rep].[prc_ReportRow_get]
(
	@ReportPartID int
)
AS
BEGIN
	SET NOCOUNT ON
	DECLARE @Err Int

	SELECT
	[ReportRowID],
	[ReportPartID],
	[ReportRowTypeID],
	[No],
	[CssClass],
	[Created]
	FROM [rep].[ReportRow]
	WHERE
	[ReportPartID] = @ReportPartID
	ORDER BY [No]

	Set @Err = @@Error

	RETURN @Err
END


GO
