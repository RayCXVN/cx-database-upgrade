SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[at].[prc_PageAll_get]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [at].[prc_PageAll_get] AS' 
END
GO



-- at.[prc_PageAll_get] 19
ALTER PROCEDURE [at].[prc_PageAll_get]
(
	@ActivityID int
)
AS
BEGIN
	SET NOCOUNT ON
	DECLARE @Err Int

	SELECT
	P.[PageID],
	ISNULL([ActivityID], 0) AS 'ActivityID',
	P.[No] as Pno,
	ISNULL(P.[Created], 0) AS 'PCreated',
	P.[Type] as PType,
	P.CssClass as PCssClass,
	QuestionID,  IsNull(ScaleID,0) 'ScaleID', Q.No as QNo, Q.Type as QType, Inverted, Mandatory, Status, Q.CssClass, P.ExtId, P.Tag, Q.Created as QCreated, ISNULL(SectionID,0) AS SectionID
	FROM [at].[Page] P 
	JOIN [at].[Question] Q on P.Pageid = Q.PageID
	AND [ActivityID] = @ActivityID
	ORDER BY P.[No],Q.No

	Set @Err = @@Error

	RETURN @Err
END




GO
