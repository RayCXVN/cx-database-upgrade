SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[at].[prc_ResultByMultiUser_getAll]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [at].[prc_ResultByMultiUser_getAll] AS' 
END
GO
/*
    Johnny - Des 07 2016 - Update store procedure for dynamic SQL
    Johnny - April 17 2017 Set default entity status
*/
ALTER PROCEDURE [at].[prc_ResultByMultiUser_getAll]
(
    @OwnerID            INT,
    @DepartmentID       INT,
    @AccessGroupList    NVARCHAR(MAX) = N'',
    @ActivityID         INT=0,
    @StatusTypeID       INT =3,
    @UseSubDepartment   BIT = 0,
    @ValidFilter        DATETIME =NULL
)
As
BEGIN
    --exec [at].[prc_ResultByMultiUser_getAll] 15, '13485,997,86413506,13512,13477,1627,13524,13525,13526,13480,13480,13480,15604,18860,18940,18932,13480', N'6,8',11,3
    SET NOCOUNT ON
    SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED
    DECLARE @Err                int,
            @SurveyTableType    int,    -- get the ID of Survey from dbo.TableType
            @ActivityTableType    int,    -- get the ID of Activity from dbo.TableType
            @DeniedSurveyList    varchar(max)='',
            @AccessGenericCount    int,
            @ReportServer        nvarchar(64)=N'',
            @ReportDB            nvarchar(64)=N'',
            @sqlCommand        nvarchar(max),
            @Parameters        nvarchar(max) = N'',
            @SurveyID        int,
            @Count            int,
            @ActiveEntityStatusID INT = 1
     
    DECLARE @SubDepartmentTable as table (id int)
    DECLARE @HDID int

    SELECT  @SurveyTableType   = TableTypeID FROM dbo.TableType WHERE Name = 'Survey'
    SELECT  @ActivityTableType = TableTypeID FROM dbo.TableType WHERE Name = 'Activity'
    SET @HDID = (SELECT hd.HDID FROM org.H_D hd JOIn org.Department d ON hd.DepartmentID = d.DepartmentID AND d.DepartmentID = @DepartmentID)
    INSERT INTO @SubDepartmentTable SELECT DepartmentID FROM org.H_D WHERE Path like '%\' + cast(@HDID as nvarchar(16)) + '\%'

    SELECT @AccessGenericCount = count(AccessID)
    FROM      AccessGeneric a
          INNER JOIN AccessGroup g ON a.AccessGroupID = g.AccessGroupID
    WHERE  a.TableTypeID IN (@ActivityTableType, @SurveyTableType)
      AND  g.OwnerID = @OwnerID

    CREATE TABLE #DeniedSurveyList (ReportServer nvarchar(64), ReportDB nvarchar(64), DeniedSurvey int)
    CREATE TABLE #UserResult (UserID int, ResultID bigint, ActivityID int, SurveyID int, BatchID int, DepartmentID int,
       RoleID int, UserGroupID int, StartDate datetime, EndDate datetime, StatusTypeID int,
       ReportServer nvarchar(64), ReportDB nvarchar(64), ActivityNo smallint, LastUpdated datetime, ValidTo datetime2, ValidFrom datetime2)
   
    IF @AccessGenericCount > 0 AND len(@AccessGroupList) > 0
    BEGIN
        -- Denied activities to group
        SET @sqlCommand = N'INSERT INTO #DeniedSurveyList (ReportServer, ReportDB, DeniedSurvey)
        SELECT s.ReportServer, s.ReportDB, s.SurveyID
        FROM      AccessGeneric a
              INNER JOIN at.Survey s ON a.Elementid = s.ActivityID AND a.Type = 2
        WHERE  a.TableTypeID = @p_ActivityTableType
          AND  a.AccessGroupID IN (' + @AccessGroupList + ')'
        IF @ActivityID > 0
        BEGIN
          SET @sqlCommand = @sqlCommand + ' AND s.ActivityID = ' + convert(NVARCHAR,@ActivityID)
        END
        SET @Parameters = N'@p_ActivityTableType int'
        EXECUTE sp_executesql @sqlCommand, @Parameters, @p_ActivityTableType = @ActivityTableType

        -- Full/Read access activities to others group and not granted to their own groups
        SET @sqlCommand = N'INSERT INTO #DeniedSurveyList (ReportServer, ReportDB, DeniedSurvey)
        SELECT s.ReportServer, s.ReportDB, s.SurveyID
        FROM      AccessGeneric a
              INNER JOIN AccessGroup g ON a.AccessGroupID = g.AccessGroupID
              INNER JOIN at.Survey s ON a.Elementid = s.ActivityID AND a.Type IN (0,1)
        WHERE  a.TableTypeID = @p_ActivityTableType
          AND  a.AccessGroupID NOT IN (' + @AccessGroupList + ')
          AND  g.OwnerID = @p_OwnerID
        EXCEPT
        SELECT s.ReportServer, s.ReportDB, s.SurveyID
        FROM      AccessGeneric a
              INNER JOIN at.Survey s ON a.Elementid = s.ActivityID AND a.Type IN (0,1)
        WHERE  a.TableTypeID = @p_ActivityTableType
          AND  a.AccessGroupID IN (' + @AccessGroupList + ')'
        IF @ActivityID > 0
        BEGIN
          SET @sqlCommand = @sqlCommand + ' AND s.ActivityID = ' + convert(NVARCHAR,@ActivityID)
        END
        SET @Parameters = N'@p_ActivityTableType int,@p_OwnerID int'
        EXECUTE sp_executesql @sqlCommand, @Parameters, @p_ActivityTableType = @ActivityTableType, @p_OwnerID = @OwnerID

        -- Denied surveys to group
        SET @sqlCommand = N'INSERT INTO #DeniedSurveyList (ReportServer, ReportDB, DeniedSurvey)
        SELECT s.ReportServer, s.ReportDB, s.SurveyID
        FROM      AccessGeneric a
              INNER JOIN at.Survey s ON a.Elementid = s.SurveyID AND a.Type = 2
        WHERE  a.TableTypeID = @p_SurveyTableType
          AND  a.AccessGroupID IN (' + @AccessGroupList + ')'
        IF @ActivityID > 0
        BEGIN
          SET @sqlCommand = @sqlCommand + ' AND s.ActivityID = ' + convert(NVARCHAR,@ActivityID)
        END
        SET @Parameters = N'@p_SurveyTableType int'
        EXECUTE sp_executesql @sqlCommand, @Parameters, @p_SurveyTableType = @SurveyTableType

        -- Full/Read access surveys to others group and not granted to their own groups
        SET @sqlCommand = N'INSERT INTO #DeniedSurveyList (ReportServer, ReportDB, DeniedSurvey)
        SELECT s.ReportServer, s.ReportDB, s.SurveyID
        FROM      AccessGeneric a
              INNER JOIN AccessGroup g ON a.AccessGroupID = g.AccessGroupID
              INNER JOIN at.Survey s ON a.Elementid = s.SurveyID AND a.Type IN (0,1)
        WHERE  a.TableTypeID = @p_SurveyTableType
          AND  a.AccessGroupID NOT IN (' + @AccessGroupList + ')
          AND  g.OwnerID = @p_OwnerID
        EXCEPT
        SELECT s.ReportServer, s.ReportDB, s.SurveyID
        FROM      AccessGeneric a
              INNER JOIN at.Survey s ON a.Elementid = s.SurveyID AND a.Type IN (0,1)
        WHERE  a.TableTypeID = @p_SurveyTableType
          AND  a.AccessGroupID IN (' + @AccessGroupList + ')'
        IF @ActivityID > 0
        BEGIN
          SET @sqlCommand = @sqlCommand + ' AND s.ActivityID = ' + convert(NVARCHAR,@ActivityID)
        END
        SET @Parameters = N'@p_SurveyTableType int,@p_OwnerID int'
        EXECUTE sp_executesql @sqlCommand, @Parameters, @p_SurveyTableType = @SurveyTableType, @p_OwnerID = @OwnerID
    END

    --Insert User on table #UserResult
    IF @UseSubDepartment = 0
    BEGIN
    INSERT INTO #UserResult (UserID,ResultID) SELECT UserID,0 FROM org.[User] WHERE DepartmentID =@DepartmentID
    END
    ELSE
    BEGIN
    INSERT INTO #UserResult (UserID,ResultID) SELECT UserID,0 FROM org.[User] u JOIN @SubDepartmentTable s ON u.DepartmentID = s.id
    INSERT INTO #UserResult (UserID,ResultID) SELECT UserID,0 FROM org.[User] WHERE DepartmentID =@DepartmentID
    END

    SELECT TOP 1 @ReportServer = ReportServer, @ReportDB = ReportDB FROM at.Activity WHERE OwnerID = @OwnerID
    DECLARE @LinkedDB NVARCHAR(MAX)
    
    DECLARE c_ResultDatabase CURSOR READ_ONLY FOR
    SELECT  s.SurveyID, s.ReportServer, s.ReportDB
    FROM    at.Survey s JOIN at.Activity act ON act.ActivityID = s.ActivityID AND act.OwnerID = @OwnerID
    WHERE NOT EXISTS (SELECT 1 FROM #DeniedSurveyList ss WHERE ss.DeniedSurvey = s.SurveyID) AND s.[Status]=2
          AND s.ActivityID =@ActivityID
    ORDER BY s.EndDate DESC      
          
    --ORDER BY s.Created DESC
    OPEN c_ResultDatabase
    FETCH NEXT FROM c_ResultDatabase INTO @SurveyID, @ReportServer, @ReportDB
    WHILE @@FETCH_STATUS=0
    BEGIN
        IF @ReportDB=DB_NAME() AND @ReportServer=@@servername
            BEGIN
            SET @LinkedDB=' '
            END
        ELSE
            BEGIN
            SET @LinkedDB= '['+ @ReportServer + '].['+ @ReportDB + '].'
            END

    SET @sqlCommand = N'SELECT @p_ActiveEntityStatusID = EntityStatusID FROM '+ @LinkedDB +'dbo.EntityStatus WHERE CodeName = ''Active'''
    EXECUTE sp_executesql @sqlCommand, N'@p_ActiveEntityStatusID INT OUTPUT', @p_ActiveEntityStatusID = @ActiveEntityStatusID OUTPUT

        --Get result except denied surveys
       SET @sqlCommand = N'UPDATE  u SET u.UserID=r.UserID, u.ResultID=r.ResultID, u.ActivityID=@p_Activity, u.SurveyID = r.SurveyID, u.BatchID = r.BatchID, u.DepartmentID = r.DepartmentID,
          u.RoleID = r.RoleID, u.UserGroupID =r.UserGroupID, u.StartDate = r.StartDate, u.EndDate = r.EndDate , u.StatusTypeID = r.StatusTypeID, u.ReportServer = @p_ReportServer, 
          u.ReportDB = @p_ReportDB, u.ActivityNo=0, u.LastUpdated = r.LastUpdated, u.ValidTo=r.ValidTo, u.ValidFrom =r.ValidFrom
       FROM    #UserResult u 
       INNER JOIN '+ @LinkedDB +'dbo.Result r ON u.UserID = r.UserID
       INNER JOIN dbo.Batch b ON r.BatchID = b.BatchID
       WHERE 
         b.Status =2
         AND r.EntityStatusID = ' + CONVERT(NVARCHAR(14), @ActiveEntityStatusID) + '
         AND r.Deleted IS NULL
         AND r.SurveyID= '+CONVERT(varchar(max),@SurveyID)+'
         AND r.StatusTypeID =@p_StatusTypeID
         AND u.ResultID =0 
         AND r.LastUpdated >= ALL
         (
            SELECT rr.LastUpdated
            FROM '+ @LinkedDB +'dbo.Result rr
            INNER JOIN  dbo.Batch bb ON rr.BatchID = bb.BatchID
            WHERE bb.Status =2  
             AND rr.EntityStatusID = ' + CONVERT(NVARCHAR(14), @ActiveEntityStatusID) + '
             AND rr.Deleted IS NULL
             AND rr.SurveyID= '+CONVERT(varchar(max),@SurveyID)+'
             AND rr.UserID =u.UserID
             AND rr.StatusTypeID =@p_StatusTypeID
        )
        AND (r.ValidTo IS NULL OR (r.ValidFrom<=@p_ValidFilter AND r.ValidTo>=@p_ValidFilter))
        '
       SET @Parameters = N'@p_ReportServer nvarchar(64),@p_ReportDB nvarchar(64),@p_Activity int,@p_StatusTypeID int,@p_ValidFilter datetime'
       EXECUTE sp_executesql @sqlCommand, @Parameters, @p_ReportServer = @ReportServer, @p_ReportDB = @ReportDB,@p_Activity =@ActivityID,@p_StatusTypeID=@StatusTypeID,@p_ValidFilter=@ValidFilter
       --Break
        SELECT @Count=Count(UserID) FROM #UserResult WHERE ResultID=0
        IF (@Count <=0)
        BREAK 
       FETCH NEXT FROM c_ResultDatabase INTO @SurveyID, @ReportServer, @ReportDB
    END
    CLOSE c_ResultDatabase
    DEALLOCATE c_ResultDatabase

    SELECT * FROM #UserResult WHERE ResultID!=0 
    ORDER BY ActivityNo, StartDate, UserID DESC
        
    DROP TABLE #DeniedSurveyList
    DROP TABLE #UserResult

    Set @Err = @@Error

    RETURN @Err
END
GO
