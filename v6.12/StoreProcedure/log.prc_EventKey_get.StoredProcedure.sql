SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[log].[prc_EventKey_get]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [log].[prc_EventKey_get] AS' 
END
GO
ALTER PROCEDURE [log].[prc_EventKey_get]
	@EventKeyID	int
AS
BEGIN
	SET NOCOUNT ON;
	DECLARE @Err Int
	
     SELECT [EventKeyID],
		  [EventTypeID],
		  [CodeName],
		  [Encrypt],
		  [Created]
	FROM  [log].[EventKey]
	WHERE [EventKeyID] = @EventKeyID
	
	Set @Err = @@Error

	RETURN @Err
  
END

GO
