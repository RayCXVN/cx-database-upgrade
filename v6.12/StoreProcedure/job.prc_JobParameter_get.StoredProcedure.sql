SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[job].[prc_JobParameter_get]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [job].[prc_JobParameter_get] AS' 
END
GO

ALTER PROCEDURE [job].[prc_JobParameter_get]
(
	@JobID int
)
AS
BEGIN
	SET NOCOUNT ON
	DECLARE @Err Int

	SELECT
	[JobParameterID],
	[JobID],
	[No],
	[Name],
	[Value],
	[Type]
	FROM [job].[JobParameter]
	WHERE
	[JobID] = @JobID
	ORDER BY [No]

	Set @Err = @@Error

	RETURN @Err
END


GO
