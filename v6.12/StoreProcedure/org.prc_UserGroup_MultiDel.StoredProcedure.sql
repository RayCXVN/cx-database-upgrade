SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[org].[prc_UserGroup_MultiDel]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [org].[prc_UserGroup_MultiDel] AS' 
END
GO
/*
    Steve - Sept 22 2016 - EntityStatus for table UserGroup
    2018-07-12 Ray:     Replace UG_U by UGMember
*/
ALTER PROCEDURE [org].[prc_UserGroup_MultiDel]
(
    @UserGroupIDList    varchar(max),
    @UserID             int,
    @MoveToNewGroup     bit = 0,
    @NewGroupID         int = 0,
    @HardDelete         bit = 0,
    @ExecutedByUserID   int = NULL
)
AS
BEGIN
    SET NOCOUNT ON
    SET XACT_ABORT ON

    DECLARE @EntityStatusID_Active int = 0, @EntityStatusReasonID_ActiveNone int;
    SELECT @EntityStatusID_Active = EntityStatusID FROM dbo.EntityStatus WHERE CodeName = N'Active';
    SELECT @EntityStatusReasonID_ActiveNone = EntityStatusReasonID FROM dbo.EntityStatusReason WHERE CodeName = N'Active_None';

    SELECT ParseValue AS UserGroupID INTO #UserGroupList FROM dbo.StringToArray(@UserGroupIDList, ',')
    SELECT DISTINCT ugm.UserID INTO #UserList FROM org.[UGMember] ugm JOIN #UserGroupList ugl ON ugl.[UserGroupID] = ugm.[UserGroupID];
    
    BEGIN TRANSACTION
        IF @HardDelete = 1
        BEGIN
            -- Remove users from being deleted groups
            UPDATE ugm SET ugm.[Deleted] = GETDATE()
            FROM [org].[UGMember] ugm
            JOIN #UserGroupList ugl ON ugl.[UserGroupID] = ugm.[UserGroupID];
        END
        
        -- Move users to new group
        IF @MoveToNewGroup = 1 AND @NewGroupID != 0
        BEGIN
            INSERT INTO [org].[UGMember] ([UserGroupID], [UserID], [Created], [CreatedBy], [LastUpdated], [LastUpdatedBy],
                                          [EntityStatusID], [EntityStatusReasonID], [CustomerID], [PeriodID], [ExtID], [Deleted])
            SELECT @NewGroupID AS [UserGroupID], ul.[UserID], GETDATE() AS [Created], @ExecutedByUserID AS [CreatedBy], GETDATE() AS [LastUpdated], @ExecutedByUserID AS [LastUpdatedBy],
                   @EntityStatusID_Active AS [EntityStatusID], @EntityStatusReasonID_ActiveNone AS [EntityStatusReasonID], u.[CustomerID], ug.[PeriodID], '' AS [ExtID], NULL AS [Deleted]
            FROM #UserList ul
            JOIN [org].[UserGroup] ug ON ug.[UserGroupID] = @NewGroupID
            JOIN [org].[User] u ON u.[UserID] = ug.[UserID]
            WHERE NOT EXISTS (SELECT 1 FROM [org].[UGMember] ugm WHERE ugm.[UserGroupID] = @NewGroupID AND ugm.[UserID] = ul.[UserID]
                                                                   AND ugm.[EntityStatusID] = @EntityStatusID_Active AND ugm.[Deleted] IS NULL)
        END
        
        IF @HardDelete = 1
        BEGIN
            -- Delete groups in list
            UPDATE ug SET ug.[Deleted] = GETDATE()
            FROM [org].[UserGroup] ug
            JOIN #UserGroupList ugl ON ugl.[UserGroupID] = ug.[UserGroupID];
        END
        ELSE
        BEGIN
            DECLARE @DeactiveEntityStatusID INT = (SELECT EntityStatusID FROM EntityStatus WHERE CodeName='Deactive')
            -- Mark soft-deleted groups in list
            UPDATE ug SET ug.[EntityStatusID] = @DeactiveEntityStatusID
            FROM [org].[UserGroup] ug
            JOIN #UserGroupList ugl ON ugl.[UserGroupID] = ug.[UserGroupID];
        END
    COMMIT TRANSACTION
    
    -- Drop temp tables
    DROP TABLE #UserGroupList
    DROP TABLE #UserList
END
GO
