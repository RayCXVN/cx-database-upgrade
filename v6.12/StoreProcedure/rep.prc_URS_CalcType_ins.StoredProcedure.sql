SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[rep].[prc_URS_CalcType_ins]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [rep].[prc_URS_CalcType_ins] AS' 
END
GO
ALTER PROCEDURE [rep].[prc_URS_CalcType_ins]
 @UserReportSettingID int,
 @ReportCalcTypeID int,
 @cUserid int,
 @Log smallint = 1
AS
BEGIN
	SET NOCOUNT ON;
	DECLARE @Err Int
    
    INSERT INTO [rep].[URS_CalcType]
           ([UserReportSettingID]
			,[ReportCalcTypeID])
     VALUES
           (@UserReportSettingID
           ,@ReportCalcTypeID)
           
    Set @Err = @@Error
    
	IF @Log = 1 
	BEGIN 
		INSERT INTO [Log].[AuditLog] ( UserId, TableName, Type, Data, Created) 
		SELECT @cUserid,'URS_CalcType',0,
		( SELECT * FROM [rep].[URS_CalcType]
		  WHERE
			[UserReportSettingID] = @UserReportSettingID
			AND [ReportCalcTypeID] = @ReportCalcTypeID				 FOR XML AUTO) as data,
				getdate() 
	END
	
	RETURN @Err       
END

GO
