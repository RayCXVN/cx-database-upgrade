SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[CreatePartitionForNewCustomer]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [dbo].[CreatePartitionForNewCustomer] AS' 
END
GO
ALTER PROCEDURE [dbo].[CreatePartitionForNewCustomer]
(   @FilePrefix nvarchar(64) = 'C',
    @PartitionFunctionName nvarchar(64) = 'GroupByCustomerID',
    @PartitionSchemeName nvarchar(64) = 'ByCustomer'
) AS
BEGIN
    DECLARE @DBName nvarchar(256), @CustomerID int, @FilePath nvarchar(max), @FileName nvarchar(64), @CommonFileName nvarchar(64) = '_COMMON', @NullFileName nvarchar(64) = '_NULL',
            @sqlCommand nvarchar(max) = '', @sqlPartitionCommand nvarchar(max) = '', @PartitionValue nvarchar(max) = '', @PartitionFileGroup nvarchar(max) = ''

    SET @DBName = DB_NAME()
    SET @FilePath = CONVERT(nvarchar(max), SERVERPROPERTY('InstanceDefaultDataPath'))

    DECLARE c_Customer CURSOR LOCAL FAST_FORWARD READ_ONLY FOR
    SELECT [CustomerID] FROM [org].[Customer] c
    WHERE NOT EXISTS (SELECT 1 FROM sys.database_files df WHERE df.[name] = @FilePrefix + CONVERT(nvarchar(32), c.[CustomerID]))
    ORDER BY [CustomerID]

    OPEN c_Customer
    FETCH NEXT FROM c_Customer INTO @CustomerID
    WHILE @@FETCH_STATUS = 0
    BEGIN
        SET @FileName = @FilePrefix + CONVERT(nvarchar(32), @CustomerID)
    
        IF NOT EXISTS (SELECT 1 FROM sys.database_files WHERE [name] = @FileName)
        BEGIN
            SET @sqlCommand = N'ALTER DATABASE [' + @DBName + '] ADD FILEGROUP [' + @FileName + ']'
            PRINT @sqlCommand
            EXEC sp_executesql @sqlCommand
            SET @sqlCommand = N'ALTER DATABASE [' + @DBName + '] ADD FILE ( NAME = N''' + @FileName + ''', FILENAME = N''' + @FilePath + @DBName + '_' + @FileName + '.mdf'' , SIZE = 5120KB , FILEGROWTH = 1024KB ) TO FILEGROUP [' + @FileName + ']'
            PRINT @sqlCommand
            EXEC sp_executesql @sqlCommand
            SET @sqlCommand = N'ALTER PARTITION SCHEME ' + @PartitionSchemeName + ' NEXT USED [' + @FileName + ']'
            PRINT @sqlCommand
            EXEC sp_executesql @sqlCommand
            SET @sqlCommand = N'ALTER PARTITION FUNCTION ' + @PartitionFunctionName + '() SPLIT RANGE (' + CONVERT(nvarchar(32), @CustomerID) + ')'
            PRINT @sqlCommand
            EXEC sp_executesql @sqlCommand
        END

        FETCH NEXT FROM c_Customer INTO @CustomerID
    END
    CLOSE c_Customer
    DEALLOCATE c_Customer
END

GO
