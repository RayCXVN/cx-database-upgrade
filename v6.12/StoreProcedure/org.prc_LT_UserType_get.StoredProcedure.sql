SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[org].[prc_LT_UserType_get]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [org].[prc_LT_UserType_get] AS' 
END
GO

ALTER PROCEDURE [org].[prc_LT_UserType_get]
(
	@UserTypeID int
)
AS
BEGIN
	SET NOCOUNT ON
	DECLARE @Err Int

	SELECT
	[LanguageID],
	[UserTypeID],
	[Name],
	[Description]
	FROM [org].[LT_UserType]
	WHERE [UserTypeID] = @UserTypeID

	Set @Err = @@Error

	RETURN @Err
END


GO
