SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[org].[prc_Update_Customer_InDepartment]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [org].[prc_Update_Customer_InDepartment] AS' 
END
GO
-- =============================================
-- Author:		Rickie 
-- Create date: 06.11.2017
-- Description:	Update customerID for users who belong to department
-- =============================================
ALTER PROCEDURE [org].[prc_Update_Customer_InDepartment]
(
    @DepartmentID int,	
    @inputCustomerID int
)
AS
BEGIN
    UPDATE [org].[User] SET [CustomerID] = @inputCustomerID
    WHERE [DepartmentID] = @DepartmentID;
END
GO
