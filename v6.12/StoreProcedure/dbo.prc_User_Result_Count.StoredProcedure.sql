SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[prc_User_Result_Count]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [dbo].[prc_User_Result_Count] AS' 
END
GO
ALTER PROCEDURE [dbo].[prc_User_Result_Count]
(
	@UserID int
)
As
BEGIN
	SET NOCOUNT ON
	DECLARE @Err INT
    DECLARE @ActiveEntityStatusID INT = (SELECT EntityStatusID FROM EntityStatus WHERE CodeName='Active')

	SELECT count(Resultid) AS ResCount
	FROM [Result]
	WHERE [Userid] = @UserID
        AND EntityStatusID = @ActiveEntityStatusID AND Deleted IS NULL

	SET @Err = @@Error

	RETURN @Err
END
-------------------------------------------------------------------------------------


GO
