SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[prc_ResultDepartment_get]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [dbo].[prc_ResultDepartment_get] AS' 
END
GO
/* ------------------------------------------------------------  
   PROCEDURE:    prc_ResultDepartment_get  
  
   Description:  Selects records from the table 'prc_Result_get'  
  
   AUTHOR:       LockwoodTech 11.07.2006 11:47:11  
   ------------------------------------------------------------
   2016-10-10 Ray: Remove column Status from org.User, org.Department, RDB.dbo.Result
*/  
ALTER PROCEDURE [dbo].[prc_ResultDepartment_get]
(  
 @DepartmentID  int,  
 @SurveyID   int  
)  
As  
BEGIN  
 SET NOCOUNT ON  
 DECLARE @Err Int  
 DECLARE @ActiveEntityStatusID INT = (SELECT EntityStatusID FROM EntityStatus WHERE CodeName='Active')
  
 Select  
 [ResultID],  
 [StartDate],  
 [EndDate],  
 [RoleID],  
 ISNULL([UserID], 0) [UserID],  
 ISNULL([UserGroupID], 0) [UserGroupID],  
 [SurveyID],  
 [BatchID],  
 [LanguageID],  
 [PageNo],  
 [DepartmentID],  
 [Anonymous],  
 [ShowBack],  
 [Email],  
 [chk],  
 [ResultKey],  
 [Created],  
 [LastUpdated],  
 [LastUpdatedBy],
 [StatusTypeID],
 EntityStatusID,
 Deleted    
 FROM [Result]  
 WHERE   
  [DepartmentID] = @DepartmentID    
  AND SurveyID = @SurveyID  
  and EntityStatusID = @ActiveEntityStatusID  AND Deleted IS NULL
   
 Set @Err = @@Error  
  
 RETURN @Err  
END


GO
