SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[res].[Resource_getByXC]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [res].[Resource_getByXC] AS' 
END
GO
ALTER PROCEDURE [res].[Resource_getByXC]
(
	@XCID                INT,
    @GetXCParent         BIT,
    @CustomerID          INT = NULL,    
    @LanguageID          INT,
	@FallbackLanguageID  INT
)
AS
BEGIN
	SET NOCOUNT ON
	DECLARE @Err Int      

    SELECT 
           C.[CategoryId]
          ,C.[CustomerId]       
          ,XCID.XCID     
          ,ISNULL(LTXC.Name,LTXCF.Name) AS [XCategoryName]     

          ,XCID.ResourceID             

          ,ISNULL(LTR.[Name],LTRF.[Name]) AS [ResourceName]
          ,ISNULL(LTR.[Description],LTRF.[Description]) AS [Description]       

          ,R.[MediaTypeId]        
          ,R.[URL]
          ,R.[StatusID]
          ,R.[Deleted]
          ,R.[ValidTo]
          ,R.[ValidFrom]
          ,R.[Active]
          ,R.[ExtId]
          ,R.[Updated]
          ,R.[Created]
          ,R.[Author]
          ,R.[AuthorName]
          ,R.[CreatedBy]
          ,R.[No]
          ,R.[ShowAuthor]
          ,R.[ShowTopBar]
          ,R.[ShowCompleteCheckbox]
          ,R.[IsExternal]
          ,R.[ShowPublishedDate]
          ,R.[ShowNote] 
          ,ISNULL(LTR.[Keyword],LTRF.[Keyword]) AS [Keyword]        
          ,ISNULL(LTR.[NoteHeading],LTRF.[NoteHeading]) AS [NoteHeading]        
          ,ISNULL(LTR.[CompletedCheckText],LTRF.[CompletedCheckText]) AS [CompletedCheckText]     
          ,ISNULL(LTR.[CompletedInfoText],LTRF.[CompletedInfoText]) AS [CompletedInfoText]                  
    FROM res.[Resource] AS R 
     INNER JOIN res.Category   AS C    ON C.CategoryId = R.CategoryId AND (@CustomerID IS NULL OR C.CustomerId = @CustomerID OR C.CustomerId IS NULL)
     INNER JOIN
     (
	   SELECT RXC.ResourceID, RXC.XCID
       FROM 
       (SELECT XCID    
	   FROM  at.XCategory XC      
	   WHERE ParentID = @XCID ) AS XCID1
       INNER JOIN res.Resource_XC RXC ON XCID1.XCID = RXC.XCID
      ) AS XCID ON R.ResourceId = XCID.ResourceID             
    LEFT JOIN at.LT_XCategory AS LTXC ON LTXC.XCID = XCID.XCID AND LTXC.LanguageId = @LanguageID
    LEFT JOIN at.LT_XCategory AS LTXCF ON LTXCF.XCID = XCID.XCID AND LTXCF.LanguageId = @FallbackLanguageID
    LEFT JOIN res.LT_Resource AS LTR  ON R.ResourceId = LTR.ResourceId AND LTR.LanguageId = @LanguageID
    LEFT JOIN res.LT_Resource AS LTRF ON R.ResourceId = LTRF.ResourceId AND LTRF.LanguageId = @FallbackLanguageID 

    ORDER BY XCID.XCID      


    Set @Err = @@Error

	RETURN @Err
END

GO
