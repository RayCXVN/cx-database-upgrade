SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[list].[prc_ItemListDataSource_copy]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [list].[prc_ItemListDataSource_copy] AS' 
END
GO

    
  --sp_helptext '[app].[prc_PortalPage_copy]'
ALTER PROCEDURE [list].[prc_ItemListDataSource_copy]  
(  
 @ItemListDataSourceID int,
 @NewItemListDataSourceID int = null output  
)  
AS  
BEGIN  

 SET XACT_ABORT ON  
 BEGIN TRANSACTION  
 DECLARE  
	@OwnerID int,  
    @FunctionName nvarchar(64),  
    @ItemListDataParameterID INT,
    @NewItemListDataParameterID INT
 SELECT  
		@OwnerID = [OwnerID],  
         @FunctionName = [FunctionName]
 FROM [list].[ItemListDataSource]   
 WHERE  
  [ItemListDataSourceID] = @ItemListDataSourceID  
  
   INSERT INTO [list].[ItemListDataSource]  
	 ([OwnerID]  
      ,[FunctionName]
      )    
	 VALUES  
	 ( @OwnerID  
           ,@FunctionName)    
 
  Set @NewItemListDataSourceID = scope_identity()  
  
   --insert LT_ItemListDataSource
  INSERT INTO [list].[LT_ItemListDataSource]
  ([LanguageID]  
           ,[ItemListDataSourceID]  
           ,[Name]  
           ,[Description])
  (SELECT [LanguageID]  
           , @NewItemListDataSourceID  
           ,[Name]  
           ,[Description]
   FROM [list].[LT_ItemListDataSource]
   WHERE ([ItemListDataSourceID] = @ItemListDataSourceID))
 
	--insert ItemListDataParameter
	DECLARE curItemListDataParameter CURSOR LOCAL FAST_FORWARD READ_ONLY FOR      
	SELECT ItemListDataParameterID        
	FROM [list].[ItemListDataParameter]    
	WHERE  ([ItemListDataSourceID] = @ItemListDataSourceID)

	OPEN curItemListDataParameter    
	FETCH NEXT FROM curItemListDataParameter INTO @ItemListDataParameterID    
	    
	WHILE (@@FETCH_STATUS = 0)    
	BEGIN    
		INSERT INTO [list].[ItemListDataParameter]  
		([ItemListDataSourceID]
      ,[DataType]
      ,[Type]
      ,[Mandatory]
      ,[DefaultValue]
      ,[ConstructionPattern])  
		 (SELECT @NewItemListDataSourceID  
      ,[DataType]
      ,[Type]
      ,[Mandatory]
      ,[DefaultValue]
      ,[ConstructionPattern]
	   FROM [list].[ItemListDataParameter]  
	   WHERE ([ItemListDataParameterID] = @ItemListDataParameterID))  
	     
	      
	   SET @NewItemListDataParameterId = SCOPE_IDENTITY()    
	     
	   --insert LT_ItemListDataParameter  
	   INSERT INTO [list].[LT_ItemListDataParameter]  
			   ([LanguageID]
      ,[ItemListDataParameterID]
      ,[Name]
      ,[Description]
			   )  
	   (SELECT  
		   [LanguageID]
		  ,@NewItemListDataParameterId
		  ,[Name]
		  ,[Description]
		FROM [list].[LT_ItemListDataParameter]  
		WHERE [ItemListDataParameterID] = @ItemListDataParameterID)  
		
		FETCH NEXT FROM curItemListDataParameter INTO @ItemListDataParameterID    
	END    
	CLOSE curItemListDataParameter    
	DEALLOCATE curItemListDataParameter    

 COMMIT TRANSACTION  
    
END  
  
  

GO
