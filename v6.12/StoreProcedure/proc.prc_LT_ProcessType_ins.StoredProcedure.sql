SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[proc].[prc_LT_ProcessType_ins]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [proc].[prc_LT_ProcessType_ins] AS' 
END
GO
ALTER PROCEDURE [proc].[prc_LT_ProcessType_ins]
(
	@ProcessTypeID int,
	@LanguageID int,
	@Name nvarchar(256),
	@Description nvarchar(max),
	@DescriptionFieldName nvarchar(256),
	@HelpText nvarchar(max),
	@WizardStep1 nvarchar(64),
	@WizardStep2 nvarchar(64),
	@WizardStep3 nvarchar(64),
	@WizardStep4 nvarchar(64),
	@WizardStep5 nvarchar(64),
	@AnswerHeader nvarchar(128),
	@WizardStep1Desc nvarchar(max) = '',
	@WizardStep2Desc nvarchar(max) = '',
	@WizardStep3Desc nvarchar(max) = '',
	@WizardStep4Desc nvarchar(max) = '',
	@WizardStep5Desc nvarchar(max) = '',
	@cUserid int,
	@Log smallint = 1
)
AS
BEGIN
	SET NOCOUNT ON
	DECLARE @Err Int

	INSERT INTO [proc].[LT_ProcessType]
	(
		[ProcessTypeID],
		[LanguageID],
		[Name],
		[Description],
		[descriptionFieldName],
		[HelpText],
		[WizardStep1],
		[WizardStep2],
		[WizardStep3],
		[WizardStep4],
		[WizardStep5],
		[AnswerHeader],
		[WizardStep1Desc],
		[WizardStep2Desc],
		[WizardStep3Desc],
		[WizardStep4Desc],
		[WizardStep5Desc]
	)
	VALUES
	(
		@ProcessTypeID,
		@LanguageID,
		@Name,
		@Description,
		@DescriptionFieldName,
		@HelpText,
		@WizardStep1,
		@WizardStep2,
		@WizardStep3,
		@WizardStep4,
		@WizardStep5,
		@AnswerHeader,
		@WizardStep1Desc,
		@WizardStep2Desc,
		@WizardStep3Desc,
		@WizardStep4Desc,
		@WizardStep5Desc
	)

	Set @Err = @@Error

	IF @Log = 1 
	BEGIN 
		INSERT INTO [Log].[AuditLog] ( UserId, TableName, Type, Data, Created) 
		SELECT @cUserid,'LT_ProcessType',0,
		( SELECT * FROM [proc].[LT_ProcessType] 
			WHERE
			[ProcessTypeID] = @ProcessTypeID AND
			[LanguageID] = @LanguageID				 FOR XML AUTO) as data,
				getdate() 
	 END

	RETURN @Err
END

GO
