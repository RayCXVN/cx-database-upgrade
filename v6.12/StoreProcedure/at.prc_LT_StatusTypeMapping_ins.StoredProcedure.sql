SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[at].[prc_LT_StatusTypeMapping_ins]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [at].[prc_LT_StatusTypeMapping_ins] AS' 
END
GO

ALTER PROCEDURE [at].[prc_LT_StatusTypeMapping_ins]
(
	@LanguageID int,
	@StatusTypeMappingID int,
	@ActionName Nvarchar(512) ='',
	@ActionDescription NVARCHAR(MAX)='',
	@cUserid int,
	@Log smallint = 1
)
AS
BEGIN
	SET NOCOUNT ON
	DECLARE @Err Int
	INSERT INTO [at].[LT_StatusTypeMapping]
	(
		[LanguageID],
		[StatusTypeMappingID],
		[ActionName],
		[ActionDescription]
	)
	VALUES
	(
		@LanguageID,
		@StatusTypeMappingID,
		@ActionName,
		@ActionDescription
	)
	Set @Err = @@Error
	IF @Log = 1 
	BEGIN 
		INSERT INTO [Log].[AuditLog] ( UserId, TableName, Type, Data, Created) 
		SELECT @cUserid,'LT_StatusTypeMapping',0,
		( SELECT * FROM [at].[LT_StatusTypeMapping] 
			WHERE
			[LanguageID] = @LanguageID AND
			[StatusTypeMappingID] = @StatusTypeMappingID				 FOR XML AUTO) as data,
				getdate() 
	 END
	RETURN @Err

END

GO
