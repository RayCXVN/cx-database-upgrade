SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[prop].[prc_LT_PropOption_get]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [prop].[prc_LT_PropOption_get] AS' 
END
GO

ALTER PROCEDURE [prop].[prc_LT_PropOption_get]
(
	@PropOptionID int
)
AS
BEGIN
	SET NOCOUNT ON
	DECLARE @Err Int

	SELECT
	[LanguageID],
	[PropOptionID],
	[Name],
	[Description]
	FROM [prop].[LT_PropOption]
	WHERE
	[PropOptionID] = @PropOptionID

	Set @Err = @@Error

	RETURN @Err
END


GO
