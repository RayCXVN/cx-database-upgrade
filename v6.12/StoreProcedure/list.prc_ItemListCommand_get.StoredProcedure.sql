SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[list].[prc_ItemListCommand_get]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [list].[prc_ItemListCommand_get] AS' 
END
GO

ALTER PROCEDURE [list].[prc_ItemListCommand_get]
	@ItemListID int
AS
BEGIN
	SET NOCOUNT ON;
	DECLARE @Err Int
	
    SELECT 
		[ItemListCommandID],
		[ItemListID],
		[PlaceAbove],
		[PlaceBelow],
		[MultiCheckCommand],
		[DisabledInEmptyList],
		[CssClass],
		[ClientFunction],
		[Created],
		[No]
	FROM [list].[ItemListCommand]
	WHERE [ItemListID] = @ItemListID
	
	Set @Err = @@Error

	RETURN @Err
  
END

GO
