SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[note].[prc_NoteTypeAccess_upd]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [note].[prc_NoteTypeAccess_upd] AS' 
END
GO
ALTER PROCEDURE [note].[prc_NoteTypeAccess_upd]
(
	@NoteTypeAccessID int,
	@NoteTypeID int,
	@TableTypeID int,
	@ItemID int,
	@Type smallint,
	@cUserid int,
	@Log smallint = 1
)
AS
BEGIN
	SET NOCOUNT ON
	DECLARE @Err Int

	UPDATE [note].[NoteTypeAccess]
	SET
		[NoteTypeID] = @NoteTypeID,
		[TableTypeID] = @TableTypeID,
		[ItemID] = @ItemID,
		[Type] = @Type
	WHERE
		[NoteTypeAccessID] = @NoteTypeAccessID
	
	Set @Err = @@Error
	
	IF @Log = 1 
	BEGIN 
		INSERT INTO [Log].[AuditLog] ( UserId, TableName, Type, Data, Created) 
		SELECT @cUserid,'NoteTypeAccess',1,
		( SELECT * FROM [note].[NoteTypeAccess] 
			WHERE
			[NoteTypeAccessID] = @NoteTypeAccessID FOR XML AUTO) as data,
				getdate() 
	 END

	RETURN @Err
END

GO
