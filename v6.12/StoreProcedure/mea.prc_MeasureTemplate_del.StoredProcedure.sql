SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[mea].[prc_MeasureTemplate_del]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [mea].[prc_MeasureTemplate_del] AS' 
END
GO
ALTER PROCEDURE [mea].[prc_MeasureTemplate_del]
(
	@MeasureTemplateID int,
	@cUserid int,
	@Log smallint = 1
)
AS  
BEGIN  
	SET NOCOUNT ON  
	DECLARE @Err Int  
  
	IF @Log = 1 
	BEGIN 
		INSERT INTO [Log].[AuditLog] ( UserId, TableName, Type, Data, Created) 
		SELECT @cUserid,'mea.MeasureTemplate',0,
		( SELECT * FROM [mea].[MeasureTemplate] 
			WHERE
			[MeasureTemplateID] = @MeasureTemplateID FOR XML AUTO) as data,
				getdate() 
	END

	DELETE FROM mea.MeasureTemplate
	WHERE
		[MeasureTemplateID] = @MeasureTemplateID
	
	Set @Err = @@Error  
	
	RETURN @Err
END

GO
