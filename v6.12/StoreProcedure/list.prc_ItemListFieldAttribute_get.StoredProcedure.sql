SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[list].[prc_ItemListFieldAttribute_get]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [list].[prc_ItemListFieldAttribute_get] AS' 
END
GO
ALTER PROCEDURE [list].[prc_ItemListFieldAttribute_get]
	@ItemListFieldID int
AS
BEGIN
	SET NOCOUNT ON;
	DECLARE @Err Int
	
    SELECT 
		[ItemListFieldAttributeID],
		[ItemListFieldID],
		[key],
		[Value],
		[Created]
	FROM [list].[ItemListFieldAttribute]
	WHERE [ItemListFieldID] = @ItemListFieldID
	
	Set @Err = @@Error

	RETURN @Err
  
END

GO
