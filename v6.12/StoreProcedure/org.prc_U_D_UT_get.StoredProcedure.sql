SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[org].[prc_U_D_UT_get]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [org].[prc_U_D_UT_get] AS' 
END
GO

ALTER PROCEDURE [org].[prc_U_D_UT_get]
(
	@U_DID int
)
AS
BEGIN
	SET NOCOUNT ON
	DECLARE @Err Int

	SELECT
	[U_DID],
	[UsertypeID]
	FROM [org].[U_D_UT]
	WHERE
	[U_DID] = @U_DID

	Set @Err = @@Error

	RETURN @Err
END


GO
