SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[list].[prc_LT_ItemList_ins]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [list].[prc_LT_ItemList_ins] AS' 
END
GO
ALTER PROCEDURE [list].[prc_LT_ItemList_ins]
(
	@LanguageID int,
	@ItemListID int,
	@Name nvarchar(256),
	@Description nvarchar(max),
	@RowTooltipText nvarchar(256),
	@EmptyListText nvarchar(max),
    @cUserid int,
    @Log smallint = 1
)    
AS
BEGIN
	SET NOCOUNT ON;
	DECLARE @Err Int
	
	INSERT INTO [list].[LT_ItemList]
           ([LanguageID]
           ,[ItemListID]
           ,[Name]
           ,[Description]
           ,[RowTooltipText]
           ,[EmptyListText])
     VALUES
           (@LanguageID
           ,@ItemListID
           ,@Name
           ,@Description
           ,@RowTooltipText
           ,@EmptyListText)
           
     Set @Err = @@Error
	 IF @Log = 1 
	 BEGIN 
		INSERT INTO [Log].[AuditLog] ( UserId, TableName, Type, Data, Created) 
		SELECT @cUserid,'LT_ItemList',0,
		( SELECT * FROM [list].[LT_ItemList] 
			WHERE
			[LanguageID] = @LanguageID AND
			[ItemListID] = @ItemListID				 FOR XML AUTO) as data,
				getdate() 
	  END

	
	 RETURN @Err
END

GO
