SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[prc_AccessGroupMember_get]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [dbo].[prc_AccessGroupMember_get] AS' 
END
GO
/*  2017-11-16 Johnny:      Add new column ArchetypeID in [dbo].[AccessGroupMember]
    2017-11-16 Ray:         refactor
*/
ALTER PROCEDURE [dbo].[prc_AccessGroupMember_get](
    @AccessGroupID int)
AS
BEGIN
    SET NOCOUNT ON;
    DECLARE @Err int;

    SELECT [AccessGroupMemberID], [AccessGroupID], ISNULL([UserID], 0) AS 'UserID', ISNULL([UserTypeID], 0) AS 'UserTypeID',
           ISNULL([DepartmentID], 0) AS 'DepartmentID', ISNULL([HDID], 0) AS 'HDID', ISNULL([UserGroupID], 0) AS 'UserGroupID',
           ISNULL([RoleID], 0) AS 'RoleID', ISNULL([DepartmentTypeID], 0) AS 'DepartmentTypeID', ISNULL([DepartmentGroupID], 0) AS 'DepartmentGroupID',
           ISNULL([CustomerID], 0) AS 'CustomerID', ISNULL([ExtGroupID], 0) AS 'ExtGroupID', ISNULL([ArchetypeID], 0) AS 'ArchetypeID'
    FROM [dbo].[AccessGroupMember]
    WHERE [AccessGroupID] = @AccessGroupID;

    SET @Err = @@Error;

    RETURN @Err;
END;
GO