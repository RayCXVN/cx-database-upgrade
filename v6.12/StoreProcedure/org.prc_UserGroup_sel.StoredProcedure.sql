SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[org].[prc_UserGroup_sel]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [org].[prc_UserGroup_sel] AS' 
END
GO
/*
	Steve - Sept 22 2016 - EntityStatus for table UserGroup
*/
ALTER PROCEDURE [org].[prc_UserGroup_sel]
(
	@UserGroupID as int	
)
AS
BEGIN
	SET NOCOUNT ON
	DECLARE @Err Int

	DECLARE @EntityStatusID INT = 0 
	SELECT @EntityStatusID=EntityStatusID FROM EntityStatus WHERE CodeName='Active'

	SELECT
	[UserGroupID],
	[OwnerID],
	ISNULL([DepartmentID], 0) AS 'DepartmentID',
	ISNULL([SurveyId], 0) AS 'SurveyId',
	[Name],
	[Description],
	[Created],
	[ExtID],
	ISNULL([PeriodID],0) AS 'PeriodID',
	ISNULL ([UserID],0) AS 'UserID',
	ISNULL ([UserGroupTypeID],0) AS 'UserGroupTypeID',
	[Tag]
	FROM [org].[UserGroup]
	WHERE [UserGroupID] = @UserGroupID AND EntityStatusID = @EntityStatusID AND Deleted IS NULL
END

GO
