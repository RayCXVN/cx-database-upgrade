/****** Object:  StoredProcedure [rl].[prc_LogSection_getReadStatus]    Script Date: 5/11/2017 10:41:02 AM ******/
DROP PROCEDURE [rl].[prc_LogSection_getReadStatus]
GO
/****** Object:  StoredProcedure [rl].[prc_LogSection_getReadStatus]    Script Date: 5/11/2017 10:41:02 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- EXEC [rl].[prc_LogSection_getReadStatus] 8, '171,238,356,380,240,371,375,1,357', 1
CREATE PROCEDURE [rl].[prc_LogSection_getReadStatus]
(   @UserID             int,
    @LogSectionIDList   nvarchar(max),
    @SiteID             int
) AS
BEGIN
    SET NOCOUNT ON
    DECLARE @LogSectionTable TABLE ([LogSectionID] bigint, [IsUpdated] bit)
    INSERT INTO @LogSectionTable ([LogSectionID]) SELECT [Value] FROM dbo.funcListToTableInt(@LogSectionIDList, ',')

    DECLARE @IsUpdated bit, @LastUpdated datetime, @TT_Result int, @ResultID bigint, @ET_ResultUpdated int, @ET_ResultViewed int
    
    SELECT @TT_Result = [TableTypeID] FROM [TableType] WHERE [Schema] = 'dbo' AND [Name] = 'Result'
    SELECT @ET_ResultUpdated     = [Value] FROM [app].[SiteParameter] WHERE [Key] = 'EVENTTYPE_ResultUpdated' AND [SiteID] = @SiteID
    SELECT @ET_ResultViewed      = [Value] FROM [app].[SiteParameter] WHERE [Key] = 'EVENTTYPE_ResultViewed' AND [SiteID] = @SiteID
    
    DECLARE c_LogSection CURSOR LOCAL FAST_FORWARD READ_ONLY FOR
    SELECT [LogSectionID] FROM @LogSectionTable

    OPEN c_LogSection
    FETCH NEXT FROM c_LogSection INTO @ResultID
    WHILE @@FETCH_STATUS = 0
    BEGIN
        SET @IsUpdated = 1
        SELECT @LastUpdated = MAX(e.[Created]) FROM [log].[Event] e WHERE e.[EventTypeID] = @ET_ResultUpdated AND e.[UserID] = @UserID AND e.[TableTypeID] = @TT_Result AND e.[ItemID] = @ResultID
        IF EXISTS (SELECT 1 FROM [log].[Event] e WHERE e.[EventTypeID] = @ET_ResultViewed AND e.[UserID] = @UserID AND e.[TableTypeID] = @TT_Result AND e.[ItemID] = @ResultID AND e.[Created] >= ISNULL(@LastUpdated, '1900-01-01'))
        BEGIN
            SET @IsUpdated = 0
        END

        UPDATE @LogSectionTable SET [IsUpdated] = @IsUpdated WHERE [LogSectionID] = @ResultID

        FETCH NEXT FROM c_LogSection INTO @ResultID
    END
    CLOSE c_LogSection
    DEALLOCATE c_LogSection

    SELECT * FROM @LogSectionTable
END
GO
