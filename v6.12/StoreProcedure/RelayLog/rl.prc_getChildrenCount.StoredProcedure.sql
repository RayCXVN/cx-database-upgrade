/****** Object:  StoredProcedure [rl].[prc_getChildrenCount]    Script Date: 5/11/2017 10:41:02 AM ******/
DROP PROCEDURE [rl].[prc_getChildrenCount]
GO
/****** Object:  StoredProcedure [rl].[prc_getChildrenCount]    Script Date: 5/11/2017 10:41:02 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/*  27-Sep-2016 Ray: Count children being transferred and waiting for acceptant
*/
CREATE PROCEDURE [rl].[prc_getChildrenCount]
(
	@SiteID             int = 1,
    @UserIDList         varchar(max) = '',
    @OutputMessage      nvarchar(max) = '' OUTPUT
)
AS
BEGIN
	SET NOCOUNT ON
    
    DECLARE @LogSectionActivityID INT, @MeasureActivityID INT, @QID_ActorDepartmentID INT, @AltID_ActorDepartmentID INT,
            @QID_Actor int, @AltID_Actor INT, @QID_HolderID int, @AltID_HolderID int, @QID_TransferRHID int, @AltID_TransferRHID int,
            @QID_TransferOMID int, @AltID_TransferOMID int,
            @ActorXML nvarchar(max), @QTxml XML, @HolderXML nvarchar(max), @ActorInfoXML nvarchar(max), @TransferInfoXML nvarchar(max),
            @PropPosition INT, @PropOfficePhone INT, @DepartmentBackground INT, @ParentDepartmentID int,
            @sqlCommand nvarchar(max) = ''
    /*
    -- The structure of site parameter LISTCOLUMN_Actor should be
    SET @ActorXML = '<Actor QuestionID="10" AlternativeID="43" />'

    -- The structure of site parameter LISTCOLUMN_Holder should be
    SET @HolderXML = N'<Holder Name="HolderID" QuestionID="5" AlternativeID="5" DataType="text" />
    */
    SELECT @ActorXML             = [Value] FROM app.[SiteParameter] WHERE [Key] = 'LISTCOLUMN_Actor'            AND [SiteID] = @SiteID        
    SELECT @HolderXML            = [Value] FROM app.[SiteParameter] WHERE [Key] = 'LISTCOLUMN_Holder'           AND [SiteID] = @SiteID
    SELECT @ActorInfoXML         = [Value] FROM app.[SiteParameter] WHERE [Key] = 'LISTCOLUMN_ActorInfo'        AND [SiteID] = @SiteID
    SELECT @TransferInfoXML      = [Value] FROM app.[SiteParameter] WHERE [Key] = 'LISTCOLUMN_TransferInfo'     AND [SiteID] = @SiteID
    SELECT @LogSectionActivityID = [Value] FROM app.[SiteParameter] WHERE [Key] = 'ACTIVITY_ID_LOG_SECTION'     AND [SiteID] = @SiteID
    SELECT @MeasureActivityID    = [Value] FROM app.[SiteParameter] WHERE [Key] = 'ACTIVITY_ID_MEASURE'         AND [SiteID] = @SiteID 
    SELECT @PropPosition         = [Value] FROM app.[SiteParameter] WHERE [Key] = 'PROP_Position'               AND [SiteID] = @SiteID
    SELECT @PropOfficePhone      = [Value] FROM app.[SiteParameter] WHERE [Key] = 'PROP_OfficePhone'            AND [SiteID] = @SiteID    
    SELECT @DepartmentBackground = [Value] FROM app.[SiteParameter] WHERE [Key] = 'PROP_DepartmentBackground'   AND [SiteID] = @SiteID    
    
    SET @QTxml = CONVERT(xml, @ActorXML)
    SELECT @QID_Actor = Tbl.Col.value('@QuestionID', 'int'), @AltID_Actor = Tbl.Col.value('@AlternativeID', 'int')
    FROM @QTxml.nodes('//Actor') Tbl(Col)     

    SET @QTxml = CONVERT(xml, @HolderXML)
    SELECT @QID_HolderID = Tbl.Col.value('@QuestionID', 'int'), @AltID_HolderID = Tbl.Col.value('@AlternativeID', 'int')
    FROM @QTxml.nodes('//Holder') Tbl(Col)
    WHERE Tbl.Col.value('@Name', 'nvarchar(max)') = 'HolderID'

    SET @QTxml = CONVERT(xml, @ActorInfoXML)
    SELECT @QID_ActorDepartmentID = Tbl.Col.value('@QuestionID', 'int'), @AltID_ActorDepartmentID = Tbl.Col.value('@AlternativeID', 'int')
    FROM @QTxml.nodes('//ActorInfo') Tbl(Col)
    WHERE Tbl.Col.value('@Name', 'nvarchar(max)') = 'ActorDepartmentID'

    SET @QTxml = CONVERT(xml, @TransferInfoXML)
    SELECT @QID_TransferRHID = Tbl.Col.value('@QuestionID', 'int'), @AltID_TransferRHID = Tbl.Col.value('@AlternativeID', 'int')
    FROM @QTxml.nodes('//TransferInfo') Tbl(Col)
    WHERE Tbl.Col.value('@Name', 'nvarchar(max)') = 'HolderID'

    SELECT @QID_TransferOMID = Tbl.Col.value('@QuestionID', 'int'), @AltID_TransferOMID = Tbl.Col.value('@AlternativeID', 'int')
    FROM @QTxml.nodes('//TransferInfo') Tbl(Col)
    WHERE Tbl.Col.value('@Name', 'nvarchar(max)') = 'OMId'

	CREATE TABLE #User ([UserID] INT, [LastName] NVARCHAR(MAX), [FirstName] NVARCHAR(MAX), [SSN] nvarchar(32),
                        [ActorChildrenNumber] int, [RelayChildrenNumber] int, [MovingLogCount] int, [WaitingLogCount] int)
    CREATE TABLE #SurveyTable ([SurveyID] int PRIMARY KEY, [ActivityID] int, [ReportServer] nvarchar(64), [ReportDB] nvarchar(64))
    CREATE TABLE #Children ([ResponsibleUserID] int, [ChildID] int, [Type] char(1), [MovingLog] int)                -- Type: A - Actor, H - Holder, W - Waiting
    CREATE TABLE #ChildrenCount ([ResponsibleUserID] int, [Type] char(1), [ChildrenCount] int, [MovingCount] int)   -- Type: A - Actor, H - Holder, W - Waiting
    CREATE TABLE #ActorInfoQuestionTable ([Name] nvarchar(max), [QuestionID] int, [AlternativeID] int, [DataType] nvarchar(64))
    
    CREATE CLUSTERED INDEX IDX_#User_UserID ON #User([UserID])	
    CREATE INDEX IDX_#SurveyTable_ActivityID ON #SurveyTable([ActivityID])
    CREATE INDEX IDX_#Children_ResponsibleUserID_Type ON #Children ([ResponsibleUserID], [Type])
    CREATE INDEX IDX_#ChildrenCount_ResponsibleUserID_Type ON #ChildrenCount ([ResponsibleUserID], [Type])

    -- List of surveys
    INSERT INTO #SurveyTable ([SurveyID], [ActivityID], [ReportServer], [ReportDB])
    SELECT s.[SurveyID], s.[ActivityID], s.[ReportServer], s.[ReportDB]
    FROM [at].[Survey] s
    WHERE s.[ActivityID] IN (@LogSectionActivityID, @MeasureActivityID) AND s.[Status] = 2

    -- List of all registered actors
    INSERT INTO #User ([UserID], [SSN], [LastName], [FirstName], [ActorChildrenNumber], [RelayChildrenNumber], [MovingLogCount], [WaitingLogCount])
    SELECT u.[UserID], u.[SSN], u.[LastName], u.[FirstName], 0 AS [ActorChildrenNumber], 0 AS [RelayChildrenNumber], 0 AS [MovingLogCount], 0 AS [WaitingLogCount]
    FROM [org].[User] u JOIN dbo.funcListToTableInt(@UserIDList, ',') l ON u.[UserID] = l.[Value]
    
    IF NOT EXISTS (SELECT 1 FROM #User) BEGIN GOTO ReturnValues END

    DECLARE @ReportServer nvarchar(64) = '', @ReportDB nvarchar(64) = ''

    DECLARE c_RDB CURSOR LOCAL FAST_FORWARD READ_ONLY FOR
    SELECT DISTINCT st.[ReportServer], st.[ReportDB] FROM #SurveyTable st

    OPEN c_RDB
    FETCH NEXT FROM c_RDB INTO @ReportServer, @ReportDB
    WHILE @@FETCH_STATUS = 0
    BEGIN
        -- Get all relay children
        SET @sqlCommand = 
       'INSERT INTO #Children ([ResponsibleUserID], [ChildID], [Type], [MovingLog])
        SELECT u.[UserID], ls.[UserID], ''H'' AS [Type], (CASE WHEN tfrh.[Value] IS NOT NULL OR tfom.[Value] IS NOT NULL THEN 1 ELSE 0 END) AS [MovingLog]
        FROM $(ReportServer).$(ReportDB).dbo.[Result] ls
        JOIN #SurveyTable st ON ls.[SurveyID] = st.[SurveyID] AND st.[ActivityID] = ' + CAST(@LogSectionActivityID AS nvarchar(32)) + '
         AND ls.[Status] >= 0 AND ls.[EndDate] IS NULL
        JOIN $(ReportServer).$(ReportDB).dbo.[Answer] lsa ON ls.[ResultID] = lsa.[ResultID] AND lsa.[QuestionID] = ' + CAST(@QID_HolderID AS nvarchar(32)) + '
         AND lsa.[AlternativeID] = ' + CAST(@AltID_HolderID AS nvarchar(32)) + '
        JOIN #User u ON lsa.[Free] = CAST(u.[UserID] AS nvarchar(32))
        LEFT JOIN $(ReportServer).$(ReportDB).dbo.[Answer] tfrh ON ls.[ResultID] = tfrh.[ResultID] AND tfrh.[QuestionID] = ' + CAST(@QID_TransferRHID AS nvarchar(32)) + '
         AND tfrh.[AlternativeID] = ' + CAST(@AltID_TransferRHID AS nvarchar(32)) + '
        LEFT JOIN $(ReportServer).$(ReportDB).dbo.[Answer] tfom ON ls.[ResultID] = tfom.[ResultID] AND tfom.[QuestionID] = ' + CAST(@QID_TransferOMID AS nvarchar(32)) + '
         AND tfom.[AlternativeID] = ' + CAST(@AltID_TransferOMID AS nvarchar(32))
        SET @sqlCommand = REPLACE(@sqlCommand, '$(ReportServer)', QUOTENAME(@ReportServer))
        SET @sqlCommand = REPLACE(@sqlCommand, '$(ReportDB)', QUOTENAME(@ReportDB))
        EXEC (@sqlCommand)

        -- Get all actor children
        SET @sqlCommand = 
       'INSERT INTO #Children ([ResponsibleUserID], [ChildID], [Type], [MovingLog])
        SELECT u.[UserID], mea.[UserID], ''A'' AS [Type], 0 AS [MovingLog]
        FROM $(ReportServer).$(ReportDB).dbo.[Result] mea
        JOIN #SurveyTable st ON mea.[SurveyID] = st.[SurveyID] AND st.[ActivityID] = ' + CAST(@MeasureActivityID AS nvarchar(32)) + '
         AND mea.[Status] >= 0 AND mea.[EndDate] IS NULL
        JOIN $(ReportServer).$(ReportDB).dbo.[Answer] ma ON mea.[ResultID] = ma.[ResultID] AND ma.[QuestionID] = ' + CAST(@QID_Actor AS nvarchar(32)) + '
         AND ma.[AlternativeID] = ' + CAST(@AltID_Actor AS nvarchar(32)) + '
        JOIN #User u ON ma.[Free] = CAST(u.[UserID] AS nvarchar(32))
         AND EXISTS (SELECT 1 FROM $(ReportServer).$(ReportDB).dbo.[Result] PoA
                              JOIN $(ReportServer).$(ReportDB).dbo.[Result] ls ON PoA.[ParentResultID] = ls.[ResultID] AND PoA.[ResultID] = mea.[ParentResultID]
                               AND PoA.[Status] >= 0 AND PoA.[EndDate] IS NULL AND ls.[Status] >= 0 AND ls.[EndDate] IS NULL)'
        SET @sqlCommand = REPLACE(@sqlCommand, '$(ReportServer)', QUOTENAME(@ReportServer))
        SET @sqlCommand = REPLACE(@sqlCommand, '$(ReportDB)', QUOTENAME(@ReportDB))
        EXEC (@sqlCommand)
        
        FETCH NEXT FROM c_RDB INTO @ReportServer, @ReportDB
    END
    CLOSE c_RDB
    DEALLOCATE c_RDB

    -- Count children in the waiting list
    SET @sqlCommand = 
   'INSERT INTO #Children ([ResponsibleUserID], [ChildID], [Type], [MovingLog])
    SELECT u.[UserID], ls.[UserID], ''W'' AS [Type], 1 AS [MovingLog]
    FROM $(ReportServer).$(ReportDB).dbo.[Result] ls
    JOIN #SurveyTable st ON ls.[SurveyID] = st.[SurveyID] AND st.[ActivityID] = ' + CAST(@LogSectionActivityID AS nvarchar(32)) + '
        AND ls.[Status] >= 0 AND ls.[EndDate] IS NULL
    JOIN $(ReportServer).$(ReportDB).dbo.[Answer] tfrh ON ls.[ResultID] = tfrh.[ResultID] AND tfrh.[QuestionID] = ' + CAST(@QID_TransferRHID AS nvarchar(32)) + '
        AND tfrh.[AlternativeID] = ' + CAST(@AltID_TransferRHID AS nvarchar(32)) + '
    JOIN #User u ON tfrh.[Value] = u.[UserID]'
    SET @sqlCommand = REPLACE(@sqlCommand, '$(ReportServer)', QUOTENAME(@ReportServer))
    SET @sqlCommand = REPLACE(@sqlCommand, '$(ReportDB)', QUOTENAME(@ReportDB))
    EXEC (@sqlCommand)

    INSERT INTO #ChildrenCount ([ResponsibleUserID], [Type], [ChildrenCount], [MovingCount])
    SELECT v.[ResponsibleUserID], v.[Type], COUNT(v.[ChildID]) AS [ChildrenCount], SUM(v.[MovingLog]) AS [MovingCount]
    FROM (SELECT c.[ResponsibleUserID], c.[Type], c.[ChildID], MAX([c].[MovingLog]) AS [MovingLog]
          FROM #Children c
          GROUP BY c.[ResponsibleUserID], c.[Type], c.[ChildID]) AS v
    GROUP BY v.[ResponsibleUserID], v.[Type]

    UPDATE u SET u.[RelayChildrenNumber] = cc.[ChildrenCount], u.[MovingLogCount] = cc.[MovingCount]
    FROM #User u JOIN #ChildrenCount cc ON u.[UserID] = cc.[ResponsibleUserID] AND cc.[Type] = 'H'

    UPDATE u SET u.[ActorChildrenNumber] = cc.[ChildrenCount]
    FROM #User u JOIN #ChildrenCount cc ON u.[UserID] = cc.[ResponsibleUserID] AND cc.[Type] = 'A'

    UPDATE u SET u.[WaitingLogCount] = cc.[ChildrenCount]
    FROM #User u JOIN #ChildrenCount cc ON u.[UserID] = cc.[ResponsibleUserID] AND cc.[Type] = 'W'

ReturnValues:
    
    DECLARE @OutputHeader nvarchar(max), @OutputFooter nvarchar(max),
            @UserID nvarchar(32), @SSN nvarchar(32) = '', @FullName nvarchar(max) = '',
            @ActorChildrenNumber nvarchar(32), @RelayChildrenNumber nvarchar(32), @MovingLogCount nvarchar(32), @WaitingLogCount nvarchar(32)

    SET @OutputHeader = '<table border=''1'' style=''width:500px;border:1px;border: 1px solid black;border-collapse: collapse''>
 <tr style=''background-color: #7092BE;color: white''>
  <td style=''border: 1px solid black''>UserID</td>
  <td style=''width:100px;border: 1px solid black''>Name</td>
  <td style=''width:100px;border: 1px solid black''>SSN</td>
  <td style=''border: 1px solid black''>Relay children</td>
  <td style=''border: 1px solid black''>Actor children</td>
  <td style=''border: 1px solid black''>Transferring children</td>
  <td style=''border: 1px solid black''>Waiting children</td>
 </tr>
 
 '
    SET @OutputFooter = '</table>'
    SET @OutputMessage = @OutputHeader

    DECLARE c_User CURSOR LOCAL FAST_FORWARD READ_ONLY FOR
    SELECT u.[UserID], u.[SSN], (u.[FirstName] + ' ' + u.[LastName]) AS [FullName],
           u.[ActorChildrenNumber], u.[RelayChildrenNumber], u.[MovingLogCount], u.[WaitingLogCount]
    FROM #User u

    OPEN c_User
    FETCH NEXT FROM c_User INTO @UserID, @SSN, @FullName, @ActorChildrenNumber, @RelayChildrenNumber, @MovingLogCount, @WaitingLogCount
    WHILE @@FETCH_STATUS = 0
    BEGIN
        SET @OutputMessage = @OutputMessage +
' <tr>
  <td style=''border: 1px solid black''>' + @UserID + '</td>
  <td style=''width:100px,border: 1px solid black''>' + @FullName + '</td>
  <td style=''width:100px;border: 1px solid black''>' + @SSN + '</td>
  <td border: 1px solid black>' + @RelayChildrenNumber + '</td>
  <td border: 1px solid black>' + @ActorChildrenNumber + '</td>
  <td border: 1px solid black>' + @MovingLogCount + '</td>
  <td border: 1px solid black>' + @WaitingLogCount + '</td>
 </tr>
'
        FETCH NEXT FROM c_User INTO @UserID, @SSN, @FullName, @ActorChildrenNumber, @RelayChildrenNumber, @MovingLogCount, @WaitingLogCount
    END
    CLOSE c_User
    DEALLOCATE c_User

    SET @OutputMessage = @OutputMessage + @OutputFooter

    DROP TABLE #SurveyTable
    DROP TABLE #User
    DROP TABLE #Children
    DROP TABLE #ChildrenCount
    DROP TABLE #ActorInfoQuestionTable
END

GO
