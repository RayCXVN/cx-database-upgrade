/****** Object:  StoredProcedure [rl].[prc_SearchUserByUserTypeAndDepartment]    Script Date: 5/11/2017 10:41:02 AM ******/
DROP PROCEDURE [rl].[prc_SearchUserByUserTypeAndDepartment]
GO
/****** Object:  StoredProcedure [rl].[prc_SearchUserByUserTypeAndDepartment]    Script Date: 5/11/2017 10:41:02 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/*
	2017-04-24	-	Sarah - Using EntityStatusID column
*/
CREATE PROCEDURE [rl].[prc_SearchUserByUserTypeAndDepartment]  
(  
     @ParentHDID     INT = NULL,  
     @DepartmentID   INT = NULL,  
     @SearchString   NVARCHAR(MAX),   
     @UserType       INT,
     @ListDeniedUserID NVARCHAR(MAX),      
     @SiteID         INT,
	 @SearchOnName	 BIT = 0,
	 @SearchAllMunicipality	     BIT = 0
)  
As  
BEGIN  
 SET NOCOUNT ON  
 DECLARE @Err Int  
 DECLARE @PropPosition INT, @PropOfficePhone INT
 DECLARE @EntityStatus_Active INT
 
 SELECT @EntityStatus_Active = E.EntityStatusID FROM EntityStatus E WHERE E.CodeName = 'Active'
 SELECT @PropPosition    = [Value] FROM app.[SiteParameter] WHERE [Key] = 'PROP_Position'     AND [SiteID] = @SiteID
 SELECT @PropOfficePhone = [Value] FROM app.[SiteParameter] WHERE [Key] = 'PROP_OfficePhone'  AND [SiteID] = @SiteID 

 IF OBJECT_ID('tempdb..#ListDeniedUserID') IS NOT NULL DROP TABLE #ListDeniedUserID
 SELECT Value AS DeniedUserID INTO #ListDeniedUserID FROM [dbo].[funcListToTableInt](@ListDeniedUserID,',')
   
     SELECT  DISTINCT
        u.[UserID],  
        u.[DepartmentID], 
        D.Name AS [DepartmentName] ,      
        u.[LastName],  
        u.[FirstName],  
        u.[Email],  
        u.[SSN],    
        PVPosition.Value AS [Position],
        PVOfficePhone.Value AS [OfficePhone],  
        u.[Mobile]
    FROM org.[User] u
    INNER JOIN org.UT_U UTU ON u.UserID = UTU.UserID AND UTU.UserTypeID = @UserType AND NOT EXISTS (SELECT 1 FROM #ListDeniedUserID WHERE DeniedUserID = u.UserID)
    INNER JOIN org.[H_D] hd on hd.DepartmentID = u.DepartmentID
    INNER JOIN org.Department D ON D.DepartmentID = hd.DepartmentID
           AND ((@DepartmentID IS NULL AND @SearchAllMunicipality = 0 AND hd.[Path] LIKE '%\' + CAST(@ParentHDID AS nvarchar(32)) + '\%') 
		       OR (@DepartmentID IS NOT NULL AND HD.DepartmentID = @DepartmentID)
		       OR (@SearchAllMunicipality = 1)) 
    LEFT JOIN app.LoginService_User lsu ON u.UserID = lsu.UserID
    LEFT JOIN  prop.PropValue PVPosition	ON PVPosition.ItemID = u.UserID AND PVPosition.PropertyID = @PropPosition
	LEFT JOIN  prop.PropValue PVOfficePhone ON PVOfficePhone.ItemID = u.UserID AND PVOfficePhone.PropertyID = @PropOfficePhone        
    WHERE u.EntityStatusID = @EntityStatus_Active AND u.Deleted IS NULL AND
    (
	(@SearchOnName = 0 AND 
		((u.[UserName] LIKE '%'+ @SearchString + '%')  
		OR (u.[FirstName] LIKE '%'+ @SearchString + '%')  
		OR (u.LastName LIKE '%'+ @SearchString + '%')  
		OR (u.[FirstName] + ' ' + u.[LastName]  like '%'+ @SearchString + '%')  
		OR (u.[LastName] LIKE '%'+ @SearchString + '%')    
		OR (D.[Name] LIKE '%'+ @SearchString + '%')    
		OR (PVPosition.Value LIKE '%'+ @SearchString + '%'))
	) OR     
    (@SearchOnName <> 0  AND
     (( u.[FirstName] LIKE '%'+ @SearchString + '%')  
	OR (u.LastName LIKE '%'+ @SearchString + '%')  
    OR (u.[FirstName] + ' ' + u.[LastName]  like '%'+ @SearchString + '%')))
	)
 Set @Err = @@Error  
 RETURN @Err  

END


GO
