/****** Object:  StoredProcedure [rl].[prc_CheckExists_PoA_Measure_Active]    Script Date: 5/11/2017 10:41:02 AM ******/
DROP PROCEDURE [rl].[prc_CheckExists_PoA_Measure_Active]
GO
/****** Object:  StoredProcedure [rl].[prc_CheckExists_PoA_Measure_Active]    Script Date: 5/11/2017 10:41:02 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [rl].[prc_CheckExists_PoA_Measure_Active]
(   @ParentResultID         BIGINT,
    @IsPoA					BIT,
	@SiteID					INT,
	@Return					BIT OUT,
	@TotalResult			INT OUT
) AS
BEGIN 
    SET NOCOUNT ON
    DECLARE @PoAActivityID int, @MeasureActivityID INT, @SqlCommand NVARCHAR(MAX), @ServerReport VARCHAR(MAX), @DBReport VARCHAR(MAX)
	SET @Return = 0     
    CREATE TABLE #Result (ResultID BIGINT, EndDate DATETIME) 
	      
    SELECT @MeasureActivityID	 = [Value] FROM app.[SiteParameter] WHERE [Key] = 'ACTIVITY_ID_MEASURE' AND [SiteID] = @SiteID
    SELECT @PoAActivityID		 = [Value] FROM app.[SiteParameter] WHERE [Key] = 'ACTIVITY_ID_POINT_OF_ATTENTION' AND [SiteID] = @SiteID
    
    SELECT SurveyID, ReportServer, ReportDB       
          INTO #ConnectionTable    
    FROM at.Survey WHERE (ActivityID = @PoAActivityID AND @IsPoA = 1) OR (ActivityID = @MeasureActivityID AND @IsPoA = 0)

    DECLARE c_looping CURSOR LOCAL FAST_FORWARD READ_ONLY FOR
    SELECT DISTINCT CT.ReportServer, CT.ReportDB  FROM #ConnectionTable CT  
              
    OPEN c_looping
    FETCH NEXT FROM c_looping INTO @ServerReport, @DBReport
    WHILE @@FETCH_STATUS = 0
    BEGIN

		SET @SqlCommand = '    
        SELECT r.ResultID, r.EndDate
        FROM ['+ @ServerReport + '].[' + @DBReport + '].[dbo].[Result] r
        INNER JOIN #ConnectionTable ct ON ct.SurveyID = r.SurveyID AND ct.ReportServer = '''+ CONVERT(VARCHAR(255),@ServerReport) + '''
		 AND ct.ReportDB = '''+ CONVERT(VARCHAR(255),@DBReport) + '''
			and R.ParentResultID ='+ CONVERT(VARCHAR(255),@ParentResultID)    

    INSERT INTO #Result (ResultID,EndDate ) 
    EXEC(@SqlCommand)

	   FETCH NEXT FROM c_looping INTO @ServerReport, @DBReport
    END
    CLOSE c_looping
    DEALLOCATE c_looping	
	
	IF EXISTS (SELECT 1 FROM #Result WHERE EndDate IS NULL ) --
	BEGIN
		SET @Return = 1		
	END

	SET @TotalResult = (SELECT COUNT(*) FROM #Result)

	RETURN @Return
	RETURN @TotalResult

	SET NOCOUNT OFF
 END


GO
