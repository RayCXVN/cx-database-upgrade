/****** Object:  StoredProcedure [rl].[prc_LogSection_GetBySSN]    Script Date: 5/11/2017 10:41:02 AM ******/
DROP PROCEDURE [rl].[prc_LogSection_GetBySSN]
GO
/****** Object:  StoredProcedure [rl].[prc_LogSection_GetBySSN]    Script Date: 5/11/2017 10:41:02 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/*
	Nov 23, 2016 - Sarah - #60389 - [Transfer log] - Show the tranferred log history on the log which is created from pacified log
	2017-04-24	 - Sarah - Using EntityStatusID column
	EXEC [rl].[prc_LogSection_GetBySSN] @SSN
*/
CREATE PROCEDURE [rl].[prc_LogSection_GetBySSN]
(   @SSN                    NVARCHAR(MAX),
	@DepartmentID           INT, 
    @ParentHDID             INT,    
    @SiteID                 INT = 1
) AS
BEGIN 
    SET NOCOUNT ON
    DECLARE @LogSectionActivityID int, @LogSectionSurveyID INT,@LogSectionServerReport VARCHAR(MAX), @LogSectionDBReport VARCHAR(MAX), @LogSectionUserID INT, @Return BIT       
    DECLARE @ParentDepartmentID INT    
    DECLARE @QTxml XML, @TableString nvarchar(max), @SqlCommand NVARCHAR(MAX), @UTMunicipality INT
	DECLARE @EntityStatus_Active INT
	SELECT @EntityStatus_Active = E.EntityStatusID FROM EntityStatus E WHERE E.CodeName = 'Active'
    CREATE TABLE #LogSection ([ResultID] bigint,[Role] NVARCHAR(MAX), [UserID] int, [EndDate] DATE, ParentResultID bigint,[Created] DATETIME ,[IsPassive] BIT, RHUserID INT)                            
    
    
    
    CREATE TABLE #ReturnTable ([ResultID] bigint,[Role] NVARCHAR(MAX), [UserID] int, [FirstName] nvarchar(max), [LastName] nvarchar(max), [DepartmentName] NVARCHAR(MAX),
                               [Position] NVARCHAR(MAX), [Email] NVARCHAR(MAX), [OfficePhone] NVARCHAR(MAX), [Mobile] NVARCHAR(MAX),[IsPassive] BIT) 

    /* The structure of site parameter LISTCOLUMN_ClientInfo should be
    SET @TableString = '<ClientInfo Name="FirstName" QuestionID="1" AlternativeID="1" DataType="text" />
    <ClientInfo Name="LastName" QuestionID="1" AlternativeID="9" DataType="text" />
    <ClientInfo Name="Age" QuestionID="1" AlternativeID="66" DataType="int" />
    <ClientInfo Name="Level" QuestionID="4" AlternativeID="0" DataType="int" />
    <ClientInfo Name="Picture" QuestionID="1" AlternativeID="15" DataType="text" />'
*/
    SELECT @TableString          = [Value] FROM app.[SiteParameter] WHERE [Key] = 'LISTCOLUMN_ClientInfo' AND [SiteID] = @SiteID   
    SELECT @LogSectionActivityID = [Value] FROM app.[SiteParameter] WHERE [Key] = 'ACTIVITY_ID_LOG_SECTION' AND [SiteID] = @SiteID
    SELECT @UTMunicipality       = [Value] FROM app.[SiteParameter] WHERE [Key] = 'UT_MUNICIPALITY_ADMINISTRATOR' AND [SiteID] = @SiteID
    SET @QTxml = CONVERT(xml, @TableString)
    SELECT Tbl.Col.value('@Name',           'nvarchar(max)') [Name],
           Tbl.Col.value('@QuestionID',     'int') [QuestionID],
           Tbl.Col.value('@AlternativeID',  'int') [AlternativeID],
           Tbl.Col.value('@DataType',       'nvarchar(64)') [DataType]
        INTO #QuestionTable
    FROM @QTxml.nodes('//ClientInfo') Tbl(Col)    
 
	SET @ParentDepartmentID = (SELECT DepartmentID FROM org.H_D WHERE HDID = @ParentHDID)
    IF OBJECT_ID('tempdb..#ListDeptTable') IS NOT NULL DROP TABLE #ListDeptTable
    SELECT DepartmentID INTO #ListDeptTable
    FROM org.H_D HD 
    WHERE HD.[Path] LIKE '%\'+ CONVERT(VARCHAR(5),@ParentHDID)+'\%' AND HD.Deleted = 0

    IF EXISTS (SELECT 1 FROM org.[User] u WHERE u.SSN = @SSN AND u.DepartmentID = @DepartmentID AND u.EntityStatusID = @EntityStatus_Active AND u.Deleted IS NULL )
    BEGIN

    DECLARE @HolderXML nvarchar(max), @QID_HolderID INT, @AltID_HolderID INT, @PropPosition INT, @PropOfficePhone INT, @RLUserID INT   
    SELECT @HolderXML       = [Value] FROM app.[SiteParameter] WHERE [Key] = 'LISTCOLUMN_Holder' AND [SiteID] = @SiteID
            SET @QTxml = CONVERT(xml, @HolderXML)            
            SELECT @QID_HolderID = Tbl.Col.value('@QuestionID', 'int'), @AltID_HolderID = Tbl.Col.value('@AlternativeID', 'int')
            FROM @QTxml.nodes('//Holder') Tbl(Col)
            WHERE Tbl.Col.value('@Name', 'nvarchar(max)') = 'HolderID'
    
    SELECT UserID INTO #LogSectionUserID
    FROM org.[User] WHERE SSN = @SSN AND EntityStatusID = @EntityStatus_Active AND Deleted IS NULL AND DepartmentID = @DepartmentID   
    
    SELECT SurveyID, ReportServer, ReportDB       
          INTO #ConnectionTable    
    FROM at.Survey WHERE ActivityID =@LogSectionActivityID
    
    DECLARE c_LogSection CURSOR LOCAL FAST_FORWARD READ_ONLY FOR
    SELECT DISTINCT CT.ReportServer, CT.ReportDB  FROM #ConnectionTable CT  
              
    OPEN c_LogSection
    FETCH NEXT FROM c_LogSection INTO @LogSectionServerReport, @LogSectionDBReport
    WHILE @@FETCH_STATUS = 0
    BEGIN
    SET @SqlCommand = '    
        SELECT DISTINCT r.[ResultID], r.[UserID], r.[EndDate], r.[ParentResultID], r.[Created], 0 AS [IsPassive], rh.Free AS [RHUserID] 
        FROM ['+ @LogSectionServerReport + '].[' + @LogSectionDBReport + '].[dbo].[Result] r 
        INNER JOIN #ListDeptTable Dept ON Dept.DepartmentID = r.DepartmentID
        INNER JOIN ['+ @LogSectionServerReport + '].[' + @LogSectionDBReport + '].[dbo].[Answer] rh ON r.[ResultID] = rh.[ResultID] AND r.EntityStatusID = ' + CAST(@EntityStatus_Active AS VARCHAR(128)) +' AND r.Deleted IS NULL
                     AND rh.QuestionID = '+ CONVERT(VARCHAR(10),@QID_HolderID) +'  AND rh.AlternativeID = '+ CONVERT(VARCHAR(10),@AltID_HolderID) +'   
        INNER JOIN #LogSectionUserID lsu ON lsu.UserID =  r.UserID 
        INNER JOIN #ConnectionTable ct ON ct.SurveyID = r.SurveyID '      
       
    INSERT INTO #LogSection ([ResultID], [UserID], [EndDate],[ParentResultID], [Created], [IsPassive], [RHUserID])
    EXEC(@SqlCommand)

    FETCH NEXT FROM c_LogSection INTO @LogSectionServerReport, @LogSectionDBReport
    END
    CLOSE c_LogSection
    DEALLOCATE c_LogSection
    
	-- Update IsPassive column if EndDate is not null
    UPDATE #LogSection SET IsPassive = 1 WHERE EndDate IS NOT NULL
	        
	SELECT @PropPosition    = [Value] FROM app.[SiteParameter] WHERE [Key] = 'PROP_Position'     AND [SiteID] = @SiteID
    SELECT @PropOfficePhone = [Value] FROM app.[SiteParameter] WHERE [Key] = 'PROP_OfficePhone'  AND [SiteID] = @SiteID   
                       
    INSERT INTO #ReturnTable (ResultID ,[Role] ,UserID , FirstName, LastName ,DepartmentName ,Position ,Email ,OfficePhone ,Mobile, IsPassive )
    SELECT ls.ResultID, 'txtRelayholder' AS [Role], u.UserID, u.FirstName, u.LastName, d.Name AS DepartmentName, PVPosition.Value AS [Position], u.Email,
                        PVOfficePhone.Value AS OfficePhone, u.Mobile, ls.IsPassive
    FROM #LogSection ls
		INNER JOIN org.[User] u ON ls.[RHUserID] = u.UserID
		INNER JOIN org.Department d ON d.DepartmentID = u.DepartmentID          
		LEFT JOIN  prop.PropValue PVPosition ON PVPosition.ItemID = u.UserID AND PVPosition.PropertyID = @PropPosition
		LEFT JOIN  prop.PropValue PVOfficePhone ON PVOfficePhone.ItemID = u.UserID AND PVOfficePhone.PropertyID = @PropOfficePhone     
  
	-- INSERT MUNICIPALITY ADMINISRTATOR
	INSERT INTO #ReturnTable (ResultID ,[Role] ,UserID , FirstName, LastName ,DepartmentName ,Position ,Email ,OfficePhone ,Mobile, IsPassive)             
	SELECT NULL AS ResultID, 'txtResponsibleMunicipality' AS [Role], u.UserID, u.FirstName, u.LastName, d.Name AS DepartmentName, PVPosition.Value AS Position, 
							u.Email, PVOfficePhone.Value AS OfficePhone, u.Mobile, 0 AS IsPassive
	FROM org.[User] u
		INNER JOIN org.Department d ON d.DepartmentID = u.DepartmentID AND U.DepartmentID = @ParentDepartmentID
		INNER JOIN org.UT_U utu ON utu.UserID = u.UserID AND utu.UserTypeID = @UTMunicipality        
		LEFT JOIN  prop.PropValue PVPosition ON PVPosition.ItemID = u.UserID AND PVPosition.PropertyID = @PropPosition
		LEFT JOIN  prop.PropValue PVOfficePhone ON PVOfficePhone.ItemID = u.UserID AND PVOfficePhone.PropertyID = @PropOfficePhone
	END

	SELECT * FROM #ReturnTable ORDER BY [Role], [FirstName], [LastName] DESC
	
    DROP TABLE #LogSection
    DROP TABLE #ReturnTable
END


GO
