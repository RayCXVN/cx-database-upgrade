/****** Object:  StoredProcedure [rl].[prc_getAllActorByHDID]    Script Date: 5/11/2017 10:41:02 AM ******/
DROP PROCEDURE [rl].[prc_getAllActorByHDID]
GO
/****** Object:  StoredProcedure [rl].[prc_getAllActorByHDID]    Script Date: 5/11/2017 10:41:02 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/*
	2016-12-15 Bug #63917 - [Actor menu] -  the latest actor ,which is added by RH, replaces all the old ones; missing position
	2017-04-24	 - Sarah - Using EntityStatusID column
*/
CREATE PROCEDURE [rl].[prc_getAllActorByHDID]
(
	@UserTypeID           INT,
	@ParentHDID           INT,
    @SiteID               INT = 1,
    @PeriodID             INT = 1,
    @UserID               INT = 0   
)
AS
BEGIN
	SET NOCOUNT ON
    
    DECLARE @LogSectionActivityID INT, @MeasureActivityID INT, @QID_ActorDepartmentID INT, @AltID_ActorDepartmentID INT,
            @QID_Actor int, @AltID_Actor INT, @QID_HolderID int, @AltID_HolderID int,
            @ActorXML nvarchar(max), @QTxml XML, @HolderXML nvarchar(max), @ActorInfoXML VARCHAR(MAX),            
            @PropPosition INT, @PropOfficePhone INT, @DepartmentBackground INT, @ParentDepartmentID int,
            @sqlCommand nvarchar(max) = ''
	DECLARE @EntityStatus_Active INT
	SELECT @EntityStatus_Active = E.EntityStatusID FROM EntityStatus E WHERE E.CodeName = 'Active'
    /*
     -- The structure of site parameter LISTCOLUMN_Actor should be
    SET @ActorXML = '<Actor QuestionID="10" AlternativeID="43" />'

    -- The structure of site parameter LISTCOLUMN_Holder should be
    SET @HolderXML = N'<Holder Name="HolderID" QuestionID="5" AlternativeID="5" DataType="text" />
    */
    SELECT @ActorXML             = [Value] FROM app.[SiteParameter] WHERE [Key] = 'LISTCOLUMN_Actor'            AND [SiteID] = @SiteID        
    SELECT @HolderXML            = [Value] FROM app.[SiteParameter] WHERE [Key] = 'LISTCOLUMN_Holder'           AND [SiteID] = @SiteID
    SELECT @ActorInfoXML         = [Value] FROM app.[SiteParameter] WHERE [Key] = 'LISTCOLUMN_ActorInfo'        AND [SiteID] = @SiteID
    SELECT @LogSectionActivityID = [Value] FROM app.[SiteParameter] WHERE [Key] = 'ACTIVITY_ID_LOG_SECTION'     AND [SiteID] = @SiteID
    SELECT @MeasureActivityID    = [Value] FROM app.[SiteParameter] WHERE [Key] = 'ACTIVITY_ID_MEASURE'         AND [SiteID] = @SiteID 
    SELECT @PropPosition         = [Value] FROM app.[SiteParameter] WHERE [Key] = 'PROP_Position'               AND [SiteID] = @SiteID
    SELECT @PropOfficePhone      = [Value] FROM app.[SiteParameter] WHERE [Key] = 'PROP_OfficePhone'            AND [SiteID] = @SiteID    
    SELECT @DepartmentBackground = [Value] FROM app.[SiteParameter] WHERE [Key] = 'PROP_DepartmentBackground'   AND [SiteID] = @SiteID    
    
    SET @QTxml = CONVERT(xml, @ActorXML)
    SELECT @QID_Actor = Tbl.Col.value('@QuestionID', 'int'), @AltID_Actor = Tbl.Col.value('@AlternativeID', 'int')
    FROM @QTxml.nodes('//Actor') Tbl(Col)     

    SET @QTxml = CONVERT(xml, @HolderXML)
    SELECT @QID_HolderID = Tbl.Col.value('@QuestionID', 'int'), @AltID_HolderID = Tbl.Col.value('@AlternativeID', 'int')
    FROM @QTxml.nodes('//Holder') Tbl(Col)
    WHERE Tbl.Col.value('@Name', 'nvarchar(max)') = 'HolderID'

    SET @QTxml = CONVERT(xml, @ActorInfoXML)
    SELECT @QID_ActorDepartmentID = Tbl.Col.value('@QuestionID', 'int'), @AltID_ActorDepartmentID = Tbl.Col.value('@AlternativeID', 'int')
    FROM @QTxml.nodes('//ActorInfo') Tbl(Col)
    WHERE Tbl.Col.value('@Name', 'nvarchar(max)') = 'ActorDepartmentID'

    SELECT HD.DepartmentID, d.[Name] INTO #DepartmentTable
    FROM org.H_D HD JOIN [org].[Department] d ON HD.[DepartmentID] = d.[DepartmentID] AND d.EntityStatusID = @EntityStatus_Active AND d.Deleted IS NULL AND HD.Deleted = 0
     AND HD.[Path] LIKE '%\' + CONVERT(VARCHAR(32), @ParentHDID) +'\%' 
	
    SELECT @ParentDepartmentID = hd.[DepartmentID] FROM [org].[H_D] hd WHERE [hd].[HDID] = @ParentHDID

	CREATE TABLE #User ([DepartmentID] INT, [DepartmentName] NVARCHAR(MAX), [OrgName] NVARCHAR(MAX), [DepartmentBorderColor] NVARCHAR(MAX), [UserID] INT,
                        [LastName] NVARCHAR(MAX), [FirstName] NVARCHAR(MAX), [Position] NVARCHAR(MAX), [OfficePhone] VARCHAR(MAX),
                        [Mobile] VARCHAR(MAX), [Email] NVARCHAR(MAX), [ActorChildrenNumber] int, [RelayChildrenNumber] int, [SSN] nvarchar(32))
    CREATE TABLE #SurveyTable ([SurveyID] int PRIMARY KEY, [ActivityID] int, [ReportServer] nvarchar(64), [ReportDB] nvarchar(64))
    CREATE TABLE #Children ([ResponsibleUserID] int, [DepartmentID] int, [ChildID] int, [Type] char(1))             -- Tyep: A - Actor, H - Holder
    CREATE TABLE #ChildrenCount ([ResponsibleUserID] int, [DepartmentID] int, [Type] char(1), [ChildrenCount] int)  -- Tyep: A - Actor, H - Holder
    CREATE TABLE #ActorInfoQuestionTable ([Name] nvarchar(max), [QuestionID] int, [AlternativeID] int, [DataType] nvarchar(64))
    CREATE TABLE #VirtualActor ([SSN] nvarchar(32), [OrgName] nvarchar(max), [DepartmentBorderColor] nvarchar(max),
                                [LastName] nvarchar(max), [FirstName] nvarchar(max), [Position] nvarchar(max), [OfficePhone] varchar(max),
                                [Mobile] varchar(max), [Email] nvarchar(max), [ChildID] int, [Created] datetime)
	
    CREATE CLUSTERED INDEX IDX_#User_UserID ON #User([UserID])	
	CREATE INDEX IDX_#User_DepartmentID ON #User([DepartmentID])
    CREATE INDEX IDX_#User_SSN ON #User([SSN])
    CREATE INDEX IDX_#SurveyTable_ActivityID ON #SurveyTable([ActivityID])
    CREATE INDEX IDX_#Children_ResponsibleUserID ON #Children ([ResponsibleUserID], [DepartmentID], [Type])
    CREATE INDEX IDX_#VirtualActor_SSN ON #VirtualActor ([SSN])

    -- List of surveys
    INSERT INTO #SurveyTable ([SurveyID], [ActivityID], [ReportServer], [ReportDB])
    SELECT s.[SurveyID], s.[ActivityID], s.[ReportServer], s.[ReportDB]
    FROM [at].[Survey] s
    WHERE s.[ActivityID] IN (@LogSectionActivityID, @MeasureActivityID) AND s.[Status] = 2
      AND EXISTS (SELECT 1 FROM [at].[Batch] b WHERE b.[SurveyID] = s.[SurveyID] AND b.[DepartmentID] = @ParentDepartmentID AND b.[Status] = 2)

    -- List of all registered actors
    INSERT INTO #User ([DepartmentID], [DepartmentName], [DepartmentBorderColor], [UserID], [LastName], [FirstName], [Mobile], [Email],
                       [ActorChildrenNumber], [RelayChildrenNumber])
    SELECT u.[DepartmentID], dl.[Name], NULL AS [DepartmentBorderColor], u.[UserID], u.[LastName], u.[FirstName], u.[Mobile], u.[Email],
           0 AS [ActorChildrenNumber], 0 AS [RelayChildrenNumber]
    FROM [org].[User] u JOIN #DepartmentTable dl ON u.[DepartmentID] = dl.DepartmentID AND u.EntityStatusID = @EntityStatus_Active AND u.Deleted IS NULL
    JOIN [org].[UT_U] utu ON u.[UserID] = utu.[UserID] AND utu.[UserTypeID] = @UserTypeID
    UNION
    SELECT ud.[DepartmentID], dl.[Name], NULL AS [DepartmentBorderColor], u.[UserID], u.[LastName], u.[FirstName], u.[Mobile], u.[Email],
           0 AS [ActorChildrenNumber], 0 AS [RelayChildrenNumber]
    FROM [org].[U_D] ud JOIN #DepartmentTable dl ON ud.[DepartmentID] = dl.DepartmentID
    JOIN [org].[UT_U] utu ON ud.[UserID] = utu.[UserID] AND utu.[UserTypeID] = @UserTypeID
    JOIN [org].[User] u ON ud.[UserID] = u.[UserID] AND u.EntityStatusID = @EntityStatus_Active AND u.Deleted IS NULL

    UPDATE u SET u.[Position] = PVPosition.[Value], u.[OfficePhone] = PVOfficePhone.[Value]
    FROM #User u
    LEFT JOIN prop.PropValue PVPosition	ON PVPosition.ItemID = u.UserID AND PVPosition.PropertyID = @PropPosition
    LEFT JOIN prop.PropValue PVOfficePhone ON PVOfficePhone.ItemID = u.UserID AND PVOfficePhone.PropertyID = @PropOfficePhone

    DECLARE @ReportServer nvarchar(64) = '', @ReportDB nvarchar(64) = ''

    DECLARE c_RDB CURSOR LOCAL FAST_FORWARD READ_ONLY FOR
    SELECT DISTINCT st.[ReportServer], st.[ReportDB] FROM #SurveyTable st

    IF NOT EXISTS (SELECT 1 FROM #User) BEGIN GOTO VirtualActors END

    OPEN c_RDB
    FETCH NEXT FROM c_RDB INTO @ReportServer, @ReportDB
    WHILE @@FETCH_STATUS = 0
    BEGIN
        -- Get all relay children
        SET @sqlCommand = 
       'INSERT INTO #Children ([ResponsibleUserID], [DepartmentID], [ChildID], [Type])
        SELECT u.[UserID], ls.[DepartmentID], ls.[UserID], ''H'' AS [Type]
        FROM $(ReportServer).$(ReportDB).dbo.[Result] ls
        JOIN #SurveyTable st ON ls.[SurveyID] = st.[SurveyID] AND st.[ActivityID] = ' + CAST(@LogSectionActivityID AS nvarchar(32)) + '
         AND ls.EntityStatusID = ' + CAST(@EntityStatus_Active AS VARCHAR(128)) +' AND ls.Deleted IS NULL AND ls.[EndDate] IS NULL
        JOIN $(ReportServer).$(ReportDB).dbo.[Answer] lsa ON ls.[ResultID] = lsa.[ResultID] AND lsa.[QuestionID] = ' + CAST(@QID_HolderID AS nvarchar(32)) + '
         AND lsa.[AlternativeID] = ' + CAST(@AltID_HolderID AS nvarchar(32)) + '
        JOIN #User u ON lsa.[Free] = CAST(u.[UserID] AS nvarchar(32)) AND u.[DepartmentID] = ls.[DepartmentID]'
        SET @sqlCommand = REPLACE(@sqlCommand, '$(ReportServer)', QUOTENAME(@ReportServer))
        SET @sqlCommand = REPLACE(@sqlCommand, '$(ReportDB)', QUOTENAME(@ReportDB))
        EXEC (@sqlCommand)

        -- Get all actor children
        SET @sqlCommand = 
       'INSERT INTO #Children ([ResponsibleUserID], [DepartmentID], [ChildID], [Type])
        SELECT u.[UserID], u.[DepartmentID], mea.[UserID], ''A'' AS [Type]
        FROM $(ReportServer).$(ReportDB).dbo.[Result] mea
        JOIN #SurveyTable st ON mea.[SurveyID] = st.[SurveyID] AND st.[ActivityID] = ' + CAST(@MeasureActivityID AS nvarchar(32)) + '
         AND mea.EntityStatusID = ' + CAST(@EntityStatus_Active AS VARCHAR(128)) +' AND mea.Deleted IS NULL AND mea.[EndDate] IS NULL
        JOIN $(ReportServer).$(ReportDB).dbo.[Answer] ma ON mea.[ResultID] = ma.[ResultID] AND ma.[QuestionID] = ' + CAST(@QID_Actor AS nvarchar(32)) + '
         AND ma.[AlternativeID] = ' + CAST(@AltID_Actor AS nvarchar(32)) + '
        JOIN #User u ON ma.[Free] = CAST(u.[UserID] AS nvarchar(32))
         AND EXISTS (SELECT 1 FROM $(ReportServer).$(ReportDB).dbo.[Result] PoA
                              JOIN $(ReportServer).$(ReportDB).dbo.[Result] ls ON PoA.[ParentResultID] = ls.[ResultID] AND PoA.[ResultID] = mea.[ParentResultID]
                               AND PoA.EntityStatusID = ' + CAST(@EntityStatus_Active AS VARCHAR(128)) +' AND PoA.Deleted IS NULL AND PoA.[EndDate] IS NULL AND ls.EntityStatusID = ' + CAST(@EntityStatus_Active AS VARCHAR(128)) +' AND ls.Deleted IS NULL AND ls.[EndDate] IS NULL)
        JOIN $(ReportServer).$(ReportDB).dbo.[Answer] ad ON ad.[ResultID] = mea.[ResultID] AND ad.[QuestionID] = ' + CAST(@QID_ActorDepartmentID AS nvarchar(32)) + '
         AND ad.[AlternativeID] = ' + CAST(@AltID_ActorDepartmentID AS nvarchar(32)) + ' AND ad.[Free] = CAST(u.[DepartmentID] AS nvarchar(32))'
        SET @sqlCommand = REPLACE(@sqlCommand, '$(ReportServer)', QUOTENAME(@ReportServer))
        SET @sqlCommand = REPLACE(@sqlCommand, '$(ReportDB)', QUOTENAME(@ReportDB))
        EXEC (@sqlCommand)
        
        FETCH NEXT FROM c_RDB INTO @ReportServer, @ReportDB
    END
    CLOSE c_RDB

    INSERT INTO #ChildrenCount ([ResponsibleUserID], [DepartmentID], [Type], [ChildrenCount])
    SELECT c.[ResponsibleUserID], c.[DepartmentID], c.[Type], COUNT(DISTINCT c.[ChildID]) AS [ChildrenCount]
    FROM #Children c
    GROUP BY c.[ResponsibleUserID], c.[DepartmentID], c.[Type]

    UPDATE u SET u.[RelayChildrenNumber] = cc.[ChildrenCount]
    FROM #User u JOIN #ChildrenCount cc ON u.[UserID] = cc.[ResponsibleUserID] AND u.[DepartmentID] = cc.[DepartmentID] AND cc.[Type] = 'H'

    UPDATE u SET u.[ActorChildrenNumber] = cc.[ChildrenCount]
    FROM #User u JOIN #ChildrenCount cc ON u.[UserID] = cc.[ResponsibleUserID] AND u.[DepartmentID] = cc.[DepartmentID] AND cc.[Type] = 'A'

VirtualActors:
    SET @QTxml = CONVERT(xml, @ActorInfoXML)
    INSERT INTO #ActorInfoQuestionTable ([Name], [QuestionID], [AlternativeID], [DataType])
    SELECT Tbl.Col.value('@Name',           'nvarchar(max)') [Name],
           Tbl.Col.value('@QuestionID',     'int') [QuestionID],
           Tbl.Col.value('@AlternativeID',  'int') [AlternativeID],
           Tbl.Col.value('@DataType',       'nvarchar(64)') [DataType]
    FROM @QTxml.nodes('//ActorInfo') Tbl(Col)
    WHERE Tbl.Col.value('@Name', 'nvarchar(max)') NOT IN ('ActorDepartmentID', 'UserID')

    OPEN c_RDB
    FETCH NEXT FROM c_RDB INTO @ReportServer, @ReportDB
    WHILE @@FETCH_STATUS = 0
    BEGIN
        SET @sqlCommand =
       'INSERT INTO #VirtualActor ([SSN], [OrgName], [LastName], [FirstName], [Position], [OfficePhone], [Mobile], [Email], [ChildID], [Created])
        SELECT LTRIM(RTRIM([SSN])) AS [SSN], LTRIM(RTRIM([DepartmentName])) AS [OrgName], LTRIM(RTRIM([LastName])) AS [LastName], LTRIM(RTRIM([FirstName])) AS [FirstName], 
               LTRIM(RTRIM([Position])) AS [Position], LTRIM(RTRIM([Office Phone])) AS [OfficePhone], LTRIM(RTRIM([Mobile Phone])) AS [Mobile], LTRIM(RTRIM([Email])) AS [Email], 
               [ChildID], [Created]
        FROM (SELECT ls.[UserID] AS [ChildID], ls.[Created], ma.[ResultID] AS [MeasureID], ma.[No] as [ActorNo], qt.[Name] AS [ColumnName],
                     (CASE WHEN qt.[DataType] = ''text'' THEN ai.[Free] WHEN qt.[DataType] = ''date'' THEN convert(nvarchar(max), ai.[DateValue], 120) ELSE convert(nvarchar(max), ai.[Value]) END) AS [Value]
              FROM $(ReportServer).$(ReportDB).dbo.[Result] ls
              JOIN #SurveyTable st ON ls.[SurveyID] = st.[SurveyID] AND st.[ActivityID] = ' + CAST(@LogSectionActivityID AS nvarchar(32)) + '
               AND ls.EntityStatusID = ' + CAST(@EntityStatus_Active AS VARCHAR(128)) +' AND ls.Deleted IS NULL AND ls.[EndDate] IS NULL
              JOIN $(ReportServer).$(ReportDB).dbo.[Answer] lsa ON ls.[ResultID] = lsa.[ResultID] AND lsa.[QuestionID] = ' + CAST(@QID_HolderID AS nvarchar(32)) + '
               AND lsa.[AlternativeID] = ' + CAST(@AltID_HolderID AS nvarchar(32)) + ' AND lsa.[Free] = ' + CAST(@UserID AS nvarchar(32)) + '
              JOIN $(ReportServer).$(ReportDB).dbo.[Result] PoA ON PoA.[ParentResultID] = ls.[ResultID] AND PoA.EntityStatusID = ' + CAST(@EntityStatus_Active AS VARCHAR(128)) +' AND PoA.Deleted IS NULL AND PoA.[EndDate] IS NULL
              JOIN $(ReportServer).$(ReportDB).dbo.[Result] mea ON mea.[ParentResultID] = PoA.[ResultID] AND mea.EntityStatusID = ' + CAST(@EntityStatus_Active AS VARCHAR(128)) +' AND mea.Deleted IS NULL AND mea.[EndDate] IS NULL
              JOIN $(ReportServer).$(ReportDB).dbo.[Answer] ma ON ma.[ResultID] = mea.[ResultID] AND ma.[QuestionID] = ' + CAST(@QID_Actor AS nvarchar(32)) + '
               AND ma.[AlternativeID] = ' + CAST(@AltID_Actor AS nvarchar(32)) + ' AND ma.[Free] = ''0''
              JOIN $(ReportServer).$(ReportDB).dbo.[Answer] ai ON ai.[ResultID] = ma.[ResultID] AND ai.[QuestionID] = ma.[QuestionID] AND ai.[No] = ma.[No]
              JOIN #ActorInfoQuestionTable qt ON ai.[QuestionID] = qt.[QuestionID] AND (ai.[AlternativeID] = qt.[AlternativeID] OR qt.[AlternativeID] = 0)
            ) v
        PIVOT (MAX([v].[Value]) FOR [v].[ColumnName] IN ([DepartmentName], [LastName], [FirstName], [Position], [Office Phone], [Mobile Phone], [Email], [SSN])) p
        '
        SET @sqlCommand = REPLACE(@sqlCommand, '$(ReportServer)', QUOTENAME(@ReportServer))
        SET @sqlCommand = REPLACE(@sqlCommand, '$(ReportDB)', QUOTENAME(@ReportDB))
        EXEC (@sqlCommand)

        FETCH NEXT FROM c_RDB INTO @ReportServer, @ReportDB
    END
    CLOSE c_RDB
    DEALLOCATE c_RDB

    DECLARE @SSN nvarchar(32), @ActorChildrenCount int = 0

    DECLARE c_VirtualActor CURSOR LOCAL FAST_FORWARD READ_ONLY FOR
    SELECT va.[SSN], COUNT(DISTINCT va.[ChildID]) AS [ActorChildrenCount]
    FROM #VirtualActor va
    GROUP BY va.[SSN]

    OPEN c_VirtualActor
    FETCH NEXT FROM c_VirtualActor INTO @SSN, @ActorChildrenCount
    WHILE @@FETCH_STATUS = 0
    BEGIN
        INSERT INTO #User ([SSN], [DepartmentID], [OrgName], [DepartmentBorderColor], [UserID], [LastName], [FirstName],
                           [Mobile], [Email], [ActorChildrenNumber], [RelayChildrenNumber], Position)
        SELECT TOP 1 @SSN, 0 AS [DepartmentID], va.[OrgName], NULL AS [DepartmentBorderColor], 0 AS [UserID], va.[LastName], va.[FirstName],
                     va.[Mobile], va.[Email], @ActorChildrenCount AS [ActorChildrenNumber], 0 AS [RelayChildrenNumber], va.Position
        FROM #VirtualActor va
        WHERE va.[SSN] = @SSN
        ORDER BY va.[Created] DESC
        
        FETCH NEXT FROM c_VirtualActor INTO @SSN, @ActorChildrenCount
    END
    CLOSE c_VirtualActor
    DEALLOCATE c_VirtualActor

ReturnValues:

    SELECT u.[DepartmentID], u.[DepartmentName], u.[OrgName], u.[DepartmentBorderColor], u.[UserID], u.[LastName], u.[FirstName],
           u.[Position], u.[OfficePhone], u.[Mobile], u.[Email], u.[ActorChildrenNumber], u.[RelayChildrenNumber]
    FROM #User u

    DROP TABLE #DepartmentTable
    DROP TABLE #SurveyTable
    DROP TABLE #User
    DROP TABLE #Children
    DROP TABLE #ChildrenCount
    DROP TABLE #ActorInfoQuestionTable
    DROP TABLE #VirtualActor
END

GO
