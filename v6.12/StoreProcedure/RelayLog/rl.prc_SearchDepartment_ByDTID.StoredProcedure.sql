/****** Object:  StoredProcedure [rl].[prc_SearchDepartment_ByDTID]    Script Date: 5/11/2017 10:41:02 AM ******/
DROP PROCEDURE [rl].[prc_SearchDepartment_ByDTID]
GO
/****** Object:  StoredProcedure [rl].[prc_SearchDepartment_ByDTID]    Script Date: 5/11/2017 10:41:02 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [rl].[prc_SearchDepartment_ByDTID]
(
	@ParentID				INT,
    @ListDepartmentTypeID   NVARCHAR(MAX),
	@SearchString			NVARCHAR(MAX),
	@ListDeniedDepartmentID NVARCHAR(MAX) = ''
)
AS
BEGIN
	SET NOCOUNT ON  
    DECLARE @Err VARCHAR(max)  

	SELECT DISTINCT Value AS DepartmentTypeID INTO #DepartmentType FROM [dbo].[funcListToTableInt](@ListDepartmentTypeID, ',')
	SELECT DISTINCT Value AS DepartmentID INTO #ListDeniedDepartment FROM [dbo].[funcListToTableInt](@ListDeniedDepartmentID, ',')

    SELECT DISTINCT DEPT.*                  
	FROM [org].[Department] AS DEPT
        INNER JOIN org.DT_D AS DTD ON DEPT.DepartmentID = DTD.DepartmentID AND DEPT.Name LIKE '%' + @SearchString + '%'
		INNER JOIN #DepartmentType DT ON DT.DepartmentTypeID = DTD.DepartmentTypeID
        INNER JOIN org.H_D AS HD ON HD.DepartmentID = DEPT.DepartmentID  AND HD.Deleted = 0 AND HD.[Path] LIKE '%\'+ CONVERT(VARCHAR(10),@ParentID) +'\%' 	         
   WHERE DEPT.DepartmentID NOT IN (SELECT DepartmentID FROM #ListDeniedDepartment)
	Set @Err = @@Error

	RETURN @Err
END


GO
