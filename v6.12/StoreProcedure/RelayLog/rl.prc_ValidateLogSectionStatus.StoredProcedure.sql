/****** Object:  StoredProcedure [rl].[prc_ValidateLogSectionStatus]    Script Date: 5/11/2017 10:41:02 AM ******/
DROP PROCEDURE [rl].[prc_ValidateLogSectionStatus]
GO
/****** Object:  StoredProcedure [rl].[prc_ValidateLogSectionStatus]    Script Date: 5/11/2017 10:41:02 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/*
	2017-04-24	 - Sarah - Using EntityStatusID column
*/

CREATE PROCEDURE [rl].[prc_ValidateLogSectionStatus]
(
     @SiteID   INT = 1
)
AS
BEGIN
BEGIN TRY
    BEGIN TRANSACTION;
	SET NOCOUNT ON
    
    DECLARE @LogSectionActivityID INT, @QID_ConsentExpires INT, @AltID_ConsentExpires INT,
            @ChildXML nvarchar(max),@QTxml XML, @LogSection_ActiveDuration INT, @PROP_LogSectionActiveDuration INT,
            @StatusTypeConsentExpires INT, @SqlCommand NVARCHAR(MAX), @StatusTypeInactiveOverTime INT, @StatusActive INT
    DECLARE @LogSectionServerReport VARCHAR(MAX), @LogSectionDatabaseReport VARCHAR(MAX) 
	DECLARE @EntityStatus_Active INT
	SELECT @EntityStatus_Active = E.EntityStatusID FROM EntityStatus E WHERE E.CodeName = 'Active'    
       
    /* The structure of site parameter LISTCOLUMN_ChildInfo should be
    <ChildInfo Name="ConsentExpires" QuestionID="3" AlternativeID="65" DataType="int" />    */

    SELECT @ChildXML						= [Value] FROM app.[SiteParameter] WHERE [Key] = 'LISTCOLUMN_ChildInfo'         AND [SiteID] = @SiteID    
    SELECT @LogSectionActivityID			= [Value] FROM app.[SiteParameter] WHERE [Key] = 'ACTIVITY_ID_LOG_SECTION'      AND [SiteID] = @SiteID
    SELECT @StatusTypeConsentExpires		= [Value] FROM app.[SiteParameter] WHERE [Key] = 'STATUSTYPE_ConsentExpires'    AND [SiteID] = @SiteID
    SELECT @StatusTypeInactiveOverTime		= [Value] FROM app.[SiteParameter] WHERE [Key] = 'STATUSTYPE_InactiveOverTime'  AND [SiteID] = @SiteID    
    SELECT @LogSection_ActiveDuration		= [Value] FROM app.[SiteParameter] WHERE [Key] = 'LogSection_ActiveDuration'    AND [SiteID] = @SiteID  
    SELECT @PROP_LogSectionActiveDuration	= [Value] FROM app.[SiteParameter] WHERE [Key] = 'PROP_LogSectionActiveDuration'AND [SiteID] = @SiteID  
  
    SET @QTxml = CONVERT(xml, @ChildXML)
    SELECT @QID_ConsentExpires = Tbl.Col.value('@QuestionID', 'int'), @AltID_ConsentExpires = Tbl.Col.value('@AlternativeID', 'int')
    FROM @QTxml.nodes('//ChildInfo') Tbl(Col)     
    WHERE Tbl.Col.value('@Name', 'nvarchar(max)') = 'ConsentTransferExpires'

	SELECT @StatusActive = StatusTypeID FROM at.StatusType WHERE CodeName = 'Active'

	CREATE TABLE #StatusTable (StatusID INT)
    SELECT C.CustomerID, Value  INTO #CustomerValidPeriod     
    FROM org.Customer C
	LEFT JOIN prop.PropValue PV ON c.CustomerID = PV.ItemID AND PV.PropertyID = @PROP_LogSectionActiveDuration    

	-- Update ValidPeriod from SiteParameter PROP_LogSectionActiveDuration for Customer not define ValidPeriod			
	UPDATE #CustomerValidPeriod SET Value = @LogSection_ActiveDuration WHERE Value IS NULL
	
    DECLARE c_SurveyChild CURSOR LOCAL FAST_FORWARD READ_ONLY FOR
    SELECT DISTINCT s.ReportServer,s.ReportDB  FROM at.[Survey] s WHERE s.ActivityID = @LogSectionActivityID 
     
    OPEN  c_SurveyChild
    FETCH NEXT FROM c_SurveyChild INTO @LogSectionServerReport, @LogSectionDatabaseReport
    WHILE @@FETCH_STATUS = 0
    BEGIN  	

	SET @SqlCommand ='	
	UPDATE R
    SET R.StatusTypeID = '+ CONVERT(VARCHAR(10),@StatusTypeConsentExpires) + '
	FROM [' + @LogSectionServerReport + '].['+ @LogSectionDatabaseReport + '].dbo.[Result] R
	INNER JOIN [' + @LogSectionServerReport + '].['+ @LogSectionDatabaseReport + '].dbo.[Answer] A ON A.[ResultID] = R.[ResultID] 
		AND R.EntityStatusID = ' + CAST(@EntityStatus_Active AS VARCHAR(128)) +' AND R.Deleted IS NULL
		AND R.EndDate IS NULL 
		AND A.[QuestionID] = ' + CONVERT(VARCHAR(10),@QID_ConsentExpires) +' AND A.[AlternativeID] = '+ CONVERT(VARCHAR(10),@AltID_ConsentExpires)+ '
		AND A.DateValue <= GETDATE()
	INNER JOIN at.[Survey] s ON s.SurveyID = R.SurveyID AND s.ActivityID = ' +CONVERT(VARCHAR(10), @LogSectionActivityID) +''	
	EXEC(@SqlCommand)

	INSERT INTO #StatusTable (StatusID)	SELECT @StatusTypeConsentExpires    
	 

	-- UPDATE inactive over time status for customer define inactive over time or not		
	SET @SqlCommand ='
	UPDATE R
    SET R.StatusTypeID = '+ CONVERT(VARCHAR(10),@StatusTypeInactiveOverTime) + '  
	FROM [' + @LogSectionServerReport + '].['+ @LogSectionDatabaseReport + '].dbo.[Result] R
    INNER JOIN at.[Survey] s ON s.SurveyID = R.SurveyID AND s.ActivityID = ' +CONVERT(VARCHAR(10), @LogSectionActivityID) +'
			AND R.EntityStatusID = ' + CAST(@EntityStatus_Active AS VARCHAR(128)) +' AND R.Deleted IS NULL 
			AND R.EndDate IS NULL
			AND R.StatusTypeID NOT IN (SELECT StatusID FROM #StatusTable)
			AND R.StatusTypeID = ' + CONVERT(NVARCHAR(64),@StatusActive) +'
    INNER JOIN #CustomerValidPeriod CVP ON R.CustomerID = CVP.CustomerID AND CVP.Value < DATEDIFF(D, R.LastUpdated, GETDATE())'   
	EXEC(@SqlCommand)
	
       FETCH NEXT FROM c_SurveyChild INTO @LogSectionServerReport, @LogSectionDatabaseReport
   END
    CLOSE c_SurveyChild
    DEALLOCATE c_SurveyChild   
	COMMIT TRANSACTION;
  END TRY
  BEGIN CATCH
    IF @@TRANCOUNT > 0
    ROLLBACK TRANSACTION;

	DECLARE @ErrorNumber INT = ERROR_NUMBER();
    DECLARE @ErrorLine INT = ERROR_LINE();
    DECLARE @ErrorMessage NVARCHAR(4000) = ERROR_MESSAGE();
    DECLARE @ErrorSeverity INT = ERROR_SEVERITY();
    DECLARE @ErrorState INT = ERROR_STATE();

    PRINT 'Actual error number: ' + CAST(@ErrorNumber AS VARCHAR(10));
    PRINT 'Actual line number: ' + CAST(@ErrorLine AS VARCHAR(10));

    RAISERROR(@ErrorMessage, @ErrorSeverity, @ErrorState);
  END CATCH
END


GO
