/****** Object:  StoredProcedure [rl].[prc_getAllActorByLogSectionID]    Script Date: 5/11/2017 10:41:02 AM ******/
DROP PROCEDURE [rl].[prc_getAllActorByLogSectionID]
GO
/****** Object:  StoredProcedure [rl].[prc_getAllActorByLogSectionID]    Script Date: 5/11/2017 10:41:02 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/*
	2016-12-15 Bug #63917 - [Actor menu] -  the latest actor ,which is added by RH, replaces all the old ones; missing position
	2017-04-24	-	Sarah - Using EntityStatusID column
*/
CREATE PROCEDURE [rl].[prc_getAllActorByLogSectionID]
(  
    @SiteID                   INT = 1,
    @LogSectionID             INT = NULL   
)
AS
BEGIN
	SET NOCOUNT ON
	DECLARE @Err INT
    
    DECLARE @LogSectionActivityID INT, @MeasureActivityID INT,@PoAActivityID INT, @PropPosition INT, @PropOfficePhone INT,     
            @QID_Actor INT, @AltID_Actor INT, @QID_HolderID INT, @AltID_HolderID INT, @UT_Holder INT, @UT_Actor INT,
            @ActorXML VARCHAR(max), @QTxml XML, @HolderXML VARCHAR(max),
            @RServerReport VARCHAR(Max), @RDatabaseReport VARCHAR(Max), @SqlCommand VARCHAR(MAX)
	DECLARE @EntityStatus_Active INT
	SELECT @EntityStatus_Active = E.EntityStatusID FROM EntityStatus E WHERE E.CodeName = 'Active'
    CREATE TABLE #FinalResult (UserID INT,DepartmentName VARCHAR(MAX), LastName VARCHAR(MAX), FirstName VARCHAR(MAX), [Position] VARCHAR(MAX), OfficePhone VARCHAR(MAX), Email VARCHAR(MAX), Mobile VARCHAR(MAX), SSN VARCHAR(MAX), IsRelay BIT)  
	CREATE TABLE #ListUnrealActor (ResultID BIGINT, UserID INT,DepartmentName VARCHAR(MAX), LastName VARCHAR(MAX), FirstName VARCHAR(MAX), [Position] VARCHAR(MAX), OfficePhone VARCHAR(MAX), Email VARCHAR(MAX), Mobile VARCHAR(MAX), SSN VARCHAR(MAX), IsRelay BIT)  
	CREATE TABLE [#User] ([UserID] INT,DepartmentName VARCHAR(MAX), [LastName] NVARCHAR(MAX), [FirstName] NVARCHAR(MAX),[Position] VARCHAR(MAX),[OfficePhone] VARCHAR(MAX), [Email] VARCHAR(MAX), [Mobile] VARCHAR(MAX))

	/*
     -- The structure of site parameter LISTCOLUMN_Actor should be
    SET @ActorXML = '<Actor QuestionID="10" AlternativeID="43" />'

    -- The structure of site parameter LISTCOLUMN_Holder should be
    SET @HolderXML = N'<Holder Name="HolderID" QuestionID="5" AlternativeID="5" DataType="text" />
    */
    SELECT @ActorXML             = [Value] FROM app.[SiteParameter] WHERE [Key] = 'LISTCOLUMN_Actor'        AND [SiteID] = @SiteID        
    SELECT @HolderXML            = [Value] FROM app.[SiteParameter] WHERE [Key] = 'LISTCOLUMN_Holder'       AND [SiteID] = @SiteID
    SELECT @UT_Holder            = [Value] FROM app.[SiteParameter] WHERE [Key] = 'UT_RELAY_HOLDER'         AND [SiteID] = @SiteID
 --   SELECT @UT_Actor             = [Value] FROM app.[SiteParameter] WHERE [Key] = 'UT_ACTOR'                AND [SiteID] = @SiteID
    SELECT @LogSectionActivityID = [Value] FROM app.[SiteParameter] WHERE [Key] = 'ACTIVITY_ID_LOG_SECTION' AND [SiteID] = @SiteID
    SELECT @MeasureActivityID    = [Value] FROM app.[SiteParameter] WHERE [Key] = 'ACTIVITY_ID_MEASURE'     AND [SiteID] = @SiteID 
	SELECT @PoAActivityID		 = [Value] FROM app.[SiteParameter] WHERE [Key] = 'ACTIVITY_ID_POINT_OF_ATTENTION' AND [SiteID] = @SiteID 
    SELECT @PropPosition         = [Value] FROM app.[SiteParameter] WHERE [Key] = 'PROP_Position'           AND [SiteID] = @SiteID
    SELECT @PropOfficePhone      = [Value] FROM app.[SiteParameter] WHERE [Key] = 'PROP_OfficePhone'        AND [SiteID] = @SiteID   

    SET @QTxml = CONVERT(xml, @ActorXML)
    SELECT @QID_Actor = Tbl.Col.value('@QuestionID', 'int'), @AltID_Actor = Tbl.Col.value('@AlternativeID', 'int')
    FROM @QTxml.nodes('//Actor') Tbl(Col)     

    SET @QTxml = CONVERT(xml, @HolderXML)
    SELECT @QID_HolderID = Tbl.Col.value('@QuestionID', 'int'), @AltID_HolderID = Tbl.Col.value('@AlternativeID', 'int')
    FROM @QTxml.nodes('//Holder') Tbl(Col)
    WHERE Tbl.Col.value('@Name', 'nvarchar(max)') = 'HolderID'  
	
	-- Step 1: Get all valid actor
    INSERT INTO #User( UserID ,[DepartmentName], LastName ,FirstName ,[Position],[OfficePhone], Email ,Mobile) 
    SELECT U.[UserID]  
		  ,D.Name				AS [DepartmentName]       
          ,U.[LastName]
          ,U.[FirstName]
		  ,PVPosition.Value		AS [Position]
		  ,PVOfficePhone.Value  AS [OfficePhone]
          ,U.[Email]
          ,U.[Mobile]             
   FROM org.[User] U  
   INNER JOIN org.UT_U UTU					ON UTU.UserID = U.UserID  AND U.EntityStatusID = @EntityStatus_Active AND U.Deleted IS NULL -- AND UTU.UserTypeID = @UT_Actor
   INNER JOIN org.Department D				ON U.DepartmentID = D.DepartmentID 
   LEFT JOIN  prop.PropValue PVPosition		ON PVPosition.ItemID = u.UserID AND PVPosition.PropertyID = @PropPosition
   LEFT JOIN  prop.PropValue PVOfficePhone  ON PVOfficePhone.ItemID = u.UserID AND PVOfficePhone.PropertyID = @PropOfficePhone
   
   /*
   INSERT INTO #User( UserID ,[DepartmentName], LastName ,FirstName ,[Position], [OfficePhone], Email ,Mobile)  
   SELECT U.[UserID]        
         ,D.Name			  AS [DepartmentName]
		 ,U.[LastName]
         ,U.[FirstName]
		 ,PVPosition.Value	  AS [Position]
		 ,PVOfficePhone.Value AS [OfficePhone]
         ,U.[Email]
         ,U.[Mobile]           
    FROM [org].[U_D] UD
    INNER JOIN org.[User] U     ON UD.UserID = U.UserID
	INNER JOIN org.Department D ON d.DepartmentID = UD.DepartmentID
    INNER JOIN org.U_D_UT UDUT  ON UDUT.U_DID = UD.U_DID AND UDUT.UserTypeID = @UT_Actor AND U.[Status] >=0   
	LEFT JOIN  prop.PropValue PVPosition	ON PVPosition.ItemID = u.UserID AND PVPosition.PropertyID = @PropPosition
    LEFT JOIN  prop.PropValue PVOfficePhone ON PVOfficePhone.ItemID = u.UserID AND PVOfficePhone.PropertyID = @PropOfficePhone 
	*/

	-- Creating index for #User(UserID)
	CREATE CLUSTERED INDEX IDX_C_User_UserID ON #User(UserID)

    SELECT DISTINCT s.ReportServer,  s.ReportDB, s.SurveyID, s.ActivityID INTO #ConnectionTable    
    FROM at.[Survey] s WHERE s.[Status] = 2 AND (ActivityID = @PoAActivityID OR ActivityID = @MeasureActivityID)

	DECLARE c_Actor CURSOR LOCAL FAST_FORWARD READ_ONLY FOR
    SELECT DISTINCT CT.ReportServer, CT.ReportDB  FROM #ConnectionTable CT  
              
    OPEN c_Actor
    FETCH NEXT FROM c_Actor INTO @RServerReport, @RDatabaseReport
    WHILE @@FETCH_STATUS = 0
    BEGIN      
             
    SET @SqlCommand = '   
     SELECT DISTINCT
           U.[UserID]
	      ,U.[DepartmentName]
          ,U.[LastName] 
          ,U.[FirstName]
          ,U.[Position]
          ,U.[OfficePhone]
          ,U.[Email]
          ,U.[Mobile]    
          ,0 [IsRelay]         		
    FROM [#User] U
    INNER JOIN
    (
    SELECT DISTINCT ma.Free
    FROM ['+@RServerReport+'].['+@RDatabaseReport+'].dbo.Result PoA 	
	INNER JOIN #ConnectionTable ctPoA ON ctPoA.SurveyID = PoA.SurveyID AND ctPOA.ActivityID = '+CONVERT(VARCHAR(10),@PoAActivityID) +'
			AND ctPoA.ReportServer = '''+CONVERT(VARCHAR(255),@RServerReport) +''' AND ctPoA.ReportDB = '''+CONVERT(VARCHAR(255),@RDatabaseReport) +'''
    INNER JOIN ['+@RServerReport+'].['+@RDatabaseReport+'].dbo.Result mea ON mea.ParentResultID = PoA.ResultID  AND PoA.EntityStatusID = '+ CAST(@EntityStatus_Active AS VARCHAR(128))+' AND PoA.Deleted IS NULL AND mea.EntityStatusID = '+ CAST(@EntityStatus_Active AS VARCHAR(128))+' AND mea.Deleted IS NULL 
				AND PoA.ParentResultID = ' + CONVERT(VARCHAR(10),@LogSectionID) +'
	INNER JOIN #ConnectionTable ctMea ON ctMea.SurveyID = mea.SurveyID AND ctMea.ActivityID = '+CONVERT(VARCHAR(10),@MeasureActivityID) +'
			AND ctMea.ReportServer = '''+CONVERT(VARCHAR(255),@RServerReport) +''' AND ctMea.ReportDB = '''+CONVERT(VARCHAR(255),@RDatabaseReport) +'''
    INNER JOIN ['+@RServerReport+'].['+@RDatabaseReport+'].dbo.Answer ma  ON mea.ResultID = ma.ResultID 
				AND ma.QuestionID = ' + CONVERT(VARCHAR(10),@QID_Actor) + ' AND ma.AlternativeID = '+CONVERT(VARCHAR(10),@AltID_Actor) +'	
    ) AS [ListActorOfLogSection] ON U.UserID = [ListActorOfLogSection].Free'

	INSERT INTO #FinalResult( UserID ,DepartmentName,LastName ,FirstName ,Position ,OfficePhone ,Email ,Mobile ,IsRelay)  
    EXEC(@SqlCommand)   
	
	SET @SqlCommand = '  
    UPDATE  FR
    SET IsRelay = 1
    FROM #FinalResult AS FR
    WHERE EXISTS (SELECT 1
                  FROM ['+@RServerReport+'].['+@RDatabaseReport+'].dbo.[Result] r				    
                  INNER JOIN ['+@RServerReport+'].['+@RDatabaseReport+'].dbo.[Answer] a ON r.[ResultID] = a.[ResultID] AND r.EntityStatusID = '+ CAST(@EntityStatus_Active AS VARCHAR(128))+' AND r.Deleted IS NULL AND r.ResultID = ' + CONVERT(VARCHAR(10),@LogSectionID) +'
                   AND a.QuestionID = ' + CONVERT(VARCHAR(10),@QID_HolderID) +' AND a.AlternativeID = ' + CONVERT(VARCHAR(10),@AltID_HolderID) +' AND a.Free = FR.UserID)'
	EXEC(@SqlCommand)		   
   
	FETCH NEXT FROM c_Actor INTO @RServerReport, @RDatabaseReport
    END
    CLOSE c_Actor
    DEALLOCATE c_Actor

     -- Get Actor who not exists in HD
    DECLARE @TableString nvarchar(max), @varChoose INT  
    DECLARE @QID_UserID INT, @AltID_UserID INT, @LogSectionServer NVARCHAR(MAX), @LogSectionDatabase NVARCHAR(MAX)
	DECLARE @UnrealActorServerReport VARCHAR(MAX), @UnrealActorDatabaseReport VARCHAR(MAX)
    SELECT @TableString = [Value] FROM app.[SiteParameter] WHERE [Key] = 'LISTCOLUMN_ActorInfo' AND [SiteID] = @SiteID
    
    SET @QTxml = CONVERT(xml, @TableString) 
    SELECT Tbl.Col.value('@Name',           'nvarchar(255)') [Name],
           Tbl.Col.value('@QuestionID',     'int') [QuestionID],
           Tbl.Col.value('@AlternativeID',  'int') [AlternativeID],
           Tbl.Col.value('@DataType',       'nvarchar(64)') [DataType]
        INTO #QuestionTable
    FROM @QTxml.nodes('//ActorInfo') Tbl(Col)     
		
    SET  @varChoose = (SELECT CASE WHEN [DataType] = 'text' THEN 0  WHEN [DataType] = 'date' THEN 1  ELSE 2 END FROM #QuestionTable WHERE Name ='UserID')
       
    SELECT @QID_UserID = QuestionID, @AltID_UserID = AlternativeID FROM #QuestionTable WHERE Name ='UserID'   
    SELECT DISTINCT s.ReportServer,  s.ReportDB, s.SurveyID, s.ActivityID INTO #ConnectionTableUnreal    
    FROM at.[Survey] s WHERE s.[Status] = 2 AND ActivityID = @MeasureActivityID
	
	-- Creating index for #User(UserID)
	CREATE CLUSTERED INDEX IDX_C_QuestionTable_NameQIDAltID ON #QuestionTable([Name],[QuestionID],[AlternativeID])
	
	DECLARE c_ActorUnreal CURSOR LOCAL FAST_FORWARD READ_ONLY FOR
    SELECT DISTINCT CT.ReportServer, CT.ReportDB  FROM #ConnectionTableUnreal CT  
              
    OPEN c_ActorUnreal
    FETCH NEXT FROM c_ActorUnreal INTO @UnrealActorServerReport, @UnrealActorDatabaseReport
    WHILE @@FETCH_STATUS = 0
    BEGIN      
    
	SET @SqlCommand = '
    SELECT  DISTINCT ResultID, [UserID], RTRIM(LTRIM([DepartmentName])) AS [DepartmentName], RTRIM(LTRIM([LastName])) AS [LastName],RTRIM(LTRIM([FirstName])) AS [FirstName], RTRIM(LTRIM([Position])) AS [Position], 
           RTRIM(LTRIM([Office Phone])) AS [OfficePhone], RTRIM(LTRIM([Email])) AS [Email], RTRIM(LTRIM([Mobile Phone])) AS [Mobile], SSN, 0 AS IsRelay
    FROM (
    SELECT  ma.ResultID, ac.No, qt.[Name], 
            (CASE WHEN qt.[DataType] = ''text'' THEN ma.[Free] WHEN qt.[DataType] = ''date'' THEN convert(nvarchar(max), ma.[DateValue],120) ELSE convert(nvarchar(max), ma.[Value]) END) [Value]              
    FROM ['+@UnrealActorServerReport+'].['+@UnrealActorDatabaseReport+'].[dbo].[Result] ls 
    
	INNER JOIN ['+@UnrealActorServerReport+'].['+@UnrealActorDatabaseReport+'].[dbo].[Result] PoA ON PoA.ParentResultID = ls.ResultID AND ls.ResultID = ' + CONVERT(VARCHAR(10),@LogSectionID) +'     
	
	INNER JOIN ['+@UnrealActorServerReport+'].['+@UnrealActorDatabaseReport+'].[dbo].[Result] mea ON mea.[ParentResultID] = PoA.[ResultID] AND PoA.EntityStatusID = '+ CAST(@EntityStatus_Active AS VARCHAR(128))+' AND PoA.Deleted IS NULL AND mea.EntityStatusID = '+ CAST(@EntityStatus_Active AS VARCHAR(128))+' AND mea.Deleted IS NULL  
    INNER JOIN #ConnectionTable ctMea ON ctMea.SurveyID = mea.SurveyID AND ctMea.ActivityID = '+CONVERT(VARCHAR(10),@MeasureActivityID) +'
			AND ctMea.ReportServer = '''+CONVERT(VARCHAR(255),@UnrealActorServerReport) +''' AND ctMea.ReportDB = '''+CONVERT(VARCHAR(255),@UnrealActorDatabaseReport) +''' 	

	INNER JOIN ['+@UnrealActorServerReport+'].['+@UnrealActorDatabaseReport+'].[dbo].[Answer] ma ON ma.[ResultID] = mea.[ResultID]  AND ma.[QuestionID] = ' + CONVERT(VARCHAR(10),@QID_Actor) +' 
    	
	INNER JOIN ['+@UnrealActorServerReport+'].['+@UnrealActorDatabaseReport+'].[dbo].[Answer] ac ON ma.ResultID = ac.ResultID AND ma.No = ac.NO    
            AND ac.QuestionID = ' + CONVERT(VARCHAR(10),@QID_UserID) +' AND ac.AlternativeID = ' + CONVERT(VARCHAR(10),@AltID_UserID) +'  
            AND (('+CONVERT(VARCHAR(10),@varChoose)+' = 0 AND ac.Free = ''0'') OR ('+CONVERT(VARCHAR(10),@varChoose)+' = 2 AND ac.Value = 0)) 			        
    INNER JOIN [#QuestionTable] qt ON ma.[QuestionID] = qt.[QuestionID] AND (ma.[AlternativeID] = qt.[AlternativeID] OR qt.[AlternativeID] = 0)
	) v
    PIVOT (MAX([v].[Value]) FOR [v].[Name] IN ( [UserID],[DepartmentName], [LastName], [FirstName], [Position], [Office Phone], [Mobile Phone], [Email],SSN)) p
	'                                     
	INSERT INTO #ListUnrealActor(ResultID, UserID ,DepartmentName,LastName ,FirstName ,Position ,OfficePhone ,Email ,Mobile ,SSN, IsRelay)  
    EXEC(@SqlCommand)   
	 
	FETCH NEXT FROM c_ActorUnreal INTO @RServerReport, @RDatabaseReport
    END

    CLOSE c_ActorUnreal
    DEALLOCATE c_ActorUnreal
		
	SELECT * INTO #ListDistinctSSN 
	FROM(	
			SELECT *,ROW_NUMBER() OVER (PARTITION BY SSN ORDER BY ResultID DESC) AS rn
			FROM #ListUnrealActor  			
		)AS A WHERE rn = 1
										
	INSERT INTO #FinalResult( UserID ,DepartmentName,LastName ,FirstName ,Position ,OfficePhone ,Email ,Mobile ,SSN, IsRelay)  
    SELECT UserID ,DepartmentName,LastName ,FirstName ,Position ,OfficePhone ,Email ,Mobile ,SSN, IsRelay FROM #ListDistinctSSN

	SELECT * FROM #FinalResult

   RETURN @Err
END


GO
