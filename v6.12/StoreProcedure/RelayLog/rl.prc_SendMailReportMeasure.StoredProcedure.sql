/****** Object:  StoredProcedure [rl].[prc_SendMailReportMeasure]    Script Date: 5/11/2017 10:41:02 AM ******/
DROP PROCEDURE [rl].[prc_SendMailReportMeasure]
GO
/****** Object:  StoredProcedure [rl].[prc_SendMailReportMeasure]    Script Date: 5/11/2017 10:41:02 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/* 
	2016-11-18 Ray   - Fix bug send mail show incorrect Actor names
	2016-12-05 Sarah - Send report mail - Check active customer	
					 - Send Expiring measure to actors: edit number of days to 7
	2016-12-14 Sarah - Fix bug #63880 Email for measures that have been already expired should be prioritized firstly
*/
CREATE PROCEDURE [rl].[prc_SendMailReportMeasure]
(   
	@SiteID					INT = 1,
	@NumberOfDaysToExpire	INT = 7
) AS
BEGIN
    SET NOCOUNT ON
  
    DECLARE @PoAActivityID INT, @MeasureActivityID INT,@MeasureType INT, @SqlCommand NVARCHAR(MAX), @MeasureServerReport  NVARCHAR(MAX), @MeasureDatabaseReport  NVARCHAR(MAX)
    CREATE TABLE #ListMeasure (ResultID BIGINT, Initiated DATE, Finished DATE, DayToDeadline INT, [MeasureType] NVARCHAR(MAX), [Description] NVARCHAR(MAX), DepartmentID  INT,[DepartmentName] NVARCHAR(MAX),
           [UserID] INT,[FirstName] NVARCHAR(MAX), [LastName] NVARCHAR(MAX),[Email] NVARCHAR(MAX), [SSN]  NVARCHAR(MAX), [LanguageID] INT, [ChildID] INT, [RHUserID] INT)
    
    DECLARE @UT_Holder INT, @UT_Actor INT, @ActorXML nvarchar(max), @QTxml XML, @HolderXML NVARCHAR(MAX),
    @QID_Actor INT, @AltID_Actor INT, @QID_HolderID INT, @AltID_HolderID INT,
    @MeasureXML nvarchar(max)

    SELECT @PoAActivityID        = [Value] FROM app.[SiteParameter] WHERE [Key] = 'ACTIVITY_ID_POINT_OF_ATTENTION'  AND [SiteID] = @SiteID
    SELECT @MeasureActivityID    = [Value] FROM app.[SiteParameter] WHERE [Key] = 'ACTIVITY_ID_MEASURE'             AND [SiteID] = @SiteID  
    SELECT @MeasureType          = [Value] FROM app.[SiteParameter] WHERE [Key] = 'QID_MeasureType'                 AND [SiteID] = @SiteID
    SELECT @MeasureXML           = [Value] FROM app.[SiteParameter] WHERE [Key] = 'LISTCOLUMN_MeasureInfo'          AND [SiteID] = @SiteID        
    SELECT @ActorXML             = [Value] FROM app.[SiteParameter] WHERE [Key] = 'LISTCOLUMN_ActorInfo'            AND [SiteID] = @SiteID        
    SELECT @HolderXML            = [Value] FROM app.[SiteParameter] WHERE [Key] = 'LISTCOLUMN_Holder'               AND [SiteID] = @SiteID        
    
    /* The structure of site parameter LISTCOLUMN_MeasureInfo should be
        <MeasureInfo Name="MeasureType" QuestionID="11" AlternativeID="0" DataType="text" />
        <MeasureInfo Name="Description" QuestionID="7" AlternativeID="45" DataType="text" />
    */       
    SET @QTxml = CONVERT(xml, @MeasureXML) 
 
    SELECT Tbl.Col.value('@Name',           'nvarchar(max)') [Name],
           Tbl.Col.value('@QuestionID',     'int') [QuestionID],
           Tbl.Col.value('@AlternativeID',  'int') [AlternativeID],
           Tbl.Col.value('@DataType',       'nvarchar(64)') [DataType]
    INTO #QuestionTable       
    FROM @QTxml.nodes('//MeasureInfo') Tbl(Col) 
    
    SET @QTxml = CONVERT(xml, @ActorXML)
    INSERT INTO #QuestionTable( Name ,QuestionID ,AlternativeID ,DataType)   
    SELECT Tbl.Col.value('@Name',           'nvarchar(max)') [Name],
           Tbl.Col.value('@QuestionID',     'int') [QuestionID],
           Tbl.Col.value('@AlternativeID',  'int') [AlternativeID],
           Tbl.Col.value('@DataType',       'nvarchar(64)') [DataType]       
    FROM @QTxml.nodes('//ActorInfo') Tbl(Col)  
    
    SET @QTxml = CONVERT(xml, @HolderXML)
    SELECT @QID_HolderID = Tbl.Col.value('@QuestionID', 'int'), @AltID_HolderID = Tbl.Col.value('@AlternativeID', 'int')
    FROM @QTxml.nodes('//Holder') Tbl(Col)
    WHERE Tbl.Col.value('@Name', 'nvarchar(max)') = 'HolderID'    
         
    SELECT s.SurveyID, s.ReportServer, s.ReportDB , s.ActivityID
    INTO  #ConnectionTable  
    FROM at.Survey s WHERE s.ActivityID = @PoAActivityID OR s.ActivityID = @MeasureActivityID
    
    DECLARE c_Measure CURSOR LOCAL FAST_FORWARD READ_ONLY FOR
    SELECT DISTINCT CT.ReportServer, CT.ReportDB
    FROM #ConnectionTable CT  
    WHERE CT.[ActivityID] = @PoAActivityID 
              
    OPEN c_Measure
    FETCH NEXT FROM c_Measure INTO @MeasureServerReport, @MeasureDatabaseReport
    WHILE @@FETCH_STATUS = 0
    BEGIN
     
       SET @SqlCommand = '  
       SELECT ResultID, Initiated, Finished, DayToDeadline, CONVERT(NVARCHAR(MAX),NULL) AS [MeasureType], [Description], ActorDepartmentID DepartmentID, [DepartmentName],
             [UserID], [FirstName], [LastName], [Email], [SSN],NULL [LanguageID], [ChildID], [RHUserID] 
       FROM
       (    
        SELECT mea.ResultID,  CONVERT(DATE,mea.StartDate) AS Initiated, CONVERT(DATE,mea.DueDate) AS Finished, DATEDIFF(DAY, GETDATE(), mea.DueDate) AS DayToDeadline, mea.UserID AS [ChildID], arh.Free AS [RHUserID] ,
         qt.Name, a.[No],
               (CASE WHEN qt.[DataType] = ''text'' THEN a.[Free] WHEN qt.[DataType] = ''date'' THEN convert(nvarchar(max),a.[DateValue],120) ELSE convert(nvarchar(max),a.[Value]) END) [Value]      
        FROM ['+ @MeasureServerReport + '].[' + @MeasureDatabaseReport +'].[dbo].[Result] PoA       
        INNER JOIN #ConnectionTable PoASurvey ON PoASurvey.SurveyID = PoA.SurveyID AND PoA.Status >= 0 AND PoA.EndDate IS NULL AND PoASurvey.ActivityID = '+ CONVERT(VARCHAR(10),@PoAActivityID) +' 
                                AND PoASurvey.ReportServer = '''+ CONVERT(VARCHAR(MAX),@MeasureServerReport) +''' AND PoASurvey.ReportDB = '''+ CONVERT(VARCHAR(MAX),@MeasureDatabaseReport) +'''
        INNER JOIN ['+ @MeasureServerReport + '].[' + @MeasureDatabaseReport +'].[dbo].[Result] mea ON mea.ParentResultID = PoA.ResultID AND mea.Status >= 0 AND mea.EndDate IS NULL
        INNER JOIN #ConnectionTable meaSurvey ON meaSurvey.SurveyID = mea.SurveyID AND meaSurvey.ActivityID = '+ CONVERT(VARCHAR(10),@MeasureActivityID) +' 
                                AND meaSurvey.ReportServer = '''+ CONVERT(VARCHAR(MAX),@MeasureServerReport) +''' AND meaSurvey.ReportDB = '''+ CONVERT(VARCHAR(MAX),@MeasureDatabaseReport) +'''  
        INNER JOIN ['+ @MeasureServerReport + '].[' + @MeasureDatabaseReport +'].[dbo].[Answer] a ON a.ResultID = mea.ResultID         
        INNER JOIN ['+ @MeasureServerReport + '].[' + @MeasureDatabaseReport +'].[dbo].[Result] ls ON ls.UserID = mea.UserID    
        INNER JOIN ['+ @MeasureServerReport + '].[' + @MeasureDatabaseReport +'].[dbo].[Answer] arh ON ls.[ResultID] = arh.[ResultID] 
                            AND ls.[Status] >= 0 AND arh.QuestionID = '+ CONVERT(VARCHAR(10),@QID_HolderID) +' AND arh.AlternativeID = '+ CONVERT(VARCHAR(10),@AltID_HolderID) +'  
        INNER JOIN #QuestionTable qt ON a.[QuestionID] = qt.[QuestionID] AND (a.[AlternativeID] = qt.[AlternativeID] OR qt.[AlternativeID] = 0) 
        WHERE DATEDIFF(DAY, GETDATE(), mea.DueDate) <= '+CAST(@NumberOfDaysToExpire AS VARCHAR(128))+' 
        ) v
       PIVOT (MAX([v].[Value]) FOR [v].[Name] IN ([Description],[ActorDepartmentID],[DepartmentName],[UserID],[FirstName],[LastName],[Email],[SSN],[HolderID])) p
       WHERE UserID IS NOT NULL'
          
        INSERT INTO #ListMeasure(ResultID ,Initiated ,Finished ,DayToDeadline ,MeasureType ,[Description] ,DepartmentID ,DepartmentName 
                                 ,UserID ,FirstName ,LastName ,Email ,SSN ,LanguageID, ChildID,[RHUserID])
        EXEC(@SqlCommand)

		--Delete if customer inactive
        SET @SqlCommand = '
		DELETE lm
        FROM #ListMeasure lm   
        INNER JOIN org.[User] u ON lm.UserID = u.UserID AND lm.UserID <> 0
		INNER JOIN org.Department d ON d.DepartmentID = u.DepartmentID 
		INNER JOIN org.Customer c ON d.CustomerID = c.CustomerID AND c.[Status] < 0'
		EXEC(@SqlCommand)

		--Update Department info
        SET @SqlCommand = '  
        UPDATE lm
        SET MeasureType = lta.Name, DepartmentID = d.DepartmentID, DepartmentName = d.Name, LanguageID = u.LanguageID
        FROM #ListMeasure lm   
        INNER JOIN ['+ @MeasureServerReport + '].[' + @MeasureDatabaseReport +'].[dbo].[Answer] a ON a.ResultID = lm.ResultID AND a.QuestionID = '+ CONVERT(VARCHAR(10),@MeasureType) +' 
        INNER JOIN at.LT_Alternative lta ON lta.AlternativeID = a.AlternativeID 
        INNER JOIN org.[User] u ON lm.UserID = u.UserID AND lta.LanguageID = u.LanguageID AND lm.UserID <> 0
		INNER JOIN org.Department d ON d.DepartmentID = u.DepartmentID '
		EXEC(@SqlCommand)

		-- UPdate MeasureType for Unreal Actor by RH's language
		SET @SqlCommand = '  
        UPDATE lm
        SET MeasureType = lta.Name
        FROM #ListMeasure lm   
        INNER JOIN ['+ @MeasureServerReport + '].[' + @MeasureDatabaseReport +'].[dbo].[Answer] a ON a.ResultID = lm.ResultID AND a.QuestionID = '+ CONVERT(VARCHAR(10),@MeasureType) +' 
        INNER JOIN at.LT_Alternative lta ON lta.AlternativeID = a.AlternativeID 
        INNER JOIN org.[User] rh ON lm.RHUserID = rh.UserID AND lta.LanguageID = rh.LanguageID AND lm.UserID = 0'	
		EXEC(@SqlCommand)
		
		UPDATE lm 
		SET LanguageID = rh.LanguageID
		FROM  #ListMeasure lm 
		INNER JOIN org.[User] rh ON lm.RHUserID = rh.UserID AND lm.UserID = 0

        FETCH NEXT FROM c_Measure INTO @MeasureServerReport, @MeasureDatabaseReport
    END
    CLOSE c_Measure
    DEALLOCATE c_Measure   

    /*
	SELECT lm.ResultID, Initiated, Finished, DayToDeadline, [MeasureType], lm.[Description],ISNULL(lm.DepartmentID,0) DepartmentID, lm.[DepartmentName],
           lm.[UserID], lm.[FirstName], lm.[LastName], lm.[Email], lm.[SSN], lm.[LanguageID], lg.LanguageCode, lm.ChildID, child.FirstName ChildFirstName, child.LastName ChildLastName, rh.UserID RHUserID, rh.FirstName RHFirstName, rh.LastName RHLastName
    FROM #ListMeasure lm   
    INNER JOIN dbo.[Language] lg ON lg.LanguageID = lm.LanguageID   
    INNER JOIN org.[User] child ON child.UserID = lm.ChildID
    INNER JOIN org.[User] rh ON rh.UserID = lm.[RHUserID]    
	ORDER BY DayToDeadline DESC
    */
    SELECT ISNULL(lm.DepartmentID,0) DepartmentID, lm.[FirstName], lm.[LastName], lm.[Email], lm.[LanguageID], l.[LanguageCode], MIN(lm.[DayToDeadline]) [DayToDeadline]
    FROM #ListMeasure lm
    JOIN [Language] l ON lm.[LanguageID] = l.[LanguageID]
    GROUP BY ISNULL(lm.DepartmentID,0), lm.[FirstName], lm.[LastName], lm.[Email], lm.[LanguageID], l.[LanguageCode]
	ORDER BY MIN(lm.[DayToDeadline])

    DROP TABLE #ListMeasure
	DROP TABLE #QuestionTable
    DROP TABLE #ConnectionTable
END

GO
