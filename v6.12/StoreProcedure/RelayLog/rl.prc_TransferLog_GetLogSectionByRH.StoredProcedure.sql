/****** Object:  StoredProcedure [rl].[prc_TransferLog_GetLogSectionByRH]    Script Date: 5/11/2017 10:41:02 AM ******/
DROP PROCEDURE [rl].[prc_TransferLog_GetLogSectionByRH]
GO
/****** Object:  StoredProcedure [rl].[prc_TransferLog_GetLogSectionByRH]    Script Date: 5/11/2017 10:41:02 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/*  
	2017-03-21 Ray:     Get more column: SSN
	2017-04-24	 - Sarah - Using EntityStatusID column
*/
-- EXEC [rl].[prc_TransferLog_GetLogSectionByRH] 8, 3, 1
CREATE PROCEDURE [rl].[prc_TransferLog_GetLogSectionByRH]
(
	@UserID		          INT,				-- Current RH's UserID login
	@DepartmentID         NVARCHAR(MAX),
    @SiteID               INT = 1      
)
AS
BEGIN
	SET NOCOUNT ON
	DECLARE @Err INT
    
    DECLARE @LogSectionActivityID INT, @LogSectionSurveyID INT,@LogSectionBatchID INT, @QID_ActorDepartmentID INT, @AltID_ActorDepartmentID INT,
            @MeasureActivityID INT, @MeasureSurveyID INT, @MeasureBatchID INT,           
            @QID_Actor int, @AltID_Actor INT, @QID_HolderID int, @AltID_HolderID int, @UT_Holder int, @UT_Actor INT,
            @TransferLogXML nvarchar(max), @QTxml XML,@ChildXML nvarchar(max), @HDID INT, @StatusType_TransferRequested INT
	DECLARE @EntityStatus_Active INT
	SELECT @EntityStatus_Active = E.EntityStatusID FROM EntityStatus E WHERE E.CodeName = 'Active'
    /*
     -- The structure of site parameter Stafettloggen.LogSection.TransferLog should be
	<TransferLog Name="RHId" QuestionID="40" AlternativeID="1225" DataType="numberic" />
	<TransferLog Name="OMId" QuestionID="40" AlternativeID="1226" DataType="numberic" />
	<TransferLog Name="DateOfRequest" QuestionID="40" AlternativeID="1227" DataType="date" />

	 -- The structure of site parameter LISTCOLUMN_ChildInfo should be
	<ChildInfo Name="FirstName" QuestionID="1" AlternativeID="1" DataType="text" />
	<ChildInfo Name="LastName" QuestionID="1" AlternativeID="9" DataType="text" />
	<ChildInfo Name="Age" QuestionID="1" AlternativeID="11" DataType="int" />
	<ChildInfo Name="Level" QuestionID="4" AlternativeID="0" DataType="int" />
	<ChildInfo Name="Picture" QuestionID="31" AlternativeID="128" DataType="int" />
	<ChildInfo Name="ConsentExpired" QuestionID="3" AlternativeID="65" DataType="date" />
	<ChildInfo Name="ConsentTransferExpires" QuestionID="9" AlternativeID="70" DataType="date" />
    */

    SELECT @TransferLogXML       = [Value] FROM app.[SiteParameter] WHERE [Key] = 'Stafettloggen.LogSection.TransferLogInformationSetting'        AND [SiteID] = @SiteID        
    SELECT @ChildXML            = [Value] FROM app.[SiteParameter] WHERE [Key] = 'LISTCOLUMN_ChildInfo'       AND [SiteID] = @SiteID
    SELECT @UT_Holder            = [Value] FROM app.[SiteParameter] WHERE [Key] = 'UT_RELAY_HOLDER'         AND [SiteID] = @SiteID   
    SELECT @LogSectionActivityID = [Value] FROM app.[SiteParameter] WHERE [Key] = 'ACTIVITY_ID_LOG_SECTION' AND [SiteID] = @SiteID
    SELECT @StatusType_TransferRequested  = [Value] FROM app.[SiteParameter] WHERE [Key] = N'Stafettloggen.LogSection.StatusTypeTransferRequested'	 AND [SiteID] = @SiteID  

    SET @QTxml = CONVERT(xml, @TransferLogXML)
    SELECT Tbl.Col.value('@Name',           'nvarchar(max)') [Name],
           Tbl.Col.value('@QuestionID',     'int') [QuestionID],
           Tbl.Col.value('@AlternativeID',  'int') [AlternativeID],
           Tbl.Col.value('@DataType',       'nvarchar(64)') [DataType]
        INTO #QuestionTable
    FROM @QTxml.nodes('//TransferLog') Tbl(Col)    

	SET @QTxml = CONVERT(xml, @ChildXML)
	INSERT INTO #QuestionTable (Name, QuestionID, AlternativeID, DataType) 
	SELECT Name, QuestionID, AlternativeID, DataType
	FROM 
	(
	SELECT Tbl.Col.value('@Name',           'nvarchar(max)') [Name],
           Tbl.Col.value('@QuestionID',     'int') [QuestionID],
           Tbl.Col.value('@AlternativeID',  'int') [AlternativeID],
           Tbl.Col.value('@DataType',       'nvarchar(64)') [DataType]     
    FROM @QTxml.nodes('//ChildInfo') Tbl(Col) 
	)A
	WHERE A.Name NOT IN ('ConsentExpired', 'ConsentTransferExpires')

	IF OBJECT_ID('tempdb..#ListBatchByDepartment') IS NOT NULL DROP TABLE #ListBatchByDepartment
    SELECT S.SurveyID, B.BatchID, S.ReportServer, S.ReportDB INTO #ListBatchByDepartment
    FROM at.Batch B
	INNER JOIN at.Survey S ON S.SurveyID = B.SurveyID AND DepartmentID = @DepartmentID AND S.ActivityID = @LogSectionActivityID
	
	--SELECT * FROM #ListBatchByDepartment
	-- Create index for #User
	CREATE INDEX IDX_C_ListBatchByDepartment_Survey		  ON #ListBatchByDepartment(SurveyID)
	CREATE INDEX IDX_C_#ListBatchByDepartment_RDB_RServer ON #ListBatchByDepartment(ReportServer,ReportDB)

    -- Step 2: count relay children and actor children for each actor above   
    CREATE TABLE #LogSection ([ResultID] bigint, [UserID] int, [SurveyID] int, [LastUpdated] datetime, [StatusTypeID] int, [FirstName] nvarchar(max), [LastName] nvarchar(max), 
							  [Age] int, [DateOfRequest] DATETIME, [Level] int, [Picture] nvarchar(max), [EndDate] DATETIME, [SSN] nvarchar(32))
    DECLARE @LogSectionServerReport VARCHAR(MAX), @LogSectionDatabaseReport VARCHAR(MAX), @SqlCommand VARCHAR(MAX), @varChoose INT, @QID_RHId INT, @AltID_RHId INT

	SELECT @QID_RHId = QuestionID, @AltID_RHId = AlternativeID FROM #QuestionTable WHERE Name = 'RHId'
	SET  @varChoose = (SELECT CASE WHEN [DataType] = 'text' THEN 0  
								   WHEN [DataType] = 'date' THEN 1  
								   ELSE 2 END FROM #QuestionTable WHERE Name = 'RHId') 	   
						         
    DECLARE c_LogSection CURSOR LOCAL FAST_FORWARD READ_ONLY FOR
    SELECT DISTINCT CT.ReportServer, CT.ReportDB
    FROM #ListBatchByDepartment CT  
              
    OPEN c_LogSection
    FETCH NEXT FROM c_LogSection INTO @LogSectionServerReport, @LogSectionDatabaseReport
    WHILE @@FETCH_STATUS = 0
    BEGIN                
        SET @SqlCommand = '
		SELECT [ResultID], [UserID], [SurveyID], [LastUpdated], [StatusTypeID], [FirstName], [LastName], [Age], [DateOfRequest], [Level], [Picture], [EndDate], [SSN]
        FROM (
        SELECT ls.ResultID , ls.UserID, ls.SurveyID, ls.LastUpdated, ls.StatusTypeID,  qt.[Name], ls.EndDate,
                (CASE 
					WHEN qt.[DataType] = ''text'' THEN child.[Free] 
					WHEN qt.[DataType] = ''date'' THEN convert(nvarchar(max), child.[DateValue],120) 
					ELSE convert(nvarchar(max), child.[Value]) END) [Value]              
        FROM  ['+ @LogSectionServerReport + '].[' + @LogSectionDatabaseReport +'].[dbo].[Result] ls
        INNER JOIN #ListBatchByDepartment lbd ON lbd.SurveyID = ls.SurveyID AND lbd.BatchID = ls.BatchID AND ls.EntityStatusID = ' + CAST(@EntityStatus_Active AS VARCHAR(128)) +' AND ls.Deleted IS NULL AND ls.EndDate IS NULL
							 AND ls.StatusTypeID = '+ CONVERT(VARCHAR(10),@StatusType_TransferRequested) + '
							 AND lbd.ReportServer = '''+ CONVERT(VARCHAR(MAX),@LogSectionServerReport) +''' AND lbd.ReportDB = '''+ CONVERT(VARCHAR(MAX),@LogSectionDatabaseReport) +'''
        INNER JOIN ['+ @LogSectionServerReport + '].[' + @LogSectionDatabaseReport +'].[dbo].[Answer] a ON ls.[ResultID] = a.[ResultID] 
                             AND a.QuestionID = '+ CONVERT(VARCHAR(10),@QID_RHId) +' AND a.AlternativeID = '+ CONVERT(VARCHAR(10),@AltID_RHId) +'
							 AND (('+ CONVERT(VARCHAR(10),@varChoose) +' = ''0'' AND a.Free = '+ CONVERT(VARCHAR(10),@UserID) + ')
							   OR ('+ CONVERT(VARCHAR(10),@varChoose) +' = ''2'' AND a.Value = '+ CONVERT(VARCHAR(10),@UserID) + ')) 
		INNER JOIN ['+ @LogSectionServerReport + '].[' + @LogSectionDatabaseReport +'].[dbo].[Answer] child ON child.[ResultID] = a.[ResultID] 
        INNER JOIN #QuestionTable qt ON child.[QuestionID] = qt.[QuestionID] AND (child.[AlternativeID] = qt.[AlternativeID] OR qt.[AlternativeID] = 0)
        ) v
        PIVOT (MAX([v].[Value]) FOR [v].[Name] IN ([FirstName], [LastName], [Age], [DateOfRequest], [Level], [Picture], [SSN])) p'
    
        INSERT INTO #LogSection([ResultID], [UserID], [SurveyID], [LastUpdated], [StatusTypeID], [FirstName], [LastName], [Age], [DateOfRequest], [Level], [Picture], [EndDate], [SSN])
        EXEC(@SqlCommand) 
          
        FETCH NEXT FROM c_LogSection INTO @LogSectionServerReport, @LogSectionDatabaseReport
    END
    CLOSE c_LogSection
    DEALLOCATE c_LogSection      

	SELECT * FROM #LogSection

    DROP TABLE #QuestionTable    
	DROP TABLE #ListBatchByDepartment
	DROP TABLE #LogSection

	RETURN @Err
	SET NOCOUNT OFF
END
GO
