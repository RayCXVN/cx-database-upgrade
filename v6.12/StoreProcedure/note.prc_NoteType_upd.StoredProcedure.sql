SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[note].[prc_NoteType_upd]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [note].[prc_NoteType_upd] AS' 
END
GO
ALTER PROCEDURE [note].[prc_NoteType_upd]
(
	@NoteTypeID int,
	@OwnerID int,
	@Name nvarchar(128),
	@UseVersioning bit,
	@UseReadLog bit,
	@UseHTML bit,
	@UseEncryption bit,
	@cUserid int,
	@Log smallint = 1
)
AS
BEGIN
	SET NOCOUNT ON
	DECLARE @Err Int

	UPDATE [note].[NoteType]
	SET
		[OwnerID] = @OwnerID,
		[Name] = @Name,
		[UseVersioning] = @UseVersioning,
		[UseReadLog] = @UseReadLog,
		[UseHTML] = @UseHTML,
		[UseEncryption] = @UseEncryption
	WHERE
		[NoteTypeID] = @NoteTypeID

	Set @Err = @@Error

	IF @Log = 1 
	BEGIN 
		INSERT INTO [Log].[AuditLog] ( UserId, TableName, Type, Data, Created) 
		SELECT @cUserid,'NoteType',1,
		( SELECT * FROM [note].[NoteType] 
			WHERE
			[NoteTypeID] = @NoteTypeID FOR XML AUTO) as data,
				getdate() 
	 END

	RETURN @Err
END

GO
