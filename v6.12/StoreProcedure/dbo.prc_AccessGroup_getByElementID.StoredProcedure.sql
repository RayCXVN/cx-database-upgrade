SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[prc_AccessGroup_getByElementID]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [dbo].[prc_AccessGroup_getByElementID] AS' 
END
GO
ALTER PROC [dbo].[prc_AccessGroup_getByElementID]
(
  @TableTypeID INT,
  @ElementID INT,
  @OwnerID INT
)
AS
  SELECT A.*
  FROM  dbo.AccessGroup AG
  JOIN dbo.AccessGeneric A ON AG.AccessGroupID = A.AccessGroupID 
							  AND AG.OwnerID = @OwnerID
							  AND A.ElementID = @ElementID
							  AND A.TableTypeID = @TableTypeID


GO
