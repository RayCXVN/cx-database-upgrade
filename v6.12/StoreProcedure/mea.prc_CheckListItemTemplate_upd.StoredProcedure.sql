SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[mea].[prc_CheckListItemTemplate_upd]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [mea].[prc_CheckListItemTemplate_upd] AS' 
END
GO
ALTER PROCEDURE [mea].[prc_CheckListItemTemplate_upd]    
(
	@CheckListItemTemplateID int,
	@MeasureTemplateID int,
	@No smallint,
	@cUserid int,  
	@Log smallint = 1
)
AS    
BEGIN    
 SET NOCOUNT ON;    
 DECLARE @Err Int    

 UPDATE [mea].[CheckListItemTemplate]
 SET
	[MeasureTemplateID] = @MeasureTemplateID,
	[No] = @No
 WHERE
	[CheckListItemTemplateId] = @CheckListItemTemplateID
 
 Set @Err = @@Error 
  
 IF @Log = 1   
 BEGIN   
  INSERT INTO [Log].[AuditLog] ( UserId, TableName, Type, Data, Created)   
  SELECT @cUserid,'mea.CheckListItemTemplate',1,  
  ( SELECT * FROM [mea].[CheckListItemTemplate]   
   WHERE  
   [CheckListItemTemplateID] = @CheckListItemTemplateID    FOR XML AUTO) as data,  
    getdate()   
  END  
  
 RETURN @Err  

END

GO
