SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[mea].[prc_LT_CheckListItemTemplate_upd]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [mea].[prc_LT_CheckListItemTemplate_upd] AS' 
END
GO
ALTER PROCEDURE [mea].[prc_LT_CheckListItemTemplate_upd]
	@LanguageID int,
	@CheckListItemTemplateID int,
	@Text nvarchar(max) = '',
	@cUserid int,
    @Log smallint = 1
AS
BEGIN
	SET NOCOUNT ON;
	DECLARE @Err Int

    UPDATE [mea].[LT_CheckListItemTemplate]
    SET    
         [Text] = @Text
    WHERE
		[LanguageID] = @LanguageID AND
		[CheckListItemTemplateID] = @CheckListItemTemplateID

    Set @Err = @@Error
	IF @Log = 1 
	BEGIN 
		INSERT INTO [Log].[AuditLog] ( UserId, TableName, Type, Data, Created) 
		SELECT @cUserid,'LT_CheckListItemTemplate',1,
		( SELECT * FROM [mea].[CheckListItemTemplate]
			WHERE
			[CheckListItemTemplateID] = @CheckListItemTemplateID				 FOR XML AUTO) as data,
				getdate() 
	END

	RETURN @Err       
END

GO
