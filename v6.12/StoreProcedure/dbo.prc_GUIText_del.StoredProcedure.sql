SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[prc_GUIText_del]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [dbo].[prc_GUIText_del] AS' 
END
GO

ALTER PROCEDURE [dbo].[prc_GUIText_del]
(
	@LanguageID int,
	@ItemID int,
	@cUserid int,
	@Log smallint = 1
)
AS
BEGIN
	SET NOCOUNT ON
	DECLARE @Err Int

	IF @Log = 1 
	BEGIN 
		INSERT INTO [Log].[AuditLog] ( UserId, TableName, Type, Data, Created) 
		SELECT @cUserid,'GUIText',2,
		( SELECT * FROM [dbo].[GUIText] 
			WHERE
			[LanguageID] = @LanguageID AND
			[ItemID] = @ItemID
			 FOR XML AUTO) as data,
			getdate() 
	END 


	DELETE FROM [dbo].[GUIText]
	WHERE
		[LanguageID] = @LanguageID AND
		[ItemID] = @ItemID

	Set @Err = @@Error

	RETURN @Err
END


GO
