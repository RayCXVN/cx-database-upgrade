SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[prop].[prc_PropValue_del]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [prop].[prc_PropValue_del] AS' 
END
GO


ALTER PROCEDURE [prop].[prc_PropValue_del]
(
	@PropValueID int,
	@cUserid int,
	@Log smallint = 1
)
AS
BEGIN
	SET NOCOUNT ON
	DECLARE @Err Int

	IF @Log = 1 
	BEGIN 
		INSERT INTO [Log].[AuditLog] ( UserId, TableName, Type, Data, Created) 
		SELECT @cUserid,'PropValue',2,
		( SELECT * FROM [prop].[PropValue] 
			WHERE
			[PropValueID] = @PropValueID
			 FOR XML AUTO) as data,
			getdate() 
	END 


	DELETE FROM [prop].[PropValue]
	WHERE
		[PropValueID] = @PropValueID

	Set @Err = @@Error

	RETURN @Err
END


GO
