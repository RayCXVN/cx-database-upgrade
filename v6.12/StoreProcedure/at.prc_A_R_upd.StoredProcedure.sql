SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[at].[prc_A_R_upd]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [at].[prc_A_R_upd] AS' 
END
GO

ALTER PROCEDURE [at].[prc_A_R_upd]
(
	@ActivityID int,
	@RoleID int,
	@No smallint,
	@cUserid int,
	@Log smallint = 1
)
AS
BEGIN
	SET NOCOUNT ON
	DECLARE @Err Int

	UPDATE [at].[A_R]
	SET
		[ActivityID] = @ActivityID,
		[RoleID] = @RoleID,
		[No] = @No
	WHERE
		[ActivityID] = @ActivityID AND
		[RoleID] = @RoleID

	IF @Log = 1 
	BEGIN 
		INSERT INTO [Log].[AuditLog] ( UserId, TableName, Type, Data, Created) 
		SELECT @cUserid,'A_R',1,
		( SELECT * FROM [at].[A_R] 
			WHERE
			[ActivityID] = @ActivityID AND
			[RoleID] = @RoleID			 FOR XML AUTO) as data,
			getdate()
	END

	Set @Err = @@Error

	RETURN @Err
END


GO
