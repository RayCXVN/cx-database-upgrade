SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[app].[prc_LoginService_ins]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [app].[prc_LoginService_ins] AS' 
END
GO
/*
	2018-10-31	Sarah	CXPLAT-848
*/
ALTER PROCEDURE [app].[prc_LoginService_ins]
    @LoginServiceID int = null output,
    @SiteID int,
    @LoginServiceType int,
    @IconUrl nvarchar(1024),  
    @Authority nvarchar(1024), 
    @MetadataAddress nvarchar(1024),  
    @RedirectUri nvarchar(1024),  
    @SecondaryClaimType nvarchar(1024),
    @ClientId nvarchar(1024),
    @ClientSecret nvarchar(1024),
    @Scope nvarchar(1024),
    @ResponseType nvarchar(1024),
    @PrimaryClaimType nvarchar(1024),  
    @PostLogoutUri nvarchar(1024),
    @Disabled bit,
    @cUserid int,
    @Log smallint = 1
AS
BEGIN
    SET NOCOUNT ON;
    DECLARE @Err Int
    
    INSERT INTO [app].[LoginService]
           ([SiteID]  
            ,[LoginServiceType]  
            ,[IconUrl]  
            ,[Authority] 
            ,[MetadataAddress]  
            ,[RedirectUri]  
            ,[SecondaryClaimType]
            ,[ClientId]
            ,[ClientSecret]
            ,[Scope]
            ,[ResponseType]
            ,[PrimaryClaimType]  
            ,[PostLogoutUri]
            ,[Disabled]
            ,[CreatedDate])
    VALUES
           (@SiteID  
            ,@LoginServiceType  
            ,@IconUrl  
            ,@Authority 
            ,@MetadataAddress  
            ,@RedirectUri  
            ,@SecondaryClaimType
            ,@ClientId
            ,@ClientSecret
            ,@Scope
            ,@ResponseType
            ,@PrimaryClaimType  
            ,@PostLogoutUri
            ,@Disabled
            ,GETDATE())
           
    Set @Err = @@Error
    Set @LoginServiceID = scope_identity()
    IF @Log = 1 
    BEGIN 
        INSERT INTO [Log].[AuditLog] ( UserId, TableName, Type, Data, Created) 
        SELECT @cUserid,'LoginService',0,
        ( SELECT * FROM [app].[LoginService]
            WHERE
            [LoginServiceID] = @LoginServiceID                 FOR XML AUTO) as data,
                getdate() 
    END

    
    RETURN @Err       
END

GO
