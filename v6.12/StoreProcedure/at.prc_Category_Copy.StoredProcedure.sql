SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[at].[prc_Category_Copy]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [at].[prc_Category_Copy] AS' 
END
GO
-- Select * from app.menu
-- select dbo.DecryptString(password),* from org.[user] where ownerid = 5
-- [AT].[prc_Activity_copy] 19,null,3,5,7,1,0
--update at.activity set ownerid = 5 where activityid = 26

ALTER PROC [at].[prc_Category_Copy]
(	
	@ActivityID	INT = 27,
	@NewAID		INT = 26,
    @ParentCategoryID INT = 2496,
    @NewPageID INT = 127,
    @NewOwnerID INT = 5,
    @NewMenuID INT = 15
	
)
AS
BEGIN
SET NOCOUNT ON
SET XACT_ABORT ON
BEGIN TRANSACTION

CREATE TABLE #Page
(
	ID	BIGINT,
	NEWID BIGINT
)
CREATE TABLE #Scale
(
	ID	BIGINT,
	NEWID BIGINT
)
CREATE TABLE #Question
(
	ID	BIGINT,
	NEWID BIGINT
)
CREATE TABLE #Category
(
	ID	BIGINT,
	NEWID BIGINT
)
CREATE TABLE #LevelGroup
(
 ID	BIGINT,
 NEWID BIGINT
)
CREATE TABLE #LevelLimit
(
 ID	BIGINT,
 NEWID BIGINT
)

CREATE TABLE #PortalPage
(
 ID	BIGINT,
 NEWID BIGINT
)

CREATE TABLE #PortalPart
(
 ID	BIGINT,
 NEWID BIGINT
)

CREATE TABLE #Menu
(
 ID	BIGINT,
 NEWID BIGINT
)

CREATE TABLE #MenuItem
(
 ID	BIGINT,
 NEWID BIGINT
)

CREATE TABLE #Bubble
(
 ID	BIGINT,
 NEWID BIGINT
)

CREATE TABLE #BubbleText
(
 ID	BIGINT,
 NEWID BIGINT
)


DECLARE
@ID	INT,
@NewID INT,
@CurOwnerID INT





-- Category
DECLARE @ParentID INT
DECLARE @NewParentID INT
DECLARE @Tag NVARCHAR(128)
DECLARE @Type SMALLINT
DECLARE @No INT
DECLARE @Fillcolor VARCHAR(16)
DECLARE @BorderColor VARCHAR(16)
DECLARE @Weight FLOAT

DECLARE curCat CURSOR LOCAL FAST_FORWARD READ_ONLY FOR 
WITH Cat (CategoryID,ParentID,Tag,TYPE,NO,Fillcolor,BorderColor,Weight)
AS
(
  --Anchor members
	  SELECT CategoryID,ParentID,Tag,TYPE,NO,Fillcolor,BorderColor,Weight
	  FROM AT.Category c
	  WHERE c.ActivityID = 27 AND c.CategoryID = 2496
  UNION ALL
  -- Recursive member def
  SELECT c.CategoryID,c.ParentID,c.Tag,c.TYPE,c.NO,c.Fillcolor,c.BorderColor,c.Weight
	  FROM AT.Category c
	  INNER JOIN Cat AS c1 ON  c.Parentid = c1.categoryid
	 )
SELECT CategoryID,ParentID,Tag,TYPE,NO,Fillcolor,BorderColor,Weight FROM Cat


OPEN curCat
FETCH NEXT FROM curCat INTO @ID,@ParentID,@Tag,@Type,@No,@Fillcolor,@BorderColor,@Weight

WHILE (@@FETCH_STATUS = 0)
BEGIN
    SET @NewParentID = @Parentid
    IF NOT @Parentid IS NULL
    BEGIN
       SELECT @NewParentID = [NEWID] FROM #Category WHERE ID = @Parentid
    END
	INSERT AT.Category(ActivityID, ParentID, Tag, TYPE, NO, Fillcolor, BorderColor, Weight, Created) 
	VALUES(@NewAID,@NewParentID,@ID,@Type,@No,@Fillcolor,@BorderColor,@Weight,GETDATE())	
	SET @NewID = SCOPE_IDENTITY()

	INSERT #Category(ID, NEWID) VALUES(@ID, @NewID)

	-- Q_C
	INSERT AT.Q_C(QuestionID, CategoryID, Weight,NO,Visible)
	SELECT Q2.Questionid,@NewID,1,QC.no,QC.visible FROM AT.Q_C QC
    JOIN AT.Question Q on Q.QuestionID = QC.QuestionID and QC.CategoryID = @ID
    JOIN AT.Question Q2 on Q2.Extid = Q.Extid and Q2.PageID = @NewPageID

	FETCH NEXT FROM curCat INTO @ID,@ParentID,@Tag,@Type,@No,@Fillcolor,@BorderColor,@Weight
END
CLOSE curCat
DEALLOCATE curCat

INSERT AT.LT_Category(LanguageID, CategoryID, Name,Description )
SELECT LanguageID, C.NEWID, Name, Description
FROM #Category C
INNER JOIN AT.LT_Category LTC ON C.ID = LTC.CategoryID



-- LevelLimits
DECLARE curLL CURSOR LOCAL FAST_FORWARD READ_ONLY FOR
SELECT LevelLimitID FROM AT.LevelLimit
WHERE CategoryID IN (SELECT CategoryID FROM AT.Category WHERE ActivityID = 27 and categoryID > 2495 )
order by CategoryID

OPEN curLL
FETCH NEXT FROM curLL INTO @ID

WHILE (@@FETCH_STATUS = 0)
BEGIN
	INSERT AT.LevelLimit(LevelGroupID, CategoryID, QuestionID, MinValue, MaxValue, Sigchange, Fillcolor, BorderColor, Created)
	SELECT NULL, 
	        (SELECT [NEWID] FROM #Category WHERE ID = LL.CategoryID), 
	        NULL , 
	        MinValue, MaxValue, Sigchange, Fillcolor, BorderColor, GETDATE()
	FROM AT.LevelLimit ll
	WHERE  LevelLimitID = @ID
	SET @NewID = SCOPE_IDENTITY()

	INSERT #LevelLimit(ID, NEWID) VALUES(@ID, @NewID)

	FETCH NEXT FROM curLL INTO @ID
END
CLOSE curLL
DEALLOCATE curLL

INSERT AT.LT_LevelLimit(LanguageID, LevelLimitID, Name, Description)
SELECT LanguageID, LL.[NEWID], Name, Description
FROM #LevelLimit LL
INNER JOIN AT.LT_LevelLimit LL2 ON LL.ID = LL2.LevelLimitID




 --Portalpage
	DECLARE curPP CURSOR LOCAL FAST_FORWARD READ_ONLY FOR
	SELECT PortalPageID
	FROM app.PortalPage 
	WHERE Ownerid = 6 and portalpageid > 1225 and portalpageid < 1238

	OPEN curPP
	FETCH NEXT FROM curPP INTO @ID

	WHILE (@@FETCH_STATUS = 0)
	BEGIN
		INSERT app.PortalPage(OwnerID, [NO])
		SELECT @NewOwnerID, [NO]
		FROM app.PortalPage	
		WHERE PortalPageID = @ID
		SET @NewID = SCOPE_IDENTITY()

		INSERT #PortalPage(ID, NEWID) VALUES(@ID, @NewID)

		FETCH NEXT FROM curPP INTO @ID
	END
	CLOSE curPP
	DEALLOCATE curPP
	INSERT app.LT_PortalPage(LanguageID, PortalPageID, Name, Description)
	SELECT LanguageID, P.[NEWID], Name,  Description
	FROM #PortalPage P
	INNER JOIN app.LT_PortalPage L ON P.ID = L.PortalPageID
  
    --PortalPart
    DECLARE @PortalPageID INT
    DECLARE @CategoryID INT
    DECLARE curPPart CURSOR LOCAL FAST_FORWARD READ_ONLY FOR
	SELECT PortalPartID,PortalPageID,CategoryID
	FROM app.PortalPart 
	WHERE PortalPageID IN (SELECT ID FROM #PortalPage)

	OPEN curPPart
	FETCH NEXT FROM curPPart INTO @ID,@PortalPageID,@CategoryID

	WHILE (@@FETCH_STATUS = 0)
	BEGIN
		INSERT app.PortalPart(PortalPageID, PortalPartTypeID, NO, CategoryID, Settings, BubbleID, ReportPartID,ReportID)
		SELECT (SELECT [NEWID] FROM #PortalPage WHERE ID = @PortalPageID), 
		       PortalPartTypeID, NO, 
		       (SELECT [NEWID] FROM #Category WHERE ID = @CategoryID), 
		       Settings, 
		       NULL, 
		       NULL,
				NULL
		FROM app.PortalPart	
		WHERE PortalPartID = @ID
		SET @NewID = SCOPE_IDENTITY()

		INSERT #PortalPart(ID, NEWID) VALUES(@ID, @NewID)

		FETCH NEXT FROM curPPart INTO @ID,@PortalPageID,@CategoryID
	END
	CLOSE curPPart
	DEALLOCATE curPPart
	
	INSERT app.LT_PortalPart(LanguageID, PortalPartID, Name, Description,TEXT,TextAbove,TextBelow)
	SELECT LanguageID, P.[NEWID], Name,  Description,TEXT,TextAbove,TextBelow
	FROM #PortalPart P
	INNER JOIN app.LT_PortalPart L ON P.ID = L.PortalPartID



 --MenuItems
  DECLARE @MenuItemTypeID AS INT
  DECLARE @CssClass AS NVARCHAR(64)
  DECLARE @URL AS NVARCHAR(128)
  DECLARE @Target AS NVARCHAR(64)
  DECLARE @IsDefault AS BIT
  DECLARE @NewPortalPageID AS INT
  
  DECLARE curMI CURSOR LOCAL FAST_FORWARD READ_ONLY FOR 
WITH MenuI (MenuItemID, MenuID, ParentID, MenuItemTypeID, PortalPageID, NO, CssClass, URL, Target, IsDefault)
AS
(
  --Anchor members
	  SELECT MenuItemID, MenuID, 342, MenuItemTypeID, PortalPageID, NO, CssClass, URL, Target, IsDefault
	  FROM app.MenuItem MI
	  WHERE MI.MenuID = 17 AND MI.MenuItemID = 737
   UNION ALL
  -- Recursive member def
     SELECT MI.MenuItemID, MI.MenuID, MI.ParentID,MI.MenuItemTypeID, MI.PortalPageID, MI.NO, MI.CssClass, MI.URL, MI.Target, MI.IsDefault
	  FROM app.MenuItem MI
	  INNER JOIN MenuI AS M1 ON  MI.Parentid = M1.MenuItemid
	 )
    SELECT MenuItemID,  ParentID, MenuItemTypeID, PortalPageID, NO, CssClass, URL, Target, IsDefault FROM MenuI

   
  OPEN curMI
FETCH NEXT FROM curMI INTO @ID,@ParentID,@MenuItemTypeID, @PortalPageID, @No, @CssClass, @URL, @Target, @IsDefault

WHILE (@@FETCH_STATUS = 0)
BEGIN
    SET @NewParentID = @Parentid
    IF NOT @Parentid IS NULL and @NewParentID <> 342
    BEGIN
       SELECT @NewParentID = [NEWID] FROM #MenuITem WHERE ID = @Parentid
    END
    
    SELECT @NewPortalPageID = [NEWID] FROM #PortalPage WHERE ID =  @PortalPageID
    
	INSERT app.MenuItem( MenuID, ParentID, MenuItemTypeID, PortalPageID, NO, CssClass, URL, Target, IsDefault, Created) 
	VALUES(@NewMenuID,@NewParentID,@MenuItemTypeID,
	       @NewPortalPageID, 
	       @No, @CssClass, @URL, @Target, @IsDefault,GETDATE())	
	
	SET @NewID = SCOPE_IDENTITY()

	INSERT #MenuITem(ID, NEWID) VALUES(@ID, @NewID)

	FETCH NEXT FROM curMI INTO @ID,@ParentID,@MenuItemTypeID, @PortalPageID, @No, @CssClass, @URL, @Target, @IsDefault
END
CLOSE curMI
DEALLOCATE curMI


INSERT App.LT_MenuItem(LanguageID, MenuItemID, Name,Description, ToolTip)
SELECT LanguageID, MI.NEWID, Name, Description, ToolTip
FROM #MenuItem MI
INNER JOIN app.LT_MenuItem LMI ON MI.ID = LMI.MenuItemID


COMMIT TRANSACTION


END



GO
