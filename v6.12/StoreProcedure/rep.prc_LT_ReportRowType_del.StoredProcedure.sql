SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[rep].[prc_LT_ReportRowType_del]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [rep].[prc_LT_ReportRowType_del] AS' 
END
GO


ALTER PROCEDURE [rep].[prc_LT_ReportRowType_del]
(
	@ReportRowTypeID int,
	@LanguageID int,
	@cUserid int,
	@Log smallint = 1
)
AS
BEGIN
	SET NOCOUNT ON
	DECLARE @Err Int

	IF @Log = 1 
	BEGIN 
		INSERT INTO [Log].[AuditLog] ( UserId, TableName, Type, Data, Created) 
		SELECT @cUserid,'LT_ReportRowType',2,
		( SELECT * FROM [rep].[LT_ReportRowType] 
			WHERE
			[ReportRowTypeID] = @ReportRowTypeID AND
			[LanguageID] = @LanguageID
			 FOR XML AUTO) as data,
			getdate() 
	END 


	DELETE FROM [rep].[LT_ReportRowType]
	WHERE
		[ReportRowTypeID] = @ReportRowTypeID AND
		[LanguageID] = @LanguageID

	Set @Err = @@Error

	RETURN @Err
END


GO
