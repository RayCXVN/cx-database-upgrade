SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[list].[prc_LT_ItemListDataSource_del]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [list].[prc_LT_ItemListDataSource_del] AS' 
END
GO
ALTER PROCEDURE [list].[prc_LT_ItemListDataSource_del]
	@LanguageID int,
	@ItemListDataSourceID int,
	@cUserid int,
	@Log smallint = 1
AS
BEGIN
	SET NOCOUNT ON;
	DECLARE @Err Int
	
	IF @Log = 1 
	BEGIN 
		INSERT INTO [Log].[AuditLog] ( UserId, TableName, Type, Data, Created) 
		SELECT @cUserid,'LT_ItemListDataSource',2,
		( SELECT * FROM [list].[LT_ItemListDataSource] 
			WHERE
			[LanguageID] = @LanguageID AND
			[ItemListDataSourceID] = @ItemListDataSourceID
			 FOR XML AUTO) as data,
			getdate() 
	END 
	
    DELETE FROM [list].[LT_ItemListDataSource]
    WHERE
		[LanguageID] = @LanguageID AND
		[ItemListDataSourceID] = @ItemListDataSourceID
	
	Set @Err = @@Error

	RETURN @Err
  
END

GO
