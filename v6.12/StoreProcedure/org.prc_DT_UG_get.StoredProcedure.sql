SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[org].[prc_DT_UG_get]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [org].[prc_DT_UG_get] AS' 
END
GO

ALTER PROCEDURE [org].[prc_DT_UG_get]
(
	@UserGroupID int
)
AS
BEGIN
	SET NOCOUNT ON
	DECLARE @Err Int

	SELECT
	[UserGroupID],
	[DepartmentTypeID]	
	FROM [org].[DT_UG]	
	WHERE
	[UserGroupID] = @UserGroupID

	Set @Err = @@Error

	RETURN @Err
END


GO
