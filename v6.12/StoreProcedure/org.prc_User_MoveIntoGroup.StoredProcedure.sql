SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[org].[prc_User_MoveIntoGroup]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [org].[prc_User_MoveIntoGroup] AS' 
END
GO
-- exec [org].[prc_User_MoveIntoGroup] 68,'770,643',
ALTER PROCEDURE [org].[prc_User_MoveIntoGroup]
(
    @UserGroupID	int,
    @UserIDList	varchar(max),
    @ErrorID		int OUTPUT
)
AS
BEGIN
    SET NOCOUNT ON
    
    SELECT ParseValue AS UserID INTO #UserList FROM dbo.StringToArray(@UserIDList, ',')
    
    SET @ErrorID = 0
    BEGIN TRANSACTION
	   -- Remove users from original groups
	   DELETE FROM org.UG_U WHERE UserID IN (SELECT UserID FROM #UserList)
	   IF @@ERROR<>0 AND @@TRANCOUNT>0
	   BEGIN
		  SET @ErrorID = @@ERROR
		  ROLLBACK TRANSACTION
		  GOTO Finish
	   END
        
	   -- Move users to new group
	   INSERT INTO org.UG_U (UserGroupID, UserID)
	   SELECT @UserGroupID, ul.UserID FROM #UserList ul
		  
	   IF @@ERROR<>0 AND @@TRANCOUNT>0
	   BEGIN
		  SET @ErrorID = @@ERROR
		  ROLLBACK TRANSACTION
		  GOTO Finish
	   END
        
    COMMIT TRANSACTION
Finish:
    -- Drop temp tables
    DROP TABLE #UserList
    Return @ErrorID
END

GO
