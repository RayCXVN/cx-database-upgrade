SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[log].[prc_EventInfo_ins]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [log].[prc_EventInfo_ins] AS' 
END
GO
ALTER PROCEDURE [log].[prc_EventInfo_ins]
(
	@EventInfoID	int = NULL output,
	@EventID		int,
	@EventKeyID	int,
	@Value		nvarchar(max),
	@cUserid		int,
	@Log			smallint = 1
)
AS
BEGIN
	SET NOCOUNT ON
	DECLARE @Err Int

	INSERT INTO [log].[EventInfo]
	(
		[EventID],
		[EventKeyID],
		[Value],
		[Created]
	)
	VALUES
	(
		@EventID,
		@EventKeyID,
		@Value,
		getdate()
	)

	Set @Err = @@Error
	Set @EventInfoID = scope_identity()
	
	IF @Log = 1 
	BEGIN 
		INSERT INTO [Log].[AuditLog] ( UserId, TableName, Type, Data, Created) 
		SELECT @cUserid,'EventInfo',0,
		( SELECT * FROM [log].[EventInfo]
			WHERE
			[EventInfoID] = @EventInfoID FOR XML AUTO) as data,
				getdate() 
	END

	RETURN @Err
END

GO
