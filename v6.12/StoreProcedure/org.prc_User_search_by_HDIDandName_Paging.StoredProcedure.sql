SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[org].[prc_User_search_by_HDIDandName_Paging]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [org].[prc_User_search_by_HDIDandName_Paging] AS' 
END
GO
/*
    Steve - Sept 22 2016 - EntityStatus for table UserGroup
    2016-10-10 Ray:     Remove column Status from org.User, org.Department, RDB.dbo.Result
    2018-07-12 Ray:     Replace UG_U by UGMember
*/
ALTER PROCEDURE [org].[prc_User_search_by_HDIDandName_Paging](
    @HDID                 int,
    @SearchString         nvarchar(32),
    @UserTypeIDList       varchar(max),
    @DeniedUserTypeIDList varchar(max),
    @RowCount             int OUTPUT,
    @RowIndex             int,
    @PageSize             int,
    @SearchInOwnedGroup   bit          = 0,
    @GroupOwnerUserID     int          = 0,
    @UserGroupIDList      varchar(max) = '',
    @CustomerID           int          = 0
) AS
BEGIN
    SET NOCOUNT ON;
    DECLARE @Err int, @ToRow int;
    DECLARE @ActiveEntityStatusID int= (SELECT [EntityStatusID] FROM [EntityStatus] WHERE [CodeName] = 'Active');

    SELECT ROW_NUMBER() OVER(ORDER BY [u].[FirstName], [u].[LastName]) AS [RowNum], [u].[UserID], [u].[DepartmentID], [u].[LanguageID], [u].[UserName],
           [u].[Password], [u].[LastName], [u].[FirstName], [u].[Email], [u].[ExtID], [u].[SSN], [u].[Created], [u].[Mobile], [u].[Tag], [u].[Locked],
           [u].[Ownerid], [u].[ChangePassword], [d].[Name] AS [DepartmentName], [u].[Gender], [u].[EntityStatusID]
    INTO [#SearchResult]
    FROM [org].[User] [u]
    JOIN [org].[H_D] [hd] ON [hd].[DepartmentID] = [u].[DepartmentID]
     AND (@HDID = 0 OR ([hd].path LIKE '%\'+CAST(@HDID AS varchar(16))+'\%' AND [hd].[deleted] = 0)
         )
    JOIN [org].[Department] [d] ON [d].[DepartmentID] = [hd].[DepartmentID] AND [d].[EntityStatusID] = @ActiveEntityStatusID AND [d].[Deleted] IS NULL
     AND (@HDID <> 0
          OR EXISTS (SELECT 1 FROM [org].[Department] cd
                              JOIN [org].[Customer] c ON c.[CustomerID] = cd.[CustomerID] AND [c].[CustomerID] = @CustomerID AND cd.[DepartmentID] = hd.[DepartmentID]
                    )
         )
    WHERE (([u].[FirstName] LIKE N'%'+@SearchString+'%')
           OR ([u].[FirstName]+' '+[u].[LastName] LIKE N'%'+@SearchString+'%')
           OR ([u].[LastName] LIKE N'%'+@SearchString+'%')
           OR ([u].[UserName] LIKE N'%'+@SearchString+'%')
           OR ([u].[Mobile] LIKE N'%'+@SearchString+'%')
          )
      AND [u].[EntityStatusID] = @ActiveEntityStatusID AND [u].[Deleted] IS NULL
      AND EXISTS (SELECT 1 FROM [org].[UT_U] [utu]
                           JOIN (SELECT [ParseValue] AS [UserTypeID] FROM [dbo].[StringToArray] (@UserTypeIDList, ',') ) AS ut
                             ON ut.[UserTypeID] = utu.[UserTypeID] AND [utu].[UserID] = [u].[UserID]
                 )
      AND NOT EXISTS (SELECT 2 FROM [org].[UT_U] [utu2]
                               JOIN (SELECT [ParseValue] AS [UserTypeID] FROM [dbo].[StringToArray] (@DeniedUserTypeIDList, ',') ) AS ut2
                                 ON ut2.[UserTypeID] = utu2.[UserTypeID] AND [utu2].[UserID] = [u].[UserID]
                     )
      AND ( (@SearchInOwnedGroup = 0 AND @UserGroupIDList = '')
           OR EXISTS (SELECT 3 FROM [org].[UGMember] [ugm]
                               JOIN [org].[UserGroup] [ug] ON [ug].[UserGroupID] = [ugm].[UserGroupID] AND [ug].[UserID] = @GroupOwnerUserID
                                AND [ug].[EntityStatusID] = @ActiveEntityStatusID AND [ug].[Deleted] IS NULL
                                AND [ugm].[EntityStatusID] = @ActiveEntityStatusID AND [ugm].[Deleted] IS NULL
                                AND [ugm].[UserID] = [u].[UserID]
                              WHERE (@UserGroupIDList = '' OR EXISTS (SELECT 4 FROM [dbo].[StringToArray] (@UserGroupIDList, ',') WHERE [ParseValue] = [ug].[UserGroupID])
                                    )
                     )
          )
    ORDER BY [u].[FirstName], [u].[LastName];

    SELECT @RowCount = COUNT([UserID]) FROM [#SearchResult];

    IF @RowIndex > @RowCount
    BEGIN
        RETURN;
    END;

    SET @ToRow = @RowIndex + @PageSize - 1;
    IF @ToRow > @RowCount
    BEGIN
        SET @ToRow = @RowCount;
    END;

    SELECT [RowNum], [UserID], [DepartmentID], [LanguageID], [UserName], [Password], [LastName], [FirstName], [Email], [ExtID], [SSN], [Created], [Mobile], [Tag],
           [Locked], [Ownerid], [ChangePassword], [DepartmentName], [Gender], [EntityStatusID]
    FROM [#SearchResult]
    WHERE [RowNum] BETWEEN @RowIndex AND @ToRow;

    SET @Err = @@Error;

    RETURN @Err;
END;
GO