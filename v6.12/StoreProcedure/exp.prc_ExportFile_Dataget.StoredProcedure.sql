SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[exp].[prc_ExportFile_Dataget]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [exp].[prc_ExportFile_Dataget] AS' 
END
GO
ALTER PROCEDURE [exp].[prc_ExportFile_Dataget]
(
	@ExportFileID	int
)
As
BEGIN
	SET NOCOUNT ON
	DECLARE @Err Int

	SELECT Data
	FROM [exp].Exportfiles
	WHERE [ExportFileID] = @ExportFileID

	Set @Err = @@Error

	RETURN @Err
End

GO
