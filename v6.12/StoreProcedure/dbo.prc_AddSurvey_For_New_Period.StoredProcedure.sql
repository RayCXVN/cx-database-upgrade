SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[prc_AddSurvey_For_New_Period]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [dbo].[prc_AddSurvey_For_New_Period] AS' 
END
GO
/* 
	2017-02-21 Steve: Add comlumn Tag when inserting survey, and UserID when inserting Batch
*/
ALTER PROCEDURE [dbo].[prc_AddSurvey_For_New_Period]
(
    @PID                            int,
    @PID2                           int,
    @ReportServer                   nvarchar(64),
    @ReportDB                       nvarchar(64),	
    @OLAPServer                     nvarchar(64),
    @OLAPDB                         nvarchar(64),
    @AllowMultiBatchPerDepartment   int = 0
)
AS
BEGIN
    INSERT at.Survey(ActivityID, HierarchyID, StartDate, EndDate, Anonymous, Status, ShowBack, LanguageID, ButtonPlacement, UsePageNo, LinkURL, FinishURL, CreateResult,  ReportDB, ReportServer, StyleSheet, Type, ReProcessOLAP, No, OLAPServer, OLAPDB, PeriodID, ProcessCategorys, DeleteResultOnUserDelete, FromSurveyID, [ExtId], [Tag])
    SELECT ActivityID, HierarchyID, StartDate, EndDate, Anonymous, Status, ShowBack, LanguageID, ButtonPlacement, UsePageNo, LinkURL, FinishURL, CreateResult, ISNULL( @ReportDB,ReportDB), ISNULL(@ReportServer,ReportServer), StyleSheet, Type, 0, No, iSNULL(@OLAPServer,OLAPServer),ISNULL( @OLAPDB,OLAPDB), @PID2, ProcessCategorys, DeleteResultOnUserDelete, SurveyID, [ExtId], 
		   CASE WHEN ISNUMERIC([Tag]) = 1 THEN [Tag] + 1 ELSE N'' END
    FROM at.Survey 
    WHERE PeriodID=@PID

    INSERT at.LT_Survey(LanguageID, SurveyID, Name, Description, Info, FinishText, DisplayName)
    SELECT l.LanguageID, s2.SurveyID, Name, Description, Info, FinishText, DisplayName from at.LT_Survey l
    INNER JOIN at.Survey s on l.SurveyID=s.SurveyID
    INNER JOIN at.Survey s2 on s.ActivityID=s2.ActivityID and s.StartDate=s2.StartDate AND s2.FromSurveyID = s.SurveyID
    WHERE s.PeriodID=@PID and s2.PeriodID=@PID2

    IF @AllowMultiBatchPerDepartment = 0
    BEGIN
        DECLARE @CreatedDate datetime = GETDATE()

        INSERT at.Batch(SurveyID, UserID, DepartmentID, Name, No, StartDate, EndDate, [Status], MailStatus, [ExtID])
        SELECT s2.SurveyID, b.UserID, b.DepartmentID, d.Name, MAX(b.No), @CreatedDate, @CreatedDate, b.[Status], b.MailStatus, MAX(b.[ExtID]) FROM at.Batch b
        INNER JOIN at.Survey s on b.SurveyID=s.SurveyID
        INNER JOIN at.Survey s2 on s.ActivityID=s2.ActivityID and s.StartDate=s2.StartDate
        INNER JOIN org.Department d ON b.DepartmentID = d.DepartmentID
        WHERE s.PeriodID=@PID and s2.PeriodID=@PID2
          AND NOT EXISTS (SELECT 1 FROM at.Batch b1 WHERE b1.SurveyID=s2.SurveyID AND b1.DepartmentID=b.DepartmentID)
        GROUP BY s2.SurveyID, b.UserID, b.DepartmentID, d.Name, b.[Status], b.MailStatus
    END
    ELSE
    BEGIN
        INSERT at.Batch(SurveyID, UserID, DepartmentID, Name, No, StartDate, EndDate, Status, MailStatus, [ExtID])
        SELECT s2.SurveyID, b.UserID, b.DepartmentID, b.Name, b.No, b.StartDate, b.EndDate, b.Status, b.MailStatus, b.[ExtID] from at.Batch b
        INNER JOIN at.Survey s on b.SurveyID=s.SurveyID
        INNER JOIN at.Survey s2 on s.ActivityID=s2.ActivityID and s.StartDate=s2.StartDate
        WHERE s.PeriodID=@PID and s2.PeriodID=@PID2
    END

    UPDATE at.Survey
    SET StartDate=DATEADD(yy,1, StartDate),
        EndDate=DATEADD(yy,1, EndDate)
    WHERE PeriodID=@PID2

    UPDATE at.Batch
    SET StartDate=s.StartDate,
        EndDate=s.EndDate
    FROM at.Batch b
    INNER JOIN at.Survey s on b.SurveyID=s.SurveyID AND s.[ExtId] != 'Global'
    WHERE s.PeriodID=@PID2

    UPDATE b SET b.[StartDate] = p.[Startdate], b.[EndDate] = p.[Enddate]
    FROM [at].[Batch] b JOIN [at].[Survey] s ON b.[SurveyID] = s.[SurveyID] AND s.[PeriodID] = @PID2 AND s.[ExtId] = 'Global'
    JOIN [at].[Period] p ON s.[PeriodID] = p.[PeriodID]

    UPDATE at.LT_Survey
    SET Name = dbo.IncreaseNumberInText (l.[Name], 4, 1900),
        DisplayName = dbo.IncreaseNumberInText (l.[DisplayName], 4, 1900)
    FROM at.LT_Survey l
    INNER JOIN at.Survey s on l.SurveyID=s.SurveyID
    WHERE s.PeriodID=@PID2

	INSERT INTO [at].[Access] (SurveyID,RoleID,DepartmentID,DepartmentTypeID,CustomerID,PageID,QuestionID,Type,Mandatory, [HDID])
	SELECT B.SurveyID, A.RoleID, A.DepartmentID, A.DepartmentTypeID, A.CustomerID, A.PageID, A.QuestionID, A.Type, A.Mandatory, A.HDID
	FROM at.Survey B join
	(select * FROM at.Access where SurveyID in (select SurveyID from at.Survey where PeriodID = @PID)) A
	ON B.FromSurveyID = A.SurveyID
	AND B.PeriodID = @PID2
END

GO
