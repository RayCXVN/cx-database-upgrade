SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[at].[prc_ScoreTemplateGroup_ins]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [at].[prc_ScoreTemplateGroup_ins] AS' 
END
GO

ALTER PROCEDURE [at].[prc_ScoreTemplateGroup_ins]
(
	@STGroupID int = null output,
	@ActivityID int = null,
	@XCID int = null,
	@cUserid int,
	@Log smallint = 1
)
AS
BEGIN
	SET NOCOUNT ON
	DECLARE @Err Int

	INSERT INTO [at].[ScoreTemplateGroup]
	(
		[ActivityID],
		[XCID]
	)
	VALUES
	(
		@ActivityID,
		@XCID
	)

	Set @Err = @@Error
	Set @STGroupID = scope_identity()

	IF @Log = 1 
	BEGIN 
		INSERT INTO [Log].[AuditLog] ( UserId, TableName, Type, Data, Created) 
		SELECT @cUserid,'ScoreTemplateGroup',0,
		( SELECT * FROM [at].[ScoreTemplateGroup] 
			WHERE
			[ScoreTemplateGroupID] = @STGroupID				 FOR XML AUTO) as data,
				getdate() 
	 END

	RETURN @Err
END

/****** Object:  StoredProcedure [at].[prc_ScoreTemplateGroup_upd]    Script Date: 12/02/2010 13:40:05 ******/
SET ANSI_NULLS ON

GO
