SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[org].[prc_AddTo_UserGroup]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [org].[prc_AddTo_UserGroup] AS' 
END
GO
ALTER PROCEDURE [org].[prc_AddTo_UserGroup]
(
    @Result                 int out,
    @UserGroupIds           varchar(max),
    @UserIds                varchar(max),
    @RemoveFromGroupIDList  varchar(max)
)
AS
BEGIN
    SET NOCOUNT ON    
    SET @Result = 0
    BEGIN TRANSACTION
    
       DECLARE @tbUserGroupId table (UserGroupId int )
       DECLARE @tbUserId table (UserId int)

       INSERT INTO @tbUserGroupId(UserGroupId)
       SELECT ParseValue FROM dbo.StringToArray(@UserGroupIds, ',')
       IF @@ERROR<>0 AND @@TRANCOUNT>0
       BEGIN
          SET @Result = @@ERROR
          ROLLBACK TRANSACTION
          GOTO Finish
       END
       
       INSERT INTO @tbUserId(UserId)
       SELECT ParseValue FROM dbo.StringToArray(@UserIds, ',')    
       IF @@ERROR<>0 AND @@TRANCOUNT>0
       BEGIN
          SET @Result = @@ERROR
          ROLLBACK TRANSACTION
          GOTO Finish
       END
    
       DELETE FROM org.UG_U
       WHERE UserID IN (SELECT UserId FROM @tbUserId)
         AND UserGroupID IN (SELECT ParseValue FROM dbo.StringToArray(@RemoveFromGroupIDList, ','))
       IF @@ERROR<>0 AND @@TRANCOUNT>0
       BEGIN
          SET @Result = @@ERROR
          ROLLBACK TRANSACTION
          GOTO Finish
       END
    
       SELECT UserGroupId, UserId
       INTO #UG_U
       FROM @tbUserGroupId, @tbUserId
       IF @@ERROR<>0 AND @@TRANCOUNT>0
       BEGIN
          SET @Result = @@ERROR
          ROLLBACK TRANSACTION
          GOTO Finish
       END
    
       -- Add user to groups if user doesn't exist in groups.
       INSERT INTO org.UG_U(UserGroupID, UserID)
       SELECT localUgu.UserGroupId, localUgu.UserId    
       FROM #UG_U localUgu
       WHERE NOT EXISTS (SELECT 1 FROM org.UG_U ugu
                 WHERE ugu.UserGroupID = localUgu.UserGroupID AND ugu.UserID = localUgu.UserID)
       IF @@ERROR<>0 AND @@TRANCOUNT>0
       BEGIN
          SET @Result = @@ERROR
          ROLLBACK TRANSACTION
          GOTO Finish
       END

    COMMIT TRANSACTION
Finish:
    -- Drop temp tables
    DROP TABLE #UG_U
    RETURN @Result
END

GO
