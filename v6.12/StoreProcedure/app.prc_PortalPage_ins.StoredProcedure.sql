SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[app].[prc_PortalPage_ins]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [app].[prc_PortalPage_ins] AS' 
END
GO
/*
	2017-06-13 Steve: Change the length of column ExtID in all related tables from xAPI format
*/
ALTER PROCEDURE [app].[prc_PortalPage_ins]
(
	@PortalPageID int = null output,
	@OwnerID int,
	@No smallint,
	@Parameter nvarchar(1024) = '',
	@cUserid int,
	@Log smallint = 1,
	@Created DATETIME = NULL,
	@ExtID nvarchar(256)=''
)
AS
BEGIN
	SET NOCOUNT ON
	DECLARE @Err Int

	SET @Created = ISNULL(@Created, GETDATE())

	INSERT INTO [app].[PortalPage]
	(
		[OwnerID],
		[No],
		[Parameter],
		[Created],
		[ExtID]
	)
	VALUES
	(
		@OwnerID,
		@No,
		@Parameter,
		@Created,
		@ExtID
	)

	Set @Err = @@Error
	Set @PortalPageID = scope_identity()

	IF @Log = 1 
	BEGIN 
		INSERT INTO [Log].[AuditLog] ( UserId, TableName, Type, Data, Created) 
		SELECT @cUserid,'PortalPage',0,
		( SELECT * FROM [app].[PortalPage] 
			WHERE
			[PortalPageID] = @PortalPageID				 FOR XML AUTO) as data,
				getdate() 
	 END

	RETURN @Err
END

GO
