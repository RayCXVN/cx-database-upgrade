SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[org].[prc_User_Search_All]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [org].[prc_User_Search_All] AS' 
END
GO
/*  2016-10-10 Ray: Remove column Status from org.User, org.Department, RDB.dbo.Result
	2016-11-07 Johnny: Display user not duplicate
*/
ALTER PROCEDURE [org].[prc_User_Search_All]
(  
 @HierarchyID int,  
 @SearchString nvarchar(32)  
)  
As  
BEGIN  
 SET NOCOUNT ON  
 DECLARE @Err Int  
  
 SELECT  
 u.[UserID],  
 u.[DepartmentID],  
 u.[LanguageID],  
 u.[UserName],  
 u.[Password],  
 u.[LastName],  
 u.[FirstName],  
 u.[Email],  
 u.[ExtID],  
 u.[SSN],  
 u.[Created],  
 u.[Mobile],  
 u.[Tag],  
 u.Locked,  
 u.Gender,
 u.DateOfBirth,
 u.ChangePassword,  
 u.[HashPassword],  
 u.[SaltPassword],  
 u.[OneTimePassword],  
 u.[OTPExpireTime],  
 u.CountryCode,
 u.EntityStatusID,
 u.Deleted,
 u.EntityStatusReasonID
 FROM org.[User] u  
 JOIN org.[H_D] hd on hd.DepartmentID = u.DepartmentID and hd.[HierarchyID] = @HierarchyID
 WHERE (u.[UserName] like '%'+ @SearchString + '%')  
 OR (u.[FirstName] like '%'+ @SearchString + '%')  
 OR (u.[FirstName] + ' ' + u.[LastName]  like '%'+ @SearchString + '%')  
 OR (u.[LastName] like '%'+ @SearchString + '%')  
 OR (u.[Email] like '%'+ @SearchString + '%')  
 OR (u.[UserID] like '%'+ @SearchString + '%')  
 OR (u.[ExtID] like '%'+ @SearchString + '%')  
 OR (u.[Mobile] like '%'+ @SearchString + '%')  
 OR (u.[SSN] like '%'+ @SearchString + '%')
 OR EXISTS (SELECT 1 FROM app.LoginService_User lsu WHERE u.UserID=lsu.UserID AND lsu.PrimaryClaimValue like '%'+ @SearchString + '%')
 SET @Err = @@Error  
  
 RETURN @Err  
END

GO
