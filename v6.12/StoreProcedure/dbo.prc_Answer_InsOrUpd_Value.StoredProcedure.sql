SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[prc_Answer_InsOrUpd_Value]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [dbo].[prc_Answer_InsOrUpd_Value] AS' 
END
GO
ALTER PROCEDURE [dbo].[prc_Answer_InsOrUpd_Value]  
(  
 @AnswerID bigint = null output,  
 @ResultID bigint,  
 @QuestionID int,  
 @AlternativeID int,  
 @Value float
)  
As  
BEGIN
SET NOCOUNT ON
  
DECLARE @Err Int=0


SELECT TOP 1 @AnswerID = AnswerID
FROM Answer
WHERE ResultID = @ResultID
AND QuestionID = @QuestionID AND AlternativeID = @AlternativeID; 
IF(@AnswerID is not null)
begin
	UPDATE [Answer]
	SET	[Value] = @Value
	WHERE [AnswerID] = @AnswerID
end
else  
begin
	INSERT INTO [Answer] ([ResultID], [QuestionID], [AlternativeID], [Value], [Free], [No])
	VALUES (@ResultID, @QuestionID, @AlternativeID, @Value, '', 0)
	SET @AnswerID = SCOPE_IDENTITY()  
	
end
SET @Err = @@Error  
RETURN @Err  
End
  
  


GO
