SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[app].[prc_Tab_ins]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [app].[prc_Tab_ins] AS' 
END
GO
ALTER PROCEDURE [app].[prc_Tab_ins]
	@TabID INT = null output,
	@SiteID int,
	@MenuItemID INT=NULL,
	@Settings XML=NULL,
	@cUserid int,
    @Log smallint = 1
AS
BEGIN
	SET NOCOUNT ON;
	DECLARE @Err Int
    
    INSERT into [app].[Tab](
      [SiteID]
      ,[MenuItemID]
      ,[Settings])
      
    VALUES
      (@SiteID
      ,@MenuItemID
      ,@Settings)
	
    Set @Err = @@Error
    Set @TabID = scope_identity()
	IF @Log = 1 
	BEGIN 
		INSERT INTO [Log].[AuditLog] ( UserId, TableName, Type, Data, Created) 
		SELECT @cUserid,'Tab',1,
		( SELECT * FROM [app].[Tab]
			WHERE
			[TabID] = @TabID				 FOR XML AUTO) as data,
				getdate() 
	END
	
	RETURN @Err       
END

GO
