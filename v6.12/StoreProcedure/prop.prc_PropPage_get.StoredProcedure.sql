SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[prop].[prc_PropPage_get]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [prop].[prc_PropPage_get] AS' 
END
GO
ALTER PROCEDURE [prop].[prc_PropPage_get]
(
	@TableTypeID int
)
AS
BEGIN
	SET NOCOUNT ON
	DECLARE @Err Int

	SELECT
	[PropPageID], 
	[TableTypeID], 
	[Type], 
	[No], 
	[Created]
	FROM [prop].[PropPage]
	WHERE
	[TableTypeID] = @TableTypeID
	ORDER BY [No]

	Set @Err = @@Error

	RETURN @Err
END
GO
