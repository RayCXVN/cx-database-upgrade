SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[at].[prc_StatusLog_ins]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [at].[prc_StatusLog_ins] AS' 
END
GO

ALTER PROCEDURE [at].[prc_StatusLog_ins]
(
	@StatusLogID int = null output,
	@StatusTypeID int,
	@ResultID bigint = null,
	@CVID int = null,
	@UserID int,
	@Comment ntext,
	@cUserid int,
	@Log smallint = 1
)
AS
BEGIN
	SET NOCOUNT ON
	DECLARE @Err Int

	INSERT INTO [at].[StatusLog]
	(
		[StatusTypeID],
		[ResultID],
		[CVID],
		[UserID],
		[Comment]
	)
	VALUES
	(
		@StatusTypeID,
		@ResultID,
		@CVID,
		@UserID,
		@Comment
	)

	Set @Err = @@Error
	Set @StatusLogID = scope_identity()

	IF @Log = 1 
	BEGIN 
		INSERT INTO [Log].[AuditLog] ( UserId, TableName, Type, Data, Created) 
		SELECT @cUserid,'StatusLog',0,
		( SELECT * FROM [at].[StatusLog] 
			WHERE
			[StatusLogID] = @StatusLogID				 FOR XML AUTO) as data,
				getdate() 
	 END

	RETURN @Err
END


GO
