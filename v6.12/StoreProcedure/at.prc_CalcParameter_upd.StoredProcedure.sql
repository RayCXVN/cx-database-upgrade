SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[at].[prc_CalcParameter_upd]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [at].[prc_CalcParameter_upd] AS' 
END
GO
ALTER PROCEDURE  [at].[prc_CalcParameter_upd]  
(  
 @CalcParameterID int,  
 @CFID int,  
 @No smallint,  
 @Name varchar(32),  
 @QuestionID INT=NULL,  
 @AlternativeID INT=NULL,  
 @CategoryID INT=NULL,  
 @CalcType smallint,  
 @Format nvarchar(32),  
 @cUserid int,  
 @Log smallint = 1,
 @ItemID int = NULL
)  
AS  
BEGIN  
 SET NOCOUNT ON  
 DECLARE @Err Int  
  
 UPDATE [at].[CalcParameter]  
 SET  
  [CFID] = @CFID,  
  [No] = @No,  
  [Name] = @Name,  
  [QuestionID] = @QuestionID,  
  [AlternativeID] = @AlternativeID,  
  [CategoryID] = @CategoryID,  
  [CalcType] = @CalcType,  
  [Format] = @Format,
  [ItemID]  = @ItemID
 WHERE  
  [CalcParameterID] = @CalcParameterID  
  
 IF @Log = 1   
 BEGIN   
  INSERT INTO [Log].[AuditLog] ( UserId, TableName, Type, Data, Created)   
  SELECT @cUserid,'CalcParameter',1,  
  ( SELECT * FROM [at].[CalcParameter]   
   WHERE  
   [CalcParameterID] = @CalcParameterID    FOR XML AUTO) as data,  
   getdate()  
 END  
  
 Set @Err = @@Error  
  
 RETURN @Err  
END  
  

GO
