SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[at].[prc_SentMail_upd]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [at].[prc_SentMail_upd] AS' 
END
GO

ALTER PROCEDURE [at].[prc_SentMail_upd]
(
	@SentMailID int,
	@MailID int,
	@LanguageID int,
	@Type smallint,
	@From nvarchar(256),
	@Bcc nvarchar(max),
	@StatusMail nvarchar(max),
	@Status smallint,
	@Sent SMALLDATETIME=NULL,
	@cUserid int,
	@Log smallint = 1
)
AS
BEGIN
	SET NOCOUNT ON
	DECLARE @Err Int

	UPDATE [at].[SentMail]
	SET
		[MailID] = @MailID,
		[LanguageID] = @LanguageID,
		[Type] = @Type,
		[From] = @From,
		[Bcc] = @Bcc,
		[StatusMail] = @StatusMail,
		[Status] = @Status,
		[Sent] = @Sent
	WHERE
		[SentMailID] = @SentMailID

	IF @Log = 1 
	BEGIN 
		INSERT INTO [Log].[AuditLog] ( UserId, TableName, Type, Data, Created) 
		SELECT @cUserid,'SentMail',1,
		( SELECT * FROM [at].[SentMail] 
			WHERE
			[SentMailID] = @SentMailID			 FOR XML AUTO) as data,
			getdate()
	END

	Set @Err = @@Error

	RETURN @Err
END


GO
