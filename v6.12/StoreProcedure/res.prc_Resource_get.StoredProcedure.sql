SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[res].[prc_Resource_get]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [res].[prc_Resource_get] AS' 
END
GO
ALTER PROCEDURE [res].[prc_Resource_get]    
(
	@CategoryID as int
)
AS    
BEGIN    
 SET NOCOUNT ON;    
 DECLARE @Err Int    

    SELECT     
	  [ResourceId]
      ,[MediaTypeId]
      ,[CategoryId]
      ,[URL]
      ,[StatusID]
      ,[Deleted]
      ,[ValidTo]
      ,[ValidFrom]
      ,[Active]
      ,[ExtId]
      ,[Updated]
      ,[Created]
      ,[AuthorName]
	  ,[Author]
      ,[CreatedBy]
      ,[No]
      ,[ShowAuthor]
      ,[ShowTopBar]
      ,[ShowCompleteCheckbox]
      ,[IsExternal]
      ,[ShowPublishedDate]
      ,[ShowNote]
      
 FROM [res].[Resource]    
 WHERE [CategoryID] = @CategoryID
 ORDER BY [No]

 Set @Err = @@Error    

 RETURN @Err    

END

GO
