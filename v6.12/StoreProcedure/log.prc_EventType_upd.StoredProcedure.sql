SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[log].[prc_EventType_upd]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [log].[prc_EventType_upd] AS' 
END
GO
ALTER PROCEDURE [log].[prc_EventType_upd]
(
	@EventTypeID	int,
	@ParentID		INT=NULL,
	@CodeName		varchar(32),
	@OwnerID		int,
	@No				smallint,
	@cUserid		int,
	@Log			smallint = 1,
	@Active			bit =1
)
AS
BEGIN
	SET NOCOUNT ON
	DECLARE @Err Int

	UPDATE [log].[EventType]
	SET
		[ParentID] = @ParentID,
		[CodeName] = @CodeName,
		[OwnerID] = @OwnerID,
		[No] = @No,
		[Active]=@Active
	WHERE
		[EventTypeID] = @EventTypeID

	Set @Err = @@Error
	
	IF @Log = 1 
	BEGIN 
		INSERT INTO [Log].[AuditLog] ( UserId, TableName, Type, Data, Created) 
		SELECT @cUserid,'EventType',1,
		( SELECT * FROM [log].[EventType]
			WHERE
			[EventTypeID] = @EventTypeID FOR XML AUTO) as data,
				getdate() 
	END

	RETURN @Err
END

GO
