SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[at].[prc_Activity_get_WithBatchCount]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [at].[prc_Activity_get_WithBatchCount] AS' 
END
GO
ALTER PROCEDURE [at].[prc_Activity_get_WithBatchCount](
    @OwnerID int)
AS
BEGIN
    SET NOCOUNT ON;
    DECLARE @Err int;

    SELECT [ActivityID], [LanguageID], [OwnerID], [StyleSheet], [Type], [No], [TooltipType], [Listview], [Descview], [Chartview], [OLAPServer], [OLAPDB],
           [Created], [SelectionHeaderType], [ExtID], [UseOLAP], [ArchetypeID],
           (SELECT COUNT(b.[BatchID])
            FROM [at].[Batch] b
            JOIN [at].[Survey] s ON s.[SurveyID] = b.[SurveyID] AND s.[ActivityID] = [act].[ActivityID]) AS [BatchCount]
    FROM [at].[Activity] [act]
    WHERE [OwnerID] = @OwnerID
    ORDER BY [No];

    SET @Err = @@Error;

    RETURN @Err;
END;
GO