SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[at].[prc_XC_A_del]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [at].[prc_XC_A_del] AS' 
END
GO


ALTER PROCEDURE [at].[prc_XC_A_del]
(
	@XCID int,
	@ActivityID int,
	@cUserid int,
	@Log smallint = 1
)
AS
BEGIN
	SET NOCOUNT ON
	DECLARE @Err Int

	IF @Log = 1 
	BEGIN 
		INSERT INTO [Log].[AuditLog] ( UserId, TableName, Type, Data, Created) 
		SELECT @cUserid,'XC_A',2,
		( SELECT * FROM [at].[XC_A] 
			WHERE
			[XCID] = @XCID AND
			[ActivityID] = @ActivityID
			 FOR XML AUTO) as data,
			getdate() 
	END 


	DELETE FROM [at].[XC_A]
	WHERE
		[XCID] = @XCID AND
		[ActivityID] = @ActivityID

	Set @Err = @@Error

	RETURN @Err
END


GO
