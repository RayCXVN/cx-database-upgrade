SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[prc_GUIText_get]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [dbo].[prc_GUIText_get] AS' 
END
GO
ALTER PROCEDURE [dbo].[prc_GUIText_get]
(
	@LanguageID	int
)
As
BEGIN
	SET NOCOUNT ON
	DECLARE @Err Int

	Select
	[LanguageID],
	[ItemID],
	[Text]
	FROM [GUIText]
	WHERE LanguageID = @LanguageID
	ORDER BY ItemID
	Set @Err = @@Error

	RETURN @Err
End



GO
