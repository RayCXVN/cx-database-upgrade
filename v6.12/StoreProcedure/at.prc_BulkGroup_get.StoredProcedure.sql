SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[at].[prc_BulkGroup_get]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [at].[prc_BulkGroup_get] AS' 
END
GO
ALTER PROCEDURE [at].[prc_BulkGroup_get]
(
	@SurveyID int
)
AS
BEGIN
	SET NOCOUNT ON
	DECLARE @Err Int

	SELECT
	[BulkGroupID],
	ISNULL([ActivityID], 0) AS 'ActivityID',
	ISNULL([SurveyID], 0) AS 'SurveyID',
	ISNULL([No], 0) AS 'No',
	ISNULL([Css], '') AS 'Css',
	ISNULL([Icon], '') AS 'Icon',
	ISNULL([Tag], '') AS 'Tag'
	FROM [at].[BulkGroup]
	WHERE
	[SurveyID] = @SurveyID
	ORDER BY [No]

	Set @Err = @@Error

	RETURN @Err
END



GO
