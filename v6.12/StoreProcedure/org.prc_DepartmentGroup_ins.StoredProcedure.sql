SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[org].[prc_DepartmentGroup_ins]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [org].[prc_DepartmentGroup_ins] AS' 
END
GO
/*  
	2017-06-13 Steve: Change the length of column ExtID in all related tables from xAPI format
*/
ALTER PROCEDURE [org].[prc_DepartmentGroup_ins]
(
	@DepartmentGroupID int = null output,
	@OwnerID int,
	@ExtID nvarchar(256),
	@No smallint,
	@cUserid int,
	@Log smallint = 1
)
AS
BEGIN
	SET NOCOUNT ON
	DECLARE @Err Int

	INSERT INTO [org].[DepartmentGroup]
	(
		[OwnerID],
		[ExtID],
		[No]
	)
	VALUES
	(
		@OwnerID,
		@ExtID,
		@No
	)

	Set @Err = @@Error
	Set @DepartmentGroupID = scope_identity()

	IF @Log = 1 
	BEGIN 
		INSERT INTO [Log].[AuditLog] ( UserId, TableName, Type, Data, Created) 
		SELECT @cUserid,'DepartmentGroup',0,
		( SELECT * FROM [org].[DepartmentGroup] 
			WHERE
			[DepartmentGroupID] = @DepartmentGroupID				 FOR XML AUTO) as data,
				getdate() 
	 END

	RETURN @Err
END

GO
