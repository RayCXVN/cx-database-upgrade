SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[rep].[prc_URS_CalcType_del]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [rep].[prc_URS_CalcType_del] AS' 
END
GO
ALTER PROCEDURE [rep].[prc_URS_CalcType_del]       
(        
 @UserReportSettingID int,
 @ReportCalcTypeID int,
 @cUserid int,
 @Log smallint = 1
)        
AS        
BEGIN        
 SET NOCOUNT ON        
 DECLARE @Err Int        
        
 IF @Log = 1         
 BEGIN         
  INSERT INTO [Log].[AuditLog] ( UserId, TableName, Type, Data, Created)         
  SELECT @cUserid,'URS_CalcType',2,        
  (SELECT * FROM [rep].[URS_CalcType]
   WHERE        
		[UserReportSettingID] = @UserReportSettingID
		AND [ReportCalcTypeID] = @ReportCalcTypeID
    FOR XML AUTO) as data,        
   getdate()         
 END         
        
        
 DELETE FROM [rep].[URS_CalcType]
 WHERE        
	[UserReportSettingID] = @UserReportSettingID
	AND [ReportCalcTypeID] = @ReportCalcTypeID
 Set @Err = @@Error        
        
 RETURN @Err        
END 

GO
