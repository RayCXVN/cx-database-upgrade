SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[prc_Result_getByDepartmentID]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [dbo].[prc_Result_getByDepartmentID] AS' 
END
GO
ALTER PROCEDURE [dbo].[prc_Result_getByDepartmentID] 
(
	@DepartmentID	int,
	@PeriodID	int
)
AS
BEGIN
	SET NOCOUNT ON
	DECLARE @Err Int, @QDB_Name NVARCHAR(255), @SqlCommand  NVARCHAR(MAX)
    DECLARE @ActiveEntityStatusID INT = (SELECT EntityStatusID FROM EntityStatus WHERE CodeName='Active')

	SET @QDB_Name = REPLACE(DB_NAME(),'AT6R','AT6Q')
	CREATE TABLE #ResultCount(ID INT IDENTITY(1,1) NOT NULL ,ResultID INT, NumberOfAnswers INT)
	INSERT INTO #ResultCount(ResultID, NumberOfAnswers)
	SELECT r.ResultID, COUNT(a.AnswerID) AS NumberOfAnswers  
	FROM [dbo].[Result] r 
	LEFT JOIN [dbo].[Answer] a ON r.ResultID=a.ResultID
	WHERE DepartmentID=@DepartmentID AND r.EntityStatusID = @ActiveEntityStatusID AND r.Deleted IS NULL
	GROUP BY r.ResultID

	CREATE TABLE #SurveyPeriod(ID INT IDENTITY(1,1) NOT NULL ,SurveyID INT)
	INSERT INTO #SurveyPeriod(SurveyID)
	SELECT SurveyID FROM [at].[Survey]
	WHERE PeriodID = @PeriodID

	CREATE TABLE #Survey(ID INT IDENTITY(1,1) NOT NULL ,SurveyID INT, BatchID INT, DepartmentID INT)
	INSERT INTO #Survey(SurveyID, BatchID, DepartmentID)
	SELECT SurveyID,BatchID,DepartmentID FROM [dbo].[Result] 
	WHERE DepartmentID=@DepartmentID AND Result.EntityStatusID = @ActiveEntityStatusID AND Result.Deleted IS NULL 
	AND SurveyID IN (SELECT SurveyID FROM #SurveyPeriod)
	GROUP BY SurveyID,BatchID,DepartmentID
	
	CREATE TABLE #Result (ID INT IDENTITY(1,1) NOT NULL ,SurveyID INT, BatchID INT, SurveyName NVARCHAR(255), BatchName NVARCHAR(255),ActivityID INT, ActivityName NVARCHAR(255), [Path] NVARCHAR(MAX), DepartmentID INT, DepartmentName NVARCHAR(255))
	INSERT INTO #Result(SurveyID, BatchID, SurveyName, BatchName, ActivityID, ActivityName, [Path], DepartmentID, DepartmentName)
	SELECT S.SurveyID, S.BatchID, LTS.Name AS SurveyName, B.Name AS BatchName, A.ActivityID, LTA.Name AS ActivityName, HD.Path AS Path, S.DepartmentID, D.Name AS DepartmentName 
	FROM #Survey S 
	LEFT JOIN at.Survey SK ON SK.SurveyID=S.SurveyID
	LEFT JOIN at.LT_Survey LTS ON S.SurveyID=LTS.SurveyID
	LEFT JOIN at.Batch B ON S.BatchID=B.BatchID
	LEFT JOIN at.Activity A ON SK.ActivityID=A.ActivityID
	LEFT JOIN at.LT_Activity LTA ON A.ActivityID=LTA.ActivityID
	LEFT JOIN org.H_D HD ON S.DepartmentID=HD.DepartmentID 
	LEFT JOIN org.Department D ON S.DepartmentID=D.DepartmentID
	
	SELECT R.ResultID, R.UserID, R.DepartmentID,RE.DepartmentName, RE.[Path], U.FirstName, U.LastName, RE.ActivityID, RE.ActivityName,R.SurveyID,RE.SurveyName,R.BatchID,RE.BatchName ,r.StatusTypeID AS ResultStatusTypeId, RC.NumberOfAnswers
	FROM [dbo].[Result] R 
	INNER JOIN #ResultCount RC ON R.ResultID= RC.ResultID
	INNER JOIN #Result RE ON R.SurveyID= RE.SurveyID AND R.BatchID=RE.BatchID
	INNER JOIN [org].[User] U ON U.UserID=R.UserID 
	WHERE R.DepartmentID = @DepartmentID AND R.EntityStatusID = @ActiveEntityStatusID AND R.Deleted IS NULL

	DROP TABLE #Result
	DROP TABLE #ResultCount
	DROP TABLE #Survey
	DROP TABLE #SurveyPeriod
	SET @Err = @@Error

	RETURN @Err
END
-------------------------------------------------------------------------------------


GO
