SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[at].[prc_XCategory_upd]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [at].[prc_XCategory_upd] AS' 
END
GO
/*  
	2017-07-27 Steve: Change the length of column ExtID in all related tables from xAPI format
*/
ALTER PROCEDURE [at].[prc_XCategory_upd]
(
	@XCID int,
	@ParentID int,
	@OwnerID int,
	@Type smallint,
	@Status smallint,
	@No smallint,
	@ExtId NVARCHAR(256) = '',
	@cUserid int,
	@Log smallint = 1
)
AS
BEGIN
	SET NOCOUNT ON
	DECLARE @Err Int

	UPDATE [at].[XCategory]
	SET
		[ParentID] = @ParentID,
		[OwnerID] = @OwnerID,
		[Type] = @Type,
		[Status] = @Status,
		[No] = @No,
		[ExtId] =@ExtId
	WHERE
		[XCID] = @XCID

	IF @Log = 1 
	BEGIN 
		INSERT INTO [Log].[AuditLog] ( UserId, TableName, Type, Data, Created) 
		SELECT @cUserid,'XCategory',1,
		( SELECT * FROM [at].[XCategory] 
			WHERE
			[XCID] = @XCID			 FOR XML AUTO) as data,
			getdate()
	END

	Set @Err = @@Error

	RETURN @Err
END


GO
