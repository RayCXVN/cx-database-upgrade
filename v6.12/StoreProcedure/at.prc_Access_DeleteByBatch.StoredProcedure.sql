SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[at].[prc_Access_DeleteByBatch]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [at].[prc_Access_DeleteByBatch] AS' 
END
GO
--DEV: Tom
--Task 22147: Implement the posibility to edit survey/batch in the list after user has ordered the survey/batch
ALTER PROCEDURE [at].[prc_Access_DeleteByBatch]
(
	@BatchID int
)
AS
BEGIN	
	DELETE FROM [at].[Access]
	WHERE [BatchID] = @BatchID	
END

GO
