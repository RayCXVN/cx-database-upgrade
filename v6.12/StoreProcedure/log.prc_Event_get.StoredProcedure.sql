SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[log].[prc_Event_get]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [log].[prc_Event_get] AS' 
END
GO
ALTER PROCEDURE [log].[prc_Event_get]
    @EventID	int
AS
BEGIN
	SET NOCOUNT ON;
	DECLARE @Err Int
	
     SELECT [EventID],
            [EventTypeID],
            [UserID],
            [IPNumber],
            [TableTypeID],
            [ItemID],
            [Created],
            [ApplicationName],
            [DepartmentID],
            [CustomerID],
            [UserData],
			[EventTypeName],
			[ArchetypeID]
    FROM  [log].[Event]
    WHERE [EventID] = @EventID
	
    Set @Err = @@Error

    RETURN @Err
  
END

GO
