SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[org].[prc_User_del]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [org].[prc_User_del] AS' 
END
GO
ALTER PROCEDURE [org].[prc_User_del]  
(  
 @UserID int,  
 @cUserid int,  
 @Log smallint = 1  
)  
AS  
BEGIN  
 SET NOCOUNT ON  
 DECLARE @Err Int  
  
 IF @Log = 1   
 BEGIN   
  INSERT INTO [Log].[AuditLog] ( UserId, TableName, Type, Data, Created)   
  SELECT @cUserid,'User',2,  
  ( SELECT * FROM [org].[User]   
   WHERE  
   [UserID] = @UserID  
    FOR XML AUTO) as data,  
   getdate()   
 END   
  
    UPDATE AT.[Batch] set Userid = NULL where UserID = @Userid  
      
      
    Delete org.ug_u where usergroupid in (Select usergroupid from org.usergroup where userid = @Userid)  
    Delete org.usergroup where userid = @Userid  
    Delete org.U_D where UserID = @Userid
	Delete app.LoginService_User where UserID = @UserId
    Delete note.NoteSubscription WHERE UserID = @UserId
	
 DELETE FROM [org].[User]  
 WHERE  
 [UserID] = @UserID  
  
 Set @Err = @@Error  
  
 RETURN @Err  
END  

GO
