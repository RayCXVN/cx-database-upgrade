SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[at].[prc_DottedRule_get]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [at].[prc_DottedRule_get] AS' 
END
GO
ALTER PROCEDURE [at].[prc_DottedRule_get]
(
	@ActivityID int
)
AS
BEGIN
	SET NOCOUNT ON
	DECLARE @Err Int

	SELECT
	[DottedRuleID],
	[ActivityID],
	ISNULL([SurveyID], 0) AS 'SurveyID',
	[Active],
	[UseOnAllQuestions],
	[UseOnAllCategorys],
	[MinResult],
	[MinResultPercentage],
	[FrequencyDottedType],
	[FrequencyDottedLimit],
	[FrequencyDotZeroValues],
	[FrequencyMinAlternativeCountDotted],
	[AverageDottedType],
	[AverageDottedLimit],
    [OnlyDepartmentsBelow],
    FrequencyMinQuestionCountDotted
	FROM [at].[DottedRule]
	WHERE
	[ActivityID] = @ActivityID

	Set @Err = @@Error

	RETURN @Err
END

GO
