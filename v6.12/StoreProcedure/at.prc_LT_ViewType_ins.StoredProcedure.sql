SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[at].[prc_LT_ViewType_ins]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [at].[prc_LT_ViewType_ins] AS' 
END
GO

ALTER PROCEDURE [at].[prc_LT_ViewType_ins]
(
	@LanguageID int,
	@ViewTypeID int,
	@Name nvarchar(256),
	@Description nvarchar(max),
	@cUserid int,
	@Log smallint = 1
)
AS
BEGIN
	SET NOCOUNT ON
	DECLARE @Err Int

	INSERT INTO [at].[LT_ViewType]
	(
		[LanguageID],
		[ViewTypeID],
		[Name],
		[Description]
	)
	VALUES
	(
		@LanguageID,
		@ViewTypeID,
		@Name,
		@Description
	)

	Set @Err = @@Error

	IF @Log = 1 
	BEGIN 
		INSERT INTO [Log].[AuditLog] ( UserId, TableName, Type, Data, Created) 
		SELECT @cUserid,'LT_ViewType',0,
		( SELECT * FROM [at].[LT_ViewType] 
			WHERE
			[LanguageID] = @LanguageID AND
			[ViewTypeID] = @ViewTypeID				 FOR XML AUTO) as data,
				getdate() 
	 END

	RETURN @Err
END


GO
