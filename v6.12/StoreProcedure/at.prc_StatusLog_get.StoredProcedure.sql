SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[at].[prc_StatusLog_get]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [at].[prc_StatusLog_get] AS' 
END
GO

ALTER PROCEDURE [at].[prc_StatusLog_get]
(
	@CVID int,
	@Resultid bigint
)
AS
BEGIN
	SET NOCOUNT ON
	DECLARE @Err Int

	SELECT
	[StatusLogID],
	[StatusTypeID],
	ISNULL([ResultID], 0) AS 'ResultID',
	ISNULL([CVID], 0) AS 'CVID',
	[UserID],
	[Comment],
	[Created]
	FROM [at].[StatusLog]
	WHERE
	 ([CVID] = @CVID AND @ResultID IS NULL) OR
	 ([Resultid] = @Resultid AND @CVID IS NULL)

	Set @Err = @@Error

	RETURN @Err
END


GO
