SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[at].[prc_LT_MailTemplate_upd]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [at].[prc_LT_MailTemplate_upd] AS' 
END
GO

ALTER PROCEDURE [at].[prc_LT_MailTemplate_upd]
(
	@LanguageID int,
	@MailTemplateID int,
	@Name nvarchar(256),
	@Subject nvarchar(256),
	@Body nvarchar(max),
	@cUserid int,
	@Log smallint = 1
)
AS
BEGIN
	SET NOCOUNT ON
	DECLARE @Err Int

	UPDATE [at].[LT_MailTemplate]
	SET
		[LanguageID] = @LanguageID,
		[MailTemplateID] = @MailTemplateID,
		[Name] = @Name,
		[Subject] = @Subject,
		[Body] = @Body
	WHERE
		[LanguageID] = @LanguageID AND
		[MailTemplateID] = @MailTemplateID

	IF @Log = 1 
	BEGIN 
		INSERT INTO [Log].[AuditLog] ( UserId, TableName, Type, Data, Created) 
		SELECT @cUserid,'LT_MailTemplate',1,
		( SELECT * FROM [at].[LT_MailTemplate] 
			WHERE
			[LanguageID] = @LanguageID AND
			[MailTemplateID] = @MailTemplateID			 FOR XML AUTO) as data,
			getdate()
	END

	Set @Err = @@Error

	RETURN @Err
END


GO
