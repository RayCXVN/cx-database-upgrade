SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[log].[prc_EventCountByEventType]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [log].[prc_EventCountByEventType] AS' 
END
GO
ALTER PROCEDURE [log].[prc_EventCountByEventType]
    @EventTypeID    varchar(max) = '',
    @HDID           int = 0,
    @UserID			varchar(max) = '',
    @FromDate       date = NULL,
    @ToDate         date = NULL,
    @ItemID			int = 0,
    @Level          int = 0 -- Level 0 - From HDID and below; 1 - HDs under given HDID; 2 - Users inside given HDID
AS
BEGIN
    SET NOCOUNT ON
    
    IF @Level = 0
    BEGIN
        SELECT convert(nvarchar, e.EventTypeID) [ID], @HDID [SubID], count(e.EventID) EventCount
        FROM   log.Event e
        WHERE  EXISTS (SELECT 1 FROM org.H_D hd WHERE hd.DepartmentID = e.DepartmentID AND hd.[Path] LIKE '%\' + CONVERT(nvarchar(16),@HDID) + '\%') 
          AND  e.EventTypeID  IN (SELECT Value FROM dbo.funcListToTableInt(@EventTypeID, ','))
          AND (cast(e.Created as date) >= @FromDate OR @FromDate IS NULL)
          AND (cast(e.Created as date) <= @ToDate OR @ToDate IS NULL)
          AND (@ItemID=0 OR e.ItemID =@ItemID)
        GROUP BY e.EventTypeID
    END
    ELSE IF @Level = 1
    BEGIN
        SELECT convert(nvarchar, e.EventTypeID) [ID], hdt.HDID [SubID], count(e.EventID) EventCount
        FROM   log.Event e JOIN org.H_D hd ON hd.DepartmentID = e.DepartmentID
        JOIN   org.H_D hdt ON hd.[Path] LIKE '%\' + CONVERT(nvarchar(16),hdt.HDID) + '\%' AND hdt.ParentID = @HDID
          AND  e.EventTypeID  IN (SELECT Value FROM dbo.funcListToTableInt(@EventTypeID, ','))
          AND (cast(e.Created as date) >= @FromDate OR @FromDate IS NULL)
          AND (cast(e.Created as date) <= @ToDate OR @ToDate IS NULL)
          AND (@ItemID=0 OR e.ItemID =@ItemID)
        GROUP BY e.EventTypeID, hdt.HDID
    END
    ELSE IF @Level = 2
    BEGIN
        SELECT convert(nvarchar, e.EventTypeID) [ID], u.UserID [SubID], count(e.EventID) EventCount
        FROM   log.Event e JOIN org.[User] u ON e.UserID = u.UserID
        JOIN   org.H_D hd ON u.DepartmentID = hd.DepartmentID AND hd.HDID = @HDID
          AND  e.EventTypeID  IN (SELECT Value FROM dbo.funcListToTableInt(@EventTypeID, ','))
          AND (cast(e.Created as date) >= @FromDate OR @FromDate IS NULL)
          AND (cast(e.Created as date) <= @ToDate OR @ToDate IS NULL)
          AND (@ItemID=0 OR e.ItemID =@ItemID)
          AND (u.UserID IN (SELECT Value FROM dbo.funcListToTableInt(@UserID, ',')) OR ISNULL(@UserID,'') = '')
        GROUP BY e.EventTypeID, u.UserID
    END
END

GO
