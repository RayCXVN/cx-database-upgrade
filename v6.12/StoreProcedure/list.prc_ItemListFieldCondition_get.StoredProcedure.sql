SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[list].[prc_ItemListFieldCondition_get]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [list].[prc_ItemListFieldCondition_get] AS' 
END
GO
ALTER PROCEDURE [list].[prc_ItemListFieldCondition_get]
	@ItemListFieldID int
AS
BEGIN
	SET NOCOUNT ON;
	DECLARE @Err Int
	
    SELECT 
		[ItemListFieldConditionID],
		[ItemListFieldID],
		[Type],
		[Param],
		[Value],
		[Created]
	FROM [list].[ItemListFieldCondition]
	WHERE [ItemListFieldID] = @ItemListFieldID
	
	Set @Err = @@Error

	RETURN @Err
  
END

GO
