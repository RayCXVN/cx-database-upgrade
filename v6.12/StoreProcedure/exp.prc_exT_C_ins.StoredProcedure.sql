SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[exp].[prc_exT_C_ins]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [exp].[prc_exT_C_ins] AS' 
END
GO

ALTER PROCEDURE [exp].[prc_exT_C_ins]
(
	@TypeID smallint,
	@ColumnID smallint,
	@No smallint,
	@cUserid int,
	@Log smallint = 1
)
AS
BEGIN
	SET NOCOUNT ON
	DECLARE @Err Int

	INSERT INTO [exp].[exT_C]
	(
		[TypeID],
		[ColumnID],
		[No]
	)
	VALUES
	(
		@TypeID,
		@ColumnID,
		@No
	)

	Set @Err = @@Error

	IF @Log = 1 
	BEGIN 
		INSERT INTO [Log].[AuditLog] ( UserId, TableName, Type, Data, Created) 
		SELECT @cUserid,'exT_C',0,
		( SELECT * FROM [exp].[exT_C] 
			WHERE
			[TypeID] = @TypeID AND
			[ColumnID] = @ColumnID				 FOR XML AUTO) as data,
				getdate() 
	 END

	RETURN @Err
END


GO
