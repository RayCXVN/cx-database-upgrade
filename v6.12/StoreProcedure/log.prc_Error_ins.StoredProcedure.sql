SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[log].[prc_Error_ins]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [log].[prc_Error_ins] AS' 
END
GO
ALTER PROC [log].[prc_Error_ins]
(
	@UserID			bigint,
	@ErrNo			int,
	@ErrDesc		varchar(500),
	@Place			varchar(50)
)
AS
BEGIN	
	SET NOCOUNT ON
 	INSERT [log].Errors(UserID, ErrNo, ErrDesc, Place)
 	VALUES(@UserID, @ErrNo, @ErrDesc, @Place) 	
	SET NOCOUNT OFF
END

GO
