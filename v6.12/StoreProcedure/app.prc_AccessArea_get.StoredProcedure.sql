SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[app].[prc_AccessArea_get]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [app].[prc_AccessArea_get] AS' 
END
GO


/* ------------------------------------------------------------
   PROCEDURE:    prc_AccessArea_get

   Description:  Selects records from the table 'AccessArea'

   AUTHOR:       LockwoodTech 07.07.2006 12:52:52
   ------------------------------------------------------------ */
ALTER PROCEDURE [app].[prc_AccessArea_get]

As
BEGIN
	SET NOCOUNT ON
	DECLARE @Err Int

	Select
	[AccessAreaID],
	[Name]
	FROM [AccessArea]
	Set @Err = @@Error

	RETURN @Err
End


GO
