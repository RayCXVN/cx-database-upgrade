SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[org].[prc_DepartmentOrgno_get]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [org].[prc_DepartmentOrgno_get] AS' 
END
GO
/*  2016-10-10 Ray: Remove column Status from org.User, org.Department, RDB.dbo.Result
*/
ALTER PROCEDURE [org].[prc_DepartmentOrgno_get]
(
    @Orgno VARCHAR(15),
    @Ownerid INT
)
AS
BEGIN
	DECLARE @EntityStatusID INT = 0
	SELECT @EntityStatusID = EntityStatusID FROM EntityStatus WHERE CodeName='Active'
    SELECT  [DepartmentID],
            [LanguageID],
            [OwnerID],
            ISNULL([CustomerID],0) CustomerID,
            [Name],
            [Description],
            [Adress],
            [PostalCode],
            [City],
            [OrgNo],
            [Created],
            [ExtID],
            [Tag],
            [Locked],
			[CountryCode],
			[Deleted],
			[EntityStatusID],
			[EntityStatusReasonID]
    FROM  org.Department
    WHERE orgno = @Orgno AND OwnerId = @Ownerid and [EntityStatusID] = @EntityStatusID AND Deleted IS NULL
      AND departmentid in (Select departmentid from H_D where deleted = 0)
END

GO
