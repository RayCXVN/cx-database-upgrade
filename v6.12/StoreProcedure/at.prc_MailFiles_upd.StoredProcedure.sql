SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[at].[prc_MailFiles_upd]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [at].[prc_MailFiles_upd] AS' 
END
GO

ALTER PROCEDURE [at].[prc_MailFiles_upd]
(
	@MailFileID int,
	@MIMEType nvarchar(64),
	@Data varbinary,
	@Description nvarchar(max),
	@Size bigint,
	@Filename nvarchar(128)='',
	@MailID INT=NULL,
	@cUserid INT,
	@Log smallint = 1
)
AS
BEGIN
	SET NOCOUNT ON
	DECLARE @Err Int

	UPDATE [at].[MailFiles]
	SET
		[MIMEType] = @MIMEType,
		[Data] = @Data,
		[Description] = @Description,
		[Size] = @Size,
		[Filename] = @Filename,
		[MailID] = @MailID
	WHERE
		[MailFileID] = @MailFileID

	IF @Log = 1 
	BEGIN 
		INSERT INTO [Log].[AuditLog] ( UserId, TableName, Type, Data, Created) 
		SELECT @cUserid,'MailFiles',1,
		( SELECT * FROM [at].[MailFiles] 
			WHERE
			[MailFileID] = @MailFileID			 FOR XML AUTO) as data,
			getdate()
	END

	Set @Err = @@Error

	RETURN @Err
END


GO
