SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[at].[prc_B_P_get]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [at].[prc_B_P_get] AS' 
END
GO
ALTER PROCEDURE [at].[prc_B_P_get]
(
	@BulkID int
)
AS
BEGIN
	SET NOCOUNT ON
	DECLARE @Err Int

	SELECT
	[BulkID],
	[PageID]
	FROM [at].[B_P]
	WHERE
	[BulkID] = @BulkID

	Set @Err = @@Error

	RETURN @Err
END



GO
