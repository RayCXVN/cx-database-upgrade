SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[org].[prc_Check_Username]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [org].[prc_Check_Username] AS' 
END
GO
ALTER PROC [org].[prc_Check_Username]
(
  @UserCount int = null output,
  @Ownerid INT,
  @userid INT,
  @username NVARCHAR(128)
)
AS
  SELECT @UserCount = COUNT(Userid) 
  FROM org.[USER]
  WHERE ownerid = @ownerid
  AND username = @username
  AND userid <> @userid  
GO
