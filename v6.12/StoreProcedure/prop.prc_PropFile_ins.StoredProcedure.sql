SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[prop].[prc_PropFile_ins]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [prop].[prc_PropFile_ins] AS' 
END
GO
ALTER PROCEDURE [prop].[prc_PropFile_ins]
(
	@PropFileID int = null output,
	@OwnerId int,
	@CustomerId INT=NULL,
	@ContentType nvarchar(64),
	@Size bigint,
	@Title nvarchar(512),
	@Filename nvarchar(256),
	@Description nvarchar(128),
	@FileData varbinary(max),
	@cUserid int,
	@Log smallint = 1
)
AS
BEGIN
	SET NOCOUNT ON
	DECLARE @Err Int

	INSERT INTO [prop].[PropFile]
	(
		[GUID],
		[OwnerId],
		[CustomerId],
		[ContentType],
		[Size],
		[Title],
		[Filename],
		[Description],
		[FileData]
	)
	VALUES
	(
		newid(),
		@OwnerId,
		@CustomerId,
		@ContentType,
		@Size,
		@Title,
		@Filename,
		@Description,
		@FileData
	)

	Set @Err = @@Error
	Set @PropFileID = scope_identity()

	IF @Log = 1 
	BEGIN 
		INSERT INTO [Log].[AuditLog] ( UserId, TableName, Type, Data, Created) 
		SELECT @cUserid,'PropFile',33,
		(	SELECT [PropFileId],[GUID],[Deleted],[OwnerId],[CustomerId],[ContentType],[Size],[Title],[FileName],[Description]
			FROM [prop].[PropFile] 
			WHERE [PropFileID] = @PropFileID FOR XML AUTO) as data,
		getdate() 
	 END

	RETURN @Err
END

GO
