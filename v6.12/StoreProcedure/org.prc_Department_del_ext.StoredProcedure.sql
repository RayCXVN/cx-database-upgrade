SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[org].[prc_Department_del_ext]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [org].[prc_Department_del_ext] AS' 
END
GO
/*  
    2016-10-10 Ray:     Remove column Status from org.User, org.Department, RDB.dbo.Result
    2016-10-31 Sarah:   Hot fix: Set limit length of string which is used to update UserName
	Steve - Dec 8, 2016 - Job Fair project: merge Q and R DB, then change how to make a dynamic query
*/
ALTER PROCEDURE [org].[prc_Department_del_ext]
(		
    @OutResult      varchar(max) out,
    @OwnerId        int,
    @UserId         int,	
    @DepartmentId   int,	
    @Log            smallint = 1
)
As
BEGIN
	SET NOCOUNT ON
    SET XACT_ABORT ON

BEGIN TRAN
    DECLARE @ActiveEntityStatusID INT = (SELECT EntityStatusID FROM EntityStatus WHERE CodeName='Active')
    DECLARE @DeactiveEntityStatusID INT = (SELECT EntityStatusID FROM EntityStatus WHERE CodeName='Deactive')
    DECLARE @StatusReason_DeactivatedByRelatedObject int
	DECLARE @DepartmentIds as varchar(MAX)
	DECLARE @UserIds as varchar(MAX)
	DECLARE @ResultIds as varchar(MAX) = ''  -- Not null
	DECLARE @Result int = 0, @LinkedDB NVARCHAR(MAX) = ' '
	
    SELECT @StatusReason_DeactivatedByRelatedObject = [EntityStatusReasonID] FROM [EntityStatusReason] WHERE [CodeName] = 'Deactive_SetByRelatedObject'

	-- Check department id belong to owner id
	IF NOT EXISTS (SELECT DepartmentID FROM [org].[Department] WHERE DepartmentID = @DepartmentId AND OwnerID = @OwnerId)
	BEGIN 
		Set @Result = -1
	END
			
	-- Not delete department have been locked
	IF EXISTS (SELECT DepartmentID FROM [org].[Department] WHERE DepartmentID = @DepartmentId AND Locked = 1)
	BEGIN 
		Set @Result = -2
	END
	
	-- Do not delete the department if there are users in it (do not count users with status is active).
	IF EXISTS (SELECT UserId FROM [org].[User] WHERE DepartmentID = @DepartmentId AND EntityStatusID = @ActiveEntityStatusID AND Deleted IS NULL)
	BEGIN 
		Set @Result = -3
	END
    
    IF @Result = 0
    BEGIN		
		UPDATE [org].[department] 
		SET EntityStatusID = @DeactiveEntityStatusID
		WHERE DepartmentID = @DepartmentId
		
		-- Delete all child departments and child users
		DECLARE @HDID as int
        SELECT @HDID = HDID FROM [org].[H_D] WHERE DepartmentID = @DepartmentId

        DECLARE @ChildDepartmentTable TABLE (DepartmentID int, HDID int)
        INSERT INTO @ChildDepartmentTable (DepartmentID, HDID)
        SELECT hd.[DepartmentID], hd.[HDID]
        FROM [org].[H_D] hd
        WHERE hd.[Path] LIKE '%\' + CAST(@HDID AS NVARCHAR(32)) + '\%' AND hd.[DepartmentID] != @DepartmentId

		UPDATE d SET d.EntityStatusID = @DeactiveEntityStatusID
        FROM [org].[department] d JOIN @ChildDepartmentTable del ON d.[DepartmentID] = del.[DepartmentID]
		
        UPDATE [org].[H_D] SET [Deleted] = 1 WHERE [HDID] = @HDID
        UPDATE hd SET hd.[Deleted] = 1
        FROM [org].[H_D] hd JOIN @ChildDepartmentTable del ON hd.[HDID] = del.[HDID]

        UPDATE u SET u.[UserName] = SUBSTRING('DEL_' + cast([UserID] AS VARCHAR(32)) + '_' + u.[UserName],1,128) ,u.EntityStatusID = @DeactiveEntityStatusID
        FROM [org].[User] u JOIN @ChildDepartmentTable del ON u.[DepartmentID] = del.[DepartmentID]
		
		-- Create list string of User ids
		SET @UserIds = (select cast(u.UserID AS VARCHAR(32)) + ',' as 'text()' 
                        from [org].[User] u JOIN @ChildDepartmentTable del ON u.[DepartmentID] = del.[DepartmentID]
                        for xml path(''))
		DECLARE @UserIdsLen int = LEN(@UserIds)
		IF(@UserIdsLen > 0)
		BEGIN
			SET @UserIds = LEFT(@UserIds,@UserIdsLen - 1)
		END
					
		-- Set status for results on HD, loop for all Report database
		DECLARE @ReportTable as Table(
			ReportServer varchar(64),
			ReportDB varchar(64),
			DeniedSurveyList varchar(max)
		)
		
		-- Create list string of Department ids
		SET @DepartmentIds = (select cast(DepartmentID AS VARCHAR(32)) + ',' as 'text()' from @ChildDepartmentTable	for xml path(''))
		DECLARE @DepartmentIdsLen int = LEN(@DepartmentIds)
		IF(@DepartmentIdsLen > 0)
		BEGIN
			SET @DepartmentIds = LEFT(@DepartmentIds,@DepartmentIdsLen - 1)
		END		
		
        IF ISNULL(@DepartmentIds, '') != ''
        BEGIN
		    INSERT INTO @ReportTable exec [at].[prc_ReportDB_getByAccessGroup] @OwnerId
				
		    DECLARE ReportDB_cursor CURSOR FORWARD_ONLY READ_ONLY FOR SELECT ReportServer, ReportDB FROM @ReportTable		
		    DECLARE @ReportServer varchar(64)
		    DECLARE @ReportDB varchar(64)
				
		    OPEN ReportDB_cursor		
		    FETCH NEXT FROM ReportDB_cursor INTO @ReportServer, @ReportDB
		    WHILE @@FETCH_STATUS = 0
		    BEGIN
				IF (@ReportServer = @@servername AND @ReportDB = DB_NAME())
					SET @LinkedDB = ' '
				ELSE 
					SET @LinkedDB = '[' + @ReportServer + '].[' + @ReportDB + '].'

				CREATE TABLE #temptable (ResultID bigint)
		
				DECLARE @sqlCommand as nvarchar(MAX) = 'INSERT INTO #temptable SELECT ResultID FROM ' + @LinkedDB + '[dbo].[Result] WHERE DepartmentID in (' + @DepartmentIds + ')
				UPDATE r SET r.EntityStatusID = ' + CAST(@DeactiveEntityStatusID AS VARCHAR(5)) + ',
								r.[EntityStatusReasonID] = ' + CAST(@StatusReason_DeactivatedByRelatedObject AS VARCHAR(32)) + '
				FROM ' + @LinkedDB + '[dbo].[Result] r JOIN #temptable t ON r.ResultID = t.ResultID'						
				exec sp_executesql @sqlCommand
			
				SET @ResultIds = @ResultIds + (select cast(ResultID AS VARCHAR(32)) + ',' as 'text()' from #temptable for xml path(''))			
						
				FETCH NEXT FROM ReportDB_cursor INTO @ReportServer, @ReportDB
		    END	
            CLOSE ReportDB_cursor
		    DEALLOCATE ReportDB_cursor
        END
		
		DECLARE @ResultIdsLen int = LEN(@ResultIds)
		IF(@ResultIdsLen > 0)
		BEGIN
			SET @ResultIds = LEFT(@ResultIds,@ResultIdsLen - 1)
		END	
							
		-- Write log
		IF @Log = 1 
		BEGIN 
			INSERT INTO [Log].[AuditLog] ( UserId, TableName, Type, Data, Created) 
			SELECT @UserId,'Department',2,
			( SELECT * FROM [org].[Department] 
				WHERE DepartmentID = @DepartmentId FOR XML AUTO) as data,getdate() 
		END 					
    END
	
    IF @Result < 0
    BEGIN
        SET @OutResult = cast(@Result AS VARCHAR(1000))
    END
    ELSE
    BEGIN
        SET @OutResult = cast(@Result AS VARCHAR(1000)) + '|' + ISNULL(@DepartmentIds,'') + '|' + ISNULL(@UserIds,'')	+ '|' + ISNULL(@ResultIds,'')	
    END

    COMMIT TRAN
	RETURN @Result
END

GO
