SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[at].[prc_Mail_get]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [at].[prc_Mail_get] AS' 
END
GO

ALTER PROCEDURE [at].[prc_Mail_get]
(
	@SurveyID int
)
AS
BEGIN
	SET NOCOUNT ON
	DECLARE @Err Int

	SELECT
	[MailID],
	[SurveyID],
	ISNULL([BatchID], 0) AS 'BatchID',
	[Type],
	[From],
	[Bcc],
	[StatusMail],
	[Created]
	FROM [at].[Mail]
	WHERE
	[SurveyID] = @SurveyID

	Set @Err = @@Error

	RETURN @Err
END


GO
