SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[prc_Result_sel]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [dbo].[prc_Result_sel] AS' 
END
GO
/* ------------------------------------------------------------
   PROCEDURE:    [dbo].[prc_Result_sel]

   Description:  Inserts a record into table 'Result'

   AUTHOR:       LockwoodTech 11.07.2006 11:47:11
   ------------------------------------------------------------
   2016-10-10 Ray: Remove column Status from org.User, org.Department, RDB.dbo.Result
*/
ALTER PROCEDURE [dbo].[prc_Result_sel]
(
	@ResultID bigint = null,
	@StartDate datetime = null output,
	@EndDate datetime = null output,
	@RoleID int output,
	@UserID int = null output,
	@UsergroupID int = null output,
	@SurveyID int output,
	@BatchID int output,
	@LanguageID int output,
	@PageNo smallint output,
	@DepartmentID int output,
	@Anonymous smallint output,
	@ShowBack bit output,
	@Email varchar(100) = null output,
    @EntityStatusID smallint output,
    @Deleted Datetime2 = null output
)
As
BEGIN
	SET NOCOUNT ON
	DECLARE @Err Int

	SELECT
	@StartDate = StartDate,
	@EndDate = EndDate,
	@RoleID = RoleID,
	@UserID = UserID,
	@UserGroupID = UserGroupID,
	@SurveyID = SurveyID,
	@BatchID = BatchID,
	@LanguageID = LanguageID,
	@PageNo = PageNo,
	@DepartmentID = DepartmentID,
	@Anonymous = [Anonymous],
	@ShowBack = ShowBack,
	@Email = Email,
    @EntityStatusID = EntityStatusID,
    @Deleted = Deleted
	FROM [Result]
	WHERE ResultID = @ResultID
		
	Set @Err = @@Error
		
	RETURN @Err
End


GO
