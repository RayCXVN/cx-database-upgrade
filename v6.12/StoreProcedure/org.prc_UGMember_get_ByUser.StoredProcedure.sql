SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[org].[prc_UG_U_get_ByUser]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'DROP PROCEDURE [org].[prc_UG_U_get_ByUser]' 
END
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[org].[prc_UGMember_get_ByUser]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [org].[prc_UGMember_get_ByUser] AS' 
END
GO
/*
    Steve - Sept 22 2016 - EntityStatus for table UserGroup
    2018-07-09 Ray:     Replace UG_U by UGMember
    2019-02-25 Ray:     Rename to prc_UGMember_get_ByUser
*/
ALTER PROCEDURE [org].[prc_UGMember_get_ByUser](
    @UserID          int,
    @UserGroupTypeID varchar(max) = ''
) AS
BEGIN
    SET NOCOUNT ON;
    DECLARE @Err int;
    DECLARE @ActiveEntityStatusID int= (SELECT [EntityStatusID] FROM [EntityStatus] WHERE [CodeName] = 'Active');

    SELECT [ugm].[UserGroupID], [ug].[Name], [ug].[UserID] [GroupOwnerID]
    FROM [org].[UGMember] [ugm]
    JOIN [org].[UserGroup] [ug] ON [ugm].[UserGroupID] = [ug].[UserGroupID] AND [ugm].[UserID] = @UserID
     AND [ug].[EntityStatusID] = @ActiveEntityStatusID AND [ug].[Deleted] IS NULL
     AND ugm.[EntityStatusID] = @ActiveEntityStatusID AND ugm.[Deleted] IS NULL
     AND (ISNULL(@UserGroupTypeID, '') = '' OR [ug].[UserGroupTypeID] IN (SELECT * FROM [dbo].[funcListToTableInt] (@UserGroupTypeID, ',')
                                                                         )
         );

    SET @Err = @@Error;

    RETURN @Err;
END;
GO
