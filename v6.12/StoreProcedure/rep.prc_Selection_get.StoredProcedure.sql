SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[rep].[prc_Selection_get]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [rep].[prc_Selection_get] AS' 
END
GO


ALTER PROCEDURE [rep].[prc_Selection_get]
(
	@SelectionGroupID int
)
AS
BEGIN
	SET NOCOUNT ON
	DECLARE @Err Int

	SELECT
	[SelectionID],
	[SelectionGroupID],
	[Active],
	[CustomName],
	[Name],
	ISNULL([ActivityID], 0) AS 'ActivityID',
	[Status],
	ISNULL([StartDate], 0) AS 'StartDate',
	ISNULL([EndDate], 0) AS 'EndDate',
	[Created],
	ISNUll([LevelGroupID],0) as 'LevelGroupID',
    [NO]
	FROM [rep].[Selection]
	WHERE
	[SelectionGroupID] = @SelectionGroupID
    ORDER BY [NO],[SelectionID]

	Set @Err = @@Error

	RETURN @Err
END



GO
