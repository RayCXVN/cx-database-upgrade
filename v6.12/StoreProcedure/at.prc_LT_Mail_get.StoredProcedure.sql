SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[at].[prc_LT_Mail_get]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [at].[prc_LT_Mail_get] AS' 
END
GO
ALTER PROCEDURE [at].[prc_LT_Mail_get]
(
	@MailID int
)
AS
BEGIN
	SET NOCOUNT ON
	DECLARE @Err Int

	SELECT
	[LanguageID],
	[MailID],
	[Name],
	[Subject],
	[Body]
	FROM [at].[LT_Mail]
	WHERE
	[MailID] = @MailID

	Set @Err = @@Error

	RETURN @Err
END

GO
