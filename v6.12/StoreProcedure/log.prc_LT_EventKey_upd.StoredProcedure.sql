SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[log].[prc_LT_EventKey_upd]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [log].[prc_LT_EventKey_upd] AS' 
END
GO
ALTER PROCEDURE [log].[prc_LT_EventKey_upd]
(
	@EventKeyID	int,
	@LanguageID	int,
	@Name		nvarchar(256),
	@Description	nvarchar(max),
	@cUserid		int,
	@Log			smallint = 1
)
AS
BEGIN
	SET NOCOUNT ON
	DECLARE @Err Int

	UPDATE [log].[LT_EventKey]
	SET
		[Name] = @Name,
		[Description] = @Description
	WHERE
		 [EventKeyID] = @EventKeyID
	  AND [LanguageID] = @LanguageID

	Set @Err = @@Error
	
	IF @Log = 1 
	BEGIN 
		INSERT INTO [Log].[AuditLog] ( UserId, TableName, Type, Data, Created) 
		SELECT @cUserid,'LT_EventKey',1,
		( SELECT * FROM [log].[LT_EventKey]
			WHERE
			[EventKeyID] = @EventKeyID AND [LanguageID] = @LanguageID FOR XML AUTO) as data,
				getdate() 
	END

	RETURN @Err
END

GO
