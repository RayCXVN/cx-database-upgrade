SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[rep].[prc_LT_DocumentTemplate_get]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [rep].[prc_LT_DocumentTemplate_get] AS' 
END
GO


ALTER PROCEDURE [rep].[prc_LT_DocumentTemplate_get]
(
	@LanguageID int,
	@DocumentTemplateID int
)
AS
BEGIN
	SET NOCOUNT ON
	DECLARE @Err Int

	SELECT
	[LanguageID],
	[DocumentTemplateID],
	[yStartPos],
	[FileContent],
	[MIMEType],
	[Size]
	FROM [rep].[LT_DocumentTemplate]
	WHERE
		[LanguageID] = @LanguageID
		AND [DocumentTemplateID] = @DocumentTemplateID

	Set @Err = @@Error

	RETURN @Err
END




GO
