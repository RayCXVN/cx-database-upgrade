SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[at].[prc_Period_ins]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [at].[prc_Period_ins] AS' 
END
GO
/*
	2017-06-13 Steve: Change the length of column ExtID in all related tables from xAPI format
*/
ALTER PROCEDURE [at].[prc_Period_ins]
(
	@PeriodID int = null output,
	@Ownerid int,
	@No smallint,
	@StartDate datetime = null,
	@EndDate datetime = null,
	@ExtID nvarchar(256) = '',
	@cUserid int,
	@Log smallint = 1
)
AS
BEGIN
	SET NOCOUNT ON
	DECLARE @Err Int

	INSERT INTO [at].[Period]
	(
		[Ownerid],
		[No],
		[Startdate],
		[EndDate],
		[ExtID]
	)
	VALUES
	(
		@Ownerid,
		@No,
		@StartDate,
		@Enddate,
		@ExtID
	)

	Set @Err = @@Error
	Set @PeriodID = scope_identity()

	IF @Log = 1 
	BEGIN 
		INSERT INTO [Log].[AuditLog] ( UserId, TableName, Type, Data, Created) 
		SELECT @cUserid,'Period',0,
		( SELECT * FROM [at].[Period] 
			WHERE
			[PeriodID] = @PeriodID				 FOR XML AUTO) as data,
				getdate() 
	 END

	RETURN @Err
END

GO
