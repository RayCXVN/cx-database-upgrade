SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[app].[prc_LoginService_User_del]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [app].[prc_LoginService_User_del] AS' 
END
GO
ALTER PROCEDURE [app].[prc_LoginService_User_del]
	@LoginServiceID int,
	@UserID int,
	@cUserid int,
    @Log smallint = 1
AS
BEGIN
	SET NOCOUNT ON;
	DECLARE @Err Int

    DELETE [app].[LoginService_User]
    WHERE     ([LoginServiceID] = @LoginServiceID)
       AND ([UserID] = @UserID)


    Set @Err = @@Error
	IF @Log = 1 
	BEGIN 
		INSERT INTO [Log].[AuditLog] ( UserId, TableName, Type, Data, Created) 
		SELECT @cUserid,'LoginService_User',2,
		( SELECT * FROM [app].[LoginService_User]
			WHERE
			[LoginServiceID] = @LoginServiceID AND
			[UserID] = @UserID				 FOR XML AUTO) as data,
				getdate() 
	END

	RETURN @Err       
END

GO
