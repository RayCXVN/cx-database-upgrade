SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[form].[prc_Form_ins]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [form].[prc_Form_ins] AS' 
END
GO
/*
	2016-12-05 Sarah - Increase Form.CssClass from 256 to 4000, FormCell.CssClass from 128 to 4000
	2017-06-13 Steve: Change the length of column ExtID in all related tables from xAPI format
*/
ALTER PROCEDURE [form].[prc_Form_ins]
(
	@FormID int = null output,
	@OwnerID int,
	@ElementID int,
	@TableTypeID smallint,
	@ContextFormFieldID INT=NULL,
	@Type smallint,
	@FName nvarchar(64),
	@CssClass nvarchar(4000),
	@ExtID nvarchar(256) = '',
	@cUserid int,
	@Log smallint = 1
)
AS
BEGIN
	SET NOCOUNT ON
	DECLARE @Err Int

	INSERT INTO [form].[Form]
	(
		[OwnerID],
		[ElementID],
		[TableTypeID],
		[ContextFormFieldID],
		[Type],
		[FName],
		[CssClass],
		[ExtID]
	)
	VALUES
	(
		@OwnerID,
		@ElementID,
		@TableTypeID,
		@ContextFormFieldID,
		@Type,
		@FName,
		@CssClass,
		@ExtID
	)

	Set @Err = @@Error
	Set @FormID = scope_identity()

	IF @Log = 1 
	BEGIN 
		INSERT INTO [Log].[AuditLog] ( UserId, TableName, Type, Data, Created) 
		SELECT @cUserid,'Form',0,
		( SELECT * FROM [form].[Form] 
			WHERE
			[FormID] = @FormID				 FOR XML AUTO) as data,
				getdate() 
	 END

	RETURN @Err
END

GO
