SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[prc_AccessGroupMember_del]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [dbo].[prc_AccessGroupMember_del] AS' 
END
GO


ALTER PROCEDURE [dbo].[prc_AccessGroupMember_del]
(
	@AccessGroupMemberID int,
	@cUserid int,
	@Log smallint = 1
)
AS
BEGIN
	SET NOCOUNT ON
	DECLARE @Err Int

	IF @Log = 1 
	BEGIN 
		INSERT INTO [Log].[AuditLog] ( UserId, TableName, Type, Data, Created) 
		SELECT @cUserid,'AccessGroupMember',2,
		( SELECT * FROM [dbo].[AccessGroupMember] 
			WHERE
			[AccessGroupMemberID] = @AccessGroupMemberID
			 FOR XML AUTO) as data,
			getdate() 
	END 


	DELETE FROM [dbo].[AccessGroupMember]
	WHERE
		[AccessGroupMemberID] = @AccessGroupMemberID

	Set @Err = @@Error

	RETURN @Err
END


GO
