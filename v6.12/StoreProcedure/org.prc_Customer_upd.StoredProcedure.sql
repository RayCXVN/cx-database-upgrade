SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[org].[prc_Customer_upd]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [org].[prc_Customer_upd] AS' 
END
GO
/*  
	2017-06-13 Steve: Change the length of column ExtID in all related tables from xAPI format
*/
ALTER PROCEDURE [org].[prc_Customer_upd]  
(  
 @CustomerID int,  
 @OwnerID int,  
 @LanguageID int,  
 @Name nvarchar(256),  
 @Status smallint,  
 @HasUserIntegration smallint, 
 @ExtID nvarchar(256),
 @RootMenuID int,  
 @CodeName nvarchar(256),
 @Logo nvarchar(max),
 @CssVariables nvarchar(max),
 @cUserid int,  
 @Log smallint = 1,
 @Favicon nvarchar(max) = ''  
)  
AS  
BEGIN  
 SET NOCOUNT ON  
 DECLARE @Err Int  
  
 UPDATE [org].[Customer]  
 SET  
  [OwnerID] = @OwnerID,  
  [LanguageID] = @LanguageID,  
  [Name] = @Name,  
  [Status] = @Status,  
  [HasUserIntegration] = @HasUserIntegration, 
  [ExtID] = @ExtID,
  [RootMenuID] = @RootMenuID,
  [CodeName] = @CodeName,
  [Logo] = @Logo,
  [CssVariables] = @CssVariables,
  [Favicon] = @Favicon
 WHERE  
  [CustomerID] = @CustomerID  
  
 IF @Log = 1   
 BEGIN   
  INSERT INTO [Log].[AuditLog] ( UserId, TableName, Type, Data, Created)   
  SELECT @cUserid,'Customer',1,  
  ( SELECT * FROM [org].[Customer]   
   WHERE  
   [CustomerID] = @CustomerID    FOR XML AUTO) as data,  
   getdate()  
 END  
  
 Set @Err = @@Error  
  
 RETURN @Err  
END

GO
