IF NOT EXISTS (SELECT 1 FROM sys.objects WHERE name = N'prc_LT_MemberRole_upd' AND type IN ( N'P', N'PC' )  AND SCHEMA_NAME(schema_id) = 'org')
    EXEC ('CREATE PROC [org].[prc_LT_MemberRole_upd] AS ')
GO
/*  
    2018-07-19 Sarah    Create procedures for Insert, Update, Delete, Get of org.MemberRole
*/
ALTER PROCEDURE [org].[prc_LT_MemberRole_upd]
(  
    @MemberRoleID int = null output,
    @LanguageID int,
    @Name nvarchar(max),
    @Description nvarchar(max),
    @cUserid int,
    @Log smallint = 1
)  
AS  
BEGIN  
 SET NOCOUNT ON  
 DECLARE @Err Int  

 UPDATE [org].[LT_MemberRole]
 SET 
    Name = @Name, 
    [Description] = @Description
 WHERE
    MemberRoleID = @MemberRoleID AND LanguageID = @LanguageID

 Set @Err = @@Error  

 IF @Log = 1 
 BEGIN 
    INSERT INTO [Log].[AuditLog] ( UserId, TableName, [Type], Data, Created) 
    SELECT @cUserid,'LT_MemberRole',1,
    ( SELECT * FROM [org].[LT_MemberRole] 
       WHERE MemberRoleID = @MemberRoleID AND LanguageID = @LanguageID
       FOR XML AUTO
    ) as data,
    getdate() 
 END  

 RETURN @Err  
END