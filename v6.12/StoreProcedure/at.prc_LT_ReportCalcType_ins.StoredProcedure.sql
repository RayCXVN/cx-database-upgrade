SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[at].[prc_LT_ReportCalcType_ins]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [at].[prc_LT_ReportCalcType_ins] AS' 
END
GO

ALTER PROCEDURE [at].[prc_LT_ReportCalcType_ins]
(
	@LanguageID int,
	@ReportCalcTypeID int,
	@Name nvarchar(256),
	@cUserid int,
	@Log smallint = 1
)
AS
BEGIN
	SET NOCOUNT ON
	DECLARE @Err Int

	INSERT INTO [at].[LT_ReportCalcType]
	(
		[LanguageID],
		[ReportCalcTypeID],
		[Name]
	)
	VALUES
	(
		@LanguageID,
		@ReportCalcTypeID,
		@Name
	)

	Set @Err = @@Error

	IF @Log = 1 
	BEGIN 
		INSERT INTO [Log].[AuditLog] ( UserId, TableName, Type, Data, Created) 
		SELECT @cUserid,'LT_ReportCalcType',0,
		( SELECT * FROM [at].[LT_ReportCalcType] 
			WHERE
			[LanguageID] = @LanguageID AND
			[ReportCalcTypeID] = @ReportCalcTypeID				 FOR XML AUTO) as data,
				getdate() 
	 END

	RETURN @Err
END


GO
