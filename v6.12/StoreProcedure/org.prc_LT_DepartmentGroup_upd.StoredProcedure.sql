SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[org].[prc_LT_DepartmentGroup_upd]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [org].[prc_LT_DepartmentGroup_upd] AS' 
END
GO

ALTER PROCEDURE [org].[prc_LT_DepartmentGroup_upd]
(
	@LanguageID int,
	@DepartmentGroupID int,
	@Name nvarchar(256),
	@Description nvarchar(max),
	@cUserid int,
	@Log smallint = 1
)
AS
BEGIN
	SET NOCOUNT ON
	DECLARE @Err Int

	UPDATE [org].[LT_DepartmentGroup]
	SET
		[LanguageID] = @LanguageID,
		[DepartmentGroupID] = @DepartmentGroupID,
		[Name] = @Name,
		[Description] = @Description
	WHERE
		[LanguageID] = @LanguageID AND
		[DepartmentGroupID] = @DepartmentGroupID

	IF @Log = 1 
	BEGIN 
		INSERT INTO [Log].[AuditLog] ( UserId, TableName, Type, Data, Created) 
		SELECT @cUserid,'LT_DepartmentGroup',1,
		( SELECT * FROM [org].[LT_DepartmentGroup] 
			WHERE
			[LanguageID] = @LanguageID AND
			[DepartmentGroupID] = @DepartmentGroupID			 FOR XML AUTO) as data,
			getdate()
	END

	Set @Err = @@Error

	RETURN @Err
END


GO
