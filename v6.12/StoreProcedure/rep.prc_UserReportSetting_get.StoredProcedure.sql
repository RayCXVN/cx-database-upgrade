SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[rep].[prc_UserReportSetting_get]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [rep].[prc_UserReportSetting_get] AS' 
END
GO
ALTER PROCEDURE [rep].[prc_UserReportSetting_get]
 @UserID int,
 @ReportID int
AS      
BEGIN      
 SET NOCOUNT ON;      
 DECLARE @Err Int          
 SELECT       
	 [UserReportSettingID]
	,[UserID]
	,[ReportID]
	,[ShowTable]
	,[ShowChart]
	,[ChartTypeID]
	,[ChartTypeIDAvg]
 FROM             
  [rep].[UserReportSetting]     
 WHERE        
  [UserID] = @UserID      
  AND [ReportID] = @ReportID
 Set @Err = @@Error      
      
 RETURN @Err      
END 

GO
