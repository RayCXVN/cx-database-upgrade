SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[org].[prc_DG_D_del]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [org].[prc_DG_D_del] AS' 
END
GO


ALTER PROCEDURE [org].[prc_DG_D_del]
(
	@DepartmentGroupID int,
	@DepartmentID int,
	@cUserid int,
	@Log smallint = 1
)
AS
BEGIN
	SET NOCOUNT ON
	DECLARE @Err Int

	IF @Log = 1 
	BEGIN 
		INSERT INTO [Log].[AuditLog] ( UserId, TableName, Type, Data, Created) 
		SELECT @cUserid,'DG_D',2,
		( SELECT * FROM [org].[DG_D] 
			WHERE
			[DepartmentGroupID] = @DepartmentGroupID AND
			[DepartmentID] = @DepartmentID
			 FOR XML AUTO) as data,
			getdate() 
	END 


	DELETE FROM [org].[DG_D]
	WHERE
	[DepartmentGroupID] = @DepartmentGroupID AND
	[DepartmentID] = @DepartmentID

	Set @Err = @@Error

	RETURN @Err
END


GO
