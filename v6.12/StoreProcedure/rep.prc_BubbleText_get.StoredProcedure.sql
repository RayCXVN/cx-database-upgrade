SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[rep].[prc_BubbleText_get]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [rep].[prc_BubbleText_get] AS' 
END
GO

ALTER PROCEDURE [rep].[prc_BubbleText_get]
(
	@BubbleID int
)
AS
BEGIN
	SET NOCOUNT ON
	DECLARE @Err Int

	SELECT
	[BubbleTextID],
	[BubbleID],
	[No],
	[FillColor]
	FROM [rep].[BubbleText]
	WHERE
	[BubbleID] = @BubbleID
	ORDER BY [No]

	Set @Err = @@Error

	RETURN @Err
END


GO
