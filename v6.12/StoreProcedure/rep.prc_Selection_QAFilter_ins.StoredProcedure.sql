SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[rep].[prc_Selection_QAFilter_ins]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [rep].[prc_Selection_QAFilter_ins] AS' 
END
GO
ALTER PROCEDURE [rep].[prc_Selection_QAFilter_ins]  
(  
 @SQAID int,  
 @AlternativeID int,  
 @Type smallint,  
 @cUserid int,  
 @Log smallint = 1,
 @ItemID int = NULL
)  
AS  
BEGIN  
 SET NOCOUNT ON  
 DECLARE @Err Int  
  
 INSERT INTO [rep].[Selection_QAFilter]  
 (  
  [SQAID],  
  [AlternativeID],  
  [Type],
  [ItemID]  
 )  
 VALUES  
 (  
  @SQAID,  
  @AlternativeID,  
  @Type,
  @ItemID  
 )  
  
 Set @Err = @@Error  
  
 IF @Log = 1   
 BEGIN   
  INSERT INTO [Log].[AuditLog] ( UserId, TableName, Type, Data, Created)   
  SELECT @cUserid,'Selection_QAFilter',0,  
  ( SELECT * FROM [rep].[Selection_QAFilter]   
   WHERE  
   [SQAID] = @SQAID AND  
   [AlternativeID] = @AlternativeID     FOR XML AUTO) as data,  
    getdate()   
  END  
  
 RETURN @Err  
END  
  

GO
