SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[org].[prc_Department_upd]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [org].[prc_Department_upd] AS' 
END
GO
/*  2016-11-02 Steve: Set default CurrentDate for @LastUpdated, @LastSynchronized
	2017-06-13 Steve: Change the length of column ExtID in all related tables from xAPI format
*/
ALTER PROCEDURE [org].[prc_Department_upd]
(
	@DepartmentID int,
	@LanguageID int,
	@OwnerID int,
	@CustomerID int = null,
	@Name nvarchar(256),
	@Description nvarchar(max),
	@Adress nvarchar(512),
	@PostalCode nvarchar(32),
	@City nvarchar(64),
	@OrgNo varchar(16),
	@ExtID nvarchar(256),
	@Tag nvarchar(max),
	@Locked smallint,
	@CountryCode int =0,
	@cUserid int,
	@Log smallint = 1,
	@LastUpdated datetime2=NULL,
	@LastUpdatedBy int = NULL,
	@LastSynchronized datetime2=NULL,
	@ArchetypeID int = NULL,
	@Deleted datetime2 = NULL,
	@EntityStatusID int = NULL,
	@EntityStatusReasonID int = NULL  
)
AS
BEGIN
	SET NOCOUNT ON
	DECLARE @Err Int

	IF @LastUpdated IS NULL
	   SET @LastUpdated = GETDATE()

	IF @LastSynchronized IS NULL
	   SET @LastSynchronized = GETDATE()

	UPDATE [org].[Department]
	SET
		[LanguageID] = @LanguageID,
		[OwnerID] = @OwnerID,
		[CustomerID] = @CustomerID,
		[Name] = @Name,
		[Description] = @Description,
		[Adress] = @Adress,
		[PostalCode] = @PostalCode,
		[City] = @City,
		[OrgNo] = @OrgNo,
		[ExtID] = @ExtID,
		[Tag] = @Tag,
		[Locked] = @Locked,
		[CountryCode]=@CountryCode,
		[LastUpdated] = @LastUpdated,
		[LastUpdatedBy] = @LastUpdatedBy,
		[LastSynchronized] = @LastSynchronized,
		[ArchetypeID] =  @ArchetypeID,
		[Deleted]	=	@Deleted ,
		[EntityStatusID] = @EntityStatusID ,
		[EntityStatusReasonID] = @EntityStatusReasonID  
	WHERE
		[DepartmentID] = @DepartmentID

	Update H_d set pathname = dbo.getpathname(HDID) 
    where departmentid = @Departmentid
    
	IF @Log = 1 
	BEGIN 
		INSERT INTO [Log].[AuditLog] ( UserId, TableName, Type, Data, Created) 
		SELECT @cUserid,'Department',1,
		( SELECT * FROM [org].[Department] 
			WHERE
			[DepartmentID] = @DepartmentID			 FOR XML AUTO) as data,
			getdate()
	END

	Set @Err = @@Error

	RETURN @Err
END


GO
