SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[prc_SurveyLastUpdatedCountByDays]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [dbo].[prc_SurveyLastUpdatedCountByDays] AS' 
END
GO
-- Select count(resultid) from result where surveyID = 47 and lastupdated < dateadd("d",.14,getdate())
-- [prc_SurveyLastUpdatedCountByDays] 47,14
ALTER Proc [dbo].[prc_SurveyLastUpdatedCountByDays]
(
  @SurveyID as int,
  @Periode as int,
  @HDID as int = 0
)
AS
BEGIN
  DECLARE @ActiveEntityStatusID INT = (SELECT EntityStatusID FROM EntityStatus WHERE CodeName='Active')
  IF  @HDID = 0
  BEGIN
    Select Count(resultid) as 'Count' 
    from result with (nolock) 
    where surveyID = @SurveyID and lastupdated > dateadd("d",@Periode,getdate()) and EntityStatusID = @ActiveEntityStatusID and Deleted IS NULL
  END
  ELSE
  BEGIN
    Select Count(resultid) as 'Count' 
    from result with (nolock)
    Join org.H_D with (nolock) on H_D.departmentid = result.departmentid and H_D.path like '%\' + cast(@HDID as nvarchar(32)) + '\%'
    where surveyID = @SurveyID and lastupdated > dateadd("d",@Periode,getdate()) and result.EntityStatusID = @ActiveEntityStatusID and result.Deleted IS NULL
  END
END
-------------------------------------------------------------------------------------


GO
