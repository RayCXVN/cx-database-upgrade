SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[prop].[prc_PropPage_upd]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [prop].[prc_PropPage_upd] AS' 
END
GO

ALTER PROCEDURE [prop].[prc_PropPage_upd]
(
	@PropPageID int,
	@TableTypeID smallint,
	@Type smallint,
	@No smallint,
	@cUserid int,
	@Log smallint = 1
)
AS
BEGIN
	SET NOCOUNT ON
	DECLARE @Err Int

	UPDATE [prop].[PropPage]
	SET
		[TableTypeID] = @TableTypeID,
		[Type] = @Type,
		[No] = @No
	WHERE
		[PropPageID] = @PropPageID

	IF @Log = 1 
	BEGIN 
		INSERT INTO [Log].[AuditLog] ( UserId, TableName, Type, Data, Created) 
		SELECT @cUserid,'PropPage',1,
		( SELECT * FROM [prop].[PropPage] 
			WHERE
			[PropPageID] = @PropPageID			 FOR XML AUTO) as data,
			getdate()
	END

	Set @Err = @@Error

	RETURN @Err
END


GO
