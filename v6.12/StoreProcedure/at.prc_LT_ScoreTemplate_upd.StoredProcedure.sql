SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[at].[prc_LT_ScoreTemplate_upd]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [at].[prc_LT_ScoreTemplate_upd] AS' 
END
GO

ALTER PROCEDURE [at].[prc_LT_ScoreTemplate_upd]
(
	@ScoreTemplateID int,
	@LanguageID int,
	@Name nvarchar(256),
	@Description nvarchar(max),
	@cUserid int,
	@Log smallint = 1
)
AS
BEGIN
	SET NOCOUNT ON
	DECLARE @Err Int

	UPDATE [at].[LT_ScoreTemplate]
	SET
		[ScoreTemplateID] = @ScoreTemplateID,
		[LanguageID] = @LanguageID,
		[Name] = @Name,
		[Description] = @Description
	WHERE
		[ScoreTemplateID] = @ScoreTemplateID AND
		[LanguageID] = @LanguageID

	IF @Log = 1 
	BEGIN 
		INSERT INTO [Log].[AuditLog] ( UserId, TableName, Type, Data, Created) 
		SELECT @cUserid,'LT_ScoreTemplate',1,
		( SELECT * FROM [at].[LT_ScoreTemplate] 
			WHERE
			[ScoreTemplateID] = @ScoreTemplateID AND
			[LanguageID] = @LanguageID			 FOR XML AUTO) as data,
			getdate()
	END

	Set @Err = @@Error

	RETURN @Err
END


GO
