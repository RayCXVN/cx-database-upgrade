SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[rep].[prc_DocumentItemType_ins]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [rep].[prc_DocumentItemType_ins] AS' 
END
GO


ALTER PROCEDURE [rep].[prc_DocumentItemType_ins]
(
	@DocumentItemTypeID int = null output,
	@Name nvarchar(256),
	@cUserid int,
	@Log smallint = 1
)
AS
BEGIN
	SET NOCOUNT ON
	DECLARE @Err Int

	INSERT INTO [rep].[DocumentItemType]
	(
		[Name]
	)
	VALUES
	(
		@Name
	)

	Set @Err = @@Error
	Set @DocumentItemTypeID = scope_identity()

	IF @Log = 1 
	BEGIN 
		INSERT INTO [Log].[AuditLog] ( UserId, TableName, Type, Data, Created) 
		SELECT @cUserid,'DocumentItemType',0,
		( SELECT * FROM [rep].[DocumentItemType] 
			WHERE
			[DocumentItemTypeID] = @DocumentItemTypeID				 FOR XML AUTO) as data,
				getdate() 
	 END

	RETURN @Err
END



GO
