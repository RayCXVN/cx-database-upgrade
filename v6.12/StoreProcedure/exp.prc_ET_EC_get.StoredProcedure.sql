SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[exp].[prc_ET_EC_get]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [exp].[prc_ET_EC_get] AS' 
END
GO
ALTER PROCEDURE [exp].[prc_ET_EC_get]
(
	@ExportTypeID smallint
)
AS
BEGIN
	SET NOCOUNT ON
	DECLARE @Err Int

	SELECT
	[ExportTypeID],
	[ExportColumnID],
	[No]
	FROM [exp].[ET_EC]
	WHERE
	[ExportTypeID] = @ExportTypeID
	ORDER BY [No]

	Set @Err = @@Error

	RETURN @Err
END


GO
