SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[log].[prc_LT_EventKey_get]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [log].[prc_LT_EventKey_get] AS' 
END
GO
ALTER PROCEDURE [log].[prc_LT_EventKey_get]
	@EventKeyID	int,
	@LanguageID	int
AS
BEGIN
	SET NOCOUNT ON;
	DECLARE @Err Int
	
     SELECT [LanguageID],
		  [EventKeyID],
		  [Name],
		  [Description]
	FROM  [log].[LT_EventKey]
	WHERE [EventKeyID] = @EventKeyID
	  AND [LanguageID] = @LanguageID
	
	Set @Err = @@Error

	RETURN @Err
  
END

GO
