SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[rep].[prc_ReportColumn_get]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [rep].[prc_ReportColumn_get] AS' 
END
GO

ALTER PROCEDURE [rep].[prc_ReportColumn_get]
(
	@ReportRowID int
)
AS
BEGIN
	SET NOCOUNT ON
	DECLARE @Err Int

	SELECT
	[ReportColumnID],
	[ReportRowID],
	[No],
	[ReportColumnTypeID],
	[Formula],
	[Width],
	[Format],
	[URL],
	[IsNegative],
	[FormulaText],
	ISNull([UseLevelLimit],0) as  'UseLevelLimit',
	[UseLevelLimitText],
	ISNULL([OwnerColorID],0) as 'OwnerColorID',
	ISNULL([QuestionID],0) as 'QuestionID',
	ISNULL([AlternativeID],0) as 'AlternativeID',
    CellAlign,
    [ItemID] 
	FROM [rep].[ReportColumn]
	WHERE
	[ReportRowID] = @ReportRowID
	ORDER BY [No]

	Set @Err = @@Error

	RETURN @Err
END


SET QUOTED_IDENTIFIER ON


GO
