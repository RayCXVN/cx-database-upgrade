SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[rep].[prc_LT_DocumentTemplate_del]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [rep].[prc_LT_DocumentTemplate_del] AS' 
END
GO

ALTER PROCEDURE [rep].[prc_LT_DocumentTemplate_del]
(
	@LanguageID int,
	@DocumentTemplateID int,
	@cUserid int,
	@Log smallint = 1
)
AS
BEGIN
	SET NOCOUNT ON
	DECLARE @Err Int

	IF @Log = 1 
	BEGIN 
		INSERT INTO [Log].[AuditLog] ( UserId, TableName, Type, Data, Created) 
		SELECT @cUserid,'LT_DocumentTemplate',2,
		( SELECT * FROM [rep].[LT_DocumentTemplate] 
			WHERE
			[LanguageID] = @LanguageID
			AND [DocumentTemplateID] = @DocumentTemplateID
			 FOR XML AUTO) as data,
			getdate() 
	END 


	DELETE FROM [rep].[LT_DocumentTemplate]
	WHERE
		[LanguageID] = @LanguageID
		AND [DocumentTemplateID] = @DocumentTemplateID

	Set @Err = @@Error

	RETURN @Err
END


GO
