SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[org].[prc_UG_U_get]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [org].[prc_UG_U_get] AS' 
END
GO
/*  2016-10-10 Ray: Remove column Status from org.User, org.Department, RDB.dbo.Result
*/
ALTER PROCEDURE [org].[prc_UG_U_get]
(
	@UserGroupID int
)
AS
BEGIN
	SET NOCOUNT ON
	DECLARE @Err Int
    DECLARE @ActiveEntityStatusID INT = (SELECT EntityStatusID FROM EntityStatus WHERE CodeName='Active')

	SELECT
	ug.[UserGroupID],
	ug.[UserID],
	u.[DepartmentID],
	u.FirstName,
	u.LastName,
	u.[Email],
    u.[Mobile],
    u.[ExtID],
    u.[SSN],
    u.[Tag],
    u.[Locked],
    u.[Gender],
    u.[DateOfBirth],
    u.[Created],
    u.[ChangePassword],
    u.[HashPassword],
    u.[SaltPassword],
    u.[OneTimePassword],
    u.[OTPExpireTime],
    u.[CountryCode],
    u.EntityStatusID,
    u.Deleted,
    u.EntityStatusReasonID
	FROM [org].[UG_U] ug
	INNER JOIN [org].[User] u ON ug.UserID = u.UserID AND u.EntityStatusID = @ActiveEntityStatusID AND u.Deleted IS NULL
	WHERE
	[UserGroupID] = @UserGroupID

	Set @Err = @@Error

	RETURN @Err
END

GO
