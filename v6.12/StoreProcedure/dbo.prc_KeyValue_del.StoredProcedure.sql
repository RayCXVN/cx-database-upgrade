SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[prc_KeyValue_del]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [dbo].[prc_KeyValue_del] AS' 
END
GO

ALTER PROCEDURE [dbo].[prc_KeyValue_del]
(
	@Id int,
	@Type tinyint,
	@Key varchar(50),
	@cUserid int,
	@Log smallint = 1
)
AS
BEGIN
	SET NOCOUNT ON
	DECLARE @Err Int

	IF @Log = 1 
	BEGIN 
		INSERT INTO [Log].[AuditLog] ( UserId, TableName, Type, Data, Created) 
		SELECT @cUserid,'KeyValue',2,
		( SELECT * FROM [dbo].[KeyValue] 
			WHERE
			[Id] = @Id AND
			[Type] = @Type AND
			[Key] = @Key
			 FOR XML AUTO) as data,
			getdate() 
	END 


	DELETE FROM [dbo].[KeyValue]
	WHERE
		[Id] = @Id AND
		[Type] = @Type AND
		[Key] = @Key

	Set @Err = @@Error

	RETURN @Err
END


GO
