SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[org].[prc_ParentHDID_getFirstByDTID]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [org].[prc_ParentHDID_getFirstByDTID] AS' 
END
GO
ALTER PROC [org].[prc_ParentHDID_getFirstByDTID]
(
	@HDID             INT,
    @DeparmentTypeID  INT
)
AS
BEGIN
	SET NOCOUNT ON  
    DECLARE @Err VARCHAR(max), @Path VARCHAR(MAX)
    DECLARE @ParentHDID TABLE(HDID INT)
    
    SELECT @Path = [Path] FROM org.H_D WHERE [HDID] = @HDID       
    SET @Path = SUBSTRING(@Path,2,LEN(@Path)-2)      
    
    INSERT INTO @ParentHDID (HDID) SELECT [Value] FROM dbo.funcListToTableInt(@Path,'\')
    
    SELECT TOP 1 hd.[HDID], hd.[DepartmentID]
    FROM @ParentHDID ph JOIN [org].[H_D] hd ON ph.[HDID] = hd.[HDID]    -- Does not check Deleted status because parent must exists for the given child
    JOIN [org].[DT_D] dtd ON hd.[DepartmentID] = dtd.[DepartmentID] AND dtd.[DepartmentTypeID] = @DeparmentTypeID
    ORDER BY hd.[Path] DESC
  
    SET @Err = @@Error     
	RETURN @Err
END

GO
