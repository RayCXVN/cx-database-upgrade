SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[rep].[prc_Report_get]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [rep].[prc_Report_get] AS' 
END
GO

ALTER PROCEDURE [rep].[prc_Report_get]
(
	@OwnerID int
)
AS
BEGIN
	SET NOCOUNT ON
	DECLARE @Err Int

	SELECT
	[ReportID],
	[OwnerID],
	[UserID],
	[Created],
	[ChartTemplate],
	[DefaultCalcType]
	FROM [rep].[Report]
	WHERE
	[OwnerID] = @OwnerID

	Set @Err = @@Error

	RETURN @Err
END


GO
