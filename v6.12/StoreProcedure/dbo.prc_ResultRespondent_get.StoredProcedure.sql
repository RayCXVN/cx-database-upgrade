SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[prc_ResultRespondent_get]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [dbo].[prc_ResultRespondent_get] AS' 
END
GO
/*  2016-10-10 Ray: Remove column Status from org.User, org.Department, RDB.dbo.Result
*/
ALTER PROCEDURE [dbo].[prc_ResultRespondent_get]
(
	@ResultKey	varchar(128),
	@Userid INT
)
As
BEGIN
	SET NOCOUNT ON
	DECLARE @Err Int
    DECLARE @ActiveEntityStatusID INT = (SELECT EntityStatusID FROM EntityStatus WHERE CodeName='Active')

	Select
	[ResultID],
	[StartDate],
	[EndDate],
	[RoleID],
	[UserID]=0,
	ISNULL([UserGroupID], 0) [UserGroupID],
	[SurveyID],
	[BatchID],
	[LanguageID],
	[PageNo],
	[DepartmentID],
	[Anonymous],
	[ShowBack],
	[Email],
	[ResultKey],
	[Lastupdated],
	[LastupdatedBy],
    EntityStatusID,
    Deleted
	FROM [Result]
	WHERE 
	( 
	   ( ResultKey = @ResultKey  AND @ResultKey <> '')
	   OR
	   (Userid = @Userid and @Userid > 0)
	  )   
	--AND EndDate IS NULL
	AND EntityStatusID = @ActiveEntityStatusID AND Deleted IS NULL
	Set @Err = @@Error

	RETURN @Err
END


GO
