SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[prc_UserType_get_ByAccessArea]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [dbo].[prc_UserType_get_ByAccessArea] AS' 
END
GO



/* ------------------------------------------------------------
   PROCEDURE:    [prc_UserType_get_ByAccessArea]

   Description:  Selects records from the table 'prc_UserType_get_ByAccessArea' selected by accessAreaID

   AUTHOR:       BMH 20.12.2006 10:32:00
   ------------------------------------------------------------ */
ALTER PROCEDURE [dbo].[prc_UserType_get_ByAccessArea]
(
	@AccessAreaID	int,
	@OwnerID	int
)
As
BEGIN
	SET NOCOUNT ON
	DECLARE @Err Int

	Select
	ut.[UserTypeID],
	ut.[OwnerID],
	ut.[ExtID],
	ut.[No],
	ut.[Created]
	FROM org.[UserType] ut
	inner Join app.[UT_AccessArea] utaa 
	on utaa.[UserTypeID] = ut.[UserTypeID]
	and utaa.AccessAreaID = @AccessAreaID
	WHERE 
	ut.[OwnerID] = @OwnerID
	ORDER BY [No]

	Set @Err = @@Error

	RETURN @Err
End



GO
