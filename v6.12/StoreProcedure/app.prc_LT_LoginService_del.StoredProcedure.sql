SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[app].[prc_LT_LoginService_del]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [app].[prc_LT_LoginService_del] AS' 
END
GO
ALTER PROCEDURE [app].[prc_LT_LoginService_del]
	@LanguageID int,
	@LoginServiceID int,
	@cUserid int,
    @Log smallint = 1
AS
BEGIN
	SET NOCOUNT ON;
	DECLARE @Err Int

    DELETE [app].[LT_LoginService]
    WHERE     [LoginServiceID] = @LoginServiceID

    Set @Err = @@Error
	IF @Log = 1 
	BEGIN 
		INSERT INTO [Log].[AuditLog] ( UserId, TableName, Type, Data, Created) 
		SELECT @cUserid,'LT_LoginService',0,
		( SELECT * FROM [app].[LT_LoginService]
			WHERE
			([LoginServiceID] = @LoginServiceID AND [LanguageID] = @LanguageID) 				 FOR XML AUTO) as data,
				getdate() 
	END

	RETURN @Err       
END

GO
