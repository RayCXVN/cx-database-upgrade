SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[at].[prc_A_VT_upd]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [at].[prc_A_VT_upd] AS' 
END
GO
ALTER PROCEDURE [at].[prc_A_VT_upd]
(
	@AVTID int,
	@ViewTypeID int,
	@ActivityID int,
	@No smallint,
	@Type smallint,
	@Template varchar(64),
	@ShowPercentage BIT,
	@ActivityViewID int=null,
	@LevelGroupID int=null,
	@CssClass nvarchar(64)='',
	@cUserid int,
	@Log smallint = 1
)
AS
BEGIN
	SET NOCOUNT ON
	DECLARE @Err Int

	UPDATE [at].[A_VT]
	SET
		[ViewTypeID] = @ViewTypeID,
		[ActivityID] = @ActivityID,
		[No] = @No,
		[Type] = @Type,
		[Template] = @Template,
		[ShowPercentage] = @ShowPercentage,
		[ActivityViewID] = @ActivityViewID,
		[LevelGroupID] = @LevelGroupID,
		[CssClass]       =@CssClass
	WHERE
		[AVTID] = @AVTID

	IF @Log = 1 
	BEGIN 
		INSERT INTO [Log].[AuditLog] ( UserId, TableName, Type, Data, Created) 
		SELECT @cUserid,'A_VT',1,
		( SELECT * FROM [at].[A_VT] 
			WHERE
			[AVTID] = @AVTID			 FOR XML AUTO) as data,
			getdate()
	END

	Set @Err = @@Error

	RETURN @Err
END

GO
