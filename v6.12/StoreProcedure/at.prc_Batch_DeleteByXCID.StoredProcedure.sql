SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[at].[prc_Batch_DeleteByXCID]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [at].[prc_Batch_DeleteByXCID] AS' 
END
GO
/*
    EXEC [at].[prc_Batch_DeleteByXCID] 109, 65622, 8, 3, 30
    Sarah - Oct 31, 2016 - Hot fix: Get EntityStatus value on QDB
    Johnny - Des 07 2016 - Update store procedure for dynamic SQL
    2018-09-07 Ray:     Remove empty results along with deleted batches
*/
ALTER PROCEDURE [at].[prc_Batch_DeleteByXCID]
(   @XCID               int,
    @DepartmentIDList   varchar(max),
    @ActivePeriodID     int,
    @NumberOfPeriods    int = 0,
    @PROP_ReadOnly      int
) AS
BEGIN
    SET NOCOUNT ON
    DECLARE @ActiveEntityStatusID VARCHAR(2) = (SELECT EntityStatusID FROM EntityStatus WHERE CodeName='Active')
    DECLARE @DepartmentTable TABLE ([DepartmentID] int)
    INSERT INTO @DepartmentTable ([DepartmentID]) SELECT DISTINCT [Value] FROM dbo.funcListToTableInt(@DepartmentIDList, ',')
    
    DECLARE @ActivePeriodEndDate datetime
    SELECT @ActivePeriodEndDate = [p].[Enddate] FROM [at].[Period] p WHERE [p].[PeriodID] = @ActivePeriodID

    DECLARE @PeriodTable TABLE ([PeriodID] int, [ReadOnlyActivity] nvarchar(16))
    IF @NumberOfPeriods <= 0 SET @NumberOfPeriods = 1
    INSERT INTO @PeriodTable ([PeriodID], [ReadOnlyActivity]) SELECT TOP(@NumberOfPeriods - 1) p.[PeriodID], 'false'
                                                              FROM [at].[Period] p
                                                              WHERE p.[Enddate] <= @ActivePeriodEndDate AND p.[PeriodID] != @ActivePeriodID
                                                              ORDER BY p.[Enddate] DESC
    INSERT INTO @PeriodTable ([PeriodID], [ReadOnlyActivity]) VALUES (@ActivePeriodID, 'true')

    DECLARE @Activity TABLE ([ActivityID] int, [ReadOnly] nvarchar(16))
    INSERT INTO @Activity ([ActivityID], [ReadOnly])
    SELECT xca.[ActivityID], ISNULL(ltrim(rtrim(pv.[Value])), 'false') [ReadOnly]
    FROM [at].[XC_A] xca LEFT JOIN [prop].[PropValue] pv ON pv.[PropertyID] = @PROP_ReadOnly AND pv.[ItemID] = cast(xca.[ActivityID] as nvarchar(32))
    WHERE xca.[XCID] = @XCID

    CREATE TABLE #Batch ([ActivityID] int, [SurveyID] int, [BatchID] int, [ReportServer] nvarchar(64), [ReportDB] nvarchar(64), [HasAnswer] bit)
    INSERT INTO #Batch ([ActivityID], [SurveyID], [BatchID], [ReportServer], [ReportDB], [HasAnswer])
    SELECT s.[ActivityID], s.[SurveyID], b.[BatchID], s.[ReportServer], s.[ReportDB], 0
    FROM [at].[Survey] s JOIN @Activity a ON s.[ActivityID] = a.[ActivityID] AND s.[Status] = 2
    JOIN @PeriodTable p ON s.[PeriodID] = p.[PeriodID] AND ((a.[ReadOnly] = p.[ReadOnlyActivity] AND a.[ReadOnly] = 'true')
                                                            OR a.[ReadOnly] = 'false')
    JOIN [at].[Batch] b ON b.[SurveyID] = s.[SurveyID]
    JOIN @DepartmentTable d ON b.[DepartmentID] = d.[DepartmentID]

    DECLARE @ReportServer nvarchar(64) = '', @ReportDB nvarchar(64) = '', @sqlCommand nvarchar(max) = ''
    DECLARE @LinkedDB NVARCHAR(MAX)
    DECLARE c_RDB CURSOR LOCAL FAST_FORWARD READ_ONLY FOR
    SELECT DISTINCT [ReportServer], [ReportDB] FROM #Batch

    OPEN c_RDB
    FETCH NEXT FROM c_RDB INTO @ReportServer, @ReportDB
    WHILE @@FETCH_STATUS = 0
    BEGIN        
        IF @ReportDB=DB_NAME() AND @ReportServer=@@servername
            BEGIN
            SET @LinkedDB=' '
            END
        ELSE
            BEGIN
            SET @LinkedDB= '['+ @ReportServer + '].['+ @ReportDB + '].'
            END
        -- Set HasAnswer = 1 if there is any answers for non-background questions
        SET @sqlCommand = 'UPDATE b SET b.[HasAnswer] = 1 FROM #Batch b
        WHERE b.[ReportServer] = ''' + @ReportServer + ''' AND b.[ReportDB] = ''' + @ReportDB + '''
          AND EXISTS (SELECT 1 FROM '+ @LinkedDB +'[dbo].[Answer] a
                               JOIN '+ @LinkedDB +'[dbo].[Result] r ON a.[ResultID] = r.[ResultID] AND r.EntityStatusID = '+@ActiveEntityStatusID+' AND r.Deleted IS NULL AND r.[BatchID] = b.[BatchID]
                               JOIN [at].[Page] p ON p.[ActivityID] = b.[ActivityID] AND p.[Type] = 0       -- standard pages
                               JOIN [at].[Question] q ON p.[PageID] = q.[PageID] AND q.[QuestionID] = a.[QuestionID]
                     )'
        EXEC sp_executesql @sqlCommand

        SET @sqlCommand = 'DELETE r FROM '+ @LinkedDB +'[dbo].[Result] r JOIN #Batch bt ON r.[BatchID] = bt.[BatchID] AND bt.[ReportServer] = ''' + @ReportServer + ''' AND bt.[ReportDB] = ''' + @ReportDB + ''' AND bt.[HasAnswer] = 0';
        EXEC sp_executesql @sqlCommand
        DELETE b FROM [at].[Batch] b JOIN #Batch bt ON b.[BatchID] = bt.[BatchID] AND bt.[ReportServer] = @ReportServer AND bt.[ReportDB] = @ReportDB AND bt.[HasAnswer] = 0

        FETCH NEXT FROM c_RDB INTO @ReportServer, @ReportDB
    END
    CLOSE c_RDB
    DEALLOCATE c_RDB

    SELECT b.[DepartmentID], COUNT(b.[BatchID]) [BatchCount]
    FROM [at].[Survey] s JOIN @Activity a ON s.[ActivityID] = a.[ActivityID] AND s.[Status] = 2
    JOIN @PeriodTable p ON s.[PeriodID] = p.[PeriodID]
    JOIN [at].[Batch] b ON b.[SurveyID] = s.[SurveyID] AND b.[Status] = 2
    JOIN @DepartmentTable d ON b.[DepartmentID] = d.[DepartmentID]
    GROUP BY b.[DepartmentID]
    
    DROP TABLE #Batch
END
GO
