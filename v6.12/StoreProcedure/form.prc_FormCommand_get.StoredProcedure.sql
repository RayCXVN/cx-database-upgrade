SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[form].[prc_FormCommand_get]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [form].[prc_FormCommand_get] AS' 
END
GO
ALTER PROCEDURE [form].[prc_FormCommand_get]
(
	@FormID int
)
AS
BEGIN
	SET NOCOUNT ON
	DECLARE @Err Int

	SELECT
	[FormCommandID],
	[FormID],
	[No],
	[FormMode],
	[FormCommandType],
	[CssClass],
	[ClientFunction],
	[Created],
	[CommandGroupClass]
	FROM [form].[FormCommand]
	WHERE
	[FormID] = @FormID
	ORDER BY [No]

	Set @Err = @@Error

	RETURN @Err
END



GO
