SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[prc_Answer_Del_ByPageID]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [dbo].[prc_Answer_Del_ByPageID] AS' 
END
GO
/*
    Sarah - Oct 10, 2016 - Remove column Status on User, Department and Result tables
    Sarah - Oct 31, 2016 - Hot fix: Get EntityStatus value on QDB
	Steve - Dec 8, 2016 - Job Fair project: merge Q and R DB, then change how to make a dynamic query
*/
ALTER PROCEDURE [dbo].[prc_Answer_Del_ByPageID]
	@SurveyID		int, 
	@ResultIDList	varchar(max),
	@PageIDList		varchar(max),
	@DeleteAll		bit =0,
	@ErrorMessage	nvarchar(max) OUTPUT,
	@DeleteResult   smallint = 0
AS
BEGIN
    SET NOCOUNT ON

    DECLARE @ReportServer nvarchar(64), @ReportDB nvarchar(64), @sqlCommand nvarchar(max), @QuestionIDList varchar(max), @Error int, @DeactiveEntityStatusID INT = 0
    DECLARE @DeactiveReasonEntityStatusID INT = 0, @LinkedDB NVARCHAR(MAX) = ' '

    SELECT @ReportServer = ReportServer, @ReportDB = ReportDB FROM at.Survey WHERE SurveyID = @SurveyID
	SELECT @QuestionIDList = STUFF((SELECT ',' + CONVERT(VARCHAR(16),q.QuestionID) AS [text()]
							 FROM at.Question q
							 WHERE q.PageID IN (SELECT Value FROM dbo.funcListToTableInt(@PageIDList,','))
							   AND (@DeleteAll=1 OR (q.ExtId != 'metadata' AND @DeleteAll=0)) 
							 FOR XML PATH ('')),1,1,'')

    SELECT @DeactiveEntityStatusID = EntityStatusID FROM dbo.EntityStatus WHERE CodeName = 'Deactive'
    SELECT @DeactiveReasonEntityStatusID = EntityStatusReasonID FROM EntityStatusReason WHERE CodeName='Deactive_ManuallySetDeactive'

	IF (@ReportServer = @@servername AND @ReportDB = DB_NAME())
		SET @LinkedDB = ' '
	ELSE 
		SET @LinkedDB = '[' + @ReportServer + '].[' + @ReportDB + '].' 
	
    SET @ErrorMessage = N''
    SET @sqlCommand = N'DELETE a FROM ' + @LinkedDB + '[dbo].Answer a
    WHERE a.ResultID IN (' + @ResultIDList + ')
      AND a.QuestionID IN (' + @QuestionIDList + ')'
    EXEC @Error = sp_executesql @sqlCommand
    --Case: Soft delete Result
    IF (@DeleteResult=1)
    BEGIN
        SET @sqlCommand = N'UPDATE ' + @LinkedDB + '[dbo].Result SET EntityStatusReasonID = '+CONVERT(NVARCHAR(14), @DeactiveReasonEntityStatusID)
        +', EntityStatusID = '+CONVERT(NVARCHAR(14), @DeactiveEntityStatusID)+'
        WHERE ResultID IN (' + @ResultIDList + ') AND NOT EXISTS (SELECT 1 FROM Answer  a WHERE a.ResultID =Result.ResultID)'
        EXEC @Error = sp_executesql @sqlCommand
    END
    --Case: Hard delete Result
    IF (@DeleteResult=2)
    BEGIN
        SET @sqlCommand = N'DELETE ' + @LinkedDB + '[dbo].Result
        WHERE ResultID IN (' + @ResultIDList + ') AND NOT EXISTS (SELECT 1 FROM Answer  a WHERE a.ResultID =Result.ResultID)'
        EXEC @Error = sp_executesql @sqlCommand
    END
    IF @Error != 0 SET @ErrorMessage = ERROR_NUMBER() + ' - ' + ERROR_MESSAGE()
END

GO
