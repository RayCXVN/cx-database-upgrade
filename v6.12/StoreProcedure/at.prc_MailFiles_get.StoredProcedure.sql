SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[at].[prc_MailFiles_get]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [at].[prc_MailFiles_get] AS' 
END
GO

ALTER PROCEDURE [at].[prc_MailFiles_get]
(
	@MailID int
)
AS
BEGIN
	SET NOCOUNT ON
	DECLARE @Err Int

	SELECT
	[MailFileID],
	[MIMEType],
	[Data],
	[Created],
	[Description],
	[Size],
	ISNULL([Filename], '') AS 'Filename',
	ISNULL([MailID], 0) AS 'MailID'
	FROM [at].[MailFiles]
	WHERE
	[MailID] = @MailID

	Set @Err = @@Error

	RETURN @Err
END


GO
