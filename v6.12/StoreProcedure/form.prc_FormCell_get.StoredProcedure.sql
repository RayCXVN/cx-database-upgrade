SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[form].[prc_FormCell_get]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [form].[prc_FormCell_get] AS' 
END
GO

ALTER PROCEDURE [form].[prc_FormCell_get]
(
	@FormRowID int
)
AS
BEGIN
	SET NOCOUNT ON
	DECLARE @Err Int

	SELECT
	[FormCellID],
	[FormRowID],
	[No],
	[CellTypeID],
	ISNULL([FormFieldID], 0) AS 'FormFieldID',
	ISNULL([FormCommandID], 0) AS 'FormCommandID',
	[CssClass],
	[Alignment],
	[Colspan],
	[Width],
	[Created]
	FROM [form].[FormCell]
	WHERE
	[FormRowID] = @FormRowID
	ORDER BY [No]

	Set @Err = @@Error

	RETURN @Err
END


GO
