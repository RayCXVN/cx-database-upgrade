SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[app].[prc_Menu_upd]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [app].[prc_Menu_upd] AS' 
END
GO


ALTER PROCEDURE [app].[prc_Menu_upd]
(
	@MenuID int,
	@OwnerID int,
	@No smallint,
	@Type smallint,
	@ThemeID int = null,
	@cUserid int,
	@Log smallint = 1
)
AS
BEGIN
	SET NOCOUNT ON
	DECLARE @Err Int

	UPDATE [app].[Menu]
	SET
		[OwnerID] = @OwnerID,
		[No] = @No,
		[Type] = @Type,
		[ThemeID] = @ThemeID 
	WHERE
		[MenuID] = @MenuID

	IF @Log = 1 
	BEGIN 
		INSERT INTO [Log].[AuditLog] ( UserId, TableName, Type, Data, Created) 
		SELECT @cUserid,'Menu',1,
		( SELECT * FROM [app].[Menu] 
			WHERE
			[MenuID] = @MenuID			 FOR XML AUTO) as data,
			getdate()
	END

	Set @Err = @@Error

	RETURN @Err
END

GO
