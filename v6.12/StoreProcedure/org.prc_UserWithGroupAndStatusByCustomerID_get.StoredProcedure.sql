SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[org].[prc_UserWithGroupAndStatusByCustomerID_get]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [org].[prc_UserWithGroupAndStatusByCustomerID_get] AS' 
END
GO
/*
    Steve - Sept 22 2016 - EntityStatus for table UserGroup
    2016-10-10 Ray:     Remove column Status from org.User, org.Department, RDB.dbo.Result
    2018-07-12 Ray:     Replace UG_U by UGMember
*/
ALTER PROCEDURE [org].[prc_UserWithGroupAndStatusByCustomerID_get](
    @CustomerID          int,
    @UserID              int,
    @UsertypeID          varchar(max),
    @UsertypeID2         int,
    @ActivityList        varchar(max),
    @ExcludeStatusTypeID varchar(max),
    @HasGroup            bit          = 1,
    @UserGroupIDList     varchar(max) = '',
    @PriorStatusTypeID   int          = 0
) AS
BEGIN
    SET NOCOUNT ON;
    DECLARE @ColumnDelimiter nvarchar(5) = CHAR(131) + CHAR(7),
            @RowDelimiter nvarchar(5) = CHAR(137) + CHAR(8),
            @ColumnDefinition nvarchar(max)= N'', -- in form of Activity_ + ID from @ActivityList
            @sqlCommand nvarchar(max)= N'', @UpdateCommand nvarchar(max)= N'', @Parameters nvarchar(max)= N'@p_StatusTypeID int,@p_UserID int',
            @ActivityID int, @i int= 0;

    DECLARE @ActiveEntityStatusID int = (SELECT [EntityStatusID] FROM [EntityStatus] WHERE [CodeName] = 'Active');
    DECLARE c_Activity CURSOR READ_ONLY
    FOR SELECT [ParseValue] FROM [dbo].[StringToArray] (@ActivityList, ',');

    DECLARE @UserTypeTable TABLE ([UserTypeID] int);
    INSERT INTO @UserTypeTable
    SELECT Value FROM [dbo].[funcListToTableInt] (@UsertypeID, ',');

    -- List of users
    SELECT [u].[UserID], [u].[Ownerid], [u].[DepartmentID], [u].[LanguageID], ISNULL([u].[RoleID], 0) AS 'RoleID', [u].[UserName], [u].[Password], [u].[LastName],
           [u].[FirstName], [u].[Email], [u].[Mobile], [u].[ExtID], [u].[SSN], [u].[Tag], [u].[Locked], [u].[ChangePassword], [u].[Created], [u].[EntityStatusID],
           [u].[Deleted], [u].[EntityStatusReasonID], [d].[Name] AS [DepartmentName],
           (SELECT CONVERT(nvarchar, [ug].[UserGroupID]) + @ColumnDelimiter + [ug].[Name] + @ColumnDelimiter + CONVERT(nvarchar, [ug].[UserGroupTypeID]) + @RowDelimiter
            FROM [org].[UGMember] [ugm]
            JOIN [org].[UserGroup] [ug] ON [ugm].[UserGroupID] = [ug].[UserGroupID] AND [ugm].[UserID] = [u].[UserID]
             AND [ug].[EntityStatusID] = @ActiveEntityStatusID AND [ug].[Deleted] IS NULL
             AND [ugm].[EntityStatusID] = @ActiveEntityStatusID AND [ugm].[Deleted] IS NULL
            FOR XML PATH('')
           ) AS 'StrUserGroups'
    INTO [#ResultStatus]
    FROM [org].[USER] [u]
    JOIN [org].[Department] [d] ON [u].[departmentid] = [d].[departmentid] AND [d].[CustomerID] = @CustomerID
     AND [d].[EntityStatusID] = @ActiveEntityStatusID AND [d].[Deleted] IS NULL
     AND [u].[EntityStatusID] = @ActiveEntityStatusID AND [u].[Deleted] IS NULL
    WHERE (ISNULL(@Usertypeid, '') = ''
           OR EXISTS (SELECT 1 FROM [org].[UT_U] [utu] JOIN @UserTypeTable ut ON ut.[UserTypeID] = utu.[UserTypeID] AND [utu].[UserID] = [u].[UserID])
          )
      AND (ISNULL(@UsertypeID2, 0) = 0
           OR EXISTS (SELECT 1 FROM [org].[UT_U] [sub] WHERE [sub].[UserID] = [u].[UserID] AND [sub].[UserTypeID] = @UsertypeID2)
          )
      AND ((@HasGroup = 0 AND @UserGroupIDList = '')
           OR EXISTS (SELECT 1 FROM [org].[UGMember] [ugm]
                               JOIN [org].[UserGroup] [ug] ON [ugm].[UserGroupID] = [ug].[UserGroupID] AND [ug].[UserID] = @UserID AND [ugm].[UserID] = [u].[UserID]
                                AND [ug].[EntityStatusID] = @ActiveEntityStatusID AND [ug].[Deleted] IS NULL
                                AND [ugm].[EntityStatusID] = @ActiveEntityStatusID AND [ugm].[Deleted] IS NULL
                              WHERE (@UserGroupIDList = ''
                                     OR [ug].[UserGroupID] IN (SELECT [ParseValue] FROM [dbo].[StringToArray] (@UserGroupIDList, ',') )
                                    )
                     )
          );

    DECLARE c_Users CURSOR SCROLL READ_ONLY
    FOR SELECT [UserID] FROM [#ResultStatus];
    OPEN c_Users;

    DECLARE @v_UserID int, @v_StatusTypeID int;

    SET @sqlCommand = 'ALTER TABLE #ResultStatus ADD ';

    OPEN c_Activity;
    FETCH NEXT FROM c_Activity INTO @ActivityID;
    WHILE @@FETCH_STATUS = 0
    BEGIN
        -- Add column to temp table to store result status
        SET @ColumnDefinition = @sqlCommand+N'Activity_'+CONVERT(nvarchar, @ActivityID)+N' int';
        EXECUTE [sp_executesql] @ColumnDefinition;

        FETCH FIRST FROM c_Users INTO @v_UserID;
        WHILE @@FETCH_STATUS = 0
        BEGIN
            EXEC @v_StatusTypeID = [at].[GetLatestStatusByActivity] @v_UserID, @ActivityID, @ExcludeStatusTypeID, @PriorStatusTypeID;

            SET @UpdateCommand = 'UPDATE #ResultStatus SET Activity_'+CONVERT(nvarchar, @ActivityID)+' = @p_StatusTypeID '+'WHERE UserID = @p_UserID';
            EXECUTE [sp_executesql] @UpdateCommand, @Parameters, @p_StatusTypeID = @v_StatusTypeID, @p_UserID = @v_UserID;
            FETCH NEXT FROM c_Users INTO @v_UserID;
        END;
        FETCH NEXT FROM c_Activity INTO @ActivityID;
    END;
    CLOSE c_Activity;
    DEALLOCATE c_Activity;

    CLOSE c_Users;
    DEALLOCATE c_Users;

    SELECT * FROM [#ResultStatus];

    DROP TABLE [#ResultStatus];
END;
GO