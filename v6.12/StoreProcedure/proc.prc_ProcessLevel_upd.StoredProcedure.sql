SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[proc].[prc_ProcessLevel_upd]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [proc].[prc_ProcessLevel_upd] AS' 
END
GO

ALTER PROCEDURE [proc].[prc_ProcessLevel_upd]
(
	@ProcessLevelID int,
	@ProcessValueID int,
	@ProcessID INT=NULL,
	@cUserid int,
	@Log smallint = 1
)
AS
BEGIN
	SET NOCOUNT ON
	DECLARE @Err Int

	UPDATE [proc].[ProcessLevel]
	SET
		[ProcessValueID] = @ProcessValueID,
		[ProcessID] = @ProcessID
	WHERE
		[ProcessLevelID] = @ProcessLevelID

	IF @Log = 1 
	BEGIN 
		INSERT INTO [Log].[AuditLog] ( UserId, TableName, Type, Data, Created) 
		SELECT @cUserid,'ProcessLevel',1,
		( SELECT * FROM [proc].[ProcessLevel] 
			WHERE
			[ProcessLevelID] = @ProcessLevelID			 FOR XML AUTO) as data,
			getdate()
	END

	Set @Err = @@Error

	RETURN @Err
END


GO
