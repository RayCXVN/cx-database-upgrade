SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[at].[prc_XCategoryType_upd]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [at].[prc_XCategoryType_upd] AS' 
END
GO
/*  
	2017-07-27 Steve: Change the length of column ExtID in all related tables from xAPI format
*/
ALTER PROCEDURE [at].[prc_XCategoryType_upd] 
(
	@XCategoryTypeID INT
	,@OwnerID INT
	,@ExtID NVARCHAR(256)
	,@No SMALLINT
	,@cUserid INT
	,@Log SMALLINT = 1
	)
AS
BEGIN
	SET NOCOUNT ON

	DECLARE @Err INT

	UPDATE [at].[XCategoryType]
	SET [OwnerID] = @OwnerID
		,[ExtID] = @ExtID
		,[No] = @No
	WHERE [XCTypeID] = @XCategoryTypeID

	IF @Log = 1
	BEGIN
		INSERT INTO [Log].[AuditLog] (
			UserId
			,TableName
			,Type
			,Data
			,Created
			)
		SELECT @cUserid
			,'XCategoryType'
			,1
			,(
				SELECT *
				FROM [at].[XCategoryType]
				WHERE [XCTypeID] = @XCategoryTypeID
				FOR XML AUTO
				) AS data
			,GETDATE()
	END

	SET @Err = @@Error

	RETURN @Err
END


GO
