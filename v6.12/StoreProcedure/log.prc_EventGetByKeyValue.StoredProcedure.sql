SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[log].[prc_EventGetByKeyValue]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [log].[prc_EventGetByKeyValue] AS' 
END
GO
ALTER PROCEDURE [log].[prc_EventGetByKeyValue]
    @EventTypeID    nvarchar(128)	='13,14',
    @EventKeyID     nvarchar(128)	='CustomerID',
    @Value          nvarchar(128)	='16',
    @EventKeyID2    int = 0,
    @Value2         nvarchar(128) = '',
    @HDID           int = 0,
    @FromDate       date = NULL,
    @ToDate         date = NULL,
    @PageIndex		INT =1,
	@PageSize		INT= 10,
	@UserIDList		NVARCHAR(MAX) ='0'
WITH RECOMPILE
AS
BEGIN
    DECLARE @sqlCommand nvarchar(max), @EntityStatusID INT = 0

	SELECT @EntityStatusID = EntityStatusID FROM dbo.EntityStatus WHERE CodeName = N'Active'

    CREATE TABLE #EventKey_tb (eventKey int)
    CREATE TABLE #UserIDTable (UserID int)
	
    INSERT INTO #EventKey_tb (eventKey) SELECT EventKeyID FROM log.EventKey WHERE EventTypeID IN (SELECT value FROM dbo.funcListToTableInt(@EventTypeID,',')) AND CodeName = @EventKeyID
	
    SET @sqlCommand = 'WITH ev AS 
    (
        SELECT ROW_NUMBER() OVER (ORDER BY e.EventID DESC) as [ROW], e.EventID, e.EventTypeID, e.UserID, e.IPNumber, e.TableTypeID, e.ItemID, e.Created, e.ApplicationName, e.UserData, e.DepartmentID, evi.Value, CASE WHEN u.[EntityStatusID] = ' + CONVERT(NVARCHAR(14), @EntityStatusID) + ' THEN 0 ELSE 1 END as [DeletedUser]
        FROM log.Event e
        JOIN log.EventInfo evi ON e.EventID = evi.EventID
         AND e.EventTypeID IN (' + @EventTypeID + ')
         AND evi.EventKeyID IN (SELECT eventKey FROM #EventKey_tb)
	     AND evi.Value IN (''' + replace(@Value, ',', ''',''') + ''')
        '

    IF @HDID > 0
    BEGIN
        SET @sqlCommand = @sqlCommand + ' AND EXISTS (SELECT 1 FROM org.H_D hd WHERE hd.DepartmentID = e.DepartmentID AND hd.[Path] LIKE ''%\' + CONVERT(nvarchar(16),@HDID) + '\%'')
        '
    END

    IF @FromDate IS NOT NULL
    BEGIN
        SET @sqlCommand = @sqlCommand + ' AND cast(e.Created as date) >= ''' + CONVERT(nvarchar(32),@FromDate,120) + '''
        '
    END

    IF @ToDate IS NOT NULL
    BEGIN
        SET @sqlCommand = @sqlCommand + ' AND cast(e.Created as date) <= ''' + CONVERT(nvarchar(32),@ToDate,120) + '''
        '
    END
    
    IF @UserIDList != '0' AND @UserIDList != ''
    BEGIN
        INSERT INTO #UserIDTable (UserID) SELECT Value FROM dbo.funcListToTableInt(@UserIDList, ',')
        SET @sqlCommand = @sqlCommand + ' JOIN #UserIDTable uit ON e.UserID = uit.UserID
        '
    END

    SET @sqlCommand = @sqlCommand + ' LEFT JOIN org.[User] u ON e.UserID = u.UserID
    )
    SELECT ev.EventID, ev.EventTypeID, ev.UserID, ev.IPNumber, ev.TableTypeID, ev.ItemID, ev.Created, ev.ApplicationName, ev.UserData, ev.DepartmentID, ev.Value, ev.[DeletedUser]
    FROM  ev
    '
    IF @EventKeyID2 > 0
    BEGIN
        SET @sqlCommand = @sqlCommand + ' JOIN log.EventInfo evi2 ON ev.EventID = evi2.EventID AND evi2.EventKeyID = ' + CONVERT(nvarchar(32),@EventKeyID2)
                                      + ' AND evi2.Value = ''' + @Value2 + '''
    '
    END

    IF @PageSize > 0
    BEGIN
        SET @sqlCommand = @sqlCommand + ' WHERE ev.[Row] BETWEEN ' + CONVERT(nvarchar(16), (@PageIndex-1)*@PageSize+1) + ' AND ' + CONVERT(nvarchar(16), @PageIndex*@PageSize)
    END
    
    SET @sqlCommand = @sqlCommand + ' ORDER BY ev.EventID DESC'
    
    PRINT @sqlCommand
    EXEC sp_executesql @sqlCommand

    DROP TABLE #EventKey_tb
    DROP TABLE #UserIDTable
END

GO
