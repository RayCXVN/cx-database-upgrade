SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT 1 FROM sys.objects WHERE name = N'prc_UserGroup_upd' AND type IN ( N'P', N'PC' )  AND SCHEMA_NAME(schema_id) = 'org')
    EXEC ('CREATE PROC [org].[prc_UserGroup_upd] AS ')
GO
/*  
    2016-11-02 Steve: Set default CurrentDate for @LastUpdated, @LastSynchronized
    2017-07-27 Steve: Change the length of column ExtID in all related tables from xAPI format
    2018-07-05 Sarah: ATAdmin/CXPLAT-294/Extend UG_UCXPLAT-854
*/
ALTER PROCEDURE [org].[prc_UserGroup_upd](
    @UserGroupID          int,
    @OwnerID              int,
    @DepartmentID         int,
    @SurveyId             int           = NULL,
    @Name                 nvarchar(256),
    @Description          ntext,
    @PeriodID             int           = NULL,
    @ExtID                nvarchar(256) = '',
    @Userid               int           = NULL,
    @UserGroupTypeID      int           = NULL,
    @Tag                  nvarchar(max) = '',
    @cUserid              int,
    @Log                  smallint      = 1,
    @LastUpdated          datetime2     = NULL,
    @LastUpdatedBy        int           = NULL,
    @LastSynchronized     datetime2     = NULL,
    @ArchetypeID          int           = NULL,
    @Deleted              datetime2     = NULL,
    @EntityStatusID       int           = NULL,
    @EntityStatusReasonID int           = NULL,
    @ReferrerArchetypeID  int           = NULL,
    @ReferrerResource     nvarchar(max) = '',
    @ReferrerToken        nvarchar(max) = ''
) AS
BEGIN
    SET NOCOUNT ON;
    DECLARE @Err int;

    IF @LastUpdated IS NULL
    BEGIN
        SET @LastUpdated = GETDATE()
    END;

    IF @LastSynchronized IS NULL
    BEGIN
        SET @LastSynchronized = GETDATE()
    END;

    UPDATE [org].[UserGroup]
    SET [OwnerID] = @OwnerID, [DepartmentID] = @DepartmentID, [SurveyId] = @SurveyId, [Name] = @Name, [Description] = @Description, [PeriodID] = @PeriodID,
        [ExtID] = @ExtID, [UserID] = @Userid, [UserGroupTypeID] = @UserGroupTypeID, [Tag] = @Tag, [LastUpdated] = @LastUpdated, [LastUpdatedBy] = @LastUpdatedBy,
        [LastSynchronized] = @LastSynchronized, [ArchetypeID] = @ArchetypeID, [Deleted] = @Deleted, [EntityStatusID] = @EntityStatusID, [EntityStatusReasonID] = @EntityStatusReasonID,
        [ReferrerArchetypeID] = @ReferrerArchetypeID, [ReferrerResource] = @ReferrerResource, [ReferrerToken] = @ReferrerToken
    WHERE [UserGroupID] = @UserGroupID;

    IF @Log = 1
    BEGIN
        INSERT INTO [Log].[AuditLog] ([UserId], [TableName], [Type], [Data], [Created])
        SELECT @cUserid, 'UserGroup', 1, (SELECT * FROM [org].[UserGroup] WHERE [UserGroupID] = @UserGroupID FOR XML AUTO) AS [data], GETDATE();
    END;

    SET @Err = @@Error;

    RETURN @Err;
END;
GO
