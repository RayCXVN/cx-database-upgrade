SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[at].[prc_DR_C_ins]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [at].[prc_DR_C_ins] AS' 
END
GO

ALTER PROCEDURE [at].[prc_DR_C_ins]
(
	@DottedRuleID int,
	@CategoryID int,
	@cUserid int,
	@Log smallint = 1
)
AS
BEGIN
	SET NOCOUNT ON
	DECLARE @Err Int

	INSERT INTO [at].[DR_C]
	(
		[DottedRuleID],
		[CategoryID]
	)
	VALUES
	(
		@DottedRuleID,
		@CategoryID
	)

	Set @Err = @@Error

	IF @Log = 1 
	BEGIN 
		INSERT INTO [Log].[AuditLog] ( UserId, TableName, Type, Data, Created) 
		SELECT @cUserid,'DR_C',0,
		( SELECT * FROM [at].[DR_C] 
			WHERE
			[DottedRuleID] = @DottedRuleID AND
			[CategoryID] = @CategoryID				 FOR XML AUTO) as data,
				getdate() 
	 END

	RETURN @Err
END


GO
