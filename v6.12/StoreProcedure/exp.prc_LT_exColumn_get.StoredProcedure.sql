SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[exp].[prc_LT_exColumn_get]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [exp].[prc_LT_exColumn_get] AS' 
END
GO

ALTER PROCEDURE [exp].[prc_LT_exColumn_get]
(
	@ColumnID smallint
)
AS
BEGIN
	SET NOCOUNT ON
	DECLARE @Err Int

	SELECT
	[LanguageID],
	[ColumnID],
	[Name]
	FROM [exp].[LT_exColumn]
	WHERE
	[ColumnID] = @ColumnID

	Set @Err = @@Error

	RETURN @Err
END


GO
