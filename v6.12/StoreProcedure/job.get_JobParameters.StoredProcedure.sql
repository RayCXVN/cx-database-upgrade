SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[job].[get_JobParameters]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [job].[get_JobParameters] AS' 
END
GO


ALTER PROC [job].[get_JobParameters]
(
	@JobID	bigint
)
AS
SELECT 
  JobParameterId as Id,
  [No],
  [Name], 
  Value,
  [Type]
FROM JobParameter 
WHERE JobID = @JobID ORDER BY No

GO
