SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[org].[prc_GetTopDepartmentByCustomerID]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [org].[prc_GetTopDepartmentByCustomerID] AS' 
END
GO
ALTER PROCEDURE [org].[prc_GetTopDepartmentByCustomerID]
(   
    @CustomerID     int
)
AS
BEGIN
	DECLARE @EntityStatusID INT = 0
	SELECT @EntityStatusID = EntityStatusID FROM dbo.EntityStatus WHERE CodeName = N'Active'

    SELECT TOP 1 hd.[HDID], hd.[DepartmentID]
    FROM [org].[H_D] hd JOIN [org].[Department] d ON hd.[DepartmentID] = d.[DepartmentID] AND hd.[Deleted] = 0 
	AND d.EntityStatusID = @EntityStatusID 
	AND d.Deleted IS NULL
	AND d.[CustomerID] = @CustomerID
    ORDER BY LEN(hd.[Path])
END


GO
