SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[app].[prc_Site_copy]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [app].[prc_Site_copy] AS' 
END
GO
ALTER PROCEDURE [app].[prc_Site_copy]  
(  
 @SiteID int,
 @NewSiteID int = null output  
)  
AS  
BEGIN  

 SET XACT_ABORT ON  
 BEGIN TRANSACTION  
 DECLARE @Err Int,  
 @OwnerID int,    
 @CodeName nvarchar(256),    
 @UseNativeLogon bit,    
 @ThemeID int,    
 @IsClosed bit,    
 @DefaultMenuID int,    
 @DefaultHierarchyID int,    
 @UseMultipleMenus bit,    
 @DefaultChartTemplate nvarchar(256),    
 @UseCustomer bit,    
 @UsePasswordRecovery bit,    
 @MaintenanceMode bit,    
 @UseRememberMe bit,  
 @cUserid int,
 @UseReturnURL bit=0,
 @UseSMS bit =0,
 @UseMemoryCache			bit	,			
 @UseDistributedCache		bit	,			
 @DistributedCacheName      nvarchar(64),
 @CacheExpiredAfter		    int	,
 @MailHeaderInfoXCompany nvarchar(64)='',
 @MailHeaderInfoMessageID nvarchar(64) =''	,		
 @LanguageIDWhenNotAuthenticated int =0,
 @LogoutURL			nvarchar(max) ='',
 @TimeOutLogoutURL nvarchar(max) =''
 
 
 SELECT  
      @OwnerID = [OwnerID]
      ,@CodeName = [CodeName]
      ,@UseNativeLogon = [UseNativeLogon]
      ,@ThemeID = [ThemeID]
      ,@IsClosed = [IsClosed]
      ,@DefaultMenuID = [DefaultMenuID]
      ,@DefaultHierarchyID = [DefaultHierarchyID]
      ,@UseMultipleMenus = [UseMultipleMenus]
      ,@DefaultChartTemplate = [DefaultChartTemplate]
      ,@UseCustomer = [UseCustomer]
      ,@UsePasswordRecovery = [UsePasswordRecovery]
      ,@MaintenanceMode = [MaintenanceMode]
      ,@UseRememberMe = [UseRememberMe]
      ,@UseReturnURL =	[UseReturnURL]
      ,@UseSMS=[UseSMS]
	  ,@UseMemoryCache		=UseMemoryCache		
	  ,@UseDistributedCache	=UseDistributedCache	
	  ,@DistributedCacheName=DistributedCacheName  
	  ,@CacheExpiredAfter	=CacheExpiredAfter		
	  ,@MailHeaderInfoXCompany =MailHeaderInfoXCompany
	  ,@MailHeaderInfoMessageID=MailHeaderInfoMessageID   
	  ,@LanguageIDWhenNotAuthenticated=LanguageIDWhenNotAuthenticated
	  ,@LogoutURL		=	LogoutURL
	  ,@TimeOutLogoutURL=TimeOutLogoutURL
	  
	  
	  
 FROM [app].[Site]   
 WHERE  
  [SiteID] = @SiteID  
  
   INSERT INTO [app].[Site]  
	 ([OwnerID]    
           ,[CodeName]    
           ,[UseNativeLogon]    
           ,[ThemeID]    
           ,[IsClosed]    
           ,[DefaultMenuID]    
           ,[DefaultHierarchyID]    
           ,[UseMultipleMenus]    
           ,[DefaultChartTemplate]    
           ,[UseCustomer]    
           ,[UsePasswordRecovery]  
           ,[MaintenanceMode]  
           ,[UseRememberMe]
           ,[UseReturnURL]
           ,UseSMS
		   ,UseMemoryCache		
		   ,UseDistributedCache	
		   ,DistributedCacheName  
		   ,CacheExpiredAfter	
			,MailHeaderInfoXCompany
			,MailHeaderInfoMessageID  
		   	,LanguageIDWhenNotAuthenticated
			,LogoutURL		
			,TimeOutLogoutURL
		   	
		   	
           )    
	 VALUES  
	 (@OwnerID    
           ,@CodeName    
           ,@UseNativeLogon    
           ,@ThemeID    
           ,@IsClosed    
           ,@DefaultMenuID    
           ,@DefaultHierarchyID    
           ,@UseMultipleMenus    
           ,@DefaultChartTemplate    
           ,@UseCustomer    
           ,@UsePasswordRecovery  
           ,@MaintenanceMode   
           ,@UseRememberMe
           ,@UseReturnURL
           ,@UseSMS
           ,@UseMemoryCache		   
		   ,@UseDistributedCache	
		   ,@DistributedCacheName
		   ,@CacheExpiredAfter
			,@MailHeaderInfoXCompany
			,@MailHeaderInfoMessageID  
			,@LanguageIDWhenNotAuthenticated
			,@LogoutURL			
			,@TimeOutLogoutURL		   
		   ) 	
  
  
  Set @NewSiteID = scope_identity()  
  
   --insert LT_Site
  INSERT INTO [app].[LT_Site]
  ([LanguageID]
      ,[SiteID]
      ,[Name]
      ,[Description]
      ,[LoginText]
      ,[FooterText]
      ,[SupportText]
      ,[ClosedText]
      ,[ExtraFooterTextAfterAuthentication]
      ,[HeadData]
	  ,BaseURL
	  ,ShortName
	  )
  (SELECT [LanguageID]
      ,@NewSiteID
      ,[Name]
      ,[Description]
      ,[LoginText]
      ,[FooterText]
      ,[SupportText]
      ,[ClosedText]
      ,[ExtraFooterTextAfterAuthentication]
      ,[HeadData]
      ,[BaseURL]
      ,ShortName
   FROM [app].[LT_Site]
   WHERE ([SiteID] = @SiteID))
 
 
 --insert Site_language
  INSERT INTO [app].[Site_Language]
  ([LanguageID]
      ,[SiteID]
      ,[No])
  (SELECT [LanguageID]
      ,@NewSiteID
      ,[No]
   FROM [app].[Site_Language]
   WHERE ([SiteID] = @SiteID))
 
  --insert SiteParameter
  INSERT INTO [app].[SiteParameter]
  ([SiteID]
      ,[Key]
      ,[Value])
  (SELECT @NewSiteID
      ,[Key]
      ,[Value]
   FROM [app].[SiteParameter]
   WHERE ([SiteID] = @SiteID))
 
 --insert SitePortalPart
  INSERT INTO [app].[SitePortalpartSetting]
   (
      [SiteID]
      ,[No]
      ,[RenderQuestionIfDottedOrZeroForAllSelections]
      ,[RenderCategoryIfDottedOrZeroForAllSelections]
      ,[RenderReportPartRowIfDottedOrZeroForAllSelections]
      ,[ShowAverageOnFactorPortalPart]
      ,[ShowLevelLimitsOnFactorPortalPart]
      ,[ShowLevelOnFactorPortalPart])
    (SELECT 
		@NewSiteID
      ,[No]
      ,[RenderQuestionIfDottedOrZeroForAllSelections]
      ,[RenderCategoryIfDottedOrZeroForAllSelections]
      ,[RenderReportPartRowIfDottedOrZeroForAllSelections]
      ,[ShowAverageOnFactorPortalPart]
      ,[ShowLevelLimitsOnFactorPortalPart]
      ,[ShowLevelOnFactorPortalPart]
   FROM [app].[SitePortalpartSetting]
   WHERE ([SiteID] = @SiteID))
   
   --Insert SiteProcess
   INSERT INTO [app].[SiteProcessSetting]
   ([SiteID]
      ,[No]
      ,[UseLocalProcessGroups]
      ,[AllCanAddAnswers]
      ,[UsePeriod]
      ,[CanChangeuser]
      ,[ShowOnlyAnswersBlowOnAdminPage]
      ,[CanAddProcessGroup]
      ,[CanCreatorEditAnswer]
      ,[ShowEditButtonOnProcessAnswer])
    (SELECT @NewSiteID
      ,[No]
      ,[UseLocalProcessGroups]
      ,[AllCanAddAnswers]
      ,[UsePeriod]
      ,[CanChangeuser]
      ,[ShowOnlyAnswersBlowOnAdminPage]
      ,[CanAddProcessGroup]
      ,[CanCreatorEditAnswer]
      ,[ShowEditButtonOnProcessAnswer]
   FROM [app].[SiteProcessSetting]
   WHERE ([SiteID] = @SiteID))
   
   
    --Insert SiteUserSetting
   INSERT INTO [app].[SiteUserSetting]
   ( [UserID]
      ,[SiteID]
      ,[EditMode]
      ,[AutoFitScreen]
      ,[ScreenResolution]
      ,[ExportTooltip])
    (SELECT 
       [UserID]
      ,@NewSiteID
      ,[EditMode]
      ,[AutoFitScreen]
      ,[ScreenResolution]
      ,[ExportTooltip]
   FROM [app].[SiteUserSetting]
   WHERE ([SiteID] = @SiteID))
   
   
       --Insert SiteViewSetting
   INSERT INTO [app].[SiteViewSetting]
   ( [SiteID]
      ,[No]
      ,[NumOfDecimals]
      ,[TableIsSelectable]
      ,[TableIsDefaultSelected]
      ,[ChartIsSelectable]
      ,[ChartIsDefaultSelected]
      ,[AverageIsSelectable]
      ,[AverageIsDefaultSelected]
      ,[TrendIsSelectable]
      ,[TrendIsDefaultSelected]
      ,[FrequencyIsSelectable]
      ,[FrequencyIsDefaultSelected]
      ,[NCountIsSelectable]
      ,[NCountIsDefaultSelected]
      ,[StandardDeviationIsSelectable]
      ,[StandardDeviationIsDefaultSelected])
    (SELECT 
       @NewSiteID
      ,[No]
      ,[NumOfDecimals]
      ,[TableIsSelectable]
      ,[TableIsDefaultSelected]
      ,[ChartIsSelectable]
      ,[ChartIsDefaultSelected]
      ,[AverageIsSelectable]
      ,[AverageIsDefaultSelected]
      ,[TrendIsSelectable]
      ,[TrendIsDefaultSelected]
      ,[FrequencyIsSelectable]
      ,[FrequencyIsDefaultSelected]
      ,[NCountIsSelectable]
      ,[NCountIsDefaultSelected]
      ,[StandardDeviationIsSelectable]
      ,[StandardDeviationIsDefaultSelected]
   FROM [app].[SiteViewSetting]
   WHERE ([SiteID] = @SiteID))
   
   
    --Insert SiteTab
   INSERT INTO [app].[Tab]
   ( [SiteID]
      ,[MenuItemID]
      ,[Settings]
      ,[Created])
    (SELECT 
       @NewSiteID
      ,[MenuItemID]
      ,[Settings]
      ,[Created]
   FROM [app].[Tab]
   WHERE ([SiteID] = @SiteID))
 COMMIT TRANSACTION  
    
END  

GO
