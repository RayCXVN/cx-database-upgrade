SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[at].[prc_QuestionsByBulkList_get]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [at].[prc_QuestionsByBulkList_get] AS' 
END
GO
ALTER PROCEDURE [at].[prc_QuestionsByBulkList_get]
	@BulkIDs varchar(Max)
AS
BEGIN
	SELECT bp.BulkID, q.QuestionID from at.Question q
	INNER JOIN at.B_P bp ON q.PageID = bp.PageID
	WHERE bp.BulkID IN (select value from dbo.funcListToTableInt(@BulkIDs,','))	
	UNION ALL 
	SELECT bq.BulkID, q.QuestionID from at.Question q
	INNER JOIN at.B_Q bq ON q.QuestionID = bq.QuestionID	
	WHERE bq.BulkID IN (select value from dbo.funcListToTableInt(@BulkIDs,','))
	ORDER BY bp.BulkID	
END

GO
