SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[proc].[prc_ProcessAnswer_upd]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [proc].[prc_ProcessAnswer_upd] AS' 
END
GO

ALTER PROCEDURE [proc].[prc_ProcessAnswer_upd]
(
	@ProcessAnswerID int,
	@ProcessID int,
	@ProcessLevelID int,
	@DepartmentID int,
	@RoleID INT=NULL,
	@UserID INT=NULL,
	@LastModified datetime,
	@cUserid int,
	@Log smallint = 1
)
AS
BEGIN
	SET NOCOUNT ON
	DECLARE @Err Int

	UPDATE [proc].[ProcessAnswer]
	SET
		[ProcessID] = @ProcessID,
		[ProcessLevelID] = @ProcessLevelID,
		[DepartmentID] = @DepartmentID,
		[RoleID] = @RoleID,
		[UserID] = @UserID,
		[LastModified] = @LastModified
	WHERE
		[ProcessAnswerID] = @ProcessAnswerID

	IF @Log = 1 
	BEGIN 
		INSERT INTO [Log].[AuditLog] ( UserId, TableName, Type, Data, Created) 
		SELECT @cUserid,'ProcessAnswer',1,
		( SELECT * FROM [proc].[ProcessAnswer] 
			WHERE
			[ProcessAnswerID] = @ProcessAnswerID			 FOR XML AUTO) as data,
			getdate()
	END

	Set @Err = @@Error

	RETURN @Err
END


GO
