SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[proc].[prc_PT_PT_upd]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [proc].[prc_PT_PT_upd] AS' 
END
GO

ALTER PROCEDURE [proc].[prc_PT_PT_upd]
(
	@FromProcessTypeID int,
	@ToProcessTypeID int,
	@cUserid int,
	@Log smallint = 1
)
AS
BEGIN
	SET NOCOUNT ON
	DECLARE @Err Int

	UPDATE [proc].[PT_PT]
	SET
		[FromProcessTypeID] = @FromProcessTypeID,
		[ToProcessTypeID] = @ToProcessTypeID
	WHERE
		[FromProcessTypeID] = @FromProcessTypeID AND
		[ToProcessTypeID] = @ToProcessTypeID

	IF @Log = 1 
	BEGIN 
		INSERT INTO [Log].[AuditLog] ( UserId, TableName, Type, Data, Created) 
		SELECT @cUserid,'PT_PT',1,
		( SELECT * FROM [proc].[PT_PT] 
			WHERE
			[FromProcessTypeID] = @FromProcessTypeID AND
			[ToProcessTypeID] = @ToProcessTypeID			 FOR XML AUTO) as data,
			getdate()
	END

	Set @Err = @@Error

	RETURN @Err
END

GO
