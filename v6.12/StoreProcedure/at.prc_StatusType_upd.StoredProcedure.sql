SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[at].[prc_StatusType_upd]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [at].[prc_StatusType_upd] AS' 
END
GO
/*
	2017-05-25 Sarah	Implement configuring StatusType's CodeName in ATAdmin for RelayLog
*/
ALTER PROCEDURE [at].[prc_StatusType_upd]
(
	@StatusTypeID int,
	@OwnerID INT=NULL,
	@Type int,
	@No smallint,
	@Value float = 0,
	@cUserid int,
	@CodeName NVARCHAR(50),
	@Log smallint = 1
)
AS
BEGIN
	SET NOCOUNT ON
	DECLARE @Err Int

	UPDATE [at].[StatusType]
	SET
		[OwnerID] = @OwnerID,
		[Type] = @Type,
		[No] = @No,
		[Value] = @Value,
		[CodeName] = @CodeName
	WHERE
		[StatusTypeID] = @StatusTypeID

	IF @Log = 1 
	BEGIN 
		INSERT INTO [Log].[AuditLog] ( UserId, TableName, Type, Data, Created) 
		SELECT @cUserid,'StatusType',1,
		( SELECT * FROM [at].[StatusType] 
			WHERE
			[StatusTypeID] = @StatusTypeID			 FOR XML AUTO) as data,
			getdate()
	END

	Set @Err = @@Error

	RETURN @Err
END


GO
