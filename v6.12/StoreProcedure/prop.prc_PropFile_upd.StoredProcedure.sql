SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[prop].[prc_PropFile_upd]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [prop].[prc_PropFile_upd] AS' 
END
GO
ALTER PROCEDURE [prop].[prc_PropFile_upd]
(
	@PropFileID int,
	@OwnerId int,
	@CustomerId INT=NULL,
	@ContentType nvarchar(64),
	@Size bigint,
	@Title nvarchar(512),
	@Filename nvarchar(256),
	@Description nvarchar(128),
	@FileData varbinary(max),
	@cUserid int,
	@Log smallint = 1
)
AS
BEGIN
	SET NOCOUNT ON
	DECLARE @Err Int

	UPDATE [prop].[PropFile]
	SET
		[OwnerId] = @OwnerId,
		[CustomerId] = @CustomerId,
		[ContentType] = @ContentType,
		[Size] = @Size,
		[Title] = @Title,
		[Filename] = @Filename,
		[Description] = @Description,
		[FileData] = @FileData
	WHERE
		[PropFileID] = @PropFileID

	IF @Log = 1 
	BEGIN 
		INSERT INTO [Log].[AuditLog] ( UserId, TableName, Type, Data, Created) 
		SELECT @cUserid,'PropFile',33,
		(	SELECT [PropFileId],[GUID],[Deleted],[OwnerId],[CustomerId],[ContentType],[Size],[Title],[FileName],[Description]
			FROM [prop].[PropFile] 
			WHERE [PropFileID] = @PropFileID FOR XML AUTO) as data,
		getdate() 
	END

	Set @Err = @@Error

	RETURN @Err
END

GO
