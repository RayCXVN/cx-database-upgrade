SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[proc].[prc_ProcessType_ins]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [proc].[prc_ProcessType_ins] AS' 
END
GO
ALTER PROCEDURE [proc].[prc_ProcessType_ins]
(
	@ProcessTypeID int = null output,
	@No smallint,
	@OwnerID int,
	@CssClass nvarchar(128) = '',
	@DepartmentSelect bit = 1,
	@UserSelect bit = 0,
	@FilesAbove bit = 1,
	@cUserid int,
	@Log smallint = 1
)
AS
BEGIN
	SET NOCOUNT ON
	DECLARE @Err Int

	INSERT INTO [proc].[ProcessType]
	(
		[No],
		[OwnerID],
		[CssClass],
		[DepartmentSelect],
		[UserSelect],
		[FilesAbove]
	)
	VALUES
	(
		@No,
		@OwnerID,
		@CssClass,
		@DepartmentSelect,
		@UserSelect,
		@FilesAbove
	)

	Set @Err = @@Error
	Set @ProcessTypeID = scope_identity()

	IF @Log = 1 
	BEGIN 
		INSERT INTO [Log].[AuditLog] ( UserId, TableName, Type, Data, Created) 
		SELECT @cUserid,'ProcessType',0,
		( SELECT * FROM [proc].[ProcessType] 
			WHERE
			[ProcessTypeID] = @ProcessTypeID				 FOR XML AUTO) as data,
				getdate() 
	 END

	RETURN @Err
END

GO
