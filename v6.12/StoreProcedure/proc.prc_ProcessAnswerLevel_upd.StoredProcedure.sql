SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[proc].[prc_ProcessAnswerLevel_upd]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [proc].[prc_ProcessAnswerLevel_upd] AS' 
END
GO

ALTER PROCEDURE [proc].[prc_ProcessAnswerLevel_upd]
(
	@ProcessAnswerID int,
	@ProcessLevelID int,
	@Description nvarchar(max),
	@cUserid int,
	@Log smallint = 1
)
AS
BEGIN
	SET NOCOUNT ON
	DECLARE @Err Int

	UPDATE [proc].[ProcessAnswerLevel]
	SET
		[ProcessAnswerID] = @ProcessAnswerID,
		[ProcessLevelID] = @ProcessLevelID,
		[Description] = @Description
	WHERE
		[ProcessAnswerID] = @ProcessAnswerID AND
		[ProcessLevelID] = @ProcessLevelID

	IF @Log = 1 
	BEGIN 
		INSERT INTO [Log].[AuditLog] ( UserId, TableName, Type, Data, Created) 
		SELECT @cUserid,'ProcessAnswerLevel',1,
		( SELECT * FROM [proc].[ProcessAnswerLevel] 
			WHERE
			[ProcessAnswerID] = @ProcessAnswerID AND
			[ProcessLevelID] = @ProcessLevelID			 FOR XML AUTO) as data,
			getdate()
	END

	Set @Err = @@Error

	RETURN @Err
END


GO
