SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[app].[prc_PortalPartType_del]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [app].[prc_PortalPartType_del] AS' 
END
GO
ALTER PROCEDURE [app].[prc_PortalPartType_del]
(
	@PortalPartTypeID int,
	@cUserid int,
	@Log smallint = 1
)
AS
BEGIN
	SET NOCOUNT ON
	DECLARE @Err Int

	IF @Log = 1 
	BEGIN 
		INSERT INTO [Log].[AuditLog] ( UserId, TableName, Type, Data, Created) 
		SELECT @cUserid,'PortalPartType',2,
		( SELECT * FROM [app].[PortalPartType] 
			WHERE
			[PortalPartTypeID] = @PortalPartTypeID
			 FOR XML AUTO) as data,
			getdate() 
	END 


	DELETE FROM [app].[PortalPartType]
	WHERE
		[PortalPartTypeID] = @PortalPartTypeID

	Set @Err = @@Error

	RETURN @Err
END


GO
