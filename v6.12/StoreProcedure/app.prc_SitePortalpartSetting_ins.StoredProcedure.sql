SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[app].[prc_SitePortalpartSetting_ins]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [app].[prc_SitePortalpartSetting_ins] AS' 
END
GO
ALTER PROCEDURE [app].[prc_SitePortalpartSetting_ins]
	@SitePortalpartSettingID int = null output,
	@SiteID int,
	@No int,
	@RenderQuestionIfDottedOrZeroForAllSelections bit,
	@RenderCategoryIfDottedOrZeroForAllSelections bit,
	@RenderReportPartRowIfDottedOrZeroForAllSelections bit,
	@ShowAverageOnFactorPortalPart bit,
	@ShowLevelLimitsOnFactorPortalPart bit,
	@ShowLevelOnFactorPortalPart bit,
	@cUserid int,
    @Log smallint = 1
AS
BEGIN
	SET NOCOUNT ON;
	DECLARE @Err Int
    
    INSERT INTO [app].[SitePortalpartSetting]
           ([SiteID]
           ,[No]
           ,[RenderQuestionIfDottedOrZeroForAllSelections]
           ,[RenderCategoryIfDottedOrZeroForAllSelections]
           ,[RenderReportPartRowIfDottedOrZeroForAllSelections]
           ,[ShowAverageOnFactorPortalPart]
           ,[ShowLevelLimitsOnFactorPortalPart]
           ,[ShowLevelOnFactorPortalPart])
     VALUES
           (@SiteID
           ,@No
           ,@RenderQuestionIfDottedOrZeroForAllSelections
           ,@RenderCategoryIfDottedOrZeroForAllSelections
           ,@RenderReportPartRowIfDottedOrZeroForAllSelections
           ,@ShowAverageOnFactorPortalPart
           ,@ShowLevelLimitsOnFactorPortalPart
           ,@ShowLevelOnFactorPortalPart)
           
    Set @Err = @@Error
    Set @SitePortalpartSettingID = scope_identity()
	IF @Log = 1 
	BEGIN 
		INSERT INTO [Log].[AuditLog] ( UserId, TableName, Type, Data, Created) 
		SELECT @cUserid,'SitePortalpartSetting',0,
		( SELECT * FROM [app].[SitePortalpartSetting]
			WHERE
			[SitePortalpartSettingID] = @SitePortalpartSettingID				 FOR XML AUTO) as data,
				getdate() 
	END
	
	RETURN @Err       
END

GO
