SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[at].[prc_Category_ins]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [at].[prc_Category_ins] AS' 
END
GO
/*  
	2017-06-13 Steve: Change the length of column ExtID in all related tables from xAPI format
*/
ALTER PROCEDURE [at].[prc_Category_ins]      
(      
 @CategoryID int = null output,      
 @ActivityID int,      
 @ParentID INT=NULL,      
 @Tag nvarchar(128),      
 @Type smallint,      
 @No smallint,      
 @Fillcolor varchar(16),      
 @BorderColor varchar(16),      
 @Weight float,      
 @NType smallint,      
 @ProcessCategory bit,      
 @cUserid int,      
 @Log smallint = 1,  
 @ExtID nvarchar(256),
 @AnswerItemID int = null ,
 @RenderAsDefault bit =1,
 @AvgType smallint = 0
)      
AS      
BEGIN      
 SET NOCOUNT ON      
 DECLARE @Err Int      
      
 INSERT INTO [at].[Category]      
 (      
  [ActivityID],      
  [ParentID],      
  [Tag],      
  [Type],      
  [No],      
  [Fillcolor],      
  [BorderColor],      
  [Weight],      
  [NType],    
  [ProcessCategory],      
  [ExtID],
  [AnswerItemID]  ,
  [RenderAsDefault],
  [AvgType]
 )      
 VALUES      
 (      
  @ActivityID,      
  @ParentID,      
  @Tag,      
  @Type,      
  @No,      
  @Fillcolor,      
  @BorderColor,      
  @Weight,      
  @NType,    
  @ProcessCategory,  
  @ExtID,
  @AnswerItemID,
  @RenderAsDefault,
  @AvgType  
 )      
      
 Set @Err = @@Error      
 Set @CategoryID = scope_identity()      
      
 IF @Log = 1       
 BEGIN       
  INSERT INTO [Log].[AuditLog] ( UserId, TableName, Type, Data, Created)       
  SELECT @cUserid,'Category',0,      
  ( SELECT * FROM [at].[Category]       
   WHERE      
   [CategoryID] = @CategoryID     FOR XML AUTO) as data,      
    getdate()       
  END      
      
 RETURN @Err      
END 

GO
