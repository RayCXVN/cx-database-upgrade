SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[at].[prc_LT_XCategoryType_get]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [at].[prc_LT_XCategoryType_get] AS' 
END
GO

ALTER PROCEDURE [at].[prc_LT_XCategoryType_get] (@XCategoryTypeID INT)
AS
BEGIN
	SET NOCOUNT ON

	DECLARE @Err INT

	SELECT [LanguageID]
		,[XCTypeID]
		,[Name]
		,[Description]
	FROM [at].[LT_XCategoryType]
	WHERE [XCTypeID] = @XCategoryTypeID

	SET @Err = @@Error

	RETURN @Err
END

GO
