SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[at].[prc_LT_StatusType_del]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [at].[prc_LT_StatusType_del] AS' 
END
GO
ALTER PROCEDURE [at].[prc_LT_StatusType_del]
(
	@LanguageID int,
	@StatusTypeID int,
	@cUserid int,
	@Log smallint = 1
)
AS
BEGIN
	SET NOCOUNT ON
	DECLARE @Err Int

	IF @Log = 1 
	BEGIN 
		INSERT INTO [Log].[AuditLog] ( UserId, TableName, Type, Data, Created) 
		SELECT @cUserid,'LT_StatusType',2,
		( SELECT * FROM [at].[LT_StatusType] 
			WHERE
			[LanguageID] = @LanguageID AND
			[StatusTypeID] = @StatusTypeID
			 FOR XML AUTO) as data,
			getdate() 
	END 


	DELETE FROM [at].[LT_StatusType]
	WHERE
		[LanguageID] = @LanguageID AND
		[StatusTypeID] = @StatusTypeID

	Set @Err = @@Error

	RETURN @Err
END


GO
