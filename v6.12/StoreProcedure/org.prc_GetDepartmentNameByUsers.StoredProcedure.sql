SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[org].[prc_GetDepartmentNameByUsers]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [org].[prc_GetDepartmentNameByUsers] AS' 
END
GO
ALTER PROCEDURE [org].[prc_GetDepartmentNameByUsers]
(
	@ListUserID varchar(max)=''
)
AS
BEGIN
	SET NOCOUNT ON
	SELECT u.UserID as UserId, d.Name as DepartmentName, d.DepartmentID FROM org.[User] u INNER JOIN  org.Department d ON u.DepartmentID = d.DepartmentID
	WHERE u.UserID IN (SELECT value FROM dbo.funcListToTableInt(@ListUserID,','))
END

GO
