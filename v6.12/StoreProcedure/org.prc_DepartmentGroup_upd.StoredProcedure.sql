SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[org].[prc_DepartmentGroup_upd]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [org].[prc_DepartmentGroup_upd] AS' 
END
GO
/*  
	2017-06-13 Steve: Change the length of column ExtID in all related tables from xAPI format
*/
ALTER PROCEDURE [org].[prc_DepartmentGroup_upd]
(
	@DepartmentGroupID int,
	@OwnerID int,
	@ExtID nvarchar(256),
	@No smallint,
	@cUserid int,
	@Log smallint = 1
)
AS
BEGIN
	SET NOCOUNT ON
	DECLARE @Err Int

	UPDATE [org].[DepartmentGroup]
	SET
		[OwnerID] = @OwnerID,
		[ExtID] = @ExtID,
		[No] = @No
	WHERE
		[DepartmentGroupID] = @DepartmentGroupID

	IF @Log = 1 
	BEGIN 
		INSERT INTO [Log].[AuditLog] ( UserId, TableName, Type, Data, Created) 
		SELECT @cUserid,'DepartmentGroup',1,
		( SELECT * FROM [org].[DepartmentGroup] 
			WHERE
			[DepartmentGroupID] = @DepartmentGroupID			 FOR XML AUTO) as data,
			getdate()
	END

	Set @Err = @@Error

	RETURN @Err
END


GO
