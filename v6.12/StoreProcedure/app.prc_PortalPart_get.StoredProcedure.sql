SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[app].[prc_PortalPart_get]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [app].[prc_PortalPart_get] AS' 
END
GO

ALTER PROCEDURE [app].[prc_PortalPart_get]
(
	@PortalPageID int
)
AS
BEGIN
	SET NOCOUNT ON
	DECLARE @Err Int

	SELECT
	[PortalPartID],
	[PortalPageID],
	[PortalPartTypeID],
	pp.[No],
	ISNULL(pp.[CategoryID], 0) AS 'CategoryID',
	ISNULL(REPLACE(CAST([Settings] AS NVARCHAR(MAX)), '&amp;', '&'), '') AS 'Settings',
	ISNULL(pp.[BubbleID], 0) AS 'BubbleID',
	ISNULL([ReportPartID], 0) AS 'ReportPartID',
	ISNULL([ReportID], 0) AS 'ReportID',
	ISNULL(pp.[CssClass],'') as 'CssClass',
	ISNULL(pp.[InlineStyle],'') as 'InlineStyle',
	COALESCE(pp.[ActivityID],c.[ActivityID],b.ActivityID, 0) AS 'ActivityID',
	ISNULL(pp.[SurveyID], 0) AS 'SurveyID'
	FROM [app].[PortalPart] pp
	LEFT OUTER JOIN [at].[Category] c on pp.CategoryID=c.CategoryID
	LEFT OUTER JOIN [rep].[Bubble] b on pp.Bubbleid = b.BubbleID
	WHERE
	[PortalPageID] = @PortalPageID
	ORDER BY [No]

	Set @Err = @@Error

	RETURN @Err
END


GO
