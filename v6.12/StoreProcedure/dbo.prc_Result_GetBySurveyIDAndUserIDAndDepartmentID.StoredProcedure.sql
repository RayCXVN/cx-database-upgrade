SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[prc_Result_GetBySurveyIDAndUserIDAndDepartmentID]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [dbo].[prc_Result_GetBySurveyIDAndUserIDAndDepartmentID] AS' 
END
GO
/*
    Sarah - Oct 10, 2016 - Remove column Status on User, Department and Result tables
	Steve - Dec 8, 2016 - Job Fair project: merge Q and R DB, then change how to make a dynamic query
    Ray - 2017-02-09 - Remove cross database reference for Azure compatible
*/
ALTER PROCEDURE [dbo].[prc_Result_GetBySurveyIDAndUserIDAndDepartmentID]
	@ListSurveyID       NVARCHAR(255),
    @UserID         INT, 
    @DepartmentID   INT  
AS
BEGIN
	SET NOCOUNT ON;

    DECLARE @SqlCommand NVARCHAR(MAX), @LinkedDB NVARCHAR(MAX) = ' '
    DECLARE @Survey INT, @ReportServer VARCHAR(255), @ReportDB VARCHAR(255)
    DECLARE @ResultTable TABLE
    ([ResultID] [bigint] ,[StartDate] [datetime] ,[EndDate] [datetime] ,[RoleID] [int] ,[UserID] [int] ,[UserGroupID] [int] ,[SurveyID] [int] ,[BatchID] [int] ,
	[LanguageID] [int] ,[PageNo] [smallint] ,[DepartmentID] [int] ,	[Anonymous] [smallint],	[ShowBack] [bit] ,	[ResultKey] [nvarchar](128),	[Email] [nvarchar](256),
	[chk] [VARCHAR](255),	[Created] [datetime] ,	[LastUpdated] [datetime] ,	[LastUpdatedBy] [int] ,	[StatusTypeID] [int] ,	[ValidFrom] [datetime2](7),
	[ValidTo] [datetime2](7),[DueDate] [datetime2](7),	[ParentResultID] [bigint] ,	[ParentResultSurveyID] [int] ,	[CustomerID] [int], [EntityStatusID] [INT], [EntityStatusReasonID] [INT],
    [Deleted] [datetime2](7)) 
    
    -- Split @SurveyID string to table
    CREATE TABLE #ListOfSurvey ([SurveyID] int, [ReportServer] VARCHAR(255), [ReportDB] VARCHAR(255))
    INSERT INTO #ListOfSurvey ([SurveyID]) SELECT Value AS SurveyID FROM [dbo].[funcListToTableInt](@ListSurveyID,',')        
    
    --SELECT S.SurveyID, S.ReportServer, S.ReportDB INTO #SurveyRDB
    UPDATE #ListOfSurvey
    SET ReportServer = S.ReportServer, ReportDB = S.ReportDB
    FROM at.Survey S INNER JOIN #ListOfSurvey LOS ON LOS.SurveyID = S.SurveyID

    DECLARE c_Survey CURSOR LOCAL FAST_FORWARD READ_ONLY FOR
    SELECT DISTINCT ReportServer, ReportDB FROM #ListOfSurvey
  
    OPEN c_Survey
    FETCH NEXT FROM c_Survey INTO @ReportServer, @ReportDB
    WHILE @@FETCH_STATUS = 0
    BEGIN      
        IF (@ReportServer = @@servername AND @ReportDB = DB_NAME())
			SET @LinkedDB = ' '
		ELSE 
			SET @LinkedDB = '[' + @ReportServer + '].[' + @ReportDB + '].' 

        SET @SqlCommand = 'SELECT R.[ResultID], R.[StartDate], R.[EndDate], R.[RoleID], R.[UserID], R.[UserGroupID], R.[SurveyID], R.[BatchID], R.[LanguageID], R.[PageNo],
	    R.[DepartmentID], R.[Anonymous], R.[ShowBack], R.[ResultKey], R.[Email], R.[chk], R.[Created], R.[LastUpdated], R.[LastUpdatedBy], R.[StatusTypeID],
	    R.[ValidFrom], R.[ValidTo], R.[DueDate], R.[ParentResultID], R.[ParentResultSurveyID], R.[EntityStatusID], R.[EntityStatusReasonID], R.[Deleted]
        FROM ' + @LinkedDB + 'dbo.[Result] R 
        INNER JOIN #ListOfSurvey SRDB ON SRDB.SurveyID = R.SurveyID 
            AND SRDB.ReportServer  = '''+ CONVERT(VARCHAR(255), @ReportServer) +'''
            AND SRDB.ReportDB = '''+ CONVERT(VARCHAR(255), @ReportDB)+ '''
            AND R.UserID = '+ CONVERT(VARCHAR(10), @UserID) + '
            AND R.DepartmentID = '+ CONVERT(VARCHAR(10), @DepartmentID)
       
        INSERT INTO @ResultTable ([ResultID],[StartDate],[EndDate],[RoleID],[UserID],[UserGroupID],[SurveyID],[BatchID],[LanguageID],[PageNo] ,
	    [DepartmentID],	[Anonymous] ,[ShowBack] ,[ResultKey],[Email], [chk] ,[Created] ,[LastUpdated] ,[LastUpdatedBy],[StatusTypeID],
	    [ValidFrom] ,[ValidTo] ,[DueDate] ,[ParentResultID] ,[ParentResultSurveyID], [EntityStatusID], [EntityStatusReasonID], [Deleted]) 
        EXEC(@SqlCommand)

        FETCH NEXT FROM c_Survey INTO @ReportServer, @ReportDB
    END
    CLOSE c_Survey
    DEALLOCATE c_Survey 

    SELECT * FROM @ResultTable
    DROP TABLE #ListOfSurvey
END

GO
