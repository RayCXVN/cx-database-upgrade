SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[org].[prc_UserCountByDepartmentAndUserType]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [org].[prc_UserCountByDepartmentAndUserType] AS' 
END
GO
ALTER PROCEDURE [org].[prc_UserCountByDepartmentAndUserType]
(
  @HDID			int,
  @DepartmentTypeID	int,
  @UserTypeIDList	varchar(max),
  @IncludeTop		smallint = 1,
  @GroupByParentHD	int = 1
)
AS
BEGIN
    SET NOCOUNT ON
    DECLARE @localHDID int, @localName nvarchar(16)
    DECLARE @ActiveEntityStatusID INT = (SELECT EntityStatusID FROM EntityStatus WHERE CodeName='Active')
    
    IF @IncludeTop = 1
    BEGIN
	   SET @localHDID = @HDID
	   SELECT @localName = d.Name FROM org.H_D hd JOIN org.Department d ON hd.DepartmentID = d.DepartmentID AND hd.HDID = @HDID
    END
    
    SELECT ISNULL(parent_hd.HDID, ISNULL(@localHDID,hd.HDID)) HDID, ISNULL(parent_hd.Name, ISNULL(@localName,d.Name)) DepartmentName, count(u.userid) as 'UserCount'
    FROM org.H_D hd 
    JOIN org.department d ON hd.PATH LIKE '%\' + cast(@HDID as nvarchar(16)) + '\%' AND d.DepartmentID = hd.departmentid AND d.EntityStatusID = @ActiveEntityStatusID AND d.Deleted IS NULL
	    AND EXISTS (SELECT 1 FROM org.DT_D dtd WHERE dtd.DepartmentID = d.DepartmentID AND dtd.DepartmentTypeID = @DepartmentTypeID)
    JOIN org.[USER] u ON u.departmentid = d.departmentid AND u.EntityStatusID = @ActiveEntityStatusID AND u.Deleted IS NULL
    JOIN org.[UT_U] utu on utu.userid = u.userid and (ISNULL(@UserTypeIDList,'')='' OR (utu.UserTypeID IN (SELECT Value FROM dbo.funcListToTableInt(@UserTypeIDList,','))))
    LEFT JOIN (SELECT phd.HDID, phd.[Path], d1.DepartmentID, d1.Name FROM org.H_D phd JOIN org.Department d1 ON d1.DepartmentID = phd.DepartmentID WHERE phd.ParentID=@HDID) AS parent_hd ON hd.[Path] LIKE '%' + [parent_hd].[Path] + '%' AND @GroupByParentHD = 1
    WHERE (@IncludeTop = 1 OR hd.HDID <> @HDID)
    GROUP BY ISNULL(parent_hd.HDID, ISNULL(@localHDID,hd.HDID)), ISNULL(parent_hd.Name, ISNULL(@localName,d.Name))
    ORDER BY ISNULL(parent_hd.Name, ISNULL(@localName,d.Name))
END

GO
