SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[log].[prc_EventKey_upd]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [log].[prc_EventKey_upd] AS' 
END
GO
ALTER PROCEDURE [log].[prc_EventKey_upd]
(
	@EventKeyID	int,
	@EventTypeID	int,
	@CodeName		varchar(32),
	@Encrypt		bit,
	@cUserid		int,
	@Log			smallint = 1
)
AS
BEGIN
	SET NOCOUNT ON
	DECLARE @Err Int

	UPDATE [log].[EventKey]
	SET
		[EventTypeID] = @EventTypeID,
		[CodeName] = @CodeName,
		[Encrypt] = @Encrypt
	WHERE
		 [EventKeyID] = @EventKeyID

	Set @Err = @@Error
	
	IF @Log = 1 
	BEGIN 
		INSERT INTO [Log].[AuditLog] ( UserId, TableName, Type, Data, Created) 
		SELECT @cUserid,'EventKey',1,
		( SELECT * FROM [log].[EventKey]
			WHERE
			[EventKeyID] = @EventKeyID FOR XML AUTO) as data,
				getdate() 
	END

	RETURN @Err
END

GO
