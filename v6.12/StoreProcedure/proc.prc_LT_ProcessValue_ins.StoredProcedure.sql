SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[proc].[prc_LT_ProcessValue_ins]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [proc].[prc_LT_ProcessValue_ins] AS' 
END
GO
ALTER PROCEDURE [proc].[prc_LT_ProcessValue_ins]
(
	@ProcessValueID int,
	@LanguageID int,
	@Name nvarchar(256),
	@Description nvarchar(max),
	@URLName nvarchar(256),
	@LeadText nvarchar(max), 
	@LeadAnswerText nvarchar(max), 
	@cUserid int,
	@Log smallint = 1
)
AS
BEGIN
	SET NOCOUNT ON
	DECLARE @Err Int

	INSERT INTO [proc].[LT_ProcessValue]
	(
		[ProcessValueID],
		[LanguageID],
		[Name],
		[Description],
		[URLName],
		[LeadText],
		[LeadAnswerText]
	)
	VALUES
	(
		@ProcessValueID,
		@LanguageID,
		@Name,
		@Description,
		@URLName,
		@LeadText,
		@LeadAnswerText  
	)

	Set @Err = @@Error

	IF @Log = 1 
	BEGIN 
		INSERT INTO [Log].[AuditLog] ( UserId, TableName, Type, Data, Created) 
		SELECT @cUserid,'LT_ProcessValue',0,
		( SELECT * FROM [proc].[LT_ProcessValue] 
			WHERE
			[ProcessValueID] = @ProcessValueID AND
			[LanguageID] = @LanguageID				 FOR XML AUTO) as data,
				getdate() 
	 END

	RETURN @Err
END

GO
