SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[rep].[prc_DocumentTemplateDto_get_ext]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [rep].[prc_DocumentTemplateDto_get_ext] AS' 
END
GO
/*  
	Steve - May 19, 2017 - Add Condition SiteID to WHERE clause
*/
ALTER PROCEDURE [rep].[prc_DocumentTemplateDto_get_ext]
(		
	@SiteID	int,
	@DocumentFormatID int,
	@ActivityId int,
	@DepartmentId int,
	@LangID int
)
As
BEGIN
	SET NOCOUNT ON
	DECLARE @Err int
	DECLARE @OwnerID int	
	DECLARE @ResultTemplateID int = null
	DECLARE @CustomerId int = null
	
	SET @OwnerID = (SELECT OwnerID FROM [app].[Site] WHERE SiteID = @SiteID)
	IF (@DepartmentId > 0)
	BEGIN
		SET @CustomerId = (SELECT CustomerID FROM org.[Department] WHERE DepartmentID = @DepartmentId)
	END	
	
	IF (@ActivityId = 0) SET @ActivityId = NULL
		
	-- If as document exist with the correct Activityid and Customerid	
	SET @ResultTemplateID = (SELECT TOP 1 DocumentTemplateID FROM [rep].[DocumentTemplate] WHERE CustomerID = @CustomerId AND ActivityID = @ActivityId AND OwnerID = @OwnerID AND DocumentFormatID = @DocumentFormatID)
	IF (@ResultTemplateID is not NULL)
	BEGIN
		GOTO CommitResult
	END
	
	-- If a document exist with the correct Customerid and the ActivityID is set to null
	SET @ResultTemplateID = (SELECT TOP 1 DocumentTemplateID FROM [rep].[DocumentTemplate] WHERE CustomerID = @CustomerId AND ActivityID is NULL AND OwnerID = @OwnerID AND DocumentFormatID = @DocumentFormatID)
	IF (@ResultTemplateID is not NULL)
	BEGIN		
		GOTO CommitResult
	END
	
	-- If as document exist whit the correct Activityid and the customerid is set to null
	SET @ResultTemplateID = (SELECT TOP 1 DocumentTemplateID FROM [rep].[DocumentTemplate] WHERE CustomerID is NULL AND ActivityID = @ActivityId AND OwnerID = @OwnerID AND DocumentFormatID = @DocumentFormatID)
	IF (@ResultTemplateID is not NULL)
	BEGIN
		GOTO CommitResult
	END
	
	-- If a document exist with  null in both ActivityID and CustomerID, use the global template depend on Document Format ID
	IF @DocumentFormatID > 0
	BEGIN
	
		IF @DocumentFormatID = 1
			SET @ResultTemplateID  = (SELECT TOP 1 [Value] FROM [app].[SiteParameter] WHERE [Key] = 'ExportTemplateWordID' AND SiteID = @SiteID)
		ELSE IF @DocumentFormatID = 2
			SET @ResultTemplateID  = (SELECT TOP 1 [Value] FROM [app].[SiteParameter] WHERE [Key] = 'ExportTemplatePdfID' AND SiteID = @SiteID)
		ELSE IF @DocumentFormatID = 4
			SET @ResultTemplateID  = (SELECT TOP 1 [Value] FROM [app].[SiteParameter] WHERE [Key] = 'ExportTemplatePptID' AND SiteID = @SiteID)	
		IF (@ResultTemplateID is not NULL)
		BEGIN
			GOTO CommitResult
		END
	END	
	
	Set @Err = @@Error
	RETURN @Err
	
	CommitResult:
		SELECT TOP 1 DT.DocumentTemplateID,DT.DocumentFormatID,OwnerID,ActivityID,Template,Created,CustomerID,ltDT.yStartPos,ltDT.FileContent,ltDT.MIMEType,ltDT.Size 
		FROM [rep].[DocumentTemplate] as DT inner JOIN [rep].[LT_DocumentTemplate] as ltDT ON DT.DocumentTemplateID = ltDT.DocumentTemplateID
		WHERE LanguageID = @LangID AND DT.DocumentTemplateID = @ResultTemplateID
End

GO
