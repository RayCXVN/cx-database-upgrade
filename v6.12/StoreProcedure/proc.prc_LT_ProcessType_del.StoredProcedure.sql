SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[proc].[prc_LT_ProcessType_del]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [proc].[prc_LT_ProcessType_del] AS' 
END
GO
ALTER PROCEDURE [proc].[prc_LT_ProcessType_del]
(
	@ProcessTypeID int,
	@LanguageID int,
	@cUserid int,
	@Log smallint = 1
)
AS
BEGIN
	SET NOCOUNT ON
	DECLARE @Err Int

	IF @Log = 1 
	BEGIN 
		INSERT INTO [Log].[AuditLog] ( UserId, TableName, Type, Data, Created) 
		SELECT @cUserid,'LT_ProcessType',2,
		( SELECT * FROM [proc].[LT_ProcessType] 
			WHERE
			[ProcessTypeID] = @ProcessTypeID AND
			[LanguageID] = @LanguageID
			 FOR XML AUTO) as data,
			getdate() 
	END 


	DELETE FROM [proc].[LT_ProcessType]
	WHERE
		[ProcessTypeID] = @ProcessTypeID AND
		[LanguageID] = @LanguageID

	Set @Err = @@Error

	RETURN @Err
END


GO
