SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[log].[prc_EventCountForSubDepartment]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [log].[prc_EventCountForSubDepartment] AS' 
END
GO
ALTER PROCEDURE [log].[prc_EventCountForSubDepartment]
(   @HDID               int,
    @EventTypeID        int,
    @DepartmentTypeID   int = 0,
    @UserTypeID         int = 0,
    @GetCurrentHDSum    bit = 0
) AS
BEGIN
    SET NOCOUNT ON

    DECLARE @SubHD TABLE (HDID int, DepartmentID int, [HasGivenDT] int)
    DECLARE @EntityStatusID INT = 0 
	SELECT @EntityStatusID=EntityStatusID FROM EntityStatus WHERE CodeName='Active'
    IF @GetCurrentHDSum = 1
    BEGIN
        INSERT INTO @SubHD ([HDID], [DepartmentID], [HasGivenDT])
        SELECT hd.[HDID], hd.[DepartmentID], 0 FROM [org].[H_D] hd WHERE hd.[HDID] = @HDID AND hd.[Deleted] = 0
    END
    ELSE
    BEGIN
        INSERT INTO @SubHD ([HDID], [DepartmentID], [HasGivenDT])
        SELECT hd.[HDID], hd.[DepartmentID], 0 FROM [org].[H_D] hd WHERE hd.[ParentID] = @HDID AND hd.[Deleted] = 0
    END

    UPDATE shd SET [HasGivenDT] = 1 FROM @SubHD shd 
    WHERE EXISTS (SELECT 1 FROM [org].[H_D] hd JOIN [org].[DT_D] dtd ON hd.[DepartmentID] = dtd.[DepartmentID] AND dtd.[DepartmentTypeID] = @DepartmentTypeID AND hd.[ParentID] = shd.[HDID] AND hd.[Deleted] = 0)

    ;WITH ev AS
    (   SELECT DISTINCT e.[UserID], hd.[Path]
        FROM [log].[Event] e JOIN [org].[Department] d ON e.[DepartmentID] = d.[DepartmentID] AND e.[EventTypeID] = @EventTypeID AND d.[EntityStatusID] = @EntityStatusID AND d.Deleted IS NULL
        JOIN [org].[H_D] hd ON e.[DepartmentID] = hd.[DepartmentID] AND hd.[Deleted] = 0
        JOIN [org].[User] u ON e.[UserID] = u.[UserID] AND u.[EntityStatusID] = @EntityStatusID AND u.Deleted IS NULL
                            AND EXISTS (SELECT 1 FROM [org].[UT_U] utu WHERE u.[UserID] = utu.[UserID] AND (utu.[UserTypeID] = @UserTypeID OR @UserTypeID = 0))
    ),
    us AS
    (   SELECT phd.[HDID], COUNT(u.[UserID]) [UserCount]
        FROM [org].[User] u JOIN [org].[Department] d ON u.[DepartmentID] = d.[DepartmentID] AND u.[EntityStatusID] = @EntityStatusID AND d.[EntityStatusID] = @EntityStatusID AND u.Deleted IS NULL AND d.Deleted IS NULL
        JOIN [org].[H_D] hd ON d.[DepartmentID] = hd.[DepartmentID] AND hd.[Deleted] = 0 
        JOIN @SubHD phd ON hd.[Path] LIKE '%\' + CONVERT(nvarchar(16), phd.[HDID]) + '\%'
        JOIN [org].[UT_U] utu ON u.[UserID] = utu.[UserID] AND utu.[UserTypeID] = @UserTypeID
        GROUP BY phd.[HDID]
    )
    SELECT v.[HDID], v.[DepartmentID], v.[Name], v.[HasGivenDT], v.[EventCount], ISNULL(v.[UserCount],0) [UserCount], ISNULL(v.[EventCount] / v.[UserCount],0) * 100 [EventPercent]
    FROM
    (   SELECT phd.[HDID], pd.[DepartmentID], pd.[Name], phd.[HasGivenDT], convert(float,COUNT(ev.[UserID])) [EventCount], convert(float,us.[UserCount]) [UserCount]
        FROM @SubHD phd JOIN [org].[Department] pd ON phd.[DepartmentID] = pd.[DepartmentID] AND pd.[EntityStatusID] = @EntityStatusID AND pd.Deleted IS NULL
        LEFT JOIN us ON us.[HDID] = phd.[HDID]
        LEFT JOIN ev ON ev.[Path] LIKE '%\' + CONVERT(nvarchar(16), phd.[HDID]) + '\%'
        GROUP BY phd.[HDID], pd.[DepartmentID], pd.[Name], phd.[HasGivenDT], convert(float,us.[UserCount])
    ) v
END

GO
