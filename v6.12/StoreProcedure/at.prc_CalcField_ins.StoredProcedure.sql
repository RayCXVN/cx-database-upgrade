SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[at].[prc_CalcField_ins]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [at].[prc_CalcField_ins] AS' 
END
GO
ALTER PROCEDURE  [at].[prc_CalcField_ins]
(
	@CFID int = null output,
	@ActivityID int,
	@Name nvarchar(256),
	@Formula nvarchar(512),
	@Format nvarchar(32),
	@AllParametersMustExist bit,
	@Type int = 1,
	@cUserid int,
	@Log smallint = 1
)
AS
BEGIN
	SET NOCOUNT ON
	DECLARE @Err Int

	INSERT INTO [at].[CalcField]
	(
		[ActivityID],
		[Name],
		[Formula],
		[Format],
		[AllParametersMustExist],
		[Type]
	)
	VALUES
	(
		@ActivityID,
		@Name,
		@Formula,
		@Format,
		@AllParametersMustExist,
		@Type
	)

	Set @Err = @@Error
	Set @CFID = scope_identity()

	IF @Log = 1 
	BEGIN 
		INSERT INTO [Log].[AuditLog] ( UserId, TableName, Type, Data, Created) 
		SELECT @cUserid,'CalcField',0,
		( SELECT * FROM [at].[CalcField] 
			WHERE
			[CFID] = @CFID				 FOR XML AUTO) as data,
				getdate() 
	 END

	RETURN @Err
END

GO
