SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[org].[prc_User_search_by_HDID]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [org].[prc_User_search_by_HDID] AS' 
END
GO
/*  2016-10-10 Ray: Remove column Status from org.User, org.Department, RDB.dbo.Result
	2017-04-17 Steve: Add search condition by SSN
*/
ALTER PROCEDURE [org].[prc_User_search_by_HDID]
(
        @HDID int,
        @SearchString        nvarchar(32),
        @SearchUsername int=0,
        @SearchExtID        int=0,
        @SearchUserId        int=0,
        @SearchEmail        int=0,
        @SearchMobile        int=0,
        @AllowGetDelete		 int =0
)
As
BEGIN
        SET NOCOUNT ON
        DECLARE @Err Int
        DECLARE @ActiveEntityStatusID INT = (SELECT EntityStatusID FROM EntityStatus WHERE CodeName='Active')

        Select
        u.[UserID],
        u.[DepartmentID],
        u.[LanguageID],
        u.[UserName],
        u.[Password],
        u.[LastName],
        u.[FirstName],
        u.[Email],
        u.[ExtID],
        u.[SSN],
        u.[Created],
        u.[Mobile],
        u.[Tag],
        u.[Locked],
        u.[Ownerid],
        u.ChangePassword,
        u.[HashPassword],
        u.[SaltPassword],
        u.[OneTimePassword],
        u.[OTPExpireTime],
		u.[DateOfBirth],
        d.Name as DepartmentName,
		u.[Gender],
        u.EntityStatusID,
        u.Deleted,
        u.EntityStatusReasonID
        FROM org.[User] u
        inner join org.[H_D] hd on hd.DepartmentID = u.DepartmentID and hd.path like  '%\' + cast(@HDID as varchar(16)) + '\%' and hd.deleted = 0
        inner join org.[Department] d on d.DepartmentID = hd.DepartmentID and d.EntityStatusID = @ActiveEntityStatusID AND d.Deleted IS NULL
        WHERE ((u.[UserName] like N'%'+ @SearchString + '%' and @SearchUsername = 1)
        OR (u.[FirstName] like N'%'+ @SearchString + '%')
        OR (u.[FirstName] + ' ' + u.[LastName]  like N'%'+ @SearchString + '%')
        OR (u.[LastName] like N'%'+ @SearchString + '%')
        OR (u.[Email] like N'%'+ @SearchString + '%' and @SearchEmail = 1)
        OR (u.[UserID] like N'%'+ @SearchString + '%' and @SearchUserId = 1)
        OR (u.[ExtID] like N'%'+ @SearchString + '%' and @SearchExtID = 1)
        OR (u.[Mobile] like N'%'+ @SearchString + '%' and @SearchMobile = 1)
		OR (u.[SSN] like N'%'+ @SearchString + '%'))
        AND(@AllowGetDelete =1 OR (u.EntityStatusID = @ActiveEntityStatusID AND u.Deleted IS NULL))

        Set @Err = @@Error

        RETURN @Err
End


GO
