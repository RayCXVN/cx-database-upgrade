SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[form].[prc_FormField_del]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [form].[prc_FormField_del] AS' 
END
GO


ALTER PROCEDURE [form].[prc_FormField_del]
(
	@FormFieldID int,
	@cUserid int,
	@Log smallint = 1
)
AS
BEGIN
	SET NOCOUNT ON
	DECLARE @Err Int

	IF @Log = 1 
	BEGIN 
		INSERT INTO [Log].[AuditLog] ( UserId, TableName, Type, Data, Created) 
		SELECT @cUserid,'FormField',2,
		( SELECT * FROM [form].[FormField] 
			WHERE
			[FormFieldID] = @FormFieldID
			 FOR XML AUTO) as data,
			getdate() 
	END 


	DELETE FROM [form].[FormField]
	WHERE
		[FormFieldID] = @FormFieldID

	Set @Err = @@Error

	RETURN @Err
END


GO
