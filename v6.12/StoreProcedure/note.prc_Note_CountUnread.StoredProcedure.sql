SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[note].[prc_Note_CountUnread]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [note].[prc_Note_CountUnread] AS' 
END
GO
/*
2017-06-14 Ray:     Improve performance
2017-09-19 Sarah:   prc_Note_getByAccess get one more column StartDate 
*/
ALTER PROCEDURE [note].[prc_Note_CountUnread]
(
    @UserID            int,
    @ListNoteTypeID    nvarchar(512)='',
    @HDID              int = 0,
    @NoteCount         int = 0 OUTPUT,
    @NotifyAtNextLogin bit = 0
)
AS
BEGIN
    SET @NoteCount = 0
    SET NOCOUNT ON
    DECLARE @NoteTypeID int, @CheckValidInDateRange bit = 1
    DECLARE @Note TABLE (NoteID int, CreatedBy int, Created datetime, AccessType int, NotifyAtNextLogin bit, Active bit, StartDate datetime) 
    
    DECLARE cur CURSOR READ_ONLY FOR
    SELECT * FROM dbo.funcListToTableInt(@ListNoteTypeID,',')
    OPEN cur
    FETCH NEXT FROM cur INTO @NoteTypeID
        WHILE @@FETCH_STATUS = 0
        BEGIN
            INSERT INTO @Note EXEC [note].[prc_Note_getByAccess] @UserID, @NoteTypeID, @HDID, @NotifyAtNextLogin, '', @CheckValidInDateRange
            FETCH NEXT FROM cur INTO @NoteTypeID
        END
    CLOSE cur
    DEALLOCATE cur
    
    SELECT @NoteCount = COUNT(n.NoteID) FROM @Note n
    WHERE NOT EXISTS (SELECT 1 FROM note.NoteReadLog rl WITH (NOLOCK) WHERE rl.NoteID = n.NoteID AND rl.UserID = @UserID) AND n.Active = 1
END
GO
