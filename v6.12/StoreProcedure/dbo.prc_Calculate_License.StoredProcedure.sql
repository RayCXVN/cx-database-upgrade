SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[prc_Calculate_License]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [dbo].[prc_Calculate_License] AS' 
END
GO
-- exec [dbo].[prc_Calculate_License] 15, 3, 2, 1, 1, 1, 0
ALTER PROCEDURE [dbo].[prc_Calculate_License]
(
	@OwnerID		    int,
	@CustomerID	    int,
	@AvailLicPropID    int,
	@LicUsedPropID	    int,
	@SystemUserID	    int,
	@UpdateLicenseUsed bit = 0,
	@UserID		    int = NULL
)
AS
BEGIN
	SET NOCOUNT ON
	DECLARE @Err Int, @License_Bought int, @License_Used int, @Released_License int, @Available_License int, @count int,
		   @LicenseBoughtEType int, @LicenseBoughtEKey int, @LicenseUsedEType int, @LicenseReleaseEType int,
		   @LB_CustomerEKey int, @LU_CustomerEKey int, @LR_CustomerEKey int,
		   @LU_UserEKey int, @LR_UserEKey int
	
	SELECT @LicenseBoughtEType = EventTypeID   FROM [log].EventType WHERE CodeName = 'LICENSE_BOUGHT' AND OwnerID = @OwnerID
	SELECT @LicenseUsedEType = EventTypeID	   FROM [log].EventType WHERE CodeName = 'LICENSE_USED' AND OwnerID = @OwnerID
	SELECT @LicenseReleaseEType = EventTypeID  FROM [log].EventType WHERE CodeName = 'LICENSE_RELEASED' AND OwnerID = @OwnerID
	
	SELECT @LicenseBoughtEKey = EventKeyID	   FROM [log].EventKey  WHERE CodeName = 'LicensePackage' AND EventTypeID = @LicenseBoughtEType
	SELECT @LB_CustomerEKey = EventKeyID	   FROM [log].EventKey  WHERE CodeName = 'CustomerID' AND EventTypeID = @LicenseBoughtEType
	SELECT @LU_CustomerEKey = EventKeyID	   FROM [log].EventKey  WHERE CodeName = 'CustomerID' AND EventTypeID = @LicenseUsedEType
	SELECT @LR_CustomerEKey = EventKeyID	   FROM [log].EventKey  WHERE CodeName = 'CustomerID' AND EventTypeID = @LicenseReleaseEType
	SELECT @LU_UserEKey = EventKeyID		   FROM [log].EventKey  WHERE CodeName = 'CandidateID' AND EventTypeID = @LicenseUsedEType
	SELECT @LR_UserEKey = EventKeyID		   FROM [log].EventKey  WHERE CodeName = 'CandidateID' AND EventTypeID = @LicenseReleaseEType
	
	-- count license bought
	SELECT @License_Bought = ISNULL(SUM(convert(int,ei.Value)),0)
	FROM [log].[EventInfo] ei JOIN [log].[Event] e ON ei.EventID = e.EventID AND e.EventTypeID = @LicenseBoughtEType
	JOIN org.[User] u ON e.UserID = u.UserID 
	JOIN [log].EventInfo ei2 ON ei2.EventID = e.EventID AND ei2.EventKeyID = @LB_CustomerEKey AND ei2.Value = @CustomerID
	WHERE ei.EventKeyID = @LicenseBoughtEKey 
	
	-- Count license used
	SELECT @License_Used = ISNULL(COUNT(DISTINCT e.EventID),0)
	FROM [log].[Event] e JOIN org.[User] u ON e.UserID = u.UserID AND e.EventTypeID = @LicenseUsedEType
	JOIN [log].EventInfo ei ON ei.EventID = e.EventID AND ei.EventKeyID = @LU_CustomerEKey AND ei.Value = @CustomerID
	JOIN [log].EventInfo ei2 ON ei2.EventID = e.EventID AND ei2.EventKeyID = @LU_UserEKey
     
     -- Count license released
	SELECT @Released_License = ISNULL(COUNT(DISTINCT e.EventID),0)
	FROM [log].[Event] e JOIN org.[User] u ON e.UserID = u.UserID AND e.EventTypeID = @LicenseReleaseEType
	JOIN [log].EventInfo ei ON ei.EventID = e.EventID AND ei.EventKeyID = @LR_CustomerEKey AND ei.Value = @CustomerID
	JOIN [log].EventInfo ei2 ON ei2.EventID = e.EventID AND ei2.EventKeyID = @LR_UserEKey
	
	SET @Available_License = @License_Bought - @License_Used + @Released_License
	
	SELECT @License_Bought as LicenseBought, (@License_Used - @Released_License) as LicenseUsed, @Available_License as AvailableLicense
	
	IF @Available_License < 0 SET @Available_License = 0
	
	-- Update Available License
	SELECT @count = COUNT(PropValueID) FROM prop.PropValue WHERE PropertyID = @AvailLicPropID AND ItemID = @CustomerID
	
	IF @count > 0
	    BEGIN
		  UPDATE prop.PropValue SET Value = @Available_License, Updated = GETDATE(), UpdatedBy = @SystemUserID
		  WHERE PropertyID = @AvailLicPropID AND ItemID = @CustomerID
	    END
	ELSE
	    BEGIN
		  INSERT INTO prop.PropValue (PropertyID, ItemID, Value, [No], Created, CreatedBy, Updated, UpdatedBy)
		  VALUES (@AvailLicPropID, @CustomerID, @Available_License, 0, GETDATE(), @SystemUserID, GETDATE(), @SystemUserID)
	    END
	    
	-- Update Used License
	SELECT @count = COUNT(PropValueID) FROM prop.PropValue WHERE PropertyID = @LicUsedPropID AND ItemID = @CustomerID
	
	IF @count > 0
	    BEGIN
		  UPDATE prop.PropValue SET Value = @License_Used - @Released_License, Updated = GETDATE(), UpdatedBy = @SystemUserID
		  WHERE PropertyID = @LicUsedPropID AND ItemID = @CustomerID
	    END
	ELSE
	    BEGIN
		  INSERT INTO prop.PropValue (PropertyID, ItemID, Value, [No], Created, CreatedBy, Updated, UpdatedBy)
		  VALUES (@LicUsedPropID, @CustomerID, @License_Used - @Released_License, 0, GETDATE(), @SystemUserID, GETDATE(), @SystemUserID)
	    END
	
	IF @UpdateLicenseUsed = 1
	BEGIN
	   IF ISNULL(@UserID,0) > 0
	   BEGIN
		   SELECT @License_Used = ISNULL(COUNT(DISTINCT e.EventID),0)
		   FROM [log].[Event] e JOIN org.[User] u ON e.UserID = u.UserID AND e.UserID = @UserID AND e.EventTypeID = @LicenseUsedEType
		   JOIN [log].EventInfo ei ON ei.EventID = e.EventID AND ei.EventKeyID = @LU_CustomerEKey AND ei.Value = @CustomerID
		   JOIN [log].EventInfo ei2 ON ei2.EventID = e.EventID AND ei2.EventKeyID = @LU_UserEKey
		  
		   SELECT @Released_License = ISNULL(COUNT(DISTINCT e.EventID),0)
		   FROM [log].[Event] e JOIN org.[User] u ON e.UserID = u.UserID AND e.UserID = @UserID AND e.EventTypeID = @LicenseReleaseEType
		   JOIN [log].EventInfo ei ON ei.EventID = e.EventID AND ei.EventKeyID = @LR_CustomerEKey AND ei.Value = @CustomerID
		   JOIN [log].EventInfo ei2 ON ei2.EventID = e.EventID AND ei2.EventKeyID = @LR_UserEKey
			  
		   SELECT @count = COUNT(PropValueID) FROM prop.PropValue WHERE PropertyID = @LicUsedPropID AND ItemID = @UserID
        	
		   IF @count > 0
			  BEGIN
				UPDATE prop.PropValue SET Value = @License_Used - @Released_License, Updated = GETDATE(), UpdatedBy = @SystemUserID
				WHERE PropertyID = @LicUsedPropID AND ItemID = @UserID
			  END
		   ELSE
			  BEGIN
				INSERT INTO prop.PropValue (PropertyID, ItemID, Value, [No], Created, CreatedBy, Updated, UpdatedBy)
				VALUES (@LicUsedPropID, @UserID, @License_Used - @Released_License, 0, GETDATE(), @SystemUserID, GETDATE(), @SystemUserID)
			  END
	   END
	   ELSE
	   BEGIN
		  CREATE TABLE #CounselorList (UserID int)
		  
		  INSERT INTO #CounselorList (UserID)
		  SELECT DISTINCT e.UserID
		  FROM [log].[Event] e JOIN org.[User] u ON e.UserID = u.UserID AND e.EventTypeID = @LicenseUsedEType
		  JOIN org.Department d ON d.DepartmentID = u.DepartmentID AND d.CustomerID = @CustomerID
	   
		  DECLARE c_Counselor CURSOR READ_ONLY FOR SELECT UserID FROM #CounselorList
		  DECLARE @CounselorID int
    	   
		  OPEN c_Counselor
		  FETCH NEXT FROM c_Counselor INTO @CounselorID
		  WHILE @@FETCH_STATUS=0
		  BEGIN
			  SELECT @License_Used = ISNULL(COUNT(DISTINCT e.EventID),0)
			  FROM [log].[Event] e JOIN org.[User] u ON e.UserID = u.UserID AND e.UserID = @CounselorID AND e.EventTypeID = @LicenseUsedEType
			  JOIN [log].EventInfo ei ON ei.EventID = e.EventID AND ei.EventKeyID = @LU_CustomerEKey AND ei.Value = @CustomerID
			  JOIN [log].EventInfo ei2 ON ei2.EventID = e.EventID AND ei2.EventKeyID = @LU_UserEKey
    			  
			  SELECT @Released_License = ISNULL(COUNT(DISTINCT e.EventID),0)
			  FROM [log].[Event] e JOIN org.[User] u ON e.UserID = u.UserID AND e.UserID = @CounselorID AND e.EventTypeID = @LicenseReleaseEType
			  JOIN [log].EventInfo ei ON ei.EventID = e.EventID AND ei.EventKeyID = @LR_CustomerEKey AND ei.Value = @CustomerID
			  JOIN [log].EventInfo ei2 ON ei2.EventID = e.EventID AND ei2.EventKeyID = @LR_UserEKey
			  
			  SELECT @count = COUNT(PropValueID) FROM prop.PropValue WHERE PropertyID = @LicUsedPropID AND ItemID = @CounselorID
            	
			  IF @count > 0
				 BEGIN
				    UPDATE prop.PropValue SET Value = @License_Used - @Released_License, Updated = GETDATE(), UpdatedBy = @SystemUserID
				    WHERE PropertyID = @LicUsedPropID AND ItemID = @CounselorID
				 END
			  ELSE
				 BEGIN
				    INSERT INTO prop.PropValue (PropertyID, ItemID, Value, [No], Created, CreatedBy, Updated, UpdatedBy)
				    VALUES (@LicUsedPropID, @CounselorID, @License_Used - @Released_License, 0, GETDATE(), @SystemUserID, GETDATE(), @SystemUserID)
				 END
			 FETCH NEXT FROM c_Counselor INTO @CounselorID
		  END
		  CLOSE c_Counselor
		  DEALLOCATE c_Counselor
	   END
	END
	
	Set @Err = @@Error

	RETURN @Err
END

GO
