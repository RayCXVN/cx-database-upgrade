SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[job].[CreateAutoCalcJob]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [job].[CreateAutoCalcJob] AS' 
END
GO
/*  Target: create a scheduled job to automatically calculate answers

    2017-08-10 Ray:     Add new parameter to handle calculation of selected results only
*/
ALTER PROCEDURE [job].[CreateAutoCalcJob]
(
    @OwnerID        integer,
    @Userid         integer,
    @Servername     nvarchar(64),
    @QDB            nvarchar(64),
    @ActivityID     integer,
    @SurveyID       integer,
    @Username       nvarchar(64),
    @Password       nvarchar(64),
    @Description    nvarchar(64) = 'AutoCalc',
    @BatchID        integer = -1,
    @ResultIDs      nvarchar(max) = ''
)
AS
BEGIN
    DECLARE @JobID int

    INSERT INTO job.job (JobTypeID, JobStatusID, OwnerID, UserID, Name, Priority, [Option], Created, StartDate, EndDate, Description)
    VALUES (26,0,@OwnerID,@Userid,'Calculate',0,0,GETDATE(),getdate(),null,@Description )

    SELECT @JobID = scope_identity()

    insert into  job.JobParameter (JobID, No, Name, Value, Type)
    values (@JobID,1,'Server',@ServerName,0)

    insert into  job.JobParameter (JobID, No, Name, Value, Type)
    values (@JobID,2,'Database',@QDB,0)

    insert into  job.JobParameter (JobID, No, Name, Value, Type)
    values (@JobID,3,'Username',@Username,0)

    insert into  job.JobParameter (JobID, No, Name, Value, Type)
    values (@JobID,4,'Password',@Password,0)

    insert into  job.JobParameter (JobID, No, Name, Value, Type)
    values (@JobID,5,'Ownerid',@OwnerID,0)

    insert into  job.JobParameter (JobID, No, Name, Value, Type)
    values (@JobID,6,'ActivityId',cast(@ActivityID as nvarchar(64)),0)

    insert into  job.JobParameter (JobID, No, Name, Value, Type)
    values (@JobID,7,'SurveyId',cast(@SurveyID as nvarchar(64)),0)

    insert into  job.JobParameter (JobID, No, Name, Value, Type)
    values (@JobID,8,'BatchId',@BatchID,0) 

    insert into  job.JobParameter (JobID, No, Name, Value, Type)
    values (@JobID,9,'ReprocessOlap','1',0) 

    insert into  job.JobParameter (JobID, No, Name, Value, Type)
    values (@JobID,10,'ResultIDs',@ResultIDs,0) 
END

GO
