SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[prop].[prc_Prop_del]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [prop].[prc_Prop_del] AS' 
END
GO


ALTER PROCEDURE [prop].[prc_Prop_del]
(
	@PropertyID int,
	@cUserid int,
	@Log smallint = 1
)
AS
BEGIN
	SET NOCOUNT ON
	DECLARE @Err Int

	IF @Log = 1 
	BEGIN 
		INSERT INTO [Log].[AuditLog] ( UserId, TableName, Type, Data, Created) 
		SELECT @cUserid,'Prop',2,
		( SELECT * FROM [prop].[Prop] 
			WHERE
			[PropertyID] = @PropertyID
			 FOR XML AUTO) as data,
			getdate() 
	END 


	DELETE FROM [prop].[Prop]
	WHERE
		[PropertyID] = @PropertyID

	Set @Err = @@Error

	RETURN @Err
END


GO
