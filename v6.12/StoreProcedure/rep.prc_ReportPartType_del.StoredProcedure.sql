SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[rep].[prc_ReportPartType_del]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [rep].[prc_ReportPartType_del] AS' 
END
GO


ALTER PROCEDURE [rep].[prc_ReportPartType_del]
(
	@ReportPartTypeID int,
	@cUserid int,
	@Log smallint = 1
)
AS
BEGIN
	SET NOCOUNT ON
	DECLARE @Err Int

	IF @Log = 1 
	BEGIN 
		INSERT INTO [Log].[AuditLog] ( UserId, TableName, Type, Data, Created) 
		SELECT @cUserid,'ReportPartType',2,
		( SELECT * FROM [rep].[ReportPartType] 
			WHERE
			[ReportPartTypeID] = @ReportPartTypeID
			 FOR XML AUTO) as data,
			getdate() 
	END 


	DELETE FROM [rep].[ReportPartType]
	WHERE
		[ReportPartTypeID] = @ReportPartTypeID

	Set @Err = @@Error

	RETURN @Err
END


GO
