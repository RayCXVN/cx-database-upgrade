SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[prc_DeactivateUser]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [dbo].[prc_DeactivateUser] AS' 
END
GO
/* 
	May 22, 2017 - Steve: improving [dbo].[prc_DeactivateUser] with 5 steps:
	--1)        Do not run if more than 25% of the users of the customer will be deactivated? 
	--3)        Not run if not at least one user for the customer has last synchronized date newer than the last «number of days» 
	--2)        Make the number of days (5) a variable 
	--4)        Make the job send an email if 1) or 3) happens (create a new Job (JobType 20), JobParam, Mail and SendMail)
	--5)        Log deactivation of a user as an event

	EXEC [dbo].[prc_DeactivateUser_Test] N'488', 65
*/
ALTER PROCEDURE [dbo].[prc_DeactivateUser]
(
	@CustomerIDs VARCHAR(MAX) = '488,495,496,497,498',
	@NumberOfDays INT         = 5
)
AS
BEGIN

	DECLARE @EntityStatus_Deactive INT, @EntityStatus_Active INT, @EntityStatusReason_Manually INT, @OwnerID INT, @Email NVARCHAR(MAX),
	@Archetype_Employee INT, @Archetype_Learner INT, @Archetype_School INT, @NumberOfDeactivatedLearners INT, @NumberOfDeactivatedEmployees INT,
	@NumberOfDeactivatedUsers INT,@NumberOfUsers INT, @NumberOfCustomers INT = 0, @i INT = 0, @CustomerID INT, @EventTypeID INT, @EventKeyID INT = NULL, @EventID INT,
	@SuperAdminUserID INT, @DepartmentID INT, @UserIDList NVARCHAR(MAX), @ReportDB NVARCHAR(MAX), @ReportServer NVARCHAR(MAX), @JobID INT, @MailID INT, @SendMailID INT, @SurveyID INT

	SELECT @EntityStatus_Deactive = E.EntityStatusID FROM EntityStatus E WHERE E.CodeName = 'Deactive'
	SELECT @EntityStatus_Active = E.EntityStatusID FROM EntityStatus E WHERE E.CodeName = 'Active'
	SELECT @EntityStatusReason_Manually = E.EntityStatusReasonID FROM EntityStatusReason E WHERE E.CodeName = 'Deactive_ManuallySetDeactive'
	SELECT @Archetype_Employee = A.ArchetypeID FROM Archetype A WHERE A.CodeName = 'Employee'
	SELECT @Archetype_Learner = A.ArchetypeID FROM Archetype A WHERE A.CodeName = 'Learner'
	SELECT @Archetype_School = A.ArchetypeID FROM Archetype A WHERE A.CodeName = 'School'
	SELECT TOP 1 @ReportDB = ReportDB, @ReportServer = ReportServer, @SurveyID = SurveyID FROM at.Survey ORDER BY Created DESC
	SELECT @OwnerID = OwnerID FROM org.Owner 

	SELECT @SuperAdminUserID = UserID, @Email = Email FROM org.[User] WHERE UserName = N'vokalint'
	SELECT @EventTypeID = EventTypeID FROM log.EventType WHERE CodeName = N'USER_DELETED'

	DECLARE @Customer TABLE (ID INT IDENTITY(1,1), CustomerID INT)

	DECLARE @User TABLE (UserID INT, DepartmentID INT)

	SET @CustomerID = ISNULL(@CustomerID, '')

	INSERT INTO @Customer(CustomerID) 
	SELECT Value FROM dbo.funcListToTableInt(@CustomerIDs,',')

	SELECT @NumberOfCustomers = COUNT(*) FROM @Customer
	WHILE @i < @NumberOfCustomers
	BEGIN 
		SET @i = @i + 1
		SELECT @CustomerID = CustomerID FROM @Customer WHERE ID = @i

		SELECT @NumberOfDeactivatedLearners = COUNT(*)
		FROM org.[User] u
		WHERE u.EntityStatusID = @EntityStatus_Active AND u.CustomerID = @CustomerID
		AND u.ArchetypeID = @Archetype_Learner AND u.locked = 1 AND u.LastSynchronized < GETDATE() - @NumberOfDays

		SELECT @NumberOfDeactivatedEmployees = COUNT(*) 
		FROM org.[User] u JOIN org.department d ON d.departmentid = u.departmentid AND d.archetypeid = @Archetype_School
		WHERE u.EntityStatusID = @EntityStatus_Active AND u.CustomerID = @CustomerID
		AND u.ArchetypeID = @Archetype_Employee AND u.locked = 1 AND u.LastSynchronized < GETDATE() - @NumberOfDays
	
		SELECT @NumberOfUsers = COUNT(*)
		FROM org.[User] u 
		WHERE u.EntityStatusID = @EntityStatus_Active AND u.CustomerID = @CustomerID

		SET @NumberOfDeactivatedUsers = @NumberOfDeactivatedLearners + @NumberOfDeactivatedEmployees

		IF (@NumberOfDeactivatedUsers = 0 OR (@NumberOfDeactivatedUsers - 0.25 * @NumberOfUsers) > 0)
		BEGIN 
			-- Send mail here: Can not delete the users of this customer
			INSERT INTO at.Mail([SurveyID], [BatchID], [Type], [From], [Bcc], [StatusMail], [Created])
			VALUES (@SurveyID, 0, 0, @Email, @Email, @Email, GETDATE())
			SET @MailID = SCOPE_IDENTITY()

			INSERT INTO at.LT_Mail ([LanguageID], [MailID], [Name], [Subject], [Body])
			VALUES (1, @MailID, @Email, N'Job 20 - Users can not be deactivated', N'<p><strong>Customer ' + CONVERT(NVARCHAR(14),@CustomerID) + ' has ' + CONVERT(NVARCHAR(14),@NumberOfUsers) +' active users with ' + CONVERT(NVARCHAR(14),@NumberOfDeactivatedUsers) + ' users that are about to be deactivated</strong></p>')
		
			INSERT INTO at.SentMail ([MailID], [LanguageID], [Type], [From], [Bcc], [StatusMail], [Status], [Sent], [Created])
			VALUES (@MailID, 1, 0, @Email, @Email, @Email, 0, GETDATE(), GETDATE())
			SET @SendMailID = SCOPE_IDENTITY()

			INSERT INTO at.LT_SentMail ([LanguageID], [SentMailID], [Name], [Subject], [Body])
			VALUES (1, @SendMailID, @Email, N'Job 20 - Users can not be deactivated', N'<p><strong>Customer ' + CONVERT(NVARCHAR(14),@CustomerID) + ' has ' + CONVERT(NVARCHAR(14),@NumberOfUsers) +' active users with ' + CONVERT(NVARCHAR(14),@NumberOfDeactivatedUsers) + ' users that are about to be deactivated</strong></p>')
		
			INSERT INTO job.Job ([JobTypeID], [JobStatusID], [OwnerID], [UserID], [Name], [Priority], [Option], [Created], [StartDate], [EndDate], [Description], [LogLevel], [JobLevel])
			VALUES (20, 0, @OwnerID, @SuperAdminUserID, N'Sending av mail <Job 20>', 0, 0, GETDATE(), GETDATE(), NULL, N'', 0 ,0)
			SET @JobID = SCOPE_IDENTITY()

			INSERT INTO job.JobParameter ([JobID], [No], [Name], [Value], [Type])
			VALUES (@JobID, 1, N'MailID', CONVERT(NVARCHAR(14),@MailID), 0)
			INSERT INTO job.JobParameter ([JobID], [No], [Name], [Value], [Type])
			VALUES (@JobID, 2, N'SentMailID', CONVERT(NVARCHAR(14),@SendMailID), 0)
			INSERT INTO job.JobParameter ([JobID], [No], [Name], [Value], [Type])
			VALUES (@JobID, 3, N'ReportDB', @ReportDB, 0)
			INSERT INTO job.JobParameter ([JobID], [No], [Name], [Value], [Type])
			VALUES (@JobID, 4, N'ReportServer', @ReportServer, 0)
		
		END
		ELSE
		BEGIN
			-- Deactivate Learners
			UPDATE u
			SET u.EntityStatusID = @EntityStatus_Deactive, u.EntityStatusReasonID = @EntityStatusReason_Manually
			OUTPUT INSERTED.UserID, INSERTED.DepartmentID INTO @User(UserID, DepartmentID)
			FROM org.[User] u
			WHERE u.EntityStatusID = @EntityStatus_Active AND u.CustomerID = @CustomerID
			AND u.ArchetypeID = @Archetype_Learner AND u.locked = 1 AND u.LastSynchronized < GETDATE() - @NumberOfDays

			-- Deactivate Employees
			UPDATE u
			SET u.EntityStatusID = @EntityStatus_Deactive, u.EntityStatusReasonID = @EntityStatusReason_Manually
			OUTPUT INSERTED.UserID, INSERTED.DepartmentID INTO @User(UserID, DepartmentID)
			FROM org.[User] u JOIN org.department d ON d.departmentid = u.departmentid AND d.archetypeid = @Archetype_School
			WHERE u.EntityStatusID = @EntityStatus_Active AND u.CustomerID = @CustomerID
			AND u.ArchetypeID = @Archetype_Employee AND u.locked = 1 AND u.LastSynchronized < GETDATE() - @NumberOfDays

			-- Log deactivation of users
			-- for one user in one event
			INSERT INTO log.Event ([EventTypeID], [UserID], [IPNumber], [TableTypeID], [ItemID], [Created], [ApplicationName], [DepartmentID], [CustomerID])
			SELECT @EventTypeID, @SuperAdminUserID, N'::1', 13, u.UserID, GETDATE(), N'Generated by DB script', u.DepartmentID, @CustomerID
			FROM @User u

			DELETE FROM @User
		END
	END
END

GO
