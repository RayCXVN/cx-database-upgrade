SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[rep].[prc_ReportColumnType_ins]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [rep].[prc_ReportColumnType_ins] AS' 
END
GO

ALTER PROCEDURE [rep].[prc_ReportColumnType_ins]
(
	@ReportColumnTypeID int = null output,
	@No smallint,
	@cUserid int,
	@Log smallint = 1
)
AS
BEGIN
	SET NOCOUNT ON
	DECLARE @Err Int

	INSERT INTO [rep].[ReportColumnType]
	(
		[No]
	)
	VALUES
	(
		@No
	)

	Set @Err = @@Error
	Set @ReportColumnTypeID = scope_identity()

	IF @Log = 1 
	BEGIN 
		INSERT INTO [Log].[AuditLog] ( UserId, TableName, Type, Data, Created) 
		SELECT @cUserid,'ReportColumnType',0,
		( SELECT * FROM [rep].[ReportColumnType] 
			WHERE
			[ReportColumnTypeID] = @ReportColumnTypeID				 FOR XML AUTO) as data,
				getdate() 
	 END

	RETURN @Err
END


GO
