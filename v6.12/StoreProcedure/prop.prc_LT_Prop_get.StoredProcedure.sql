SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[prop].[prc_LT_Prop_get]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [prop].[prc_LT_Prop_get] AS' 
END
GO

ALTER PROCEDURE [prop].[prc_LT_Prop_get]
(
	@PropertyID int
)
AS
BEGIN
	SET NOCOUNT ON
	DECLARE @Err Int

	SELECT
	[LanguageID],
	[PropertyID],
	[Name],
	[Description]
	FROM [prop].[LT_Prop]
	WHERE
	[PropertyID] = @PropertyID

	Set @Err = @@Error

	RETURN @Err
END


GO
