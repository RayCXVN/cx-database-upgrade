SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[job].[prc_JobStatusLog_ins]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [job].[prc_JobStatusLog_ins] AS' 
END
GO

ALTER PROCEDURE [job].[prc_JobStatusLog_ins]
(
	@JobStatusLogID int = null output,
	@JobID int,
	@JobStatusID smallint,
	@Description nvarchar(max),
	@cUserid int,
	@Log smallint = 1
)
AS
BEGIN
	SET NOCOUNT ON
	DECLARE @Err Int

	INSERT INTO [job].[JobStatusLog]
	(
		[JobID],
		[JobStatusID],
		[Description]
	)
	VALUES
	(
		@JobID,
		@JobStatusID,
		@Description
	)

	Set @Err = @@Error
	Set @JobStatusLogID = scope_identity()

	IF @Log = 1 
	BEGIN 
		INSERT INTO [Log].[AuditLog] ( UserId, TableName, Type, Data, Created) 
		SELECT @cUserid,'JobStatusLog',0,
		( SELECT * FROM [job].[JobStatusLog] 
			WHERE
			[JobStatusLogID] = @JobStatusLogID				 FOR XML AUTO) as data,
				getdate() 
	 END

	RETURN @Err
END


GO
