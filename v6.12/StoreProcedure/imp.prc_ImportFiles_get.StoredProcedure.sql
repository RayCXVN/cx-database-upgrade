SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[imp].[prc_ImportFiles_get]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [imp].[prc_ImportFiles_get] AS' 
END
GO
ALTER PROCEDURE [imp].[prc_ImportFiles_get]
(
  @Userid INT
)
AS
BEGIN
	SET NOCOUNT ON
	DECLARE @Err Int

	SELECT
	[ImportFileID],
	[UserID],
	[MIMEType],
	[Data],
	[DataError],
	[Created],
	[EmailSendtTo],
	[Description],
	[Size],
	[SizeError],
	[Filename],
	[FilenameError],
	ISNULL([SurveyID], 0) AS 'SurveyID'
	FROM [imp].[ImportFiles]
	WHERE UserId = @Userid

	Set @Err = @@Error

	RETURN @Err
END
GO
