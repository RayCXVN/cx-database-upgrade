SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[at].[prc_DR_Q_get]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [at].[prc_DR_Q_get] AS' 
END
GO

ALTER PROCEDURE [at].[prc_DR_Q_get]
(
	@DottedRuleID int
)
AS
BEGIN
	SET NOCOUNT ON
	DECLARE @Err Int

	SELECT
	[DottedRuleID],
	[QuestionID]
	FROM [at].[DR_Q]
	WHERE
	[DottedRuleID] = @DottedRuleID

	Set @Err = @@Error

	RETURN @Err
END


GO
