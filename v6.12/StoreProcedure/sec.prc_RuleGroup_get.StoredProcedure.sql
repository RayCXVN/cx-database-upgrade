SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[sec].[prc_RuleGroup_get]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [sec].[prc_RuleGroup_get] AS' 
END
GO
ALTER PROCEDURE [sec].[prc_RuleGroup_get]
AS
BEGIN
SET NOCOUNT ON
DECLARE @Err int
SELECT
    [RuleGroupID],
    [GroupAccessRuleID],
    [AccessRuleID]
FROM [sec].[RuleGroup]
SET @Err = @@Error
RETURN @Err
END


GO
