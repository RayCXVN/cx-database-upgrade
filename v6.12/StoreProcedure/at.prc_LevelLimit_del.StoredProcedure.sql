SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[at].[prc_LevelLimit_del]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [at].[prc_LevelLimit_del] AS' 
END
GO


ALTER PROCEDURE [at].[prc_LevelLimit_del]
(
	@LevelLimitID int,
	@cUserid int,
	@Log smallint = 1
)
AS
BEGIN
	SET NOCOUNT ON
	DECLARE @Err Int

	IF @Log = 1 
	BEGIN 
		INSERT INTO [Log].[AuditLog] ( UserId, TableName, Type, Data, Created) 
		SELECT @cUserid,'LevelLimit',2,
		( SELECT * FROM [at].[LevelLimit] 
			WHERE
			[LevelLimitID] = @LevelLimitID
			 FOR XML AUTO) as data,
			getdate() 
	END 


	DELETE FROM [at].[LevelLimit]
	WHERE
		[LevelLimitID] = @LevelLimitID

	Set @Err = @@Error

	RETURN @Err
END


GO
