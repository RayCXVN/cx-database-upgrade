SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[org].[prc_GetUserGroup_ByHDID_UserType]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [org].[prc_GetUserGroup_ByHDID_UserType] AS' 
END
GO
/*
	Steve - Sept 22 2016 - EntityStatus for table UserGroup
*/
ALTER PROCEDURE [org].[prc_GetUserGroup_ByHDID_UserType]
(
	@ListUserType		nvarchar(max),
	@OwnerID			INT,
	@UserGroupTypeID	varchar(max),
	@HDID				INT
)
AS
BEGIN
    DECLARE @ActiveEntityStatusID INT = (SELECT EntityStatusID FROM EntityStatus WHERE CodeName='Active')

	SELECT DISTINCT
		ug.UserGroupID,
		ug.OwnerID,
		ug.DepartmentID,
		ug.SurveyId,
		ug.Name,
		ug.Description,
		ug.Created,
		ug.ExtID,
		ug.PeriodID,
		ug.UserID,
		ug.UserGroupTypeID,
		ug.Tag
	 FROM org.UserGroup ug
		INNER JOIN org.Department d ON d.DepartmentID = ug.DepartmentID
		INNER JOIN org.[H_D] hd on hd.DepartmentID = d.DepartmentID and hd.path like  '%\' + cast(@HDID as varchar(16)) + '\%' and hd.deleted = 0
		INNER JOIN org.UT_U ut ON ug.UserID = ut.UserID AND ut.UserTypeID IN (SELECT value FROM dbo.funcListToTableInt(@ListUserType,','))
	WHERE 
		ug.OwnerID = @OwnerID		AND
		ug.UserGroupTypeID IN (SELECT value FROM dbo.funcListToTableInt(@UserGroupTypeID,','))
		AND ug.EntityStatusID = @ActiveEntityStatusID AND ug.Deleted IS NULL
END

GO
