SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[at].[prc_ReportCalcType_upd]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [at].[prc_ReportCalcType_upd] AS' 
END
GO

ALTER PROCEDURE [at].[prc_ReportCalcType_upd]
(
	@ReportCalcTypeID int,
	@No smallint,
	@cUserid int,
	@Log smallint = 1
)
AS
BEGIN
	SET NOCOUNT ON
	DECLARE @Err Int

	UPDATE [at].[ReportCalcType]
	SET
		[ReportCalcTypeID] = @ReportCalcTypeID,
		[No] = @No
	WHERE
		[ReportCalcTypeID] = @ReportCalcTypeID

	IF @Log = 1 
	BEGIN 
		INSERT INTO [Log].[AuditLog] ( UserId, TableName, Type, Data, Created) 
		SELECT @cUserid,'ReportCalcType',1,
		( SELECT * FROM [at].[ReportCalcType] 
			WHERE
			[ReportCalcTypeID] = @ReportCalcTypeID			 FOR XML AUTO) as data,
			getdate()
	END

	Set @Err = @@Error

	RETURN @Err
END


GO
