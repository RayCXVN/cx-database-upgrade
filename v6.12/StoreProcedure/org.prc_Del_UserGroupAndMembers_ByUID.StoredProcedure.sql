SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[org].[prc_Del_UserGroupAndMembers_ByUID]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [org].[prc_Del_UserGroupAndMembers_ByUID] AS' 
END
GO
/*
	Steve - Sept 22 2016 - EntityStatus for table UserGroup
*/
ALTER PROCEDURE [org].[prc_Del_UserGroupAndMembers_ByUID]
(
	@UserID	   int,
	@HardDelete	   bit = 0,
	@Result    int out
)
AS
BEGIN
    -- exec [org].[prc_Del_UserGroupAndMembers_ByUID] 16278
    SET NOCOUNT ON
	IF @HardDelete = 0
    BEGIN
		DECLARE @DeactiveEntityStatusID INT = (SELECT EntityStatusID FROM EntityStatus WHERE CodeName='Deactive')
		UPDATE org.UserGroup SET EntityStatusID = @DeactiveEntityStatusID WHERE UserID = @UserID
	END
	ELSE
	BEGIN
		DELETE FROM org.UG_U WHERE UserGroupID IN (SELECT UserGroupID FROM org.UserGroup WHERE UserID = @UserID)
		DELETE FROM org.UserGroup WHERE UserID = @UserID
	END
    set @Result = @@Error    
END

GO
