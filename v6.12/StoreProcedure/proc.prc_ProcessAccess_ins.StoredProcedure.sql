SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[proc].[prc_ProcessAccess_ins]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [proc].[prc_ProcessAccess_ins] AS' 
END
GO

ALTER PROCEDURE [proc].[prc_ProcessAccess_ins]
(
	@ProcessAccessID int = null output,
	@ProcessID int,
	@DepartmentID int,
	@cUserid int,
	@Log smallint = 1
)
AS
BEGIN
	SET NOCOUNT ON
	DECLARE @Err Int

	INSERT INTO [proc].[ProcessAccess]
	(
		[ProcessID],
		[DepartmentID]
	)
	VALUES
	(
		@ProcessID,
		@DepartmentID
	)

	Set @Err = @@Error
	Set @ProcessAccessID = scope_identity()

	IF @Log = 1 
	BEGIN 
		INSERT INTO [Log].[AuditLog] ( UserId, TableName, Type, Data, Created) 
		SELECT @cUserid,'ProcessAccess',0,
		( SELECT * FROM [proc].[ProcessAccess] 
			WHERE
			[ProcessAccessID] = @ProcessAccessID				 FOR XML AUTO) as data,
				getdate() 
	 END

	RETURN @Err
END


GO
