SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[at].[prc_DR_Q_ins]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [at].[prc_DR_Q_ins] AS' 
END
GO

ALTER PROCEDURE [at].[prc_DR_Q_ins]
(
	@DottedRuleID int,
	@QuestionID int,
	@cUserid int,
	@Log smallint = 1
)
AS
BEGIN
	SET NOCOUNT ON
	DECLARE @Err Int

	INSERT INTO [at].[DR_Q]
	(
		[DottedRuleID],
		[QuestionID]
	)
	VALUES
	(
		@DottedRuleID,
		@QuestionID
	)

	Set @Err = @@Error

	IF @Log = 1 
	BEGIN 
		INSERT INTO [Log].[AuditLog] ( UserId, TableName, Type, Data, Created) 
		SELECT @cUserid,'DR_Q',0,
		( SELECT * FROM [at].[DR_Q] 
			WHERE
			[DottedRuleID] = @DottedRuleID AND
			[QuestionID] = @QuestionID				 FOR XML AUTO) as data,
				getdate() 
	 END

	RETURN @Err
END


GO
