SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[job].[prc_JobStatus_get]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [job].[prc_JobStatus_get] AS' 
END
GO
ALTER PROCEDURE [job].[prc_JobStatus_get]
AS
BEGIN
	SELECT [JobStatusID],[No]
	FROM [job].[JobStatus]
END

GO
