SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[log].[prc_GetRecentUserByEventType]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [log].[prc_GetRecentUserByEventType] AS' 
END
GO
/*
    Created By:     Sarah
    Created Date:   Aug 19, 2016
    Description:    Task #60464: [VOKAL2][Landing page][Code] Write event logs for recent student

    EXEC [log].[prc_GetRecentUserByEventType]  @EventTypeID = '9,28', @NumberOfRow = 6, @UserID=17225, @HDID=17319
*/

ALTER PROCEDURE [log].[prc_GetRecentUserByEventType]
    @EventTypeID    nvarchar(128)	= '13,14',
    @HDID           int = 0,
    @UserID         INT = 0,
    @NumberOfRow	INT = 10
AS
BEGIN
    DECLARE @ActiveEntityStatusID INT = (SELECT EntityStatusID FROM EntityStatus WHERE CodeName='Active')
    DECLARE @UserTableTypeID INT = (SELECT T.TableTypeID FROM TableType T WITH (NOLOCK) WHERE T.Name = 'User')
    
	;WITH TempUser AS 
	(
		SELECT ROW_NUMBER() OVER (PARTITION BY e.ItemID, e.UserID ORDER BY e.Created DESC) AS [Row], U.UserID, U.FirstName, U.LastName, 
            U.DepartmentID, D.Name DepartmentName, U.Gender, e.Created EventCreated
		FROM [log].[Event] e WITH (NOLOCK)
            INNER JOIN org.H_D hd WITH (NOLOCK) ON hd.DepartmentID = e.DepartmentID 
		        AND (@HDID = 0 OR hd.[Path] LIKE '%\' + CONVERT(NVARCHAR(16),@HDID) + '\%')
		        AND  e.EventTypeID IN (SELECT value FROM dbo.funcListToTableInt(@EventTypeID,','))
                AND (@UserID = 0 OR e.UserID = @UserID)
            INNER JOIN org.[User] U WITH (NOLOCK) ON U.UserID = E.ItemID AND U.EntityStatusID = @ActiveEntityStatusID AND U.Deleted IS NULL
            INNER JOIN org.Department D WITH (NOLOCK) ON U.DepartmentID = D.DepartmentID AND D.EntityStatusID = @ActiveEntityStatusID AND D.Deleted IS NULL
        WHERE E.TableTypeID = @UserTableTypeID
	)
	SELECT TOP (@NumberOfRow) U.UserID, U.FirstName + ' ' + U.LastName CompleteName, U.DepartmentID, U.DepartmentName, U.Gender
	FROM TempUser U
    WHERE U.[Row] = 1
    ORDER BY U.EventCreated DESC
END

GO
