SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[app].[prc_Site_get]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [app].[prc_Site_get] AS' 
END
GO
ALTER PROCEDURE [app].[prc_Site_get]  
 @OwnerID int  
AS  
BEGIN  
 SET NOCOUNT ON;  
 DECLARE @Err Int      
 SELECT   
	  [SiteID]  
      ,[OwnerID]  
      ,[CodeName]  
      ,[UseNativeLogon]  
      ,[ThemeID]  
      ,[IsClosed]  
      ,[DefaultMenuID]  
      ,[DefaultHierarchyID]  
      ,[UseMultipleMenus]  
      ,[DefaultChartTemplate]  
      ,[UseCustomer]  
      ,[UsePasswordRecovery]  
      ,[MaintenanceMode]
      ,[UseRememberMe]   
      ,[Created]
      ,[UseReturnURL]
      ,[UseSMS]
      ,UseMemoryCache		
	  ,UseDistributedCache	
	  ,DistributedCacheName  
	  ,CacheExpiredAfter
	  ,MailHeaderInfoXCompany 
	  ,MailHeaderInfoMessageID
	  ,LanguageIDWhenNotAuthenticated	
	  ,LogoutURL		
	  ,TimeOutLogoutURL
      ,Logo
	  ,CssVariables
      ,Favicon     
 FROM         
  Site  
 WHERE    
  Site.OwnerID = @OwnerID  
 Set @Err = @@Error  

 RETURN @Err  
END   

GO
