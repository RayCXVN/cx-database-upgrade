SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[prc_ResultChoice_get]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [dbo].[prc_ResultChoice_get] AS' 
END
GO
ALTER PROCEDURE [dbo].[prc_ResultChoice_get]
(
	@SurveyID 	int,
	@RoleID 	int,
	@DepartmentID 	int
)
As
	Select
      DISTINCT
      ChoiceID,
	  [No]
      FROM [Choice] C          
      WHERE 
            C.SurveyID = @SurveyID
            AND (C.RoleID = @RoleID Or C.RoleID IS NULL)
            AND (C.DepartmentID = @DepartmentID OR C.DepartmentID IS NULL)
            AND (C.DepartmentTypeID IS NULL OR C.DepartmentTypeID IN (SELECT DepartmentTypeID FROM DT_D WHERE DepartmentID = @DepartmentID))
      ORDER BY C.[No]


GO
