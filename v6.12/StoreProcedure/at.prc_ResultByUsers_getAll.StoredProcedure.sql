SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[at].[prc_ResultByUsers_getAll]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [at].[prc_ResultByUsers_getAll] AS' 
END
GO
/*
    Steve - Des 12 2016 - Create store procedure to improve performance
*/
ALTER PROCEDURE [at].[prc_ResultByUsers_getAll]
(
	@UserIDList             NVARCHAR(MAX) = '',
	@OwnerID                INT
)
As
BEGIN
	--exec [at].[prc_ResultByUsers_getAll] '16940,38924,16944', 9
	SET NOCOUNT ON
	SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED
	DECLARE @Err				int,
			@SurveyTableType	int,	-- get the ID of Survey from dbo.TableType
			@ActivityTableType	int,	-- get the ID of Activity from dbo.TableType
			@ReportServer		nvarchar(64)=N'',
			@ReportDB			nvarchar(64)=N'',
			@sqlCommand         nvarchar(max)

	SELECT  @SurveyTableType   = TableTypeID FROM dbo.TableType WHERE Name = 'Survey'
	SELECT  @ActivityTableType = TableTypeID FROM dbo.TableType WHERE Name = 'Activity'
    DECLARE @ActiveEntityStatusID VARCHAR(2) = (SELECT EntityStatusID FROM EntityStatus WHERE CodeName='Active')

	CREATE TABLE #UserList(UserID INT)
	INSERT INTO #UserList(UserID) SELECT Value FROM dbo.funcListToTableInt(@UserIDList, N',')

	CREATE TABLE #UserResult (ResultID bigint, ActivityID int, SurveyID int, UserID int, BatchID int, DepartmentID int,
	   RoleID int, UserGroupID int, StartDate datetime, EndDate datetime, StatusTypeID int,
	   ReportServer nvarchar(64), ReportDB nvarchar(64), ActivityNo smallint, LastUpdated datetime, Created datetime, ValidFrom datetime, ValidTo datetime,
	   ParentResultID bigint, ParentResultSurveyID int, CustomerID int, Deleted DATETIME2(7), EntityStatusID int, EntityStatusReasonID int)

	DECLARE c_ResultDatabase CURSOR READ_ONLY FOR
	SELECT DISTINCT s.ReportServer, s.ReportDB
	FROM at.Survey s JOIN at.Activity act ON act.ActivityID = s.ActivityID AND act.OwnerID = @OwnerID
	DECLARE @LinkedDB NVARCHAR(MAX)
	
	OPEN c_ResultDatabase
	FETCH NEXT FROM c_ResultDatabase INTO @ReportServer, @ReportDB
	WHILE @@FETCH_STATUS=0
	BEGIN
		IF @ReportDB=DB_NAME() AND @ReportServer=@@servername
			BEGIN
			SET @LinkedDB=' '
			END
		ELSE
			BEGIN
			SET @LinkedDB= '['+ @ReportServer + '].['+ @ReportDB + '].'
			END
	   -- Get result except denied surveys
	   SET @sqlCommand = N'INSERT INTO #UserResult (ResultID, ActivityID, SurveyID, UserID, BatchID, DepartmentID, RoleID, UserGroupID, StartDate, EndDate, StatusTypeID, ReportServer, ReportDB, ActivityNo, 
			LastUpdated, Created, ValidFrom, ValidTo, ParentResultID, ParentResultSurveyID, CustomerID, Deleted, EntityStatusID, EntityStatusReasonID)
	   SELECT ResultID, NULL, SurveyID, UserID, BatchID, DepartmentID, RoleID, UserGroupID, StartDate, EndDate, StatusTypeID, ''' + @ReportServer + ''', ''' + @ReportDB + ''', NULL, 
			LastUpdated, Created, ValidFrom, ValidTo, ParentResultID, ParentResultSurveyID, CustomerID, Deleted, EntityStatusID, EntityStatusReasonID
	   FROM	'+ @LinkedDB +'dbo.Result r
	   WHERE  r.UserID IN (SELECT UserID FROM #UserList)
	     AND  r.EntityStatusID = '+@ActiveEntityStatusID+' AND r.Deleted IS NULL '

	   EXECUTE sp_executesql @sqlCommand

	   FETCH NEXT FROM c_ResultDatabase INTO @ReportServer, @ReportDB
	END
	CLOSE c_ResultDatabase
	DEALLOCATE c_ResultDatabase

	UPDATE #UserResult SET ActivityID = (SELECT s.ActivityID FROM at.Survey s WHERE s.SurveyID = ur.SurveyID)
	FROM #UserResult ur
	
	DELETE FROM #UserResult WHERE ActivityID IS NULL

	UPDATE #UserResult SET ActivityNo = (SELECT a.No FROM at.Activity a WHERE a.ActivityID = ur.ActivityID)
	FROM #UserResult ur

	SELECT * FROM #UserResult ORDER BY ActivityNo, StartDate DESC

	DROP TABLE #UserList
	DROP TABLE #UserResult

	Set @Err = @@Error

	RETURN @Err
END


GO
