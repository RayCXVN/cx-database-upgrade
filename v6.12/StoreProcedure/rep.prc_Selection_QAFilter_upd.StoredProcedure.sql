SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[rep].[prc_Selection_QAFilter_upd]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [rep].[prc_Selection_QAFilter_upd] AS' 
END
GO
  
ALTER PROCEDURE [rep].[prc_Selection_QAFilter_upd]  
(  
 @SQAID int,  
 @AlternativeID int,  
 @Type smallint,  
 @cUserid int,  
 @Log smallint = 1,
 @ItemID int = NULL
)  
AS  
BEGIN  
 SET NOCOUNT ON  
 DECLARE @Err Int  
  
 UPDATE [rep].[Selection_QAFilter]  
 SET  
  [SQAID] = @SQAID,  
  [AlternativeID] = @AlternativeID,  
  [Type] = @Type,
  [ItemID] = @ItemID
 WHERE  
  [SQAID] = @SQAID AND  
  [AlternativeID] = @AlternativeID  
  
 IF @Log = 1   
 BEGIN   
  INSERT INTO [Log].[AuditLog] ( UserId, TableName, Type, Data, Created)   
  SELECT @cUserid,'Selection_QAFilter',1,  
  ( SELECT * FROM [rep].[Selection_QAFilter]   
   WHERE  
   [SQAID] = @SQAID AND  
   [AlternativeID] = @AlternativeID    FOR XML AUTO) as data,  
   getdate()  
 END  
  
 Set @Err = @@Error  
  
 RETURN @Err  
END  
    

GO
