SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[at].[prc_LT_XCategoryType_ins]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [at].[prc_LT_XCategoryType_ins] AS' 
END
GO
ALTER PROCEDURE [at].[prc_LT_XCategoryType_ins] (
	@LanguageID INT
	,@XCategoryTypeID INT
	,@Name NVARCHAR(256)
	,@Description NVARCHAR(max)
	,@cUserid INT
	,@Log SMALLINT = 1
	)
AS
BEGIN
	SET NOCOUNT ON

	DECLARE @Err INT

	INSERT INTO [at].[LT_XCategoryType] (
		[LanguageID]
		,[XCTypeID]
		,[Name]
		,[Description]
		)
	VALUES (
		@LanguageID
		,@XCategoryTypeID
		,@Name
		,@Description
		)

	SET @Err = @@Error

	IF @Log = 1
	BEGIN
		INSERT INTO [Log].[AuditLog] (
			UserId
			,TableName
			,Type
			,Data
			,Created
			)
		SELECT @cUserid
			,'LT_XCategoryType'
			,0
			,(
				SELECT *
				FROM [at].[LT_XCategoryType]
				WHERE [LanguageID] = @LanguageID
					AND [XCTypeID] = @XCategoryTypeID
				FOR XML AUTO
				) AS data
			,GETDATE()
	END

	RETURN @Err
END

GO
