SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[at].[prc_Question_get_by_activityID]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [at].[prc_Question_get_by_activityID] AS' 
END
GO
/*  2019-02-21 Ray:     Sort by Pages then Questions
*/
ALTER PROCEDURE [at].[prc_Question_get_by_activityID](
    @ActivityID int)
AS
BEGIN
    SET NOCOUNT ON;
    DECLARE @Err int;

    SELECT q.[QuestionID], q.[PageID], ISNULL(q.[ScaleID], 0) AS 'ScaleID', q.[No], q.[Type], q.[Inverted], q.[Mandatory], q.[Status], q.[CssClass], q.[ExtId],
           q.[Tag], q.[Created], ISNULL(q.[SectionID], 0) AS 'SectionID', q.[Width]
    FROM [at].[Question] q
    JOIN [at].[Page] p ON p.[PageID] = q.[PageID] AND p.[ActivityID] = @ActivityID
    ORDER BY p.[No], q.[No];

    SET @Err = @@Error;

    RETURN @Err;
END;
GO