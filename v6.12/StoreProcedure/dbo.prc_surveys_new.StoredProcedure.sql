SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[prc_surveys_new]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [dbo].[prc_surveys_new] AS' 
END
GO
/*  2016-10-10 Ray: Remove column Status from org.User, org.Department, RDB.dbo.Result
*/
ALTER proc [dbo].[prc_surveys_new]
(   @SiteID      int = 0,
    @OwnerID     int = 0
)
--WITH RECOMPILE
AS
BEGIN
SET NOCOUNT ON
BEGIN TRANSACTION
DECLARE @ActiveEntityStatusID INT, @StatusReason_ActiveNone int

SELECT @ActiveEntityStatusID = EntityStatusID FROM EntityStatus WHERE CodeName='Active'
SELECT @StatusReason_ActiveNone = [EntityStatusReasonID] FROM [EntityStatusReason] WHERE [CodeName] = 'Active_None'

declare 
	@PID1        int,
    @Name        varchar(50),
    @StartDate   datetime,
    @EndDate     datetime,
    @FromTag     int = 0,
    @ToTag       int = 0

set language danish; -- For correct month name

select @PID1 = PeriodID from at.Period where GETDATE() between Startdate and Enddate; --Current periodID
select @Name = UPPER(LEFT(DATENAME(month, GETDATE()),1)) + SUBSTRING(DATENAME(month, GETDATE()), 2, LEN(DATENAME(month, GETDATE()))-1) + ' ' +  DATENAME(year, GETDATE()); --Survey name
SELECT @StartDate = DATEADD(DAY, 1, EOMONTH(GETDATE(), -1))-- survey StartDate set to the first date of current month
SELECT @EndDate = DATEADD(DAY, 1, EOMONTH(GETDATE(), 0))-- survey EndDate set to the first date of next month
SELECT @FromTag = MAX(CAST([Tag] AS int)) FROM at.[Survey] WHERE ISNUMERIC([Tag]) = 1
SET @ToTag = @FromTag + 1

set LANGUAGE us_english;


DECLARE @SID         int,
        @ActivityID  int,
        @NewID       int,
        @NewSID      int,
        @BID         int,
        @Msg         nvarchar(max),
        @NewSurveyIDList    nvarchar(max),
        @TrivselsSurvey     nvarchar(max),
        @NewStatusTypeID    int,
        @DT_Class           int,
        @UT_Student         int,
        @BatchHDID          int,
        @ScriptUserID       int

SELECT @DT_Class   = [p].[Value] FROM app.[SiteParameter] p WHERE p.[SiteID] = @SiteID AND p.[Key] = 'Platform.Common.ClassDepartmentTypeId'
SELECT @UT_Student = [p].[Value] FROM app.[SiteParameter] p WHERE p.[SiteID] = @SiteID AND p.[Key] = 'Platform.Common.ElevUserType'
SELECT @ScriptUserID = [UserID] FROM org.[User] where [UserName] = 'vokalint'

DECLARE @TrivselsSurveyTable TABLE (SurveyID int)        

--Survey
DECLARE @SurveyTable TABLE (SurveyID int, [ActivityID] int)

INSERT INTO @SurveyTable (SurveyID, [ActivityID])
SELECT SurveyID, s.[ActivityID]
FROM at.Survey s
--INNER JOIN XC_A xc ON s.ActivityID = xc.ActivityID
WHERE s.Tag = CONVERT(NVARCHAR(256), @FromTag)

--Delete result dust
DELETE R
FROM Result R 
JOIN @SurveyTable S ON S.SurveyID = R.SurveyID
WHERE NOT EXISTS (SELECT 1 FROM Answer A WHERE R.ResultID=A.ResultID)

DECLARE curSur CURSOR LOCAL FAST_FORWARD READ_ONLY FOR
SELECT SurveyID, [ActivityID] FROM @SurveyTable

OPEN curSur
FETCH NEXT FROM curSur INTO @SID, @ActivityID

RAISERROR ('START', 0, 1)

WHILE (@@FETCH_STATUS = 0)
BEGIN
       SET @Msg = 'Processing survey ' + convert(nvarchar(32),@SID)
       RAISERROR (@Msg, 0, 1)

       INSERT at.Survey(ActivityID, HierarchyID, StartDate, EndDate, Anonymous, Status, ShowBack, LanguageID, ButtonPlacement, UsePageNo, LinkURL, FinishURL, CreateResult, ReportDB, ReportServer, StyleSheet, Type, Created, LastProcessed, No, OLAPServer, OLAPDB, PeriodID, ProcessCategorys, DeleteResultOnUserDelete, FromSurveyID, LastProcessedResultID, LastProcessedAnswerID, ExtId, Tag)
       SELECT ActivityID, HierarchyID, @StartDate, @EndDate, Anonymous, Status, ShowBack, LanguageID, ButtonPlacement, UsePageNo, LinkURL, FinishURL, CreateResult, ReportDB, ReportServer, StyleSheet, Type, GETDATE(), LastProcessed, No, OLAPServer, OLAPDB, @PID1, ProcessCategorys, DeleteResultOnUserDelete, SurveyID, LastProcessedResultID, LastProcessedAnswerID, ExtId, @ToTag
       FROM at.Survey WHERE SurveyID = @SID
       PRINT 'GML:'

       SET @NewSID = SCOPE_IDENTITY()
	   UPDATE at.Survey SET EndDate = @StartDate
	   WHERE SurveyID = @SID

       PRINT 'NY:'
       INSERT at.Access(SurveyID, RoleID, DepartmentID, DepartmentTypeID, CustomerID, PageID, QuestionID, Type, Mandatory, [HDID])
       SELECT @NewSID, RoleID, DepartmentID, DepartmentTypeID, CustomerID, PageID, QuestionID, Type, Mandatory, [HDID]
       FROM at.Access 
       WHERE SurveyID = @SID
       
       INSERT at.LT_Survey(LanguageID, SurveyID, Name, Description, Info, FinishText, DisplayName)
       SELECT l.LanguageID, @NewSID, @Name, l.Description, Info, FinishText, @Name 
       FROM at.LT_Survey l      
       WHERE SurveyID = @SID
                    
       --Batch
       DECLARE curB CURSOR LOCAL FAST_FORWARD READ_ONLY FOR
       SELECT BatchID
       FROM at.[Batch]
       WHERE SurveyID = @SID
       
       OPEN curB
       FETCH NEXT FROM curB INTO @BID
       
       WHILE (@@FETCH_STATUS = 0)
       BEGIN
             INSERT at.[Batch](SurveyID, UserID, DepartmentID, Name, [No], StartDate, EndDate, [Status], MailStatus)
             SELECT @NewSID, UserID, DepartmentID, Name, [No], @StartDate, @EndDate, [Status], MailStatus
             FROM at.[Batch]        
             WHERE BatchID = @BID
             
             SET @NewID = SCOPE_IDENTITY()
			 UPDATE at.Batch SET EndDate = @StartDate
			 WHERE BatchID = @BID
             
             IF EXISTS (SELECT 1 FROM at.[XC_A] xca JOIN at.[XCategory] xc ON xca.[XCID] = xc.[XCID] AND xca.[ActivityID] = @ActivityID AND xc.[ExtId] = 'StudentSurvey')
             BEGIN
                 SELECT @NewStatusTypeID = st.[StatusTypeID] FROM at.[StatusType] st WHERE st.[OwnerID] = @OwnerID AND st.[CodeName] = 'Assigned'
                 SELECT @BatchHDID = hd.[HDID] FROM at.[Batch] b JOIN org.[H_D] hd ON b.[DepartmentID] = hd.[DepartmentID] AND b.[BatchID] = @BID

                 print 'Add new results for all students'
                 INSERT Result(StartDate, EndDate, RoleID, UserID, UserGroupID, SurveyID, BatchID, LanguageID, PageNo, DepartmentID, [Anonymous], ShowBack, ResultKey, ParentResultID, ParentResultSurveyID, LastUpdated, LastUpdatedBy, StatusTypeID, Created, CreatedBy, CustomerID, Deleted, EntityStatusID, EntityStatusReasonID)
                 SELECT NULL, NULL, rm.[RoleID], u.[UserID], 0, @NewSID, @NewID, u.[LanguageID], 0, u.[DepartmentID], 0, 0, '', NULL, NULL, getdate(), ISNULL(@ScriptUserID, u.[UserID]), @NewStatusTypeID, GETDATE(), ISNULL(@ScriptUserID, u.[UserID]), d.CustomerID, NULL, @ActiveEntityStatusID, @StatusReason_ActiveNone
                 FROM org.[User] u JOIN org.[Department] d ON u.[DepartmentID] = d.[DepartmentID] AND d.EntityStatusID = @ActiveEntityStatusID AND u.EntityStatusID = @ActiveEntityStatusID AND u.Deleted IS NULL AND d.Deleted IS NULL
                 JOIN org.[H_D] hd ON d.[DepartmentID] = hd.[DepartmentID] AND hd.[Deleted] = 0 AND hd.[Path] LIKE '%\' + CAST(@BatchHDID AS nvarchar(32)) + '\%'
                 JOIN org.[UT_U] utu ON u.[UserID] = utu.[UserID] AND utu.[UserTypeID] = @UT_Student
                 JOIN at.[RoleMapping] rm ON rm.[ActivityID] = @ActivityID
                 JOIN org.[UT_U] ut ON rm.[UserTypeID] = ut.[UserTypeID] AND ut.[UserID] = u.[UserID]

                 RAISERROR ('%d results inserted', 0, 1, @@rowcount) WITH NOWAIT
             END
             ELSE IF EXISTS (SELECT 1 FROM at.[XC_A] xca JOIN at.[XCategory] xc ON xca.[XCID] = xc.[XCID] AND xca.[ActivityID] = @ActivityID AND xc.[ExtId] = 'FFM')
             BEGIN
                 print 'Kopierer resultater inn i ny batch'
                 INSERT Result(StartDate, EndDate, RoleID, UserID, UserGroupID, SurveyID, BatchID, LanguageID, PageNo, DepartmentID, [Anonymous], ShowBack, ResultKey, ParentResultID, ParentResultSurveyID, LastUpdated, LastUpdatedBy, StatusTypeID, Created, CreatedBy, CustomerID, Deleted, EntityStatusID, EntityStatusReasonID)
                 SELECT StartDate, EndDate, RoleID, UserID, UserGroupID, @NewSID, @NewID, LanguageID, PageNo, DepartmentID, [Anonymous], ShowBack, ResultKey, ResultID, SurveyID, LastUpdated, LastUpdatedBy, StatusTypeID, Created, CreatedBy, CustomerID, Deleted, EntityStatusID, EntityStatusReasonID
                 FROM Result 
                 WHERE BatchID = @BID AND EntityStatusID = @ActiveEntityStatusID AND Result.Deleted IS NULL
                                        
                 print 'Kopierer svarene'
                 INSERT Answer(ResultID, QuestionID, AlternativeID, Value, DateValue, Free, [No], ItemID, [CustomerID], [LastUpdated], [LastUpdatedBy], [Created], [CreatedBy]) 
                 SELECT RN.ResultID, A.QuestionID, A.AlternativeID, A.Value, A.DateValue, A.Free, A.[No], A.ItemID, A.[CustomerID], A.[LastUpdated], A.[LastUpdatedBy], A.[Created] ,A.[CreatedBy]  FROM Answer A
                 INNER JOIN Result R ON A.ResultID = R.ResultID
                 INNER JOIN Result RN ON RN.ParentResultID = R.ResultID         
                 WHERE R.BatchID = @BID AND RN.BatchID = @NewID 
             END
             ELSE
             BEGIN
                 print 'Kopierer resultater inn i ny batch'
                 INSERT Result(StartDate, EndDate, RoleID, UserID, UserGroupID, SurveyID, BatchID, LanguageID, PageNo, DepartmentID, [Anonymous], ShowBack, ResultKey, ParentResultID, ParentResultSurveyID, LastUpdated, LastUpdatedBy, StatusTypeID, Created, CreatedBy, CustomerID, Deleted, EntityStatusID, EntityStatusReasonID)
                 SELECT StartDate, EndDate, RoleID, UserID, UserGroupID, @NewSID, @NewID, LanguageID, PageNo, DepartmentID, [Anonymous], ShowBack, ResultKey, ResultID, SurveyID, LastUpdated, LastUpdatedBy, StatusTypeID, Created, CreatedBy, CustomerID, Deleted, EntityStatusID, EntityStatusReasonID
                 FROM Result 
                 WHERE BatchID = @BID AND EntityStatusID = @ActiveEntityStatusID AND Result.Deleted IS NULL
                                        
                 print 'Kopierer svarene'
                 INSERT Answer(ResultID, QuestionID, AlternativeID, Value, DateValue, Free, [No], ItemID, [CustomerID], [LastUpdated], [LastUpdatedBy], [Created], [CreatedBy]) 
                 SELECT RN.ResultID, A.QuestionID, A.AlternativeID, A.Value, A.DateValue, A.Free, A.[No], A.ItemID , A.[CustomerID], A.[LastUpdated], A.[LastUpdatedBy], A.[Created] ,A.[CreatedBy] FROM Answer A
                 INNER JOIN Result R ON A.ResultID = R.ResultID
                 INNER JOIN Result RN ON RN.ParentResultID = R.ResultID         
                 WHERE R.BatchID = @BID AND RN.BatchID = @NewID 
             END

             FETCH NEXT FROM curB INTO @BID
       END
       CLOSE curB
       DEALLOCATE curB
       update Survey set LastProcessed='1900-01-01' where SurveyID=@NewSID
                
       FETCH NEXT FROM curSur INTO @SID, @ActivityID
END
CLOSE curSur
DEALLOCATE curSur 

SELECT TOP 1 @TrivselsSurvey = [Value] FROM app.[SiteParameter] WHERE [Key] = 'Vokal.UniLogin.TrivselsIds' AND [SiteID] = @SiteID
IF ISNULL(@TrivselsSurvey, '') != ''
BEGIN
    INSERT INTO @TrivselsSurveyTable ([SurveyID]) SELECT [ParseValue] FROM dbo.StringToArray(@TrivselsSurvey, ',')

    SET @NewSurveyIDList = STUFF((SELECT ',' + CONVERT(nvarchar(32),s.[SurveyID])
                                  FROM at.[Survey] s JOIN @TrivselsSurveyTable st ON s.FromSurveyID = st.[SurveyID] AND s.[PeriodID] = @PID1
                                  FOR XML PATH ('')), 1, 1, '')
    PRINT 'Set new site parameter Vokal.UniLogin.TrivselsIds = "' + @NewSurveyIDList + '"'

    UPDATE app.[SiteParameter] SET [Value] = @NewSurveyIDList WHERE [Key] = 'Vokal.UniLogin.TrivselsIds' AND [SiteID] = @SiteID
END
COMMIT TRANSACTION
END


GO
