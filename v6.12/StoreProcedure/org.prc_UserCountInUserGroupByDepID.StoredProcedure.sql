SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[org].[prc_UserCountInUserGroupByDepID]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [org].[prc_UserCountInUserGroupByDepID] AS' 
END
GO
/*
    Steve - Sept 22 2016 - EntityStatus for table UserGroup
    2018-07-12 Ray:     Replace UG_U by UGMember

EXEC [org].[prc_UserCountInUserGroupByDepID] 23824, 4, '38'
*/
ALTER PROCEDURE [org].[prc_UserCountInUserGroupByDepID]
(
  @DepartmentID         int,
  @PeriodID             int,
  @DepartmentTypeIDList varchar(max)
) AS
BEGIN
    SET NOCOUNT ON
    DECLARE @ActiveEntityStatusID INT = (SELECT EntityStatusID FROM EntityStatus WHERE CodeName='Active')
    
    SELECT ug.UserGroupID, ug.Name UserGroupName, COUNT(ugm.UserID) AS [UserCount]
    FROM org.UserGroup ug
    JOIN org.[UGMember] ugm ON ugm.UserGroupID = ug.UserGroupID AND ug.DepartmentID = @DepartmentID AND ug.PeriodID = @PeriodID
     AND ug.EntityStatusID = @ActiveEntityStatusID AND ug.Deleted IS NULL
     AND ugm.[EntityStatusID] = @ActiveEntityStatusID AND ugm.[Deleted] IS NULL
    JOIN org.[User] u ON u.UserID = ugm.UserID AND u.EntityStatusID = @ActiveEntityStatusID AND u.Deleted IS NULL
    JOIN org.DT_UG dtug ON dtug.UserGroupID = ug.UserGroupID AND dtug.DepartmentTypeID IN (SELECT Value From dbo.funcListToTableInt(@DepartmentTypeIDList,','))
    GROUP BY ug.UserGroupID, ug.Name
    ORDER BY ug.Name
END  

GO
