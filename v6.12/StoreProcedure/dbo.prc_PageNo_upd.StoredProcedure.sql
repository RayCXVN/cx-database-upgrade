SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[prc_PageNo_upd]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [dbo].[prc_PageNo_upd] AS' 
END
GO
ALTER PROC [dbo].[prc_PageNo_upd]
(
	@ResultID	bigint,
	@PageNo		smallint
)
AS
UPDATE Result
SET PageNo = @PageNo
WHERE ResultID = @ResultID


GO
