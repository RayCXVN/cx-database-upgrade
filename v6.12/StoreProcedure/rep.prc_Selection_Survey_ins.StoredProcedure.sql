SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[rep].[prc_Selection_Survey_ins]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [rep].[prc_Selection_Survey_ins] AS' 
END
GO

ALTER PROCEDURE [rep].[prc_Selection_Survey_ins]
(
	@S_SurveyID int = null output,
	@SelectionID int,
	@SurveyID int,
	@cUserid int,
	@Log smallint = 1
)
AS
BEGIN
	SET NOCOUNT ON
	DECLARE @Err Int

	INSERT INTO [rep].[Selection_Survey]
	(
		[SelectionID],
		[SurveyID]
	)
	VALUES
	(
		@SelectionID,
		@SurveyID
	)

	Set @Err = @@Error
	Set @S_SurveyID = scope_identity()

	IF @Log = 1 
	BEGIN 
		INSERT INTO [Log].[AuditLog] ( UserId, TableName, Type, Data, Created) 
		SELECT @cUserid,'Selection_Survey',0,
		( SELECT * FROM [rep].[Selection_Survey] 
			WHERE
			[S_SurveyID] = @S_SurveyID				 FOR XML AUTO) as data,
				getdate() 
	 END

	RETURN @Err
END


GO
