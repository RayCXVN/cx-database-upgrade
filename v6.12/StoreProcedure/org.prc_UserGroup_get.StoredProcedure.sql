IF NOT EXISTS (SELECT 1 FROM sys.objects WHERE name = N'prc_UserGroup_get' AND type IN ( N'P', N'PC' )  AND SCHEMA_NAME(schema_id) = 'org')
	EXEC ('CREATE PROC [org].[prc_UserGroup_get] AS ')
GO

/*
    Steve - Sept 22 2016 - EntityStatus for table UserGroup
    Sarah - 2018-07-05   - ATAdmin/CXPLAT-294/Extend UG_UCXPLAT-854
    2019-02-28 Ray:     Improve performance for ATAdmin
*/
ALTER PROCEDURE [org].[prc_UserGroup_get](
    @DepartmentID int = NULL,
    @SurveyID     int = NULL,
    @Userid       int = NULL
) AS
BEGIN
    SET NOCOUNT ON;
    DECLARE @Err int;
    DECLARE @ActiveEntityStatusID int= (SELECT [EntityStatusID] FROM [EntityStatus] WHERE [CodeName] = 'Active');

    SELECT [ug].[UserGroupID], [ug].[OwnerID], ISNULL([ug].[DepartmentID], 0) AS 'DepartmentID', ISNULL([ug].[SurveyId], 0) AS 'SurveyId', [ug].[Name],
           [ug].[Description], [ug].[Created], [ug].[ExtID], ISNULL([ug].[PeriodID], 0) AS 'PeriodID', ISNULL([ug].[UserID], 0) AS 'UserID',
           ISNULL([ug].[UsergroupTypeID], 0) AS 'UserGroupTypeID', [ug].[Tag], [ug].[LastUpdated], [ug].[ReferrerResource], [ug].[ReferrerToken],
           [ug].[ReferrerArchetypeID], [ug].[LastUpdatedBy], [ug].[LastSynchronized], [ug].[ArchetypeID], [ug].[Deleted], [ug].[EntityStatusID],
           [ug].[EntityStatusReasonID], [ug].[ReferrerToken], [ug].[ReferrerResource], [ug].[ReferrerArchetypeID],
           ISNULL(STUFF((SELECT ',' + CAST(dt.[DepartmentTypeID] AS nvarchar(32)) AS [text()]
                         FROM [org].[DT_UG] dt
                         WHERE dt.[UserGroupID] = ug.[UserGroupID]
                         FOR XML PATH (''))
                        , 1, 1, ''), '') AS [DepartmentTypeList]
    FROM [org].[UserGroup] [ug]
    LEFT JOIN [org].[Department] [d] ON [d].[DepartmentID] = [ug].[DepartmentID]
    WHERE ([ug].[DepartmentID] = @DepartmentID OR @DepartmentID IS NULL)
      AND ([ug].[SurveyId] = @SurveyID OR @SurveyID IS NULL)
      AND ([ug].[UserId] = @UserID OR @UserID IS NULL)
      AND ([ug].[DepartmentID] IS NULL OR ([d].[EntityStatusID] = @ActiveEntityStatusID AND [d].[Deleted] IS NULL)
          )
      AND [ug].[EntityStatusID] = @ActiveEntityStatusID
      AND [ug].[Deleted] IS NULL;
    SET @Err = @@Error;

    RETURN @Err;
END;