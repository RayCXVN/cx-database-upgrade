SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[prc_TableType_get]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [dbo].[prc_TableType_get] AS' 
END
GO



ALTER PROCEDURE [dbo].[prc_TableType_get]
AS
BEGIN
	SET NOCOUNT ON
	DECLARE @Err Int

	SELECT
	[TableTypeID],
	[Name],
	[Schema],
	[Type]
	FROM [dbo].[TableType]

	Set @Err = @@Error

	RETURN @Err
END



GO
