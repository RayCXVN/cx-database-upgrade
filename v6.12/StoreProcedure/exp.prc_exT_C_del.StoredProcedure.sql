SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[exp].[prc_exT_C_del]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [exp].[prc_exT_C_del] AS' 
END
GO


ALTER PROCEDURE [exp].[prc_exT_C_del]
(
	@TypeID smallint,
	@ColumnID smallint,
	@cUserid int,
	@Log smallint = 1
)
AS
BEGIN
	SET NOCOUNT ON
	DECLARE @Err Int

	IF @Log = 1 
	BEGIN 
		INSERT INTO [Log].[AuditLog] ( UserId, TableName, Type, Data, Created) 
		SELECT @cUserid,'exT_C',2,
		( SELECT * FROM [exp].[exT_C] 
			WHERE
			[TypeID] = @TypeID AND
			[ColumnID] = @ColumnID
			 FOR XML AUTO) as data,
			getdate() 
	END 


	DELETE FROM [exp].[exT_C]
	WHERE
		[TypeID] = @TypeID AND
		[ColumnID] = @ColumnID

	Set @Err = @@Error

	RETURN @Err
END


GO
