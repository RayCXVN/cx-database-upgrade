SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[at].[prc_LT_A_S_get]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [at].[prc_LT_A_S_get] AS' 
END
GO
ALTER PROCEDURE [at].[prc_LT_A_S_get] (@ASID INT)
AS
BEGIN
	SET NOCOUNT ON

	DECLARE @Err INT

	SELECT [ASID],
		[LanguageID],
		[StatusName],
		[StatusDescription],
		[ReadOnlyText]
	FROM [at].[LT_A_S]
	WHERE [ASID] = @ASID

	SET @Err = @@Error

	RETURN @Err
END

GO
