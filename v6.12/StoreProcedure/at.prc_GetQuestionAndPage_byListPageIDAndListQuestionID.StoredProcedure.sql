SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[at].[prc_GetQuestionAndPage_byListPageIDAndListQuestionID]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [at].[prc_GetQuestionAndPage_byListPageIDAndListQuestionID] AS' 
END
GO
ALTER PROCEDURE [at].[prc_GetQuestionAndPage_byListPageIDAndListQuestionID]
(
	@ListPageID nvarchar(max),
	@ListQuestionID nvarchar(max),
	@LanguageID int,
	@DefaultLanguageID int
)
AS
BEGIN

	SELECT  p.PageID, p.ActivityID, p.[No], p.Created, p.Type, p.CssClass, p.ExtID, Tag,ISNULL (lp.LanguageID, lpp.LanguageID) as LanguageID, ISNULL (lp.Name,lpp.Name) as Name  FROM at.Page p 
	LEFT JOIN at.LT_Page lp ON lp.PageID = p.PageID AND  lp.LanguageID =@LanguageID
	LEFT JOIN at.LT_Page lpp ON lpp.PageID = p.PageID AND lpp.LanguageID =@DefaultLanguageID
	WHERE p.PageID  IN (SELECT value FROM dbo.funcListToTableInt(@ListPageID,','))

	SELECT q.QuestionID, q.PageID, q.ScaleID, q.No, q.Type, q.Inverted, q.Mandatory, q.Status, q.CssClass, q.ExtId, q.Tag, q.Created, q.SectionID,ISNULL( lq.LanguageID,lqq.LanguageID) as LanguageID,ISNULL( lq.Name,lqq.Name) as Name from at.Question q 
	LEFT JOIN at.LT_Question lq ON lq.QuestionID = q.QuestionID AND lq.LanguageID =@LanguageID
	LEFT JOIN at.LT_Question lqq ON lqq.QuestionID =q.QuestionID AND lqq.LanguageID =@DefaultLanguageID
	WHERE q.QuestionID IN (SELECT value FROM dbo.funcListToTableInt(@ListQuestionID,',')) 

END

GO
