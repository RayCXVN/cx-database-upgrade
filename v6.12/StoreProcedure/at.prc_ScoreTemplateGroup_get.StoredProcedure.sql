SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[at].[prc_ScoreTemplateGroup_get]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [at].[prc_ScoreTemplateGroup_get] AS' 
END
GO

ALTER PROCEDURE [at].[prc_ScoreTemplateGroup_get]
(
	@ActivityID int = null,
	@XCID int = null
)
AS
BEGIN
	SET NOCOUNT ON
	DECLARE @Err Int

	SELECT
	[ScoreTemplateGroupID] AS 'STGroupID',
	ISNULL([ActivityID], 0) AS 'ActivityID',
	ISNULL([XCID], 0) AS 'XCID',
	[Created]
	FROM [at].[ScoreTemplateGroup]
	WHERE
	(@ActivityID IS NOT NULL AND [ActivityID] = @ActivityID) OR
	(@XCID IS NOT NULL AND [XCID] = @XCID)
	
	Set @Err = @@Error

	RETURN @Err
END


GO
