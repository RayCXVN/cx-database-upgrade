IF NOT EXISTS (SELECT 1 FROM sys.objects WHERE name = N'prc_LT_MemberRole_get' AND type IN ( N'P', N'PC' )  AND SCHEMA_NAME(schema_id) = 'org')
	EXEC ('CREATE PROC [org].[prc_LT_MemberRole_get] AS ')
GO
/*  
    2018-07-19 Sarah    Create procedures for Insert, Update, Delete, Get of org.MemberRole
    2019-02-21 Ray      Get By @OwnerID
*/
ALTER PROCEDURE [org].[prc_LT_MemberRole_get]
(
    @OwnerID int
)
AS
BEGIN
    SET NOCOUNT ON
    DECLARE @Err Int

    SELECT lt.[MemberRoleID], lt.[LanguageID], lt.[Name], lt.[Description]
    FROM [org].[MemberRole] mr
    JOIN [org].[LT_MemberRole] lt ON lt.[MemberRoleID] = mr.[MemberRoleID] AND mr.[OwnerID] = @OwnerID;

    SET @Err = @@Error

    RETURN @Err
END