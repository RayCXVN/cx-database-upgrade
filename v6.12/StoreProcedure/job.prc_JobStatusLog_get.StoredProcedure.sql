SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[job].[prc_JobStatusLog_get]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [job].[prc_JobStatusLog_get] AS' 
END
GO

ALTER PROCEDURE [job].[prc_JobStatusLog_get]
(
	@JobID int
)
AS
BEGIN
	SET NOCOUNT ON
	DECLARE @Err Int

	SELECT
	[JobStatusLogID],
	[JobID],
	[JobStatusID],
	[Description],
	[Created]
	FROM [job].[JobStatusLog]
	WHERE
	[JobID] = @JobID

	Set @Err = @@Error

	RETURN @Err
END


GO
