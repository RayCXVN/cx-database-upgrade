SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[app].[prc_SiteUserSetting_upd]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [app].[prc_SiteUserSetting_upd] AS' 
END
GO
ALTER PROCEDURE [app].[prc_SiteUserSetting_upd]  
 @SiteUserSettingID int = null output,
 @UserID int,
 @SiteID int,
 @EditMode smallint,
 @AutoFitScreen smallint,
 @ScreenResolution int,
 @ExportTooltip smallint,
 @cUserid int,
 @Log smallint = 1
AS  
BEGIN  
 SET NOCOUNT ON;  
 DECLARE @Err Int  
      
    UPDATE [app].[SiteUserSetting]  
    SET		[EditMode] = @EditMode
			,[AutoFitScreen] = @AutoFitScreen
			,[ScreenResolution] = @ScreenResolution
			,[ExportTooltip] = @ExportTooltip
     WHERE [SiteID] = @SiteID  
			AND [UserID] = @UserID
             
    Set @Err = @@Error  
    Set @SiteUserSettingID = scope_identity()  
 IF @Log = 1   
 BEGIN   
  INSERT INTO [Log].[AuditLog] ( UserId, TableName, Type, Data, Created)   
  SELECT @cUserid,'SiteUserSetting',0,  
  (SELECT * FROM [app].[SiteUserSetting]  
   WHERE  
   [SiteUserSettingID] = @SiteUserSettingID     FOR XML AUTO) as data,  
    getdate()   
 END  
   
 RETURN @Err         
END  

GO
