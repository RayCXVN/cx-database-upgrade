SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[form].[prc_FormField_ins]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [form].[prc_FormField_ins] AS' 
END
GO
ALTER PROCEDURE [form].[prc_FormField_ins]  
(  
 @FormFieldID int = null output,  
 @FormID int,  
 @No smallint = 0,  
 @TableTypeID SMALLINT=NULL,  
 @RelatedTableTypeID SMALLINT=NULL,  
 @FieldTypeID int,  
 @ValueType smallint,  
 @FormMode smallint,  
 @RenderType smallint,  
 @DefaultValue nvarchar(256),  
 @AvailableValues nvarchar(256),  
 @Mandatory bit,  
 @Multiple bit,  
 @PropertyID INT=NULL,  
 @FieldName nvarchar(256),  
 @Parameters nvarchar(256),  
 @cUserid int,  
 @Log smallint = 1,
 @OverrideLocked bit,
 @CssClass nvarchar(max) = ''
)  
AS  
BEGIN  
 SET NOCOUNT ON  
 DECLARE @Err Int  
  
 INSERT INTO [form].[FormField]  
 (  
  [FormID],  
  [TableTypeID],  
  [No],  
  [RelatedTableTypeID],  
  [FieldTypeID],  
  [ValueType],  
  [FormMode],  
  [RenderType],  
  [DefaultValue],  
  [AvailableValues],  
  [Mandatory],  
  [Multiple],  
  [PropertyID],  
  [FieldName],  
  [Parameters],
  [OverrideLocked],
  [CssClass]
 )  
 VALUES  
 (  
  @FormID,  
  @TableTypeID,  
  @No,  
  @RelatedTableTypeID,  
  @FieldTypeID,  
  @ValueType,  
  @FormMode,  
  @RenderType,  
  @DefaultValue,  
  @AvailableValues,  
  @Mandatory,  
  @Multiple,  
  @PropertyID,  
  @FieldName,  
  @Parameters,
  @OverrideLocked,
  @CssClass
 )  
  
 Set @Err = @@Error  
 Set @FormFieldID = scope_identity()  
  
 IF @Log = 1   
 BEGIN   
  INSERT INTO [Log].[AuditLog] ( UserId, TableName, Type, Data, Created)   
  SELECT @cUserid,'FormField',0,  
  ( SELECT * FROM [form].[FormField]   
   WHERE  
   [FormFieldID] = @FormFieldID     FOR XML AUTO) as data,  
    getdate()   
  END  
  
 RETURN @Err  
END

GO
