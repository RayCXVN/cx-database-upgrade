SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[at].[prc_LT_Bulk_ins]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [at].[prc_LT_Bulk_ins] AS' 
END
GO
ALTER PROCEDURE [at].[prc_LT_Bulk_ins]
(
	@LanguageID int,
	@BulkID int,
	@Name varchar(100),
	@Description varchar(500),
	@ToolTip ntext,
	@cUserid int,
	@Log smallint = 1
)
AS
BEGIN
	SET NOCOUNT ON
	DECLARE @Err Int

	INSERT INTO [at].[LT_Bulk]
	(
		[LanguageID],
		[BulkID],
		[Name],
		[Description],
		[ToolTip]
	)
	VALUES
	(
		@LanguageID,
		@BulkID,
		@Name,
		@Description,
		@ToolTip
	)

	Set @Err = @@Error

	IF @Log = 1 
	BEGIN 
		INSERT INTO [Log].[AuditLog] ( UserId, TableName, Type, Data, Created) 
		SELECT @cUserid,'LT_Bulk',0,
		( SELECT * FROM [at].[LT_Bulk] 
			WHERE
			[LanguageID] = @LanguageID AND
			[BulkID] = @BulkID				 FOR XML AUTO) as data,
				getdate() 
	 END

	RETURN @Err
END



GO
