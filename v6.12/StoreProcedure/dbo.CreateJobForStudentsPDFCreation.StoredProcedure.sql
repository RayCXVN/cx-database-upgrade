SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[CreateJobForStudentsPDFCreation]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [dbo].[CreateJobForStudentsPDFCreation] AS' 
END
GO
/* 
	2017-05-19 Steve: Change text: Vokal --> Engage, SiteID 3 --> 5
*/
ALTER PROCEDURE [dbo].[CreateJobForStudentsPDFCreation] (
       @UserID as int,
       @OwnerID as int,
       @UserIDs as nvarchar(max), 
       @hdid_school as int,
       @Username varchar(100),
       @Password varchar(100),
       @Server as nvarchar(50)=@@servername,
       @Database as nvarchar(50),
       @PropertyIdStoreFile as int = 23,
       @PropertyIdBirthDay as int = 8,
       @LanguageId as int = 0,
       @SiteID as int = 5

)

AS
BEGIN

       declare @jobid int, @TextItems as nvarchar(max)
       
	   --Add logic get language from department base on hdid  
	   if ISNULL(@LanguageId, 0) = 0 
			begin
				SELECT @LanguageId=d.LanguageID from org.H_D h
				JOIN org.Department d ON d.DepartmentID=h.DepartmentID
				WHERE h.HDID=@hdid_school
			end
       --Initialization
       select  @TextItems = ''
       set @PropertyIdStoreFile = (select [Value] from app.SiteParameter where [key]='Platform.NewSchoolYear.ZipSLGProp' AND SiteID = @SiteID)
       
       --Create Job
       insert into job.Job (JobTypeID, JobStatusID, OwnerID, UserID, Name, Priority, [Option], StartDate, [Description])
       select 27,3, @OwnerID, @UserID, 'Generation of PDF file for Students (ENGAGE)', 1, 0, getdate(), 'Engage - produsering av zip-fil med resultpdfs for elever som slutter'
       
       set @JobId = scope_identity()
       
       --Create JobParameters
       insert into job.JobParameter (JobID, No, Name, Value, Type)
       select @JobID, 1, 'Server', @Server, 0
       union all
       select @JobID, 2, 'Database', @Database, 0
       union all
       select @JobID, 3, 'Username', ISNULL(@Username,''), 0
       union all
       select @JobID, 4, 'Password', ISNULL(@Password,''), 0
       union all
       select @JobID, 5, 'OwnerID', cast(@OwnerID as nvarchar(20)), 0
       union all
       select @JobID, 6, 'UserIDs', ISNULL(@UserIDs,''), 0
       union all
       select @JobID, 7, 'TextItems', ISNULL(@TextItems,''), 0
       union all
       select @JobID, 8, 'PropertyID', cast(@PropertyIdStoreFile as nvarchar(20)), 0
       union all
       select @JobID, 9, 'Description', 'Samling av alle kartlegginger og vurderinger registrert eleven', 0
       union all
       select @JobID, 10, 'TargetHDID', cast(@hdid_school as nvarchar(20)), 0
       union all
       select @JobID, 11, 'HDID', cast(@hdid_school as nvarchar(20)), 0
       union all
       select @JobID, 12, 'PropIdBirthDay', cast(@PropertyIdBirthDay as nvarchar(20)), 0
       union all
       select @JobID, 13, 'LanguageId', cast(@LanguageId as nvarchar(50)), 0
       
       --Change status on Job
       update job.Job set JobStatusId = 0 where JobId = @JobId
           
END

GO
