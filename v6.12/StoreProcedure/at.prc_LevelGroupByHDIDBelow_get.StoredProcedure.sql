SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[at].[prc_LevelGroupByHDIDBelow_get]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [at].[prc_LevelGroupByHDIDBelow_get] AS' 
END
GO



-- [at].[prc_LevelGroupByHDID_get] 16,15
ALTER PROCEDURE [at].[prc_LevelGroupByHDIDBelow_get]
(
	@ActivityID int,
	@HDID int
)
AS
BEGIN
	SET NOCOUNT ON
	DECLARE @Err Int
	
	SELECT
	[LevelGroupID],
	[ActivityID],
	[Tag],
	[No],
	isNull([CustomerID],0) 'CustomerID',
	ISNULL([Departmentid],0) 'DepartmentID',
	ISNULL([RoleID],0) 'RoleID'
	FROM [at].[LevelGroup]
	WHERE
	[ActivityID] = @ActivityID
	and ( departmentid IN 
	
	(
	  Select departmentid from org.H_D where path like '%\' + cast(@HDID as nvarchar(16)) + '\%'
	  
	) or ISNULL([Departmentid],0) = 0
	)
	ORDER BY [No]


	Set @Err = @@Error

	RETURN @Err
END



GO
