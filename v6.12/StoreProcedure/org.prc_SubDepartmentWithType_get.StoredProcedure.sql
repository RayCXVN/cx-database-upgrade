SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[org].[prc_SubDepartmentWithType_get]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [org].[prc_SubDepartmentWithType_get] AS' 
END
GO
/*  
	2016-10-10 Ray: Remove column Status from org.User, org.Department, RDB.dbo.Result
	2017-01-10 Sarah: Merge script change from release
	2017-01-13 Sarah: Show count number for each department
    2017-03-13 Ray: Hide private kindergartens

	exec [org].[prc_SubDepartmentWithType_get] @HDID=1, @DepartmentTypeID=36,@GetCurrentHDSum=0,@CheckBatchInSub=1,@SurveyID=326,@PROP_OwnerDepartmentID=53,@DT_Owner=39,@DeniedDTIDList='37', @CurrentHDID=1
*/

ALTER PROCEDURE [org].[prc_SubDepartmentWithType_get]
(   @HDID                       INT,
    @DepartmentTypeID           INT = 0,
    @GetCurrentHDSum            BIT = 0,
    @CheckBatchInSub            BIT = 0,
    @SurveyID                   INT = 0,
    @PROP_OwnerDepartmentID     INT = 0,
    @DT_Owner                   INT = 0,
	@DeniedDTIDList				VARCHAR(MAX) = '',
	@CurrentHDID				INT = 1
) AS
BEGIN
    SET NOCOUNT ON

	DECLARE @EntityStatusID_Active INT
    SELECT @EntityStatusID_Active = [EntityStatusID] FROM [EntityStatus] WHERE [CodeName] = 'Active'

    CREATE TABLE #DeniedDTIDTable ([DepartmentTypeID] int)
    INSERT INTO #DeniedDTIDTable SELECT [Value] FROM dbo.funcListToTableInt(@DeniedDTIDList, ',')

    CREATE TABLE #SubHD (HDID INT, DepartmentID INT, [HasChildren] INT, [IsOwner] BIT, [HasBatch] BIT, AllowAppear BIT)
    DECLARE @ReportServer NVARCHAR(64) = @@servername, @ReportDB NVARCHAR(64) = DB_NAME(), @sqlCommand NVARCHAR(MAX) = ''
	DECLARE @LinkedDB NVARCHAR(MAX)

    IF @GetCurrentHDSum = 1
    BEGIN
        INSERT INTO #SubHD ([HDID], [DepartmentID], [HasChildren], [IsOwner], [HasBatch])
        SELECT hd.[HDID], hd.[DepartmentID], 0 AS [HasChildren], (CASE WHEN dtd.[DepartmentTypeID] IS NOT NULL THEN 1 ELSE 0 END) AS [IsOwner], 0 AS [HasBatch]
        FROM [org].[H_D] hd LEFT JOIN [org].[DT_D] dtd ON hd.[DepartmentID] = dtd.[DepartmentID] AND dtd.[DepartmentTypeID] = @DT_Owner
        WHERE hd.[HDID] = @HDID AND hd.[Deleted] = 0
    END
    ELSE
    BEGIN
        -- If given HDID is an Owner department
        IF EXISTS (SELECT 1 FROM [org].[H_D] hd JOIN [org].[DT_D] dtd ON hd.[DepartmentID] = dtd.[DepartmentID] AND dtd.[DepartmentTypeID] = @DT_Owner AND hd.HDID = @HDID)
        BEGIN
            INSERT INTO #SubHD ([HDID], [DepartmentID], [HasChildren], [IsOwner], [HasBatch], AllowAppear)
            SELECT hd.[HDID], d.[DepartmentID], 0, 
					CASE WHEN dtd.[DepartmentTypeID] IS NOT NULL THEN 1 ELSE 0 END AS [IsOwner],  
					CASE WHEN b.DepartmentID IS NOT NULL THEN 1 ELSE 0 END AS [HasBatch],
					1 AS AllowAppear
            FROM [org].[Department] d 
				JOIN [prop].[PropValue] pv ON d.[DepartmentID] = pv.[ItemID] AND pv.[PropertyID] = @PROP_OwnerDepartmentID AND d.EntityStatusID = @EntityStatusID_Active AND d.Deleted IS NULL
				JOIN [org].[H_D] phd ON phd.[HDID] = @CurrentHDID AND phd.[HDID] = @HDID AND pv.[Value] = CAST(phd.[DepartmentID] AS NVARCHAR(32))
				JOIN [org].[H_D] hd ON d.[DepartmentID] = hd.[DepartmentID] AND hd.[Deleted] = 0
				LEFT JOIN [org].[DT_D] dtd ON hd.[DepartmentID] = dtd.[DepartmentID] AND dtd.[DepartmentTypeID] = @DT_Owner
				LEFT OUTER JOIN (
					SELECT b.DepartmentID 
					FROM at.Batch b 
					WHERE b.SurveyID = @SurveyID AND b.[Status] > 0
					GROUP BY b.DepartmentID
				) b ON b.DepartmentID = d.DepartmentID
        END
		
        -- Get direct children departments
        INSERT INTO #SubHD ([HDID], [DepartmentID], [HasChildren], [IsOwner], [HasBatch])
        SELECT hd.[HDID], hd.[DepartmentID], 0 AS [HasChildren], (CASE WHEN dtd.[DepartmentTypeID] IS NOT NULL THEN 1 ELSE 0 END) AS [IsOwner], 0 AS [HasBatch]
        FROM [org].[H_D] hd 
			JOIN org.Department d ON hd.DepartmentID = d.DepartmentID AND d.EntityStatusID = @EntityStatusID_Active AND d.Deleted IS NULL
			LEFT JOIN [org].[DT_D] dtd ON hd.[DepartmentID] = dtd.[DepartmentID] AND dtd.[DepartmentTypeID] = @DT_Owner
        WHERE hd.[ParentID] = @HDID AND hd.[Deleted] = 0
          AND NOT EXISTS (SELECT 1 FROM #SubHD shd WHERE shd.[HDID] = hd.[HDID])
		  AND NOT EXISTS (SELECT 1 FROM [org].[DT_D] dtd2 JOIN #DeniedDTIDTable ddt ON dtd2.[DepartmentTypeID] = ddt.[DepartmentTypeID] AND dtd2.[DepartmentID] = hd.[DepartmentID])
    END
    
    IF @CheckBatchInSub = 1
    BEGIN
        UPDATE shd SET [shd].[HasBatch] = 1
        FROM #SubHD shd
        WHERE EXISTS (SELECT 1 FROM [at].[Batch] b JOIN [org].[H_D] hd ON b.[DepartmentID] = hd.[DepartmentID] AND b.[Status] > 0
                                                    AND hd.[Path] LIKE '%\' + CONVERT(NVARCHAR(16), shd.HDID) + '\%' AND hd.[Deleted] = 0
                                                    AND (b.[SurveyID] = @SurveyID)
                                                    AND NOT EXISTS (SELECT 1 FROM [org].[DT_D] dtd JOIN #DeniedDTIDTable ddt ON dtd.[DepartmentTypeID] = ddt.[DepartmentTypeID]
                                                                              AND dtd.[DepartmentID] = b.[DepartmentID]))
        
        UPDATE shd SET [shd].[HasBatch] = 1
        FROM #SubHD shd
        WHERE shd.[HasBatch] = 0 AND shd.[IsOwner] = 1
          AND EXISTS (SELECT 1 
					  FROM [at].[Batch] b 
							JOIN [org].[Department] d ON b.[DepartmentID] = d.[DepartmentID] AND d.EntityStatusID = @EntityStatusID_Active AND d.Deleted IS NULL
							JOIN [prop].[PropValue] pv ON pv.[PropertyID] = @PROP_OwnerDepartmentID AND pv.[ItemID] = d.[DepartmentID] AND b.[Status] > 0
								AND pv.[Value] = CAST(shd.[DepartmentID] AS NVARCHAR(32)) AND (b.[SurveyID] = @SurveyID))

        DELETE FROM #SubHD WHERE [HasBatch] = 0
    END
	
    UPDATE #SubHD SET [HasChildren] = 1 WHERE [IsOwner] = 1 AND HDID = @CurrentHDID
    
	IF @CheckBatchInSub = 1
    BEGIN
		UPDATE shd SET [HasChildren] = 1 FROM #SubHD shd 
		WHERE shd.[HasChildren] = 0
		  AND EXISTS (SELECT 1 
					  FROM  org.Department d 
							JOIN [org].[H_D] hd ON hd.[Path] LIKE '%\' + CONVERT(NVARCHAR(16), shd.HDID) + '\%' AND hd.[Deleted] = 0 
										AND d.EntityStatusID = @EntityStatusID_Active AND d.Deleted IS NULL AND hd.DepartmentID <> shd.DepartmentID
							JOIN at.Batch b ON d.DepartmentID = b.DepartmentID AND b.SurveyID = @SurveyID AND b.[Status] > 0
					)
		
	END
	ELSE 
	BEGIN
		UPDATE shd SET [HasChildren] = 1 FROM #SubHD shd 
		WHERE shd.[HasChildren] = 0
		  AND EXISTS (SELECT 1 
					  FROM [org].[H_D] hd 
							JOIN org.Department d ON hd.DepartmentID = d.DepartmentID AND d.EntityStatusID = @EntityStatusID_Active AND d.Deleted IS NULL
								AND hd.[ParentID] = shd.[HDID] AND hd.[Deleted] = 0
					)
	END

	SELECT @ReportDB = S.ReportDB, @ReportServer = s.ReportServer FROM at.Survey S WHERE S.SurveyID = @SurveyID
	IF @ReportDB=DB_NAME() AND @ReportServer=@@servername
	BEGIN
		SET @LinkedDB=' '
	END
	ELSE
	BEGIN
		SET @LinkedDB= '['+ @ReportServer + '].['+ @ReportDB + '].'
	END

	SET @sqlCommand ='
	;WITH ChildDepartment AS
	(
		SELECT S.HDID ParentHDID, S.DepartmentID ParentDepartmentID, s.HasChildren, pd.Name ParentName, d.DepartmentID
		FROM #SubHD S 
			JOIN org.H_D hd ON hd.[Path] LIKE ''%\''+ CAST(S.HDID AS VARCHAR(128)) + ''\%'' AND hd.Deleted = 0
			JOIN org.Department d ON hd.DepartmentID = d.DepartmentID AND d.EntityStatusID = '+CAST(@EntityStatusID_Active AS VARCHAR(128))+' AND d.Deleted IS NULL
			JOIN org.Department pd ON pd.DepartmentID = S.DepartmentID AND pd.EntityStatusID = ' + CAST(@EntityStatusID_Active AS VARCHAR(128)) +' AND pd.Deleted IS NULL
			 AND (' + CAST(@DepartmentTypeID AS VARCHAR(128)) + ' = 0
				 OR EXISTS (SELECT 1 FROM [org].[DT_D] dtd WHERE dtd.[DepartmentID] = hd.[DepartmentID] AND dtd.[DepartmentTypeID] = ' + CAST(@DepartmentTypeID AS VARCHAR(128)) + ')
				 )
			 AND (S.AllowAppear = 1 OR NOT EXISTS (SELECT 1 FROM [org].[DT_D] dtd2 JOIN #DeniedDTIDTable ddt ON dtd2.[DepartmentTypeID] = ddt.[DepartmentTypeID] AND dtd2.[DepartmentID] = hd.[DepartmentID]))
		UNION ALL
		SELECT S.HDID, S.DepartmentID ParentDepartmentID, s.HasChildren, pd.Name ParentName, cd.DepartmentID
        FROM #SubHD S
			JOIN [prop].[PropValue] pv ON pv.[Value] = CAST(s.[DepartmentID] AS NVARCHAR(32)) AND pv.[PropertyID] = '+CAST(@PROP_OwnerDepartmentID AS VARCHAR(128))+' 
				AND S.HDID = '+ CAST(@CurrentHDID AS VARCHAR(128)) +'
			JOIN org.Department cd ON pv.ItemID = cd.DepartmentID AND cd.EntityStatusID = '+CAST(@EntityStatusID_Active AS VARCHAR(128))+' AND cd.Deleted IS NULL
			JOIN org.Department pd ON s.DepartmentID = pd.DepartmentID
			LEFT JOIN [org].[DT_D] dtd ON cd.[DepartmentID] = dtd.[DepartmentID] AND dtd.[DepartmentTypeID] = '+CAST(@DT_Owner AS VARCHAR(128))+'
			LEFT OUTER JOIN (
				SELECT b.DepartmentID
				FROM at.Batch b 
				WHERE b.SurveyID = '+CAST(@SurveyID AS VARCHAR(128))+' AND b.[Status] > 0
				GROUP BY b.DepartmentID
			) b ON b.DepartmentID = cd.DepartmentID
		WHERE ('+CAST(@CheckBatchInSub AS VARCHAR(128))+' = 0 OR b.DepartmentID IS NOT NULL)
	)
	SELECT chd.ParentHDID HDID, chd.ParentDepartmentID DepartmentID, chd.ParentName [Name], chd.HasChildren, '+CAST(@SurveyID AS VARCHAR(128))+' SurveyID, 
		CONVERT(FLOAT,COUNT(r.[ResultID])) AS Invited, CONVERT(FLOAT,COUNT(r.EndDate)) as Finished,
		CASE WHEN CONVERT(FLOAT,COUNT(r.[ResultID])) > 0 THEN CONVERT(FLOAT,COUNT(r.EndDate))/CONVERT(FLOAT,COUNT(r.[ResultID]))*100 ELSE 0 END [PercentFinished]
    FROM ChildDepartment chd
		LEFT JOIN [at].[Batch] b ON  b.DepartmentID = chd.DepartmentID AND b.[Status] > 0 AND b.[SurveyID] = '+CAST(@SurveyID AS VARCHAR(128)) + '
		LEFT JOIN '+@LinkedDB+'[dbo].[Result] r ON r.[BatchID] = b.[BatchID] AND r.[SurveyID] = b.[SurveyID] AND r.EntityStatusID = ' + CAST(@EntityStatusID_Active AS VARCHAR(128)) +' AND r.Deleted IS NULL 
	GROUP BY chd.ParentHDID, chd.ParentDepartmentID, chd.ParentName, chd.HasChildren
	ORDER BY chd.ParentName
	'
	EXEC sp_executesql @sqlCommand
	
    DROP TABLE #DeniedDTIDTable
    DROP TABLE #SubHD
END

GO
