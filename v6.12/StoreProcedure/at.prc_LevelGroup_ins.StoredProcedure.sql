SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[at].[prc_LevelGroup_ins]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [at].[prc_LevelGroup_ins] AS' 
END
GO

ALTER PROCEDURE [at].[prc_LevelGroup_ins]
(
	@LevelGroupID int = null output,
	@ActivityID int,
	@Tag nvarchar(128),
	@No smallint,
	@CustomerID int = null,
	@DepartmentID int = null,
	@RoleID int = null,
	@cUserid int,
	@Log smallint = 1
)
AS
BEGIN
	SET NOCOUNT ON
	DECLARE @Err Int

	INSERT INTO [at].[LevelGroup]
	(
		[ActivityID],
		[Tag],
		[No],
		[CustomerID],
		[DepartmentID],
		[RoleID]
	)
	VALUES
	(
		@ActivityID,
		@Tag,
		@No,
		@CustomerID,
		@DepartmentID,
		@RoleID
	)

	Set @Err = @@Error
	Set @LevelGroupID = scope_identity()

	IF @Log = 1 
	BEGIN 
		INSERT INTO [Log].[AuditLog] ( UserId, TableName, Type, Data, Created) 
		SELECT @cUserid,'LevelGroup',0,
		( SELECT * FROM [at].[LevelGroup] 
			WHERE
			[LevelGroupID] = @LevelGroupID				 FOR XML AUTO) as data,
				getdate() 
	 END

	RETURN @Err
END


GO
