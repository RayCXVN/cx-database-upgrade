SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[sec].[prc_AccessControlList_get]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [sec].[prc_AccessControlList_get] AS' 
END
GO
ALTER PROCEDURE [sec].[prc_AccessControlList_get]
AS
BEGIN
SET NOCOUNT ON
DECLARE @Err int
SELECT
    [AccessControlListID],
    [GroupAccessRuleID],
    [AccessGroupID],
    [No],
    [RuleType]
FROM [sec].[AccessControlList]
SET @Err = @@Error
RETURN @Err
END

GO
