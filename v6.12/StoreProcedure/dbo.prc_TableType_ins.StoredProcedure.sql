SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[prc_TableType_ins]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [dbo].[prc_TableType_ins] AS' 
END
GO


ALTER PROCEDURE [dbo].[prc_TableType_ins]
(
	@TableTypeID smallint = null output,
	@Name nvarchar(128),
	@Schema nvarchar(8),
	@cUserid int,
	@Log smallint = 1
)
AS
BEGIN
	SET NOCOUNT ON
	DECLARE @Err Int

	INSERT INTO [dbo].[TableType]
	(
		[Name],
		[Schema]
	)
	VALUES
	(
		@Name,
		@Schema
	)

	Set @Err = @@Error
	Set @TableTypeID = scope_identity()

	IF @Log = 1 
	BEGIN 
		INSERT INTO [Log].[AuditLog] ( UserId, TableName, Type, Data, Created) 
		SELECT @cUserid,'TableType',0,
		( SELECT * FROM [dbo].[TableType] 
			WHERE
			[TableTypeID] = @TableTypeID				 FOR XML AUTO) as data,
				getdate() 
	 END

	RETURN @Err
END



GO
