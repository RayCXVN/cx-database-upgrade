SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[rep].[prc_Selection_Survey_Batch_ins]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [rep].[prc_Selection_Survey_Batch_ins] AS' 
END
GO

ALTER PROCEDURE [rep].[prc_Selection_Survey_Batch_ins]
(
	@S_SurveyID int,
	@BatchID int,
	@cUserid int,
	@Log smallint = 1
)
AS
BEGIN
	SET NOCOUNT ON
	DECLARE @Err Int

	INSERT INTO [rep].[Selection_Survey_Batch]
	(
		[S_SurveyID],
		[BatchID]
	)
	VALUES
	(
		@S_SurveyID,
		@BatchID
	)

	Set @Err = @@Error

	IF @Log = 1 
	BEGIN 
		INSERT INTO [Log].[AuditLog] ( UserId, TableName, Type, Data, Created) 
		SELECT @cUserid,'Selection_Survey_Batch',0,
		( SELECT * FROM [rep].[Selection_Survey_Batch] 
			WHERE
			[S_SurveyID] = @S_SurveyID AND
			[BatchID] = @BatchID				 FOR XML AUTO) as data,
				getdate() 
	 END

	RETURN @Err
END


GO
