SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[org].[prc_LT_DepartmentType_ins]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [org].[prc_LT_DepartmentType_ins] AS' 
END
GO

ALTER PROCEDURE [org].[prc_LT_DepartmentType_ins]
(
	@LanguageID int,
	@DepartmentTypeID int,
	@Name nvarchar(256),
	@Description nvarchar(max),
	@cUserid int,
	@Log smallint = 1
)
AS
BEGIN
	SET NOCOUNT ON
	DECLARE @Err Int

	INSERT INTO [org].[LT_DepartmentType]
	(
		[LanguageID],
		[DepartmentTypeID],
		[Name],
		[Description]
	)
	VALUES
	(
		@LanguageID,
		@DepartmentTypeID,
		@Name,
		@Description
	)

	Set @Err = @@Error

	IF @Log = 1 
	BEGIN 
		INSERT INTO [Log].[AuditLog] ( UserId, TableName, Type, Data, Created) 
		SELECT @cUserid,'LT_DepartmentType',0,
		( SELECT * FROM [org].[LT_DepartmentType] 
			WHERE
			[LanguageID] = @LanguageID AND
			[DepartmentTypeID] = @DepartmentTypeID				 FOR XML AUTO) as data,
				getdate() 
	 END

	RETURN @Err
END


GO
