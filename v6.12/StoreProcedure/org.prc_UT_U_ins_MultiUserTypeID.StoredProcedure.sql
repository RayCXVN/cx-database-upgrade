SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[org].[prc_UT_U_ins_MultiUserTypeID]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [org].[prc_UT_U_ins_MultiUserTypeID] AS' 
END
GO
ALTER PROCEDURE [org].[prc_UT_U_ins_MultiUserTypeID]
(
	@UserTypeIDs nvarchar(max),
	@UserID int,
	@cUserid int,
	@Log smallint = 1
)
AS
BEGIN
	SET NOCOUNT ON
	DECLARE @Err Int

	INSERT INTO [org].[UT_U] (		[UserTypeID],		[UserID]	)
	SELECT value, @UserID  from dbo.funcListToTableInt(@UserTypeIDs,',')

	Set @Err = @@Error

	IF @Log = 1 
	BEGIN 
		INSERT INTO [Log].[AuditLog] ( UserId, TableName, Type, Data, Created) 
		SELECT @cUserid,'UT_U',0,
		( SELECT * FROM [org].[UT_U] 
			WHERE
			[UserTypeID] IN (SELECT value  from dbo.funcListToTableInt(@UserTypeIDs,',')) AND
			[UserID] = @UserID				 FOR XML AUTO) as data,
				getdate() 
	 END

	RETURN @Err
END

GO
