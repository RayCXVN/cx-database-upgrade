SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[job].[prc_JobType_upd]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [job].[prc_JobType_upd] AS' 
END
GO

ALTER PROCEDURE [job].[prc_JobType_upd]
(
	@JobTypeID smallint,
	@No smallint,
	@cUserid int,
	@Log smallint = 1
)
AS
BEGIN
	SET NOCOUNT ON
	DECLARE @Err Int

	UPDATE [job].[JobType]
	SET
		[No] = @No
	WHERE
		[JobTypeID] = @JobTypeID

	IF @Log = 1 
	BEGIN 
		INSERT INTO [Log].[AuditLog] ( UserId, TableName, Type, Data, Created) 
		SELECT @cUserid,'JobType',1,
		( SELECT * FROM [job].[JobType] 
			WHERE
			[JobTypeID] = @JobTypeID			 FOR XML AUTO) as data,
			getdate()
	END

	Set @Err = @@Error

	RETURN @Err
END


GO
