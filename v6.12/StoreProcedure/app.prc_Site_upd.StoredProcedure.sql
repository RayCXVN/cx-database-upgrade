SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[app].[prc_Site_upd]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [app].[prc_Site_upd] AS' 
END
GO
ALTER PROCEDURE [app].[prc_Site_upd]  
 @SiteID int = null output,  
 @OwnerID int,  
 @CodeName nvarchar(256),  
 @UseNativeLogon bit,  
 @ThemeID int,  
 @IsClosed bit,  
 @DefaultMenuID int,  
 @DefaultHierarchyID int,  
 @UseMultipleMenus bit,  
 @DefaultChartTemplate nvarchar(256),  
 @UseCustomer bit,  
 @UsePasswordRecovery bit,  
 @MaintenanceMode bit,  
 @UseRememberMe bit,
 @cUserid int,  
 @Log smallint = 1,
 @UseReturnURL bit =0 ,
 @UseSMS bit =0,
 @UseMemoryCache			bit	,			
 @UseDistributedCache		bit	,			
 @DistributedCacheName      nvarchar(64),
 @CacheExpiredAfter		    int	,
 @MailHeaderInfoXCompany nvarchar(64)='',
 @MailHeaderInfoMessageID nvarchar(64) ='',
 @LanguageIDWhenNotAuthenticated Int =0,
 @LogoutURL nvarchar(max) ='',
 @TimeOutLogoutURL nvarchar(max) ='',
 @Logo nvarchar(max) = '', 
 @CssVariables nvarchar(max) = '',
 @Favicon nvarchar(max) = ''
AS  
BEGIN  
 SET NOCOUNT ON;  
 DECLARE @Err Int  

    UPDATE [app].[Site]  
    SET     [CodeName] =  @CodeName  
           ,[UseNativeLogon] = @UseNativeLogon  
           ,[ThemeID] = @ThemeID  
           ,[IsClosed] = @IsClosed  
           ,[DefaultMenuID] = @DefaultMenuID  
           ,[DefaultHierarchyID] = @DefaultHierarchyID  
           ,[UseMultipleMenus] = @UseMultipleMenus  
           ,[DefaultChartTemplate] = @DefaultChartTemplate  
           ,[UseCustomer] = @UseCustomer  
           ,[UsePasswordRecovery] = @UsePasswordRecovery  
           ,[MaintenanceMode] = @MaintenanceMode 
           ,[UseRememberMe] = @UseRememberMe
           ,[UseReturnURL]=@UseReturnURL
           ,[UseSMS]=@UseSMS
		   ,UseMemoryCache		=@UseMemoryCache		
		   ,UseDistributedCache	=@UseDistributedCache	
		   ,DistributedCacheName=@DistributedCacheName   
		   ,CacheExpiredAfter	=@CacheExpiredAfter
		   ,MailHeaderInfoXCompany =@MailHeaderInfoXCompany
		   ,MailHeaderInfoMessageID=@MailHeaderInfoMessageID
		   ,LanguageIDWhenNotAuthenticated= @LanguageIDWhenNotAuthenticated
		   ,LogoutURL			=@LogoutURL			
		   ,TimeOutLogoutURL	 =@TimeOutLogoutURL
		   ,Logo = @Logo
		   ,CssVariables = @CssVariables
		   ,Favicon = @Favicon
            WHERE   
           [OwnerID] = @OwnerID  
           AND [SiteID] = @SiteID  
    Set @Err = @@Error  
    Set @SiteID = scope_identity()  
 IF @Log = 1   
 BEGIN   
  INSERT INTO [Log].[AuditLog] ( UserId, TableName, Type, Data, Created)   
  SELECT @cUserid,'Site',1,  
  ( SELECT * FROM [app].[Site]  
   WHERE  
   [SiteID] = @SiteID     FOR XML AUTO) as data,  
    getdate()   
 END  

 RETURN @Err         
END 

GO
