SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[rep].[prc_Selection_ins]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [rep].[prc_Selection_ins] AS' 
END
GO




ALTER PROCEDURE [rep].[prc_Selection_ins]
(
	@SelectionID int = null output,
	@SelectionGroupID int,
	@Active bit,
	@CustomName bit,
	@Name nvarchar(256),
	@ActivityID INT=NULL,
	@Status smallint,
	@StartDate DATETIME=NULL,
	@EndDate DATETIME=NULL,
	@LevelGroupID int = null,
    @No smallint = 0,
    @HierarchyID INT=NULL,
	@cUserid int,
	@Log smallint = 1
)
AS
BEGIN
	SET NOCOUNT ON
	DECLARE @Err Int

	INSERT INTO [rep].[Selection]
	(
		[SelectionGroupID],
		[Active],
		[CustomName],
		[Name],
		[ActivityID],
		[Status],
		[StartDate],
		[EndDate],
		[LevelGroupID],
		[No],
		[HierarchyID]
	)
	VALUES
	(
		@SelectionGroupID,
		@Active,
		@CustomName,
		@Name,
		@ActivityID,
		@Status,
		@StartDate,
		@EndDate,
		@LevelGroupID,
        @No,
        @HierarchyID
	)

	Set @Err = @@Error
	Set @SelectionID = scope_identity()

	IF @Log = 1 
	BEGIN 
		INSERT INTO [Log].[AuditLog] ( UserId, TableName, Type, Data, Created) 
		SELECT @cUserid,'Selection',0,
		( SELECT * FROM [rep].[Selection] 
			WHERE
			[SelectionID] = @SelectionID				 FOR XML AUTO) as data,
				getdate() 
	 END

	RETURN @Err
END



GO
