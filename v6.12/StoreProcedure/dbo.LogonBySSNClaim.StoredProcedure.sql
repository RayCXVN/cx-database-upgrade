SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[LogonBySSNClaim]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [dbo].[LogonBySSNClaim] AS' 
END
GO
ALTER PROCEDURE [dbo].[LogonBySSNClaim]
(
  @OwnerId int,
  @SSN nvarchar(64),
  @ErrNo int		OUTPUT
 )
AS
BEGIN  
DECLARE @ret as integer
DECLARE @ActiveEntityStatusID INT = (SELECT EntityStatusID FROM EntityStatus WHERE CodeName='Active')
SELECT 	
	u.UserName,
	u.DepartmentID,
	u.UserID,
	u.OwnerId,
	u.FirstName,
	u.LastName,
	d.Name AS DepartmentName,
	u.LanguageID,
	d.CustomerID
	FROM 	
	[org].[User] u		
	INNER JOIN org.Department d ON u.DepartmentID = d.DepartmentID
	WHERE		
	u.EntityStatusID = @ActiveEntityStatusID AND u.Deleted IS NULL
	AND u.[Ownerid] = @OwnerId		
	and u.SSN= @SSN
	ORDER BY u.Created 

SET @ret = @@ROWCOUNT
IF @ret < 1 
BEGIN
	SET @ErrNo = 1 /* Ingen brukere ble funnet */
END
ELSE
BEGIN	
	IF @ret > 1 
	BEGIN
		SET @ErrNo = 2 /* Flere brukere med samme propertyvalue ble funnet! */
	END
	ELSE
	BEGIN	
		SET @ErrNo = 0
	END
END

END  

GO
