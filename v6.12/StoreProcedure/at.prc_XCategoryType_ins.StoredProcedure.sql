SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[at].[prc_XCategoryType_ins]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [at].[prc_XCategoryType_ins] AS' 
END
GO
/*
	2017-07-26 Steve: Change the length of column ExtID in all related tables from xAPI format
*/
ALTER PROCEDURE [at].[prc_XCategoryType_ins] 
(
	@XCategoryTypeID INT = NULL OUTPUT
	,@OwnerID INT
	,@ExtID NVARCHAR(256)
	,@No SMALLINT
	,@cUserid INT
	,@Log SMALLINT = 1
	)
AS
BEGIN
	SET NOCOUNT ON

	DECLARE @Err INT

	INSERT INTO [at].[XCategoryType] (
		[OwnerID]
		,[ExtID]
		,[No]
		)
	VALUES (
		@OwnerID
		,@ExtID
		,@No
		)

	SET @Err = @@Error
	SET @XCategoryTypeID = SCOPE_IDENTITY()

	IF @Log = 1
	BEGIN
		INSERT INTO [Log].[AuditLog] (
			UserId
			,TableName
			,Type
			,Data
			,Created
			)
		SELECT @cUserid
			,'XCategoryType'
			,0
			,(
				SELECT *
				FROM [at].[XCategoryType]
				WHERE [XCTypeID] = @XCategoryTypeID
				FOR XML AUTO
				) AS data
			,GETDATE()
	END

	RETURN @Err
END

GO
