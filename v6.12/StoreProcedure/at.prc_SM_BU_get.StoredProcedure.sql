SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[at].[prc_SM_BU_get]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [at].[prc_SM_BU_get] AS' 
END
GO

ALTER PROCEDURE [at].[prc_SM_BU_get]
(
	@SentMailID int
)
AS
BEGIN
	SET NOCOUNT ON
	DECLARE @Err Int

	SELECT
	[SentMailID],
	[BatchID],
	[ResultID]
	FROM [at].[SM_BU]
	WHERE
	[SentMailID] = @SentMailID

	Set @Err = @@Error

	RETURN @Err
END


GO
