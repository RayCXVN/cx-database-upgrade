SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[at].[prc_Bulk_upd]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [at].[prc_Bulk_upd] AS' 
END
GO
ALTER PROCEDURE [at].[prc_Bulk_upd]
(
	@BulkID int,
	@BulkGroupID int,
	@No smallint,
	@Css varchar(50),
	@Icon varchar(50),
	@Tag varchar(50),
	@cUserid int,
	@Log smallint = 1
)
AS
BEGIN
	SET NOCOUNT ON
	DECLARE @Err Int

	UPDATE [at].[Bulk]
	SET		
		[BulkGroupID] = @BulkGroupID,
		[No] = @No,
		[Css] = @Css,
		[Icon] = @Icon,
		[Tag] = @Tag
	WHERE
		[BulkID] = @BulkID

	IF @Log = 1 
	BEGIN 
		INSERT INTO [Log].[AuditLog] ( UserId, TableName, Type, Data, Created) 
		SELECT @cUserid,'Bulk',1,
		( SELECT * FROM [at].[Bulk] 
			WHERE
			[BulkID] = @BulkID			 FOR XML AUTO) as data,
			getdate()
	END

	Set @Err = @@Error

	RETURN @Err
END



GO
