SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[at].[prc_LT_Role_ins]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [at].[prc_LT_Role_ins] AS' 
END
GO

ALTER PROCEDURE [at].[prc_LT_Role_ins]
(
	@LanguageID int,
	@RoleID int,
	@Name nvarchar(256),
	@Description nvarchar(max),
	@cUserid int,
	@Log smallint = 1
)
AS
BEGIN
	SET NOCOUNT ON
	DECLARE @Err Int

	INSERT INTO [at].[LT_Role]
	(
		[LanguageID],
		[RoleID],
		[Name],
		[Description]
	)
	VALUES
	(
		@LanguageID,
		@RoleID,
		@Name,
		@Description
	)

	Set @Err = @@Error

	IF @Log = 1 
	BEGIN 
		INSERT INTO [Log].[AuditLog] ( UserId, TableName, Type, Data, Created) 
		SELECT @cUserid,'LT_Role',0,
		( SELECT * FROM [at].[LT_Role] 
			WHERE
			[LanguageID] = @LanguageID AND
			[RoleID] = @RoleID				 FOR XML AUTO) as data,
				getdate() 
	 END

	RETURN @Err
END


GO
