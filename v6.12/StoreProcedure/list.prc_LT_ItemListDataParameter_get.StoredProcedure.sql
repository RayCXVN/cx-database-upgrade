SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[list].[prc_LT_ItemListDataParameter_get]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [list].[prc_LT_ItemListDataParameter_get] AS' 
END
GO
ALTER PROCEDURE [list].[prc_LT_ItemListDataParameter_get]
	@ItemListDataParameterID int
AS
BEGIN
	SET NOCOUNT ON;
	DECLARE @Err Int
	
    SELECT 
		[LanguageID],
		[ItemListDataParameterID],
		[Name],
		[Description]
	FROM [list].[LT_ItemListDataParameter]
	WHERE [ItemListDataParameterID] = @ItemListDataParameterID
	
	Set @Err = @@Error

	RETURN @Err
  
END

GO
