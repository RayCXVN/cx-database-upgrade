SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[job].[prc_Bubble_job_get]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [job].[prc_Bubble_job_get] AS' 
END
GO
ALTER PROC [job].[prc_Bubble_job_get]
AS
	 SELECT DISTINCT B.BubbleID,B.ActivityID,S.SurveyID, 
	       COALESCE(CASE WHEN S.reportDB = '' THEN NULL ELSE s.ReportDB END,
	         CASE WHEN O.reportDB = '' THEN NULL ELSE O.ReportDB END)   AS ReportDB,
	         COALESCE(CASE WHEN S.reportServer = '' THEN NULL ELSE S.reportServer END,
	         CASE WHEN O.reportServer = '' THEN NULL ELSE O.reportServer END)   AS reportServer,
            isnull(BS.LastProcessedDate,'1900-01-01') as LastProcessedDate
	FROM rep.Bubble B 
	JOIN org.[Owner] O ON O.ownerid = B.Ownerid
	JOIN at.Activity A ON B.ActivityID = A.ActivityID
	JOIN at.Survey S ON B.ActivityID = S.ActivityID
	JOIN rep.Bubble_SurveyLimit SL ON SL.surveyID = s.SurveyID and b.bubbleid = SL.bubbleid
	LEFT OUTER JOIN rep.Bubble_Status BS ON BS.SurveyID = S.Surveyid
	WHERE  (( 
              S.EndDate > getdate()-1 
              and ISNULL(BS.LastProcessedDate,'1900-01-01') < getdate() -0.95
              AND DATEPART(hh,getdate()) IN (1,2,3,4,5,6,7) 
            )
           OR BS.Reprocess = 1) 
           and b.status = 1
	 
	
	 

	 
 

GO
