SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[exp].[prc_ExportFiles_upd]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [exp].[prc_ExportFiles_upd] AS' 
END
GO

ALTER PROCEDURE [exp].[prc_ExportFiles_upd]
(
	@ExportFileID int,
	@UserID int,
	@MIMEType nvarchar(64),
	@Data varbinary,
	@EmailSendtTo nvarchar(128),
	@Description nvarchar(128),
	@Size bigint,
	@Filename nvarchar(128),
	@SurveyID INT=NULL,
	@cUserid int,
	@Log smallint = 1
)
AS
BEGIN
	SET NOCOUNT ON
	DECLARE @Err Int

	UPDATE [exp].[ExportFiles]
	SET
		[UserID] = @UserID,
		[MIMEType] = @MIMEType,
		[Data] = @Data,
		[EmailSendtTo] = @EmailSendtTo,
		[Description] = @Description,
		[Size] = @Size,
		[Filename] = @Filename,
		[SurveyID] = @SurveyID
	WHERE
		[ExportFileID] = @ExportFileID

	IF @Log = 1 
	BEGIN 
		INSERT INTO [Log].[AuditLog] ( UserId, TableName, Type, Data, Created) 
		SELECT @cUserid,'ExportFiles',1,
		( SELECT * FROM [exp].[ExportFiles] 
			WHERE
			[ExportFileID] = @ExportFileID			 FOR XML AUTO) as data,
			getdate()
	END

	Set @Err = @@Error

	RETURN @Err
END


GO
