SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[app].[prc_Theme_upd]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [app].[prc_Theme_upd] AS' 
END
GO

ALTER PROCEDURE [app].[prc_Theme_upd]
(
	@ThemeID int,
	@Name nvarchar(64),
	@StyleSheet nvarchar(256),
	@Parameters nvarchar(max),
	@cUserid int,
	@Log smallint = 1
)
AS
BEGIN
	SET NOCOUNT ON
	DECLARE @Err Int

	UPDATE [app].[Theme]
	SET
		[Name] = @Name,
		[StyleSheet] = @StyleSheet,
		[Parameters] = @Parameters
	WHERE
		[ThemeID] = @ThemeID

	IF @Log = 1 
	BEGIN 
		INSERT INTO [Log].[AuditLog] ( UserId, TableName, Type, Data, Created) 
		SELECT @cUserid,'Theme',1,
		( SELECT * FROM [app].[Theme] 
			WHERE
			[ThemeID] = @ThemeID			 FOR XML AUTO) as data,
			getdate()
	END

	Set @Err = @@Error

	RETURN @Err
END


GO
