SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[at].[prc_Q_C_upd]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [at].[prc_Q_C_upd] AS' 
END
GO
ALTER PROCEDURE [at].[prc_Q_C_upd]    
(    
 @QuestionID int,    
 @CategoryID int,    
 @Weight float,    
 @No smallint,    
 @Visible smallint,    
 @cUserid int,    
 @Log smallint = 1,
 @ItemID int = NULL
)    
AS    
BEGIN    
 SET NOCOUNT ON    
 DECLARE @Err Int    
    
 UPDATE [at].[Q_C]    
 SET    
  [QuestionID] = @QuestionID,    
  [CategoryID] = @CategoryID,    
  [Visible] =@Visible,  
  [Weight] = @Weight,    
  [No] = @No,
  [ItemID] = @ItemID
 WHERE    
  [QuestionID] = @QuestionID AND    
  [CategoryID] = @CategoryID    
    
 IF @Log = 1     
 BEGIN     
  INSERT INTO [Log].[AuditLog] ( UserId, TableName, Type, Data, Created)     
  SELECT @cUserid,'Q_C',1,    
  ( SELECT * FROM [at].[Q_C]     
   WHERE    
   [QuestionID] = @QuestionID AND    
   [CategoryID] = @CategoryID    FOR XML AUTO) as data,    
   getdate()    
 END    
    
 Set @Err = @@Error    
    
 RETURN @Err    
END    
        

GO
