SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[prc_ResultAnswer_MultiInsert]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [dbo].[prc_ResultAnswer_MultiInsert] AS' 
END
GO
----------------------------------------------------------------------------------------------------------------
-- Dev: Steve
-- Desc: Fix bug for adding constraint to dbo.Result to assure that Result.CustomerID match with their corresponding User.CustomerID
/*
    2016-10-10 Sarah:   Remove column Status on User, Department and Result tables
    2016-10-31 Sarah:   Hot fix: Get EntityStatus value on QDB and Set limit length of string which is used to update UserName
    2016-12-07 Johnny:  Update store procedure for dynamic SQL
    2017-07-05 Steve:   Add CustomerID (value of its User) when inserting a new result
    2017-09-19 Ray:     Get CustomerID by department instead of user and Refactor
*/
ALTER PROCEDURE [dbo].[prc_ResultAnswer_MultiInsert] 
(
    @SurveyID           INT,
    @BatchID            INT,
    @DepartmentID       INT,
    @LanguageID         INT,
    @ListUserID         VARCHAR(max),
    @UpdatedByUser      INT,
    @StatusTypeID       INT,
    @ValidFrom          datetime2(7) = NULL,
    @DueDate            datetime2(7) = NULL,
    @UserGroupID        INT = 0,
    @UserTypeID         INT = 0
)
AS
BEGIN
    DECLARE @ReportServer NVARCHAR(64), @ReportDB NVARCHAR(64), @Error int = 0, @ActivityID int
    DECLARE @sqlCommand NVARCHAR(max), @sqlParameters NVARCHAR(MAX), @ActiveEntityStatusID INT = 0, @EntityStatusReasonID INT = NULL
    
    SET @UserGroupID = ISNULL(@UserGroupID,0)
    SET @UserTypeID = ISNULL(@UserTypeID,0)

    SELECT @ReportServer = ReportServer, @ReportDB = ReportDB, @ActivityID = ActivityID FROM at.Survey
    WHERE  SurveyID = @SurveyId

    SELECT @ActiveEntityStatusID = EntityStatusID FROM dbo.EntityStatus WHERE CodeName = 'Active'
    
    CREATE TABLE #UserTable (UserID int, ErrorCode int, ResultID bigint, RoleID int, CustomerID int)
    CREATE TABLE #QuestionTable (QuestionID int, ScaleID int, Inverted bit, PropertyID nvarchar(64))
    
    INSERT INTO #UserTable (UserID, ErrorCode, CustomerID) SELECT Value, 0, 0 FROM dbo.funcListToTableInt(@ListUserId,',')
    UPDATE t SET t.CustomerID = d.[CustomerID]
    FROM #UserTable t
    JOIN org.[User] u ON t.UserID = u.UserID
    JOIN [org].[Department] d ON u.[DepartmentID] = d.[DepartmentID];

    DECLARE @LinkedDB NVARCHAR(MAX)
    IF @ReportDB=DB_NAME() AND @ReportServer=@@servername
    BEGIN
        SET @LinkedDB=' '
    END
    ELSE
    BEGIN
        SET @LinkedDB= '['+ @ReportServer + '].['+ @ReportDB + '].'
    END

    SET @sqlCommand= N'DELETE u FROM #UserTable u
                       WHERE EXISTS (SELECT 1 FROM '+ @LinkedDB +'dbo.Result r
                                     WHERE r.UserID = u.UserID AND r.SurveyID = @p_SurveyID AND r.BatchID = @p_BatchID AND r.EntityStatusID = '+ CAST(@ActiveEntityStatusID AS VARCHAR(32))+' AND r.Deleted IS NULL'
    IF (@UserGroupID > 0)
    BEGIN
        SET @sqlCommand +='AND r.UserGroupID = @p_UserGroupID '
    END
    SET @sqlCommand +=')'
    SET @sqlParameters = N'@p_SurveyID int,@p_BatchID int,@p_UserGroupID INT'
    EXECUTE sp_executesql @sqlCommand, @sqlParameters, @p_SurveyID = @SurveyId, @p_BatchID = @BatchId, @p_UserGroupID = @UserGroupID
    
    IF NOT EXISTS (SELECT 1 FROM #UserTable)
    BEGIN
        SET @Error = -1
        GOTO EndProcedure
    END

    IF(@UserTypeID > 0) --if UsertypeID has value, we use this role for creating new results.
    BEGIN
        UPDATE #UserTable SET RoleID = (SELECT TOP 1 rm.RoleID
                                        FROM at.RoleMapping rm 
                                        JOIN at.Survey s ON s.ActivityID = rm.ActivityID AND s.SurveyID = @SurveyID
                                        WHERE rm.UserTypeID = @UserTypeID)
    END
    ELSE --it tries to map in RoleMapping with current usertype, if nothing found, the first role in RoleMapping will be used for creating new results. 
    BEGIN
        UPDATE #UserTable SET RoleID = rm.RoleID
        FROM #UserTable u JOIN org.UT_U utu ON u.UserID = utu.UserID
        JOIN at.RoleMapping rm ON rm.UserTypeID = utu.UserTypeID
        JOIN at.Survey s ON s.ActivityID = rm.ActivityID AND s.SurveyID = @SurveyID

        UPDATE #UserTable SET RoleID = (SELECT TOP 1 rm.RoleID
                                        FROM at.RoleMapping rm 
                                        JOIN at.Survey s ON s.ActivityID = rm.ActivityID AND s.SurveyID = @SurveyID
                                        ORDER BY rm.UserTypeID)
        WHERE RoleID IS NULL
    END
    
    INSERT INTO #QuestionTable (QuestionID, ScaleID, Inverted, PropertyID)
    SELECT q.QuestionID, q.ScaleID, q.Inverted, REPLACE(lower(q.ExtId), 'propvalue_', '')
    FROM at.Question q
    JOIN at.Page p ON q.PageID = p.PageID AND p.[Type] = 1 AND lower(q.ExtId) LIKE 'propvalue%' AND p.ActivityID = @ActivityID
    
    UPDATE #QuestionTable SET PropertyID = LEFT(PropertyID, CHARINDEX('_',PropertyID)-1) WHERE CHARINDEX('_',PropertyID) > 0

    DECLARE c_User CURSOR FAST_FORWARD READ_ONLY FOR
    SELECT UserID, RoleID, CustomerID FROM #UserTable
     
    DECLARE @UserID int, @RoleID int, @ResultID bigint, @ErrorCount int = 0, @ErrorCode int, @CustomerID int
     
    OPEN c_User
    FETCH NEXT FROM c_User INTO @UserID, @RoleID, @CustomerID
    WHILE @@FETCH_STATUS = 0
    BEGIN
BEGIN TRANSACTION
       
        SET @sqlCommand = 'INSERT INTO '+ @LinkedDB +'dbo.Result (UserID, RoleID, SurveyID, BatchID, DepartmentID, LanguageID, StatusTypeID, StartDate, LastUpdatedBy, [ValidFrom], [DueDate], UserGroupID, EntityStatusID, [EntityStatusReasonID], [CustomerID])
        VALUES (@p_UserID, @p_RoleID, @p_SurveyID, @p_BatchID, @p_DepartmentID, @p_LanguageID, @p_StatusTypeID, GETDATE(), @p_UpdatedByUser, @p_ValidFrom, @p_DueDate, @p_UserGroupID, @p_ActiveEntityStatusID, @p_EntityStatusReasonID, @p_CustomerID)
        SELECT @p_ResultID = SCOPE_IDENTITY()'
        SET @sqlParameters = '@p_UserID int, @p_RoleID int, @p_SurveyID int, @p_BatchID int, @p_DepartmentID int, @p_LanguageID int, @p_UpdatedByUser int, @p_StatusTypeID int, @p_ValidFrom datetime2(7), @p_DueDate datetime2(7), @p_UserGroupID INT,@p_ActiveEntityStatusID INT, @p_EntityStatusReasonID INT, @p_CustomerID int, @p_ResultID bigint OUTPUT'
        EXECUTE @ErrorCode = sp_executesql @sqlCommand, @sqlParameters, @p_UserID = @UserID, @p_RoleID = @RoleID, @p_SurveyID = @SurveyID, @p_BatchID = @BatchID, @p_DepartmentID = @DepartmentID, 
                @p_LanguageID = @LanguageID, @p_UpdatedByUser = @UpdatedByUser, @p_StatusTypeID = @StatusTypeID, @p_ValidFrom = @ValidFrom, @p_DueDate = @DueDate, @p_UserGroupID = @UserGroupID, 
                @p_ActiveEntityStatusID = @ActiveEntityStatusID, @p_EntityStatusReasonID = @EntityStatusReasonID, @p_CustomerID = @CustomerID, @p_ResultID = @ResultID OUTPUT
       
        SET @ErrorCount = @ErrorCode

        IF @ErrorCount = 0
        BEGIN
            COMMIT TRANSACTION
            UPDATE #UserTable SET ResultID = @ResultID WHERE UserID = @UserID
        END
        ELSE
        BEGIN
            ROLLBACK TRANSACTION
            UPDATE #UserTable SET ErrorCode = 1 WHERE UserID = @UserID
        END
     
        FETCH NEXT FROM c_User INTO @UserID, @RoleID, @CustomerID
    END
    CLOSE c_User
    DEALLOCATE c_User

EndProcedure:
    SELECT * FROM #UserTable

    DROP TABLE #UserTable
    DROP TABLE #QuestionTable
    
    Return @Error
END

GO
