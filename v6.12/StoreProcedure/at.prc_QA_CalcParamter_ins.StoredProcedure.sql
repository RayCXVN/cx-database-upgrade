SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[at].[prc_QA_CalcParamter_ins]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [at].[prc_QA_CalcParamter_ins] AS' 
END
GO

ALTER PROCEDURE [at].[prc_QA_CalcParamter_ins]
(
	@QA_CalcParamterID int = null output,
	@CFID int,
	@No smallint,
	@Name varchar(32),
	@QuestionID int,
	@AlternativeID int,
	@CategoryID int,
	@CalcType smallint,
	@Format nvarchar(32),
	@cUserid int,
	@Log smallint = 1
)
AS
BEGIN
	SET NOCOUNT ON
	DECLARE @Err Int

	INSERT INTO [at].[QA_CalcParamter]
	(
		[CFID],
		[No],
		[Name],
		[QuestionID],
		[AlternativeID],
		[CategoryID],
		[CalcType],
		[Format]
	)
	VALUES
	(
		@CFID,
		@No,
		@Name,
		@QuestionID,
		@AlternativeID,
		@CategoryID,
		@CalcType,
		@Format
	)

	Set @Err = @@Error
	Set @QA_CalcParamterID = scope_identity()

	IF @Log = 1 
	BEGIN 
		INSERT INTO [Log].[AuditLog] ( UserId, TableName, Type, Data, Created) 
		SELECT @cUserid,'QA_CalcParamter',0,
		( SELECT * FROM [at].[QA_CalcParamter] 
			WHERE
			[QA_CalcParamterID] = @QA_CalcParamterID				 FOR XML AUTO) as data,
				getdate() 
	 END

	RETURN @Err
END


GO
