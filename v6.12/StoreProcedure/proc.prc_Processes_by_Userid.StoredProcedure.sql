SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[proc].[prc_Processes_by_Userid]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [proc].[prc_Processes_by_Userid] AS' 
END
GO
ALTER Proc [proc].[prc_Processes_by_Userid]
(
 @Userid int,
 @Ownerid int
)
AS
  Select distinct p.ProcessID
  from [proc].Process p
  JOIN [proc].ProcessAnswer pa on p.Processid = pa.processid and ( pa.UserID = @Userid or p.UserID = @Userid)

GO
