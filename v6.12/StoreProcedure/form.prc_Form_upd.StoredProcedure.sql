SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[form].[prc_Form_upd]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [form].[prc_Form_upd] AS' 
END
GO
/*
	2016-12-05 Sarah - Increase Form.CssClass from 256 to 4000, FormCell.CssClass from 128 to 4000
	2017-06-13 Steve: Change the length of column ExtID in all related tables from xAPI format
*/
ALTER PROCEDURE [form].[prc_Form_upd]
(
	@FormID int,
	@OwnerID int,
	@ElementID int,
	@TableTypeID smallint,
	@ContextFormFieldID INT=NULL,
	@Type smallint,
	@FName nvarchar(64),
	@CssClass nvarchar(4000),
	@ExtID nvarchar(256) = '',
	@cUserid int,
	@Log smallint = 1
)
AS
BEGIN
	SET NOCOUNT ON
	DECLARE @Err Int

	UPDATE [form].[Form]
	SET
		[OwnerID] = @OwnerID,
		[ElementID] = @ElementID,
		[TableTypeID] = @TableTypeID,
		[ContextFormFieldID] = @ContextFormFieldID,
		[Type] = @Type,
		[FName] = @FName,
		[CssClass] = @CssClass,
		[ExtID] = @ExtID
	WHERE
		[FormID] = @FormID

	IF @Log = 1 
	BEGIN 
		INSERT INTO [Log].[AuditLog] ( UserId, TableName, Type, Data, Created) 
		SELECT @cUserid,'Form',1,
		( SELECT * FROM [form].[Form] 
			WHERE
			[FormID] = @FormID			 FOR XML AUTO) as data,
			getdate()
	END

	Set @Err = @@Error

	RETURN @Err
END


GO
