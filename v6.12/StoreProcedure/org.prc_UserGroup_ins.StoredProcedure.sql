SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT 1 FROM sys.objects WHERE name = N'prc_UserGroup_ins' AND type IN ( N'P', N'PC' )  AND SCHEMA_NAME(schema_id) = 'org')
    EXEC ('CREATE PROC [org].[prc_UserGroup_ins] AS ')
GO
/*  
    2016-11-02 Steve: Set default CurrentDate for @LastUpdated, @LastSynchronized
    2017-07-26 Steve: Change the length of column ExtID in all related tables from xAPI format
    2018-07-05 Sarah: ATAdmin/CXPLAT-294/Extend UG_UCXPLAT-854
*/
ALTER PROCEDURE [org].[prc_UserGroup_ins](
    @UserGroupID          int           = NULL OUTPUT,
    @OwnerID              int,
    @DepartmentID         int           = NULL,
    @SurveyId             int           = NULL,
    @Name                 nvarchar(256),
    @Description          ntext,
    @PeriodID             int           = NULL,
    @ExtID                nvarchar(256) = '',
    @Userid               int           = NULL,
    @UsergroupTypeID      int           = NULL,
    @Tag                  nvarchar(max) = '',
    @cUserid              int,
    @Log                  smallint      = 1,
    @LastUpdated          datetime2     = NULL,
    @LastUpdatedBy        int           = NULL,
    @LastSynchronized     datetime2     = NULL,
    @ArchetypeID          int           = NULL,
    @Deleted              datetime2     = NULL,
    @EntityStatusID       int           = NULL,
    @EntityStatusReasonID int           = NULL,
    @ReferrerArchetypeID  int           = NULL,
    @ReferrerResource     nvarchar(max) = '',
    @ReferrerToken        nvarchar(max) = ''
) AS
BEGIN
    SET NOCOUNT ON;
    DECLARE @Err int;

    IF @LastUpdated IS NULL
    BEGIN
        SET @LastUpdated = GETDATE()
    END;

    IF @LastSynchronized IS NULL
    BEGIN
        SET @LastSynchronized = GETDATE()
    END;

    INSERT INTO [org].[UserGroup] ([OwnerID], [DepartmentID], [SurveyId], [Name], [Description], [PeriodID], [ExtID], [UserID], [UsergroupTypeID], [Tag],
                                   [LastUpdated], [LastUpdatedBy], [LastSynchronized], [ArchetypeID], [Deleted], [EntityStatusID], [EntityStatusReasonID],
                                   [ReferrerArchetypeID], [ReferrerResource], [ReferrerToken])
    VALUES (@OwnerID, @DepartmentID, @SurveyId, @Name, @Description, @PeriodID, @ExtID, @Userid, @UsergroupTypeID, @Tag,
            @LastUpdated, @LastUpdatedBy, @LastSynchronized, @ArchetypeID, @Deleted, @EntityStatusID, @EntityStatusReasonID,
            @ReferrerArchetypeID, @ReferrerResource, @ReferrerToken);

    SET @Err = @@Error;
    SET @UserGroupID = SCOPE_IDENTITY();

    IF @Log = 1
    BEGIN
        INSERT INTO [Log].[AuditLog] ([UserId], [TableName], [Type], [Data], [Created])
        SELECT @cUserid, 'UserGroup', 0, (SELECT * FROM [org].[UserGroup] WHERE [UserGroupID] = @UserGroupID FOR XML AUTO) AS [data], GETDATE();
    END;

    RETURN @Err;
END;
GO