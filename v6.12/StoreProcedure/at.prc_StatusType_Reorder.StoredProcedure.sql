SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[at].[prc_StatusType_Reorder]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [at].[prc_StatusType_Reorder] AS' 
END
GO
ALTER PROCEDURE [at].[prc_StatusType_Reorder](
    @OwnerID            int,
    @StatusTypeID       int,
    @CurrentPosition    int,
    @NewPosition        int
)
AS
BEGIN
    SET NOCOUNT ON;
    DECLARE @Err int;

    IF @NewPosition > @CurrentPosition
    BEGIN
        UPDATE [at].[StatusType] SET [No] = [No] - 1 WHERE [OwnerID] = @OwnerID AND [No] > @CurrentPosition AND [No] < @NewPosition;
        UPDATE [at].[StatusType] SET [No] = @NewPosition - 1 WHERE [StatusTypeID] = @StatusTypeID;
    END
    ELSE IF @NewPosition < @CurrentPosition
    BEGIN
        UPDATE [at].[StatusType] SET [No] = [No] + 1 WHERE [OwnerID] = @OwnerID AND [No] >= @NewPosition AND [No] < @CurrentPosition;
        UPDATE [at].[StatusType] SET [No] = @NewPosition WHERE [StatusTypeID] = @StatusTypeID;
    END
    ELSE IF @NewPosition = @CurrentPosition
    BEGIN
        UPDATE [at].[StatusType] SET [No] = [No] + 1 WHERE [OwnerID] = @OwnerID AND [No] >= @NewPosition;
        UPDATE [at].[StatusType] SET [No] = @NewPosition WHERE [StatusTypeID] = @StatusTypeID;
    END
    
    SET @Err = @@Error;

    RETURN @Err;
END;
GO