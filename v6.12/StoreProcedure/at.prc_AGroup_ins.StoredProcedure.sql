SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[at].[prc_AGroup_ins]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [at].[prc_AGroup_ins] AS' 
END
GO
ALTER PROCEDURE [at].[prc_AGroup_ins]
(
	@AGID int = null output ,
	@ActivityID int,
	@ScaleID int,
	@Type smallint,
	@Tag nvarchar(256),
	@ExtId nvarchar(256),
	@No smallint,
	@cUserid int,
	@Log smallint = 1,
	@CssClass nvarchar(64)
)
AS
BEGIN
	SET NOCOUNT ON
	DECLARE @Err Int

	INSERT INTO [at].[AGroup]
	(
		[ActivityID],
		[ScaleID],
		[Type],
		[Tag],
		[ExtId],
		[No],
		[CssClass]
	)
	VALUES
	(
		@ActivityID,
		@ScaleID,
		@Type,
		@Tag,
		@ExtId,
		@No,
		@CssClass
	)

	Set @Err = @@Error
	Set @AGID = scope_identity()

	IF @Log = 1 
	BEGIN 
		INSERT INTO [Log].[AuditLog] ( UserId, TableName, Type, Data, Created) 
		SELECT @cUserid,'AGroup',0,
		( SELECT * FROM [at].[AGroup] 
			WHERE
			[AGID] = @AGID				 FOR XML AUTO) as data,
				getdate() 
	 END

	RETURN @Err
END

GO
