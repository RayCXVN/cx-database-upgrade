IF NOT EXISTS (SELECT 1 FROM sys.objects WHERE name = N'prc_LT_MemberRole_del' AND type IN ( N'P', N'PC' )  AND SCHEMA_NAME(schema_id) = 'org')
	EXEC ('CREATE PROC [org].[prc_LT_MemberRole_del] AS ')
GO
/*  
    2018-07-19 Sarah    Create procedures for Insert, Update, Delete, Get of org.MemberRole
*/
ALTER PROCEDURE [org].[prc_LT_MemberRole_del]
(  
    @MemberRoleID int,
    @LanguageID int,
    @cUserid int,
    @Log smallint = 1
)  
AS  
BEGIN  
 SET NOCOUNT ON  
 DECLARE @Err Int  

 IF @Log = 1 
 BEGIN 
    INSERT INTO [Log].[AuditLog] ( UserId, TableName, [Type], Data, Created) 
    SELECT @cUserid,'LT_MemberRole',2,
    ( SELECT * FROM [org].[LT_MemberRole] 
       WHERE MemberRoleID = @MemberRoleID AND LanguageID = @LanguageID
       FOR XML AUTO
    ) as data,
    getdate() 
 END 


DELETE FROM [org].[LT_MemberRole]
WHERE
MemberRoleID = @MemberRoleID AND LanguageID = @LanguageID

 RETURN @Err  
END