SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[note].[prc_LT_Note_ins]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [note].[prc_LT_Note_ins] AS' 
END
GO
ALTER PROCEDURE [note].[prc_LT_Note_ins]
(
	@LanguageID int,
	@NoteID int,
	@Subject nvarchar(max),
	@Note nvarchar(max),
	@cUserid int,
	@Log smallint = 1
)
AS
BEGIN
	SET NOCOUNT ON
	DECLARE @Err Int

	INSERT INTO [note].[LT_Note]
	(
		[LanguageID],
		[NoteID],
		[Subject],
		[Note]
	)
	VALUES
	(
		@LanguageID,
		@NoteID,
		@Subject,
		@Note
	)

	Set @Err = @@Error
	
	IF @Log = 1 
	BEGIN 
		INSERT INTO [Log].[AuditLog] ( UserId, TableName, Type, Data, Created) 
		SELECT @cUserid,'LT_Note',0,
		( SELECT * FROM [note].[LT_Note] 
			WHERE
			[NoteID] = @NoteID
			AND [LanguageID] = @LanguageID FOR XML AUTO) as data,
				getdate() 
	 END

	RETURN @Err
END

GO
