SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[at].[prc_SentMailFiles_ins]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [at].[prc_SentMailFiles_ins] AS' 
END
GO

ALTER PROCEDURE [at].[prc_SentMailFiles_ins]
(
	@SentMailFileID int = null output,
	@MIMEType nvarchar(64),
	@Data varbinary,
	@Description ntext,
	@Size bigint,
	@Filename nvarchar(128)='',
	@SentMailID INT=NULL,
	@cUserid int,
	@Log smallint = 1
)
AS
BEGIN
	SET NOCOUNT ON
	DECLARE @Err Int

	INSERT INTO [at].[SentMailFiles]
	(
		[MIMEType],
		[Data],
		[Description],
		[Size],
		[Filename],
		[SentMailID]
	)
	VALUES
	(
		@MIMEType,
		@Data,
		@Description,
		@Size,
		@Filename,
		@SentMailID
	)

	Set @Err = @@Error
	Set @SentMailFileID = scope_identity()

	IF @Log = 1 
	BEGIN 
		INSERT INTO [Log].[AuditLog] ( UserId, TableName, Type, Data, Created) 
		SELECT @cUserid,'SentMailFiles',0,
		( SELECT * FROM [at].[SentMailFiles] 
			WHERE
			[SentMailFileID] = @SentMailFileID				 FOR XML AUTO) as data,
				getdate() 
	 END

	RETURN @Err
END


GO
