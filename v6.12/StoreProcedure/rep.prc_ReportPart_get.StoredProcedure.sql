SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[rep].[prc_ReportPart_get]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [rep].[prc_ReportPart_get] AS' 
END
GO
ALTER PROCEDURE [rep].[prc_ReportPart_get]
(
	@ReportID int
)
AS
BEGIN
	SET NOCOUNT ON
	DECLARE @Err Int

	SELECT
	[ReportPartID],
	[ReportID],
	[Name],
	[ReportPartTypeID],
	[SelectionDir],
	[ChartTypeID],
	[ElementID],
	[No],
	[Created],
	[ScaleMinValue],
	[ScaleMaxValue]
	FROM [rep].[ReportPart]
	WHERE
	[ReportID] = @ReportID
    order by [No],[ReportPartID]
	Set @Err = @@Error

	RETURN @Err
END



GO
