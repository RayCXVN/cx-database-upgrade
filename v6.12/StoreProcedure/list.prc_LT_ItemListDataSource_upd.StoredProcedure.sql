SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[list].[prc_LT_ItemListDataSource_upd]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [list].[prc_LT_ItemListDataSource_upd] AS' 
END
GO

-- =============================================
ALTER PROCEDURE [list].[prc_LT_ItemListDataSource_upd]
	@LanguageID int,
	@ItemListDataSourceID int,
	@Name nvarchar(256),
	@Description nvarchar(max),
    @cUserid int,
    @Log smallint = 1
AS
BEGIN
	SET NOCOUNT ON;
	DECLARE @Err Int
	
    UPDATE [list].[LT_ItemListDataSource]
    SET 
		[Name] = @Name,
        [Description] = @Description
     WHERE 
		[ItemListDataSourceID] = @ItemListDataSourceID AND
		[LanguageID] = @LanguageID
		
	IF @Log = 1 
	BEGIN 
		INSERT INTO [Log].[AuditLog] ( UserId, TableName, Type, Data, Created) 
		SELECT @cUserid,'LT_ItemListDataSource',1,
		( SELECT * FROM [list].[LT_ItemListDataSource] 
			WHERE
				[ItemListDataSourceID] = @ItemListDataSourceID AND
				[LanguageID] = @LanguageID			 FOR XML AUTO) as data,
			getdate()
	END

	Set @Err = @@Error

	RETURN @Err	
END


GO
