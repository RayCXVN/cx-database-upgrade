SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[list].[prc_LT_ItemListField_del]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [list].[prc_LT_ItemListField_del] AS' 
END
GO
ALTER PROCEDURE [list].[prc_LT_ItemListField_del]
	@LanguageID int,
	@ItemListFieldID int,
	@cUserid int,
	@Log smallint = 1
AS
BEGIN
	SET NOCOUNT ON;
	DECLARE @Err Int
	
	IF @Log = 1 
	BEGIN 
		INSERT INTO [Log].[AuditLog] ( UserId, TableName, Type, Data, Created) 
		SELECT @cUserid,'LT_ItemListField',2,
		( SELECT * FROM [list].[LT_ItemListField] 
			WHERE
			[LanguageID] = @LanguageID AND
			[ItemListFieldID] = @ItemListFieldID
			 FOR XML AUTO) as data,
			getdate() 
	END 
	
    DELETE FROM [list].[LT_ItemListField]
    WHERE
		[LanguageID] = @LanguageID AND
		[ItemListFieldID] = @ItemListFieldID
	
	Set @Err = @@Error

	RETURN @Err
  
END

GO
