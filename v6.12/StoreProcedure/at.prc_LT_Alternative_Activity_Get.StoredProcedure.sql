SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[at].[prc_LT_Alternative_Activity_Get]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [at].[prc_LT_Alternative_Activity_Get] AS' 
END
GO

ALTER Proc [at].[prc_LT_Alternative_Activity_Get]
(
  @ActivityID int
)
as

Select LTA.Languageid, LTA.AlternativeID,LTA.name, LTA.Label, LTA.Description from 
at.Scale  S
Join At.Alternative A on S.Scaleid = A.scaleid
Join at.LT_Alternative LTA on LTA.Alternativeid = A.Alternativeid
where activityid = @ActivityID
Order by LTA.AlternativeID
GO
