SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[proc].[prc_Process_get_test]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [proc].[prc_Process_get_test] AS' 
END
GO
-- [proc].[prc_Process_get_test] null,1955
ALTER PROCEDURE [proc].[prc_Process_get_test]
(
    @ProcessGroupID int = null,
	@DepartmentID int = null
)
AS
BEGIN
	SET NOCOUNT ON
	DECLARE @Err Int

	SELECT
	[ProcessID],
	[ProcessGroupID],
	[DepartmentID],
	[Active],
	[TargetValue],
	[UserID],
	ISNULL([CustomerID], 0) AS 'CustomerID',
	ISNULL([StartDate], 0) AS 'StartDate',
	ISNULL([DueDate], 0) AS 'DueDate',
	[Responsible],
	[URL],
	[No],
	[Created],
	isnull(ProcessTypeID,0) As 'ProcessTypeID' ,
	isPublic 
	FROM [proc].[Process] P
	WHERE
	
	  (   @DepartmentID IN 
	    (
	      Select Departmentid from [proc].ProcessAnswer PA where pA.ProcessID = P.ProcessID
	    ) 
	    OR @DepartmentID = P.DepartmentID
	    OR @Departmentid is null
	    OR (
	           @Departmentid is not null and
	            [ProcessGroupID] IN  
	            (
	              select ProcessgroupID 
				  from [proc].processgroup 
				  where ownerid = (Select ownerid from org.department where departmentid = @Departmentid)
				) and isPublic = 1 and isnull(customerid,0) = (Select isnull(customerid,0) from org.department where departmentid = @Departmentid)
	      
	    )
	    )
	AND ([ProcessGroupID] = @ProcessGroupID OR @ProcessGroupID is  null)
	
	ORDER BY [No]

	Set @Err = @@Error

	RETURN @Err
END
GO
