SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[org].[prc_Hierarchy_copy_hd]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [org].[prc_Hierarchy_copy_hd] AS' 
END
GO
/* ------------------------------------------------------------
   PROCEDURE:    prc_Hierarchy_copy_hd

   Description:  Copy collection of HD's to the new hierarchy

   AUTHOR:       LockwoodTech 07.07.2006 12:52:51
   ------------------------------------------------------------ */

ALTER PROCEDURE [org].[prc_Hierarchy_copy_hd]
(
	@FromHierarchyID int,
    @FromHDID int,
	@ToHDIDParent int,
    @ToHierarchyID int,
	@IncludeChildren bit,
    @bFirstNode bit = 1
)
As
BEGIN
	SET NOCOUNT ON
	
    DECLARE @Err Int
	DECLARE @HDID as int
	DECLARE @NewHDID as int
	DECLARE @HierarchyID as int
	DECLARE @DepartmentID as int
	DECLARE @ParentID as int
	DECLARE @Created as smalldatetime
	DECLARE @Deleted as bit
    DECLARE @ToParentId as int

   --Copy top node
    if @bFirstNode = 1
    BEGIN
		insert into [org].H_D (HierarchyID, DepartmentID, ParentID, Path, PathName, Created, Deleted)
		select @ToHierarchyID, Departmentid, @ToHDIDParent,'','', getdate(), 0 from [org].H_D 
		WHERE  HDID = @FromHDID
	    
		select @ToParentId = scope_identity()
    END
    ELSE
    BEGIN
      set @ToParentId = @ToHDIDParent
    END 
    -- Copy all below
    IF @IncludeChildren = 1
    BEGIN 
			DECLARE H_D_Cursor CURSOR LOCAL FAST_FORWARD  FOR
			SELECT [HDID], [HierarchyID], [DepartmentID], [ParentID], [Deleted] FROM [org].H_D WHERE HierarchyID = @FromHierarchyID and parentid = @FromHDID and Deleted = 0

			OPEN H_D_Cursor;
			FETCH NEXT FROM H_D_Cursor INTO @HDID, @HierarchyID, @DepartmentID, @ParentID,  @Deleted;
			WHILE @@FETCH_STATUS = 0
				BEGIN
					insert into [org].H_D (HierarchyID, DepartmentID, ParentID, Path, PathName, Created, Deleted)
					Values (@ToHierarchyID, @DepartmentID, @ToParentId,'','', getdate(), @Deleted)
		            
					set @NewHDID  = scope_identity()

					exec [org].[prc_Hierarchy_copy_hd]  @FromHierarchyID ,@HDID , @NewHDID,@ToHierarchyID,@IncludeChildren,0 

					FETCH NEXT FROM H_D_Cursor INTO @HDID, @HierarchyID, @DepartmentID, @ParentID,  @Deleted;
				END

			CLOSE H_D_Cursor
			DEALLOCATE H_D_Cursor
	END
	Set @Err = @@Error

	RETURN @Err
End

GO
