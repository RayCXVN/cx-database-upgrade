SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[at].[prc_B_R_upd]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [at].[prc_B_R_upd] AS' 
END
GO
ALTER PROCEDURE [at].[prc_B_R_upd]
(
	@BulkID int,
	@RoleID int
)
AS
BEGIN
	SET NOCOUNT ON
	DECLARE @Err Int

	UPDATE [at].[B_R]
	SET
		[BulkID] = @BulkID,
		[RoleID] = @RoleID
	WHERE
		[BulkID] = @BulkID AND
		[RoleID] = @RoleID

	Set @Err = @@Error

	RETURN @Err
END



GO
