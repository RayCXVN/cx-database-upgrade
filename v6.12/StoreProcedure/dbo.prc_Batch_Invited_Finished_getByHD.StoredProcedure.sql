SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[prc_Batch_Invited_Finished_getByHD]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [dbo].[prc_Batch_Invited_Finished_getByHD] AS' 
END
GO
ALTER PROCEDURE [dbo].[prc_Batch_Invited_Finished_getByHD]
(
    @SurveyID               int,
    @HDID                   int,
    @DepartmentTypeID       int = 0,
    @DeniedDTIDList         nvarchar(max) = '',
    @GetDepartmentSummary   bit = 0
)
As
BEGIN
	SET NOCOUNT ON
	DECLARE @Err int
    DECLARE @ActiveEntityStatusID INT = (SELECT EntityStatusID FROM EntityStatus WHERE CodeName='Active')

    IF @GetDepartmentSummary = 1
    BEGIN
        SELECT v.[HDID], v.[DepartmentName], v.[Invited], v.[Finished], v.[Finished]/v.[Invited]*100 [PercentFinished]
        FROM (SELECT hd.[HDID], d.[Name] [DepartmentName], convert(float,count(r.[ResultID])) as Invited, convert(float,count(r.EndDate)) as Finished
	          FROM [Result] r JOIN at.[Batch] b ON r.[BatchID] = b.[BatchID] AND r.[SurveyID] = b.[SurveyID] AND b.[SurveyID] = @SurveyID AND r.EntityStatusID = @ActiveEntityStatusID AND r.Deleted IS NULL AND b.[Status] > 0
              JOIN org.[Department] d ON b.[DepartmentID] = d.[DepartmentID] AND d.EntityStatusID = @ActiveEntityStatusID AND d.Deleted IS NULL
              JOIN org.[H_D] hd ON hd.[DepartmentID] = d.[DepartmentID] AND hd.[Deleted] = 0 AND hd.[Path] LIKE '%\' + CONVERT(nvarchar(16),@HDID) + '\%'
              WHERE (@DepartmentTypeID = 0 OR EXISTS (SELECT 1 FROM org.[DT_D] dtd WHERE dtd.[DepartmentID] = b.[DepartmentID] AND dtd.[DepartmentTypeID] = @DepartmentTypeID))
                AND NOT EXISTS (SELECT 2 FROM org.[DT_D] dtd2 WHERE dtd2.[DepartmentID] = d.[DepartmentID] AND dtd2.[DepartmentTypeID] IN (SELECT ParseValue FROM dbo.StringToArray(@DeniedDTIDList, ',')))
              GROUP BY hd.[HDID], d.[Name]) v
        ORDER BY v.[HDID], v.[DepartmentName]
    END
    ELSE
    BEGIN
        SELECT v.[HDID], v.[DepartmentName], v.[BatchID], v.[BatchName], v.[Invited], v.[Finished], v.[Finished]/v.[Invited]*100 [PercentFinished], v.[StartDate], v.[EndDate], v.[Created]
        FROM (SELECT hd.[HDID], d.[Name] [DepartmentName], r.[BatchID], b.[Name] [BatchName], convert(float,count(r.[ResultID])) as Invited, convert(float,count(r.EndDate)) as Finished,
                     b.[StartDate], b.[EndDate], b.[Created]
	          FROM [Result] r JOIN at.[Batch] b ON r.[BatchID] = b.[BatchID] AND r.[SurveyID] = b.[SurveyID] AND b.[SurveyID] = @SurveyID AND r.EntityStatusID = @ActiveEntityStatusID AND r.Deleted IS NULL AND b.[Status] > 0
              JOIN org.[Department] d ON b.[DepartmentID] = d.[DepartmentID] AND d.EntityStatusID = @ActiveEntityStatusID AND d.Deleted IS NULL
              JOIN org.[H_D] hd ON hd.[DepartmentID] = d.[DepartmentID] AND hd.[Deleted] = 0 AND hd.[Path] LIKE '%\' + CONVERT(nvarchar(16),@HDID) + '\%'
              WHERE (@DepartmentTypeID = 0 OR EXISTS (SELECT 1 FROM org.[DT_D] dtd WHERE dtd.[DepartmentID] = b.[DepartmentID] AND dtd.[DepartmentTypeID] = @DepartmentTypeID))
                AND NOT EXISTS (SELECT 2 FROM org.[DT_D] dtd2 WHERE dtd2.[DepartmentID] = d.[DepartmentID] AND dtd2.[DepartmentTypeID] IN (SELECT ParseValue FROM dbo.StringToArray(@DeniedDTIDList, ',')))
              GROUP BY hd.[HDID], d.[Name], r.[BatchID], b.[Name], b.[StartDate], b.[EndDate], b.[Created]) v
        ORDER BY v.[HDID], v.[DepartmentName], v.[BatchName]
    END

	SET @Err = @@Error

	RETURN @Err
END

---------------------------------------------------------------------


GO
