SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[org].[prc_Department_getByDTID]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [org].[prc_Department_getByDTID] AS' 
END
GO
/*  Ray - 2017-02-09 - Remove cross database reference for Azure compatible
*/
ALTER PROCEDURE [org].[prc_Department_getByDTID]
(
	@ParentID               INT,
    @ListDepartmentTypeID   VARCHAR(MAX)
)
AS
BEGIN
	SET NOCOUNT ON  
    DECLARE @Err int
    
    SELECT Value AS DepartmentTypeID INTO #ListDepartmentTypeID FROM [dbo].[funcListToTableInt](@ListDepartmentTypeID, ',')
    
    SELECT DEPT.*                  
	FROM [org].[Department] AS DEPT
    JOIN org.H_D AS HD ON HD.DepartmentID = DEPT.DepartmentID  AND HD.Deleted = 0 AND HD.[Path] LIKE '%\'+ CONVERT(VARCHAR(32),@ParentID) +'\%' 	         
    WHERE EXISTS (SELECT 1 FROM org.DT_D dtd JOIN #ListDepartmentTypeID l ON dtd.[DepartmentTypeID] = l.[DepartmentTypeID]
                                                                         AND dtd.[DepartmentID] = DEPT.[DepartmentID])
	SET @Err = @@Error
    DROP TABLE #ListDepartmentTypeID

	RETURN @Err
END

GO
