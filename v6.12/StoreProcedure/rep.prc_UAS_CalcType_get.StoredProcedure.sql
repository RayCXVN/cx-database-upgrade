SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[rep].[prc_UAS_CalcType_get]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [rep].[prc_UAS_CalcType_get] AS' 
END
GO
ALTER PROCEDURE [rep].[prc_UAS_CalcType_get]
 @UserActivitySettingID int
AS      
BEGIN      
 SET NOCOUNT ON;      
 DECLARE @Err Int          
 SELECT       
	 [UserActivitySettingID]
	,[ReportCalcTypeID]
 FROM             
  [rep].[UAS_CalcType]     
 WHERE        
  [UserActivitySettingID] = @UserActivitySettingID      
 Set @Err = @@Error      
      
 RETURN @Err      
END 

GO
