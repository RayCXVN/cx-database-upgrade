SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[del_Owner]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [dbo].[del_Owner] AS' 
END
GO



-- del_Owner 3
/*	Opprettet av : Morten Ø
	Dato : 09.09.2010
	
	Sletter angitt owner og alt underliggende.
	NB! Sletter ikke Result i R-basene.
	
*/

ALTER PROC [dbo].[del_Owner](
	@OwnerID	INT
) AS
BEGIN
BEGIN TRANSACTION qq

DELETE app.PortalPage WHERE OwnerID = @OwnerID
DELETE dbo.AccessGroup WHERE OwnerID = @OwnerID
DELETE at.StatusType WHERE OwnerID = @OwnerID
DELETE AT.A_VT WHERE ActivityID IN (SELECT ActivityID FROM AT.Activity WHERE OwnerID = @OwnerID)
DELETE AT.XCategory WHERE OwnerID = @OwnerID
DELETE AT.LevelGroup WHERE ActivityID IN (SELECT ActivityID FROM AT.Activity WHERE OwnerID = @OwnerID)
DELETE org.[UserGroup] WHERE OwnerID = @OwnerID
DELETE at.RoleMapping WHERE ActivityID IN (SELECT ActivityID FROM AT.Activity WHERE OwnerID = @OwnerID)
DELETE at.A_R WHERE ActivityID IN (SELECT ActivityID FROM AT.Activity WHERE OwnerID = @OwnerID)
DELETE at.A_R where RoleID in (Select RoleID from at.Role where Ownerid = @OwnerID)
DELETE AT.ActivityView_Q WHERE ActivityViewID IN (SELECT CVVID FROM AT.CVView WHERE ActivityID IN (SELECT ActivityID FROM AT.Activity WHERE OwnerID = @OwnerID) )
DELETE AT.ActivityView WHERE ActivityID IN (SELECT ActivityID FROM AT.Activity WHERE OwnerID = @OwnerID)
DELETE AT.Survey WHERE ActivityID IN (SELECT ActivityID FROM AT.Activity WHERE OwnerID = @OwnerID)
DELETE Rep.Selection  WHERE LevelGroupID IN (SELECT LevelgroupID FROM AT.LevelGroup  where   ActivityID IN (SELECT ActivityID FROM AT.Activity WHERE OwnerID = @OwnerID))
DELETE AT.LevelGroup WHERE ActivityID IN (SELECT ActivityID FROM AT.Activity WHERE OwnerID = @OwnerID)
DELETE rep.RepLevelLimit WHERE reportid in (Select reportid from rep.report where OwnerID = @OwnerID) 
DELETE rep.report WHERE OwnerID = @OwnerID
DELETE AT.QA_Calc WHERE Questionid IN ( SELECT Questionid FROM AT.Question q JOIN AT.page p ON q.pageid = p.pageid  JOIN AT.Activity a ON a.ownerid = @OwnerID AND a.activityid = p.activityid   )
DELETE AT.Action WHERE Questionid IN ( SELECT Questionid FROM AT.Question q JOIN AT.page p ON q.pageid = p.pageid  JOIN AT.Activity a ON a.ownerid = @OwnerID AND a.activityid = p.activityid   )
DELETE AT.Action WHERE pageid IN ( SELECT pageid FROM AT.page p JOIN AT.Activity a ON a.ownerid = @OwnerID AND a.activityid = p.activityid   )
DELETE AT.CalcParameter WHERE Questionid IN ( SELECT Questionid FROM AT.Question q JOIN AT.page p ON q.pageid = p.pageid  JOIN AT.Activity a ON a.ownerid = @OwnerID AND a.activityid = p.activityid   )
DELETE AT.CalcParameter WHERE AlternativeID IN ( SELECT Alternativeid FROM AT.Alternative alt JOIN AT.scale s ON alt.scaleid = s.scaleid  JOIN AT.Activity a ON a.ownerid = @OwnerID AND a.activityid = s.activityid   )
DELETE AT.CalcParameter WHERE CategoryID IN ( Select CategoryID from at.category c join at.activity a on c.activityid = a.activityid and a.ownerid = @OwnerID  )
DELETE AT.Access WHERE Questionid IN ( SELECT Questionid FROM AT.Question q JOIN AT.page p ON q.pageid = p.pageid  JOIN AT.Activity a ON a.ownerid = @OwnerID AND a.activityid = p.activityid   )
DELETE AT.Activity WHERE OwnerID = @OwnerID
DELETE [PROC].[Process] WHERE ProcessTypeID IN (SELECT ProcessTypeid FROM [PROC].[ProcessType] WHERE OwnerID = @OwnerID )
DELETE [PROC].[ProcessType] WHERE OwnerID = @OwnerID
DELETE [PROC].[ProcessValue] WHERE OwnerID = @OwnerID
DELETE [PROC].[ProcessGroup] WHERE OwnerID = @OwnerID
DELETE org.[USER] WHERE OwnerID = @OwnerID
DELETE org.[UserType] WHERE OwnerID = @OwnerID
DELETE org.[Hierarchy] WHERE OwnerID = @OwnerID
DELETE org.[Department] WHERE OwnerID = @OwnerID
DELETE AT.[ROLE] WHERE OwnerID = @OwnerID
DELETE org.[Owner] WHERE OwnerID = @OwnerID


COMMIT TRANSACTION qq
END


GO
