SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[prop].[prc_LT_PropPage_upd]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [prop].[prc_LT_PropPage_upd] AS' 
END
GO

ALTER PROCEDURE [prop].[prc_LT_PropPage_upd]
(
	@LanguageID int,
	@PropPageID int,
	@Name nvarchar(256),
	@Description nvarchar(max),
	@cUserid int,
	@Log smallint = 1
)
AS
BEGIN
	SET NOCOUNT ON
	DECLARE @Err Int

	UPDATE [prop].[LT_PropPage]
	SET
		[LanguageID] = @LanguageID,
		[PropPageID] = @PropPageID,
		[Name] = @Name,
		[Description] = @Description
	WHERE
		[LanguageID] = @LanguageID AND
		[PropPageID] = @PropPageID

	IF @Log = 1 
	BEGIN 
		INSERT INTO [Log].[AuditLog] ( UserId, TableName, Type, Data, Created) 
		SELECT @cUserid,'LT_PropPage',1,
		( SELECT * FROM [prop].[LT_PropPage] 
			WHERE
			[LanguageID] = @LanguageID AND
			[PropPageID] = @PropPageID			 FOR XML AUTO) as data,
			getdate()
	END

	Set @Err = @@Error

	RETURN @Err
END


GO
