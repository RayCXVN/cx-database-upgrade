SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[org].[prc_Username_check]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [org].[prc_Username_check] AS' 
END
GO
/* ------------------------------------------------------------
   PROCEDURE:    prc_Username_check

   Description:  

   AUTHOR:       Morty
   ------------------------------------------------------------ */
ALTER PROCEDURE [org].[prc_Username_check]
(
	@Username		varchar(50),
	@Found			bit OUTPUT
)
As
BEGIN
	SET NOCOUNT ON
	DECLARE @Err	Int,
			@Count	int
			

	Select
	@Count = COUNT(*)
	FROM [org].[User]
	WHERE UserName LIKE @Username
	
	IF @Count > 0
		SET @Found = 1
	ELSE
		SET @Found = 0
	Set @Err = @@Error

	RETURN @Err
End





GO
