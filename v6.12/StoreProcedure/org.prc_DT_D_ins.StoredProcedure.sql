SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[org].[prc_DT_D_ins]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [org].[prc_DT_D_ins] AS' 
END
GO

ALTER PROCEDURE [org].[prc_DT_D_ins]
(
	@DepartmentTypeID int,
	@DepartmentID int,
	@cUserid int,
	@Log smallint = 1
)
AS
BEGIN
	SET NOCOUNT ON
	DECLARE @Err Int

	INSERT INTO [org].[DT_D]
	(
		[DepartmentTypeID],
		[DepartmentID]
	)
	VALUES
	(
		@DepartmentTypeID,
		@DepartmentID
	)

	Set @Err = @@Error

	IF @Log = 1 
	BEGIN 
		INSERT INTO [Log].[AuditLog] ( UserId, TableName, Type, Data, Created) 
		SELECT @cUserid,'DT_D',0,
		( SELECT * FROM [org].[DT_D] 
			WHERE
			[DepartmentTypeID] = @DepartmentTypeID AND
			[DepartmentID] = @DepartmentID				 FOR XML AUTO) as data,
				getdate() 
	 END

	RETURN @Err
END


GO
