/****** Object:  StoredProcedure [stb].[prc_UpdateDepartmentFromNBR]    Script Date: 4/18/2017 11:27:23 AM ******/
DROP PROCEDURE [stb].[prc_UpdateDepartmentFromNBR]
GO
/****** Object:  StoredProcedure [stb].[prc_UpdateDepartmentFromNBR]    Script Date: 4/18/2017 11:27:32 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/*
	2017-02-15 - Sarah - Remove duplicate owner department
*/
CREATE PROCEDURE [stb].[prc_UpdateDepartmentFromNBR]
AS
BEGIN
SET XACT_ABORT ON
BEGIN TRANSACTION

    SET NOCOUNT ON
    DECLARE @DT_Kommune int = 35, @DT_Kindergarten int = 36, @DT_Private int = 37, @MinDepartmentID int = 100
    DECLARE @ToOwnerID int = 17, @FromHierarchyID int = 1, @ToHierarchyID int = 21,
            @DepartmentID int, @ParentDepartmentID int, @ParentOrgNo nvarchar(32), @ImportDate date = GETDATE(),
            @NewDepartmentID int, @NewParentDepartmentID int, @GrandParentHDID int, @NewParentHDID int, @Msg nvarchar(max),
            @BasilOrgNo nvarchar(256), @STB_DepartmentID int, @IsPrivate int, @LoggingUserID int = 1, @DT_Owner int = 39,
            @TopNodeDepartmentID int = 1, @OwnerHDID int, @OwnerDepartmentName nvarchar(64) = 'Barnehageeier',
            @PROP_OwnerDepartmentID int = 53, @EntityStatusID_Active int, @EntityStatusID_Inactive int

	SELECT @EntityStatusID_Active = E.EntityStatusID FROM EntityStatus E WHERE E.CodeName = 'Active'
	SELECT @EntityStatusID_Inactive = E.EntityStatusID FROM EntityStatus E WHERE E.CodeName = 'Inactive'

    CREATE TABLE #UpdatedDepartment ([ActionType] nvarchar(32), [Type] int, [DepartmentID] int, [Name] nvarchar(max), [Description] nvarchar(max), [Adress] nvarchar(max), [PostalCode] nvarchar(max), [City] nvarchar(max), [Tag] nvarchar(max), [OrgNo] nvarchar(32),
                                     [NewName] nvarchar(max), [NewDescription] nvarchar(max), [NewAdress] nvarchar(max), [NewPostalCode] nvarchar(max), [NewCity] nvarchar(max), [NewTag] nvarchar(max), [NewOrgNo] nvarchar(32))
    CREATE TABLE #UpdatedHD ([ActionType] nvarchar(32), [Type] int, [HDID] int, [DepartmentID] int)
    CREATE TABLE #UpdatedDTD ([ActionType] nvarchar(32), [Type] int, [DepartmentTypeID] int, [DepartmentID] int)
    CREATE TABLE #UpdatedProp ([ActionType] nvarchar(32), [Type] int DEFAULT 0, [PropValueID] int, [PropertyID] int, [ItemID] int, [Value] nvarchar(max))
    
    RAISERROR ('Set local departments as inactive for soft deleted departments from NBR', 0, 1) WITH NOWAIT
    UPDATE d SET d.EntityStatusID = @EntityStatusID_Inactive
    OUTPUT 'Update', 1, [DELETED].[DepartmentID], [DELETED].[Name], N'Set inactive due to soft delete from NBR' AS [Description], '' AS [Adress], '' AS [PostalCode], '' AS [City], '' AS [Tag], [DELETED].[OrgNo],
           '' AS [NewName], '' AS [NewDescription], '' AS [NewAdress], '' AS [NewPostalCode], '' AS [NewCity], '' AS [NewTag], '' AS [NewOrgNo]
    INTO #UpdatedDepartment ([ActionType], [Type], [DepartmentID], [Name], [Description], [Adress], [PostalCode], [City], [Tag], [OrgNo],
                             [NewName], [NewDescription], [NewAdress], [NewPostalCode], [NewCity], [NewTag], [NewOrgNo])
    FROM [org].[Department] d JOIN [NBR_Department] nd ON d.[OrgNo] = nd.[OrgNo] COLLATE Danish_Norwegian_CI_AS AND d.[OrgNo] != '' AND d.EntityStatusID = @EntityStatusID_Active AND nd.[Status] < 0 AND d.[DepartmentID] > @MinDepartmentID
     AND (nd.[DT_Kommune] = 1 OR nd.[DT_Privat] = 1 OR nd.[DT_Offentlig] = 1 OR nd.[DT_Barnehageeier] = 1)

    RAISERROR ('%d rows updated', 0, 1, @@rowcount) WITH NOWAIT

    RAISERROR ('Updating imported departments by OrgNo', 0, 1) WITH NOWAIT
    UPDATE d SET [d].[Name] = nd.[Name], d.[Description] = ISNULL(nd.[Description],''), d.[Adress] = ISNULL(nd.[Adress],''), d.[PostalCode] = ISNULL(nd.[PostalCode],''), d.[City] = ISNULL(nd.[City],''), d.[Tag] = ISNULL(nd.[ExtID],''), 
		d.EntityStatusID = ( CASE WHEN nd.[Status] = 0 THEN @EntityStatusID_Active ELSE @EntityStatusID_Inactive END)
    OUTPUT 'Update', 1, [DELETED].[DepartmentID], [DELETED].[Name], [DELETED].[Description], [DELETED].[Adress], [DELETED].[PostalCode], [DELETED].[City], [DELETED].[Tag], [DELETED].[OrgNo],
           [INSERTED].[Name], [INSERTED].[Description], [INSERTED].[Adress], [INSERTED].[PostalCode], [INSERTED].[City], [INSERTED].[Tag], [INSERTED].[OrgNo]
    INTO #UpdatedDepartment ([ActionType], [Type], [DepartmentID], [Name], [Description], [Adress], [PostalCode], [City], [Tag], [OrgNo],
                             [NewName], [NewDescription], [NewAdress], [NewPostalCode], [NewCity], [NewTag], [NewOrgNo])
    FROM [org].[Department] d
    JOIN [NBR_Department] nd ON d.[OrgNo] = nd.[OrgNo] COLLATE Danish_Norwegian_CI_AS AND d.[OrgNo] != '' AND d.EntityStatusID IN (@EntityStatusID_Active, @EntityStatusID_Inactive) AND nd.[Status] >= 0 AND d.[DepartmentID] > @MinDepartmentID
    WHERE EXISTS (SELECT 1 FROM [org].[H_D] hd WHERE d.[DepartmentID] = hd.[DepartmentID] AND hd.[Deleted] = 0)
      AND EXISTS (SELECT 1 FROM [NBR_H_D] nhd WHERE nd.[DepartmentID] = nhd.[DepartmentID] AND nhd.[Deleted] = 0)
      AND (nd.[DT_Kommune] = 1 OR nd.[DT_Privat] = 1 OR nd.[DT_Offentlig] = 1 OR nd.[DT_Barnehageeier] = 1)
      AND ([d].[Name] != nd.[Name] COLLATE Danish_Norwegian_CI_AS
           OR ISNULL(d.[Description],'') != ISNULL(CONVERT(nvarchar(max),nd.[Description]),'') COLLATE Danish_Norwegian_CI_AS
           OR ISNULL(d.[Adress],'') != ISNULL(nd.[Adress],'') COLLATE Danish_Norwegian_CI_AS
           OR ISNULL(d.[PostalCode],'') != ISNULL(nd.[PostalCode],'') COLLATE Danish_Norwegian_CI_AS
           OR ISNULL(d.[City],'') != ISNULL(nd.[City],'') COLLATE Danish_Norwegian_CI_AS
           OR ISNULL(d.[Tag],'') != ISNULL(nd.[ExtID],'') COLLATE Danish_Norwegian_CI_AS)

    SET @Msg = CONVERT(nvarchar(32),@@rowcount) + ' rows updated'
    RAISERROR (@Msg, 0, 1) WITH NOWAIT

    RAISERROR ('Remove department type Private if it''s no longer private in NBR', 0, 1) WITH NOWAIT
    DELETE dtd OUTPUT 'Delete', 2, [DELETED].[DepartmentTypeID], [DELETED].[DepartmentID] INTO #UpdatedDTD ([ActionType], [Type], [DepartmentTypeID], [DepartmentID])
    FROM [org].[DT_D] dtd JOIN [org].[Department] d ON dtd.[DepartmentID] = d.[DepartmentID] AND dtd.[DepartmentTypeID] = @DT_Private AND d.[OrgNo] != ''
    JOIN [org].[H_D] hd ON d.[DepartmentID] = hd.[DepartmentID] AND hd.[Deleted] = 0 AND d.[DepartmentID] > @MinDepartmentID
    WHERE EXISTS (SELECT 1 FROM [NBR_Department] nd JOIN [NBR_H_D] nhd ON nd.[DepartmentID] = nhd.[DepartmentID] AND nhd.[Deleted] = 0 AND nd.[Status] >= 0
                                                     AND nd.[OrgNo] = d.[OrgNo] COLLATE Danish_Norwegian_CI_AS AND nd.[DT_Privat] = 0)

    SET @Msg = CONVERT(nvarchar(32),@@rowcount) + ' rows updated'
    RAISERROR (@Msg, 0, 1) WITH NOWAIT

    RAISERROR ('Mark some departments as Private', 0, 1) WITH NOWAIT
    INSERT INTO [org].[DT_D] ([DepartmentTypeID], [DepartmentID])
    OUTPUT 'Insert', 0, [INSERTED].[DepartmentTypeID], [INSERTED].[DepartmentID] INTO #UpdatedDTD ([ActionType], [Type], [DepartmentTypeID], [DepartmentID])
    SELECT @DT_Private, d.[DepartmentID]
    FROM [org].[Department] d JOIN [NBR_Department] nd ON nd.[OrgNo] = d.[OrgNo] COLLATE Danish_Norwegian_CI_AS AND d.[OrgNo] != '' AND nd.[DT_Privat] = 1
     AND d.EntityStatusID = @EntityStatusID_Active AND d.Deleted IS NULL AND nd.[Status] >= 0 AND d.[DepartmentID] > @MinDepartmentID -- Test departments in STB
    WHERE NOT EXISTS (SELECT 1 FROM [org].[DT_D] dtd WHERE dtd.[DepartmentID] = d.[DepartmentID] AND dtd.[DepartmentTypeID] = @DT_Private)
      AND EXISTS (SELECT 1 FROM [org].[H_D] hd WHERE d.[DepartmentID] = hd.[DepartmentID] AND hd.[Deleted] = 0)

    SET @Msg = CONVERT(nvarchar(32),@@rowcount) + ' rows updated'
    RAISERROR (@Msg, 0, 1) WITH NOWAIT
    RAISERROR ('Importing new departments...', 0, 1) WITH NOWAIT

    -- Get new departments since last import
    DECLARE c_Department CURSOR LOCAL FAST_FORWARD READ_ONLY FOR
    SELECT nd.DepartmentID, m.BasilOrgNo, d.[DepartmentID] STB_DepartmentID, nd.[DT_Privat]
    FROM [NBR_Department] nd JOIN [NBR_H_D] nhd ON nd.[DepartmentID] = nhd.[DepartmentID] AND nd.[Status] >= 0 AND nhd.[Deleted] = 0 AND nd.[OrgNo] != ''
     AND (nd.[DT_Privat] = 1 OR nd.[DT_Offentlig] = 1)
    LEFT JOIN [org].[OrgNoMapping] m ON m.NBROrgNo = nd.[OrgNo] COLLATE Danish_Norwegian_CI_AS
    LEFT JOIN [org].[Department] d ON d.[OrgNo] = m.BasilOrgNo AND d.EntityStatusID = @EntityStatusID_Active AND d.Deleted IS NULL AND d.[DepartmentID] > @MinDepartmentID
    WHERE NOT EXISTS (SELECT 1 FROM [org].[Department] d WHERE nd.[OrgNo] = d.[OrgNo] COLLATE Danish_Norwegian_CI_AS AND d.EntityStatusID = @EntityStatusID_Active AND d.Deleted IS NULL AND d.[OrgNo] != '' AND d.[DepartmentID] > @MinDepartmentID)

    OPEN c_Department
    FETCH NEXT FROM c_Department INTO @DepartmentID, @BasilOrgNo, @STB_DepartmentID, @IsPrivate
    WHILE @@FETCH_STATUS = 0
    BEGIN
        SET @NewParentHDID = 0

        -- Found a department in STB match in [org].[OrgNoMapping]
        IF @STB_DepartmentID > 0
        BEGIN
            UPDATE d SET [d].[Name] = nd.[Name], d.[Description] = ISNULL(nd.[Description],''), d.[Adress] = ISNULL(nd.[Adress],''), d.[PostalCode] = ISNULL(nd.[PostalCode],''), d.[City] = ISNULL(nd.[City],''), d.[Tag] = ISNULL(nd.[ExtID],''),
                         [d].[ExtID] = [d].[OrgNo], [d].[OrgNo] = nd.[OrgNo]
            OUTPUT 'Update', 1, [DELETED].[DepartmentID], [DELETED].[Name], [DELETED].[Description], [DELETED].[Adress], [DELETED].[PostalCode], [DELETED].[City], [DELETED].[Tag], [DELETED].[OrgNo],
                   [INSERTED].[Name], [INSERTED].[Description], [INSERTED].[Adress], [INSERTED].[PostalCode], [INSERTED].[City], [INSERTED].[Tag], [INSERTED].[OrgNo]
            INTO #UpdatedDepartment
            FROM [org].[Department] d
            JOIN [NBR_Department] nd ON nd.[DepartmentID] = @DepartmentID AND d.[DepartmentID] = @STB_DepartmentID

            SET @NewDepartmentID = @STB_DepartmentID
        END
        ELSE
        BEGIN
            INSERT INTO [org].[Department] ([ExtID], [Name], [Adress], [PostalCode], [City], [Description], [OrgNo], [CountryCode], [LanguageID], [OwnerID], [Created], [CustomerID], [Tag], EntityStatusID)
            OUTPUT 'Insert', 0, [INSERTED].[DepartmentID], '', '', '', '', '', '', '',
                   [INSERTED].[Name], [INSERTED].[Description], [INSERTED].[Adress], [INSERTED].[PostalCode], [INSERTED].[City], [INSERTED].[Tag], [INSERTED].[OrgNo]
            INTO #UpdatedDepartment
            SELECT ISNULL(@BasilOrgNo,''), [Name], ISNULL([Adress],''), ISNULL([PostalCode],''), ISNULL([City],''), ISNULL([Description],''), [OrgNo], 47, [LanguageID], @ToOwnerID, @ImportDate, NULL, ISNULL([ExtID],''), @EntityStatusID_Active
            FROM   [NBR_Department] d WHERE d.[DepartmentID] = @DepartmentID

            SET @NewDepartmentID = SCOPE_IDENTITY()
        END

        SELECT @ParentOrgNo = pd.[OrgNo], @ParentDepartmentID = pd.[DepartmentID], @GrandParentHDID = phd.[ParentID]
        FROM [NBR_H_D] hd JOIN [NBR_H_D] phd ON hd.[ParentID] = phd.[HDID] AND hd.[DepartmentID] = @DepartmentID AND hd.[HierarchyID] = @FromHierarchyID
        JOIN [NBR_Department] pd ON pd.[DepartmentID] = phd.[DepartmentID]

        -- Create parent department and H_D if not exists
        IF NOT EXISTS (SELECT 1 FROM [org].[Department] d WHERE d.[OrgNo] = @ParentOrgNo AND [d].[OwnerID] = @ToOwnerID)
        BEGIN
            INSERT INTO [org].[Department] ([ExtID], [Name], [Adress], [PostalCode], [City], [Description], [OrgNo], [CountryCode], [LanguageID], [OwnerID], [Created], [CustomerID], [Tag], EntityStatusID)
            OUTPUT 'Insert', 0, [INSERTED].[DepartmentID], '', '', '', '', '', '', '',
                   [INSERTED].[Name], [INSERTED].[Description], [INSERTED].[Adress], [INSERTED].[PostalCode], [INSERTED].[City], [INSERTED].[Tag], [INSERTED].[OrgNo]
            INTO #UpdatedDepartment
            SELECT '', [Name], ISNULL([Adress],''), ISNULL([PostalCode],''), ISNULL([City],''), ISNULL([Description],''), [OrgNo], 47, [LanguageID], @ToOwnerID, @ImportDate, NULL, ISNULL([ExtID],''), @EntityStatusID_Active
            FROM   [NBR_Department] d WHERE d.[DepartmentID] = @ParentDepartmentID

            SET @NewParentDepartmentID = SCOPE_IDENTITY()
            
            INSERT INTO [org].[H_D] ([HierarchyID], [DepartmentID], [ParentID], [Created], [Deleted])
            OUTPUT 'Insert', 0, [INSERTED].[HDID], [INSERTED].[DepartmentID] INTO #UpdatedHD ([ActionType], [Type], [HDID], [DepartmentID])
            SELECT @ToHierarchyID, @NewParentDepartmentID, hdv6.[HDID], @ImportDate, 0
            FROM [NBR_H_D] hd JOIN [NBR_Department] d ON hd.[DepartmentID] = d.[DepartmentID] AND hd.[HDID] = @GrandParentHDID
            JOIN [org].[Department] dv6 ON d.[OrgNo] = dv6.[OrgNo] COLLATE Danish_Norwegian_CI_AS AND d.[OrgNo] != '' AND dv6.EntityStatusID = @EntityStatusID_Active AND dv6.Deleted IS NULL
            JOIN [org].[H_D] hdv6 ON dv6.[DepartmentID] = hdv6.[DepartmentID] AND hdv6.[Deleted] = 0

            SET @NewParentHDID = SCOPE_IDENTITY()

            INSERT INTO [org].[DT_D] ([DepartmentTypeID], [DepartmentID])
            OUTPUT 'Insert', 0, [INSERTED].[DepartmentTypeID], [INSERTED].[DepartmentID] INTO #UpdatedDTD ([ActionType], [Type], [DepartmentTypeID], [DepartmentID])
            VALUES (@DT_Kommune, @NewParentDepartmentID)
        END

        -- Get parent of new department
        IF @NewParentHDID = 0
        BEGIN
            SELECT @NewParentHDID = hd.[HDID]
            FROM [org].[Department] d JOIN [org].[H_D] hd ON d.[DepartmentID] = hd.[DepartmentID] AND d.[OwnerID] = @ToOwnerID AND d.[OrgNo] = @ParentOrgNo AND d.EntityStatusID = @EntityStatusID_Active AND d.Deleted IS NULL AND hd.[Deleted] = 0 AND hd.[HierarchyID] = @ToHierarchyID
        END

        INSERT INTO [org].[H_D] ([HierarchyID], [DepartmentID], [ParentID], [Created], [Deleted])
        OUTPUT 'Insert', 0, [INSERTED].[HDID], [INSERTED].[DepartmentID] INTO #UpdatedHD ([ActionType], [Type], [HDID], [DepartmentID])
        SELECT @ToHierarchyID, @NewDepartmentID, @NewParentHDID, @ImportDate, 0

        INSERT INTO [org].[DT_D] ([DepartmentTypeID], [DepartmentID])
        OUTPUT 'Insert', 0, [INSERTED].[DepartmentTypeID], [INSERTED].[DepartmentID] INTO #UpdatedDTD ([ActionType], [Type], [DepartmentTypeID], [DepartmentID])
        VALUES (@DT_Kindergarten, @NewDepartmentID)
        
        IF @IsPrivate = 1
        BEGIN
            INSERT INTO [org].[DT_D] ([DepartmentTypeID], [DepartmentID])
            OUTPUT 'Insert', 0, [INSERTED].[DepartmentTypeID], [INSERTED].[DepartmentID] INTO #UpdatedDTD ([ActionType], [Type], [DepartmentTypeID], [DepartmentID])
            VALUES (@DT_Private, @NewDepartmentID)
        END

        SET @Msg = 'Inserted new department ' + CONVERT(nvarchar(16),@NewDepartmentID) + ' to HDID ' + CONVERT(nvarchar(16),@NewParentHDID)
        RAISERROR (@Msg, 0, 1) WITH NOWAIT

        FETCH NEXT FROM c_Department INTO @DepartmentID, @BasilOrgNo, @STB_DepartmentID, @IsPrivate
    END
    CLOSE c_Department
    DEALLOCATE c_Department

    RAISERROR ('Importing Owner departments...', 0, 1) WITH NOWAIT
    SELECT @OwnerHDID = hd.[HDID]
    FROM [org].[Department] d JOIN [org].[H_D] hd ON d.[DepartmentID] = hd.[DepartmentID] AND d.EntityStatusID = @EntityStatusID_Active AND d.Deleted IS NULL AND hd.[Deleted] = 0
     AND d.[Name] = @OwnerDepartmentName AND hd.[ParentID] = @TopNodeDepartmentID

    INSERT INTO [org].[Department] ([ExtID], [Name], [Adress], [PostalCode], [City], [Description], [OrgNo], [CountryCode], [LanguageID], [OwnerID], [Created], [CustomerID], [Tag], EntityStatusID)
    OUTPUT 'InsertOwner', 0, [INSERTED].[DepartmentID], '', '', '', '', '', '', '',
            [INSERTED].[Name], [INSERTED].[Description], [INSERTED].[Adress], [INSERTED].[PostalCode], [INSERTED].[City], [INSERTED].[Tag], [INSERTED].[OrgNo]
    INTO #UpdatedDepartment
    SELECT '', [Name], ISNULL([Adress],''), ISNULL([PostalCode],''), ISNULL([City],''), ISNULL([Description],''), [OrgNo], 47, [LanguageID], @ToOwnerID, @ImportDate, NULL, ISNULL([ExtID],''), @EntityStatusID_Active
    FROM [NBR_Department] nd
    WHERE nd.[DT_Barnehageeier] = 1 AND nd.[Status] >= 0
      AND NOT EXISTS (SELECT 1 FROM [org].[Department] d WHERE d.[OrgNo] = nd.[OrgNo] COLLATE Danish_Norwegian_CI_AS AND d.EntityStatusID = @EntityStatusID_Active AND d.Deleted IS NULL AND d.[OrgNo] != '' AND d.[OwnerID] = @ToOwnerID AND d.[DepartmentID] > @MinDepartmentID)
    
    RAISERROR ('%d rows imported', 0, 1, @@rowcount) WITH NOWAIT

    INSERT INTO [org].[H_D] ([HierarchyID], [DepartmentID], [ParentID], [Created], [Deleted])
    OUTPUT 'Insert', 0, [INSERTED].[HDID], [INSERTED].[DepartmentID] INTO #UpdatedHD ([ActionType], [Type], [HDID], [DepartmentID])
    SELECT @ToHierarchyID, ud.[DepartmentID], @OwnerHDID, @ImportDate, 0
    FROM #UpdatedDepartment ud
    WHERE ud.[ActionType] = 'InsertOwner'

    INSERT INTO [org].[DT_D] ([DepartmentTypeID], [DepartmentID])
    OUTPUT 'Insert', 0, [INSERTED].[DepartmentTypeID], [INSERTED].[DepartmentID] INTO #UpdatedDTD ([ActionType], [Type], [DepartmentTypeID], [DepartmentID])
    SELECT @DT_Owner, ud.[DepartmentID]
    FROM #UpdatedDepartment ud
    WHERE ud.[ActionType] = 'InsertOwner'

    INSERT INTO [org].[DT_D] ([DepartmentTypeID], [DepartmentID])
    OUTPUT 'Insert', 0, [INSERTED].[DepartmentTypeID], [INSERTED].[DepartmentID] INTO #UpdatedDTD ([ActionType], [Type], [DepartmentTypeID], [DepartmentID])
    SELECT @DT_Kindergarten, ud.[DepartmentID]
    FROM #UpdatedDepartment ud
    WHERE ud.[ActionType] = 'InsertOwner'

    UPDATE #UpdatedDepartment SET [ActionType] = 'Insert' WHERE [ActionType] = 'InsertOwner'

    RAISERROR ('Setting owner''s departmentID', 0, 1) WITH NOWAIT
    ;MERGE [prop].[PropValue] T
	USING (SELECT DISTINCT @PROP_OwnerDepartmentID PropertyID, d.[DepartmentID], od.[DepartmentID] OwnerDepartmentID, 0 [No], @ImportDate Created, @LoggingUserID CreatedBy, @ImportDate Updated, @LoggingUserID UpdatedBy
		   FROM [NBR_Department] nd 
				JOIN [org].[Department] d ON nd.[Status] >= 0 AND d.EntityStatusID = @EntityStatusID_Active AND d.Deleted IS NULL AND nd.[OrgNo] = d.[OrgNo] COLLATE Danish_Norwegian_CI_AS AND d.[OrgNo] != '' AND ISNULL(nd.[Tag],'') != '' AND d.[DepartmentID] > @MinDepartmentID
				JOIN [org].[Department] od ON nd.[Tag] = od.[Tag] COLLATE Danish_Norwegian_CI_AS AND od.EntityStatusID = @EntityStatusID_Active AND od.Deleted IS NULL AND od.[DepartmentID] > @MinDepartmentID
		   WHERE nd.DT_Privat = 1
		  ) S
	ON T.PropertyID = S.PropertyID AND T.ItemID = S.DepartmentID
	WHEN MATCHED AND T.Value <> S.OwnerDepartmentID THEN
		UPDATE SET T.Value = S.OwnerDepartmentID
	WHEN NOT MATCHED BY TARGET THEN
		INSERT ([PropertyID], [ItemID], [Value], [No], [Created], [CreatedBy], [Updated], [UpdatedBy])
		VALUES (S.PropertyID, S.DepartmentID, S.OwnerDepartmentID, S.[No], S.Created, S.CreatedBy, S.Updated, S.UpdatedBy)
	WHEN NOT MATCHED BY SOURCE THEN
		DELETE
	OUTPUT
		$ACTION,
		DELETED.PropValueID,
		DELETED.PropertyID,
		DELETED.ItemID,
		DELETED.Value
		INTO #UpdatedProp ([ActionType], [PropValueID], [PropertyID], [ItemID], [Value]);

    RAISERROR ('Added %d references to owner departmentID', 0, 1, @@rowcount) WITH NOWAIT

    RAISERROR ('Logging operations', 0, 1) WITH NOWAIT
    INSERT INTO [log].[AuditLog] ([UserId], [TableName], [Type], [Created], [Data])
    SELECT @LoggingUserID, 'Department', t.[Type], GETDATE(),
           dbo.GetStandardXML('<org.Department DepartmentID="' + convert(nvarchar(32),t.[DepartmentID]) + '" Name="' + t.[Name] + '" NewName="' + t.[NewName] + '" Description="' + t.[Description] + '" NewDescription="' + t.[NewDescription]
           + '" Adress="' + t.[Adress] + '" NewAdress="' + t.[NewAdress] + '" PostalCode="' + t.[PostalCode] + '" NewPostalCode="' + t.[NewPostalCode] + '" City="' + t.[City] + '" NewCity="' + t.[NewCity] + '" Tag="' + t.[Tag] + '" NewTag="' + t.[NewTag]
           + '" OrgNo="' + t.[OrgNo] + '" NewOrgNo="' + t.[NewOrgNo] + '" />')
    FROM #UpdatedDepartment t

    INSERT INTO [log].[AuditLog] ([UserId], [TableName], [Type], [Created], [Data])
    SELECT @LoggingUserID, 'H_D', t.[Type], GETDATE(),
           dbo.GetStandardXML('<org.H_D HDID="' + convert(nvarchar(32),t.[HDID]) + '" DepartmentID="' + convert(nvarchar(32),t.[DepartmentID]) + '" />')
    FROM #UpdatedHD t

    INSERT INTO [log].[AuditLog] ([UserId], [TableName], [Type], [Created], [Data])
    SELECT @LoggingUserID, 'DT_D', t.[Type], GETDATE(),
           dbo.GetStandardXML('<org.DT_D DepartmentTypeID="' + convert(nvarchar(32),t.[DepartmentTypeID]) + '" DepartmentID="' + convert(nvarchar(32),t.[DepartmentID]) + '" />')
    FROM #UpdatedDTD t

    INSERT INTO [log].[AuditLog] ([UserId], [TableName], [Type], [Created], [Data])
    SELECT @LoggingUserID, 'PropValue', t.[Type], GETDATE(),
           dbo.GetStandardXML('<prop.PropValue PropValueID="' + convert(nvarchar(32),t.[PropValueID]) + '" PropertyID="' + convert(nvarchar(32),t.[PropertyID]) + '" ItemID="' + convert(nvarchar(32),t.[ItemID]) + '" Value="' + t.[Value] + '" />')
    FROM #UpdatedProp t

    DROP TABLE #UpdatedDepartment
    DROP TABLE #UpdatedHD
    DROP TABLE #UpdatedDTD
    DROP TABLE #UpdatedProp
    RAISERROR ('Update completed', 0, 1) WITH NOWAIT
COMMIT TRANSACTION
END

GO
