SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[del_AllOwners_Except]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [dbo].[del_AllOwners_Except] AS' 
END
GO

-- del_AllOwners_Except 3
/*	Opprettet av : Morten Ø
	Dato : 09.09.2010
	
	Sletter alle [Owner] bortsett fra angitt OwnerID
	Se dbo.[del_Owner] for mer info
*/

ALTER proc [dbo].[del_AllOwners_Except](
	@OwnerID	int
) AS
BEGIN
	DECLARE @TmpID	int
	BEGIN TRANSACTION TT
	DECLARE curO CURSOR LOCAL FAST_FORWARD READ_ONLY FOR
	SELECT OwnerID
	FROM org.[Owner] WHERE OwnerID <> @OwnerID

	OPEN curO
	FETCH NEXT FROM curO INTO @TmpID
	WHILE (@@FETCH_STATUS = 0)
	BEGIN
		EXEC del_Owner @TmpID
		FETCH NEXT FROM curO INTO @TmpID
	END
	CLOSE curO
	DEALLOCATE curO
	COMMIT TRANSACTION TT
END

GO
