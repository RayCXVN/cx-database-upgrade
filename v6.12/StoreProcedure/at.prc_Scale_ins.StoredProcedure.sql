SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[at].[prc_Scale_ins]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [at].[prc_Scale_ins] AS' 
END
GO
/*
	2017-07-26 Steve: Change the length of column ExtID in all related tables from xAPI format
*/
ALTER PROCEDURE [at].[prc_Scale_ins]
(
	@ScaleID int = null output,
	@ActivityID INT=NULL,
	@MinSelect smallint,
	@MaxSelect smallint,
	@Type smallint,
	@MinValue float,
	@MaxValue float,
	@Tag nvarchar(128),
	@ExtID nvarchar(256),
	@cUserid int,
	@Log smallint = 1
)
AS
BEGIN
	SET NOCOUNT ON
	DECLARE @Err Int

	INSERT INTO [at].[Scale]
	(
		[ActivityID],
		[MinSelect],
		[MaxSelect],
		[Type],
		[MinValue],
		[MaxValue],
		[Tag],
		[ExtID]
	)
	VALUES
	(
		@ActivityID,
		@MinSelect,
		@MaxSelect,
		@Type,
		@MinValue,
		@MaxValue,
		@Tag,
		@ExtID
	)

	Set @Err = @@Error
	Set @ScaleID = scope_identity()

	IF @Log = 1 
	BEGIN 
		INSERT INTO [Log].[AuditLog] ( UserId, TableName, Type, Data, Created) 
		SELECT @cUserid,'Scale',0,
		( SELECT * FROM [at].[Scale] 
			WHERE
			[ScaleID] = @ScaleID				 FOR XML AUTO) as data,
				getdate() 
	 END

	RETURN @Err
END

GO
