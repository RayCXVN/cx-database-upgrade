SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[at].[prc_StatusTypeMapping_upd]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [at].[prc_StatusTypeMapping_upd] AS' 
END
GO

ALTER PROCEDURE [at].[prc_StatusTypeMapping_upd]
(
	@StatusTypeMappingID int,
	@ActivityID int,
	@CustomerID INT=NULL,
	@FromStatusTypeID int = null,
	@ToStatusTypeID int,
	@ShowActionInUI bit =0,
	@No smallint,
	@Settings XML ='',
	@cUserid int,
	@Log smallint = 1
)
AS
BEGIN
	SET NOCOUNT ON
	DECLARE @Err Int

	UPDATE [at].[StatusTypeMapping]
	SET
		[ActivityID] = @ActivityID,
		[CustomerID] = @CustomerID,
		[FromStatusTypeID] = @FromStatusTypeID,
		[ToStatusTypeID] = @ToStatusTypeID,
		[ShowActionInUI]=   @ShowActionInUI,
		[No] = @No,
		[Settings] =@Settings
	WHERE
		[StatusTypeMappingID] = @StatusTypeMappingID

	IF @Log = 1 
	BEGIN 
		INSERT INTO [Log].[AuditLog] ( UserId, TableName, Type, Data, Created) 
		SELECT @cUserid,'StatusTypeMapping',1,
		( SELECT * FROM [at].[StatusTypeMapping] 
			WHERE
			[StatusTypeMappingID] = @StatusTypeMappingID			 FOR XML AUTO) as data,
			getdate()
	END

	Set @Err = @@Error

	RETURN @Err
END

GO
