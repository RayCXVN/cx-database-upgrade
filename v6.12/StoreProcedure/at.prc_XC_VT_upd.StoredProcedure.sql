SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[at].[prc_XC_VT_upd]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [at].[prc_XC_VT_upd] AS' 
END
GO

ALTER PROCEDURE [at].[prc_XC_VT_upd]
(
	@XCVTID int,
	@ViewTypeID int,
	@XCID int,
	@No smallint,
	@Type smallint,
	@Template varchar(64),
	@ShowPercentage BIT,
	@cUserid int,
	@Log smallint = 1
)
AS
BEGIN
	SET NOCOUNT ON
	DECLARE @Err Int

	UPDATE [at].[XC_VT]
	SET
		[ViewTypeID] = @ViewTypeID,
		[XCID] = @XCID,
		[No] = @No,
		[Type] = @Type,
		[Template] = @Template,
		[ShowPercentage] = @ShowPercentage
	WHERE
		[XCVTID] = @XCVTID

	IF @Log = 1 
	BEGIN 
		INSERT INTO [Log].[AuditLog] ( UserId, TableName, Type, Data, Created) 
		SELECT @cUserid,'XC_VT',1,
		( SELECT * FROM [at].[XC_VT] 
			WHERE
			[XCVTID] = @XCVTID			 FOR XML AUTO) as data,
			getdate()
	END

	Set @Err = @@Error

	RETURN @Err
END


GO
