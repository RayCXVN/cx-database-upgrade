SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[at].[prc_LT_Page_upd]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [at].[prc_LT_Page_upd] AS' 
END
GO
ALTER PROCEDURE [at].[prc_LT_Page_upd]
(
	@LanguageID int,
	@PageID int,
	@Name nvarchar(max),
	@Info nvarchar(max),
	@Description nvarchar(max),
	@cUserid int,
	@Log smallint = 1
)
AS
BEGIN
	SET NOCOUNT ON
	DECLARE @Err Int

	UPDATE [at].[LT_Page]
	SET
		[LanguageID] = @LanguageID,
		[PageID] = @PageID,
		[Name] = @Name,
		[Info] = @Info,
		[Description] = @Description
	WHERE
		[LanguageID] = @LanguageID AND
		[PageID] = @PageID

	IF @Log = 1 
	BEGIN 
		INSERT INTO [Log].[AuditLog] ( UserId, TableName, Type, Data, Created) 
		SELECT @cUserid,'LT_Page',1,
		( SELECT * FROM [at].[LT_Page] 
			WHERE
			[LanguageID] = @LanguageID AND
			[PageID] = @PageID			 FOR XML AUTO) as data,
			getdate()
	END

	Set @Err = @@Error

	RETURN @Err
END

GO
