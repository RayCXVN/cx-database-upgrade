SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[form].[prc_FormCommandCondition_ins]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [form].[prc_FormCommandCondition_ins] AS' 
END
GO
ALTER PROCEDURE [form].[prc_FormCommandCondition_ins] (
	@FormCommandConditionID INT = NULL OUTPUT
	,@FormCommandID INT
	,@Type NVARCHAR(32)
	,@Param NVARCHAR(MAX)
	,@Value NVARCHAR(32)
	,@cUserid INT
	,@Log SMALLINT = 1
	)
AS
BEGIN
	SET NOCOUNT ON;

	DECLARE @Err INT

	INSERT INTO [form].[FormCommandCondition] (
		[FormCommandID]
		,[Type]
		,[Param]
		,[Value]
		)
	VALUES (
		@FormCommandID
		,@Type
		,@Param
		,@Value
		)

	SET @Err = @@Error
	SET @FormCommandConditionID = scope_identity()

	IF @Log = 1
	BEGIN
		INSERT INTO [Log].[AuditLog] (
			UserId
			,TableName
			,Type
			,Data
			,Created
			)
		SELECT @cUserid
			,'FormCommandCondition'
			,0
			,(
				SELECT *
				FROM [form].[FormCommandCondition]
				WHERE [FormCommandConditionID] = @FormCommandConditionID
				FOR XML AUTO
				) AS data
			,getdate()
	END

	RETURN @Err
END


GO
