SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[at].[prc_DR_S_ins]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [at].[prc_DR_S_ins] AS' 
END
GO

ALTER PROCEDURE [at].[prc_DR_S_ins]
(
	@DottedRuleID int,
	@ScaleID int,
	@cUserid int,
	@Log smallint = 1
)
AS
BEGIN
	SET NOCOUNT ON
	DECLARE @Err Int

	INSERT INTO [at].[DR_S]
	(
		[DottedRuleID],
		[ScaleID]
	)
	VALUES
	(
		@DottedRuleID,
		@ScaleID
	)

	Set @Err = @@Error

	IF @Log = 1 
	BEGIN 
		INSERT INTO [Log].[AuditLog] ( UserId, TableName, Type, Data, Created) 
		SELECT @cUserid,'DR_S',0,
		( SELECT * FROM [at].[DR_S] 
			WHERE
			[DottedRuleID] = @DottedRuleID AND
			[ScaleID] = @ScaleID				 FOR XML AUTO) as data,
				getdate() 
	 END

	RETURN @Err
END


GO
