SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[org].[prc_User_SearchByID]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [org].[prc_User_SearchByID] AS' 
END
GO
/*  2016-10-10 Ray: Remove column Status from org.User, org.Department, RDB.dbo.Result
*/
ALTER PROCEDURE [org].[prc_User_SearchByID]
(  
 @UserID int
)  
As  
BEGIN  
 SET NOCOUNT ON  
 DECLARE @Err Int  
  
 Select  
 u.[UserID],  
 u.[DepartmentID],  
 u.[LanguageID],  
 u.[UserName],  
 u.[Password],  
 u.[LastName],  
 u.[FirstName],  
 u.[Email],  
 u.[ExtID],  
 u.[SSN],  
 u.[Created],  
 u.[Mobile],  
 u.[Tag],  
 u.Locked,  
 u.Gender,
 u.DateOfBirth,
 u.ChangePassword,  
 u.[HashPassword],  
 u.[SaltPassword],  
 u.[OneTimePassword],  
 u.[OTPExpireTime],  
 u.CountryCode,
 u.EntityStatusID,
 u.Deleted,
 u.EntityStatusReasonID
 FROM org.[User] u  
 WHERE 
	u.[UserID] = @UserID
  
 Set @Err = @@Error  
  
 RETURN @Err  
End

GO
