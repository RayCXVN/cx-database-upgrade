SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[log].[prc_BubbleLog_ins]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [log].[prc_BubbleLog_ins] AS' 
END
GO

ALTER PROCEDURE [log].[prc_BubbleLog_ins]
(
	@BubbleLogID int = null output,
	@BubbleID int,
	@SurveyID int,
	@Type smallint,
	@Status smallint,
	@Description nvarchar(max),
	@ReportServer nvarchar(64)='',
	@ReportDB nvarchar(64)=''
)
AS
BEGIN
	SET NOCOUNT ON
	DECLARE @Err Int

	INSERT INTO [log].[BubbleLog]
	(
		[BubbleID],
		[SurveyID],
		[Type],
		[Status],
		[Description],
		[ReportServer],
		[ReportDB]
	)
	VALUES
	(
		@BubbleID,
		@SurveyID,
		@Type,
		@Status,
		@Description,
		@ReportServer,
		@ReportDB
	)

	Set @Err = @@Error

	RETURN @Err
END


GO
