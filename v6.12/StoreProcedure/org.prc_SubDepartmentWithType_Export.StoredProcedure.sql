SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[org].[prc_SubDepartmentWithType_Export]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [org].[prc_SubDepartmentWithType_Export] AS' 
END
GO
/*
	2017-01-17	- Sarah - Created in STB project
	2017-04-25	- Sarah - excluded department which does not have department type Barnehage
	EXEC [org].[prc_SubDepartmentWithType_Export] @HDID=1,@DepartmentTypeID=36,@CheckBatchInSub=1,@SurveyID=328,@PROP_OwnerDepartmentID=53,@DT_Owner=39,@DeniedDTIDList='37',@CurrentHDID=1
*/
CREATE PROCEDURE [org].[prc_SubDepartmentWithType_Export] 
(   @HDID                       INT,
    @DepartmentTypeID           INT = 0,
    @CheckBatchInSub            BIT = 0,
    @SurveyID                   INT = 0,
    @PROP_OwnerDepartmentID     INT = 0,
    @DT_Owner                   INT = 0,
	@DeniedDTIDList				VARCHAR(MAX) = '',
	@SiteID						INT = 3,
	@CurrentHDID				INT = 1
) AS
BEGIN
    SET NOCOUNT ON

	DECLARE @EntityStatusID_Active INT
    SELECT @EntityStatusID_Active = [EntityStatusID] FROM [EntityStatus] WHERE [CodeName] = 'Active'

	CREATE TABLE #DeniedDTIDTable ([DepartmentTypeID] int)
    INSERT INTO #DeniedDTIDTable SELECT [Value] FROM dbo.funcListToTableInt(@DeniedDTIDList, ',')

    CREATE TABLE #SubHD (HDID INT, DepartmentID INT, [IsOwner] BIT, [HasBatch] BIT, [Path] NVARCHAR(MAX), [ParentPathName] NVARCHAR(MAX), [ParentPath] NVARCHAR(MAX), Invited FLOAT DEFAULT 0, Finished FLOAT DEFAULT 0, PercentFinished FLOAT DEFAULT 0, AllowAppear BIT)
	CREATE TABLE #Result (HDID INT, ResultID INT, EndDate DATETIME)
    DECLARE @ReportServer NVARCHAR(64) = @@servername, @ReportDB NVARCHAR(64) = DB_NAME(), @sqlCommand NVARCHAR(MAX) = ''
	DECLARE @LinkedDB NVARCHAR(MAX)
	DECLARE @DepartmentType_Fylke INT, @DepartmentType_Kommune INT, @DepartmentType_Barnehage INT, @DepartmentType_FrittståendeEllerPrivat INT

	SELECT @DepartmentType_Fylke = S.Value FROM app.SiteParameter s WHERE S.[Key] = 'Platform.Common.FylkeDepartmentTypeId' AND s.SiteID = @SiteID
	SELECT @DepartmentType_Kommune = S.Value FROM app.SiteParameter s WHERE S.[Key] = 'Platform.Common.KommuneDepartmentTypeId' AND s.SiteID = @SiteID
	SELECT @DepartmentType_Barnehage = S.Value FROM app.SiteParameter s WHERE S.[Key] = 'Platform.Common.BarnehageDepartmentTypeId' AND s.SiteID = @SiteID
	SELECT @DepartmentType_FrittståendeEllerPrivat = S.Value FROM app.SiteParameter s WHERE S.[Key] = 'Platform.Common.FrittståendeEllerPrivatDepartmentTypeId' AND s.SiteID = @SiteID
	
	-- If given HDID is an Owner department	
	INSERT INTO #SubHD (HDID, DepartmentID, HasBatch, [ParentPathName], IsOwner, [Path], ParentPath, AllowAppear)
	SELECT hd.[HDID], d.[DepartmentID],
			CASE WHEN b.DepartmentID IS NOT NULL THEN 1 ELSE 0 END AS [HasBatch],
			phd.[PathName] [ParentPathName],
			CASE WHEN dtd.[DepartmentTypeID] IS NOT NULL THEN 1 ELSE 0 END AS [IsOwner],  
			hd.[Path],
			phd.[Path] ParentPath,
			1 AS AllowAppear
    FROM [org].[Department] d 
		JOIN [prop].[PropValue] pv ON d.[DepartmentID] = pv.[ItemID] AND pv.[PropertyID] = @PROP_OwnerDepartmentID AND d.EntityStatusID = @EntityStatusID_Active AND d.Deleted IS NULL
		JOIN [org].[H_D] phd ON phd.[HDID] = @CurrentHDID AND pv.[Value] = CAST(phd.[DepartmentID] AS NVARCHAR(32))
		JOIN [org].[H_D] hd ON d.[DepartmentID] = hd.[DepartmentID] AND hd.[Deleted] = 0
		LEFT JOIN [org].[DT_D] dtd ON hd.[DepartmentID] = dtd.[DepartmentID] AND dtd.[DepartmentTypeID] = @DT_Owner
		LEFT OUTER JOIN (
			SELECT b.DepartmentID 
			FROM at.Batch b 
			WHERE b.SurveyID = @SurveyID AND b.[Status] > 0
				  AND EXISTS (SELECT 1 FROM org.DT_D dtd WHERE dtd.DepartmentID = b.DepartmentID AND dtd.DepartmentTypeID = @DepartmentType_Barnehage)
			GROUP BY b.DepartmentID
		) b ON b.DepartmentID = d.DepartmentID
	
	-- Get direct children departments
	INSERT INTO #SubHD ([HDID], [DepartmentID], [IsOwner], [HasBatch], [Path], ParentPathName)
	SELECT hd.[HDID], hd.[DepartmentID], (CASE WHEN dtd.[DepartmentTypeID] IS NOT NULL THEN 1 ELSE 0 END) AS [IsOwner], 0 AS [HasBatch], hd.[Path], 
		CASE WHEN hd.HDID = @HDID THEN NULL ELSE hd.[PathName] END ParentPathName
	FROM [org].[H_D] hd 
		JOIN org.Department d ON hd.DepartmentID = d.DepartmentID AND d.EntityStatusID = @EntityStatusID_Active AND d.Deleted IS NULL
		LEFT JOIN [org].[DT_D] dtd ON hd.[DepartmentID] = dtd.[DepartmentID] AND dtd.[DepartmentTypeID] = @DT_Owner
	WHERE hd.[Path] LIKE '%\' + CAST(@HDID AS VARCHAR(128)) + '\%' AND hd.[Deleted] = 0 AND hd.ParentID IS NOT NULL
		AND NOT EXISTS (SELECT 1 FROM #SubHD shd WHERE shd.[HDID] = hd.[HDID])
		AND NOT EXISTS (SELECT 1 FROM [org].[DT_D] dtd2 JOIN #DeniedDTIDTable ddt ON dtd2.[DepartmentTypeID] = ddt.[DepartmentTypeID] AND dtd2.[DepartmentID] = hd.[DepartmentID])
	
    IF @CheckBatchInSub = 1
    BEGIN
        UPDATE shd SET [shd].[HasBatch] = 1
        FROM #SubHD shd
        WHERE EXISTS (SELECT 1 
					  FROM [at].[Batch] b 
							JOIN [org].[H_D] hd ON b.[DepartmentID] = hd.[DepartmentID] AND b.[Status] > 0
								 AND hd.[Path] LIKE '%\' + CONVERT(NVARCHAR(16), shd.HDID) + '\%' AND hd.[Deleted] = 0
                                 AND (b.[SurveyID] = @SurveyID)
								 AND NOT EXISTS (SELECT 1 FROM [org].[DT_D] dtd JOIN #DeniedDTIDTable ddt ON dtd.[DepartmentTypeID] = ddt.[DepartmentTypeID] AND dtd.[DepartmentID] = b.[DepartmentID])
								 )

        
        UPDATE shd SET [shd].[HasBatch] = 1
        FROM #SubHD shd
        WHERE shd.[HasBatch] = 0 AND shd.[IsOwner] = 1
          AND EXISTS (SELECT 1 
					  FROM [at].[Batch] b 
							JOIN [org].[Department] d ON b.[DepartmentID] = d.[DepartmentID] AND d.EntityStatusID = @EntityStatusID_Active AND d.Deleted IS NULL
                            JOIN [prop].[PropValue] pv ON pv.[PropertyID] = @PROP_OwnerDepartmentID AND pv.[ItemID] = d.[DepartmentID] AND b.[Status] > 0
                                 AND pv.[Value] = CAST(shd.[DepartmentID] AS NVARCHAR(32))
                                 AND b.[SurveyID] = @SurveyID
								 AND EXISTS (SELECT 1 FROM org.DT_D DTD WHERE DTD.DepartmentID = shd.DepartmentID AND DTD.DepartmentTypeID = @DepartmentType_Barnehage))

        DELETE FROM #SubHD WHERE [HasBatch] = 0
    END
	
	SELECT @ReportDB = S.ReportDB, @ReportServer = s.ReportServer FROM at.Survey S WHERE S.SurveyID = @SurveyID
	IF @ReportDB=DB_NAME() AND @ReportServer=@@servername
	BEGIN
		SET @LinkedDB=' '
	END
	ELSE
	BEGIN
		SET @LinkedDB= '['+ @ReportServer + '].['+ @ReportDB + '].'
	END

	--Get all result 
	SET @sqlCommand = 'INSERT INTO #Result
	SELECT shd.HDID, r.ResultID, r.EndDate
	FROM at.Batch b 
		JOIN #SubHD shd ON b.DepartmentID = shd.DepartmentID
		JOIN '+@LinkedDB+'[dbo].Result r ON r.BatchID = b.BatchID AND r.SurveyID = b.SurveyID AND r.EntityStatusID = ' + CAST(@EntityStatusID_Active AS VARCHAR(128)) + ' AND r.Deleted IS NULL
	WHERE b.SurveyID = ' + CAST(@SurveyID AS VARCHAR(128)) + ' AND b.[Status] > 0
		AND (shd.AllowAppear = 1 OR NOT EXISTS (SELECT 1 FROM [org].[DT_D] dtd2 WHERE dtd2.[DepartmentID] = b.[DepartmentID] AND dtd2.[DepartmentTypeID] IN (SELECT ParseValue FROM dbo.StringToArray('+@DeniedDTIDList+', '',''))))
		AND (' + CAST(@DepartmentTypeID AS VARCHAR(128)) + ' = 0 OR EXISTS (SELECT 1 FROM [org].[DT_D] dtd WHERE dtd.[DepartmentID] = b.[DepartmentID] AND dtd.[DepartmentTypeID] = ' + CAST(@DepartmentTypeID AS VARCHAR(128)) + '))
	'
	EXEC sp_executesql @sqlCommand

	;WITH Temp AS(
		SELECT shd.HDID,
			Invited = CONVERT(FLOAT,COUNT(r.[ResultID])), 
			Finished = CONVERT(FLOAT,COUNT(r.EndDate)),
			[PercentFinished] = CASE WHEN CONVERT(FLOAT,COUNT(r.[ResultID])) > 0 THEN CONVERT(FLOAT,COUNT(r.EndDate))/CONVERT(FLOAT,COUNT(r.[ResultID]))*100 ELSE 0 END 
		FROM #SubHD shd
			JOIN org.H_D hd ON hd.[Path] LIKE '%\' + CAST(shd.HDID AS VARCHAR(128)) + '\%'
			JOIN #Result r ON r.HDID = hd.HDID
		GROUP BY shd.HDID
	)
	UPDATE shd SET
		shd.Invited = T.Invited, 
		shd.Finished = T.Finished,
		shd.[PercentFinished] = T.[PercentFinished]
	FROM #SubHD shd
		JOIN Temp T ON T.HDID = shd.HDID

	;WITH Temp AS(
		SELECT shd.HDID,
			Invited = CONVERT(FLOAT,COUNT(r.[ResultID])), 
			Finished = CONVERT(FLOAT,COUNT(r.EndDate)),
			[PercentFinished] = CASE WHEN CONVERT(FLOAT,COUNT(r.[ResultID])) > 0 THEN CONVERT(FLOAT,COUNT(r.EndDate))/CONVERT(FLOAT,COUNT(r.[ResultID]))*100 ELSE 0 END 
		FROM #SubHD shd
			JOIN #SubHD hd ON hd.[ParentPath] LIKE '%\' + CAST(shd.HDID AS VARCHAR(128)) + '\%' OR hd.HDID = shd.HDID
			JOIN #Result r ON r.HDID = hd.HDID
		WHERE shd.IsOwner = 1
		GROUP BY shd.HDID
	)
	UPDATE shd SET
		shd.Invited = T.Invited, 
		shd.Finished = T.Finished,
		shd.[PercentFinished] = T.[PercentFinished]
	FROM #SubHD shd
		JOIN Temp T ON T.HDID = shd.HDID
	
	SELECT @SurveyID SurveyID, S.HDID, S.DepartmentID, 
		CASE WHEN Fylke.Name IS NULL THEN '--' ELSE Fylke.Name END Fylke, 
		CASE WHEN Kommune.Name IS NULL THEN '--' ELSE Kommune.Name END Kommune, 
		CASE WHEN Barnehage.Name IS NULL THEN '--' ELSE Barnehage.Name END Barnehage,
		S.Invited, S.Finished, S.PercentFinished
	FROM #SubHD S
		LEFT OUTER JOIN (SELECT hd.HDID, hd.DepartmentID, d.Name
						 FROM org.H_D hd 
							JOIN org.Department d ON hd.DepartmentID = d.DepartmentID
						 WHERE EXISTS (SELECT 1 FROM org.DT_D dtd WHERE dtd.DepartmentID = d.DepartmentID AND dtd.DepartmentTypeID = @DepartmentType_Fylke)
						) Fylke ON s.[Path] LIKE '%\' + CAST(Fylke.HDID AS VARCHAR(128)) + '\%'
		LEFT OUTER JOIN (SELECT hd.HDID, hd.DepartmentID, d.Name
						 FROM org.H_D hd 
							JOIN org.Department d ON hd.DepartmentID = d.DepartmentID
						 WHERE EXISTS (SELECT 1 FROM org.DT_D dtd WHERE dtd.DepartmentID = d.DepartmentID AND dtd.DepartmentTypeID = @DepartmentType_Kommune)
						) Kommune ON s.[Path] LIKE '%\' + CAST(Kommune.HDID AS VARCHAR(128)) + '\%'
		LEFT OUTER JOIN (SELECT hd.HDID, hd.DepartmentID, d.Name
						 FROM org.H_D hd 
							JOIN org.Department d ON hd.DepartmentID = d.DepartmentID
						 WHERE EXISTS (SELECT 1 FROM org.DT_D dtd WHERE dtd.DepartmentID = d.DepartmentID AND dtd.DepartmentTypeID = @DepartmentType_Barnehage)
						) Barnehage ON s.[Path] LIKE '%\' + CAST(Barnehage.HDID AS VARCHAR(128)) + '\%'
	ORDER BY S.ParentPathName

	DROP TABLE #SubHD
	DROP TABLE #Result
END

GO
