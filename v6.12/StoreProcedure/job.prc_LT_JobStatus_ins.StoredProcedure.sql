SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[job].[prc_LT_JobStatus_ins]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [job].[prc_LT_JobStatus_ins] AS' 
END
GO

ALTER PROCEDURE [job].[prc_LT_JobStatus_ins]
(
	@LanguageID int,
	@JobStatusID smallint,
	@Name nvarchar(256),
	@Description nvarchar(max),
	@cUserid int,
	@Log smallint = 1
)
AS
BEGIN
	SET NOCOUNT ON
	DECLARE @Err Int

	INSERT INTO [job].[LT_JobStatus]
	(
		[LanguageID],
		[JobStatusID],
		[Name],
		[Description]
	)
	VALUES
	(
		@LanguageID,
		@JobStatusID,
		@Name,
		@Description
	)

	Set @Err = @@Error

	IF @Log = 1 
	BEGIN 
		INSERT INTO [Log].[AuditLog] ( UserId, TableName, Type, Data, Created) 
		SELECT @cUserid,'LT_JobStatus',0,
		( SELECT * FROM [job].[LT_JobStatus] 
			WHERE
			[LanguageID] = @LanguageID AND
			[JobStatusID] = @JobStatusID				 FOR XML AUTO) as data,
				getdate() 
	 END

	RETURN @Err
END


GO
