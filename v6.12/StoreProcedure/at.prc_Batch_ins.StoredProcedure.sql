SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[at].[prc_Batch_ins]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [at].[prc_Batch_ins] AS' 
END
GO
/*  
	2017-06-13 Steve: Change the length of column ExtID in all related tables from xAPI format
*/
ALTER PROCEDURE [at].[prc_Batch_ins]
(
	@BatchID int = null output,
	@SurveyID int,
	@UserID INT=NULL,
	@DepartmentID INT=NULL,
	@Name nvarchar(256),
	@No smallint,
	@StartDate DATETIME=NULL,
	@EndDate DATETIME=NULL,
	@Status smallint,
	@MailStatus smallint,
	@cUserid int,
	@Log smallint = 1,
	@Created DATETIME = NULL,
	@ExtID nvarchar(256)=''
)
AS
BEGIN
	SET NOCOUNT ON
	DECLARE @Err Int

	SET @Created = ISNULL(@Created, GETDATE())

	INSERT INTO [at].[Batch]
	(
		[SurveyID],
		[UserID],
		[DepartmentID],
		[Name],
		[No],
		[StartDate],
		[EndDate],
		[Status],
		[MailStatus],
		[Created],
		[ExtID]
	)
	VALUES
	(
		@SurveyID,
		@UserID,
		@DepartmentID,
		@Name,
		@No,
		@StartDate,
		@EndDate,
		@Status,
		@MailStatus,
		@Created,
		@ExtID
	)

	Set @Err = @@Error
	Set @BatchID = scope_identity()

	IF @Log = 1 
	BEGIN 
		INSERT INTO [Log].[AuditLog] ( UserId, TableName, Type, Data, Created) 
		SELECT @cUserid,'Batch',0,
		( SELECT * FROM [at].[Batch] 
			WHERE
			[BatchID] = @BatchID				 FOR XML AUTO) as data,
				getdate() 
	 END

	RETURN @Err
END

GO
