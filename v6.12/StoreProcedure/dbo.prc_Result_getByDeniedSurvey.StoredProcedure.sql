SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[prc_Result_getByDeniedSurvey]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [dbo].[prc_Result_getByDeniedSurvey] AS' 
END
GO
/* ------------------------------------------------------------
   PROCEDURE:    prc_Result_getByDeniedSurvey

   Description:  Get results excluding denied surveys
   ------------------------------------------------------------
   2016-10-10 Ray: Remove column Status from org.User, org.Department, RDB.dbo.Result
*/
ALTER PROCEDURE [dbo].[prc_Result_getByDeniedSurvey]
(
	@BatchID		int = NULL,
	@UserID			int = NULL,
	@UserGroupID	int = NULL,
	@DeniedSurveyList varchar(max)=NULL	-- Exclude all denied surveys
)
As
BEGIN
	SET NOCOUNT ON
	DECLARE @Err Int, @SqlCommand varchar(max)=''
    DECLARE @ActiveEntityStatusID VARCHAR(5) = (SELECT EntityStatusID FROM EntityStatus WHERE CodeName='Active')

	SET @SqlCommand ='SELECT
	[ResultID],
	[StartDate],
	[EndDate],
	[RoleID],
	ISNULL([UserID], 0) [UserID],
	ISNULL([UserGroupID], 0) [UserGroupID],
	[SurveyID],
	[BatchID],
	[LanguageID],
	[PageNo],
	[DepartmentID],
	[Anonymous],
	[ShowBack],
	[Email],
	[chk],
	[ResultKey],
	[Created],
	[LastUpdated],
	[LastUpdatedBy],
	ISNULL([StatusTypeID],0) [StatusTypeID],
    EntityStatusID,
    Deleted
	FROM [Result]
	WHERE EntityStatusID = '+@ActiveEntityStatusID+' AND Deleted IS NULL '
	
	IF @BatchID IS NOT NULL AND @UserID IS NULL AND @UserGroupID IS NULL
	BEGIN
		SET @SqlCommand = @SqlCommand + ' AND [BatchID] = @BatchID'
	END
	ELSE IF @UserID IS NOT NULL AND @BatchID Is NULL AND @UserGroupID IS NULL
	BEGIN
		SET @SqlCommand = @SqlCommand + ' AND [UserID] = @UserID'
	END
	ELSE IF @UserGroupID IS NOT NULL AND @BatchID Is NULL AND @UserID IS NULL
	BEGIN
		SET @SqlCommand = @SqlCommand + ' AND [UserGroupID] = @UserGroupID'
	END
	
	IF @DeniedSurveyList IS NOT NULL
	BEGIN
		SET @SqlCommand = @SqlCommand + ' AND SurveyID NOT IN (' + @DeniedSurveyList + ')'
	END
	
	EXECUTE sp_executesql @SqlCommand
	Set @Err = @@Error
	RETURN @Err
END


GO
