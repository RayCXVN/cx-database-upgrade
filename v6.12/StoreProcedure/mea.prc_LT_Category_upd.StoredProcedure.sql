SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[mea].[prc_LT_Category_upd]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [mea].[prc_LT_Category_upd] AS' 
END
GO
ALTER PROCEDURE [mea].[prc_LT_Category_upd]
 (		   
		   @LanguageID int
           ,@CategoryId int
           ,@Name nvarchar(256)
           ,@Description nvarchar(256),
           @cUserid int,  
		   @Log smallint = 1)
AS

BEGIN  
 SET NOCOUNT ON  
 DECLARE @Err Int  
  
 UPDATE [mea].[LT_Category]
SET           
           [Name] = @Name,
           [Description] = @Description
WHERE     
           (
           [LanguageID] = @LanguageID AND  [CategoryId] = @CategoryId      
           )
  
 Set @Err = @@Error  
  
 IF @Log = 1   
 BEGIN   
  INSERT INTO [Log].[AuditLog] ( UserId, TableName, Type, Data, Created)   
  SELECT @cUserid,'LT_Category',1,  
  ( SELECT * FROM  [mea].[LT_Category] 
   WHERE  
   [CategoryId] = @CategoryId     FOR XML AUTO) as data,  
    getdate()   
  END  
  
 RETURN @Err  
END  

GO
