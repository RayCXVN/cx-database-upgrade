SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[form].[prc_FormFieldCondition_ins]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [form].[prc_FormFieldCondition_ins] AS' 
END
GO

ALTER PROCEDURE [form].[prc_FormFieldCondition_ins] (
	@FormFieldConditionID INT = NULL OUTPUT
	,@FormFieldID INT
	,@Type NVARCHAR(32)
	,@Param NVARCHAR(32)
	,@Value NVARCHAR(32)
	,@cUserid INT
	,@Log SMALLINT = 1
	)
AS
BEGIN
	SET NOCOUNT ON;

	DECLARE @Err INT

	INSERT INTO [form].[FormFieldCondition] (
		[FormFieldID]
		,[Type]
		,[Param]
		,[Value]
		)
	VALUES (
		@FormFieldID
		,@Type
		,@Param
		,@Value
		)

	SET @Err = @@Error
	SET @FormFieldConditionID = scope_identity()

	IF @Log = 1
	BEGIN
		INSERT INTO [Log].[AuditLog] (
			UserId
			,TableName
			,Type
			,Data
			,Created
			)
		SELECT @cUserid
			,'FormFieldCondition'
			,0
			,(
				SELECT *
				FROM [form].[FormFieldCondition]
				WHERE [FormFieldConditionID] = @FormFieldConditionID
				FOR XML AUTO
				) AS data
			,getdate()
	END

	RETURN @Err
END

GO
