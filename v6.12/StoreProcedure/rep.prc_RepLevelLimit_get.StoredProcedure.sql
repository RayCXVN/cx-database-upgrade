SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[rep].[prc_RepLevelLimit_get]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [rep].[prc_RepLevelLimit_get] AS' 
END
GO


ALTER PROCEDURE [rep].[prc_RepLevelLimit_get]
(
	@ReportID int = Null,
	@ReportColumnID int = Null
)
AS
BEGIN
	SET NOCOUNT ON
	DECLARE @Err Int

	SELECT
	[RepLevelLimitID],
	ISNULL([ReportColumnID], 0) AS 'ReportColumnID',
	ISNULL([ReportID], 0) AS 'ReportID',
	[MinValue],
	[MaxValue],
	[Sigchange],
	[Fillcolor],
	[BorderColor],
	[Created],
	isnull([LevelGroupID],0) 'LevelGroupID',
	[NegativeTrend]
	FROM [rep].[RepLevelLimit]
	WHERE
	(NOT ReportID IS NULL AND [ReportID] = @ReportID)
	OR (NOT @ReportColumnID IS NULL AND [ReportColumnID] = @ReportColumnID)

	Set @Err = @@Error

	RETURN @Err
END




GO
