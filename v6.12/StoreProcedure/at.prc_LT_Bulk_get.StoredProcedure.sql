SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[at].[prc_LT_Bulk_get]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [at].[prc_LT_Bulk_get] AS' 
END
GO
ALTER PROCEDURE [at].[prc_LT_Bulk_get]
(
	@BulkID int
)
AS
BEGIN
	SET NOCOUNT ON
	DECLARE @Err Int

	SELECT
	[LanguageID],
	[BulkID],
	ISNULL([Name], '') AS 'Name',
	ISNULL([Description], '') AS 'Description',
	ISNULL([ToolTip], '') AS 'ToolTip'
	FROM [at].[LT_Bulk]
	WHERE
	[BulkID] = @BulkID

	Set @Err = @@Error

	RETURN @Err
END



GO
