SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[org].[prc_DT_D_delAll]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [org].[prc_DT_D_delAll] AS' 
END
GO
ALTER PROCEDURE [org].[prc_DT_D_delAll]
(	
	@DepartmentID int
)
As
BEGIN
	SET NOCOUNT ON
	DECLARE @Err Int

	DELETE
	FROM [org].[DT_D]
	WHERE	
	[DepartmentID] = @DepartmentID
	AND DepartmentTypeID > 1
	Set @Err = @@Error

	RETURN @Err
End
GO
