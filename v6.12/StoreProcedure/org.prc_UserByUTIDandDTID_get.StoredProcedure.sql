SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[org].[prc_UserByUTIDandDTID_get]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [org].[prc_UserByUTIDandDTID_get] AS' 
END
GO
/*  
    2017-01-20 Johnny:     Remove column Status from org.Department
*/
ALTER PROCEDURE [org].[prc_UserByUTIDandDTID_get]
(
    @HDID                   int,
    @UsertypeID             int,
    @UsertypeID2            int,
    @DepartmentTypeIDList   nvarchar(max)
)
AS

DECLARE @ActiveEntityStatusID INT = (SELECT EntityStatusID FROM EntityStatus WHERE CodeName='Active')
SELECT  u.[UserID],
        u.[OwnerID],
        u.[DepartmentID],
        u.[LanguageID],
        ISNULL(u.[RoleID], 0) AS 'RoleID',
        u.[UserName],
        u.[Password],
        u.[LastName],
        u.[FirstName],
        u.[Email],
        u.[Mobile],
        u.[ExtID],
        u.[SSN],
        u.[Tag],
        u.[Locked],
        u.[ChangePassword],
        u.[HashPassword],
        u.[SaltPassword],
        u.[OneTimePassword],
        u.[OTPExpireTime],
        u.[Created],
        d.Name as DepartmentName
FROM org.[H_D] hd 
JOIN org.[Department] d ON d.[DepartmentID] = hd.[DepartmentID] and d.EntityStatusID=@ActiveEntityStatusID  AND d.Deleted IS NULL
JOIN org.[User] u ON u.Departmentid = d.Departmentid
JOIN org.[UT_U] utu on utu.userid = u.userid and (utu.usertypeid = @Usertypeid)
JOIN org.[UT_U] utu2 on utu2.userid = u.userid and (utu2.usertypeid = @Usertypeid2)
WHERE hd.PATH LIKE '%\' + cast(@HDID as nvarchar(16)) + '\%'
  AND u.EntityStatusID=@ActiveEntityStatusID AND u.Deleted IS NULL
  AND EXISTS (SELECT 1 FROM [org].[DT_D] dtd WHERE d.[DepartmentID] = dtd.[DepartmentID]
  AND dtd.[DepartmentTypeID] IN (SELECT [Value] FROM dbo.funcListToTableInt(@DepartmentTypeIDList, ',')))
ORDER BY d.Name, u.[FirstName], u.[LastName]

GO
