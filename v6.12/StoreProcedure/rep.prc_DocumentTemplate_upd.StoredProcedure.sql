SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[rep].[prc_DocumentTemplate_upd]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [rep].[prc_DocumentTemplate_upd] AS' 
END
GO



ALTER PROCEDURE [rep].[prc_DocumentTemplate_upd]
(
	@DocumentTemplateID int,
	@DocumentFormatID int,
	@OwnerID int,
	@ActivityID int = null,
	@Template nvarchar(128),
	@CustomerID int = null,
	@cUserid int,
	@Log smallint = 1
)
AS
BEGIN
	SET NOCOUNT ON
	DECLARE @Err Int

	UPDATE [rep].[DocumentTemplate]
	SET
		[DocumentFormatID] = @DocumentFormatID,
		[OwnerID] = @OwnerID,
		[ActivityID] = @ActivityID,
		[Template] = @Template,
		[CustomerID] = @CustomerID
	WHERE
		[DocumentTemplateID] = @DocumentTemplateID

	IF @Log = 1 
	BEGIN 
		INSERT INTO [Log].[AuditLog] ( UserId, TableName, Type, Data, Created) 
		SELECT @cUserid,'DocumentTemplate',1,
		( SELECT * FROM [rep].[DocumentTemplate] 
			WHERE
			[DocumentTemplateID] = @DocumentTemplateID			 FOR XML AUTO) as data,
			getdate()
	END

	Set @Err = @@Error

	RETURN @Err
END




GO
