SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[proc].[prc_ProcessGroup_get]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [proc].[prc_ProcessGroup_get] AS' 
END
GO

ALTER PROCEDURE [proc].[prc_ProcessGroup_get]
(
	@OwnerID int,
	@CustomerID int = null
)
AS
BEGIN
	SET NOCOUNT ON
	DECLARE @Err Int

	SELECT
	[ProcessGroupID],
	[OwnerID],
	ISNULL([CustomerID], 0) AS 'CustomerID',
	[Active],
	[UserID],
	[No],
	[Created]
	FROM [proc].[ProcessGroup]
	WHERE
	[OwnerID] = @OwnerID
	and (@CustomerID is null or CustomerID = @customerid)
	ORDER BY [No]

	Set @Err = @@Error

	RETURN @Err
END



GO
