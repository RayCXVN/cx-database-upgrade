SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[at].[prc_QA_Calc_ins]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [at].[prc_QA_Calc_ins] AS' 
END
GO
ALTER PROCEDURE  [at].[prc_QA_Calc_ins]  
(  
 @QAID int = null output,  
 @QuestionID int,  
 @AlternativeID int,  
 @CFID int,  
 @MinValue float,  
 @MaxValue float,  
 @No smallint,  
 @NewValue float,  
 @UseNewValue smallint,  
 @UseCalculatedValue smallint,  
 @RoleID int = null,  
 @cUserid int,  
 @Log smallint = 1,
 @ItemID int = NULL
)  
AS  
BEGIN  
 SET NOCOUNT ON  
 DECLARE @Err Int  
  
 INSERT INTO [at].[QA_Calc]  
 (  
  [QuestionID],  
  [AlternativeID],  
  [CFID],  
  [MinValue],  
  [MaxValue],  
  [No],  
  [NewValue],  
  [UseNewValue],  
  [UseCalculatedValue],  
  [RoleID],
  [ItemID]  
 )  
 VALUES  
 (  
  @QuestionID,  
  @AlternativeID,  
  @CFID,  
  @MinValue,  
  @MaxValue,  
  @No,  
  @NewValue,  
  @UseNewValue,  
  @UseCalculatedValue,  
  @RoleID,
  @ItemID  
 )  
  
 Set @Err = @@Error  
 Set @QAID = scope_identity()  
  
 IF @Log = 1   
 BEGIN   
  INSERT INTO [Log].[AuditLog] ( UserId, TableName, Type, Data, Created)   
  SELECT @cUserid,'QA_Calc',0,  
  ( SELECT * FROM [at].[QA_Calc]   
   WHERE  
   [QAID] = @QAID     FOR XML AUTO) as data,  
    getdate()   
  END  
  
 RETURN @Err  
END  
  
  
  

GO
