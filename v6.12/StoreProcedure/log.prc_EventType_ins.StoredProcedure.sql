SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[log].[prc_EventType_ins]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [log].[prc_EventType_ins] AS' 
END
GO


ALTER PROCEDURE [log].[prc_EventType_ins]
(
	@EventTypeID	int = NULL output,
	@ParentID		INT=NULL,
	@CodeName		varchar(32),
	@OwnerID		int,
	@No				smallint,
	@cUserid		int,
	@Log			smallint = 1,
	@Active			bit =1
)
AS
BEGIN
	SET NOCOUNT ON
	DECLARE @Err Int

	INSERT INTO [log].[EventType]
	(
		[ParentID],
		[CodeName],
		[OwnerID],
		[No],
		[Active]
	)
	VALUES
	(
		@ParentID,
		@CodeName,
		@OwnerID,
		@No,
		@Active
	)

	Set @Err = @@Error
	Set @EventTypeID = scope_identity()
	
	IF @Log = 1 
	BEGIN 
		INSERT INTO [Log].[AuditLog] ( UserId, TableName, Type, Data, Created) 
		SELECT @cUserid,'EventType',0,
		( SELECT * FROM [log].[EventType]
			WHERE
			[EventTypeID] = @EventTypeID FOR XML AUTO) as data,
				getdate() 
	END

	RETURN @Err
END


GO
