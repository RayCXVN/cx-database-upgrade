SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[app].[prc_SiteViewSetting_upd]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [app].[prc_SiteViewSetting_upd] AS' 
END
GO
ALTER PROCEDURE [app].[prc_SiteViewSetting_upd]  
 @SiteViewSettingID int = null output,  
 @SiteID int,  
 @No int,  
 @NumOfDecimals smallint,  
 @TableIsSelectable bit,  
 @TableIsDefaultSelected bit,  
 @ChartIsSelectable bit,  
 @ChartIsDefaultSelected bit,  
 @AverageIsSelectable bit,  
 @AverageIsDefaultSelected bit,  
 @TrendIsSelectable bit,  
 @TrendIsDefaultSelected bit,  
 @FrequencyIsSelectable bit,  
 @FrequencyIsDefaultSelected bit,  
 @NCountIsSelectable bit,  
 @NCountIsDefaultSelected bit,  
 @StandardDeviationIsSelectable bit,  
 @StandardDeviationIsDefaultSelected bit,  
 @cUserid int,  
 @Log smallint = 1,
 @DefaultMinResultDotted int =0,
 @DefaultMinQuestionCountDotted int =0  
AS  
BEGIN  
 SET NOCOUNT ON;  
 DECLARE @Err Int  
      
    UPDATE [app].[SiteViewSetting]  
    
    SET     
           [No]  = @No 
           ,[NumOfDecimals]  = @NumOfDecimals  
           ,[TableIsSelectable]  = @TableIsSelectable  
           ,[TableIsDefaultSelected]  = @TableIsDefaultSelected  
           ,[ChartIsSelectable]  = @ChartIsSelectable  
           ,[ChartIsDefaultSelected]  = @ChartIsDefaultSelected  
           ,[AverageIsSelectable]   = @AverageIsSelectable  
           ,[AverageIsDefaultSelected]   = @AverageIsDefaultSelected  
           ,[TrendIsSelectable]  = @TrendIsSelectable  
           ,[TrendIsDefaultSelected]  = @TrendIsDefaultSelected  
           ,[FrequencyIsSelectable]  = @FrequencyIsSelectable  
           ,[FrequencyIsDefaultSelected]  = @FrequencyIsDefaultSelected  
           ,[NCountIsSelectable]  = @NCountIsSelectable  
           ,[NCountIsDefaultSelected]  = @NCountIsDefaultSelected  
           ,[StandardDeviationIsSelectable]  = @StandardDeviationIsSelectable  
           ,[StandardDeviationIsDefaultSelected] = @StandardDeviationIsDefaultSelected  
           ,[DefaultMinResultDotted] =@DefaultMinResultDotted
           ,[DefaultMinQuestionCountDotted]=@DefaultMinQuestionCountDotted
     WHERE [SiteID] = @SiteID  
			AND [SiteViewSettingID] = @SiteViewSettingID
             
    Set @Err = @@Error  
    Set @SiteViewSettingID = scope_identity()  
 IF @Log = 1   
 BEGIN   
  INSERT INTO [Log].[AuditLog] ( UserId, TableName, Type, Data, Created)   
  SELECT @cUserid,'SiteViewSetting',0,  
  ( SELECT * FROM [app].[SiteViewSetting]  
   WHERE  
   [SiteViewSettingID] = @SiteViewSettingID     FOR XML AUTO) as data,  
    getdate()   
 END  
   
 RETURN @Err         
END  

GO
