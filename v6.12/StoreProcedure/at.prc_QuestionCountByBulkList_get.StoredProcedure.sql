SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[at].[prc_QuestionCountByBulkList_get]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [at].[prc_QuestionCountByBulkList_get] AS' 
END
GO
/*
	2017-01-10 Ray:  Fix filter questions by Type
*/
ALTER PROCEDURE [at].[prc_QuestionCountByBulkList_get]
(
	@BulkIDs                        varchar(Max),
	@OnlyCountQuestionTypeQuestion  bit = 0
)
AS
BEGIN	
    SELECT BulkID, COUNT(DISTINCT QuestionID) as QuestionCount FROM	
    (
        SELECT bp.BulkID, q.QuestionID
        FROM at.Question q
        JOIN at.B_P bp ON q.PageID = bp.PageID
            AND (@OnlyCountQuestionTypeQuestion=0 OR (@OnlyCountQuestionTypeQuestion=1 AND q.[Type]=0))
        WHERE bp.BulkID IN (select value from dbo.funcListToTableInt(@BulkIDs,','))	
        UNION 
        SELECT bq.BulkID, q.QuestionID
        FROM at.Question q
        JOIN at.B_Q bq ON q.QuestionID = bq.QuestionID
	        AND (@OnlyCountQuestionTypeQuestion=0 OR (@OnlyCountQuestionTypeQuestion=1 AND q.[Type]=0))
        WHERE bq.BulkID IN (select value from dbo.funcListToTableInt(@BulkIDs,','))
    ) AS qt
    GROUP BY BulkID
END

GO
