SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[org].[prc_UserCountInUserGroupByHDID]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [org].[prc_UserCountInUserGroupByHDID] AS' 
END
GO
/*
    Steve - Sept 22 2016 - EntityStatus for table UserGroup
    2018-07-12 Ray:     Replace UG_U by UGMember
*/
ALTER PROCEDURE [org].[prc_UserCountInUserGroupByHDID]
(
    @HDID                   int,
    @PeriodID               int,
    @DepartmentTypeIDList   varchar(max),
    @GroupLevel             int = 1     -- 0: detail, 1: parent level (1 level under @HDID), 2: total
)
AS
BEGIN
    SET NOCOUNT ON
    DECLARE @ActiveEntityStatusID INT = (SELECT EntityStatusID FROM EntityStatus WHERE CodeName='Active')
    
    IF @GroupLevel = 2
    BEGIN
       SELECT @HDID AS UserGroupID, CONVERT(nvarchar(16), @HDID) AS UserGroupName, COUNT(ugm.UserID) AS [UserCount]
       FROM org.UserGroup ug
       JOIN org.[UGMember] ugm ON ugm.[UserGroupID] = ug.[UserGroupID] AND ug.[PeriodID] = @PeriodID
        AND ug.[EntityStatusID] = @ActiveEntityStatusID AND ug.[Deleted] IS NULL
        AND ugm.[EntityStatusID] = @ActiveEntityStatusID AND ugm.[Deleted] IS NULL
       JOIN org.H_D hd ON hd.DepartmentID = ug.DepartmentID AND hd.[Path] like '%\' + CONVERT(nvarchar(16), @HDID) + '\%'
       JOIN org.[User] u ON u.[UserID] = ugm.[UserID] AND u.[EntityStatusID] = @ActiveEntityStatusID AND u.[Deleted] IS NULL
       JOIN org.DT_UG dtug ON dtug.[UserGroupID] = ug.[UserGroupID] AND dtug.[DepartmentTypeID] IN (SELECT Value FROM dbo.funcListToTableInt(@DepartmentTypeIDList,','))
    END
    ELSE
    BEGIN
       SELECT ISNULL(parent_hd.HDID, ug.UserGroupID) AS UserGroupID, ISNULL(parent_hd.Name, ug.Name) AS UserGroupName, COUNT(ugm.UserID) AS [UserCount]
       FROM org.UserGroup ug
       JOIN org.[UGMember] ugm ON ugm.[UserGroupID] = ug.[UserGroupID] AND ug.[PeriodID] = @PeriodID
        AND ug.[EntityStatusID] = @ActiveEntityStatusID AND ug.[Deleted] IS NULL
        AND ugm.[EntityStatusID] = @ActiveEntityStatusID AND ugm.[Deleted] IS NULL
       JOIN org.H_D hd ON hd.DepartmentID = ug.DepartmentID AND hd.[Path] like '%\' + CONVERT(nvarchar(16), @HDID) + '\%'
       JOIN org.[User] u ON u.[UserID] = ugm.[UserID] AND u.[EntityStatusID] = @ActiveEntityStatusID AND u.[Deleted] IS NULL
       JOIN org.DT_UG dtug ON dtug.[UserGroupID] = ug.[UserGroupID] AND dtug.[DepartmentTypeID] IN (SELECT Value From dbo.funcListToTableInt(@DepartmentTypeIDList,','))
       LEFT JOIN (SELECT phd.HDID, phd.[Path], d1.DepartmentID, d1.Name
                  FROM org.H_D phd
                  JOIN org.Department d1 ON d1.DepartmentID = phd.DepartmentID AND phd.ParentID=@HDID) AS parent_hd
              ON hd.[Path] LIKE '%' + [parent_hd].[Path] + '%' AND @GroupLevel = 1
       GROUP BY ISNULL(parent_hd.HDID, ug.UserGroupID), ISNULL(parent_hd.Name, ug.Name)
       ORDER BY ISNULL(parent_hd.Name, ug.Name)
    END
END
GO
