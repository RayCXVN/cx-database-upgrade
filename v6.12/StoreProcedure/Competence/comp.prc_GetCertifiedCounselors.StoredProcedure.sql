USE [production-competence-sg-at6qr]
GO
/****** Object:  StoredProcedure [comp].[prc_GetCertifiedCounselors]    Script Date: 9/22/2017 3:56:42 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Steve Nguyen
-- Create date:     Sept 26th 2016
-- Description:	vip24 statistics / EXEC [comp].[prc_GetCertifiedCounselors] '26', N'2016-01-01', N'2016-09-15', N'18,26'
-- =============================================
CREATE PROCEDURE [comp].[prc_GetCertifiedCounselors]
(
	@HDID NVARCHAR(MAX) = '',
	@FromDate DATETIME = NULL,
	@ToDate DATETIME = NULL,
	@CustomerIDList NVARCHAR(MAX) = ''
)
AS
BEGIN 
	DECLARE @CustomerIDTable TABLE(ID INT)
	IF @CustomerIDList IS NULL
	   INSERT INTO @CustomerIDTable(ID) SELECT CustomerID FROM org.Customer
	ELSE 
	   INSERT INTO @CustomerIDTable(ID) SELECT [Value] FROM dbo.funcListToTableInt(@CustomerIDList, ',')

	DECLARE @EventTypeID INT = 0, @EventKeyID INT = 0
	SELECT @EventTypeID = EventTypeID FROM log.EventType WHERE CodeName = N'LOGIN_ATTEMPT_SUCCESS'
	SELECT @EventKeyID = EventKeyID FROM log.EventKey WHERE CodeName = N'PasswordType' AND EventTypeID = @EventTypeID

	SELECT 
		c.Name AS CustomerName, 
		format(v.Created ,'yyyy-MM-dd hh:mm') as 'Dato',
		u.FirstName + ' ' + u.LastName AS FullName, 
		u.Mobile AS MobileNumber, 
		u.Email AS Email, 
		format(U_L.LastHashLogin ,'yyyy-MM-dd hh:mm') as 'Siste pålogging',
		inst.FirstName + ' ' + inst.LastName AS InstructorFullName, 
		d.Name AS DepartmentName 
	FROM org.[User] u
	JOIN 
		(SELECT e.UserID AS UserID, e.DepartmentID As DepartmentID, e.CustomerID as CustomerID, MAX(e.Created) AS LastHashLogin FROM log.[Event] e
			JOIN log.EventInfo ei on ei.EventID = e.EventID AND ei.EventKeyID = @EventKeyID AND e.EventTypeID = @EventTypeID AND Value = 'Hash'
			AND e.CustomerID IN (SELECT ID FROM @CustomerIDTable)
			JOIN org.UT_U utu ON utu.UserID = e.UserID AND utu.UserTypeID = 38
			WHERE EXISTS (SELECT 1 FROM dbo.AccessGroupMember gm WHERE gm.AccessGroupID IN (13, 1032, 2035) AND gm.CustomerID = e.CustomerID)
			AND EXISTS (SELECT 1 FROM org.H_D hd WHERE (([Path] LIKE '%\' + @HDID + '\%') OR (@HDID IS NULL)) AND hd.DepartmentID = e.DepartmentID)
			GROUP BY e.UserID, e.DepartmentID, e.CustomerID
		) AS U_L ON u.UserID = U_L.UserID
	JOIN org.Customer c ON c.CustomerID = U_L.CustomerID
	JOIN prop.PropValue v ON v.ItemID = u.UserID AND v.PropertyID = 6 
	AND (((@FromDate <= v.Created) OR (@FromDate IS NULL)) AND ((v.Created < @ToDate) OR (@ToDate IS NULL))) 
	JOIN org.[User] inst ON v.CreatedBy = inst.UserID
	JOIN org.Department d ON inst.DepartmentID = d.DepartmentID 
	ORDER BY c.Name
END
GO
