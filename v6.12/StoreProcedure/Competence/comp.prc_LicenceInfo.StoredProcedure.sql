USE [production-competence-sg-at6qr]
GO
/****** Object:  StoredProcedure [comp].[prc_LicenceInfo]    Script Date: 9/22/2017 3:56:42 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- [comp].[prc_LicenceInfo] '26', NULL, '2013-11-01','2013-11-07 23:59:59'

CREATE proc [comp].[prc_LicenceInfo](
@HDID varchar(32) = null,
@CustomerID int = NULL,
@FromDate datetime = NULL,
@ToDate datetime = NULL
)
AS
select IIF(LicenseCustomer.AccessGroupID = 13 , 'Lisenskunde', 'Rammeavtale'), e.EventID, c.Name as 'Kunde', d.Name as 'Avdeling', format(e.Created,'yyyy-MM-dd hh:mm') as 'Dato', ei_userid.Value as 'KandidatID', ei_name.Value as 'Kandidat', (u_Counselor.FirstName + ' ' + u_Counselor.LastName) as 'Veileder', 
CASE WHEN hd.Path like '%\11999\%' THEN 'Fønix' ELSE '' END as 'Partner'
from log.Event e 
join log.EventInfo ei_userid on ei_userid.eventid = e.eventid and ei_userid.EventKeyID = 5 and (e.Created > @FromDate or @FromDate is null) and (e.Created < @ToDate or @ToDate is null) and e.EventTypeID = 3
join log.EventInfo ei_name on ei_name.eventid = e.eventid and ei_name.EventKeyID = 6
join log.EventInfo ei_customerid on ei_customerid.eventid = e.eventid and ei_customerid.EventKeyID = 8 and (ei_customerid.Value = @CustomerID or @CustomerID is null)
join org.[user] u_Counselor on u_Counselor.userid = e.UserID
join org.Department d on d.DepartmentID = u_Counselor.DepartmentID
join org.Customer c on c.CustomerID = ei_customerid.Value
join org.H_D hd on hd.DepartmentID = u_Counselor.DepartmentID and (hd.Path like '%\' + @HDID + '\%' or @HDID is null)
left join dbo.AccessGroupMember LicenseCustomer on LicenseCustomer.AccessGroupID = 13 and LicenseCustomer.CustomerID = c.CustomerID
where 
ei_userid.Value not in (
select ei_userid.Value as 'KandidatID'
from log.Event e 
join log.EventInfo ei_userid on ei_userid.eventid = e.eventid and ei_userid.EventKeyID = 9 and e.EventTypeID = 4
join log.EventInfo ei_name on ei_name.eventid = e.eventid and ei_name.EventKeyID = 10
join log.EventInfo ei_customerid on ei_customerid.eventid = e.eventid and ei_customerid.EventKeyID = 13
join org.[user] u on u.userid = e.UserID
join org.Department d on d.DepartmentID = u.DepartmentID
join org.Customer c on c.CustomerID = ei_customerid.Value
join org.H_D hd on hd.DepartmentID = u.DepartmentID and (hd.Path like '%\' + @HDID + '\%' or @HDID is null)
left join dbo.AccessGroupMember LicenseCustomer on LicenseCustomer.AccessGroupID = 13 and LicenseCustomer.CustomerID = c.CustomerID
)
order by c.name,e.EventID
GO
