USE [production-competence-sg-at6qr]
GO
/****** Object:  StoredProcedure [comp].[ListCareerSuggestionsForProfile]    Script Date: 9/22/2017 3:56:42 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:	Geir Fuhre Pettersen
-- Create date: 30.01.2013
-- Description:	List occupation categories that
-- is suitable for a Competence Interest profile and
-- a specified level of competency (education).
-- =============================================
-- exec [comp].[ListCareerSuggestionsForProfile] 1, 'I','E','S', 0
-- exec [comp].[ListCareerSuggestionsForProfile] 1, 'R','R','S', 0
CREATE PROCEDURE [comp].[ListCareerSuggestionsForProfile] 
	@languageId int, -- cxStudio language ID
	@profile1 char(1) = NULL, -- 1. priority letter for the interest profile, can be NULL, ' ', or one of these chars: RIASEC
	@profile2 char(1) = NULL, -- 2. priority letter for the interest profile, can be NULL, ' ', or one of these chars: RIASEC
	@profile3 char(1) = NULL, -- 3. priority letter for the interest profile, can be NULL, ' ', or one of these chars: RIASEC
	@competencyLevel char(1) = NULL -- a number indicating the level of education needed, can be NULL, ' ', or one of these chars: 0123 (3=University)
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

	IF @profile1 = 'R'
	BEGIN
		SELECT cc.[CareerCategoryID], cc.[No], ltcc.[Name] [CategoryName], ltcc.[Description], cg.[CareerGuideId], lt.[Title], cg.[URL]
		FROM comp.CareerGuide cg JOIN comp.LT_CareerGuide lt ON cg.CareerGuideId = lt.CareerGuideId AND lt.LanguageID = @languageId
		JOIN [comp].[CareerCategory] cc ON cg.[CareerCategoryID] = cc.[CareerCategoryID]
        JOIN [comp].[LT_CareerCategory] ltcc ON cc.[CareerCategoryID] = ltcc.[CareerCategoryID] AND ltcc.[LanguageID] = @languageId
		WHERE
		 -- There must be defined some educational level(s) on the row
		((cg.Comp_None + cg.Comp_HighSchool + cg.Comp_College + cg.Comp_University) > 0)
		 -- The educational level on the row must match the one in the parameter
		AND (@competencyLevel IS NULL OR @competencyLevel = ' ' OR ((@competencyLevel = '0' AND cg.Comp_None >= 1) OR (@competencyLevel = '1' AND cg.Comp_HighSchool >= 1) OR (@competencyLevel = '2' AND cg.Comp_College >= 1) OR (@competencyLevel = '3' AND cg.Comp_University >= 1)))
		-- R is at number 1 or R is at number 2 and there is no other RIASEC with the value 1 or R is at value 3 and there is zero or only one other with value 2
		AND (cg.R = 1
			OR (cg.R = 2 AND NOT COALESCE(cg.R,0) = 1 AND NOT COALESCE(cg.I,0) = 1 AND NOT COALESCE(cg.A,0) = 1 AND NOT COALESCE(cg.S,0) = 1 AND NOT COALESCE(cg.E,0) = 1 AND NOT COALESCE(cg.C,0) = 1)
			OR (cg.R = 3 AND cg.num_of_1values = 0 AND cg.num_of_2values IN (0,1))
			)
		AND (@profile2 IS NULL OR @profile2 = ' '
				OR ((@profile2 = 'R' AND cg.R IN (1,2)) OR (@profile2 = 'I' AND cg.I IN (1,2)) OR (@profile2 = 'A' AND cg.A IN (1,2)) OR (@profile2 = 'S' AND cg.S IN (1,2)) OR (@profile2 = 'E' AND cg.E IN (1,2)) OR (@profile2 = 'C' AND cg.C IN (1,2)))
				OR ((cg.num_of_2values = 0) AND ((@profile2 = 'R' AND cg.R = 3) OR (@profile2 = 'I' AND cg.I = 3) OR (@profile2 = 'A' AND cg.A = 3) OR (@profile2 = 'S' AND cg.S = 3) OR (@profile2 = 'E' AND cg.E = 3) OR (@profile2 = 'C' AND cg.C = 3)))
				OR ((cg.num_of_2values = 1) AND  (@profile1 = 'R' AND cg.R = 2)   AND ((@profile2 = 'R' AND cg.R = 3) OR (@profile2 = 'I' AND cg.I = 3) OR (@profile2 = 'A' AND cg.A = 3) OR (@profile2 = 'S' AND cg.S = 3) OR (@profile2 = 'E' AND cg.E = 3) OR (@profile2 = 'C' AND cg.C = 3)))
			)
		AND (@profile3 IS NULL OR @profile3 = ' ' OR ((@profile3 = 'R' AND cg.R IN (1,2,3,4)) OR (@profile3 = 'I' AND cg.I IN (1,2,3,4)) OR (@profile3 = 'A' AND cg.A IN (1,2,3,4)) OR (@profile3 = 'S' AND cg.S IN (1,2,3,4)) OR (@profile3 = 'E' AND cg.E IN (1,2,3,4)) OR (@profile3 = 'C' AND cg.C IN (1,2,3,4))))
		ORDER BY cc.[No], lt.[Title]
	END
	
	ELSE IF @profile1 = 'I'
	BEGIN
		SELECT cc.[CareerCategoryID], cc.[No], ltcc.[Name] [CategoryName], ltcc.[Description], cg.[CareerGuideId], lt.[Title], cg.[URL]
		FROM comp.CareerGuide cg JOIN comp.LT_CareerGuide lt ON cg.CareerGuideId = lt.CareerGuideId AND lt.LanguageID = @languageId
		JOIN [comp].[CareerCategory] cc ON cg.[CareerCategoryID] = cc.[CareerCategoryID]
        JOIN [comp].[LT_CareerCategory] ltcc ON cc.[CareerCategoryID] = ltcc.[CareerCategoryID] AND ltcc.[LanguageID] = @languageId
		WHERE
		((cg.Comp_None + cg.Comp_HighSchool + cg.Comp_College + cg.Comp_University) > 0)
		AND (@competencyLevel IS NULL OR @competencyLevel = ' ' OR ((@competencyLevel = '0' AND cg.Comp_None >= 1) OR (@competencyLevel = '1' AND cg.Comp_HighSchool >= 1) OR (@competencyLevel = '2' AND cg.Comp_College >= 1) OR (@competencyLevel = '3' AND cg.Comp_University >= 1)))
		AND (cg.I = 1
			OR (cg.I = 2 AND NOT COALESCE(cg.R,0) = 1 AND NOT COALESCE(cg.I,0) = 1 AND NOT COALESCE(cg.A,0) = 1 AND NOT COALESCE(cg.S,0) = 1 AND NOT COALESCE(cg.E,0) = 1 AND NOT COALESCE(cg.C,0) = 1)
			OR (cg.I = 3 AND cg.num_of_1values = 0 AND cg.num_of_2values IN (0,1))
			)
		AND (@profile2 IS NULL OR @profile2 = ' '
				OR ((@profile2 = 'R' AND cg.R IN (1,2)) OR (@profile2 = 'I' AND cg.I IN (1,2)) OR (@profile2 = 'A' AND cg.A IN (1,2)) OR (@profile2 = 'S' AND cg.S IN (1,2)) OR (@profile2 = 'E' AND cg.E IN (1,2)) OR (@profile2 = 'C' AND cg.C IN (1,2)))
				OR ((cg.R <> 2 AND cg.I <> 2 AND cg.A <> 2 AND cg.S <> 2 AND cg.E <> 2 AND cg.C <> 2) AND ((@profile2 = 'R' AND cg.R = 3) OR (@profile2 = 'I' AND cg.I = 3) OR (@profile2 = 'A' AND cg.A = 3) OR (@profile2 = 'S' AND cg.S = 3) OR (@profile2 = 'E' AND cg.E = 3) OR (@profile2 = 'C' AND cg.C = 3)))
				OR ((cg.num_of_2values = 1) AND  (@profile1 = 'I' AND cg.I = 2)   AND ((@profile2 = 'R' AND cg.R = 3) OR (@profile2 = 'I' AND cg.I = 3) OR (@profile2 = 'A' AND cg.A = 3) OR (@profile2 = 'S' AND cg.S = 3) OR (@profile2 = 'E' AND cg.E = 3) OR (@profile2 = 'C' AND cg.C = 3)))
			)
		AND (@profile3 IS NULL OR @profile3 = ' ' OR ((@profile3 = 'R' AND cg.R IN (1,2,3,4)) OR (@profile3 = 'I' AND cg.I IN (1,2,3,4)) OR (@profile3 = 'A' AND cg.A IN (1,2,3,4)) OR (@profile3 = 'S' AND cg.S IN (1,2,3,4)) OR (@profile3 = 'E' AND cg.E IN (1,2,3,4)) OR (@profile3 = 'C' AND cg.C IN (1,2,3,4))))
		ORDER BY cc.[No], lt.[Title]
	END
	
	ELSE IF @profile1 = 'A'
	BEGIN
		SELECT cc.[CareerCategoryID], cc.[No], ltcc.[Name] [CategoryName], ltcc.[Description], cg.[CareerGuideId], lt.[Title], cg.[URL]
		FROM comp.CareerGuide cg JOIN comp.LT_CareerGuide lt ON cg.CareerGuideId = lt.CareerGuideId AND lt.LanguageID = @languageId
		JOIN [comp].[CareerCategory] cc ON cg.[CareerCategoryID] = cc.[CareerCategoryID]
        JOIN [comp].[LT_CareerCategory] ltcc ON cc.[CareerCategoryID] = ltcc.[CareerCategoryID] AND ltcc.[LanguageID] = @languageId
		WHERE
		((cg.Comp_None + cg.Comp_HighSchool + cg.Comp_College + cg.Comp_University) > 0)
		AND (@competencyLevel IS NULL OR @competencyLevel = ' ' OR ((@competencyLevel = '0' AND cg.Comp_None >= 1) OR (@competencyLevel = '1' AND cg.Comp_HighSchool >= 1) OR (@competencyLevel = '2' AND cg.Comp_College >= 1) OR (@competencyLevel = '3' AND cg.Comp_University >= 1)))
		AND (cg.A = 1
			OR (cg.A = 2 AND NOT COALESCE(cg.R,0) = 1 AND NOT COALESCE(cg.I,0) = 1 AND NOT COALESCE(cg.A,0) = 1 AND NOT COALESCE(cg.S,0) = 1 AND NOT COALESCE(cg.E,0) = 1 AND NOT COALESCE(cg.C,0) = 1)
			OR (cg.A = 3 AND cg.num_of_1values = 0 AND cg.num_of_2values IN (0,1))
			)
		AND (@profile2 IS NULL OR @profile2 = ' '
				OR ((@profile2 = 'R' AND cg.R IN (1,2)) OR (@profile2 = 'I' AND cg.I IN (1,2)) OR (@profile2 = 'A' AND cg.A IN (1,2)) OR (@profile2 = 'S' AND cg.S IN (1,2)) OR (@profile2 = 'E' AND cg.E IN (1,2)) OR (@profile2 = 'C' AND cg.C IN (1,2)))
				OR ((cg.R <> 2 AND cg.I <> 2 AND cg.A <> 2 AND cg.S <> 2 AND cg.E <> 2 AND cg.C <> 2) AND ((@profile2 = 'R' AND cg.R = 3) OR (@profile2 = 'I' AND cg.I = 3) OR (@profile2 = 'A' AND cg.A = 3) OR (@profile2 = 'S' AND cg.S = 3) OR (@profile2 = 'E' AND cg.E = 3) OR (@profile2 = 'C' AND cg.C = 3)))
				OR ((cg.num_of_2values = 1) AND  (@profile1 = 'A' AND cg.A = 2)   AND ((@profile2 = 'R' AND cg.R = 3) OR (@profile2 = 'I' AND cg.I = 3) OR (@profile2 = 'A' AND cg.A = 3) OR (@profile2 = 'S' AND cg.S = 3) OR (@profile2 = 'E' AND cg.E = 3) OR (@profile2 = 'C' AND cg.C = 3)))
			)
		AND (@profile3 IS NULL OR @profile3 = ' ' OR ((@profile3 = 'R' AND cg.R IN (1,2,3,4)) OR (@profile3 = 'I' AND cg.I IN (1,2,3,4)) OR (@profile3 = 'A' AND cg.A IN (1,2,3,4)) OR (@profile3 = 'S' AND cg.S IN (1,2,3,4)) OR (@profile3 = 'E' AND cg.E IN (1,2,3,4)) OR (@profile3 = 'C' AND cg.C IN (1,2,3,4))))
		ORDER BY cc.[No], lt.[Title]
	END
	
	ELSE IF @profile1 = 'S'
	BEGIN
		SELECT cc.[CareerCategoryID], cc.[No], ltcc.[Name] [CategoryName], ltcc.[Description], cg.[CareerGuideId], lt.[Title], cg.[URL]
		FROM comp.CareerGuide cg JOIN comp.LT_CareerGuide lt ON cg.CareerGuideId = lt.CareerGuideId AND lt.LanguageID = @languageId
		JOIN [comp].[CareerCategory] cc ON cg.[CareerCategoryID] = cc.[CareerCategoryID]
        JOIN [comp].[LT_CareerCategory] ltcc ON cc.[CareerCategoryID] = ltcc.[CareerCategoryID] AND ltcc.[LanguageID] = @languageId
		WHERE
		((cg.Comp_None + cg.Comp_HighSchool + cg.Comp_College + cg.Comp_University) > 0)
		AND (@competencyLevel IS NULL OR @competencyLevel = ' ' OR ((@competencyLevel = '0' AND cg.Comp_None >= 1) OR (@competencyLevel = '1' AND cg.Comp_HighSchool >= 1) OR (@competencyLevel = '2' AND cg.Comp_College >= 1) OR (@competencyLevel = '3' AND cg.Comp_University >= 1)))
		AND (cg.S = 1
			OR (cg.S = 2 AND NOT COALESCE(cg.R,0) = 1 AND NOT COALESCE(cg.I,0) = 1 AND NOT COALESCE(cg.A,0) = 1 AND NOT COALESCE(cg.S,0) = 1 AND NOT COALESCE(cg.E,0) = 1 AND NOT COALESCE(cg.C,0) = 1)
			OR (cg.S = 3 AND cg.num_of_1values = 0 AND cg.num_of_2values IN (0,1))
			)
		AND (@profile2 IS NULL OR @profile2 = ' '
				OR ((@profile2 = 'R' AND cg.R IN (1,2)) OR (@profile2 = 'I' AND cg.I IN (1,2)) OR (@profile2 = 'A' AND cg.A IN (1,2)) OR (@profile2 = 'S' AND cg.S IN (1,2)) OR (@profile2 = 'E' AND cg.E IN (1,2)) OR (@profile2 = 'C' AND cg.C IN (1,2)))
				OR ((cg.R <> 2 AND cg.I <> 2 AND cg.A <> 2 AND cg.S <> 2 AND cg.E <> 2 AND cg.C <> 2) AND ((@profile2 = 'R' AND cg.R = 3) OR (@profile2 = 'I' AND cg.I = 3) OR (@profile2 = 'A' AND cg.A = 3) OR (@profile2 = 'S' AND cg.S = 3) OR (@profile2 = 'E' AND cg.E = 3) OR (@profile2 = 'C' AND cg.C = 3)))
				OR ((cg.num_of_2values = 1) AND  (@profile1 = 'S' AND cg.S = 2)   AND ((@profile2 = 'R' AND cg.R = 3) OR (@profile2 = 'I' AND cg.I = 3) OR (@profile2 = 'A' AND cg.A = 3) OR (@profile2 = 'S' AND cg.S = 3) OR (@profile2 = 'E' AND cg.E = 3) OR (@profile2 = 'C' AND cg.C = 3)))
			)
		AND (@profile3 IS NULL OR @profile3 = ' ' OR ((@profile3 = 'R' AND cg.R IN (1,2,3,4)) OR (@profile3 = 'I' AND cg.I IN (1,2,3,4)) OR (@profile3 = 'A' AND cg.A IN (1,2,3,4)) OR (@profile3 = 'S' AND cg.S IN (1,2,3,4)) OR (@profile3 = 'E' AND cg.E IN (1,2,3,4)) OR (@profile3 = 'C' AND cg.C IN (1,2,3,4))))
		ORDER BY cc.[No], lt.[Title]
	END
	
	ELSE IF @profile1 = 'E'
	BEGIN
		SELECT cc.[CareerCategoryID], cc.[No], ltcc.[Name] [CategoryName], ltcc.[Description], cg.[CareerGuideId], lt.[Title], cg.[URL]
		FROM comp.CareerGuide cg JOIN comp.LT_CareerGuide lt ON cg.CareerGuideId = lt.CareerGuideId AND lt.LanguageID = @languageId
		JOIN [comp].[CareerCategory] cc ON cg.[CareerCategoryID] = cc.[CareerCategoryID]
        JOIN [comp].[LT_CareerCategory] ltcc ON cc.[CareerCategoryID] = ltcc.[CareerCategoryID] AND ltcc.[LanguageID] = @languageId
		WHERE
		((cg.Comp_None + cg.Comp_HighSchool + cg.Comp_College + cg.Comp_University) > 0)
		AND (@competencyLevel IS NULL OR @competencyLevel = ' ' OR ((@competencyLevel = '0' AND cg.Comp_None >= 1) OR (@competencyLevel = '1' AND cg.Comp_HighSchool >= 1) OR (@competencyLevel = '2' AND cg.Comp_College >= 1) OR (@competencyLevel = '3' AND cg.Comp_University >= 1)))
		AND (cg.E = 1
			OR (cg.E = 2 AND NOT COALESCE(cg.R,0) = 1 AND NOT COALESCE(cg.I,0) = 1 AND NOT COALESCE(cg.A,0) = 1 AND NOT COALESCE(cg.S,0) = 1 AND NOT COALESCE(cg.E,0) = 1 AND NOT COALESCE(cg.C,0) = 1)
			OR (cg.E = 3 AND cg.num_of_1values = 0 AND cg.num_of_2values IN (0,1))
			)
		AND (@profile2 IS NULL OR @profile2 = ' '
				OR ((@profile2 = 'R' AND cg.R IN (1,2)) OR (@profile2 = 'I' AND cg.I IN (1,2)) OR (@profile2 = 'A' AND cg.A IN (1,2)) OR (@profile2 = 'S' AND cg.S IN (1,2)) OR (@profile2 = 'E' AND cg.E IN (1,2)) OR (@profile2 = 'C' AND cg.C IN (1,2)))
				OR ((cg.R <> 2 AND cg.I <> 2 AND cg.A <> 2 AND cg.S <> 2 AND cg.E <> 2 AND cg.C <> 2) AND ((@profile2 = 'R' AND cg.R = 3) OR (@profile2 = 'I' AND cg.I = 3) OR (@profile2 = 'A' AND cg.A = 3) OR (@profile2 = 'S' AND cg.S = 3) OR (@profile2 = 'E' AND cg.E = 3) OR (@profile2 = 'C' AND cg.C = 3)))
				OR ((cg.num_of_2values = 1) AND  (@profile1 = 'E' AND cg.E = 2)   AND ((@profile2 = 'R' AND cg.R = 3) OR (@profile2 = 'I' AND cg.I = 3) OR (@profile2 = 'A' AND cg.A = 3) OR (@profile2 = 'S' AND cg.S = 3) OR (@profile2 = 'E' AND cg.E = 3) OR (@profile2 = 'C' AND cg.C = 3)))
			)
		AND (@profile3 IS NULL OR @profile3 = ' ' OR ((@profile3 = 'R' AND cg.R IN (1,2,3,4)) OR (@profile3 = 'I' AND cg.I IN (1,2,3,4)) OR (@profile3 = 'A' AND cg.A IN (1,2,3,4)) OR (@profile3 = 'S' AND cg.S IN (1,2,3,4)) OR (@profile3 = 'E' AND cg.E IN (1,2,3,4)) OR (@profile3 = 'C' AND cg.C IN (1,2,3,4))))
		ORDER BY cc.[No], lt.[Title]
	END
	
	ELSE IF @profile1 = 'C'
	BEGIN
		SELECT cc.[CareerCategoryID], cc.[No], ltcc.[Name] [CategoryName], ltcc.[Description], cg.[CareerGuideId], lt.[Title], cg.[URL]
		FROM comp.CareerGuide cg JOIN comp.LT_CareerGuide lt ON cg.CareerGuideId = lt.CareerGuideId AND lt.LanguageID = @languageId
		JOIN [comp].[CareerCategory] cc ON cg.[CareerCategoryID] = cc.[CareerCategoryID]
        JOIN [comp].[LT_CareerCategory] ltcc ON cc.[CareerCategoryID] = ltcc.[CareerCategoryID] AND ltcc.[LanguageID] = @languageId
		WHERE
		((cg.Comp_None + cg.Comp_HighSchool + cg.Comp_College + cg.Comp_University) > 0)
		AND (@competencyLevel IS NULL OR @competencyLevel = ' ' OR ((@competencyLevel = '0' AND cg.Comp_None >= 1) OR (@competencyLevel = '1' AND cg.Comp_HighSchool >= 1) OR (@competencyLevel = '2' AND cg.Comp_College >= 1) OR (@competencyLevel = '3' AND cg.Comp_University >= 1)))
		AND (cg.C = 1
			OR (cg.C = 2 AND NOT COALESCE(cg.R,0) = 1 AND NOT COALESCE(cg.I,0) = 1 AND NOT COALESCE(cg.A,0) = 1 AND NOT COALESCE(cg.S,0) = 1 AND NOT COALESCE(cg.E,0) = 1 AND NOT COALESCE(cg.C,0) = 1)
			OR (cg.C = 3 AND cg.num_of_1values = 0 AND cg.num_of_2values IN (0,1))
			)
		AND (@profile2 IS NULL OR @profile2 = ' '
				OR ((@profile2 = 'R' AND cg.R IN (1,2)) OR (@profile2 = 'I' AND cg.I IN (1,2)) OR (@profile2 = 'A' AND cg.A IN (1,2)) OR (@profile2 = 'S' AND cg.S IN (1,2)) OR (@profile2 = 'E' AND cg.E IN (1,2)) OR (@profile2 = 'C' AND cg.C IN (1,2)))
				OR ((cg.R <> 2 AND cg.I <> 2 AND cg.A <> 2 AND cg.S <> 2 AND cg.E <> 2 AND cg.C <> 2) AND ((@profile2 = 'R' AND cg.R = 3) OR (@profile2 = 'I' AND cg.I = 3) OR (@profile2 = 'A' AND cg.A = 3) OR (@profile2 = 'S' AND cg.S = 3) OR (@profile2 = 'E' AND cg.E = 3) OR (@profile2 = 'C' AND cg.C = 3)))
				OR ((cg.num_of_2values = 1) AND  (@profile1 = 'C' AND cg.C = 2)   AND ((@profile2 = 'R' AND cg.R = 3) OR (@profile2 = 'I' AND cg.I = 3) OR (@profile2 = 'A' AND cg.A = 3) OR (@profile2 = 'S' AND cg.S = 3) OR (@profile2 = 'E' AND cg.E = 3) OR (@profile2 = 'C' AND cg.C = 3)))
			)
		AND (@profile3 IS NULL OR @profile3 = ' ' OR ((@profile3 = 'R' AND cg.R IN (1,2,3,4)) OR (@profile3 = 'I' AND cg.I IN (1,2,3,4)) OR (@profile3 = 'A' AND cg.A IN (1,2,3,4)) OR (@profile3 = 'S' AND cg.S IN (1,2,3,4)) OR (@profile3 = 'E' AND cg.E IN (1,2,3,4)) OR (@profile3 = 'C' AND cg.C IN (1,2,3,4))))
		ORDER BY cc.[No], lt.[Title]
	END

END
GO
