SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[comp].[prc_GetCandidates]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [comp].[prc_GetCandidates] AS' 
END
GO
/*  EXEC [comp].[prc_GetCandidates] '26', N'2016-01-01', N'2016-09-15', N'18,26'
    
    2018-07-13 Ray:     Replace UG_U by UGMember
*/
ALTER PROCEDURE [comp].[prc_GetCandidates](
    @HDID           nvarchar(max) = '',
    @FromDate       datetime      = NULL,
    @ToDate         datetime      = NULL,
    @CustomerIDList nvarchar(max) = ''
) AS
BEGIN
    DECLARE @EntityStatusID_Active int = 0;
    SELECT @EntityStatusID_Active = EntityStatusID FROM dbo.EntityStatus WHERE CodeName = N'Active';

    DECLARE @CustomerIDTable TABLE ([ID] int);
    IF @CustomerIDList IS NULL
    BEGIN
        INSERT INTO @CustomerIDTable ([ID])
        SELECT [CustomerID] FROM [org].[Customer];
    END
    ELSE
    BEGIN
        INSERT INTO @CustomerIDTable ([ID])
        SELECT [Value] FROM [dbo].[funcListToTableInt] (@CustomerIDList, ',');
    END;

    DECLARE @EventTypeID int, @EventKeyID int;
    SELECT @EventTypeID = [EventTypeID] FROM log.[EventType] WHERE [CodeName] = N'LICENSE_USED';
    SELECT @EventKeyID = [EventKeyID] FROM log.[EventKey] WHERE [CodeName] = N'CandidateID' AND [EventTypeID] = @EventTypeID;

    SELECT [c].[Name] AS [CustomerName], format([ev].[CreatedDate], 'yyyy-MM-dd hh:mm') AS 'Dato', [u].[FirstName]+' '+[u].[LastName] AS [FullName],
           [u].[Mobile] AS [MobileNumber], [u].[Email] AS [Email], [coun].[FirstName]+' '+[coun].[LastName] AS [CounselorFullName], [d].[Name] AS [DepartmentName]
    FROM [org].[User] [u]
    JOIN (SELECT [utu].[UserID] AS [CandidateID], [e].[DepartmentID] AS [DepartmentID], [e].[CustomerID] AS [CustomerID], [e].[Created] AS [CreatedDate]
          FROM log.[Event] [e]
          JOIN log.[EventInfo] [ei] ON [ei].[EventID] = [e].[EventID] AND [e].[EventTypeID] = @EventTypeID AND [ei].[EventKeyID] = @EventKeyID
           AND ((@FromDate <= [e].[Created] OR @FromDate IS NULL)
                AND ([e].[Created] < @ToDate OR @ToDate IS NULL)
               )
           AND [e].[CustomerID] IN (SELECT [ID] FROM @CustomerIDTable)
          JOIN [org].[UT_U] [utu] ON [ei].Value = CONVERT( nvarchar(32), [utu].[UserID]) AND [utu].[UserTypeID] = 39
          WHERE EXISTS (SELECT 1 FROM [dbo].[AccessGroupMember] [gm] WHERE [gm].[AccessGroupID] IN (13, 1032, 2035) AND [gm].[CustomerID] = [e].[CustomerID])
            AND EXISTS (SELECT 2 FROM [org].[H_D] [hd] WHERE [hd].[DepartmentID] = [e].[DepartmentID]
                                                         AND (@HDID IS NULL OR [Path] LIKE '%\'+ @HDID + '\%')
                       )
         ) AS [ev] ON [u].[UserID] = [ev].[CandidateID] AND u.[EntityStatusID] = @EntityStatusID_Active AND u.[Deleted] IS NULL
    JOIN [org].[Customer] [c] ON [c].[CustomerID] = [ev].[CustomerID]
    JOIN [org].[UGMember] [ugm] ON [u].[UserID] = [ugm].[UserID] AND ugm.[EntityStatusID] = @EntityStatusID_Active AND ugm.[Deleted] IS NULL
    JOIN [org].[UserGroup] [ug] ON [ug].[UserGroupID] = [ugm].[UserGroupID] AND ug.[EntityStatusID] = @EntityStatusID_Active AND ug.[Deleted] IS NULL
    JOIN [org].[User] [coun] ON [coun].[UserID] = [ug].[UserID]
    JOIN [org].[Department] [d] ON [d].[DepartmentID] = [coun].[DepartmentID] AND d.[EntityStatusID] = @EntityStatusID_Active AND d.[Deleted] IS NULL
    ORDER BY [c].[Name], [ev].[CreatedDate];
END;
GO