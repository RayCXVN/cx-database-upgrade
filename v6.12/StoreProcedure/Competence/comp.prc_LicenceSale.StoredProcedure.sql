USE [production-competence-sg-at6qr]
GO
/****** Object:  StoredProcedure [comp].[prc_LicenceSale]    Script Date: 9/22/2017 3:56:42 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- [comp].prc_LicenceSale NULL, '2013-11-01','2013-11-07 23:59:59'

CREATE proc [comp].[prc_LicenceSale](
@CustomerID int = NULL,
@FromDate datetime = NULL,
@ToDate datetime = NULL
)
AS
select e.EventID, c.CustomerID as 'KundeId', c.Name as 'Kunde', e.Created as 'Dato', ei_package.Value as 'Antall',(u.FirstName + ' ' + u.LastName) as 'Kjøper' 
from log.Event e 
join log.EventInfo ei_customerid on ei_customerid.eventid = e.eventid and ei_customerid.EventKeyID = 1 and (e.Created > @FromDate or @FromDate is null) and (e.Created < @ToDate or @ToDate is null) 
join log.EventInfo ei_package on ei_package.eventid = e.eventid and ei_package.EventKeyID = 3
join org.[user] u on u.userid = e.UserID
join org.Department d on d.DepartmentID = u.DepartmentID
join org.Customer c on c.CustomerID = ei_customerid.Value  and (ei_customerid.Value = @CustomerID or @CustomerID is null)
join org.H_D hd on hd.DepartmentID = d.DepartmentID and hd.Path like '%\26\%'
where e.EventTypeID in (2)
order by e.EventID
GO
