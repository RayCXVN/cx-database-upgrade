USE [production-competence-sg-at6qr]
GO
/****** Object:  StoredProcedure [comp].[prc_GetNumberOfLogins]    Script Date: 9/22/2017 3:56:42 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Steve Nguyen
-- Create date:     Sept 15th 2016
-- Description:	vip24 statistics / EXEC [comp].[prc_GetNumberOfLogins] '26', N'2016-01-01', N'2016-09-15', N'18,26'
-- =============================================
CREATE PROCEDURE [comp].[prc_GetNumberOfLogins]
(
	@HDID NVARCHAR(MAX) = '',
	@FromDate DATETIME = NULL,
	@ToDate DATETIME = NULL,
	@CustomerIDList NVARCHAR(MAX) = ''
)
AS
BEGIN 
	DECLARE @CustomerIDTable TABLE(ID INT, Name NVARCHAR(MAX))
	IF @CustomerIDList IS NULL
	   INSERT INTO @CustomerIDTable(ID, Name) SELECT CustomerID, Name FROM org.Customer 
	ELSE 
	   INSERT INTO @CustomerIDTable(ID, Name) SELECT t.[Value], c.Name FROM dbo.funcListToTableInt(@CustomerIDList, ',') t JOIN org.Customer c ON t.Value = c.CustomerID

	SELECT 
	   t.ID, 
	   t.Name, 
	   CASE WHEN e.CustomerID IS NULL THEN 0 
		   ELSE 1
	   END AS Number
     INTO #NumberOfLogins
	   FROM log.Event e
	JOIN log.EventInfo ei ON e.EventID = ei.EventID AND e.EventTypeID = 6 AND ei.EventKeyID = 2037 AND ei.Value = 'Hash' 	
	AND (((@FromDate <= e.Created) OR (@FromDate IS NULL)) AND ((e.Created < @ToDate) OR (@ToDate IS NULL)))	
	RIGHT JOIN @CustomerIDTable t ON e.CustomerID = t.ID
	WHERE EXISTS (SELECT 1 FROM dbo.AccessGroupMember gm WHERE gm.AccessGroupID IN (13, 1032, 2035) AND gm.CustomerID = e.CustomerID)
	AND EXISTS (SELECT 1 FROM org.H_D hd WHERE (([Path] LIKE '%\' + @HDID + '\%') OR (@HDID IS NULL)) AND hd.DepartmentID = e.DepartmentID)
	AND EXISTS (SELECT 1 FROM org.Department d JOIN @CustomerIDTable tt ON d.CustomerID = tt.ID
				JOIN org.[User] u ON d.DepartmentID = u.DepartmentID
				JOIN org.UT_U utu ON u.UserID = utu.UserID AND utu.UserTypeID IN (38,39,41))
	
	-- SELECT * FROM #NumberOfLogins WHERE Number IN (0,1) ORDER BY Number ASC
	SELECT ID, Name, SUM(Number) AS Logins
	FROM #NumberOfLogins
	GROUP BY ID, Name
	ORDER BY Logins DESC

	DROP TABLE #NumberOfLogins
END
GO
