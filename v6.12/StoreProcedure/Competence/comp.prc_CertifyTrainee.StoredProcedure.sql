USE [production-competence-sg-at6qr]
GO
/****** Object:  StoredProcedure [comp].[prc_CertifyTrainee]    Script Date: 9/22/2017 3:56:42 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-------------------------------------------------------------------------------------------------------------------
-- DEV: Ray
-- Task 23553: vip24 - Create a script to manually certify trainees and keep specific testcandidates
CREATE PROCEDURE [comp].[prc_CertifyTrainee]
(   @CustomerID                 int,
    @InstructorUserID           int,
    @TraineeUserID              int,
    @KeepTestCandidateIDList    varchar(max) = ''
) AS
BEGIN
    SET NOCOUNT ON
    SET XACT_ABORT ON
    DECLARE @OwnerID int = 15, @AG_LicenseControl int = 13, @UT_Trainee int = 37, @UT_TestCandidate int = 40, @TableType_User int = 13, @SystemUserID int = 1,
            @PROP_AvailableLicense int = 2, @PROP_UsedLicense int = 1, @PROP_InstructorUserID int = 6,
            @IsLicenseControlled bit, @AvailableLicense int, @CandidateCount int, @TraineeDepartmentID int, @DeletedRowCount int, @CandidateID int,
            @ET_CertifyTrainee int, @ET_LicenseUsed int, @EventID int, @UserData xml, @Msg nvarchar(max),
            @FullName nvarchar(256), @DepartmentName nvarchar(256), @Username nvarchar(128), @PathName nvarchar(max), @CandidateName nvarchar(256), @CandidateUsername nvarchar(128)

    DECLARE @CandidateIDTable TABLE (CandidateID int, FullName nvarchar(256), Username nvarchar(128))

    SELECT @IsLicenseControlled = 1 FROM [AccessGroupMember] WHERE [AccessGroupID] = @AG_LicenseControl AND [CustomerID] = @CustomerID
    INSERT INTO @CandidateIDTable (CandidateID) SELECT [Value] FROM dbo.funcListToTableInt(@KeepTestCandidateIDList, ',')
    SET @CandidateCount = @@rowcount
    
    UPDATE ct SET ct.FullName = u.[FirstName] + ' ' + u.[LastName], ct.Username = u.[UserName]
    FROM @CandidateIDTable ct JOIN org.[User] u ON ct.CandidateID = u.UserID
    --SELECT * FROM @CandidateIDTable

    -- If license controlled, check available license with number of test candidates in the list
    IF @IsLicenseControlled = 1
    BEGIN
        RAISERROR ('Info: Customer is license controlled. Checking available license...', 0, 1)
        SELECT @AvailableLicense = [pv].[Value] FROM [prop].[PropValue] pv WHERE [pv].[PropertyID] = @PROP_AvailableLicense AND [pv].[ItemID] = @CustomerID
        IF @AvailableLicense < @CandidateCount  -- If not enough license left, abort certifying
        BEGIN
            PRINT 'Error: Not enough available license. Available license = ' + CONVERT(nvarchar(16),@AvailableLicense) + '; Candidate count = ' + CONVERT(nvarchar(16),@CandidateCount)
            PRINT 'Abort certifying'
            RETURN
        END
        RAISERROR ('Info: Checking available license - OK', 0, 1)
    END
    ELSE
    BEGIN
        RAISERROR ('Info: Customer is not license controlled', 0, 1)
    END

    SELECT @ET_CertifyTrainee = [EventTypeID] FROM [log].[EventType] WHERE [OwnerID] = @OwnerID AND [CodeName] = 'CERTIFY_TRAINEE' AND [Active] = 1
    SELECT @TraineeDepartmentID = [DepartmentID] FROM [org].[User] WHERE [UserID] = @TraineeUserID

BEGIN TRANSACTION
    RAISERROR ('Info: Certifying...', 0, 1)
    DELETE FROM [org].[UT_U] WHERE [UserID] = @TraineeUserID AND [UserTypeID] = @UT_Trainee

    INSERT INTO [prop].[PropValue] ([PropertyID], [ItemID], [Value], [Created], [CreatedBy], [Updated], [UpdatedBy])
    VALUES (@PROP_InstructorUserID, @TraineeUserID, @InstructorUserID, GETDATE(), @InstructorUserID, GETDATE(), @InstructorUserID)

    IF ISNULL(@ET_CertifyTrainee,0) > 0
    BEGIN
        RAISERROR ('Info: Logging CERTIFY_TRAINEE event', 0, 1)
        
        SELECT @FullName = u.FirstName + ' ' + u.LastName, @DepartmentName = d.[Name], @Username = u.[UserName], @PathName = hd.[PathName]
        FROM org.[User] u
        JOIN org.Department d on u.DepartmentID = d.DepartmentID AND u.[UserID] = @InstructorUserID
        JOIN org.H_D hd on hd.DepartmentID = d.DepartmentID

        SET @UserData = CONVERT(xml,N'<UserDataField UserName="' + @Username + '" FullName="' + @FullName + '" DepartmentName="' + @DepartmentName + '" HDPath="' + @PathName + '" />')

        INSERT INTO [log].[Event] ([EventTypeID], [UserID], [IPNumber], [TableTypeID], [ItemID], [Created], [ApplicationName], [DepartmentID], [CustomerID], [UserData])
        VALUES (@ET_CertifyTrainee, @InstructorUserID, '::1', @TableType_User, @TraineeUserID, getdate(), 'By script', @TraineeDepartmentID, @CustomerID, @UserData)

        SET @EventID = SCOPE_IDENTITY()
        INSERT INTO [log].[EventInfo] ([EventID], [EventKeyID], [Value], [Created])
        SELECT TOP 1 @EventID, [EventKeyID], @FullName, GETDATE()
        FROM  [log].[EventKey]
        WHERE [EventTypeID] = @ET_CertifyTrainee AND [CodeName] = 'InstructorName'

        INSERT INTO [log].[EventInfo] ([EventID], [EventKeyID], [Value], [Created])
        SELECT TOP 1 @EventID, [EventKeyID], @DepartmentName, GETDATE()
        FROM  [log].[EventKey]
        WHERE [EventTypeID] = @ET_CertifyTrainee AND [CodeName] = 'DepartmentName'

        RAISERROR ('Info: Finish logging CERTIFY_TRAINEE event', 0, 1)
    END
    ELSE
    BEGIN
        RAISERROR ('Warning: No event logging for certifying trainee due to missing active Event Type CERTIFY_TRAINEE', 0, 1)
        RAISERROR ('Info: Certifying is continue without logging this event', 0, 1)
    END

    RAISERROR ('Info: Processing test candidates', 0, 1)
    IF @CandidateCount > 0
    BEGIN
        DELETE utu
        FROM [org].[UT_U] utu JOIN @CandidateIDTable ct ON utu.[UserID] = ct.[CandidateID] AND utu.[UserTypeID] = @UT_TestCandidate

        SET @DeletedRowCount = @@rowcount
        SET @Msg = 'Info: Converted ' + CONVERT(nvarchar(16),@DeletedRowCount) + ' test candidate(s)'
        RAISERROR (@Msg, 0, 1)

        SELECT @ET_LicenseUsed = [EventTypeID] FROM [log].[EventType] WHERE [OwnerID] = @OwnerID AND [CodeName] = 'LICENSE_USED' AND [Active] = 1

        SELECT @FullName = u.FirstName + ' ' + u.LastName, @DepartmentName = d.[Name], @Username = u.[UserName], @PathName = hd.[PathName]
        FROM org.[User] u
        JOIN org.Department d on u.DepartmentID = d.DepartmentID AND u.[UserID] = @TraineeUserID
        JOIN org.H_D hd on hd.DepartmentID = d.DepartmentID

        SET @UserData = CONVERT(xml,N'<UserDataField UserName="' + @Username + '" FullName="' + @FullName + '" DepartmentName="' + @DepartmentName + '" HDPath="' + @PathName + '" />')

        RAISERROR ('Info: Logging LICENSE_USED event', 0, 1)

        DECLARE c_Candidate CURSOR LOCAL FAST_FORWARD READ_ONLY FOR
        SELECT CandidateID, FullName, Username FROM @CandidateIDTable

        OPEN c_Candidate
        FETCH NEXT FROM c_Candidate INTO @CandidateID, @CandidateName, @CandidateUsername
        WHILE @@FETCH_STATUS = 0
        BEGIN
            INSERT INTO [log].[Event] ([EventTypeID], [UserID], [IPNumber], [TableTypeID], [ItemID], [Created], [ApplicationName], [DepartmentID], [CustomerID], [UserData])
            VALUES (@ET_LicenseUsed, @TraineeUserID, '::1', @TableType_User, @CandidateID, getdate(), 'By script', @TraineeDepartmentID, @CustomerID, @UserData)

            SET @EventID = SCOPE_IDENTITY()
            INSERT INTO [log].[EventInfo] ([EventID], [EventKeyID], [Value], [Created])
            SELECT TOP 1 @EventID, [EventKeyID], @CandidateID, GETDATE()
            FROM  [log].[EventKey]
            WHERE [EventTypeID] = @ET_LicenseUsed AND [CodeName] = 'CandidateID'

            INSERT INTO [log].[EventInfo] ([EventID], [EventKeyID], [Value], [Created])
            SELECT TOP 1 @EventID, [EventKeyID], ISNULL(@CandidateName,''), GETDATE()
            FROM  [log].[EventKey]
            WHERE [EventTypeID] = @ET_LicenseUsed AND [CodeName] = 'CandidateName'

            INSERT INTO [log].[EventInfo] ([EventID], [EventKeyID], [Value], [Created])
            SELECT TOP 1 @EventID, [EventKeyID], ISNULL(@CandidateUsername,''), GETDATE()
            FROM  [log].[EventKey]
            WHERE [EventTypeID] = @ET_LicenseUsed AND [CodeName] = 'CandidateUsername'

            INSERT INTO [log].[EventInfo] ([EventID], [EventKeyID], [Value], [Created])
            SELECT TOP 1 @EventID, [EventKeyID], @CustomerID, GETDATE()
            FROM  [log].[EventKey]
            WHERE [EventTypeID] = @ET_LicenseUsed AND [CodeName] = 'CustomerID'

            FETCH NEXT FROM c_Candidate INTO @CandidateID, @CandidateName, @CandidateUsername
        END
        CLOSE c_Candidate
        DEALLOCATE c_Candidate

        RAISERROR ('Info: Finish logging LICENSE_USED event', 0, 1)
        EXEC [dbo].[prc_Calculate_License] 	@OwnerID, @CustomerID, @PROP_AvailableLicense, @PROP_UsedLicense, @SystemUserID, 1, @TraineeUserID
        RAISERROR ('Info: License usage recalculated', 0, 1)
    END

    EXEC [org].[prc_Delete_UsersInGroup_ByUTID] @TraineeUserID, @UT_TestCandidate, @OwnerID

    RAISERROR ('Info: Finish certifying trainee', 0, 1)
COMMIT TRANSACTION

END
GO
