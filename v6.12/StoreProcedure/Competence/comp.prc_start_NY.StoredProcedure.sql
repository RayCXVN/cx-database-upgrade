USE [production-competence-sg-at6qr]
GO
/****** Object:  StoredProcedure [comp].[prc_start_NY]    Script Date: 9/22/2017 3:56:42 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

--------------------------------------------------------------------------------------------------------
-- Dev: Johnny
-- Prepare new year for 2017
/*  2017-04-05 Ray:     Adapt to use in merged Q and R
*/
CREATE PROCEDURE [comp].[prc_start_NY]
AS
BEGIN
    SET XACT_ABORT ON
    BEGIN TRAN
    DECLARE @LastPeriodID int, @NewPeriodID int, @LastPeriodEndDate datetime, @NewPeriodStartDate datetime, @sqlCommand nvarchar(max), @sqlParam nvarchar(max)

    SELECT @LastPeriodID = MAX([p].[PeriodID]) FROM [at].[Period] p
    SET @NewPeriodID = @LastPeriodID + 1

    -- Add new period with EndDate set to 20-JAN of the following year
    SET IDENTITY_INSERT [at].[Period] ON
    INSERT INTO [at].[Period] ([PeriodID], [Ownerid], [No], [Startdate], [Enddate])
    SELECT @NewPeriodID, p.[Ownerid], p.[No] + 1, DATEADD(YEAR, 1, p.[Startdate]), DATEADD(YEAR, 1, p.[Enddate])
    FROM [at].[Period] p
    WHERE [p].[PeriodID] = @LastPeriodID
    SET IDENTITY_INSERT [at].[Period] OFF

    -- Increase 1 to year number in last period name to form the new period name
    INSERT INTO [at].[LT_Period] ([LanguageID], [PeriodID], [Name], [Description])
    SELECT [LanguageID], @NewPeriodID, [dbo].[IncreaseNumberInText]([Name],4,2000), [dbo].[IncreaseNumberInText]([Description],4,2000)
    FROM [at].[LT_Period]
    WHERE [PeriodID] = @LastPeriodID

    -- Add new period's surveys and batches based on last period surveys, batches
    EXEC [dbo].[prc_AddSurvey_For_New_Period] @LastPeriodID, @NewPeriodID, NULL, NULL, NULL, NULL, 0

    -- Set last period's EndDate, last period surveys and batches' EndDate to end of last year
    SELECT @LastPeriodEndDate = CONVERT(nvarchar(4),YEAR([Startdate])) + '-12-31', @NewPeriodStartDate = DATEADD(YEAR, 1, [Startdate]) FROM [at].[Period] WHERE [PeriodID] = @LastPeriodID
    UPDATE [at].[Period] SET [Enddate] = @LastPeriodEndDate WHERE [PeriodID] = @LastPeriodID
    UPDATE [at].[Survey] SET [EndDate] = @LastPeriodEndDate WHERE [PeriodID] = @LastPeriodID
    UPDATE b SET b.[EndDate] = @LastPeriodEndDate FROM [at].[Batch] b JOIN [at].[Survey] s ON b.[SurveyID] = s.[SurveyID] AND s.[PeriodID] = @LastPeriodID

	-- Set StartDate and EndDate for new period's surveys and batches 
	-- (StartDate is the first of Jan of the new year and EndDate is the 20th of Jan of the year after according to StartDate and EndDate of the new period)
	DECLARE @StartDate DATETIME, @EndDate DATETIME
	SELECT @StartDate = Startdate, @EndDate = Enddate FROM [at].[Period] WHERE PeriodID = @NewPeriodID
	UPDATE at.Survey
    SET StartDate=@StartDate,
        EndDate=@EndDate
    WHERE PeriodID=@NewPeriodID

    UPDATE at.Batch
    SET StartDate=s.StartDate,
        EndDate=s.EndDate
    FROM at.Batch b
    INNER JOIN at.Survey s on b.SurveyID=s.SurveyID
    WHERE s.PeriodID=@NewPeriodID

    -- Build a mapping table between last period batches and new period batches
    CREATE TABLE #SurveyBatchTable ([FromSurveyID] int, [FromBatchID] int, [ToSurveyID] int, [ToBatchID] int, [ReportServer] nvarchar(64), [ReportDB] nvarchar(64))

    INSERT INTO #SurveyBatchTable ([FromSurveyID], [FromBatchID], [ToSurveyID], [ToBatchID], [ReportServer], [ReportDB])
    SELECT fb.[SurveyID], fb.[BatchID], ts.[SurveyID], tb.[BatchID], ts.[ReportServer], ts.[ReportDB]
    FROM [at].[Survey] ts JOIN [at].[Batch] tb ON ts.[SurveyID] = tb.[SurveyID] AND ts.[PeriodID] = @NewPeriodID AND [ts].[Status] > 0 AND tb.[Status] > 0
    JOIN [at].[Batch] fb ON fb.[SurveyID] = ts.[FromSurveyID] AND fb.[DepartmentID] = tb.[DepartmentID] AND fb.[Status] > 0

    DECLARE c_ReportDB CURSOR LOCAL FAST_FORWARD READ_ONLY FOR
    SELECT DISTINCT s.[ReportServer], [s].[ReportDB] FROM [at].[Survey] s WHERE [s].[PeriodID] = @LastPeriodID

    DECLARE @ReportServer nvarchar(64), @ReportDB nvarchar(64), @ConnectionString nvarchar(128) = ''

    OPEN c_ReportDB
    FETCH NEXT FROM c_ReportDB INTO @ReportServer, @ReportDB
    WHILE @@FETCH_STATUS = 0
    BEGIN
        IF @ReportServer = @@servername AND @ReportDB = DB_NAME()
        BEGIN
            SET @ConnectionString = ''
        END
        ELSE
        BEGIN
            SET @ConnectionString = '[' + @ReportServer + '].[' + @ReportDB + '].'
        END

        SET @sqlCommand = 'UPDATE r SET [r].[SurveyID] = sbt.[ToSurveyID], [r].[BatchID] = sbt.[ToBatchID]
        FROM ' + @ConnectionString + '[dbo].[Result] r JOIN #SurveyBatchTable sbt ON r.[SurveyID] = sbt.[FromSurveyID] AND r.[BatchID] = sbt.[FromBatchID]
        WHERE ([r].[EndDate] >= @p_NewPeriodStartDate OR r.[EndDate] IS NULL)'

        SET @sqlParam = '@p_NewPeriodStartDate datetime'

        EXEC sp_executesql @sqlCommand, @sqlParam, @p_NewPeriodStartDate = @NewPeriodStartDate
    
        FETCH NEXT FROM c_ReportDB INTO @ReportServer, @ReportDB
    END
    CLOSE c_ReportDB
    DEALLOCATE c_ReportDB

    DROP TABLE #SurveyBatchTable

    COMMIT TRAN
END
GO
