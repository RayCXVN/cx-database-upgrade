SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[at].[prc_LT_Alternative_upd]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [at].[prc_LT_Alternative_upd] AS' 
END
GO

ALTER PROCEDURE [at].[prc_LT_Alternative_upd]
(
	@LanguageID int,
	@AlternativeID int,
	@Name nvarchar(256),
	@Label nvarchar(256),
	@Description nvarchar(max),
	@cUserid int,
	@Log smallint = 1
)
AS
BEGIN
	SET NOCOUNT ON
	DECLARE @Err Int

	UPDATE [at].[LT_Alternative]
	SET
		[LanguageID] = @LanguageID,
		[AlternativeID] = @AlternativeID,
		[Name] = @Name,
		[Label] = @Label,
		[Description] = @Description
	WHERE
		[LanguageID] = @LanguageID AND
		[AlternativeID] = @AlternativeID

	IF @Log = 1 
	BEGIN 
		INSERT INTO [Log].[AuditLog] ( UserId, TableName, Type, Data, Created) 
		SELECT @cUserid,'LT_Alternative',1,
		( SELECT * FROM [at].[LT_Alternative] 
			WHERE
			[LanguageID] = @LanguageID AND
			[AlternativeID] = @AlternativeID			 FOR XML AUTO) as data,
			getdate()
	END

	Set @Err = @@Error

	RETURN @Err
END


GO
