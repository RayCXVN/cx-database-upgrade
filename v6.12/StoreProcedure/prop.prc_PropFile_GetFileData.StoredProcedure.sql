SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[prop].[prc_PropFile_GetFileData]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [prop].[prc_PropFile_GetFileData] AS' 
END
GO

ALTER PROCEDURE [prop].[prc_PropFile_GetFileData]
(
	@PropFileID	int
)
As
BEGIN
	SET NOCOUNT ON
	DECLARE @Err Int

	SELECT FileData
	FROM [prop].[PropFile]
	WHERE [PropFileID] = @PropFileID

	Set @Err = @@Error

	RETURN @Err
End


GO
