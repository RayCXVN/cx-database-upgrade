SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[app].[prc_LT_Site_upd]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [app].[prc_LT_Site_upd] AS' 
END
GO
ALTER PROCEDURE [app].[prc_LT_Site_upd]    
 @LanguageID int,    
 @SiteID int,    
 @Name nvarchar(256),    
 @Description nvarchar(max),    
 @LoginText nvarchar(max),    
 @FooterText nvarchar(max),    
 @SupportText nvarchar(max),    
 @ClosedText nvarchar(max),    
 @ExtraFooterTextAfterAuthentication nvarchar(max),    
 @HeadData nvarchar(max),    
 @cUserid int,    
 @Log smallint = 1,
 @BaseURL nvarchar(256)='',
 @ShortName nvarchar(256) ='',
 @ProductURL			NVARCHAR(max),
 @ProductURLTitle		NVARCHAR(max),
 @BlogURL				NVARCHAR(max),
 @BlogURLTitle			NVARCHAR(max),
 @BodyData			    NVARCHAR(max) = ''

AS    
BEGIN    
 SET NOCOUNT ON;    
 DECLARE @Err Int    
        
    UPDATE [app].[LT_Site]    
    SET    [Name]  = @Name     
           ,[Description]  = @Description     
           ,[LoginText]   = @LoginText    
           ,[FooterText]  = @FooterText    
           ,[SupportText]   = @SupportText    
           ,[ClosedText]  = @ClosedText  
           ,[ExtraFooterTextAfterAuthentication] = @ExtraFooterTextAfterAuthentication 
           ,[HeadData]   = @HeadData
		   ,[BaseURL] = @BaseURL
		   ,[ShortName]= @ShortName
		   ,ProductURL=@ProductURL
		   ,ProductURLTitle=@ProductURLTitle
		   ,BlogURL=@BlogURL
		   ,BlogURLTitle=@BlogURLTitle
		   ,BodyData  = @BodyData
 WHERE [LanguageID] = @LanguageID     
           AND [SiteID] = @SiteID  
               
    Set @Err = @@Error    
 IF @Log = 1     
 BEGIN     
  INSERT INTO [Log].[AuditLog] ( UserId, TableName, Type, Data, Created)     
  SELECT @cUserid,'LT_Site',0,    
  ( SELECT * FROM [app].[LT_Site]    
   WHERE    
   [LanguageID] = @LanguageID AND    
   [SiteID] = @SiteID     FOR XML AUTO) as data,    
    getdate()     
 END    
     
 RETURN @Err           
END    


GO
