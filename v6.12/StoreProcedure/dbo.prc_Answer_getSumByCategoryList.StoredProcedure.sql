SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[prc_Answer_getSumByCategoryList]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [dbo].[prc_Answer_getSumByCategoryList] AS' 
END
GO
ALTER PROCEDURE [dbo].[prc_Answer_getSumByCategoryList]
(
	@ResultID		int,
	@CategoryList	nvarchar(max),
	@QuestionList	nvarchar(max) =''
)
As
BEGIN
    SET NOCOUNT ON
    DECLARE @Err Int

    SELECT qc.CategoryID, ISNULL(sum(ans.Value),0) AS value
    FROM Answer ans JOIN at.Q_C qc ON ans.QuestionID = qc.QuestionID
				AND qc.CategoryID IN (SELECT ParseValue FROM StringToArray(@CategoryList,','))
				AND (@QuestionList='' OR qc.QuestionID IN (SELECT ParseValue FROM StringToArray(@QuestionList,',')))
    WHERE ans.ResultID = @ResultID
    GROUP BY qc.CategoryID

    Set @Err = @@Error

    RETURN @Err
END


GO
