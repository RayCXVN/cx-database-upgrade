SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[note].[prc_Note_get]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [note].[prc_Note_get] AS' 
END
GO
ALTER PROCEDURE [note].[prc_Note_get]
	@NoteTypeID int
AS
BEGIN
	SET NOCOUNT ON;
	DECLARE @Err Int

    SELECT 
		[NoteID],
		[NoteTypeID],
		[Active],
		[ExtID],
		[NotifyAtNextLogin],
		[StartDate],
		[EndDate],
		[CreatedBy],
		[Created],
		[ChangedBy],
		[Changed],
		[Deleted]
	FROM [note].[Note]
	WHERE [NoteTypeID] = @NoteTypeID

	Set @Err = @@Error

	RETURN @Err

END

GO
