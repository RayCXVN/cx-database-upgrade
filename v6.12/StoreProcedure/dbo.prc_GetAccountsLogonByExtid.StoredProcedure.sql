SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[prc_GetAccountsLogonByExtid]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [dbo].[prc_GetAccountsLogonByExtid] AS' 
END
GO
ALTER PROCEDURE [dbo].[prc_GetAccountsLogonByExtid]
(
	@ExtId		nvarchar(64),
	@UserName	nvarchar(128) = NULL, /* OPTIONAL */
	@OwnerId	int	,
	@ErrNo		int		OUTPUT
)
AS
BEGIN
    DECLARE @ret as integer		
    DECLARE @EntityStatus_Active int
    SELECT @EntityStatus_Active = [EntityStatusID] FROM [dbo].[EntityStatus] WHERE [CodeName] = 'Active'

		SELECT 	
			u.UserName,
			u.DepartmentID,
			u.UserID,
			u.OwnerId,
			u.FirstName,
			u.LastName,
			d.Name AS DepartmentName,
			u.LanguageID,
			d.CustomerID,
			u.SSN
		FROM 	
			[org].[User] u
			INNER JOIN org.Department d ON u.DepartmentID = d.DepartmentID
		WHERE
			((@UserName IS NULL) OR (UserName = @UserName))
			AND u.[ExtId] = @ExtId
			AND u.[ExtId] IS NOT NULL
			AND u.[ExtId] <> ''
			AND u.[EntityStatusID] = @EntityStatus_Active
			AND u.[Ownerid] = @OwnerId
			ORDER BY u.Created 
    
SET @ret = @@ROWCOUNT
IF @ret < 1 
BEGIN
	SET @ErrNo = 1 /* the user wasn't found */
END
ELSE
BEGIN	
	IF @ret > 1 
	BEGIN
		SET @ErrNo = 2 /* multiple users was found */
	END
	ELSE
	BEGIN	
		SET @ErrNo = 0
	END
END
END


GO
