SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[proc].[prc_LT_ProcessType_upd]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [proc].[prc_LT_ProcessType_upd] AS' 
END
GO
ALTER PROCEDURE [proc].[prc_LT_ProcessType_upd]
(
	@ProcessTypeID int,
	@LanguageID int,
	@Name nvarchar(256),
	@Description nvarchar(max),
	@DescriptionFieldName nvarchar(256),
	@HelpText nvarchar(max),
	@WizardStep1 nvarchar(64),
	@WizardStep2 nvarchar(64),
	@WizardStep3 nvarchar(64),
	@WizardStep4 nvarchar(64),
	@WizardStep5 nvarchar(64),
	@AnswerHeader nvarchar(128),
	@WizardStep1Desc nvarchar(max) = '',
	@WizardStep2Desc nvarchar(max) = '',
	@WizardStep3Desc nvarchar(max) = '',
	@WizardStep4Desc nvarchar(max) = '',
	@WizardStep5Desc nvarchar(max) = '',
	@cUserid int,
	@Log smallint = 1
)
AS
BEGIN
	SET NOCOUNT ON
	DECLARE @Err Int

	UPDATE [proc].[LT_ProcessType]
	SET
		[ProcessTypeID] = @ProcessTypeID,
		[LanguageID] = @LanguageID,
		[Name] = @Name,
		[Description] = @Description,
		[DescriptionFieldName] = @DescriptionFieldName ,
		[HelpText] = @HelpText ,
		[WizardStep1] = @WizardStep1, 
		[WizardStep2] = @WizardStep2,
		[WizardStep3] = @WizardStep3,
		[WizardStep4] = @WizardStep4, 
		[WizardStep5] = @WizardStep5, 
		[AnswerHeader] = @AnswerHeader,
		[WizardStep1Desc] = @WizardStep1Desc, 
		[WizardStep2Desc] = @WizardStep2Desc,
		[WizardStep3Desc] = @WizardStep3Desc,
		[WizardStep4Desc] = @WizardStep4Desc, 
		[WizardStep5Desc] = @WizardStep5Desc 
	WHERE
		[ProcessTypeID] = @ProcessTypeID AND
		[LanguageID] = @LanguageID

	IF @Log = 1 
	BEGIN 
		INSERT INTO [Log].[AuditLog] ( UserId, TableName, Type, Data, Created) 
		SELECT @cUserid,'LT_ProcessType',1,
		( SELECT * FROM [proc].[LT_ProcessType] 
			WHERE
			[ProcessTypeID] = @ProcessTypeID AND
			[LanguageID] = @LanguageID			 FOR XML AUTO) as data,
			getdate()
	END

	Set @Err = @@Error

	RETURN @Err
END

GO
