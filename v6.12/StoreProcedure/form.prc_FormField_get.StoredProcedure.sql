SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[form].[prc_FormField_get]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [form].[prc_FormField_get] AS' 
END
GO
ALTER PROCEDURE [form].[prc_FormField_get]  
(  
 @FormID int  
)  
AS  
BEGIN  
 SET NOCOUNT ON  
 DECLARE @Err Int  
  
 SELECT  
 [FormFieldID],  
 [FormID],  
 [No],  
 ISNULL([TableTypeID], 0) AS 'TableTypeID',  
 ISNULL([RelatedTableTypeID], 0) AS 'RelatedTableTypeID',  
 [FieldTypeID],  
 [ValueType],  
 [FormMode],  
 [RenderType],  
 [DefaultValue],  
 [AvailableValues],  
 [Mandatory],  
 [Multiple],  
 ISNULL([PropertyID], 0) AS 'PropertyID',  
 [FieldName],  
 [Parameters],  
 [Created],
 [OverrideLocked],
 [CssClass]
 FROM [form].[FormField]  
 WHERE  
 [FormID] = @FormID  
 Order by No  
  
 Set @Err = @@Error  
  
 RETURN @Err  
END  

GO
