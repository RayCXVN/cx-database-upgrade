SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[rep].[prc_RepLevelLimit_upd]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [rep].[prc_RepLevelLimit_upd] AS' 
END
GO

ALTER PROCEDURE [rep].[prc_RepLevelLimit_upd]
(
	@RepLevelLimitID int,
	@ReportColumnID INT=NULL,
	@ReportID INT=NULL,
	@MinValue float,
	@MaxValue float,
	@Sigchange float,
	@Fillcolor varchar(16),
	@BorderColor varchar(16),
	@LevelGroupID int = null,
	@NegativeTrend bit = 0,
	@cUserid int,
	@Log smallint = 1
)
AS
BEGIN
	SET NOCOUNT ON
	DECLARE @Err Int

	UPDATE [rep].[RepLevelLimit]
	SET
		[ReportColumnID] = @ReportColumnID,
		[ReportID] = @ReportID,
		[MinValue] = @MinValue,
		[MaxValue] = @MaxValue,
		[Sigchange] = @Sigchange,
		[Fillcolor] = @Fillcolor,
		[BorderColor] = @BorderColor,
		[LevelGroupID] = @LevelGroupID,
		[NegativeTrend] = @NegativeTrend
	WHERE
		[RepLevelLimitID] = @RepLevelLimitID

	IF @Log = 1 
	BEGIN 
		INSERT INTO [Log].[AuditLog] ( UserId, TableName, Type, Data, Created) 
		SELECT @cUserid,'RepLevelLimit',1,
		( SELECT * FROM [rep].[RepLevelLimit] 
			WHERE
			[RepLevelLimitID] = @RepLevelLimitID			 FOR XML AUTO) as data,
			getdate()
	END

	Set @Err = @@Error

	RETURN @Err
END



GO
