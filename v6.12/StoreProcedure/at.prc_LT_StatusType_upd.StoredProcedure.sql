SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[at].[prc_LT_StatusType_upd]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [at].[prc_LT_StatusType_upd] AS' 
END
GO

ALTER PROCEDURE [at].[prc_LT_StatusType_upd]
(
	@LanguageID int,
	@StatusTypeID int,
	@Name varchar(50),
	@Description varchar(250),
    @DefaultActionName Nvarchar(512) ='',
    @DefaultActionDescription NVARCHAR(MAX) ='',
	@cUserid int,
	@Log smallint = 1
)
AS
BEGIN
	SET NOCOUNT ON
	DECLARE @Err Int

	UPDATE [at].[LT_StatusType]
	SET
		[LanguageID] = @LanguageID,
		[StatusTypeID] = @StatusTypeID,
		[Name] = @Name,
		[Description] = @Description,
		[DefaultActionName]=@DefaultActionName,
		[DefaultActionDescription] =@DefaultActionDescription
	WHERE
		[LanguageID] = @LanguageID AND
		[StatusTypeID] = @StatusTypeID

	IF @Log = 1 
	BEGIN 
		INSERT INTO [Log].[AuditLog] ( UserId, TableName, Type, Data, Created) 
		SELECT @cUserid,'LT_StatusType',1,
		( SELECT * FROM [at].[LT_StatusType] 
			WHERE
			[LanguageID] = @LanguageID AND
			[StatusTypeID] = @StatusTypeID			 FOR XML AUTO) as data,
			getdate()
	END

	Set @Err = @@Error

	RETURN @Err
END

GO
