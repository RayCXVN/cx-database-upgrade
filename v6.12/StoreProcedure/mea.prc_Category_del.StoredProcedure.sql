SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[mea].[prc_Category_del]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [mea].[prc_Category_del] AS' 
END
GO
ALTER PROCEDURE [mea].[prc_Category_del]    
(    
 @CategoryId int,    
 @cUserid int,    
 @Log smallint = 1    
)    
AS    
BEGIN    
 SET NOCOUNT ON    
 DECLARE @Err Int    
    
 IF @Log = 1     
 BEGIN     
  INSERT INTO [Log].[AuditLog] ( UserId, TableName, Type, Data, Created)     
  SELECT @cUserid,'mea.Category',2,    
  ( SELECT * FROM [mea].[Category]     
   WHERE    
   [CategoryId] = @CategoryId    
    FOR XML AUTO) as data,    
   getdate()     
 END     
    
    
 DELETE FROM [mea].[Category]    
 WHERE    
  [CategoryId] = @CategoryId    
    
 Set @Err = @@Error    
    
 RETURN @Err    
END    
    

GO
