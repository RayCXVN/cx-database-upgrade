SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[job].[prc_LT_JobType_upd]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [job].[prc_LT_JobType_upd] AS' 
END
GO

ALTER PROCEDURE [job].[prc_LT_JobType_upd]
(
	@LanguageID int,
	@JobTypeID smallint,
	@Name nvarchar(256),
	@Description nvarchar(max),
	@cUserid int,
	@Log smallint = 1
)
AS
BEGIN
	SET NOCOUNT ON
	DECLARE @Err Int

	UPDATE [job].[LT_JobType]
	SET
		[LanguageID] = @LanguageID,
		[JobTypeID] = @JobTypeID,
		[Name] = @Name,
		[Description] = @Description
	WHERE
		[LanguageID] = @LanguageID AND
		[JobTypeID] = @JobTypeID

	IF @Log = 1 
	BEGIN 
		INSERT INTO [Log].[AuditLog] ( UserId, TableName, Type, Data, Created) 
		SELECT @cUserid,'LT_JobType',1,
		( SELECT * FROM [job].[LT_JobType] 
			WHERE
			[LanguageID] = @LanguageID AND
			[JobTypeID] = @JobTypeID			 FOR XML AUTO) as data,
			getdate()
	END

	Set @Err = @@Error

	RETURN @Err
END


GO
