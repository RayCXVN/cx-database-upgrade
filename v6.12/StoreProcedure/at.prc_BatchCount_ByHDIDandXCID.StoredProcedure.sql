SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[at].[prc_BatchCount_ByHDIDandXCID]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [at].[prc_BatchCount_ByHDIDandXCID] AS' 
END
GO
ALTER PROCEDURE [at].[prc_BatchCount_ByHDIDandXCID]
(   @ParentXCID             int,
    @HDID                   int,
    @DepartmentTypeID       int,
    @LanguageID             int,
    @ActivePeriodID         int,
    @NumberOfPeriods        int = 0,
    @FilterXCByPropertyID   int = 0,
    @PropertyValue          int = 0
) AS
BEGIN
    SET NOCOUNT ON
    DECLARE @ActiveEntityStatusID INT = (SELECT EntityStatusID FROM EntityStatus WHERE CodeName='Active')
    DECLARE @TT_UserType int = 0, @TT_DepartmentType int = 0, @RelationType_Standard int = 1, @ActivePeriodEndDate datetime
    SELECT @TT_UserType = [TableTypeID]           FROM [TableType] WHERE [Name] = 'UserType'
    SELECT @TT_DepartmentType = [TableTypeID]     FROM [TableType] WHERE [Name] = 'DepartmentType'
    SELECT @ActivePeriodEndDate = [p].[Enddate]   FROM [at].[Period] p WHERE [p].[PeriodID] = @ActivePeriodID

    DECLARE @PeriodTable TABLE ([PeriodID] int)
    IF @NumberOfPeriods <= 0 SET @NumberOfPeriods = 1
    INSERT INTO @PeriodTable ([PeriodID]) SELECT TOP(@NumberOfPeriods - 1) p.[PeriodID]
                                          FROM [at].[Period] p
                                          WHERE p.[Enddate] <= @ActivePeriodEndDate AND p.[PeriodID] != @ActivePeriodID
                                          ORDER BY p.[Enddate] DESC
    INSERT INTO @PeriodTable ([PeriodID]) VALUES (@ActivePeriodID)

    CREATE TABLE #Department ([DepartmentID] int, [Name] nvarchar(max), [XCID] int, [XCategoryName] nvarchar(max), [BatchCount] int)

    INSERT INTO #Department ([DepartmentID], [Name], [XCID], [XCategoryName], [BatchCount])
    SELECT d.[DepartmentID], d.[Name], sb.[XCID], sb.[XCategoryName], COUNT(sb.[BatchID]) [BatchCount]
    FROM [org].[H_D] hd JOIN [org].[Department] d ON hd.[DepartmentID] = d.[DepartmentID] AND hd.[Deleted] = 0 AND d.EntityStatusID = @ActiveEntityStatusID AND d.Deleted IS NULL AND hd.[Path] LIKE '%\' + CAST(@HDID AS nvarchar(32)) + '\%'
    JOIN [org].[DT_D] dtd ON d.[DepartmentID] = dtd.[DepartmentID] AND dtd.[DepartmentTypeID] = @DepartmentTypeID
    JOIN (SELECT b.[BatchID], b.[DepartmentID], xc.[XCID], ltxc.[Name] [XCategoryName]
          FROM [at].[XC_A] xca JOIN [at].[XCategory] xc ON xca.[XCID] = xc.[XCID] AND xc.[ParentID] = @ParentXCID
           AND (EXISTS (SELECT 1 FROM [prop].[PropValue] pv WHERE pv.[PropertyID] = @FilterXCByPropertyID AND pv.[ItemID] = xca.[XCID] AND pv.[Value] = @PropertyValue)
                OR @PropertyValue = -1
                OR (NOT EXISTS (SELECT 1 FROM [prop].[PropValue] pv2 WHERE pv2.[PropertyID] = @FilterXCByPropertyID AND pv2.[ItemID] = xca.[XCID]) AND @PropertyValue = 0)
               )
          JOIN [at].[Survey] s ON xca.[ActivityID] = s.[ActivityID] AND s.[Status] = 2 AND s.[ExtId] != 'Global'
          JOIN @PeriodTable pt ON pt.[PeriodID] = s.[PeriodID]
          JOIN [at].[Batch] b ON b.[SurveyID] = s.[SurveyID] AND b.[Status] = 2
          LEFT JOIN [at].[LT_XCategory] ltxc ON ltxc.[XCID] = xc.[XCID] AND ltxc.[LanguageID] = @LanguageID) sb ON sb.[DepartmentID] = d.[DepartmentID]
    GROUP BY d.[DepartmentID], d.[Name], sb.[XCID], sb.[XCategoryName]
    ORDER BY d.[Name], sb.[XCategoryName]

    INSERT INTO #Department ([DepartmentID], [Name], [XCID], [XCategoryName], [BatchCount])
    SELECT d.[DepartmentID], d.[Name], sb.[XCID], sb.[XCategoryName], -1 [BatchCount]
    FROM [org].[H_D] hd JOIN [org].[Department] d ON hd.[DepartmentID] = d.[DepartmentID] AND hd.[Deleted] = 0 AND d.EntityStatusID = @ActiveEntityStatusID AND d.Deleted IS NULL AND hd.[Path] LIKE '%\' + CAST(@HDID AS nvarchar(32)) + '\%'
    JOIN [org].[DT_D] dtd ON d.[DepartmentID] = dtd.[DepartmentID] AND dtd.[DepartmentTypeID] = @DepartmentTypeID
    CROSS JOIN (SELECT xc.[XCID], ltxc.[Name] [XCategoryName]
                FROM [at].[XCategory] xc LEFT JOIN [at].[LT_XCategory] ltxc ON ltxc.[XCID] = xc.[XCID] AND ltxc.[LanguageID] = @LanguageID
                WHERE xc.[ParentID] = @ParentXCID
                  AND (EXISTS (SELECT 1 FROM [prop].[PropValue] pv WHERE pv.[PropertyID] = @FilterXCByPropertyID AND pv.[ItemID] = xc.[XCID] AND pv.[Value] = @PropertyValue)
                       OR @PropertyValue = -1
                       OR (NOT EXISTS (SELECT 1 FROM [prop].[PropValue] pv2 WHERE pv2.[PropertyID] = @FilterXCByPropertyID AND pv2.[ItemID] = xc.[XCID]) AND @PropertyValue = 0)
                      )
               ) sb
    WHERE NOT EXISTS (SELECT 1 FROM #Department dl WHERE dl.[DepartmentID] = d.[DepartmentID] AND dl.[XCID] = sb.[XCID])

    DECLARE @XC_DT TABLE ([XCID] int, [DepartmentTypeID] int)
    INSERT INTO @XC_DT ([XCID], [DepartmentTypeID])
    SELECT xca.[XCID], om.[ToID] [DepartmentTypeID]
    FROM [at].[XC_A] xca JOIN (SELECT DISTINCT d.[XCID] FROM #Department d) v ON xca.[XCID] = v.[XCID]
    JOIN [at].[RoleMapping] rm ON rm.[ActivityID] = xca.[ActivityID]
    JOIN [ObjectMapping] om ON om.[RelationTypeID] = @RelationType_Standard AND om.[FromTableTypeID] = @TT_UserType AND om.[FromID] = rm.[UserTypeID] AND om.[ToTableTypeID] = @TT_DepartmentType
    GROUP BY xca.[XCID], om.[ToID]

    UPDATE d SET [d].[BatchCount] = 0
    FROM #Department d JOIN [org].[DT_D] dtd ON d.[DepartmentID] = dtd.[DepartmentID] AND d.[BatchCount] = -1
    WHERE EXISTS (SELECT 1 FROM @XC_DT xc WHERE xc.[DepartmentTypeID] = dtd.[DepartmentTypeID] AND xc.[XCID] = d.[XCID])

    DECLARE @BatchCountByXCID TABLE ([XCID] int, [TotalBatch] int, [PropValue] int)
    INSERT INTO @BatchCountByXCID ([XCID], [TotalBatch], [PropValue])
    SELECT xca.[XCID], COUNT(s.[SurveyID]), ISNULL(pv.[Value],0)
    FROM [at].[XC_A] xca JOIN [at].[XCategory] xc ON xca.[XCID] = xc.[XCID] AND xc.[ParentID] = @ParentXCID
    JOIN [at].[Survey] s ON s.[ActivityID] = xca.[ActivityID] AND s.[Status] = 2 AND s.[ExtId] != 'Global'
    JOIN @PeriodTable pt ON s.[PeriodID] = pt.[PeriodID]
    LEFT JOIN [prop].[PropValue] pv ON pv.[PropertyID] = @FilterXCByPropertyID AND pv.[ItemID] = xca.[XCID]
    GROUP BY xca.[XCID], ISNULL(pv.[Value],0)

    IF EXISTS (SELECT TOP 1 1 FROM @BatchCountByXCID)
    BEGIN
        SELECT d.*, bc.[TotalBatch], bc.[PropValue] FROM #Department d JOIN @BatchCountByXCID bc ON d.[XCID] = bc.[XCID] ORDER BY [Name], [XCategoryName]
    END
    ELSE
    BEGIN
        SELECT 0 [DepartmentID], CAST('' AS nvarchar(max)) AS [Name], xc.[XCID], lt.[Name] [XCategoryName], 0 [BatchCount], 0 [TotalBatch], CAST(ISNULL(pv.[Value],0) AS int) [PropValue]
        FROM [at].[XCategory] xc LEFT JOIN [at].[LT_XCategory] lt ON xc.[XCID] = lt.[XCID] AND lt.[LanguageID] = @LanguageID
        LEFT JOIN [prop].[PropValue] pv ON pv.[PropertyID] = @FilterXCByPropertyID AND pv.[ItemID] = xc.[XCID]
        WHERE xc.[ParentID] = @ParentXCID
    END
    DROP TABLE #Department
END

GO
