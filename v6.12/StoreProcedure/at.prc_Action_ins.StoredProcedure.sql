SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[at].[prc_Action_ins]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [at].[prc_Action_ins] AS' 
END
GO
ALTER PROCEDURE [at].[prc_Action_ins]
(
	@ActionID int = null output,
	@ChoiceID int,
	@No smallint,
	@PageID INT=NULL,
	@QuestionID int = null,
	@AlternativeID int = null,
	@Type smallint,
	@AVTID  int =null,
	@cUserid int,
	@Log smallint = 1
)
AS
BEGIN
	SET NOCOUNT ON
	DECLARE @Err Int

	INSERT INTO [at].[Action]
	(
		[ChoiceID],
		[No],
		[PageID],
		[QuestionID],
		[AlternativeID],
		[Type],
		[AVTID]
	)
	VALUES
	(
		@ChoiceID,
		@No,
		@PageID,
		@QuestionID,
		@AlternativeID,
		@Type,
		@AVTID
	)

	Set @Err = @@Error
	Set @ActionID = scope_identity()

	IF @Log = 1 
	BEGIN 
		INSERT INTO [Log].[AuditLog] ( UserId, TableName, Type, Data, Created) 
		SELECT @cUserid,'Action',0,
		( SELECT * FROM [at].[Action] 
			WHERE
			[ActionID] = @ActionID				 FOR XML AUTO) as data,
				getdate() 
	 END

	RETURN @Err
END

GO
