SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[at].[prc_S_CT_get]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [at].[prc_S_CT_get] AS' 
END
GO

ALTER PROCEDURE [at].[prc_S_CT_get]
(
	@ScaleID int
)
AS
BEGIN
	SET NOCOUNT ON
	DECLARE @Err Int

	SELECT
	[ScaleID],
	[ReportCalcTypeID]
	FROM [at].[S_CT]
	WHERE
	[ScaleID] = @ScaleID

	Set @Err = @@Error

	RETURN @Err
END


GO
