SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[mea].[prc_LT_Category_get]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [mea].[prc_LT_Category_get] AS' 
END
GO
ALTER PROCEDURE [mea].[prc_LT_Category_get]  
 @CategoryId int    
AS    
BEGIN    
 SET NOCOUNT ON;    
 DECLARE @Err Int        
SELECT [LanguageID]
      ,[CategoryId]
      ,[Name]
      ,[Description]
  FROM [mea].[LT_Category] 
 WHERE      
  [mea].[LT_Category].CategoryId = @CategoryId    
 Set @Err = @@Error    
    
 RETURN @Err    
END    


GO
