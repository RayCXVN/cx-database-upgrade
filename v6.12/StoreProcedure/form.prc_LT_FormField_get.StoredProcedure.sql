SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[form].[prc_LT_FormField_get]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [form].[prc_LT_FormField_get] AS' 
END
GO

ALTER PROCEDURE [form].[prc_LT_FormField_get]
(
	@FormFieldID int
)
AS
BEGIN
	SET NOCOUNT ON
	DECLARE @Err Int

	SELECT
	[LanguageID],
	[FormFieldID],
	[Name],
	[RegularExpression],
	[RegularExpressionText],
	[Description]
	FROM [form].[LT_FormField]
	WHERE
	[FormFieldID] = @FormFieldID

	Set @Err = @@Error

	RETURN @Err
END


GO
