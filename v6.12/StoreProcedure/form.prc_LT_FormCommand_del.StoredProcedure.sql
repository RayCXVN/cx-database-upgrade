SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[form].[prc_LT_FormCommand_del]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [form].[prc_LT_FormCommand_del] AS' 
END
GO


ALTER PROCEDURE [form].[prc_LT_FormCommand_del]
(
	@LanguageID int,
	@FormCommandID int,
	@cUserid int,
	@Log smallint = 1
)
AS
BEGIN
	SET NOCOUNT ON
	DECLARE @Err Int

	IF @Log = 1 
	BEGIN 
		INSERT INTO [Log].[AuditLog] ( UserId, TableName, Type, Data, Created) 
		SELECT @cUserid,'LT_FormCommand',2,
		( SELECT * FROM [form].[LT_FormCommand] 
			WHERE
			[LanguageID] = @LanguageID AND
			[FormCommandID] = @FormCommandID
			 FOR XML AUTO) as data,
			getdate() 
	END 


	DELETE FROM [form].[LT_FormCommand]
	WHERE
		[LanguageID] = @LanguageID AND
		[FormCommandID] = @FormCommandID

	Set @Err = @@Error

	RETURN @Err
END


GO
