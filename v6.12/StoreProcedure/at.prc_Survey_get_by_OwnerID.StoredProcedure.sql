SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[at].[prc_Survey_get_by_OwnerID]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [at].[prc_Survey_get_by_OwnerID] AS' 
END
GO
ALTER PROCEDURE [at].[prc_survey_get_by_ownerID](
    @OwnerID int)
AS
BEGIN
    SET NOCOUNT ON;
    DECLARE @Err int;

    SELECT [SurveyID], [ActivityID], ISNULL([HierarchyID], 0) AS 'HierarchyID', [StartDate], [EndDate], [Anonymous], [Status], [ShowBack], [LanguageID],
           [ButtonPlacement], [UsePageNo], [LinkURL], [FinishURL], [CreateResult], [ReportDB], [ReportServer], [StyleSheet], [Type], [Created],
           [LastProcessed], [ReProcessOLAP], [No], [OLAPServer], [OLAPDB], ISNULL([PeriodID], 0) AS 'PeriodID', [DeleteResultOnUserDelete],
           [ProcessCategorys], [FromSurveyID], [LastProcessedResultID], [LastProcessedAnswerID], [ExtId], [Tag],
           (SELECT COUNT(*) FROM [at].[Batch] WHERE [SurveyID] = [survey].[SurveyID]) AS [BatchCount]
    FROM [at].[Survey] [survey]
    WHERE [ActivityID] IN (SELECT [ActivityID] FROM [at].[Activity] WHERE [OwnerID] = @OwnerID)
    ORDER BY [No];

    SET @Err = @@Error;

    RETURN @Err;
END;
GO
