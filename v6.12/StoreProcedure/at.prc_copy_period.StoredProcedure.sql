SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[at].[prc_copy_period]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [at].[prc_copy_period] AS' 
END
GO
--Delete from at.survey where   periodid = 5

--Select * from at.period


--Update at.survey set periodid = 2 where  enddate = '2009-12-31' and periodid = 3
-- at.prc_copy_period 3,5,'2010','2011','2011','2012',1
ALTER proc [at].[prc_copy_period]
(
  @FromPeriodID as integer,
  @ToPeriodID as integer,
  @Year1 as nvarchar(4),
  @Year2 as nvarchar(4),
  @Year1New as nvarchar(4),
  @Year2New as nvarchar(4),
  @CopyAccessAndChoice as bit
)
as
  Declare @SurveyID as int
  Declare @NewSurveyID as int 

 

  declare SurveyList cursor for
  select SurveyID from at.Survey where periodid = @FromPeriodID 
	OPEN SurveyList
	FETCH NEXT FROM SurveyList 
	INTO @SurveyID


  WHILE @@FETCH_STATUS = 0
	BEGIN


      insert into at.survey(ActivityID, HierarchyID, StartDate, EndDate, Anonymous, Status, ShowBack, LanguageID, ButtonPlacement, UsePageNo, LinkURL, FinishURL, CreateResult,  ReportDB, ReportServer, StyleSheet, Type, Created, LastProcessed, ReProcessOLAP, No, OLAPServer, OLAPDB, PeriodID, ProcessCategorys, DeleteResultOnUserDelete)
      Select ActivityID, HierarchyID, dateadd(yy,1,Startdate), dateadd(yy,1,Enddate), Anonymous, Status, ShowBack, LanguageID, ButtonPlacement, UsePageNo, LinkURL, FinishURL, CreateResult,  ReportDB, ReportServer, StyleSheet, Type, getdate(), '1900-01-01', 0, No, OLAPServer, OLAPDB, @ToPeriodID, ProcessCategorys, DeleteResultOnUserDelete 
      from at.survey 
      where surveyID = @SurveyID
      
      select @NewSurveyID = SCOPE_IDENTITY()
      
      insert into at.lt_survey(LanguageID, SurveyID, Name, Description, Info, FinishText, DisplayName)
       Select LanguageID,@NewSurveyID,Replace(Replace(Name,@Year2,@Year2New),@Year1,@Year1New), Description, Info, FinishText,Replace(Replace(DisplayName,@Year2,@Year2New),@Year1,@Year1New) 
       FROM at.lt_survey where surveyID = @SurveyID
       
       insert into at.Batch (SurveyID, UserID, DepartmentID, Name, No, StartDate, EndDate, Status, MailStatus, Created)
       Select @NewSurveyID, UserID, DepartmentID, Name, No, dateadd(yy,1,Startdate), dateadd(yy,1,Enddate), Status, MailStatus, getdate() 
       from at.Batch where surveyid =  @SurveyID
       
       IF @CopyAccessAndChoice = 1 BEGIN
	      exec at.prc_Access_Copy @SurveyID,@NewSurveyID
	      exec at.prc_Choice_copy @SurveyID,@NewSurveyID
	   END
	
	   insert into dbo.AccessGeneric ( TableTypeID, Elementid, Type, AccessGroupID, Created)
	   select TableTypeID, @NewSurveyID, Type, AccessGroupID, getdate() from dbo.AccessGeneric where TableTypeID = 1 and Elementid = @SurveyID
	
	  FETCH NEXT FROM SurveyList 
	  INTO @SurveyID
	   
	END
	CLOSE SurveyList
	DEALLOCATE SurveyList

GO
