SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[org].[prc_Department_Single_get]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [org].[prc_Department_Single_get] AS' 
END
GO
/*  2016-10-10 Ray: Remove column Status from org.User, org.Department, RDB.dbo.Result
*/
ALTER PROCEDURE [org].[prc_Department_Single_get]
(
	@DepartmentID int
)
AS
BEGIN
	SET NOCOUNT ON
	DECLARE @Err Int

	SELECT
	[DepartmentID],
	[LanguageID],
	[OwnerID],
    ISNULL([CustomerID],0) CustomerID,
	[Name],
	[Description],
	[Adress],
	[PostalCode],
	[City],
	[OrgNo],
	[Created],
	[ExtID],
	[Tag],
	[Locked],
	[CountryCode],
	[EntityStatusID],
    [Deleted],
    [EntityStatusReasonID]
	FROM [org].[Department]
	WHERE
	[DepartmentID] = @DepartmentID

	Set @Err = @@Error

	RETURN @Err
END

GO
