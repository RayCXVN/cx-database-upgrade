SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[proc].[prc_ProcAnswerFile_GetFileData]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [proc].[prc_ProcAnswerFile_GetFileData] AS' 
END
GO
ALTER PROCEDURE [proc].[prc_ProcAnswerFile_GetFileData]
(
	@ProcessAnswerFileId	int
)
As
BEGIN
	SET NOCOUNT ON
	DECLARE @Err Int

	SELECT FileData
	FROM [proc].[ProcessAnswerFile]
	WHERE [ProcessAnswerFileID] = @ProcessAnswerFileId

	Set @Err = @@Error

	RETURN @Err
End

GO
