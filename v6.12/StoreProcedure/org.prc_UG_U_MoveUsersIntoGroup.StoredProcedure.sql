SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[org].[prc_UG_U_MoveUsersIntoGroup]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [org].[prc_UG_U_MoveUsersIntoGroup] AS' 
END
GO
-- =============================================
-- Author:		Tom Lee
-- Create date: 03/12/2012
-- Description:	Select Leader for +Competence
-- =============================================
ALTER PROCEDURE [org].[prc_UG_U_MoveUsersIntoGroup]
	@UserIds varchar(max),
	@LeaderGroupId int,
	@UserGroupTypeId int,
	@Message nvarchar(max) OUTPUT
AS
BEGIN
	DECLARE @UserId int, @IsLeader bit = 0, @LeaderID int
	DECLARE @UserIDTable TABLE (UserID int)	

	SELECT @LeaderID = ug.UserID FROM org.UserGroup ug WHERE UserGroupID = @LeaderGroupId

	INSERT INTO @UserIDTable (UserID) SELECT ParseValue FROM dbo.StringToArray(@UserIds,',')

	SET @Message = ' '
	SELECT @IsLeader = 1 FROM @UserIDTable WHERE UserID = @LeaderID
	IF @IsLeader = 1	
	BEGIN		
		SELECT @Message = FirstName + ' '+ LastName FROM org.[User] where UserID = @LeaderID
		SET @Message = @Message
		DELETE FROM @UserIDTable WHERE UserID = @LeaderID
	END
		
	BEGIN TRANSACTION
	-- Remove users from original groups
	DELETE ugu FROM org.UG_U ugu JOIN org.UserGroup ug ON ugu.UserGroupID = ug.UserGroupID AND ug.UserGroupTypeID = @UserGroupTypeId
	WHERE ugu.UserID IN (SELECT UserID FROM @UserIDTable)
	IF @@ERROR<>0 AND @@TRANCOUNT>0
	BEGIN		
		ROLLBACK TRANSACTION
		GOTO Finish
	END

	-- Move users to new group
	INSERT INTO org.UG_U (UserGroupID, UserID) SELECT @LeaderGroupId, UserID FROM @UserIDTable		  
	IF @@ERROR<>0 AND @@TRANCOUNT>0
	BEGIN		
		ROLLBACK TRANSACTION
		GOTO Finish
	END
	COMMIT TRANSACTION
	Finish:
    --Return @Message
END

GO
