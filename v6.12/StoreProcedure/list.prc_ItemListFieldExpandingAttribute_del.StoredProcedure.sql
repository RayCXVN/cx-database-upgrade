SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[list].[prc_ItemListFieldExpandingAttribute_del]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [list].[prc_ItemListFieldExpandingAttribute_del] AS' 
END
GO
ALTER PROCEDURE [list].[prc_ItemListFieldExpandingAttribute_del]
	@ItemListFieldExpandingAttributeID int,
	@cUserid int,
	@Log smallint = 1
AS
BEGIN
	SET NOCOUNT ON;
	DECLARE @Err Int
	
	IF @Log = 1 
	BEGIN 
		INSERT INTO [Log].[AuditLog] ( UserId, TableName, Type, Data, Created) 
		SELECT @cUserid,'ItemListFieldExpandingAttribute',2,
		( SELECT * FROM [list].[ItemListFieldExpandingAttribute] 
			WHERE
			[ItemListFieldExpandingAttributeID] = @ItemListFieldExpandingAttributeID
			 FOR XML AUTO) as data,
			getdate() 
	END 
	
    DELETE FROM [list].[ItemListFieldExpandingAttribute]
    WHERE
		[ItemListFieldExpandingAttributeID] = @ItemListFieldExpandingAttributeID
	
	Set @Err = @@Error

	RETURN @Err
  
END

GO
