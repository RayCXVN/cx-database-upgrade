SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[rep].[prc_UserActivityDisplaySetting]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [rep].[prc_UserActivityDisplaySetting] AS' 
END
GO
-- Stored Procedure

ALTER PROCEDURE [rep].[prc_UserActivityDisplaySetting]	
	@UserActivitySettingID int = null output,
	@UserID int,
	@ActivityID int,
	@ShowTable smallint = 1,
	@ShowChart smallint = 0,
	@ChartTypeID int = 9,
	@ChartTypeIDAvg int = 7,	
	@TypeIds nvarchar(MAX),
	@Log smallint = 1
AS 
BEGIN
	SET NOCOUNT ON;
	DECLARE @Err int
	
	IF (@UserID = 0 or @ActivityID = 0)
		RETURN -1
		
BEGIN TRANSACTION
	--Detect UserActivitySettingID exist in UserActivitySetting table		
	SET @UserActivitySettingID = (SELECT TOP 1 UserActivitySettingID FROM [rep].[UserActivitySetting] WHERE UserID = @UserId AND ActivityID = @ActivityId)

	IF (@UserActivitySettingID is NULL)
		EXEC [rep].[prc_UserActivitySetting_ins] @UserActivitySettingID output, @UserID, @ActivityID, @ShowTable, @ShowChart, @ChartTypeID, @ChartTypeIDAvg, 1, 0, 0, @UserID, @Log
	ELSE
	BEGIN
		UPDATE [rep].[UserActivitySetting] 
		SET		[ShowTable] = @ShowTable
				,[ShowChart] = @ShowChart
				,[ChartTypeID] = @ChartTypeID
				,[ChartTypeIDAvg] = @ChartTypeIDAvg					
		WHERE [UserActivitySettingID] = @UserActivitySettingID 
		
		IF @Log = 1   
		BEGIN   
		  INSERT INTO [Log].[AuditLog] ( UserId, TableName, Type, Data, Created)   
		  SELECT @UserID,'UserActivitySetting',1,  
		  (SELECT * FROM [rep].[UserActivitySetting]  
		   WHERE  
		   [UserActivitySettingID] = @UserActivitySettingID     FOR XML AUTO) as data,  
			getdate()   
		END
	END

	IF @Log = 1   
	BEGIN   
	  INSERT INTO [Log].[AuditLog] ( UserId, TableName, Type, Data, Created)   
	  SELECT @UserID,'UAS_CalcType',2,  
	  (SELECT * FROM [rep].[UAS_CalcType]  
	   WHERE  
	   [UserActivitySettingID] = @UserActivitySettingID     FOR XML AUTO) as data,  
		getdate()   
	END
	
	--Delete old records 		
	DELETE [rep].[UAS_CalcType] 
	WHERE UserActivitySettingID = @UserActivitySettingID			
	/************/

	IF(@TypeIds = '') GOTO CommitResult
	
	INSERT INTO [rep].[UAS_CalcType] (UserActivitySettingID, ReportCalcTypeID)
	SELECT @UserActivitySettingID, ParseValue FROM [dbo].[StringToArray](@TypeIds,',')
	Set @Err = @@Error
	
	IF @Log = 1   
	BEGIN   
	  INSERT INTO [Log].[AuditLog] ( UserId, TableName, Type, Data, Created)   
	  SELECT @UserID,'UAS_CalcType',0,  
	  (SELECT * FROM [rep].[UAS_CalcType]  
	   WHERE  
	   [UserActivitySettingID] = @UserActivitySettingID     FOR XML AUTO) as data,  
		getdate()   
	END

CommitResult:
	COMMIT TRANSACTION
	RETURN @Err
END

GO
