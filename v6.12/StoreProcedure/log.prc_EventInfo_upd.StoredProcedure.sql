SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[log].[prc_EventInfo_upd]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [log].[prc_EventInfo_upd] AS' 
END
GO
ALTER PROCEDURE [log].[prc_EventInfo_upd]
(
	@EventInfoID	int,
	@EventID		int,
	@EventKeyID	int,
	@Value		nvarchar(max),
	@cUserid		int,
	@Log			smallint = 1
)
AS
BEGIN
	SET NOCOUNT ON
	DECLARE @Err Int

	UPDATE [log].[EventInfo]
	SET
		[EventID] = @EventID,
		[EventKeyID] = @EventKeyID,
		[Value] = @Value
	WHERE
		 [EventInfoID] = @EventInfoID

	Set @Err = @@Error
	
	IF @Log = 1 
	BEGIN 
		INSERT INTO [Log].[AuditLog] ( UserId, TableName, Type, Data, Created) 
		SELECT @cUserid,'EventInfo',1,
		( SELECT * FROM [log].[EventInfo]
			WHERE
			[EventInfoID] = @EventInfoID FOR XML AUTO) as data,
				getdate() 
	END

	RETURN @Err
END

GO
