SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[org].[prc_U_D_ins]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [org].[prc_U_D_ins] AS' 
END
GO

ALTER PROCEDURE [org].[prc_U_D_ins]
(
	@U_DID int = null output,
	@DepartmentID int,
	@UserID int,
	@Selected bit,
	@cUserid int,
	@Log smallint = 1
)
AS
BEGIN
	SET NOCOUNT ON
	DECLARE @Err Int

	INSERT INTO [org].[U_D]
	(
		[DepartmentID],
		[UserID],
		[Selected]
	)
	VALUES
	(
		@DepartmentID,
		@UserID,
		@Selected
	)

	Set @Err = @@Error
	Set @U_DID = scope_identity()

	IF @Log = 1 
	BEGIN 
		INSERT INTO [Log].[AuditLog] ( UserId, TableName, Type, Data, Created) 
		SELECT @cUserid,'U_D',0,
		( SELECT * FROM [org].[U_D] 
			WHERE
			[U_DID] = @U_DID				 FOR XML AUTO) as data,
				getdate() 
	 END

	RETURN @Err
END


GO
