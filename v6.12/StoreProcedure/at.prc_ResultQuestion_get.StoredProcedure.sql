SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[at].[prc_ResultQuestion_get]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [at].[prc_ResultQuestion_get] AS' 
END
GO


ALTER PROCEDURE [at].[prc_ResultQuestion_get] --1,2,2,3
(
    @ActivityID int, 
	@SurveyID 	int,
	@BatchID	int = null,
	@RoleID 	int,
	@DepartmentID 	int
)
As 
    DECLARE @Customerid INT

    SELECT @Customerid = CustomerID FROM org.department WHERE departmentid = @DepartmentID

	Select
      DISTINCT
      Q.[QuestionID],
      Q.[PageID],
      Q.[No]
      FROM [AT].[Question] Q WITH (NOLOCK) 
      INNER JOIN [AT].Page P WITH (NOLOCK) ON Q.PageID = P.PageID AND P.ActivityID = @ActivityID
      LEFT OUTER JOIN [AT].[Access] A WITH (NOLOCK) ON 
      (
         (Q.[QuestionID] = A.[QuestionID]) 
      OR (Q.PageID = A.PageID AND A.QuestionID IS NULL) 
      OR (A.PageID IS NULL AND A.[QuestionID] IS NULL)  
      ) 
      AND A.SurveyID = @SurveyID
      WHERE 
            A.SurveyID = @SurveyID
            AND (A.BatchID = @BatchID Or A.BatchID IS NULL)
            AND (A.RoleID = @RoleID Or A.RoleID IS NULL)
            AND (A.DepartmentID = @DepartmentID OR A.DepartmentID IS NULL)
            AND (A.DepartmentTypeID IS NULL OR A.DepartmentTypeID IN (SELECT DepartmentTypeID FROM org.DT_D with (nolock)  WHERE DepartmentID = @DepartmentID))
			AND (A.CustomerID is NULL OR  A.Customerid = @Customerid)     



GO
