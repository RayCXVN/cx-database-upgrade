SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[Logon_OTP]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [dbo].[Logon_OTP] AS' 
END
GO
/*
    Sarah - Oct 10, 2016 - Remove column Status on User, Department and Result tables
*/
ALTER PROCEDURE [dbo].[Logon_OTP]
(
	@UserName	nvarchar(128),
	@PassWord	nvarchar(64),
	@UserID		int		OUTPUT,
	@LanguageID	int=1		OUTPUT,
	@DepartmentID	int		OUTPUT,
	@Firstname	nvarchar(64) 	OUTPUT,
	@Lastname	nvarchar(64) 	OUTPUT,
	@Email		nvarchar(256) 	OUTPUT,
	@Mobile		nvarchar(16) 	OUTPUT,
	@ExtId		nvarchar(64) 	OUTPUT,
	@OwnerId	int	,
	@ErrNo		int		OUTPUT,
	@ChangePassword bit = 0 OUTPUT,
    @EntityStatusID int OUTPUT
)
AS
BEGIN
DECLARE @ActiveEntityStatusID INT = (SELECT EntityStatusID FROM EntityStatus WHERE CodeName='Active')

SELECT 	
	@DepartmentID = DepartmentID,
	@LanguageID = LanguageID,
	@UserID = UserID,
	@Firstname = Firstname,
	@Lastname = Lastname,
	@Email = Email,
	@Mobile = Mobile,
	@ExtId = ExtId,
	@OwnerId = OwnerId,
	@ChangePassword = ChangePassword,
    @EntityStatusID = EntityStatusID
FROM 	
	[org].[User] 
WHERE 	
	UserName = @UserName
	AND [OneTimePassword] = @Password	
    AND EntityStatusID = @ActiveEntityStatusID AND Deleted IS NULL
    AND [Ownerid] = @OwnerId

IF @@RowCount < 1 
BEGIN
	SET @ErrNo = 1
END
ELSE
BEGIN	
	SET @ErrNo = 0
END
END

GO
