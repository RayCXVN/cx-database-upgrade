SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[job].[add_Status]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [job].[add_Status] AS' 
END
GO
ALTER procedure [job].[add_Status]
	@JobID    bigint,
	@Status   text,
	@Type			smallint
	AS 
	INSERT INTO [JobStatusLog]
	(
		[JobID],
		[Description],
		[JobStatusID],
		Created
	)
	VALUES
	(
		@JobID,
		@Status,
		@Type,
		getdate()
	)
	RETURN @@ERROR
GO
