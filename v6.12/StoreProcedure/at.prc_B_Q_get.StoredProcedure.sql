SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[at].[prc_B_Q_get]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [at].[prc_B_Q_get] AS' 
END
GO
ALTER PROCEDURE [at].[prc_B_Q_get]
(
	@BulkID int
)
AS
BEGIN
	SET NOCOUNT ON
	DECLARE @Err Int

	SELECT
	[BulkID],
	[QuestionID]
	FROM [at].[B_Q]
	WHERE
	[BulkID] = @BulkID

	Set @Err = @@Error

	RETURN @Err
END



GO
