SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[proc].[prc_ProcessAnswerFile_upd]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [proc].[prc_ProcessAnswerFile_upd] AS' 
END
GO

ALTER PROCEDURE [proc].[prc_ProcessAnswerFile_upd]
(
	@ProcessAnswerFileId int,
	@ProcessAnswerId int,
	@ContentType nvarchar(64),
	@Size bigint,
	@Title nvarchar(64),
	@FileName nvarchar(256),
	@Description nvarchar(128),
	@FileData varbinary(max),
	@No smallint,
	@cUserid int,
	@Log smallint = 1
)
AS
BEGIN
	SET NOCOUNT ON
	DECLARE @Err Int

	UPDATE [proc].[ProcessAnswerFile]
	SET
		[ProcessAnswerId] = @ProcessAnswerId,
		[ContentType] = @ContentType,
		[Size] = @Size,
		[Title] = @Title,
		[FileName] = @FileName,
		[Description] = @Description,
		[FileData] = @FileData,
		[No] = @No
	WHERE
		[ProcessAnswerFileId] = @ProcessAnswerFileId

	IF @Log = 1 
	BEGIN 
		INSERT INTO [Log].[AuditLog] ( UserId, TableName, Type, Data, Created) 
		SELECT @cUserid,'ProcessAnswerFile',1,
		( SELECT * FROM [proc].[ProcessAnswerFile] 
			WHERE
			[ProcessAnswerFileId] = @ProcessAnswerFileId			 FOR XML AUTO) as data,
			getdate()
	END

	Set @Err = @@Error

	RETURN @Err
END

GO
