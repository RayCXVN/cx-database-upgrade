SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[rep].[prc_Bubble_Status_upd]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [rep].[prc_Bubble_Status_upd] AS' 
END
GO

ALTER PROCEDURE [rep].[prc_Bubble_Status_upd]
(
	@BubbleID int,
	@SurveyID int,
	@LastProcessedDate datetime,
	@Reprocess SMALLINT=NULL,
	@cUserid int,
	@Log smallint = 1
)
AS
BEGIN
	SET NOCOUNT ON
	DECLARE @Err Int

	UPDATE [rep].[Bubble_Status]
	SET
		[BubbleID] = @BubbleID,
		[SurveyID] = @SurveyID,
		[LastProcessedDate] = @LastProcessedDate,
		[Reprocess] = @Reprocess
	WHERE
		[BubbleID] = @BubbleID AND
		[SurveyID] = @SurveyID

	IF @Log = 1 
	BEGIN 
		INSERT INTO [Log].[AuditLog] ( UserId, TableName, Type, Data, Created) 
		SELECT @cUserid,'Bubble_Status',1,
		( SELECT * FROM [rep].[Bubble_Status] 
			WHERE
			[BubbleID] = @BubbleID AND
			[SurveyID] = @SurveyID			 FOR XML AUTO) as data,
			getdate()
	END

	Set @Err = @@Error

	RETURN @Err
END


GO
