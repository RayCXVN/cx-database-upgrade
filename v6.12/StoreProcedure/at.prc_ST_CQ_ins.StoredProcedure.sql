SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[at].[prc_ST_CQ_ins]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [at].[prc_ST_CQ_ins] AS' 
END
GO
  
ALTER PROCEDURE [at].[prc_ST_CQ_ins]  
(  
 @ST_CQ_ID int = null output,  
 @ScoreTemplateID int,  
 @CategoryID int = null,  
 @QuestionID int = null,  
 @AlternativeID int = null,
 @MinValue float,  
 @MaxValue float,  
 @cUserid int,  
 @Log smallint = 1,
 @ItemID int = NULL
)  
AS  
BEGIN  
 SET NOCOUNT ON  
 DECLARE @Err Int  
  
 INSERT INTO [at].[ST_CQ]  
 (  
  [ScoreTemplateID],  
  [CategoryID],  
  [QuestionID],  
  [AlternativeID],
  [MinValue],  
  [MaxValue],
  [ItemID]  
 )  
 VALUES  
 (  
  @ScoreTemplateID,  
  @CategoryID,  
  @QuestionID,  
  @AlternativeID,
  @MinValue,  
  @MaxValue,
  @ItemID
 )  
  
 Set @Err = @@Error  
 Set @ST_CQ_ID = scope_identity()  
  
 IF @Log = 1   
 BEGIN   
  INSERT INTO [Log].[AuditLog] ( UserId, TableName, Type, Data, Created)   
  SELECT @cUserid,'ST_CQ',0,  
  ( SELECT * FROM [at].[ST_CQ]   
   WHERE  
   [ST_CQ_ID] = @ST_CQ_ID     FOR XML AUTO) as data,  
    getdate()   
  END  
  
 RETURN @Err  
END  
  
  

GO
