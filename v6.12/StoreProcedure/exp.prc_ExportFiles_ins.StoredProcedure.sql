SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[exp].[prc_ExportFiles_ins]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [exp].[prc_ExportFiles_ins] AS' 
END
GO

ALTER PROCEDURE [exp].[prc_ExportFiles_ins]
(
	@ExportFileID int = null output,
	@UserID int,
	@MIMEType nvarchar(64),
	@Data varbinary,
	@EmailSendtTo nvarchar(128),
	@Description nvarchar(128),
	@Size bigint,
	@Filename nvarchar(128),
	@SurveyID INT=NULL,
	@cUserid int,
	@Log smallint = 1
)
AS
BEGIN
	SET NOCOUNT ON
	DECLARE @Err Int

	INSERT INTO [exp].[ExportFiles]
	(
		[UserID],
		[MIMEType],
		[Data],
		[EmailSendtTo],
		[Description],
		[Size],
		[Filename],
		[SurveyID]
	)
	VALUES
	(
		@UserID,
		@MIMEType,
		@Data,
		@EmailSendtTo,
		@Description,
		@Size,
		@Filename,
		@SurveyID
	)

	Set @Err = @@Error
	Set @ExportFileID = scope_identity()

	IF @Log = 1 
	BEGIN 
		INSERT INTO [Log].[AuditLog] ( UserId, TableName, Type, Data, Created) 
		SELECT @cUserid,'ExportFiles',0,
		( SELECT * FROM [exp].[ExportFiles] 
			WHERE
			[ExportFileID] = @ExportFileID				 FOR XML AUTO) as data,
				getdate() 
	 END

	RETURN @Err
END


GO
