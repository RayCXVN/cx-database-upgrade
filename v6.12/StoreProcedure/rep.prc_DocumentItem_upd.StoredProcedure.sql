SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[rep].[prc_DocumentItem_upd]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [rep].[prc_DocumentItem_upd] AS' 
END
GO


ALTER PROCEDURE [rep].[prc_DocumentItem_upd]
(
	@DocumentItemID int,
	@DocumentItemTypeID int,
	@DF_O_ID int,
	@BGColor nvarchar(16),
	@FGColor nvarchar(16),
	@FontSize int,
	@FontName nvarchar(32),
	@FontIsBold int,
	@BorderColor nvarchar(16)='',
	@cUserid int,
	@Log smallint = 1
)
AS
BEGIN
	SET NOCOUNT ON
	DECLARE @Err Int

	UPDATE [rep].[DocumentItem]
	SET
		[DocumentItemTypeID] = @DocumentItemTypeID,
		[DF_O_ID] = @DF_O_ID,
		[BGColor] = @BGColor,
		[FGColor] = @FGColor,
		[FontSize] = @FontSize,
		[FontName] = @FontName,
		[FontIsBold] = @FontIsBold,
		[BorderColor] = @BorderColor
	WHERE
		[DocumentItemID] = @DocumentItemID

	IF @Log = 1 
	BEGIN 
		INSERT INTO [Log].[AuditLog] ( UserId, TableName, Type, Data, Created) 
		SELECT @cUserid,'DocumentItem',1,
		( SELECT * FROM [rep].[DocumentItem] 
			WHERE
			[DocumentItemID] = @DocumentItemID			 FOR XML AUTO) as data,
			getdate()
	END

	Set @Err = @@Error

	RETURN @Err
END



GO
