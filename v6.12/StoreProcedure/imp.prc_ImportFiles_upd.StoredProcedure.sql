SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[imp].[prc_ImportFiles_upd]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [imp].[prc_ImportFiles_upd] AS' 
END
GO

ALTER PROCEDURE [imp].[prc_ImportFiles_upd]
(
	@ImportFileID int,
	@UserID int,
	@MIMEType nvarchar(64),
	@Data varbinary,
	@DataError VARBINARY=NULL,
	@EmailSendtTo nvarchar(128),
	@Description nvarchar(128),
	@Size bigint,
	@SizeError bigint,
	@Filename nvarchar(128),
	@FilenameError nvarchar(128),
	@SurveyID INT=NULL,
	@cUserid int,
	@Log smallint = 1
)
AS
BEGIN
	SET NOCOUNT ON
	DECLARE @Err Int

	UPDATE [imp].[ImportFiles]
	SET
		[UserID] = @UserID,
		[MIMEType] = @MIMEType,
		[Data] = @Data,
		[DataError] = @DataError,
		[EmailSendtTo] = @EmailSendtTo,
		[Description] = @Description,
		[Size] = @Size,
		[SizeError] = @SizeError,
		[Filename] = @Filename,
		[FilenameError] = @FilenameError,
		[SurveyID] = @SurveyID
	WHERE
		[ImportFileID] = @ImportFileID

	IF @Log = 1 
	BEGIN 
		INSERT INTO [Log].[AuditLog] ( UserId, TableName, Type, Data, Created) 
		SELECT @cUserid,'ImportFiles',1,
		( SELECT * FROM [imp].[ImportFiles] 
			WHERE
			[ImportFileID] = @ImportFileID			 FOR XML AUTO) as data,
			getdate()
	END

	Set @Err = @@Error

	RETURN @Err
END


GO
