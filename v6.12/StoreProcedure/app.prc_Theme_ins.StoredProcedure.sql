SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[app].[prc_Theme_ins]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [app].[prc_Theme_ins] AS' 
END
GO

ALTER PROCEDURE [app].[prc_Theme_ins]
(
	@ThemeID int = null output,
	@Name nvarchar(64),
	@StyleSheet nvarchar(256),
	@Parameters nvarchar(max),
	@cUserid int,
	@Log smallint = 1
)
AS
BEGIN
	SET NOCOUNT ON
	DECLARE @Err Int

	INSERT INTO [app].[Theme]
	(
		[Name],
		[StyleSheet],
		[Parameters]
	)
	VALUES
	(
		@Name,
		@StyleSheet,
		@Parameters
	)

	Set @Err = @@Error
	Set @ThemeID = scope_identity()

	IF @Log = 1 
	BEGIN 
		INSERT INTO [Log].[AuditLog] ( UserId, TableName, Type, Data, Created) 
		SELECT @cUserid,'Theme',0,
		( SELECT * FROM [app].[Theme] 
			WHERE
			[ThemeID] = @ThemeID				 FOR XML AUTO) as data,
				getdate() 
	 END

	RETURN @Err
END


GO
