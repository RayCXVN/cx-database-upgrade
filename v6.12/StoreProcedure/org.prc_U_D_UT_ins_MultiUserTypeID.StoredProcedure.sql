SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[org].[prc_U_D_UT_ins_MultiUserTypeID]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [org].[prc_U_D_UT_ins_MultiUserTypeID] AS' 
END
GO
ALTER PROCEDURE [org].[prc_U_D_UT_ins_MultiUserTypeID]
(
	@U_DID int,
	@UsertypeIDs nvarchar(max),
	@cUserid int,
	@Log smallint = 1
)
AS
BEGIN
	SET NOCOUNT ON
	DECLARE @Err Int

	INSERT INTO [org].[U_D_UT]([U_DID],[UsertypeID])  SELECT @U_DID,value from dbo.funcListToTableInt(@UsertypeIDs,',')

	Set @Err = @@Error

	IF @Log = 1 
	BEGIN 
		INSERT INTO [Log].[AuditLog] ( UserId, TableName, Type, Data, Created) 
		SELECT @cUserid,'U_D_UT',0,
		( SELECT * FROM [org].[U_D_UT] 
			WHERE
			[U_DID] = @U_DID AND
			[UsertypeID] IN  (SELECT value from dbo.funcListToTableInt(@UsertypeIDs,',')	)			 FOR XML AUTO) as data,
				getdate() 
	 END

	RETURN @Err
END

GO
