SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[rep].[prc_UserActivitySetting_del]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [rep].[prc_UserActivitySetting_del] AS' 
END
GO
ALTER PROCEDURE [rep].[prc_UserActivitySetting_del]        
(        
 @UserActivitySettingID int,
 @cUserid int,
 @Log smallint = 1
)        
AS        
BEGIN        
 SET NOCOUNT ON        
 DECLARE @Err Int        
        
 IF @Log = 1         
 BEGIN         
  INSERT INTO [Log].[AuditLog] ( UserId, TableName, Type, Data, Created)         
  SELECT @cUserid,'UserActivitySetting',2,        
  (SELECT * FROM [rep].[UserActivitySetting]
   WHERE        
		[UserActivitySettingID] = @UserActivitySettingID
    FOR XML AUTO) as data,        
   getdate()         
 END         
        
        
 DELETE FROM [rep].[UserActivitySetting]
 WHERE        
  [UserActivitySettingID] = @UserActivitySettingID
 Set @Err = @@Error        
        
 RETURN @Err        
END 

GO
