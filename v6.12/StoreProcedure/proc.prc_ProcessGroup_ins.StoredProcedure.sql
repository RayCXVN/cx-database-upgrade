SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[proc].[prc_ProcessGroup_ins]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [proc].[prc_ProcessGroup_ins] AS' 
END
GO
/*
	2017-07-26 Steve: Change the length of column ExtID in all related tables from xAPI format
*/
ALTER PROCEDURE [proc].[prc_ProcessGroup_ins]
(
	@ProcessGroupID int = null output,
	@OwnerID int,
	@CustomerID INT=NULL,
	@Active smallint,
	@UserID int,
	@No smallint,
	@cUserid int,
	@Log smallint = 1,
	@Created DATETIME = NULL,
	@ExtID nvarchar(256)=''
)
AS
BEGIN
	SET NOCOUNT ON
	DECLARE @Err Int

	SET @Created = ISNULL(@Created, GETDATE())

	INSERT INTO [proc].[ProcessGroup]
	(
		[OwnerID],
		[CustomerID],
		[Active],
		[UserID],
		[No],
		[Created],
		[ExtID]
	)
	VALUES
	(
		@OwnerID,
		@CustomerID,
		@Active,
		@UserID,
		@No,
		@Created,
		@ExtID
	)

	Set @Err = @@Error
	Set @ProcessGroupID = scope_identity()

	IF @Log = 1 
	BEGIN 
		INSERT INTO [Log].[AuditLog] ( UserId, TableName, Type, Data, Created) 
		SELECT @cUserid,'ProcessGroup',0,
		( SELECT * FROM [proc].[ProcessGroup] 
			WHERE
			[ProcessGroupID] = @ProcessGroupID				 FOR XML AUTO) as data,
				getdate() 
	 END

	RETURN @Err
END

GO
