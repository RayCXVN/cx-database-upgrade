SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[org].[prc_HDMapping_CheckAccess]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [org].[prc_HDMapping_CheckAccess] AS' 
END
GO
ALTER PROCEDURE [org].[prc_HDMapping_CheckAccess]
(
    @MappingHDIDList varchar(max),
    @CheckingDepartmentIDList varchar(max)
)
AS
BEGIN
    WITH HDID_Recursive AS (
        SELECT hdo.HDID, hdo.ParentID, hdo.DepartmentID
        FROM org.H_D hdo
        WHERE hdo.HDID IN (select value from dbo.funcListToTableInt(@MappingHDIDList,','))
        UNION ALL
        SELECT hdp.HDID, hdp.ParentID, hdp.DepartmentID
        FROM org.H_D hdp
        INNER JOIN HDID_Recursive map ON map.HDID= hdp.ParentID
    )
    SELECT DISTINCT hd.DepartmentID,hd.HDID
    FROM HDID_Recursive hd WHERE hd.DepartmentID IN (select value from dbo.funcListToTableInt(@CheckingDepartmentIDList,','))
END

GO
