SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[org].[prc_DT_UG_ins]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [org].[prc_DT_UG_ins] AS' 
END
GO

ALTER PROCEDURE [org].[prc_DT_UG_ins]
(
	@DepartmentTypeID int,
	@UserGroupID int,	
	@cUserid int,
	@Log smallint = 1
)
AS
BEGIN
	SET NOCOUNT ON
	DECLARE @Err Int

	INSERT INTO [org].[DT_UG]
	(
		[UserGroupID],
		[DepartmentTypeID]
	)
	VALUES
	(
		@UserGroupID,
		@DepartmentTypeID
	)

	Set @Err = @@Error

	IF @Log = 1 
	BEGIN 
		INSERT INTO [Log].[AuditLog] ( UserId, TableName, Type, Data, Created) 
		SELECT @cUserid,'DT_UG',0,
		( SELECT * FROM [org].[DT_UG] 
			WHERE
			[UserGroupID] = @UserGroupID AND
			[DepartmentTypeID] = @DepartmentTypeID	 FOR XML AUTO) as data,
				getdate() 
	 END

	RETURN @Err
END


GO
