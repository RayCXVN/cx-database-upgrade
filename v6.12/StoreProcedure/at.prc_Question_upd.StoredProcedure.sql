SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[at].[prc_Question_upd]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [at].[prc_Question_upd] AS' 
END
GO
ALTER PROCEDURE [at].[prc_Question_upd](
    @QuestionID int,
    @PageID     int,
    @ScaleID    int           = NULL,
    @No         [smallint],
    @Type       [smallint],
    @Inverted   bit,
    @Mandatory  bit,
    @Status     [smallint],
    @CssClass   nvarchar(64),
    @ExtId      nvarchar(256),
    @Tag        nvarchar(128),
    @SectionID  int           = NULL,
    @cUserid    int,
    @Log        [smallint]    = 1,
    @Width      int           = NULL)
AS
BEGIN
    SET NOCOUNT ON;
    DECLARE @Err int;

    UPDATE [at].[Question] SET [PageID] = @PageID, [ScaleID] = @ScaleID, [No] = @No, [Type] = @Type, [Inverted] = @Inverted, [Mandatory] = @Mandatory,
                                [Status] = @Status, [CssClass] = @CssClass, [ExtId] = @ExtId, [Tag] = @Tag, [SectionID] = @SectionID, [Width] = @Width
    WHERE [QuestionID] = @QuestionID;

    IF @Log = 1
    BEGIN
        INSERT INTO [Log].[AuditLog] ([UserId], [TableName], [Type], [Data], [Created])
        SELECT @cUserid, 'Question', 1, (SELECT * FROM [at].[Question] WHERE [QuestionID] = @QuestionID FOR XML AUTO) AS [data], GETDATE();
    END;

    SET @Err = @@Error;

    RETURN @Err;
END;

GO