SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[at].[prc_LT_Question_ins]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [at].[prc_LT_Question_ins] AS' 
END
GO
ALTER PROCEDURE [at].[prc_LT_Question_ins]
(
	@LanguageID int,
	@QuestionID int,
	@Name ntext,
	@Title ntext,
	@ReportText nvarchar(512),
	@Description nvarchar(max),
	@ShortName nvarchar(64) = '',
	@cUserid int,
	@Log smallint = 1
)
AS
BEGIN
	SET NOCOUNT ON
	DECLARE @Err Int

	INSERT INTO [at].[LT_Question]
	(
		[LanguageID],
		[QuestionID],
		[Name],
		[Title],
		[ReportText],
		[Description],
		[ShortName]
	)
	VALUES
	(
		@LanguageID,
		@QuestionID,
		@Name,
		@Title,
		@ReportText,
		@Description,
		@ShortName
	)

	Set @Err = @@Error

	IF @Log = 1 
	BEGIN 
		INSERT INTO [Log].[AuditLog] ( UserId, TableName, Type, Data, Created) 
		SELECT @cUserid,'LT_Question',0,
		( SELECT * FROM [at].[LT_Question] 
			WHERE
			[LanguageID] = @LanguageID AND
			[QuestionID] = @QuestionID				 FOR XML AUTO) as data,
				getdate() 
	 END

	RETURN @Err
END


GO
