SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[org].[prc_UserSoftDelete_get]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [org].[prc_UserSoftDelete_get] AS' 
END
GO
/*  2016-10-10 Ray: Remove column Status from org.User, org.Department, RDB.dbo.Result
*/
ALTER PROCEDURE [org].[prc_UserSoftDelete_get]
(  
 @DepartmentID int  
)  
AS  
BEGIN  
 SET NOCOUNT ON  
 DECLARE @Err Int, @EntityStatusID INT = 0  
 SELECT @EntityStatusID = EntityStatusID FROM dbo.EntityStatus WHERE CodeName = N'Deactive'

 SELECT  
 [UserID],  
 [Ownerid],  
 [DepartmentID],  
 [LanguageID],  
 ISNULL([RoleID], 0) AS 'RoleID',  
 [UserName],  
 [Password],  
 [LastName],  
 [FirstName],  
 [Email],  
 [Mobile],  
 [ExtID],  
 [SSN],  
 [Tag],  
 [Locked],  
 [ChangePassword],  
 [HashPassword],  
 [SaltPassword],  
 [OneTimePassword],  
 [OTPExpireTime],  
 [Created],  
 [CountryCode],
 [Gender],
 [DateOfBirth],
 [ForceUserLoginAgain],
 [LastUpdated],
 [LastUpdatedBy],
 [LastSynchronized],
 [ArchetypeID],
 [CustomerID],
 [Deleted],
 [EntityStatusID],
 [EntityStatusReasonID]
 FROM [org].[User]  
 WHERE  
 [DepartmentID] = @DepartmentID  
 AND [EntityStatusID] = @EntityStatusID 
 AND Deleted IS NULL 

 Set @Err = @@Error  

 RETURN @Err  
END  

GO
