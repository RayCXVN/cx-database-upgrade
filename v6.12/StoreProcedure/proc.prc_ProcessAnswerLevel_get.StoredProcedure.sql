SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[proc].[prc_ProcessAnswerLevel_get]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [proc].[prc_ProcessAnswerLevel_get] AS' 
END
GO

ALTER PROCEDURE [proc].[prc_ProcessAnswerLevel_get]
(
	@ProcessAnswerID int
)
AS
BEGIN
	SET NOCOUNT ON
	DECLARE @Err Int

	SELECT
	[ProcessAnswerID],
	[ProcessLevelID],
	[Description],
	[Created]
	FROM [proc].[ProcessAnswerLevel]
	WHERE
	[ProcessAnswerID] = @ProcessAnswerID

	Set @Err = @@Error

	RETURN @Err
END


GO
