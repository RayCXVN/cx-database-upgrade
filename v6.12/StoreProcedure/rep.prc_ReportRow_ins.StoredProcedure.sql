SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[rep].[prc_ReportRow_ins]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [rep].[prc_ReportRow_ins] AS' 
END
GO

ALTER PROCEDURE [rep].[prc_ReportRow_ins]
(
	@ReportRowID int = null output,
	@ReportPartID int,
	@ReportRowTypeID int,
	@No smallint,
	@CssClass varchar(64),
	@cUserid int,
	@Log smallint = 1
)
AS
BEGIN
	SET NOCOUNT ON
	DECLARE @Err Int

	INSERT INTO [rep].[ReportRow]
	(
		[ReportPartID],
		[ReportRowTypeID],
		[No],
		[CssClass]
	)
	VALUES
	(
		@ReportPartID,
		@ReportRowTypeID,
		@No,
		@CssClass
	)

	Set @Err = @@Error
	Set @ReportRowID = scope_identity()

	IF @Log = 1 
	BEGIN 
		INSERT INTO [Log].[AuditLog] ( UserId, TableName, Type, Data, Created) 
		SELECT @cUserid,'ReportRow',0,
		( SELECT * FROM [rep].[ReportRow] 
			WHERE
			[ReportRowID] = @ReportRowID				 FOR XML AUTO) as data,
				getdate() 
	 END

	RETURN @Err
END


GO
