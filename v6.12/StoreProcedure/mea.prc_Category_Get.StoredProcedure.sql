SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[mea].[prc_Category_Get]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [mea].[prc_Category_Get] AS' 
END
GO
ALTER PROCEDURE [mea].[prc_Category_Get]

AS
BEGIN
	SELECT [CategoryId]
      ,[MeasureTypeId]
      ,[CustomerId]
      ,[CreatedBy]
      ,[CreatedDate]
      ,[ChangedBy]
      ,[ChangedDate]
  FROM [mea].[Category]

END


GO
