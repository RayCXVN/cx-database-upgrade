SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[note].[prc_Note_ins]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [note].[prc_Note_ins] AS' 
END
GO
/*
	2017-06-13 Steve: Change the length of column ExtID in all related tables from xAPI format
*/
ALTER PROCEDURE [note].[prc_Note_ins]
(
	@NoteID int = null output,
	@NoteTypeID int,
	@Active bit,
	@ExtID nvarchar(256),
	@NotifyAtNextLogin bit,
	@StartDate datetime,
	@EndDate datetime,
	@CreatedBy int,
	@ChangedBy int,
	@Deleted bit,
	@cUserid int,
	@Log smallint = 1
)
AS
BEGIN
	SET NOCOUNT ON
	DECLARE @Err Int

	INSERT INTO [note].[Note]
	(
		[NoteTypeID],
		[Active],
		[ExtID],
		[NotifyAtNextLogin],
		[StartDate],
		[EndDate],
		[CreatedBy],
		[ChangedBy],
		[Deleted]
	)
	VALUES
	(
		@NoteTypeID,
		@Active,
		@ExtID,
		@NotifyAtNextLogin,
		@StartDate,
		@EndDate,
		@CreatedBy,
		@ChangedBy,
		@Deleted
	)

	Set @Err = @@Error
	Set @NoteID = scope_identity()

	IF @Log = 1 
	BEGIN 
		INSERT INTO [Log].[AuditLog] ( UserId, TableName, Type, Data, Created) 
		SELECT @cUserid,'Note',0,
		( SELECT * FROM [note].[Note] 
			WHERE
			[NoteID] = @NoteID FOR XML AUTO) as data,
				getdate() 
	 END

	RETURN @Err
END

GO
