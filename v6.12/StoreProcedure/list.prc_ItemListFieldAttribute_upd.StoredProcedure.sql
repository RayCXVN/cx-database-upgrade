SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[list].[prc_ItemListFieldAttribute_upd]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [list].[prc_ItemListFieldAttribute_upd] AS' 
END
GO
ALTER PROCEDURE [list].[prc_ItemListFieldAttribute_upd]
	@ItemListFieldAttributeID int,
	@ItemListFieldID int,
	@key nvarchar(64),
	@Value nvarchar(max)='',
    @cUserid int,
    @Log smallint = 1
AS
BEGIN
	SET NOCOUNT ON;
	DECLARE @Err Int
	
    UPDATE [list].[ItemListFieldAttribute]
    SET 
		[ItemListFieldID] = @ItemListFieldID,
        [key] = @key,
        [Value] = @Value
     WHERE 
		[ItemListFieldAttributeID] = @ItemListFieldAttributeID
		
	IF @Log = 1 
	BEGIN 
		INSERT INTO [Log].[AuditLog] ( UserId, TableName, Type, Data, Created) 
		SELECT @cUserid,'ItemListFieldAttribute',1,
		( SELECT * FROM [list].[ItemListFieldAttribute] 
			WHERE
			[ItemListFieldAttributeID] = @ItemListFieldAttributeID			 FOR XML AUTO) as data,
			getdate()
	END

	Set @Err = @@Error

	RETURN @Err	
END

GO
