SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[org].[prc_GetUserGroup_ByCutomerID]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [org].[prc_GetUserGroup_ByCutomerID] AS' 
END
GO
/*
	Steve - Sept 22 2016 - EntityStatus for table UserGroup
*/
ALTER PROCEDURE [org].[prc_GetUserGroup_ByCutomerID]
(
	@UserID				INT,
	@CustomerID			INT,
	@OwnerID			INT,
	@UserGroupTypeID	varchar(max)
)
AS
BEGIN
    DECLARE @ActiveEntityStatusID INT = (SELECT EntityStatusID FROM EntityStatus WHERE CodeName='Active')

	SELECT 
		ug.UserGroupID,
		ug.OwnerID,
		ug.DepartmentID,
		ug.SurveyId,
		ug.Name,
		ug.Description,
		ug.Created,
		ug.ExtID,
		ug.PeriodID,
		ug.UserID,
		ug.UserGroupTypeID,
		ug.Tag
	 FROM org.UserGroup ug
		INNER JOIN org.Department d ON d.DepartmentID = ug.DepartmentID
		INNER JOIN org.Customer c ON d.CustomerID = c.CustomerID
	WHERE 
		ug.UserID =@UserID			AND 
		c.CustomerID =@CustomerID	AND
		ug.OwnerID = @OwnerID		AND
		ug.UserGroupTypeID IN (SELECT value FROM dbo.funcListToTableInt(@UserGroupTypeID,','))
		AND ug.EntityStatusID = @ActiveEntityStatusID AND ug.Deleted IS NULL
END

GO
