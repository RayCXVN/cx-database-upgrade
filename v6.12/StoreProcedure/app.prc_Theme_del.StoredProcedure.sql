SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[app].[prc_Theme_del]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [app].[prc_Theme_del] AS' 
END
GO
ALTER PROCEDURE [app].[prc_Theme_del]
(
	@ThemeID int,
	@cUserid int,
	@Log smallint = 1
)
AS
BEGIN
	SET NOCOUNT ON
	DECLARE @Err Int

	IF @Log = 1 
	BEGIN 
		INSERT INTO [Log].[AuditLog] ( UserId, TableName, Type, Data, Created) 
		SELECT @cUserid,'Theme',2,
		( SELECT * FROM [app].[Theme] 
			WHERE
			[ThemeID] = @ThemeID
			 FOR XML AUTO) as data,
			getdate() 
	END 


	DELETE FROM [app].[Theme]
	WHERE
		[ThemeID] = @ThemeID

	Set @Err = @@Error

	RETURN @Err
END


GO
