SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[at].[prc_Action_get]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [at].[prc_Action_get] AS' 
END
GO
ALTER PROCEDURE [at].[prc_Action_get]
(
	@ChoiceID int
)
AS
BEGIN
	SET NOCOUNT ON
	DECLARE @Err Int

	SELECT
	[ActionID],
	[ChoiceID],
	[No],
	ISNULL([PageID], 0) AS 'PageID',
	ISNULL([QuestionID], 0) AS 'QuestionID',
	ISNULL([AlternativeID], 0) AS 'AlternativeID',
	[Type],
	[Created],
	[AVTID]
	FROM [at].[Action]
	WHERE
	[ChoiceID] = @ChoiceID
	ORDER BY [No]

	Set @Err = @@Error

	RETURN @Err
END

GO
