SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[at].[prc_LevelGroup_copy]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [at].[prc_LevelGroup_copy] AS' 
END
GO
-- Select * from app.menu
-- select dbo.DecryptString(password),* from org.[user] where ownerid = 5
-- [AT].[prc_LevelGroup_copy] 0,null, 17
--update at.activity set ownerid = 5 where activityid = 26

ALTER PROC [at].[prc_LevelGroup_copy]
(	
	@LevelGroupID	INT,
	@NewLGID	INT = NULL OUTPUT,
	@ActivityID		INT = NULL
)
AS
BEGIN
SET NOCOUNT ON
SET XACT_ABORT ON
BEGIN TRANSACTION
CREATE TABLE #LevelGroup
(
 ID	BIGINT,
 NEWID BIGINT
)
CREATE TABLE #LevelLimit
(
 ID	BIGINT,
 NEWID BIGINT
)

CREATE TABLE #RepLevelLimit
(
 ID	BIGINT,
 NEWID BIGINT
)

DECLARE
@ID	INT,
@NewID INT,
@CurOwnerID INT


-- LevelGroup
IF @LevelGroupID = 0 
BEGIN
	INSERT AT.LevelGroup(ActivityID, NO)
	SELECT TOP 1 ActivityID, NO FROM (
	SELECT ActivityID, MAX(NO) + 1 AS NO
	FROM AT.LevelGroup
	WHERE ActivityID = @ActivityID
	GROUP BY ActivityID
	UNION
	SELECT @ActivityID AS ActivityID ,1 AS NO ) AS A
	ORDER BY A.No DESC
	
	SET @NewLGID = SCOPE_IDENTITY()
	--PRINT @NewLGID
END
ELSE
BEGIN
	INSERT AT.LevelGroup(ActivityID, Tag, NO, CustomerID, Departmentid, Roleid)
	SELECT ActivityID, Tag, NO, CustomerID, Departmentid, Roleid
	FROM AT.LevelGroup
	WHERE LevelGroupID = @LevelGroupID
	SET @NewLGID = SCOPE_IDENTITY()
END

SELECT @curOwnerid = ownerid FROM at.levelgroup lg 
JOIN at.activity a ON a.activityid = lg.activityid  
AND  lg.levelgroupid =  @NewLGID

INSERT AT.LT_LevelGroup(LanguageID, LevelGroupID, Name, Description)
SELECT LanguageID, @NewLGID, Name, Description
FROM AT.LT_LevelGroup 
WHERE LevelGroupID = @LevelGroupID 


-- LevelLimits
IF @LevelGroupID = 0 
BEGIN
	DECLARE curLL CURSOR LOCAL FAST_FORWARD READ_ONLY FOR
	SELECT LevelLimitID FROM AT.LevelLimit
	WHERE LevelGroupID IS NULL
	AND (CategoryID IN (SELECT CategoryID FROM at.Category WHERE ActivityID = @ActivityID)
		OR
		QuestionID IN (SELECT Q.QuestionID FROM at.Question Q INNER JOIN at.Page P ON Q.PageID = P.PageID WHERE P.ActivityID = @ActivityID))		
END
ELSE
BEGIN
	DECLARE curLL CURSOR LOCAL FAST_FORWARD READ_ONLY FOR
	SELECT LevelLimitID FROM AT.LevelLimit
	WHERE LevelGroupID = @LevelGroupID 
END

OPEN curLL
FETCH NEXT FROM curLL INTO @ID

WHILE (@@FETCH_STATUS = 0)
BEGIN
	INSERT AT.LevelLimit(LevelGroupID, CategoryID, QuestionID, MinValue, MaxValue, Sigchange, OwnerColorID , Created,NegativeTrend)
	SELECT @NewLGID, 
	        CategoryID, 
	        QuestionID, 
	        MinValue, MaxValue, Sigchange, OwnerColorID, GETDATE(),NegativeTrend
	FROM AT.LevelLimit ll
	WHERE  LevelLimitID = @ID
	SET @NewID = SCOPE_IDENTITY()
	--PRINT @NewID
	INSERT #LevelLimit(ID, NEWID) VALUES(@ID, @NewID)

	FETCH NEXT FROM curLL INTO @ID
END
CLOSE curLL
DEALLOCATE curLL

INSERT AT.LT_LevelLimit(LanguageID, LevelLimitID, Name, Description)
SELECT LanguageID, LL.[NEWID], Name, Description
FROM #LevelLimit LL
INNER JOIN AT.LT_LevelLimit LL2 ON LL.ID = LL2.LevelLimitID
  
  
-- RepLevelLimits
IF @LevelGroupID > 0
BEGIN
  DECLARE curRLL CURSOR LOCAL FAST_FORWARD READ_ONLY FOR
  SELECT RepLevelLimitID FROM Rep.RepLevelLimit
  WHERE LevelGroupID = @LevelGroupID 
END
ELSE
BEGIN
  DECLARE curRLL CURSOR LOCAL FAST_FORWARD READ_ONLY FOR
  SELECT RepLevelLimitID FROM rep.repLevelLimit ll
  JOIN rep.reportcolumn  rc ON ll.reportcolumnid = rc.reportcolumnid
  JOIN rep.reportrow rw ON rw.reportrowid = rc.reportrowid
  JOIN rep.reportpart rp ON rw.reportpartid = rp.reportpartid
  JOIN rep.report r ON r.reportid = rp.reportid AND r.ownerid = @curOwnerid 
END

OPEN curRLL
FETCH NEXT FROM curRLL INTO @ID

WHILE (@@FETCH_STATUS = 0)
BEGIN
	INSERT Rep.RepLevelLimit(LevelGroupID, ReportColumnID, ReportID, MinValue, MaxValue, Sigchange, Fillcolor, BorderColor, Created,NegativeTrend)
	SELECT @NewLGID, 
	        ReportColumnID, ReportID, MinValue, MaxValue, Sigchange, Fillcolor, BorderColor, Created,NegativeTrend
	FROM Rep.RepLevelLimit ll
	WHERE  RepLevelLimitID = @ID
	SET @NewID = SCOPE_IDENTITY()

	INSERT #RepLevelLimit(ID, NEWID) VALUES(@ID, @NewID)

	FETCH NEXT FROM curRLL INTO @ID
END
CLOSE curRLL
DEALLOCATE curRLL

INSERT Rep.LT_RepLevelLimit(LanguageID, RepLevelLimitID, Name, Description)
SELECT LanguageID, LL.[NEWID], Name, Description
FROM #RepLevelLimit LL
INNER JOIN REP.LT_RepLevelLimit LL2 ON LL.ID = LL2.RepLevelLimitID  

COMMIT TRANSACTION

END



GO
