SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[form].[prc_FormFieldCondition_del]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [form].[prc_FormFieldCondition_del] AS' 
END
GO

ALTER PROCEDURE [form].[prc_FormFieldCondition_del] @FormFieldConditionID INT
	,@cUserid INT
	,@Log SMALLINT = 1
AS
BEGIN
	SET NOCOUNT ON;

	DECLARE @Err INT

	IF @Log = 1
	BEGIN
		INSERT INTO [Log].[AuditLog] (
			UserId
			,TableName
			,Type
			,Data
			,Created
			)
		SELECT @cUserid
			,'FormFieldCondition'
			,2
			,(
				SELECT *
				FROM [form].[FormFieldCondition]
				WHERE [FormFieldConditionID] = @FormFieldConditionID
				FOR XML AUTO
				) AS data
			,getdate()
	END

	DELETE
	FROM [form].[FormFieldCondition]
	WHERE [FormFieldConditionID] = @FormFieldConditionID

	SET @Err = @@Error

	RETURN @Err
END

GO
