SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[prc_RelationType_get]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [dbo].[prc_RelationType_get] AS' 
END
GO

ALTER PROCEDURE [dbo].[prc_RelationType_get]
(
	@OwnerID int
)
AS
BEGIN
	SET NOCOUNT ON
	DECLARE @Err Int

	SELECT
	[RelationTypeID],
	ISNULL([OwnerID], 0) AS 'OwnerID',
	ISNULL([No], 0) AS 'No',
	[Created]
	FROM [dbo].[RelationType]
	WHERE
	[OwnerID] = @OwnerID
	ORDER BY [No]

	Set @Err = @@Error

	RETURN @Err
END


GO
