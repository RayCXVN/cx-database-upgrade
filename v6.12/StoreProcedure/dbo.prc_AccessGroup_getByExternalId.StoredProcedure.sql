SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[prc_AccessGroup_getByExternalId]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [dbo].[prc_AccessGroup_getByExternalId] AS' 
END
GO


ALTER PROCEDURE [dbo].[prc_AccessGroup_getByExternalId]  
(
@OwnerId INT,
@UserId INT,
@Departmentid INT
)
AS

BEGIN

DECLARE @RoleId INT
DECLARE @CustomerID INT

--Select Role ID
SELECT @RoleId = RoleID
FROM org.[User]
WHERE org.[User].UserID = @UserId

--Select CustomerID
SELECT @CustomerID = CustomerID
FROM org.Department
WHERE DepartmentID = @Departmentid

CREATE TABLE #TempUserAccessGroup
(
 UserId INT,
 DepartmentId INT,
 AccessGroupID INT
)

CREATE TABLE #AccessGroupByUser (AccessGroupID int, OwnerID int, [No] smallint, Created smalldatetime)

INSERT INTO #AccessGroupByUser 
Exec prc_AccessGroup_getByUserid @OwnerId,@UserId,@Departmentid,@RoleId,@CustomerID

INSERT INTO #TempUserAccessGroup
( UserId ,
  DepartmentId ,
  AccessGroupID
)
(SELECT @UserId, @Departmentid, AccessGroupID
	FROM #AccessGroupByUser
)

SELECT * FROM #TempUserAccessGroup FOR XML RAW ('AG'); 

DROP TABLE #TempUserAccessGroup
DROP TABLE #AccessGroupByUser

END

GO
