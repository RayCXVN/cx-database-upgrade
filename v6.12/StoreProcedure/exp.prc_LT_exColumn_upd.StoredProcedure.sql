SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[exp].[prc_LT_exColumn_upd]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [exp].[prc_LT_exColumn_upd] AS' 
END
GO

ALTER PROCEDURE [exp].[prc_LT_exColumn_upd]
(
	@LanguageID int,
	@ColumnID smallint,
	@Name nvarchar(256),
	@cUserid int,
	@Log smallint = 1
)
AS
BEGIN
	SET NOCOUNT ON
	DECLARE @Err Int

	UPDATE [exp].[LT_exColumn]
	SET
		[LanguageID] = @LanguageID,
		[ColumnID] = @ColumnID,
		[Name] = @Name
	WHERE
		[LanguageID] = @LanguageID AND
		[ColumnID] = @ColumnID

	IF @Log = 1 
	BEGIN 
		INSERT INTO [Log].[AuditLog] ( UserId, TableName, Type, Data, Created) 
		SELECT @cUserid,'LT_exColumn',1,
		( SELECT * FROM [exp].[LT_exColumn] 
			WHERE
			[LanguageID] = @LanguageID AND
			[ColumnID] = @ColumnID			 FOR XML AUTO) as data,
			getdate()
	END

	Set @Err = @@Error

	RETURN @Err
END


GO
