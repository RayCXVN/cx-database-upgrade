SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[org].[prc_Owner_sel]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [org].[prc_Owner_sel] AS' 
END
GO
ALTER PROCEDURE [org].[prc_Owner_sel]
(
  @Ownerid AS INT
)
AS
BEGIN
	SET NOCOUNT ON
	DECLARE @Err INT

	SELECT
	[OwnerID],
	[Name],
	[ReportServer],
	[ReportDB],
	ISNULL([MainHierarchyId], 0) AS 'MainHierarchyId',
	[OLAPServer],
	[OLAPDB],
	[Css],
	[Url],
	[LoginType],
	[PREFIX],
	[Description],
	[Logging],
	ISNULL([Created], 0) AS 'Created',
	OTPLength,
	OTPCharacters,
	OTPAllowLowercase,
	OTPAllowUppercase,
	UseOTPCaseSensitive,
	[UseHashPassword],
	[UseOTP],
	[DefaultHashMethod],
	[OTPDuration]
	
	FROM [org].[Owner]
	WHERE OwnerID = @Ownerid
	
	SET @Err = @@ERROR

	RETURN @Err
END

GO
