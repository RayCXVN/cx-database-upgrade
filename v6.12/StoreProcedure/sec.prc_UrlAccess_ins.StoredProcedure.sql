SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[sec].[prc_UrlAccess_ins]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [sec].[prc_UrlAccess_ins] AS' 
END
GO
ALTER PROCEDURE [sec].[prc_UrlAccess_ins]    
   (
   @UrlAccessID int = null output,  
   @SiteId  Integer,
   @Controller  nvarchar(128),
   @Action  nvarchar(128),
   @MenuItemId AS INTEGER,
   @cUserid int,  
   @Log smallint = 1  
)  
AS  
BEGIN  
 SET NOCOUNT ON  
 DECLARE @Err Int  
  
 INSERT INTO [sec].[UrlAccess]
           ([SiteId]
           ,[Controller]
           ,[Action]
           ,[MenuItemId])
     VALUES
     (@SiteId,
     @Controller,
     @Action,
     @MenuItemId)
  
 Set @Err = @@Error  
 Set @UrlAccessID = scope_identity()  
  
 IF @Log = 1   
 BEGIN   
  INSERT INTO [Log].[AuditLog] ( UserId, TableName, Type, Data, Created)   
  SELECT @cUserid,'UrlAccess',0,  
  ( SELECT * FROM [sec].[UrlAccess]   
   WHERE  
   [UrlAccessID] = @UrlAccessID     FOR XML AUTO) as data,  
    getdate()   
  END  
  
 RETURN @Err  
END

GO
