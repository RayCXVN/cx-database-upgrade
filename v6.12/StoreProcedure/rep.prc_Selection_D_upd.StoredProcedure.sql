SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[rep].[prc_Selection_D_upd]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [rep].[prc_Selection_D_upd] AS' 
END
GO

ALTER PROCEDURE [rep].[prc_Selection_D_upd]
(
	@SelectionID int,
	@DepartmentID int,
	@cUserid int,
	@Log smallint = 1
)
AS
BEGIN
	SET NOCOUNT ON
	DECLARE @Err Int

	UPDATE [rep].[Selection_D]
	SET
		[SelectionID] = @SelectionID,
		[DepartmentID] = @DepartmentID
	WHERE
		[SelectionID] = @SelectionID AND
		[DepartmentID] = @DepartmentID

	IF @Log = 1 
	BEGIN 
		INSERT INTO [Log].[AuditLog] ( UserId, TableName, Type, Data, Created) 
		SELECT @cUserid,'Selection_D',1,
		( SELECT * FROM [rep].[Selection_D] 
			WHERE
			[SelectionID] = @SelectionID AND
			[DepartmentID] = @DepartmentID			 FOR XML AUTO) as data,
			getdate()
	END

	Set @Err = @@Error

	RETURN @Err
END


GO
