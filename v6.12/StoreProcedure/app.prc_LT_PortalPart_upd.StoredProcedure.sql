SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[app].[prc_LT_PortalPart_upd]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [app].[prc_LT_PortalPart_upd] AS' 
END
GO


ALTER PROCEDURE [app].[prc_LT_PortalPart_upd]
(
	@LanguageID int,
	@PortalPartID int,
	@Name nvarchar(256),
	@Description ntext,
	@Text ntext,
	@TextAbove nvarchar(max),
	@TextBelow nvarchar(max),
	@HeaderText nvarchar(max) = '',
	@FooterText nvarchar(max) = '',
	@BeforeContentText nvarchar(max) = '',
	@AfterContentText nvarchar(max) = '',
	@cUserid int,
	@Log smallint = 1
)
AS
BEGIN
	SET NOCOUNT ON
	DECLARE @Err Int

	UPDATE [app].[LT_PortalPart]
	SET
		[LanguageID] = @LanguageID,
		[PortalPartID] = @PortalPartID,
		[Name] = @Name,
		[Description] = @Description,
		[Text] = @Text,
		[TextAbove] = @TextAbove,
		[TextBelow] = @TextBelow,
		[HeaderText] = @HeaderText,
		[FooterText] = @FooterText,
		[BeforeContentText] = @BeforeContentText,
		[AfterContentText] = @AfterContentText
	WHERE
		[LanguageID] = @LanguageID AND
		[PortalPartID] = @PortalPartID

	IF @Log = 1 
	BEGIN 
		INSERT INTO [Log].[AuditLog] ( UserId, TableName, Type, Data, Created) 
		SELECT @cUserid,'LT_PortalPart',1,
		( SELECT * FROM [app].[LT_PortalPart] 
			WHERE
			[LanguageID] = @LanguageID AND
			[PortalPartID] = @PortalPartID			 FOR XML AUTO) as data,
			getdate()
	END

	Set @Err = @@Error

	RETURN @Err
END



GO
