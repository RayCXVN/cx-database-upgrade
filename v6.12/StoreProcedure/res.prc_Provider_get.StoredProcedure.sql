SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[res].[prc_Provider_get]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [res].[prc_Provider_get] AS' 
END
GO
ALTER PROCEDURE [res].[prc_Provider_get]  
 @OwnerID int  
AS  
BEGIN  
 SET NOCOUNT ON;  
 DECLARE @Err Int  
   
 SELECT [ProviderId]
       ,[Icon]
       ,[OwnerId]
       ,[CustomerId]
       ,[LinkGenerationMethod]
       ,[ExtID]
	   ,[UseLogging]
 FROM [res].[Provider]  
 WHERE [OwnerID] = @OwnerID  
   
 Set @Err = @@Error  
  
 RETURN @Err  
    
END

GO
