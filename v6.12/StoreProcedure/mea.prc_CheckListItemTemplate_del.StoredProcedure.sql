SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[mea].[prc_CheckListItemTemplate_del]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [mea].[prc_CheckListItemTemplate_del] AS' 
END
GO
ALTER PROCEDURE [mea].[prc_CheckListItemTemplate_del]
(
	@CheckListItemTemplateID int,
	@cUserid int,
	@Log smallint = 1
)
AS  
BEGIN  
	SET NOCOUNT ON  
	DECLARE @Err Int  
  
	IF @Log = 1 
	BEGIN 
		INSERT INTO [Log].[AuditLog] ( UserId, TableName, Type, Data, Created) 
		SELECT @cUserid,'mea.CheckListItemTemplate',2,
		( SELECT * FROM [mea].[CheckListItemTemplate] 
			WHERE
			[CheckListItemTemplateID] = @CheckListItemTemplateID FOR XML AUTO) as data,
				getdate() 
	END

	DELETE FROM mea.CheckListItemTemplate
	WHERE
		[CheckListItemTemplateID] = @CheckListItemTemplateID
	
	Set @Err = @@Error  
	
	RETURN @Err
END

GO
