SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[at].[prc_A_ChartType_upd]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [at].[prc_A_ChartType_upd] AS' 
END
GO

ALTER PROCEDURE [at].[prc_A_ChartType_upd]
(
	@AChartTypeID int,
	@ReportChartTypeID int,
	@ActivityID int,
	@No smallint,
	@DefaultSelected smallint,
	@cUserid int,
	@Log smallint = 1
)
AS
BEGIN
	SET NOCOUNT ON
	DECLARE @Err Int

	UPDATE [at].[A_ChartType]
	SET
		[ReportChartTypeID] = @ReportChartTypeID,
		[ActivityID] = @ActivityID,
		[No] = @No,
		[DefaultSelected] = @DefaultSelected
	WHERE
		[AChartTypeID] = @AChartTypeID

	IF @Log = 1 
	BEGIN 
		INSERT INTO [Log].[AuditLog] ( UserId, TableName, Type, Data, Created) 
		SELECT @cUserid,'A_ChartType',1,
		( SELECT * FROM [at].[A_ChartType] 
			WHERE
			[AChartTypeID] = @AChartTypeID			 FOR XML AUTO) as data,
			getdate()
	END

	Set @Err = @@Error

	RETURN @Err
END


GO
