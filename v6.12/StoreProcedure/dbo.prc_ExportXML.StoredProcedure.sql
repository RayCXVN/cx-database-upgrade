SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[prc_ExportXML]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [dbo].[prc_ExportXML] AS' 
END
GO
/*
    Sarah - Oct 10, 2016 - Remove column Status on User, Department and Result tables
*/
ALTER Proc [dbo].[prc_ExportXML]
(
    @SurveyID INTEGER
)
AS
BEGIN
    DECLARE @ActiveEntityStatusID INT = (SELECT EntityStatusID FROM EntityStatus WHERE CodeName='Active')

    SELECT Result.ResultID, 
        convert(VARCHAR(19),Result.StartDate,120) AS StartDate, 
        convert(VARCHAR(19),Result.EndDate,120) AS EndDate, Result.RoleID, Result.UserID, 
        Result.SurveyID, Result.BatchID, Result.LanguageID, Result.PageNo, Result.DepartmentID, 
        Result.Anonymous, 
        Result.Email, Result.EntityStatusID, Result.EntityStatusReasonID, Result.Deleted,
        Answer.AnswerID,  Answer.QuestionID, Answer.AlternativeID, cast(Answer.Value AS NUMERIC(8,2)) AS value, Answer.Free 
    FROM Result 
        INNER JOIN Answer  ON Answer.resultid = Result.resultid
    WHERE surveyid = @SurveyID AND Result.EntityStatusID = @ActiveEntityStatusID AND Result.EndDate IS NOT NULL AND Result.Deleted IS NULL
    FOR XML AUTO,ELEMENTS
END


GO
