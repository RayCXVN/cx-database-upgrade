SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[at].[prc_LT_Role_upd]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [at].[prc_LT_Role_upd] AS' 
END
GO

ALTER PROCEDURE [at].[prc_LT_Role_upd]
(
	@LanguageID int,
	@RoleID int,
	@Name nvarchar(256),
	@Description nvarchar(max),
	@cUserid int,
	@Log smallint = 1
)
AS
BEGIN
	SET NOCOUNT ON
	DECLARE @Err Int

	UPDATE [at].[LT_Role]
	SET
		[LanguageID] = @LanguageID,
		[RoleID] = @RoleID,
		[Name] = @Name,
		[Description] = @Description
	WHERE
		[LanguageID] = @LanguageID AND
		[RoleID] = @RoleID

	IF @Log = 1 
	BEGIN 
		INSERT INTO [Log].[AuditLog] ( UserId, TableName, Type, Data, Created) 
		SELECT @cUserid,'LT_Role',1,
		( SELECT * FROM [at].[LT_Role] 
			WHERE
			[LanguageID] = @LanguageID AND
			[RoleID] = @RoleID			 FOR XML AUTO) as data,
			getdate()
	END

	Set @Err = @@Error

	RETURN @Err
END


GO
