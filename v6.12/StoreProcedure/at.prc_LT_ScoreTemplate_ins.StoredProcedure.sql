SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[at].[prc_LT_ScoreTemplate_ins]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [at].[prc_LT_ScoreTemplate_ins] AS' 
END
GO

ALTER PROCEDURE [at].[prc_LT_ScoreTemplate_ins]
(
	@ScoreTemplateID int,
	@LanguageID int,
	@Name nvarchar(256),
	@Description nvarchar(max),
	@cUserid int,
	@Log smallint = 1
)
AS
BEGIN
	SET NOCOUNT ON
	DECLARE @Err Int

	INSERT INTO [at].[LT_ScoreTemplate]
	(
		[ScoreTemplateID],
		[LanguageID],
		[Name],
		[Description]
	)
	VALUES
	(
		@ScoreTemplateID,
		@LanguageID,
		@Name,
		@Description
	)

	Set @Err = @@Error

	IF @Log = 1 
	BEGIN 
		INSERT INTO [Log].[AuditLog] ( UserId, TableName, Type, Data, Created) 
		SELECT @cUserid,'LT_ScoreTemplate',0,
		( SELECT * FROM [at].[LT_ScoreTemplate] 
			WHERE
			[ScoreTemplateID] = @ScoreTemplateID AND
			[LanguageID] = @LanguageID				 FOR XML AUTO) as data,
				getdate() 
	 END

	RETURN @Err
END


GO
