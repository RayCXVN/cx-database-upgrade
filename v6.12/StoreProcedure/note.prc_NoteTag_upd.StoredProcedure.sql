SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[note].[prc_NoteTag_upd]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [note].[prc_NoteTag_upd] AS' 
END
GO
ALTER PROCEDURE [note].[prc_NoteTag_upd]
(
	@NoteTagID int,
	@NoteTypeID int,
	@Type smallint,
	@No smallint,
	@cUserid int,
	@Log smallint = 1
)
AS
BEGIN
	SET NOCOUNT ON
	DECLARE @Err Int

	UPDATE [note].[NoteTag]
	SET
		[NoteTypeID] = @NoteTypeID,
		[Type] = @Type,
		[No] = @No
	WHERE
		[NoteTagID] = @NoteTagID

	Set @Err = @@Error
	
	IF @Log = 1 
	BEGIN 
		INSERT INTO [Log].[AuditLog] ( UserId, TableName, Type, Data, Created) 
		SELECT @cUserid,'NoteTag',1,
		( SELECT * FROM [note].[NoteTag] 
			WHERE
			[NoteTagID] = @NoteTagID FOR XML AUTO) as data,
				getdate() 
	 END

	RETURN @Err
END

GO
