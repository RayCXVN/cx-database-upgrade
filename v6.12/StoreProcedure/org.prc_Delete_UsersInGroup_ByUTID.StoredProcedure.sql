SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[org].[prc_Delete_UsersInGroup_ByUTID]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [org].[prc_Delete_UsersInGroup_ByUTID] AS' 
END
GO
/*
	Steve - Sept 22 2016 - EntityStatus for table UserGroup
	Steve - Dec 8, 2016 - Job Fair project: merge Q and R DB, then change how to make a dynamic query
*/
ALTER PROCEDURE [org].[prc_Delete_UsersInGroup_ByUTID]
(
	@OwnerUserIDList	   varchar(max),
	@UTID_ForDelete	   int,
	@OwnerID			   int
)
AS
BEGIN
    SET NOCOUNT ON

	DECLARE @ActiveEntityStatusID INT = (SELECT EntityStatusID FROM EntityStatus WHERE CodeName='Active')
	DECLARE @LinkedDB NVARCHAR(MAX) = ' '
    
    CREATE TABLE #OwnerUserList (UserID int, DeleteError int)
    CREATE TABLE #AllUsersInGroup (UserID int)
    CREATE TABLE #UserWithTypeInGroup (UserID int)
    CREATE TABLE #ResultDatabase (ServerName nvarchar(256), DatabaseName nvarchar(256))
    
    DECLARE c_OwnerUser CURSOR READ_ONLY FOR SELECT UserID FROM #OwnerUserList
    DECLARE c_ResultDB  CURSOR SCROLL READ_ONLY FOR SELECT ServerName, DatabaseName FROM #ResultDatabase
    DECLARE @OwnerUserID int, @ServerName nvarchar(256), @DBName nvarchar(256), @sqlCommand nvarchar(max), 
		  @DeleteError int, @ErrorLevel int
        
    INSERT INTO #OwnerUserList (UserID)
    SELECT ParseValue FROM dbo.StringToArray(@OwnerUserIDList, ',')
    
    INSERT INTO #ResultDatabase (ServerName, DatabaseName)
    SELECT DISTINCT s.ReportServer, s.ReportDB
    FROM at.Survey s JOIN at.Activity a ON s.ActivityID = a.ActivityID AND a.OwnerID = @OwnerID
    
    OPEN c_OwnerUser
    OPEN c_ResultDB
    FETCH NEXT FROM c_OwnerUser INTO @OwnerUserID
    WHILE @@FETCH_STATUS=0
    BEGIN
BEGIN TRANSACTION
	   IF (@ServerName = @@servername AND @DBName = DB_NAME())
		   SET @LinkedDB = ' '
	   ELSE 
		   SET @LinkedDB = '[' + @ServerName + '].[' + @DBName + '].'

	   SET @DeleteError = 0
	   -- Get all users created by given UserID
	   INSERT INTO #AllUsersInGroup (UserID)
	   SELECT ugu.UserID
	   FROM org.UG_U ugu JOIN org.UserGroup ug ON ugu.UserGroupID = ug.UserGroupID AND ug.UserID = @OwnerUserID AND ug.EntityStatusID = @ActiveEntityStatusID AND ug.Deleted IS NULL
	   
	   -- Get list of test counselee
	   INSERT INTO #UserWithTypeInGroup (UserID)
	   SELECT utu.UserID
	   FROM  org.UT_U utu
	   WHERE utu.UserTypeID = @UTID_ForDelete
	     AND utu.UserID IN (SELECT UserID FROM #AllUsersInGroup)
	     
	   -- Delete results of test counselee
	   FETCH FIRST FROM c_ResultDB INTO @ServerName, @DBName
	   WHILE @@FETCH_STATUS=0
	   BEGIN
		  SET @sqlCommand = 'DELETE FROM ' + @LinkedDB + 'dbo.Result
			 WHERE UserID IN (SELECT UserID FROM #UserWithTypeInGroup)'
		  EXECUTE sp_executesql @sqlCommand
		  
		  IF @@Error != 0
		  BEGIN
			 SET @DeleteError = @@Error
			 BREAK
		  END
		  
		  FETCH NEXT FROM c_ResultDB INTO @ServerName, @DBName
	   END
	   
	   IF @DeleteError != 0 GOTO NextOwnerUser
	   
	   -- Remove user with given type from userGroup
	   DELETE FROM org.UG_U WHERE UserID IN (SELECT UserID FROM #UserWithTypeInGroup)
	   IF @@Error != 0
	   BEGIN
		  SET @DeleteError = @@Error
		  GOTO NextOwnerUser
	   END
	   
	   -- Remove userType
	   DELETE FROM org.UT_U WHERE UserID IN (SELECT UserID FROM #UserWithTypeInGroup)
	   IF @@Error != 0
	   BEGIN
		  SET @DeleteError = @@Error
		  GOTO NextOwnerUser
	   END
	   
	   -- Remove event log
	   DELETE FROM [log].[Event] WHERE UserID IN (SELECT UserID FROM #UserWithTypeInGroup)
	   IF @@Error != 0
	   BEGIN
		  SET @DeleteError = @@Error
		  GOTO NextOwnerUser
	   END
	   
	   -- Remove users in being deleted usergroups
	   DELETE FROM org.UG_U WHERE UserGroupID IN (SELECT UserGroupID FROM org.UserGroup WHERE UserID IN (SELECT UserID FROM #UserWithTypeInGroup))
	   IF @@Error != 0
	   BEGIN
		  SET @DeleteError = @@Error
		  GOTO NextOwnerUser
	   END
	   
	   -- Remove usergroup
	   DELETE FROM org.UserGroup WHERE UserID IN (SELECT UserID FROM #UserWithTypeInGroup)
	   IF @@Error != 0
	   BEGIN
		  SET @DeleteError = @@Error
		  GOTO NextOwnerUser
	   END

	   -- Remove Note Subscription
	   DELETE FROM [note].[NoteSubscription] WHERE [UserID] IN (SELECT UserID FROM #UserWithTypeInGroup)
	   IF @@Error != 0
	   BEGIN
		  SET @DeleteError = @@Error
		  GOTO NextOwnerUser
	   END

	   -- Delete user with given type
	   DELETE FROM org.[User] WHERE UserID IN (SELECT UserID FROM #UserWithTypeInGroup)
	   IF @@Error != 0
	   BEGIN
		  SET @DeleteError = @@Error
		  GOTO NextOwnerUser
	   END
	   
NextOwnerUser:
	   IF @DeleteError != 0 ROLLBACK TRANSACTION
	   ELSE COMMIT TRANSACTION
	   -- Clean temp table
	   DELETE FROM #AllUsersInGroup WHERE UserID IN (SELECT UserID FROM #UserWithTypeInGroup)
	   DELETE FROM #UserWithTypeInGroup
	   
	   -- Update process status
	   UPDATE #OwnerUserList SET DeleteError = @DeleteError WHERE UserID = @OwnerUserID
	   
	   FETCH NEXT FROM c_OwnerUser INTO @OwnerUserID
    END
    CLOSE c_ResultDB
    DEALLOCATE c_ResultDB
    
    CLOSE c_OwnerUser
    DEALLOCATE c_OwnerUser
    
    SELECT * FROM #OwnerUserList
    
    DROP TABLE #OwnerUserList
    DROP TABLE #AllUsersInGroup
    DROP TABLE #UserWithTypeInGroup
END

GO
