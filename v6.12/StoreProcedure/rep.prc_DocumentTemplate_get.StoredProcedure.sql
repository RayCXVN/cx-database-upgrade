SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[rep].[prc_DocumentTemplate_get]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [rep].[prc_DocumentTemplate_get] AS' 
END
GO


ALTER PROCEDURE [rep].[prc_DocumentTemplate_get]
(
	@Ownerid int,
	@ActivityID int = null
)
AS
BEGIN
	SET NOCOUNT ON
	DECLARE @Err Int

	SELECT
	[DocumentTemplateID],
	[DocumentFormatID],
	[OwnerID],
	ISNULL([ActivityID], 0) AS 'ActivityID',
	[Template],
	[Created],
	ISNULL([CustomerID], 0) AS 'CustomerID'
	FROM [rep].[DocumentTemplate]
	WHERE
	[OwnerID] = @Ownerid and
	( [ActivityID] = @ActivityID OR  @ActivityID is NULL)

	Set @Err = @@Error

	RETURN @Err
END




GO
