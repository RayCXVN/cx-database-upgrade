SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[at].[prc_LT_BulkGroup_upd]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [at].[prc_LT_BulkGroup_upd] AS' 
END
GO
ALTER PROCEDURE [at].[prc_LT_BulkGroup_upd]
(
	@LanguageID int,
	@BulkGroupID int,
	@Name varchar(100),
	@Description varchar(500),
	@ToolTip ntext,
	@cUserid int,
	@Log smallint = 1
)
AS
BEGIN
	SET NOCOUNT ON
	DECLARE @Err Int

	UPDATE [at].[LT_BulkGroup]
	SET
		[LanguageID] = @LanguageID,
		[BulkGroupID] = @BulkGroupID,
		[Name] = @Name,
		[Description] = @Description,
		[ToolTip] = @ToolTip
	WHERE
		[LanguageID] = @LanguageID AND
		[BulkGroupID] = @BulkGroupID

	IF @Log = 1 
	BEGIN 
		INSERT INTO [Log].[AuditLog] ( UserId, TableName, Type, Data, Created) 
		SELECT @cUserid,'LT_BulkGroup',1,
		( SELECT * FROM [at].[LT_BulkGroup] 
			WHERE
			[LanguageID] = @LanguageID AND
			[BulkGroupID] = @BulkGroupID			 FOR XML AUTO) as data,
			getdate()
	END

	Set @Err = @@Error

	RETURN @Err
END



GO
