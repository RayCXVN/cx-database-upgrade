SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[rep].[prc_UserReportDisplaySetting]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [rep].[prc_UserReportDisplaySetting] AS' 
END
GO
-- Stored Procedure

ALTER PROCEDURE [rep].[prc_UserReportDisplaySetting]	
	@UserReportSettingID int = null output,
	@UserID int,
	@ReportID int,
	@ShowTable smallint = 1,
	@ShowChart smallint = 0,
	@ChartTypeID int = 9,
	@ChartTypeIDAvg int = 7,	
	@TypeIds nvarchar(MAX),
	@Log smallint = 1
AS 
BEGIN
	SET NOCOUNT ON;
	DECLARE @Err int
	
	IF (@UserID = 0 or @ReportID = 0)
		RETURN -1
		
BEGIN TRANSACTION
	--Detect UserReportSettingID exist in [UserReportSetting] table		
	SET @UserReportSettingID = (SELECT TOP 1 UserReportSettingID FROM [rep].[UserReportSetting] WHERE UserID = @UserId AND ReportID = @ReportId)

	IF (@UserReportSettingID is NULL)
		EXEC [rep].[prc_UserReportSetting_ins] @UserReportSettingID output, @UserID, @ReportID, @ShowTable, @ShowChart, @ChartTypeID, @ChartTypeIDAvg, @UserID, @Log
	ELSE
	BEGIN
		UPDATE [rep].[UserReportSetting] 
		SET		[ShowTable] = @ShowTable
				,[ShowChart] = @ShowChart
				,[ChartTypeID] = @ChartTypeID
				,[ChartTypeIDAvg] = @ChartTypeIDAvg					
		WHERE [UserReportSettingID] = @UserReportSettingID 

		IF @Log = 1   
		BEGIN   
		  INSERT INTO [Log].[AuditLog] ( UserId, TableName, Type, Data, Created)   
		  SELECT @UserID,'UserReportSetting',1,  
		  (SELECT * FROM [rep].[UserReportSetting]  
		   WHERE  
		   [UserReportSettingID] = @UserReportSettingID     FOR XML AUTO) as data,  
			getdate()   
		END
	END

	IF @Log = 1   
	BEGIN   
	  INSERT INTO [Log].[AuditLog] ( UserId, TableName, Type, Data, Created)   
	  SELECT @UserID,'URS_CalcType',2,  
	  (SELECT * FROM [rep].[URS_CalcType]  
	   WHERE  
	   [UserReportSettingID] = @UserReportSettingID     FOR XML AUTO) as data,  
		getdate()   
	END

	--Delete old records 		
	DELETE [rep].[URS_CalcType] 
	WHERE [UserReportSettingID] = @UserReportSettingID			

	IF(@TypeIds = '') GOTO CommitResult

	INSERT INTO [rep].[URS_CalcType] (UserReportSettingID, ReportCalcTypeID)
	SELECT @UserReportSettingID, ParseValue FROM [dbo].[StringToArray](@TypeIds,',')
	Set @Err = @@Error
	
	IF @Log = 1   
	BEGIN   
	  INSERT INTO [Log].[AuditLog] ( UserId, TableName, Type, Data, Created)   
	  SELECT @UserID,'URS_CalcType',0,  
	  (SELECT * FROM [rep].[URS_CalcType]  
	   WHERE  
	   [UserReportSettingID] = @UserReportSettingID     FOR XML AUTO) as data,  
		getdate()   
	END

CommitResult:
	COMMIT TRANSACTION
	RETURN @Err
END

GO
