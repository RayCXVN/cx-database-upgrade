SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[at].[prc_Role_ins]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [at].[prc_Role_ins] AS' 
END
GO

ALTER PROCEDURE [at].[prc_Role_ins]
(
	@RoleID int = null output,
	@Ownerid int,
	@No smallint,
	@Type smallint,
	@cUserid int,
	@Log smallint = 1
)
AS
BEGIN
	SET NOCOUNT ON
	DECLARE @Err Int

	INSERT INTO [at].[Role]
	(
		[Ownerid],
		[No],
		[Type]
	)
	VALUES
	(
		@Ownerid,
		@No,
		@Type
	)

	Set @Err = @@Error
	Set @RoleID = scope_identity()

	IF @Log = 1 
	BEGIN 
		INSERT INTO [Log].[AuditLog] ( UserId, TableName, Type, Data, Created) 
		SELECT @cUserid,'Role',0,
		( SELECT * FROM [at].[Role] 
			WHERE
			[RoleID] = @RoleID				 FOR XML AUTO) as data,
				getdate() 
	 END

	RETURN @Err
END


GO
