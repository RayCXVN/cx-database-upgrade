SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[prc_Survey_Role_Count]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [dbo].[prc_Survey_Role_Count] AS' 
END
GO
ALTER proc [dbo].[prc_Survey_Role_Count]
(
    @SurveyId  INT, 
	@RoleID	   INT,
	@Count	   INT = 0 OUTPUT
)
AS
BEGIN
    DECLARE @ActiveEntityStatusID INT = (SELECT EntityStatusID FROM EntityStatus WHERE CodeName='Active')

	SELECT @Count = COUNT(*)
	FROM Result
	WHERE Surveyid =  @SurveyId AND EntityStatusID = @ActiveEntityStatusID AND RoleID = @RoleID AND Deleted IS NULL
END
---------------------------------------------------------------------


GO
