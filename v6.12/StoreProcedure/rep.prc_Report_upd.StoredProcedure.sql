SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[rep].[prc_Report_upd]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [rep].[prc_Report_upd] AS' 
END
GO

ALTER PROCEDURE [rep].[prc_Report_upd]
(
	@ReportID int,
	@OwnerID int,
	@UserID int,
	@ChartTemplate nvarchar(512),
	@DefaultCalcType smallint,
	@cUserid int,
	@Log smallint = 1
)
AS
BEGIN
	SET NOCOUNT ON
	DECLARE @Err Int

	UPDATE [rep].[Report]
	SET
		[OwnerID] = @OwnerID,
		[UserID] = @UserID,
		[ChartTemplate] = @ChartTemplate,
		[DefaultCalcType] = @DefaultCalcType
	WHERE
		[ReportID] = @ReportID

	IF @Log = 1 
	BEGIN 
		INSERT INTO [Log].[AuditLog] ( UserId, TableName, Type, Data, Created) 
		SELECT @cUserid,'Report',1,
		( SELECT * FROM [rep].[Report] 
			WHERE
			[ReportID] = @ReportID			 FOR XML AUTO) as data,
			getdate()
	END

	Set @Err = @@Error

	RETURN @Err
END


GO
