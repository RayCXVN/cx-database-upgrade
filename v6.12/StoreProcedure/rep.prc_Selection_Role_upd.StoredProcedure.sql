SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[rep].[prc_Selection_Role_upd]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [rep].[prc_Selection_Role_upd] AS' 
END
GO

ALTER PROCEDURE [rep].[prc_Selection_Role_upd]
(
	@SelectionID int,
	@RoleID int,
	@cUserid int,
	@Log smallint = 1
)
AS
BEGIN
	SET NOCOUNT ON
	DECLARE @Err Int

	UPDATE [rep].[Selection_Role]
	SET
		[SelectionID] = @SelectionID,
		[RoleID] = @RoleID
	WHERE
		[SelectionID] = @SelectionID AND
		[RoleID] = @RoleID

	IF @Log = 1 
	BEGIN 
		INSERT INTO [Log].[AuditLog] ( UserId, TableName, Type, Data, Created) 
		SELECT @cUserid,'Selection_Role',1,
		( SELECT * FROM [rep].[Selection_Role] 
			WHERE
			[SelectionID] = @SelectionID AND
			[RoleID] = @RoleID			 FOR XML AUTO) as data,
			getdate()
	END

	Set @Err = @@Error

	RETURN @Err
END


GO
