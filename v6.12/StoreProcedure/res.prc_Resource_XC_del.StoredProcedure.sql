SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[res].[prc_Resource_XC_del]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [res].[prc_Resource_XC_del] AS' 
END
GO

ALTER PROCEDURE [res].[prc_Resource_XC_del]
(
	@ResourceID int,
	@XCID	int,
	@cUserid int,
	@Log smallint = 1
)
AS
BEGIN
	SET NOCOUNT ON
	DECLARE @Err Int

	IF @Log = 1 
	BEGIN 
		INSERT INTO [Log].[AuditLog] ( UserId, TableName, Type, Data, Created) 
		SELECT @cUserid,'res.Resource_XC',2,
		( SELECT * FROM [res].[Resource_XC] 
			WHERE
			[ResourceID] = @ResourceID AND
			[XCID] = @XCID
			 FOR XML AUTO) as data,
			getdate() 
	END 

	DELETE
	FROM [res].[Resource_XC]
	WHERE 
		[ResourceID] = @ResourceID AND [XCID] = @XCID

	Set @Err = @@Error

	RETURN @Err
END


GO
