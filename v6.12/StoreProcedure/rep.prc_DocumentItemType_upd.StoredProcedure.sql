SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[rep].[prc_DocumentItemType_upd]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [rep].[prc_DocumentItemType_upd] AS' 
END
GO


ALTER PROCEDURE [rep].[prc_DocumentItemType_upd]
(
	@DocumentItemTypeID int,
	@Name nvarchar(256),
	@cUserid int,
	@Log smallint = 1
)
AS
BEGIN
	SET NOCOUNT ON
	DECLARE @Err Int

	UPDATE [rep].[DocumentItemType]
	SET
		[Name] = @Name
	WHERE
		[DocumentItemTypeID] = @DocumentItemTypeID

	IF @Log = 1 
	BEGIN 
		INSERT INTO [Log].[AuditLog] ( UserId, TableName, Type, Data, Created) 
		SELECT @cUserid,'DocumentItemType',1,
		( SELECT * FROM [rep].[DocumentItemType] 
			WHERE
			[DocumentItemTypeID] = @DocumentItemTypeID			 FOR XML AUTO) as data,
			getdate()
	END

	Set @Err = @@Error

	RETURN @Err
END



GO
