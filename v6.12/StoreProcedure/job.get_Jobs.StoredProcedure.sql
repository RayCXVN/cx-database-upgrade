SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[job].[get_Jobs]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [job].[get_Jobs] AS' 
END
GO
ALTER PROC [job].[get_Jobs]
(@types as varchar(200))
AS
SELECT TOP 10 * FROM Job 
WHERE StartDate <= getdate() AND (JobStatusID = 0)
  AND JobTypeId IN (
     SELECT Value 
     FROM [dbo].[funcListToTableInt](@types,',')
  )
ORDER BY Priority, JobID

GO
