SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[form].[prc_FormFieldCondition_upd]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [form].[prc_FormFieldCondition_upd] AS' 
END
GO
ALTER PROCEDURE [form].[prc_FormFieldCondition_upd] @FormFieldConditionID INT
	,@FormFieldID INT
	,@Type NVARCHAR(32)
	,@Param NVARCHAR(32)
	,@Value NVARCHAR(32)
	,@cUserid INT
	,@Log SMALLINT = 1
AS
BEGIN
	SET NOCOUNT ON;

	DECLARE @Err INT

	UPDATE [form].[FormFieldCondition]
	SET [FormFieldID] = @FormFieldID
		,[Type] = @Type
		,[Param] = @Param
		,[Value] = @Value
	WHERE [FormFieldConditionID] = @FormFieldConditionID

	IF @Log = 1
	BEGIN
		INSERT INTO [Log].[AuditLog] (
			UserId
			,TableName
			,Type
			,Data
			,Created
			)
		SELECT @cUserid
			,'FormFieldCondition'
			,1
			,(
				SELECT *
				FROM [form].[FormFieldCondition]
				WHERE [FormFieldConditionID] = @FormFieldConditionID
				FOR XML AUTO
				) AS data
			,getdate()
	END

	SET @Err = @@Error

	RETURN @Err
END

GO
