SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[at].[prc_SentMail_ins]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [at].[prc_SentMail_ins] AS' 
END
GO

ALTER PROCEDURE [at].[prc_SentMail_ins]
(
	@SentMailID int = null output,
	@MailID int,
	@LanguageID int,
	@Type smallint,
	@From nvarchar(256),
	@Bcc nvarchar(max),
	@StatusMail nvarchar(max),
	@Status smallint,
	@Sent SMALLDATETIME=NULL,
	@cUserid int,
	@Log smallint = 1
)
AS
BEGIN
	SET NOCOUNT ON
	DECLARE @Err Int

	INSERT INTO [at].[SentMail]
	(
		[MailID],
		[LanguageID],
		[Type],
		[From],
		[Bcc],
		[StatusMail],
		[Status],
		[Sent]
	)
	VALUES
	(
		@MailID,
		@LanguageID,
		@Type,
		@From,
		@Bcc,
		@StatusMail,
		@Status,
		@Sent
	)

	Set @Err = @@Error
	Set @SentMailID = scope_identity()

	IF @Log = 1 
	BEGIN 
		INSERT INTO [Log].[AuditLog] ( UserId, TableName, Type, Data, Created) 
		SELECT @cUserid,'SentMail',0,
		( SELECT * FROM [at].[SentMail] 
			WHERE
			[SentMailID] = @SentMailID				 FOR XML AUTO) as data,
				getdate() 
	 END

	RETURN @Err
END


GO
