SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[at].[prc_Activity_copy]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [at].[prc_Activity_copy] AS' 
END
GO
/****************************************************************
DECLARE @NewAID int
[at].[prc_Activity_copy] @ActivityID = 36, @NewAID = @NewAID OUTPUT, @OldOwnerID = 0, @NewOwnerID = 0, @CopyBubbles = 0, @CopyScoreTemplate = 1, @CopyChoice = 1, @CopyActivityView = 1

    Johnny - Feb 27 2018 - Fix insert QuestionID Null for table QA_Calc
    2018-08-16 Ray:     Fix missing related tables, columns to be copied; Refactor; Rebuild using MERGE
    2018-08-27 Ray:     Remove possibility to copy portalpages/parts and menus
****************************************************************/
ALTER PROCEDURE [at].[prc_Activity_copy] (
    @ActivityID        int,
    @NewAID            int = NULL OUTPUT,
    @OldOwnerID        int = 0,
    @NewOwnerID        int = 0,
    @CopyBubbles       int = 1,
    @CopyScoreTemplate int = 1,
    @CopyChoice        int = 1,
    @CopyActivityView  int = 1
) AS
BEGIN
    SET NOCOUNT ON;
    SET XACT_ABORT ON;
    BEGIN TRANSACTION;

    DECLARE @ImportMapping TABLE ([ObjectType] nvarchar(32), [FromID] bigint, [ToID] bigint);
    DECLARE @CurOwnerID int;

    IF @NewOwnerid > 0
    BEGIN
        SET @CurOwnerID = @NewOwnerid;
    END;
    ELSE
    BEGIN
        IF @OldOwnerid > 0
        BEGIN
            SET @CurOwnerID = @OldOwnerid;
        END;
        ELSE
        BEGIN
            SELECT @CurOwnerID = [Ownerid] FROM [at].[Activity] WHERE [ActivityID] = @ActivityID;
        END;;
    END;

    INSERT INTO [at].[Activity] ([LanguageID], [OwnerID], [StyleSheet], [Type], [No], [TooltipType], [Listview], [Descview], [Chartview], [OLAPServer], [OLAPDB],
                                 [Created], [SelectionHeaderType], [ReportServer], [ReportDB], [ExtID], [UseOLAP], [ArchetypeID], [MasterID])
    OUTPUT 'Activity', @ActivityID, [INSERTED].[ActivityID]
    INTO @ImportMapping ([ObjectType], [FromID], [ToID])
    SELECT [LanguageID], @CurOwnerID, [StyleSheet], [TYPE], [NO], [TooltipType], [Listview], [Descview], [Chartview], [OLAPServer], [OLAPDB],
           GETDATE() AS [Created], [SelectionHeaderType], [ReportServer], [ReportDB], [MasterID] AS [ExtID], [UseOLAP], [ArchetypeID], NEWID() AS [MasterID]
    FROM [at].[Activity] t
    WHERE [ActivityID] = @ActivityID;
    
    SELECT @NewAID = im.[ToID] FROM @ImportMapping im WHERE im.[ObjectType] = 'Activity' AND im.[FromID] = @ActivityID;

    INSERT INTO [at].[LT_Activity] ([LanguageID], [ActivityID], [Name], [Info], [StartText], [Description], [RoleName], [SurveyName], [BatchName], [DisplayName], [ShortName])
    SELECT [LanguageID], im.[ToID], [Name], [Info], [StartText], [Description], [RoleName], [SurveyName], [BatchName], [DisplayName], [ShortName]
    FROM [at].[LT_Activity] lt
    JOIN @ImportMapping im ON lt.[ActivityID] = im.[FromID] AND im.[ObjectType] = 'Activity';
    
    INSERT INTO [at].[A_CalcType] ([ReportCalcTypeID], [ActivityID], [No], [DefaultSelected], [Format], [Created])
    SELECT [ReportCalcTypeID], im.[ToID], [No], [DefaultSelected], [Format], GETDATE() AS [Created]
    FROM [at].[A_CalcType] t
    JOIN @ImportMapping im ON t.[ActivityID] = im.[FromID] AND im.[ObjectType] = 'Activity';
    
    INSERT INTO [at].[A_ChartType] ([ReportChartTypeID], [ActivityID], [No], [DefaultSelected], [Created])
    SELECT [ReportChartTypeID], im.[ToID], [No], [DefaultSelected], GETDATE() AS [Created]
    FROM [at].[A_ChartType] t
    JOIN @ImportMapping im ON t.[ActivityID] = im.[FromID] AND im.[ObjectType] = 'Activity';
    
    INSERT INTO [at].[A_L] ([ActivityID], [LanguageID])
    SELECT [im].[ToID], [LanguageID]
    FROM [at].[A_L] [t]
    JOIN @ImportMapping [im] ON [t].[ActivityID] = [im].[FromID] AND [im].[ObjectType] = 'Activity';
    
    IF @NewOwnerID = 0
    BEGIN
        INSERT INTO [at].[XC_A] ([XCID], [ActivityID], [No], [Listview], [Descview], [Chartview], [FillColor], [BorderColor])
        SELECT [XCID], im.[ToID], [No], [Listview], [Descview], [Chartview], [FillColor], [BorderColor]
        FROM [at].[XC_A] t
        JOIN @ImportMapping im ON t.[ActivityID] = im.[FromID] AND im.[ObjectType] = 'Activity';

        INSERT INTO [at].[A_R] ([ActivityID], [RoleID], [No])
        SELECT im.[ToID], [RoleID], [No]
        FROM [at].[A_R] t
        JOIN @ImportMapping [im] ON [t].[ActivityID] = [im].[FromID] AND [im].[ObjectType] = 'Activity';

        ;MERGE INTO [at].[A_S] USING
        (SELECT im.[ToID], s.*
         FROM [at].[A_S] s JOIN @ImportMapping im ON s.[ActivityID] = im.[FromID] AND im.[ObjectType] = 'Activity'
        ) AS t ON 1 = 0
        WHEN NOT MATCHED THEN
        INSERT ([ActivityID], [StatusTypeID], [No])
        VALUES (t.[ToID], [StatusTypeID], [No])
        OUTPUT 'A_S', t.[ASID], [INSERTED].[ASID]
        INTO @ImportMapping ([ObjectType], [FromID], [ToID]);

        INSERT INTO [at].[LT_A_S] ([ASID], [LanguageID], [StatusName], [StatusDescription], [ReadOnlyText])
        SELECT im.[ToID], [LanguageID], [StatusName], [StatusDescription], [ReadOnlyText]
        FROM [at].[LT_A_S] t
        JOIN @ImportMapping [im] ON [t].[ASID] = [im].[FromID] AND [im].[ObjectType] = 'A_S';
        
        ;MERGE INTO [at].[StatusTypeMapping] USING
        (SELECT im.[ToID], s.*
         FROM [at].[StatusTypeMapping] s
         JOIN @ImportMapping im ON s.[ActivityID] = im.[FromID] AND im.[ObjectType] = 'Activity'
        ) AS t ON 1 = 0
        WHEN NOT MATCHED THEN
        INSERT ([ActivityID], [CustomerID], [FromStatusTypeID], [ToStatusTypeID], [No], [Created])
        VALUES (t.[ToID], [CustomerID], [FromStatusTypeID], [ToStatusTypeID], [No], GETDATE())
        OUTPUT 'StatusTypeMapping', t.[StatusTypeMappingID], [INSERTED].[StatusTypeMappingID]
        INTO @ImportMapping ([ObjectType], [FromID], [ToID]);
 
        INSERT INTO [at].[LT_StatusTypeMapping] ([LanguageID], [StatusTypeMappingID], [ActionName], [ActionDescription])
        SELECT [LanguageID], im.[ToID], [ActionName], [ActionDescription]
        FROM [at].[LT_StatusTypeMapping] t
        JOIN @ImportMapping [im] ON [t].[StatusTypeMappingID] = [im].[FromID] AND [im].[ObjectType] = 'StatusTypeMapping';

        INSERT INTO [at].[StatusAction] ([ActivityID], [FromStatusTypeID], [ToStatusTypeID], [StatusActionTypeID], [No], [Created], [Settings], [MasterID])
        SELECT im.[ToID], [FromStatusTypeID], [ToStatusTypeID], [StatusActionTypeID], [No], GETDATE() AS [Created], [Settings], NEWID() AS [MasterID]
        FROM [at].[StatusAction] t
        JOIN @ImportMapping im ON t.[ActivityID] = im.[FromID] AND im.[ObjectType] = 'Activity';
    END;
    
    INSERT INTO [at].[RoleMapping] ([RoleID], [SurveyID], [ActivityID], [UserTypeID], [MasterID])
    SELECT [RoleID], [SurveyID], im.[ToID], [UserTypeID], NEWID() AS [MasterID]
    FROM [at].[RoleMapping] t
    JOIN @ImportMapping im ON t.[ActivityID] = im.[FromID] AND im.[ObjectType] = 'Activity'
    WHERE t.[SurveyID] IS NULL;

--#region Section
    MERGE INTO [at].[Section] USING 
    (SELECT im.[ToID], s.*
     FROM [at].[Section] s JOIN @ImportMapping im ON s.[ActivityID] = im.[FromID] AND im.[ObjectType] = 'Activity'
    ) AS t ON 1 = 0
    WHEN NOT MATCHED THEN
    INSERT ([ActivityID], [Type], [MinOccurance], [MaxOccurance], [Moveable], [CssClass], [Created], [MasterID])
    VALUES (t.[ToID], [Type], [MinOccurance], [MaxOccurance], [Moveable], [CssClass], GETDATE(), NEWID())
    OUTPUT 'Section', t.[SectionID], [INSERTED].[SectionID]
    INTO @ImportMapping ([ObjectType], [FromID], [ToID]);

    INSERT INTO [at].[LT_Section] ([LanguageID], [SectionID], [Name], [Title], [Description])
    SELECT [LanguageID], im.[ToID], [Name], [Title], [Description]
    FROM [at].[LT_Section] t JOIN @ImportMapping im ON t.[SectionID] = im.[FromID] AND im.[ObjectType] = 'Section';
--#endregion Section

--#region Mail Template
    MERGE INTO [at].[MailTemplate] USING 
    (SELECT im.[ToID], s.*
     FROM [at].[MailTemplate] s JOIN @ImportMapping im ON s.[ActivityID] = im.[FromID] AND im.[ObjectType] = 'Activity'
    ) AS t ON 1 = 0
    WHEN NOT MATCHED THEN
    INSERT ([OwnerID], [ActivityID], [Type], [MasterID])
    VALUES ([OwnerID], t.[ToID], [Type], NEWID())
    OUTPUT 'MailTemplate', t.[MailTemplateID], [INSERTED].[MailTemplateID]
    INTO @ImportMapping ([ObjectType], [FromID], [ToID]);

    INSERT INTO [at].[LT_MailTemplate] ([LanguageID], [MailTemplateID], [Name], [Subject], [Body])
    SELECT [LanguageID], im.[ToID], [Name], [Subject], [Body]
    FROM [at].[LT_MailTemplate] t JOIN @ImportMapping im ON t.[MailTemplateID] = im.[FromID] AND im.[ObjectType] = 'MailTemplate';
--#endregion Mail Template

--#region Category
    ;MERGE INTO [at].[Category] USING
    (SELECT im.[ToID], s.*
     FROM [at].[Category] s JOIN @ImportMapping im ON s.[ActivityID] = im.[FromID] AND im.[ObjectType] = 'Activity'
    ) AS t ON 1 = 0
    WHEN NOT MATCHED THEN
    INSERT ([ActivityID], [ParentID], [Tag], [Type], [No], [Fillcolor], [BorderColor], [Weight], [Created], [NType], [ProcessCategory], [ExtID], [AnswerItemID],
            [RenderAsDefault], [AvgType], [MasterID])
    VALUES (t.[ToID], NULL, [Tag], [Type], [No], [Fillcolor], [BorderColor], [Weight], GETDATE(), [NType], [ProcessCategory], [ExtID], [AnswerItemID],
            [RenderAsDefault], [AvgType], NEWID())
    OUTPUT 'Category', t.[CategoryID], [INSERTED].[CategoryID]
    INTO @ImportMapping ([ObjectType], [FromID], [ToID]);
    
    UPDATE u SET u.[ParentID] = p.[ToID]
    FROM [at].[Category] t JOIN @ImportMapping im ON t.[CategoryID] = im.[FromID] AND im.[ObjectType] = 'Category' AND t.[ParentID] IS NOT NULL
    JOIN @ImportMapping p ON t.[ParentID] = p.[FromID] AND p.[ObjectType] = 'Category'
    JOIN [at].[Category] u ON u.[CategoryID] = im.[ToID];

    INSERT INTO [at].[LT_Category] ([LanguageID], [CategoryID], [Name], [Description], [DescriptionExtended])
    SELECT [LanguageID], im.[ToID], [Name], [Description], [DescriptionExtended]
    FROM [at].[LT_Category] t JOIN @ImportMapping im ON t.[CategoryID] = im.[FromID] AND im.[ObjectType] = 'Category';
--#endregion Category

--#region Scale Alternative
    ;MERGE INTO [at].[Scale] USING
    (SELECT im.[ToID], s.*
     FROM [at].[Scale] s JOIN @ImportMapping im ON s.[ActivityID] = im.[FromID] AND im.[ObjectType] = 'Activity'
    ) AS t ON 1 = 0
    WHEN NOT MATCHED THEN
    INSERT ([ActivityID], [MinSelect], [MaxSelect], [Type], [MinValue], [MaxValue], [Created], [Tag], [ExtID], [MasterID])
    VALUES (t.[ToID], [MinSelect], [MaxSelect], [Type], [MinValue], [MaxValue], GETDATE(), [Tag], [ExtID], NEWID())
    OUTPUT 'Scale', t.[ScaleID], [INSERTED].[ScaleID]
    INTO @ImportMapping ([ObjectType], [FromID], [ToID]);

    INSERT INTO [at].[LT_Scale] ([LanguageID], [ScaleID], [Name], [Title], [Description])
    SELECT [LanguageID], im.[ToID], [Name], [Title], [Description]
    FROM [at].[LT_Scale] t JOIN @ImportMapping im ON t.[ScaleID] = im.[FromID] AND im.[ObjectType] = 'Scale';
    
    INSERT INTO [at].[S_CT] ([ScaleID], [ReportCalcTypeID])
    SELECT im.[ToID], [ReportCalcTypeID]
    FROM [at].[S_CT] t JOIN @ImportMapping im ON t.[ScaleID] = im.[FromID] AND im.[ObjectType] = 'Scale';
    
    MERGE INTO [at].[AGroup] USING 
    (SELECT im.[ToID], m1.[ToID] AS [ToScaleID], s.*
     FROM [at].[AGroup] s JOIN @ImportMapping im ON s.[ActivityID] = im.[FromID] AND im.[ObjectType] = 'Activity'
     LEFT JOIN @ImportMapping m1 ON s.[ScaleID] = m1.[FromID] AND m1.[ObjectType] = 'Scale'
    ) AS t ON 1 = 0
    WHEN NOT MATCHED THEN
    INSERT ([ScaleID], [No], [ActivityID], [Type], [Tag], [ExtId], [CssClass], [MasterID])
    VALUES (t.[ToScaleID], [No], t.[ToID], [Type], [Tag], [ExtId], [CssClass], NEWID())
    OUTPUT 'AGroup', t.[AGID], [INSERTED].[AGID]
    INTO @ImportMapping ([ObjectType], [FromID], [ToID]);

    INSERT INTO [at].[LT_AGroup] ([LanguageID], [AGID], [Name], [Title], [Description])
    SELECT [LanguageID], im.[ToID], [Name], [Title], [Description]
    FROM [at].[LT_AGroup] t JOIN @ImportMapping im ON t.[AGID] = im.[FromID] AND im.[ObjectType] = 'AGroup';

    ;MERGE INTO [at].[Alternative] USING
    (SELECT im.[ToID], m1.[ToID] AS [ToAGID], s.*
     FROM [at].[Alternative] s JOIN @ImportMapping im ON s.[ScaleID] = im.[FromID] AND im.[ObjectType] = 'Scale'
     LEFT JOIN @ImportMapping m1 ON s.[AGID] = m1.[FromID] AND m1.[ObjectType] = 'AGroup'
    ) AS t ON 1 = 0
    WHEN NOT MATCHED THEN
    INSERT ([ScaleID], [AGID], [Type], [No], [Value], [InvertedValue], [Calc], [SC], [MinValue], [MaxValue], [Format], [Size], [CssClass], [Created],
            [DefaultValue], [Tag], [ExtID], [Width], [ParentID], [OwnerColorID], [DefaultCalcType], [UseEncryption], [MasterID])
    VALUES (t.[ToID], t.[ToAGID], [Type], [No], [Value], [InvertedValue], [Calc], [SC], [MinValue], [MaxValue], [Format], [Size], [CssClass], GETDATE(),
            [DefaultValue], [Tag], [ExtID], [Width], NULL, [OwnerColorID], [DefaultCalcType], [UseEncryption], NEWID())
    OUTPUT 'Alternative', t.[AlternativeID], [INSERTED].[AlternativeID]
    INTO @ImportMapping ([ObjectType], [FromID], [ToID]);

    UPDATE u SET u.[ParentID] = p.[ToID]
    FROM [at].[Alternative] t JOIN @ImportMapping im ON t.[AlternativeID] = im.[FromID] AND im.[ObjectType] = 'Alternative' AND t.[ParentID] IS NOT NULL
    JOIN @ImportMapping p ON t.[ParentID] = p.[FromID] AND p.[ObjectType] = 'Alternative'
    JOIN [at].[Alternative] u ON u.[AlternativeID] = im.[ToID];

    INSERT INTO [at].[LT_Alternative] ([LanguageID], [AlternativeID], [Name], [Label], [Description])
    SELECT [LanguageID], im.[ToID], [Name], [Label], [Description]
    FROM [at].[LT_Alternative] t JOIN @ImportMapping im ON t.[AlternativeID] = im.[FromID] AND im.[ObjectType] = 'Alternative';
    
    INSERT INTO [at].[AG_A] ([AGID], [AlternativeID])
    SELECT im.[ToID] AS [AGID], m1.[ToID] AS [AlternativeID]
    FROM [at].[AG_A] t JOIN @ImportMapping im ON t.[AGID] = im.[FromID] AND im.[ObjectType] = 'AGroup'
    JOIN @ImportMapping m1 ON t.[AlternativeID] = m1.[FromID] AND m1.[ObjectType] = 'Alternative';
--#endregion Scale Alternative

--#region Page Question
    ;MERGE INTO [at].[Page] USING
    (SELECT im.[ToID], s.*
     FROM [at].[Page] s JOIN @ImportMapping im ON s.[ActivityID] = im.[FromID] AND im.[ObjectType] = 'Activity'
    ) AS t ON 1 = 0
    WHEN NOT MATCHED THEN
    INSERT ([ActivityID], [No], [Created], [Type], [CssClass], [ExtID], [Tag], [MasterID])
    VALUES (t.[ToID], [No], GETDATE(), [Type], [CssClass], [ExtID], [Tag], NEWID())
    OUTPUT 'Page', t.[PageID], [INSERTED].[PageID]
    INTO @ImportMapping ([ObjectType], [FromID], [ToID]);

    INSERT INTO [at].[LT_Page] ([LanguageID], [PageID], [Name], [Info], [Description])
    SELECT [LanguageID], im.[ToID], [Name], [Info], [Description]
    FROM [at].[LT_Page] t JOIN @ImportMapping im ON t.[PageID] = im.[FromID] AND im.[ObjectType] = 'Page';
    
    ;MERGE INTO [at].[Question] USING
    (SELECT im.[ToID] AS [ToPageID], m1.[ToID] AS [ToScaleID], m2.[ToID] AS [ToSectionID], s.*
     FROM [at].[Question] s JOIN @ImportMapping im ON s.[PageID] = im.[FromID] AND im.[ObjectType] = 'Page'
     LEFT JOIN @ImportMapping m1 ON s.[ScaleID] = m1.[FromID] AND m1.[ObjectType] = 'Scale'
     LEFT JOIN @ImportMapping m2 ON s.[SectionID] = m2.[FromID] AND m2.[ObjectType] = 'Section'
    ) AS t ON 1 = 0
    WHEN NOT MATCHED THEN
    INSERT ([PageID], [ScaleID], [No], [Type], [Inverted], [Mandatory], [Status], [CssClass], [ExtId], [Tag], [Created], [SectionID], [Width], [MasterID])
    VALUES ([ToPageID], [ToScaleID], [No], [Type], [Inverted], [Mandatory], [Status], [CssClass], [ExtId], [Tag], GETDATE(), [ToSectionID], [Width], NEWID())
    OUTPUT 'Question', t.[QuestionID], [INSERTED].[QuestionID]
    INTO @ImportMapping ([ObjectType], [FromID], [ToID]);
    
    INSERT INTO [at].[LT_Question] ([LanguageID], [QuestionID], [Name], [Title], [ReportText], [Description], [ShortName])
    SELECT [LanguageID], im.[ToID], [Name], [Title], [ReportText], [Description], [ShortName]
    FROM [at].[LT_Question] t JOIN @ImportMapping im ON t.[QuestionID] = im.[FromID] AND im.[ObjectType] = 'Question';

    INSERT INTO [at].[Q_C] ([QuestionID], [CategoryID], [Weight], [No], [ItemID], [Visible])
    SELECT im.[ToID] AS [QuestionID], m1.[ToID] AS [CategoryID], [Weight], [No], [ItemID], [Visible]
    FROM [at].[Q_C] t JOIN @ImportMapping im ON t.[QuestionID] = im.[FromID] AND im.[ObjectType] = 'Question'
    LEFT JOIN @ImportMapping m1 ON t.[CategoryID] = m1.[FromID] AND m1.[ObjectType] = 'Category';
    
    INSERT INTO [at].[XC_P] ([XCID], [PageID])
    SELECT [XCID], im.[ToID]
    FROM [at].[XC_P] t JOIN @ImportMapping im ON t.[PageID] = im.[FromID] AND im.[ObjectType] = 'Page';

    INSERT INTO [at].[XC_Q] ([XCID], [QuestionID])
    SELECT [XCID], im.[ToID]
    FROM [at].[XC_Q] t JOIN @ImportMapping im ON t.[QuestionID] = im.[FromID] AND im.[ObjectType] = 'Question';
--#endregion Page Question

--#region Formula
    ;MERGE INTO [at].[CalcField] USING 
    (SELECT im.[ToID], s.*
     FROM [at].[CalcField] s JOIN @ImportMapping im ON s.[ActivityID] = im.[FromID] AND im.[ObjectType] = 'Activity'
    ) AS t ON 1 = 0
    WHEN NOT MATCHED THEN
    INSERT ([ActivityID], [Name], [Formula], [Format], [Created], [AllParametersMustExist], [Type], [MasterID])
    VALUES (t.[ToID], [Name], [Formula], [Format], GETDATE(), [AllParametersMustExist], [Type], NEWID())
    OUTPUT 'CalcField', t.[CFID], [INSERTED].[CFID]
    INTO @ImportMapping ([ObjectType], [FromID], [ToID]);

    ;MERGE INTO [at].[CalcParameter] USING 
    (SELECT im.[ToID] AS [ToCFID], m1.[ToID] AS [ToQuestionID], m2.[ToID] AS [ToAlternativeID], m3.[ToID] AS [ToCategoryID], s.*
     FROM [at].[CalcParameter] s JOIN @ImportMapping im ON s.[CFID] = im.[FromID] AND im.[ObjectType] = 'CalcField'
     LEFT JOIN @ImportMapping m1 ON s.[QuestionID] = m1.[FromID] AND m1.[ObjectType] = 'Question'
     LEFT JOIN @ImportMapping m2 ON s.[AlternativeID] = m2.[FromID] AND m2.[ObjectType] = 'Alternative'
     LEFT JOIN @ImportMapping m3 ON s.[CategoryID] = m3.[FromID] AND m3.[ObjectType] = 'Category'
    ) AS t ON 1 = 0
    WHEN NOT MATCHED THEN
    INSERT ([CFID], [No], [Name], [QuestionID], [AlternativeID], [CategoryID], [CalcType], [Format], [ItemID], [Created], [MasterID])
    VALUES ([ToCFID], [No], [Name], [ToQuestionID], [ToAlternativeID], [ToCategoryID], [CalcType], [Format], [ItemID], GETDATE(), NEWID())
    OUTPUT 'CalcParameter', t.[CalcParameterID], [INSERTED].[CalcParameterID]
    INTO @ImportMapping ([ObjectType], [FromID], [ToID]);

    INSERT INTO [at].[QA_Calc] ([QuestionID], [AlternativeID], [CFID], [MinValue], [MaxValue], [No], [NewValue], [UseNewValue], [ItemID], [Created], [UseCalculatedValue], [RoleID])
    SELECT m1.[ToID] AS [QuestionID], m2.[ToID] AS [AlternativeID], im.[ToID] AS [CFID], [MinValue], [MaxValue], [No], [NewValue], [UseNewValue], [ItemID], GETDATE() AS [Created], [UseCalculatedValue], [RoleID]
    FROM [at].[QA_Calc] t JOIN @ImportMapping im ON t.[CFID] = im.[FromID] AND im.[ObjectType] = 'CalcField'
    LEFT JOIN @ImportMapping m1 ON t.[QuestionID] = m1.[FromID] AND m1.[ObjectType] = 'Question'
    LEFT JOIN @ImportMapping m2 ON t.[AlternativeID] = m2.[FromID] AND m2.[ObjectType] = 'Alternative';
--#endregion Formula

--#region LevelLimit
    ;MERGE INTO [at].[LevelGroup] USING 
    (SELECT im.[ToID], s.*
     FROM [at].[LevelGroup] s JOIN @ImportMapping im ON s.[ActivityID] = im.[FromID] AND im.[ObjectType] = 'Activity'
    ) AS t ON 1 = 0
    WHEN NOT MATCHED THEN
    INSERT ([ActivityID], [Tag], [No], [CustomerID], [Departmentid], [Roleid], [Created], [MasterID])
    VALUES (t.[ToID], [Tag], [No], [CustomerID], [Departmentid], [Roleid], GETDATE(), NEWID())
    OUTPUT 'LevelGroup', t.[LevelGroupID], [INSERTED].[LevelGroupID]
    INTO @ImportMapping ([ObjectType], [FromID], [ToID]);
    
    INSERT INTO [at].[LT_LevelGroup] ([LanguageID], [LevelGroupID], [Name], [Description])
    SELECT [LanguageID], im.[ToID], [Name], [Description]
    FROM [at].[LT_LevelGroup] t JOIN @ImportMapping im ON t.[LevelGroupID] = im.[FromID] AND im.[ObjectType] = 'LevelGroup';
    
    ;MERGE INTO [at].[LevelLimit] USING
    (SELECT im.[ToID] AS [ToLevelGroupID], m1.[ToID] AS [ToQuestionID], m2.[ToID] AS [ToAlternativeID], m3.[ToID] AS [ToCategoryID], s.*
     FROM [at].[LevelLimit] s JOIN @ImportMapping im ON s.[LevelGroupID] = im.[FromID] AND im.[ObjectType] = 'LevelGroup'
     LEFT JOIN @ImportMapping m1 ON s.[QuestionID] = m1.[FromID] AND m1.[ObjectType] = 'Question'
     LEFT JOIN @ImportMapping m2 ON s.[AlternativeID] = m2.[FromID] AND m2.[ObjectType] = 'Alternative'
     LEFT JOIN @ImportMapping m3 ON s.[CategoryID] = m3.[FromID] AND m3.[ObjectType] = 'Category'
    ) AS t ON 1 = 0
    WHEN NOT MATCHED THEN
    INSERT ([LevelGroupID], [CategoryID], [QuestionID], [MinValue], [MaxValue], [Sigchange], [Created], [NegativeTrend], [AlternativeID], [ExtID], [OwnerColorID],
            [ItemID], [MatchingType], [MasterID])
    VALUES ([ToLevelGroupID], [ToCategoryID], [ToQuestionID], [MinValue], [MaxValue], [Sigchange], GETDATE(), [NegativeTrend], [ToAlternativeID], [ExtID], [OwnerColorID],
            [ItemID], [MatchingType], NEWID())
    OUTPUT 'LevelLimit', t.[LevelLimitID], [INSERTED].[LevelLimitID]
    INTO @ImportMapping ([ObjectType], [FromID], [ToID]);
    
    INSERT INTO [at].[LT_LevelLimit] ([LevelLimitID], [LanguageID], [Name], [Description])
    SELECT im.[ToID], [LanguageID], [Name], [Description]
    FROM [at].[LT_LevelLimit] t JOIN @ImportMapping im ON t.[LevelLimitID] = im.[FromID] AND im.[ObjectType] = 'LevelLimit';
--#endregion LevelLimit

--#region ScoreTemplate
    IF @CopyScoreTemplate = 1
    BEGIN
        ;MERGE INTO [at].[ScoreTemplateGroup] USING 
        (SELECT im.[ToID], s.*
         FROM [at].[ScoreTemplateGroup] s JOIN @ImportMapping im ON s.[ActivityID] = im.[FromID] AND im.[ObjectType] = 'Activity'
        ) AS t ON 1 = 0
        WHEN NOT MATCHED THEN
        INSERT ([ActivityID], [XCID], [Created], [MasterID])
        VALUES (t.[ToID], [XCID], GETDATE(), NEWID())
        OUTPUT 'ScoreTemplateGroup', t.[ScoreTemplateGroupID], [INSERTED].[ScoreTemplateGroupID]
        INTO @ImportMapping ([ObjectType], [FromID], [ToID]);

        INSERT INTO [at].[LT_ScoreTemplateGroup] ([ScoreTemplateGroupID], [LanguageID], [Name], [Description])
        SELECT im.[ToID], [LanguageID], [Name], [Description]
        FROM [at].[LT_ScoreTemplateGroup] t JOIN @ImportMapping im ON t.[ScoreTemplateGroupID] = im.[FromID] AND im.[ObjectType] = 'ScoreTemplateGroup';
    
        ;MERGE INTO [at].[ScoreTemplate] USING 
        (SELECT im.[ToID] AS [ToActivityID], m1.[ToID] AS [ToScoreTemplateGroupID], s.*
         FROM [at].[ScoreTemplate] s JOIN @ImportMapping im ON s.[ActivityID] = im.[FromID] AND im.[ObjectType] = 'Activity'
         LEFT JOIN @ImportMapping m1 ON s.[ScoreTemplateGroupID] = m1.[FromID] AND m1.[ObjectType] = 'ScoreTemplateGroup'
        ) AS t ON 1 = 0
        WHEN NOT MATCHED THEN
        INSERT ([ScoreTemplateGroupID], [ActivityID], [XCID], [Type], [No], [Created], [DefaultState], [OwnerColorID], [MasterID])
        VALUES ([ToScoreTemplateGroupID], [ToActivityID], [XCID], [Type], [No], GETDATE(), [DefaultState], [OwnerColorID], NEWID())
        OUTPUT 'ScoreTemplate', t.[ScoreTemplateID], [INSERTED].[ScoreTemplateID]
        INTO @ImportMapping ([ObjectType], [FromID], [ToID]);

        INSERT INTO [at].[LT_ScoreTemplate] ([ScoreTemplateID], [LanguageID], [Name], [Description])
        SELECT im.[ToID] AS [ScoreTemplateID], [LanguageID], [Name], [Description]
        FROM [at].[LT_ScoreTemplate] t JOIN @ImportMapping im ON t.[ScoreTemplateID] = im.[FromID] AND im.[ObjectType] = 'ScoreTemplate';

        INSERT INTO [at].[ST_CQ] ([ScoreTemplateID], [CategoryID], [QuestionID], [MinValue], [MaxValue], [AlternativeID], [ItemID])
        SELECT im.[ToID] AS [ScoreTemplateID], m3.[ToID] AS [CategoryID], m1.[ToID] AS [QuestionID], [MinValue], [MaxValue], m2.[ToID] AS [AlternativeID], [ItemID]
        FROM [at].[ST_CQ] t JOIN @ImportMapping im ON t.[ScoreTemplateID] = im.[FromID] AND im.[ObjectType] = 'ScoreTemplate'
        LEFT JOIN @ImportMapping m1 ON t.[QuestionID] = m1.[FromID] AND m1.[ObjectType] = 'Question'
        LEFT JOIN @ImportMapping m2 ON t.[AlternativeID] = m2.[FromID] AND m2.[ObjectType] = 'Alternative'
        LEFT JOIN @ImportMapping m3 ON t.[CategoryID] = m3.[FromID] AND m3.[ObjectType] = 'Category';
    END
--#endregion ScoreTemplate

--#region ActivityView
    IF @CopyActivityView = 1
    BEGIN
        ;MERGE INTO [at].[ActivityView] USING 
        (SELECT im.[ToID], s.*
         FROM [at].[ActivityView] s JOIN @ImportMapping im ON s.[ActivityID] = im.[FromID] AND im.[ObjectType] = 'Activity'
        ) AS t ON 1 = 0
        WHEN NOT MATCHED THEN
        INSERT ([DepartmentID], [CustomerID], [ActivityID], [Created], [ExtId], [MasterID])
        VALUES ([DepartmentID], [CustomerID], t.[ToID], GETDATE(), [ExtId], NEWID())
        OUTPUT 'ActivityView', t.[ActivityViewID], [INSERTED].[ActivityViewID]
        INTO @ImportMapping ([ObjectType], [FromID], [ToID]);

        INSERT INTO [at].[LT_ActivityView] ([LanguageID], [ActivityViewID], [Name], [Description])
        SELECT [LanguageID], im.[ToID] AS [ActivityViewID], [Name], [Description]
        FROM [at].[LT_ActivityView] t JOIN @ImportMapping im ON t.[ActivityViewID] = im.[FromID] AND im.[ObjectType] = 'ActivityView';
    
        ;MERGE INTO [at].[ActivityView_Q] USING 
        (SELECT im.[ToID] AS [ToActivityViewID], m1.[ToID] AS [ToQuestionID], m2.[ToID] AS [ToAlternativeID], m3.[ToID] AS [ToCategoryID], m4.[ToID] AS [ToSectionID], s.*
         FROM [at].[ActivityView_Q] s JOIN @ImportMapping im ON s.[ActivityViewID] = im.[FromID] AND im.[ObjectType] = 'ActivityView'
         LEFT JOIN @ImportMapping m1 ON s.[QuestionID] = m1.[FromID] AND m1.[ObjectType] = 'Question'
         LEFT JOIN @ImportMapping m2 ON s.[AlternativeID] = m2.[FromID] AND m2.[ObjectType] = 'Alternative'
         LEFT JOIN @ImportMapping m3 ON s.[CategoryID] = m3.[FromID] AND m3.[ObjectType] = 'Category'
         LEFT JOIN @ImportMapping m4 ON s.[SectionID] = m4.[FromID] AND m4.[ObjectType] = 'Section'
        ) AS t ON 1 = 0
        WHEN NOT MATCHED THEN
        INSERT ([ActivityViewID], [QuestionID], [SectionID], [No], [CategoryID], [AlternativeID], [ItemID])
        VALUES ([ToActivityViewID], [ToQuestionID], [ToSectionID], [No], [ToCategoryID], [ToAlternativeID], [ItemID])
        OUTPUT 'ActivityView_Q', t.[ActivityViewQID], [INSERTED].[ActivityViewQID]
        INTO @ImportMapping ([ObjectType], [FromID], [ToID]);
    
        INSERT INTO [at].[ActivityView_QA] ([ActivityViewQID], [AlternativeID], [No])
        SELECT im.[ToID] AS [ActivityViewQID], m2.[ToID] AS [AlternativeID], [No]
        FROM [at].[ActivityView_QA] t JOIN @ImportMapping im ON t.[ActivityViewQID] = im.[FromID] AND im.[ObjectType] = 'ActivityView_Q'
        LEFT JOIN @ImportMapping m2 ON t.[AlternativeID] = m2.[FromID] AND m2.[ObjectType] = 'Alternative';
    END
--#endregion Activity View

--#region A_VT
    IF @NewOwnerID = 0
    BEGIN
        ;MERGE INTO [at].[A_VT] USING
        (SELECT im.[ToID] AS [ToActivityID], m1.[ToID] AS [ToActivityViewID], m2.[ToID] AS [ToLevelGroupID], m3.[ToID] AS [ToScoreTemplateGroupID], s.*
         FROM [at].[A_VT] s JOIN @ImportMapping im ON s.[ActivityID] = im.[FromID] AND im.[ObjectType] = 'Activity'
         LEFT JOIN @ImportMapping m1 ON s.[ActivityViewID] = m1.[FromID] AND m1.[ObjectType] = 'ActivityView'
         LEFT JOIN @ImportMapping m2 ON s.[LevelGroupID] = m2.[FromID] AND m2.[ObjectType] = 'LevelGroup'
         LEFT JOIN @ImportMapping m3 ON s.[ScoreTemplateGroupID] = m3.[FromID] AND m3.[ObjectType] = 'ScoreTemplateGroup'
        ) AS t ON 1 = 0
        WHEN NOT MATCHED THEN
        INSERT ([ViewTypeID], [ActivityID], [No], [Type], [Template], [ShowPercentage], [ActivityViewID], [LevelGroupID], [ScoreTemplateGroupID], [CssClass], [MasterID])
        VALUES ([ViewTypeID], [ToActivityID], [No], [Type], [Template], [ShowPercentage], [ToActivityViewID], [ToLevelGroupID], [ToScoreTemplateGroupID], [CssClass], NEWID())
        OUTPUT 'A_VT', t.[AVTID], [INSERTED].[AVTID]
        INTO @ImportMapping ([ObjectType], [FromID], [ToID]);

        INSERT INTO [at].[LT_A_VT] ([LanguageID], [AVTID], [Name], [Description], [TextAbove], [TextBelow])
        SELECT [LanguageID], im.[ToID] AS [AVTID], [Name], [Description], [TextAbove], [TextBelow]
        FROM [at].[LT_A_VT] t JOIN @ImportMapping im ON t.[AVTID] = im.[FromID] AND im.[ObjectType] = 'A_VT';
    END
--#endregion A_VT

--#region Show/Hide
    IF @CopyChoice = 1
    BEGIN
        ;MERGE INTO [at].[Choice] USING 
        (SELECT im.[ToID] AS [ToActivityID], s.*
         FROM [at].[Choice] s JOIN @ImportMapping im ON s.[ActivityID] = im.[FromID] AND im.[ObjectType] = 'Activity'
         WHERE s.[SurveyID] IS NULL
        ) AS t ON 1 = 0
        WHEN NOT MATCHED THEN
        INSERT ([ActivityID], [SurveyID], [RoleID], [DepartmentID], [DepartmentTypeID], [Name], [No], [Type], [Created], [UsergroupID], [HDID], [UserTypeID], [MasterID])
        VALUES ([ToActivityID], [SurveyID], [RoleID], [DepartmentID], [DepartmentTypeID], [Name], [No], [Type], GETDATE(), [UsergroupID], [HDID], [UserTypeID], NEWID())
        OUTPUT 'Choice', t.[ChoiceID], [INSERTED].[ChoiceID]
        INTO @ImportMapping ([ObjectType], [FromID], [ToID]);

        ;MERGE INTO [at].[Action] USING
        (SELECT im.[ToID] AS [ToChoiceID], m1.[ToID] AS [ToQuestionID], m2.[ToID] AS [ToAlternativeID], m3.[ToID] AS [ToPageID], m4.[ToID] AS [ToAVTID], s.*
         FROM [at].[Action] s JOIN @ImportMapping im ON s.[ChoiceID] = im.[FromID] AND im.[ObjectType] = 'Choice'
         LEFT JOIN @ImportMapping m1 ON s.[QuestionID] = m1.[FromID] AND m1.[ObjectType] = 'Question'
         LEFT JOIN @ImportMapping m2 ON s.[AlternativeID] = m2.[FromID] AND m2.[ObjectType] = 'Alternative'
         LEFT JOIN @ImportMapping m3 ON s.[PageID] = m3.[FromID] AND m3.[ObjectType] = 'Page'
         LEFT JOIN @ImportMapping m4 ON s.[AVTID] = m4.[FromID] AND m4.[ObjectType] = 'A_VT'
        ) AS t ON 1 = 0
        WHEN NOT MATCHED THEN
        INSERT ([ChoiceID], [No], [PageID], [QuestionID], [AlternativeID], [Type], [Created], [AVTID], [MasterID])
        VALUES ([ToChoiceID], [No], [ToPageID], [ToQuestionID], [ToAlternativeID], [Type], GETDATE(), [ToAVTID], NEWID())
        OUTPUT 'Action', t.[ActionID], [INSERTED].[ActionID]
        INTO @ImportMapping ([ObjectType], [FromID], [ToID]);

        INSERT INTO [at].[LT_Action] ([LanguageID], [ActionID], [Response])
        SELECT [LanguageID], im.[ToID] AS [ActionID], [Response]
        FROM [at].[LT_Action] t JOIN @ImportMapping im ON t.[ActionID] = im.[FromID] AND im.[ObjectType] = 'Action';

        INSERT INTO [at].[Combi] ([ChoiceID], [QuestionID], [AlternativeID], [ValueFormula])
        SELECT im.[ToID] AS [ChoiceID], m1.[ToID] AS [QuestionID], m2.[ToID] AS [AlternativeID], [ValueFormula]
        FROM [at].[Combi] t JOIN @ImportMapping im ON t.[ChoiceID] = im.[FromID] AND im.[ObjectType] = 'Choice'
        LEFT JOIN @ImportMapping m1 ON t.[QuestionID] = m1.[FromID] AND m1.[ObjectType] = 'Question'
        LEFT JOIN @ImportMapping m2 ON t.[AlternativeID] = m2.[FromID] AND m2.[ObjectType] = 'Alternative';
    END
--#endregion Show/Hide

--#region Dotted Rule
    ;MERGE INTO [at].[DottedRule] USING 
    (SELECT im.[ToID] AS [ToActivityID], s.*
     FROM [at].[DottedRule] s JOIN @ImportMapping im ON s.[ActivityID] = im.[FromID] AND im.[ObjectType] = 'Activity'
     WHERE s.[SurveyID] IS NULL
    ) AS t ON 1 = 0
    WHEN NOT MATCHED THEN
    INSERT ([ActivityID], [SurveyID], [Active], [UseOnAllQuestions], [UseOnAllCategorys], [MinResult], [FrequencyDottedType], [FrequencyDottedLimit],
            [FrequencyDotZeroValues], [FrequencyMinAlternativeCountDotted], [AverageDottedType], [AverageDottedLimit], [Created], [MinResultPercentage],
            [OnlyDepartmentsBelow], [FrequencyMinQuestionCountDotted], [MasterID])
    VALUES ([ToActivityID], [SurveyID], [Active], [UseOnAllQuestions], [UseOnAllCategorys], [MinResult], [FrequencyDottedType], [FrequencyDottedLimit],
            [FrequencyDotZeroValues], [FrequencyMinAlternativeCountDotted], [AverageDottedType], [AverageDottedLimit], GETDATE(), [MinResultPercentage],
            [OnlyDepartmentsBelow], [FrequencyMinQuestionCountDotted], NEWID())
    OUTPUT 'DottedRule', t.[DottedRuleID], [INSERTED].[DottedRuleID]
    INTO @ImportMapping ([ObjectType], [FromID], [ToID]);

    INSERT INTO [at].[LT_DottedRule] ([LanguageID], [DottedRuleID], [Name], [Description])
    SELECT [LanguageID], im.[ToID] AS [DottedRuleID], [Name], [Description]
    FROM [at].[LT_DottedRule] t JOIN @ImportMapping im ON t.[DottedRuleID] = im.[FromID] AND im.[ObjectType] = 'DottedRule';
    
    INSERT INTO [at].[DR_C] ([DottedRuleID], [CategoryID])
    SELECT im.[ToID] AS [DottedRuleID], m3.[ToID] AS [CategoryID]
    FROM [at].[DR_C] t JOIN @ImportMapping im ON t.[DottedRuleID] = im.[FromID] AND im.[ObjectType] = 'DottedRule'
    LEFT JOIN @ImportMapping m3 ON t.[CategoryID] = m3.[FromID] AND m3.[ObjectType] = 'Category';

    INSERT INTO [at].[DR_Q] ([DottedRuleID], [QuestionID])
    SELECT im.[ToID] AS [DottedRuleID], m1.[ToID] AS [QuestionID]
    FROM [at].[DR_Q] t JOIN @ImportMapping im ON t.[DottedRuleID] = im.[FromID] AND im.[ObjectType] = 'DottedRule'
    LEFT JOIN @ImportMapping m1 ON t.[QuestionID] = m1.[FromID] AND m1.[ObjectType] = 'Question';

    INSERT INTO [at].[DR_S] ([DottedRuleID], [ScaleID])
    SELECT im.[ToID] AS [DottedRuleID], m2.[ToID] AS [ScaleID]
    FROM [at].[DR_S] t JOIN @ImportMapping im ON t.[DottedRuleID] = im.[FromID] AND im.[ObjectType] = 'DottedRule'
    LEFT JOIN @ImportMapping m2 ON t.[ScaleID] = m2.[FromID] AND m2.[ObjectType] = 'Scale';
--#endregion Dotted Rule

--#region Bulk
    ;MERGE INTO [at].[BulkGroup] USING
    (SELECT im.[ToID] AS [ToActivityID], s.*
     FROM [at].[BulkGroup] s JOIN @ImportMapping im ON s.[ActivityID] = im.[FromID] AND im.[ObjectType] = 'Activity'
     WHERE s.[SurveyID] IS NULL
    ) AS t ON 1 = 0
    WHEN NOT MATCHED THEN
    INSERT ([ActivityID], [SurveyID], [No], [Css], [Icon], [Tag], [MasterID])
    VALUES ([ToActivityID], [SurveyID], [No], [Css], [Icon], [Tag], NEWID())
    OUTPUT 'BulkGroup', t.[BulkGroupID], [INSERTED].[BulkGroupID]
    INTO @ImportMapping ([ObjectType], [FromID], [ToID]);

    INSERT INTO [at].[LT_BulkGroup] ([LanguageID], [BulkGroupID], [Name], [Description], [ToolTip])
    SELECT [LanguageID], im.[ToID] AS [BulkGroupID], [Name], [Description], [ToolTip]
    FROM [at].[LT_BulkGroup] t JOIN @ImportMapping im ON t.[BulkGroupID] = im.[FromID] AND im.[ObjectType] = 'BulkGroup';

    ;MERGE INTO [at].[Bulk] USING
    (SELECT im.[ToID] AS [ToBulkGroupID], s.*
     FROM [at].[Bulk] s JOIN @ImportMapping im ON s.[BulkGroupID] = im.[FromID] AND im.[ObjectType] = 'BulkGroup'
    ) AS t ON 1 = 0
    WHEN NOT MATCHED THEN
    INSERT ([BulkGroupID], [No], [Css], [Icon], [Tag], [MasterID])
    VALUES ([ToBulkGroupID], [No], [Css], [Icon], [Tag], NEWID())
    OUTPUT 'Bulk', t.[BulkID], [INSERTED].[BulkID]
    INTO @ImportMapping ([ObjectType], [FromID], [ToID]);

    INSERT INTO [at].[LT_Bulk] ([LanguageID], [BulkID], [Name], [Description], [ToolTip])
    SELECT [LanguageID], im.[ToID] AS [BulkID], [Name], [Description], [ToolTip]
    FROM [at].[LT_Bulk] t JOIN @ImportMapping im ON t.[BulkID] = im.[FromID] AND im.[ObjectType] = 'Bulk';

    INSERT INTO [at].[B_P] ([BulkID], [PageID])
    SELECT m1.[ToID] AS [BulkID], im.[ToID] AS [PageID]
    FROM [at].[B_P] t JOIN @ImportMapping im ON t.[PageID] = im.[FromID] AND im.[ObjectType] = 'Page'
    JOIN @ImportMapping m1 ON t.[BulkID] = m1.[FromID] AND m1.[ObjectType] = 'Bulk';

    INSERT INTO [at].[B_Q] ([BulkID], [QuestionID])
    SELECT m1.[ToID] AS [BulkID], im.[ToID] AS [QuestionID]
    FROM [at].[B_Q] t JOIN @ImportMapping im ON t.[QuestionID] = im.[FromID] AND im.[ObjectType] = 'Question'
    JOIN @ImportMapping m1 ON t.[BulkID] = m1.[FromID] AND m1.[ObjectType] = 'Bulk';

    INSERT INTO [at].[B_R] ([BulkID], [RoleID])
    SELECT m1.[ToID] AS [BulkID], [RoleID]
    FROM [at].[B_R] t
    JOIN @ImportMapping m1 ON t.[BulkID] = m1.[FromID] AND m1.[ObjectType] = 'Bulk';
--#endregion Bulk

--#region Bubble
    IF @CopyBubbles = 1
    BEGIN
        ;MERGE INTO [rep].[Bubble] USING
        (SELECT im.[ToID] AS [ToActivityID], s.*
         FROM [rep].[Bubble] s JOIN @ImportMapping im ON s.[ActivityID] = im.[FromID] AND im.[ObjectType] = 'Activity'
        ) AS t ON 1 = 0
        WHEN NOT MATCHED THEN
        INSERT ([OwnerID], [ActivityID], [No], [Status], [Created], [MasterID])
        VALUES (@CurOwnerID, [ToActivityID], [No], [Status], GETDATE(), NEWID())
        OUTPUT 'Bubble', t.[BubbleID], [INSERTED].[BubbleID]
        INTO @ImportMapping ([ObjectType], [FromID], [ToID]);

        INSERT INTO [rep].[LT_Bubble] ([LanguageID], [BubbleID], [Name], [Description])
        SELECT [LanguageID], im.[ToID] AS [BubbleID], [Name], [Description]
        FROM [rep].[LT_Bubble] t JOIN @ImportMapping im ON t.[BubbleID] = im.[FromID] AND im.[ObjectType] = 'Bubble';

        INSERT INTO [rep].[Bubble_Category] ([BubbleID], [CategoryID], [AxisNo])
        SELECT im.[ToID] AS [BubbleID], m1.[ToID] AS [CategoryID], [AxisNo]
        FROM [rep].[Bubble_Category] t JOIN @ImportMapping im ON t.[BubbleID] = im.[FromID] AND im.[ObjectType] = 'Bubble'
        JOIN @ImportMapping m1 ON t.[CategoryID] = m1.[FromID] AND m1.[ObjectType] = 'Category';

        ;MERGE INTO [rep].[BubbleText] USING
        (SELECT im.[ToID] AS [ToBubbleID], s.*
         FROM [rep].[BubbleText] s JOIN @ImportMapping im ON s.[BubbleID] = im.[FromID] AND im.[ObjectType] = 'Bubble'
        ) AS t ON 1 = 0
        WHEN NOT MATCHED THEN
        INSERT ([BubbleID], [No], [FillColor])
        VALUES ([ToBubbleID], [No], [FillColor])
        OUTPUT 'BubbleText', t.[BubbleTextID], [INSERTED].[BubbleTextID]
        INTO @ImportMapping ([ObjectType], [FromID], [ToID]);

        INSERT INTO [rep].[LT_BubbleText] ([LanguageID], [BubbleTextID], [Name], [Description])
        SELECT [LanguageID], im.[ToID] AS [BubbleTextID], [Name], [Description]
        FROM [rep].[LT_BubbleText] t JOIN @ImportMapping im ON t.[BubbleTextID] = im.[FromID] AND im.[ObjectType] = 'BubbleText';
    END;
--#endregion Bubble

--#region Properties
    
    -- Activity
    INSERT INTO [prop].[PropValue] ([PropertyID], [PropOptionID], [ItemID], [Value], [PropFileID], [No], [Created], [CreatedBy], [Updated], [UpdatedBy], [CustomerID])
    SELECT t.[PropertyID], t.[PropOptionID], @NewAID AS [ItemID], t.[Value], t.[PropFileID], t.[No], GETDATE() AS [Created], t.[CreatedBy], GETDATE() AS [Updated], t.[UpdatedBy], t.[CustomerID]
    FROM [prop].[PropPage] pp
    JOIN [dbo].[TableType] tt ON tt.[TableTypeID] = pp.[TableTypeID] AND tt.[Schema] = 'at' AND tt.[Name] = 'Activity'
    JOIN [prop].[Prop] p ON p.[PropPageID] = pp.[PropPageID]
    JOIN [prop].[PropValue] t ON t.[PropertyID] = p.[PropertyID] AND t.[ItemID] = @ActivityID;

    -- Page
    INSERT INTO [prop].[PropValue] ([PropertyID], [PropOptionID], [ItemID], [Value], [PropFileID], [No], [Created], [CreatedBy], [Updated], [UpdatedBy], [CustomerID])
    SELECT t.[PropertyID], t.[PropOptionID], im.[ToID] AS [ItemID], t.[Value], t.[PropFileID], t.[No], GETDATE() AS [Created], t.[CreatedBy], GETDATE() AS [Updated], t.[UpdatedBy], t.[CustomerID]
    FROM [prop].[PropPage] pp
    JOIN [dbo].[TableType] tt ON tt.[TableTypeID] = pp.[TableTypeID] AND tt.[Schema] = 'at' AND tt.[Name] = 'Page'
    JOIN [prop].[Prop] p ON p.[PropPageID] = pp.[PropPageID]
    JOIN [prop].[PropValue] t ON t.[PropertyID] = p.[PropertyID]
    JOIN @ImportMapping im ON t.[ItemID] = im.[FromID] AND im.[ObjectType] = 'Page';

    -- Question
    INSERT INTO [prop].[PropValue] ([PropertyID], [PropOptionID], [ItemID], [Value], [PropFileID], [No], [Created], [CreatedBy], [Updated], [UpdatedBy], [CustomerID])
    SELECT t.[PropertyID], t.[PropOptionID], im.[ToID] AS [ItemID], t.[Value], t.[PropFileID], t.[No], GETDATE() AS [Created], t.[CreatedBy], GETDATE() AS [Updated], t.[UpdatedBy], t.[CustomerID]
    FROM [prop].[PropPage] pp
    JOIN [dbo].[TableType] tt ON tt.[TableTypeID] = pp.[TableTypeID] AND tt.[Schema] = 'at' AND tt.[Name] = 'Question'
    JOIN [prop].[Prop] p ON p.[PropPageID] = pp.[PropPageID]
    JOIN [prop].[PropValue] t ON t.[PropertyID] = p.[PropertyID]
    JOIN @ImportMapping im ON t.[ItemID] = im.[FromID] AND im.[ObjectType] = 'Question';

    -- A_VT
    INSERT INTO [prop].[PropValue] ([PropertyID], [PropOptionID], [ItemID], [Value], [PropFileID], [No], [Created], [CreatedBy], [Updated], [UpdatedBy], [CustomerID])
    SELECT t.[PropertyID], t.[PropOptionID], im.[ToID] AS [ItemID], t.[Value], t.[PropFileID], t.[No], GETDATE() AS [Created], t.[CreatedBy], GETDATE() AS [Updated], t.[UpdatedBy], t.[CustomerID]
    FROM [prop].[PropPage] pp
    JOIN [dbo].[TableType] tt ON tt.[TableTypeID] = pp.[TableTypeID] AND tt.[Schema] = 'at' AND tt.[Name] = 'A_VT'
    JOIN [prop].[Prop] p ON p.[PropPageID] = pp.[PropPageID]
    JOIN [prop].[PropValue] t ON t.[PropertyID] = p.[PropertyID]
    JOIN @ImportMapping im ON t.[ItemID] = im.[FromID] AND im.[ObjectType] = 'A_VT';

--#endregion Properties

    
    COMMIT TRANSACTION;

END;
GO