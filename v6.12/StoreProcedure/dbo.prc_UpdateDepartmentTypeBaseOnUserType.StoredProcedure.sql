SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[prc_UpdateDepartmentTypeBaseOnUserType]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [dbo].[prc_UpdateDepartmentTypeBaseOnUserType] AS' 
END
GO
/*
    Created by: Gam Vo
    Created date: Jun 27, 2016 
    Description: Update Department Type base on Type of User in each class
*/
ALTER PROCEDURE [dbo].[prc_UpdateDepartmentTypeBaseOnUserType]
AS
BEGIN
    SET XACT_ABORT ON
    BEGIN TRANSACTION
 
    DECLARE @IncludeSoftDeleteUser BIT = 0              -- 1: Include soft delete user. 0: Exclude soft delete user
    DECLARE @HDID INT = null                            -- NULL: Get all class in system. NOT NULL: get all class in this H_D
    DECLARE @ClassTypeID INT = 37
 
 
    DECLARE @TableDepartmentTypeID INT = (SElECT T.TableTypeID FROM TableType T WITH (NOLOCK) WHERE T.Name = 'DepartmentType')
    DECLARE @TableUserTypeID INT = (SElECT T.TableTypeID FROM TableType T WITH (NOLOCK) WHERE T.Name = 'UserType')
    DECLARE @EntityStatusID_Active INT = 0 
    SELECT @EntityStatusID_Active = EntityStatusID FROM EntityStatus WHERE CodeName='Active'
 
    CREATE TABLE #Department
    (
        DepartmentID INT NOT NULL,
        Name VARCHAR(256)
    )
 
    CREATE TABLE #Result
    (
        DepartmentID INT NOT NULL,
        DepartmentTypeID INT NOT NULL,
    )
 
    --1. Get all class bass on the @HDID
    IF @HDID IS NULL
    BEGIN
        INSERT INTO #Department (DepartmentID, Name)
        SELECT D.DepartmentID, D.Name
        FROM org.Department D WITH (NOLOCK)
            JOIN org.DT_D DTD WITH (NOLOCk) ON D.DepartmentID = DTD.DepartmentID
        WHERE DTD.DepartmentTypeID = @ClassTypeID AND D.EntityStatusID = @EntityStatusID_Active AND D.Deleted IS NULL
    END
    ELSE
    BEGIN
        INSERT INTO #Department (DepartmentID, Name)
        SELECT D.DepartmentID, D.Name
        FROM org.H_D Parent WITH (NOLOCK)
            JOIN org.H_D Child WITH (NOLOCK) ON Child.Path LIKE Parent.Path + '%'
            JOIN org.Department D WITH (NOLOCK) ON Child.DepartmentID = D.DepartmentID
            JOIN org.DT_D DTD WITH (NOLOCK) ON DTD.DepartmentID = D.DepartmentID
        WHERE Parent.HDID = @HDID AND DTD.DepartmentTypeID = @ClassTypeID AND D.EntityStatusID = @EntityStatusID_Active AND D.Deleted IS NULL
    END
 
    --Get mapping DepartmentType => UserType
    SELECT OM.OMID, LTD.DepartmentTypeID, LTD.Name DepartmentTypeName, LTU.UserTypeID, LTU.Name UserTypeName
    INTO #Mapping
    FROM ObjectMapping OM WITH (NOLOCK)
        JOIN org.LT_DepartmentType LTD WITH (NOLOCK) ON OM.FromID = LTD.DepartmentTypeID AND LTD.LanguageID = 1
        JOIN org.LT_UserType LTU WITH (NOLOCK) ON OM.ToID = LTU.UserTypeID AND LTD.LanguageID = 1
    WHERE OM.FromTableTypeID = @TableDepartmentTypeID AND OM.ToTableTypeID = @TableUserTypeID
 
    --Get Department type base on User type in each class
    SELECT DISTINCT D.DepartmentID, D.Name, M.DepartmentTypeID
    INTO #DepartmentType
    FROM org.[User] U WITH (NOLOCK)
        JOIN org.UT_U UTU WITH (NOLOCK) ON UTU.UserID = U.UserID
        JOIN org.LT_UserType LTU WITH (NOLOCK) ON LTU.UserTypeID = UTU.UserTypeID
        JOIN #Department D ON U.DepartmentID = D.DepartmentID
        JOIN #Mapping M ON UTU.UserTypeID = M.UserTypeID
    WHERE (U.EntityStatusID = @EntityStatusID_Active AND U.Deleted IS NULL) OR (@IncludeSoftDeleteUser = 1)
 
    --Clear current Department Type
    DELETE T
    FROM org.DT_D T
        JOIN #Department D ON D.DepartmentID = T.DepartmentID
        JOIN #Mapping M ON M.DepartmentTypeID = T.DepartmentTypeID
 
    --Insert new Department Type
    INSERT INTO org.DT_D (DepartmentID, DepartmentTypeID)
    OUTPUT INSERTED.DepartmentID, INSERTED.DepartmentTypeID
    INTO #Result (DepartmentID, DepartmentTypeID)
    SELECT DT.DepartmentID, DT.DepartmentTypeID FROM #DepartmentType DT
 
    --Return result
    SELECT R.DepartmentID, D.Name DepartmentName, R.DepartmentTypeID, LTDT.Name DepartmentTypeName
    FROM #Result R
        JOIN org.Department D WITH (NOLOCK) ON R.DepartmentID = D.DepartmentID
        JOIN org.LT_DepartmentType LTDT WITH (NOLOCK) ON LTDT.DepartmentTypeID = R.DepartmentTypeID AND LTDT.LanguageID = 1
 
    IF OBJECT_ID('tempdb..#Department') IS NOT NULL
        DROP TABLE #Department
    IF OBJECT_ID('tempdb..#Mapping') IS NOT NULL
        DROP TABLE #Mapping
    IF OBJECT_ID('tempdb..#DepartmentType') IS NOT NULL
        DROP TABLE #DepartmentType
    IF OBJECT_ID('tempdb..#Result') IS NOT NULL
        DROP TABLE #Result
 
    COMMIT TRANSACTION
END

GO
