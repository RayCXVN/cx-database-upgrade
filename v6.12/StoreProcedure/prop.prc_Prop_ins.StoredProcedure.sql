SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[prop].[prc_Prop_ins]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [prop].[prc_Prop_ins] AS' 
END
GO

ALTER PROCEDURE [prop].[prc_Prop_ins]
(
	@PropertyID int = null output,
	@PropPageID int,
	@No smallint,
	@MultiValue bit,
	@Type smallint,
	@ValueType smallint,
	@FormatString nvarchar(50),
	@cUserid int,
	@Log smallint = 1
)
AS
BEGIN
	SET NOCOUNT ON
	DECLARE @Err Int

	INSERT INTO [prop].[Prop]
	(
		[PropPageID],
		[No],
		[MultiValue],
		[Type],
		[ValueType],
		[FormatString]
	)
	VALUES
	(
		@PropPageID,
		@No,
		@MultiValue,
		@Type,
		@ValueType,
		@FormatString
	)

	Set @Err = @@Error
	Set @PropertyID = scope_identity()

	IF @Log = 1 
	BEGIN 
		INSERT INTO [Log].[AuditLog] ( UserId, TableName, Type, Data, Created) 
		SELECT @cUserid,'Prop',0,
		( SELECT * FROM [prop].[Prop] 
			WHERE
			[PropertyID] = @PropertyID				 FOR XML AUTO) as data,
				getdate() 
	 END

	RETURN @Err
END


GO
