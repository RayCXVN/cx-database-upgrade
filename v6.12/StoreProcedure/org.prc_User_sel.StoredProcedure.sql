SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[org].[prc_User_sel]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [org].[prc_User_sel] AS' 
END
GO
/*  2016-10-10 Ray: Remove column Status from org.User, org.Department, RDB.dbo.Result
*/
ALTER PROCEDURE [org].[prc_User_sel]
(
  @UserId int
 )
AS
BEGIN
  Select UserID, Ownerid, DepartmentID, LanguageID, isnull(RoleID,0) 'RoleID', UserName, Password, LastName, FirstName, Email, Mobile, ExtID, SSN, 
    Tag, Locked, ChangePassword, Created, [HashPassword], [SaltPassword],	[OneTimePassword], [OTPExpireTime],CountryCode,Gender,DateOfBirth, 
    EntityStatusID, Deleted, EntityStatusReasonID
  FROM org.[User]
  WHERE Userid = @UserId
END

GO
