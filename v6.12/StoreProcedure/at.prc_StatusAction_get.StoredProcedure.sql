SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[at].[prc_StatusAction_get]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [at].[prc_StatusAction_get] AS' 
END
GO

ALTER PROCEDURE [at].[prc_StatusAction_get]
(
	@ActivityID int
)
AS
BEGIN
	SET NOCOUNT ON
	DECLARE @Err Int

	SELECT
	[StatusActionID],
	[ActivityID],
	ISNULL([FromStatusTypeID], 0) AS 'FromStatusTypeID',
	ISNULL([ToStatusTypeID], 0) AS 'ToStatusTypeID',
	[StatusActionTypeID],
	[No],
	[Created],
	[Settings]
	FROM [at].[StatusAction]
	WHERE
	[ActivityID] = @ActivityID
	ORDER BY [No]

	Set @Err = @@Error

	RETURN @Err
END

GO
