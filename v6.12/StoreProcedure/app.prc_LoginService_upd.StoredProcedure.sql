SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[app].[prc_LoginService_upd]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [app].[prc_LoginService_upd] AS' 
END
GO
/*
	2018-10-31	Sarah	CXPLAT-848
*/
ALTER PROCEDURE [app].[prc_LoginService_upd]
    @LoginServiceID int,
    @SiteID int,
    @LoginServiceType int,
    @IconUrl nvarchar(1024),  
    @Authority nvarchar(1024), 
    @MetadataAddress nvarchar(1024),  
    @RedirectUri nvarchar(1024),  
    @SecondaryClaimType nvarchar(1024),
    @ClientId nvarchar(1024),
    @ClientSecret nvarchar(1024),
    @Scope nvarchar(1024),
    @ResponseType nvarchar(1024),
    @PrimaryClaimType nvarchar(1024),  
    @PostLogoutUri nvarchar(1024),
    @Disabled bit,
    @cUserid int,
    @Log smallint = 1
AS
BEGIN
    SET NOCOUNT ON;
    DECLARE @Err Int

    UPDATE [app].[LoginService]
    SET    
        [SiteID] = @SiteID,
        [LoginServiceType] = @LoginServiceType,
        [IconUrl] = @IconUrl,
        [Authority] = @Authority,
        [MetadataAddress] = @MetadataAddress,
        [RedirectUri] = @RedirectUri,
        [SecondaryClaimType] = @SecondaryClaimType,
        [ClientId] = @ClientId,
        [ClientSecret] = @ClientSecret,
        [Scope] = @Scope,
        [ResponseType] = @ResponseType,
        [PrimaryClaimType] = @PrimaryClaimType,
        [PostLogoutUri] = @PostLogoutUri,
        [Disabled] = @Disabled
    WHERE [LoginServiceID] = @LoginServiceID

    Set @Err = @@Error
    IF @Log = 1 
    BEGIN 
        INSERT INTO [Log].[AuditLog] ( UserId, TableName, Type, Data, Created) 
        SELECT @cUserid,'LoginService',1,
        ( SELECT * FROM [app].[LoginService]
            WHERE
            [LoginServiceID] = @LoginServiceID                 FOR XML AUTO) as data,
                getdate() 
    END

    RETURN @Err       
END

GO
