SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[rep].[prc_DocumentItemType_get]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [rep].[prc_DocumentItemType_get] AS' 
END
GO


ALTER PROCEDURE [rep].[prc_DocumentItemType_get]
AS
BEGIN
	SET NOCOUNT ON
	DECLARE @Err Int

	SELECT
	[DocumentItemTypeID],
	[Name],
	[Created]
	FROM [rep].[DocumentItemType]

	Set @Err = @@Error

	RETURN @Err
END



GO
