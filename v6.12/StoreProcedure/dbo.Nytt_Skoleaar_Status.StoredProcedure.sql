SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[Nytt_Skoleaar_Status]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [dbo].[Nytt_Skoleaar_Status] AS' 
END
GO
ALTER PROCEDURE [dbo].[Nytt_Skoleaar_Status]
AS
    DECLARE @EntityStatusID INT = 0 
	SELECT @EntityStatusID=EntityStatusID FROM EntityStatus WHERE CodeName='Active'
select 
	isnull(ltpo.name,'Ikke påbegynt') as nyttskoleår_status, 
	c.name as kundenavn, 
	cast(c.customerid as nvarchar(10)) as customerid, 
	d.name as skolenavn, 
	cast(d.departmentid as nvarchar(10)) as departmentid 
from org.department d
inner join org.DT_D dtd on dtd.DEpartmentID = d.departmentid and dtd.departmenttypeid = 36
left join org.customer c on c.customerid = d.customerid and c.ownerid = d.ownerid
left join prop.propvalue pv on pv.itemid = d.departmentid and pv.propertyid = 13
left join prop.lt_propoption ltpo on ltpo.propoptionid = pv.propoptionid
where d.[EntityStatusID] = @EntityStatusID AND d.Deleted IS NULL
order by c.name, d.name


GO
