SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[org].[prc_UserCountByHDIDAndUserType]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [org].[prc_UserCountByHDIDAndUserType] AS' 
END
GO
ALTER PROC [org].[prc_UserCountByHDIDAndUserType]
(
  @HDID INT,
  @Usertypeid INT,
  @IncludeTop smallint = 1
)
AS
BEGIN
    DECLARE @ActiveEntityStatusID INT = (SELECT EntityStatusID FROM EntityStatus WHERE CodeName='Active')

    SELECT count(u.userid) as 'Antall' FROM org.H_D hd 
    JOIN org.department d ON hd.PATH LIKE '%\' + cast(@HDID as nvarchar(16)) + '\%' AND d.departmentid = hd.departmentid 
    JOIN org.[USER] u ON u.departmentid = d.departmentid
    JOIN org.[UT_U] utu on utu.userid = u.userid and (utu.usertypeid = @Usertypeid)
    WHERE u.EntityStatusID = @ActiveEntityStatusID AND u.Deleted IS NULL AND (@IncludeTop = 1 OR hd.HDID <> @HDID)
END

GO
