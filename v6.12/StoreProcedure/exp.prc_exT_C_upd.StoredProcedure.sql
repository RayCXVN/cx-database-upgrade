SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[exp].[prc_exT_C_upd]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [exp].[prc_exT_C_upd] AS' 
END
GO

ALTER PROCEDURE [exp].[prc_exT_C_upd]
(
	@TypeID smallint,
	@ColumnID smallint,
	@No smallint,
	@cUserid int,
	@Log smallint = 1
)
AS
BEGIN
	SET NOCOUNT ON
	DECLARE @Err Int

	UPDATE [exp].[exT_C]
	SET
		[TypeID] = @TypeID,
		[ColumnID] = @ColumnID,
		[No] = @No
	WHERE
		[TypeID] = @TypeID AND
		[ColumnID] = @ColumnID

	IF @Log = 1 
	BEGIN 
		INSERT INTO [Log].[AuditLog] ( UserId, TableName, Type, Data, Created) 
		SELECT @cUserid,'exT_C',1,
		( SELECT * FROM [exp].[exT_C] 
			WHERE
			[TypeID] = @TypeID AND
			[ColumnID] = @ColumnID			 FOR XML AUTO) as data,
			getdate()
	END

	Set @Err = @@Error

	RETURN @Err
END


GO
