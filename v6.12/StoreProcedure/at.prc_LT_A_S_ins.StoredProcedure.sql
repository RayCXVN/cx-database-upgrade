SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[at].[prc_LT_A_S_ins]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [at].[prc_LT_A_S_ins] AS' 
END
GO
ALTER PROCEDURE [at].[prc_LT_A_S_ins] (
	@LanguageID INT,
	@ASID INT,
	@StatusName NVARCHAR(512),
	@StatusDescription NVARCHAR(512),
	@ReadOnlyText NVARCHAR(MAX) = '',
	@cUserid INT,
	@Log SMALLINT = 1
	)
AS
BEGIN
	SET NOCOUNT ON

	DECLARE @Err INT

	INSERT INTO [at].[LT_A_S] (
		[ASID],
		[LanguageID],
		[StatusName],
		[StatusDescription],
		[ReadOnlyText]
		)
	VALUES (
		@ASID,
		@LanguageID,
		@StatusName,
		@StatusDescription,
		@ReadOnlyText
		)

	SET @Err = @@Error

	IF @Log = 1
	BEGIN
		INSERT INTO [Log].[AuditLog] (
			UserId,
			TableName,
			Type,
			Data,
			Created
			)
		SELECT @cUserid,
			'LT_A_S',
			0,
			(
				SELECT *
				FROM [at].[LT_A_S]
				WHERE [LanguageID] = @LanguageID
					AND [ASID] = @ASID
				FOR XML AUTO
				) AS data,
			getdate()
	END

	RETURN @Err
END

GO
