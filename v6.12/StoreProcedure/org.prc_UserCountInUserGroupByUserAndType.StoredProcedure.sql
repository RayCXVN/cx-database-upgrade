SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[org].[prc_UserCountInUserGroupByUserAndType]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [org].[prc_UserCountInUserGroupByUserAndType] AS' 
END
GO
/*
    Steve - Sept 22 2016 - EntityStatus for table UserGroup
    2018-07-12 Ray:     Replace UG_U by UGMember
*/
ALTER PROCEDURE [org].[prc_UserCountInUserGroupByUserAndType]
(    
    @ListUserID                 varchar(max) = '',
    @UserTypeID                 int,
    @UserGroupTypeID            varchar(128),
    @DenyUserTypeList           nvarchar(max) = '',
    @CountInGroupOwnerCurrentHD bit = 0
)
AS
BEGIN
    DECLARE @ActiveEntityStatusID INT = (SELECT EntityStatusID FROM EntityStatus WHERE CodeName='Active')

    IF @CountInGroupOwnerCurrentHD = 0
    BEGIN
        SELECT COUNT( DISTINCT ugm.UserID) as [Count], uag.UserID
        FROM (SELECT value AS [UserID] FROM dbo.funcListToTableInt(@ListUserID,',')) AS ul
        JOIN org.UserGroup uag ON ul.[UserID] = uag.[UserID] AND uag.EntityStatusID = @ActiveEntityStatusID AND uag.Deleted IS NULL
         AND uag.UserGroupTypeID IN (SELECT value FROM dbo.funcListToTableInt(@UserGroupTypeID,','))
        JOIN org.[UGMember] ugm ON ugm.UserGroupID = uag.UserGroupID AND ugm.[EntityStatusID] = @ActiveEntityStatusID AND ugm.[Deleted] IS NULL
        JOIN org.UT_U utu ON utu.UserID = ugm.UserID AND utu.UserTypeID = @UserTypeID
        JOIN org.[User] us ON us.UserID = ugm.UserID AND us.EntityStatusID = @ActiveEntityStatusID AND us.Deleted IS NULL
        WHERE NOT EXISTS (SELECT 1 FROM org.UT_U utu2 WHERE utu2.UserID = ugm.UserID AND utu2.UserTypeID IN (SELECT value FROm dbo.funcListToTableInt(@DenyUserTypeList,',')))
        GROUP BY uag.UserID
    END
    ELSE
    BEGIN
        SELECT COUNT( DISTINCT ugm.UserID) as [Count], uag.UserID
        FROM (SELECT value AS [UserID] FROM dbo.funcListToTableInt(@ListUserID,',')) AS ul
        JOIN org.UserGroup uag ON uag.[UserID] = ul.[UserID] AND uag.EntityStatusID = @ActiveEntityStatusID AND uag.Deleted IS NULL
         AND uag.UserGroupTypeID IN (SELECT value FROM dbo.funcListToTableInt(@UserGroupTypeID,','))
        JOIN org.[UGMember] ugm ON ugm.UserGroupID = uag.UserGroupID AND ugm.[EntityStatusID] = @ActiveEntityStatusID AND ugm.[Deleted] IS NULL
        JOIN org.UT_U utu ON utu.UserID = ugm.[UserID] AND utu.UserTypeID = @UserTypeID
        JOIN org.[User] us ON us.UserID = ugm.UserID AND us.EntityStatusID = @ActiveEntityStatusID AND us.Deleted IS NULL
        JOIN [org].[User] u ON uag.[UserID] = u.[UserID]
        JOIN [org].[H_D] phd ON u.[DepartmentID] = phd.[DepartmentID]
        JOIN [org].[H_D] chd ON us.[DepartmentID] = chd.[DepartmentID] AND chd.[Path] LIKE '%\' + CONVERT(nvarchar(32), phd.[HDID]) + '\%'
        WHERE NOT EXISTS (SELECT 1 FROM org.UT_U utu2 WHERE utu2.UserID = ugm.UserID AND utu2.UserTypeID IN (SELECT value FROm dbo.funcListToTableInt(@DenyUserTypeList,',')))
        GROUP BY uag.UserID
    END
END
GO
