SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[prc_LT_RelationType_get]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [dbo].[prc_LT_RelationType_get] AS' 
END
GO

ALTER PROCEDURE [dbo].[prc_LT_RelationType_get]
(
	@RelationTypeID int
)
AS
BEGIN
	SET NOCOUNT ON
	DECLARE @Err Int

	SELECT
	[LanguageID],
	[RelationTypeID],
	[Name],
	[Description]
	FROM [dbo].[LT_RelationType]
	WHERE
	[RelationTypeID] = @RelationTypeID

	Set @Err = @@Error

	RETURN @Err
END


GO
