SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[job].[prc_JobStatusLog_upd]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [job].[prc_JobStatusLog_upd] AS' 
END
GO

ALTER PROCEDURE [job].[prc_JobStatusLog_upd]
(
	@JobStatusLogID int,
	@JobID int,
	@JobStatusID smallint,
	@Description nvarchar(max),
	@cUserid int,
	@Log smallint = 1
)
AS
BEGIN
	SET NOCOUNT ON
	DECLARE @Err Int

	UPDATE [job].[JobStatusLog]
	SET
		[JobID] = @JobID,
		[JobStatusID] = @JobStatusID,
		[Description] = @Description
	WHERE
		[JobStatusLogID] = @JobStatusLogID

	IF @Log = 1 
	BEGIN 
		INSERT INTO [Log].[AuditLog] ( UserId, TableName, Type, Data, Created) 
		SELECT @cUserid,'JobStatusLog',1,
		( SELECT * FROM [job].[JobStatusLog] 
			WHERE
			[JobStatusLogID] = @JobStatusLogID			 FOR XML AUTO) as data,
			getdate()
	END

	Set @Err = @@Error

	RETURN @Err
END


GO
