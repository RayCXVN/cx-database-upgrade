SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[app].[prc_PP_VT_ins]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [app].[prc_PP_VT_ins] AS' 
END
GO

ALTER PROCEDURE [app].[prc_PP_VT_ins]
(
	@PortalPartID int,
	@ViewTypeID int,
	@No smallint,
	@cUserid int,
	@Log smallint = 1
)
AS
BEGIN
	SET NOCOUNT ON
	DECLARE @Err Int

	INSERT INTO [app].[PP_VT]
	(
		[PortalPartID],
		[ViewTypeID],
		[No]
	)
	VALUES
	(
		@PortalPartID,
		@ViewTypeID,
		@No
	)

	Set @Err = @@Error

	IF @Log = 1 
	BEGIN 
		INSERT INTO [Log].[AuditLog] ( UserId, TableName, Type, Data, Created) 
		SELECT @cUserid,'PP_VT',0,
		( SELECT * FROM [app].[PP_VT] 
			WHERE
			[PortalPartID] = @PortalPartID AND
			[ViewTypeID] = @ViewTypeID				 FOR XML AUTO) as data,
				getdate() 
	 END

	RETURN @Err
END


GO
