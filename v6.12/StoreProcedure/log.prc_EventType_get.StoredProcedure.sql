SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[log].[prc_EventType_get]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [log].[prc_EventType_get] AS' 
END
GO

ALTER PROCEDURE [log].[prc_EventType_get]
	@EventTypeID int
AS
BEGIN
	SET NOCOUNT ON;
	DECLARE @Err Int
	
     SELECT [EventTypeID],
		  [ParentID],
		  [CodeName],
		  [OwnerID],
		  [No],
		  [Active]
	FROM [log].[EventType]
	WHERE [EventTypeID] = @EventTypeID
	
	Set @Err = @@Error

	RETURN @Err
  
END

GO
