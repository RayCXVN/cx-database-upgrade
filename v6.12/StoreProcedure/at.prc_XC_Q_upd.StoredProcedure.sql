SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[at].[prc_XC_Q_upd]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [at].[prc_XC_Q_upd] AS' 
END
GO
ALTER PROCEDURE [at].[prc_XC_Q_upd]
(
	@XCID int,
	@QuestionID INT,
	@cUserid int,
	@Log smallint = 1
)
AS
BEGIN
	SET NOCOUNT ON
	DECLARE @Err Int
	UPDATE [at].[XC_Q]
	SET
		[XCID] = @XCID,
		[QuestionID] = @QuestionID
	WHERE
		[XCID] = @XCID AND
		[QuestionID] = @QuestionID
	IF @Log = 1 
	BEGIN 
		INSERT INTO [Log].[AuditLog] ( UserId, TableName, Type, Data, Created) 
		SELECT @cUserid,'XC_Q',1,

		( SELECT * FROM [at].[XC_Q] 
			WHERE
			[XCID] = @XCID AND
			[QuestionID] = @QuestionID			 FOR XML AUTO) as data,
			getdate()
	END
	Set @Err = @@Error
	RETURN @Err
END

GO
