SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[job].[prc_Job_getByDepartmentAndJobType]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [job].[prc_Job_getByDepartmentAndJobType] AS' 
END
GO


ALTER PROCEDURE [job].[prc_Job_getByDepartmentAndJobType]
(
	@OwnerID int,
	@DepartmentID  int,
	@JobTypeID int
)
AS
BEGIN
select j.[JobID],
	 j.[JobTypeID],
	 j.[JobStatusID],
	 j.[OwnerID],
	 j.[UserID],
	 j.[Name],
	 j.[Priority],
	 ISNULL(j.[Option], 0) AS 'Option',
	 j.[Created],
	 j.[StartDate],
	ISNULL( j.[EndDate], '1900-01-01') AS 'EndDate',
	 j.[Description]
from job.Job j
	join  job.JobParameter jp on j.JobID = jp.JobID and  jp.Name = 'DepartmentID' and jp.Value = cast(@Departmentid as nvarchar(16))
where j.JobTypeID = @JobTypeID and j.OwnerID = @OwnerID
END


GO
