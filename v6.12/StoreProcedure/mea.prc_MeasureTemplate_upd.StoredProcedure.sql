SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[mea].[prc_MeasureTemplate_upd]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [mea].[prc_MeasureTemplate_upd] AS' 
END
GO
ALTER PROCEDURE [mea].[prc_MeasureTemplate_upd]    
(
	@MeasureTemplateID int,
	@CustomerID int = null,
	@MeasureTypeID int,
	@ChangedBy int = null,
	@Active bit,
	@Deleted bit,
	@Editable bit,
	@Private bit,
	@cUserid int,  
	@Log smallint = 1
)
AS    
BEGIN    
 SET NOCOUNT ON;    
 DECLARE @Err Int    

 UPDATE [mea].[MeasureTemplate]
 SET
	[CustomerID] = @CustomerID,
	[MeasureTypeID] = @MeasureTypeID,
	[ChangedBy] = @ChangedBy,
	[ChangedDate] = getdate(),
	[Active] = @Active,
	[Deleted ] = @Deleted, 
	[Editable] = @Editable,
	[Private] = @Private
 WHERE
	[MeasureTemplateId] = @MeasureTemplateID
 
 Set @Err = @@Error 
  
 IF @Log = 1   
 BEGIN   
  INSERT INTO [Log].[AuditLog] ( UserId, TableName, Type, Data, Created)   
  SELECT @cUserid,'mea.MeasureTemplate',0,  
  ( SELECT * FROM [mea].[MeasureTemplate]   
   WHERE  
   [MeasureTemplateID] = @MeasureTemplateID    FOR XML AUTO) as data,  
    getdate()   
  END  
  
 RETURN @Err  

END

GO
