SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[at].[prc_Activity_upd]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [at].[prc_Activity_upd] AS' 
END
GO
/*  
	2017-06-13 Steve: Change the length of column ExtID in all related tables from xAPI format
*/
ALTER PROCEDURE [at].[prc_Activity_upd]
(
	@ActivityID int,
	@LanguageID int,
	@OwnerID int,
	@StyleSheet nvarchar(128),
	@Type smallint,
	@No smallint,
	@TooltipType smallint,
	@Listview smallint,
	@Descview smallint,
	@Chartview smallint,
	@OLAPServer nvarchar(64),
	@OLAPDB nvarchar(64),
	@SelectionHeaderType smallint = 0,
	@ExtID nvarchar(256)='',
	@cUserid int,
	@Log smallint = 1,
	@UseOLAP bit = 1,
	@ArchetypeID int = NULL
)
AS
BEGIN
	SET NOCOUNT ON
	DECLARE @Err Int

	UPDATE [at].[Activity]
	SET
		[LanguageID] = @LanguageID,
		[OwnerID] = @OwnerID,
		[StyleSheet] = @StyleSheet,
		[Type] = @Type,
		[No] = @No,
		[TooltipType] = @TooltipType,
		[Listview] = @Listview,
		[Descview] = @Descview,
		[Chartview] = @Chartview,
		[OLAPServer] = @OLAPServer,
		[OLAPDB] = @OLAPDB,
		[SelectionHeaderType] = @SelectionHeaderType,
		[ExtID] = @ExtID,
		[UseOLAP]= @UseOLAP,
		[ArchetypeID] = @ArchetypeID
	WHERE
		[ActivityID] = @ActivityID

	IF @Log = 1 
	BEGIN 
		INSERT INTO [Log].[AuditLog] ( UserId, TableName, Type, Data, Created) 
		SELECT @cUserid,'Activity',1,
		( SELECT * FROM [at].[Activity] 
			WHERE
			[ActivityID] = @ActivityID			 FOR XML AUTO) as data,
			getdate()
	END

	Set @Err = @@Error

	RETURN @Err
END

GO
