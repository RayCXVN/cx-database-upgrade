SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[rep].[prc_ReportFilter_ins]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [rep].[prc_ReportFilter_ins] AS' 
END
GO


ALTER PROCEDURE [rep].[prc_ReportFilter_ins]
(
	@ActivityID int,
	@QuestionID int,
	@FilterDim bit,
	@cUserid int,
	@Log smallint = 1
)
AS
BEGIN
	SET NOCOUNT ON
	DECLARE @Err Int

	INSERT INTO [rep].[ReportFilter]
	(
		[ActivityID],
		[QuestionID],
		[FilterDim] 
	)
	VALUES
	(
		@ActivityID,
		@QuestionID,
		@FilterDim
	)

	Set @Err = @@Error

	IF @Log = 1 
	BEGIN 
		INSERT INTO [Log].[AuditLog] ( UserId, TableName, Type, Data, Created) 
		SELECT @cUserid,'ReportFilter',0,
		( SELECT * FROM [rep].[ReportFilter] 
			WHERE
			[ActivityID] = @ActivityID AND
			[QuestionID] = @QuestionID				 FOR XML AUTO) as data,
				getdate() 
	 END

	RETURN @Err
END



GO
