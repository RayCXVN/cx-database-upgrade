SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[note].[prc_Note_del]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [note].[prc_Note_del] AS' 
END
GO
ALTER PROCEDURE [note].[prc_Note_del]
(
	@NoteID int,
	@cUserid int,
	@Log smallint = 1
)
AS
BEGIN
	SET NOCOUNT ON
	DECLARE @Err Int
	
	IF @Log = 1 
	BEGIN 
		INSERT INTO [Log].[AuditLog] ( UserId, TableName, Type, Data, Created) 
		SELECT @cUserid,'Note',2,
		( SELECT * FROM [note].[Note] 
			WHERE
			[NoteID] = @NoteID FOR XML AUTO) as data,
				getdate() 
	 END
	 
	DELETE FROM [note].[Note]
	WHERE
		[NoteID] = @NoteID
		
	Set @Err = @@Error
	
	RETURN @Err
END

GO
