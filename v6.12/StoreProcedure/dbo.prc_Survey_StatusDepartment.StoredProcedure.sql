SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[prc_Survey_StatusDepartment]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [dbo].[prc_Survey_StatusDepartment] AS' 
END
GO
ALTER proc [dbo].[prc_Survey_StatusDepartment]
@SurveyID as int,
@HierarchyID as int,
@bAll as Bit = 0
as
BEGIN
DECLARE @ActiveEntityStatusID INT = (SELECT EntityStatusID FROM EntityStatus WHERE CodeName='Active')

SELECT dbo.GetPathNameDep(@HierarchyID,department.departmentid) as [Path],  sum(case when resultid is null then 0 else 1 end) as Invited,
    sum(case when enddate is null then 0 else 1 end) as Finished, department.departmentid, [name]
FROM Result  join org.department on department.departmentid =  Result.departmentid and Result.EntityStatusID = @ActiveEntityStatusID and Result.Deleted IS NULL
 and surveyid =  @SurveyID  and department.EntityStatusID = @ActiveEntityStatusID AND department.Deleted IS NULL
GROUP BY department.departmentid,[name]

END
-------------------------------------------------------------------------------------


GO
