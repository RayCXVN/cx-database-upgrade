SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[org].[prc_H_D_get]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [org].[prc_H_D_get] AS' 
END
GO
/*  2016-10-10 Ray: Remove column Status from org.User, org.Department, RDB.dbo.Result
	2017-02-15 Steve: Change EntityStatus Condition (take all except Deactive)
*/
ALTER PROCEDURE [org].[prc_H_D_get]
(
	@ParentID		int = NULL,
	@HierarchyID	int = NULL,
	@DepartmentID int = NULL
)
AS
BEGIN
	SET NOCOUNT ON
	DECLARE @Err Int
    DECLARE @DeactiveEntityStatusID INT = (SELECT EntityStatusID FROM EntityStatus WHERE CodeName='Deactive')
	
	IF NOT @ParentID IS NULL
		Select
		hd.[HDID],
		hd.[HierarchyID],
		hd.[DepartmentID],
		ISNULL([ParentID], 0) ParentID,
		hd.[Path],
		hd.[PathName],
		hd.[Created]
		FROM org.[H_D] hd
		join org.department d  on d.departmentid = hd.departmentid
		WHERE hd.[ParentID] = @ParentID AND hd.[Deleted] = 0 and d.EntityStatusID <> @DeactiveEntityStatusID AND d.Deleted IS NULL
		--order by d.Name asc
	ELSE
		Select
		hd.[HDID],
		hd.[HierarchyID],
		hd.[DepartmentID],
		ISNULL([ParentID], 0) ParentID,
		hd.[Path],
		hd.[PathName],
		hd.[Created]
		FROM org.[H_D] hd
		join org.department d  on d.departmentid = hd.departmentid
		WHERE hd.[HierarchyID] = @HierarchyID AND hd.[Deleted] = 0 and d.EntityStatusID <> @DeactiveEntityStatusID AND d.Deleted IS NULL
		--WHERE hd.[HierarchyID] = @HierarchyID AND hd.Departmentid = @DepartmentID AND hd.[Deleted] = 0
		--order by d.Name asc
	Set @Err = @@Error

	RETURN @Err
END

GO
