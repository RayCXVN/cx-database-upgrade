SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[prc_Survey_StatusRapport]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [dbo].[prc_Survey_StatusRapport] AS' 
END
GO
--EXEC prc_Survey_StatusRapport @SurveyID = 43, @DepList= '34325,26567,24983'
ALTER proc [dbo].[prc_Survey_StatusRapport]
(
    @SurveyID AS INT,
    @DepList AS VARCHAR(6000)
)
AS
BEGIN

DECLARE @ActiveEntityStatusID INT = (SELECT EntityStatusID FROM EntityStatus WHERE CodeName='Active')
CREATE TABLE #DepartmentList
(
    DepartmentID INT
)

INSERT INTO #DepartmentList (DepartmentID)
SELECT * FROM dbo.funcListToTableInt(@DepList,',')

SELECT COUNT(*),COUNT(enddate),departmentid
FROM dbo.Result
WHERE surveyid = @SurveyID AND departmentid IN (SELECT DepartmentID FROM #DepartmentList)
    AND EntityStatusID = @ActiveEntityStatusID AND Deleted IS NULL
GROUP BY departmentid

DROP TABLE #DepartmentList

END
---------------------------------------------------------------------


GO
