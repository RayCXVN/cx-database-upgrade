SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[proc].[prc_Process_Copy]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [proc].[prc_Process_Copy] AS' 
END
GO
-- select * from [proc].process where processID IN (257,2902)
-- delete from [proc].process where processid = 2901
/*
Declare @N as int
exec [PROC].prc_Process_Copy @N,257,1,0,16572
Print @N
go
*/


ALTER PROC [proc].[prc_Process_Copy]
(
 @NewProcessID AS INT = NULL OUTPUT, 
 @ProcessID AS INT,
 @CopyAnswers AS BIT = 1,
 @CopyAnswersLog AS BIT = 1,
 @PeriodID AS INT = 0,
 @UserID AS INT = 0,
 @Active as bit = 1,
 @StartDate as datetime,
 @DueDate as datetime, 
 @DeactivateProcess AS BIT = 0,
 @CopyAccess as bit = 1
)
AS
  DECLARE @ID AS INT
  DECLARE @NewID AS INT 
  
  CREATE TABLE #PL
(
 ID INT,
 NEWID INT
)

 CREATE TABLE #ANS
(
 ID INT,
 NEWID INT
)
 SET XACT_ABORT ON
 
  IF @DeactivateProcess = 1
  BEGIN
    UPDATE [PROC].process SET  Active = 0 WHERE ProcessID = @ProcessID
  
  END


  INSERT INTO [PROC].Process (ProcessGroupID, DepartmentID, Active, TargetValue, UserID, CustomerID, StartDate, DueDate, Responsible, URL, NO, Created, ProcessTypeID, isPublic, ExtID, PeriodID) 
  SELECT ProcessGroupID, DepartmentID, @Active, TargetValue, UserID, CustomerID, @StartDate, @DueDate, Responsible, URL, NO, GETDATE() AS created, ProcessTypeID, isPublic, ExtID, CASE WHEN @PeriodID > 0 THEN @PeriodID ELSE PeriodID END
  FROM [PROC].Process  WHERE ProcessID = @ProcessID
  
  SELECT @NewProcessID = SCOPE_IDENTITY() 
  

  
   INSERT INTO [PROC].LT_Process (ProcessID, LanguageID, Name, Description, Created)
    SELECT @NewProcessID, LanguageID, Name, Description, GETDATE()
    FROM [PROC].LT_Process
    WHERE ProcessID = @ProcessID
   
   insert into [proc].[ProcessFile] (ProcessId, ContentType, Size, Title, FileName, Description, FileData, No, Created, CustomerID)
   Select @NewProcessID, ContentType, Size, Title, FileName, Description, FileData, No, Created,customerid from [proc].ProcessFile 
   Where ProcessID = @ProcessID
   

  DECLARE curPL CURSOR LOCAL FAST_FORWARD READ_ONLY FOR
  SELECT  ProcessLevelID
  FROM [PROC].ProcessLevel 
  WHERE ProcessID = @ProcessID

OPEN curPL
	FETCH NEXT FROM curPL INTO @ID

	WHILE (@@FETCH_STATUS = 0)
	BEGIN

  INSERT INTO [PROC].ProcessLevel ( ProcessValueID, ProcessID, Created)
  SELECT  ProcessValueID, @NewProcessID, GETDATE()
  FROM [PROC].ProcessLevel 
  WHERE ProcessLevelID = @ID
  
    SELECT @NewID = SCOPE_IDENTITY() 
    
    INSERT INTO [PROC].LT_ProcessLevel(ProcessLevelID, LanguageID, Name, Description, Created)
    SELECT @NewID, LanguageID, Name, Description, GETDATE()
    FROM [PROC].LT_ProcessLevel
    WHERE ProcessLevelID = @ID
    
    INSERT INTO #PL(ID,NEWID ) VALUES (@ID,@NewID ) 
    
    FETCH NEXT FROM curPL INTO @ID
  
  END
	CLOSE curPL
	DEALLOCATE curPL


  
  
  if @CopyAccess = 1
  BEGIN
	  INSERT INTO [PROC].ProcessAccess ( ProcessID, DepartmentID, Created)
	  SELECT  @NewProcessID, DepartmentID, GETDATE() 
	  FROM  [PROC].ProcessAccess
	  WHERE ProcessID = @ProcessID
  END 

  
  
  IF @CopyAnswers = 1
  BEGIN
     DECLARE curAns CURSOR LOCAL FAST_FORWARD READ_ONLY FOR
     SELECT ProcessAnswerID
     FROM [PROC].ProcessAnswer pa
     WHERE pa.ProcessID = @ProcessID
     
     OPEN curAns
	FETCH NEXT FROM curAns INTO @ID

	WHILE (@@FETCH_STATUS = 0)
	BEGIN
	
     INSERT INTO [PROC].ProcessAnswer (ProcessID, ProcessLevelID, DepartmentID, RoleID, UserID, LastModified, Created)
     SELECT @NewProcessID, #PL.NEWID, DepartmentID, RoleID, UserID, LastModified, GETDATE()
     FROM [PROC].ProcessAnswer pa
     JOIN #PL ON #PL.ID = pa.ProcessLevelID 
     WHERE pa.ProcessAnswerID = @ID
     
     SELECT @NewID = SCOPE_IDENTITY() 
      
      INSERT INTO #ANS ( ID, NEWID  ) VALUES (@ID, @NewID )
     
     INSERT INTO [PROC].ProcessAnswerLevel (ProcessAnswerID, ProcessLevelID, Description, Created)
     SELECT @NewID, #PL.NEWID , Description, GETDATE() 
     FROM  [PROC].ProcessAnswerLevel pl
     JOIN #PL ON #PL.ID = pl.ProcessLevelID 
     WHERE ProcessAnswerID = @ID
     
    
     Insert into [proc].ProcessAnswerFile (ProcessAnswerId, ContentType, Size, Title, FileName, Description, FileData, No, Created)
     Select @NewID, ContentType, Size, Title, FileName, Description, FileData, No, Created
     FROM [proc].ProcessAnswerFile
     WHERE ProcessAnswerId = @ID
     
     FETCH NEXT FROM curAns INTO @ID
    END
     
     CLOSE curAns
	DEALLOCATE curAns
	
	IF @CopyAnswersLog = 1
	BEGIN
	  INSERT INTO [PROC].ProcessAnswerLog (UserID, ProcessAnswerID, ProcessLevelID, Comment, Created)
	  SELECT  UserID, #ANS.NEWID , #PL.NEWID , Comment, Created
	  FROM [PROC].ProcessAnswerLog pla
	  JOIN #ANS ON #ANS.ID = pla.ProcessAnswerID
	  JOIN #PL ON #PL.ID = pla.ProcessLevelID 
	END
  END
  
    DROP TABLE #PL
    DROP TABLE #ANS

GO
