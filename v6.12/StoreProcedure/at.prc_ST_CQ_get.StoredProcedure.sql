SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[at].[prc_ST_CQ_get]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [at].[prc_ST_CQ_get] AS' 
END
GO

ALTER PROCEDURE [at].[prc_ST_CQ_get]
(
      @ScoreTemplateID int
)
AS
BEGIN
      SET NOCOUNT ON
      DECLARE @Err Int

      SELECT
      [ST_CQ_ID],
      [ScoreTemplateID],
      ISNULL([CategoryID], 0) AS 'CategoryID',
      ISNULL([QuestionID], 0) AS 'QuestionID',
      ISNULL([AlternativeID], 0) AS 'AlternativeID',
      [MinValue],
      [MaxValue],
      [ItemID]
      FROM [at].[ST_CQ]
      WHERE
      [ScoreTemplateID] = @ScoreTemplateID

      Set @Err = @@Error

      RETURN @Err
END



GO
