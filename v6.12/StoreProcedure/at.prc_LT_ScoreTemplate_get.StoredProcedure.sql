SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[at].[prc_LT_ScoreTemplate_get]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [at].[prc_LT_ScoreTemplate_get] AS' 
END
GO
ALTER PROCEDURE [at].[prc_LT_ScoreTemplate_get]
(
	@ScoreTemplateID int
)
AS
BEGIN
	SET NOCOUNT ON
	DECLARE @Err Int

	SELECT
	[ScoreTemplateID],
	[LanguageID],
	[Name],
	[Description]
	FROM [at].[LT_ScoreTemplate]
	WHERE
	[ScoreTemplateID] = @ScoreTemplateID

	Set @Err = @@Error

	RETURN @Err
END

GO
