SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[sec].[prc_AccessRule_ins]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [sec].[prc_AccessRule_ins] AS' 
END
GO
ALTER PROCEDURE [sec].[prc_AccessRule_ins]
(
	@AccessRuleID int = NULL output,
	@No int,
    @Inheritance int,
    @ItemID int,
    @TableTypeID smallint, 
    @TableTypeParameters  nvarchar(512),
    @Access int,
    @Type  int,
    @AccessRuleName NVARCHAR(512)='',
	@cUserid int,
	@Log smallint = 1
)
AS
BEGIN

	SET NOCOUNT ON
	DECLARE @Err Int
	INSERT INTO [sec].[AccessRule]
           ([No]
           ,[Inheritance]
           ,[ItemID]
           ,[TableTypeID]
           ,[TableTypeParameters]
           ,[Access]
           ,[Type]
           ,[AccessRuleName])
     VALUES
           (@No,
			@Inheritance,
			@ItemID,
			@TableTypeID, 
			@TableTypeParameters,
			@Access,
			@Type,
			@AccessRuleName
			)
	Set @Err = @@Error
	Set @AccessRuleID = scope_identity()
	IF @Log = 1 
	BEGIN 
		INSERT INTO [Log].[AuditLog] ( UserId, TableName, Type, Data, Created) 
		SELECT @cUserid,'AccessRule',0,
		( SELECT * FROM [sec].[AccessRule] 
			WHERE [AccessRuleID] = @AccessRuleID				 FOR XML AUTO) as data,	getdate() 
	 END
	RETURN @Err
END

GO
