SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[rep].[prc_LT_DocumentTemplate_upd]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [rep].[prc_LT_DocumentTemplate_upd] AS' 
END
GO

ALTER PROCEDURE [rep].[prc_LT_DocumentTemplate_upd]
(
	@LanguageID int,
	@DocumentTemplateID int,
	@yStartPos int = 0,
	@MIMEType nvarchar(50),
	@Size bigint,
	@FileContent as varbinary(Max) = null,
	@cUserid int,
	@Log smallint = 1
)
AS
BEGIN
	SET NOCOUNT ON
	DECLARE @Err Int

	UPDATE [rep].[LT_DocumentTemplate]
	SET
		[yStartPos] = @yStartPos,
		[MIMEType] = @MIMEType,
		[Size] = @Size,
		FileContent = @FileContent
	WHERE
		[LanguageID] = @LanguageID
		AND [DocumentTemplateID] = @DocumentTemplateID

	IF @Log = 1 
	BEGIN 
		INSERT INTO [Log].[AuditLog] ( UserId, TableName, Type, Data, Created) 
		SELECT @cUserid,'LT_DocumentTemplate',1,
		( SELECT * FROM [rep].[LT_DocumentTemplate] 
			WHERE
			[LanguageID] = @LanguageID
			AND [DocumentTemplateID] = @DocumentTemplateID			 FOR XML AUTO) as data,
			getdate()
	END

	Set @Err = @@Error

	RETURN @Err
END




GO
