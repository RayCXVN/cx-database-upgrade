SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[note].[prc_NoteTag_Note_ins]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [note].[prc_NoteTag_Note_ins] AS' 
END
GO
ALTER PROCEDURE [note].[prc_NoteTag_Note_ins]
(
	@NoteID int,
	@NoteTagID int,
	@cUserid int,
	@Log smallint = 1
)
AS
BEGIN
	SET NOCOUNT ON
	DECLARE @Err Int

	INSERT INTO [note].[NoteTag_Note]
	(
		[NoteID],
		[NoteTagID]
	)
	VALUES
	(
		@NoteID,
		@NoteTagID
	)
		
	Set @Err = @@Error
	
	IF @Log = 1 
	BEGIN 
		INSERT INTO [Log].[AuditLog] ( UserId, TableName, Type, Data, Created) 
		SELECT @cUserid,'NoteTag_Note',0,
		( SELECT * FROM [note].[NoteTag_Note] 
			WHERE
			[NoteID] = @NoteID
			AND [NoteTagID] = @NoteTagID FOR XML AUTO) as data,
				getdate() 
	 END

	RETURN @Err
END

GO
