SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[org].[prc_UT_U_upd]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [org].[prc_UT_U_upd] AS' 
END
GO

ALTER PROCEDURE [org].[prc_UT_U_upd]
(
	@UserTypeID int,
	@UserID int,
	@cUserid int,
	@Log smallint = 1
)
AS
BEGIN
	SET NOCOUNT ON
	DECLARE @Err Int

	UPDATE [org].[UT_U]
	SET
		[UserTypeID] = @UserTypeID,
		[UserID] = @UserID
	WHERE
		[UserTypeID] = @UserTypeID AND
		[UserID] = @UserID

	IF @Log = 1 
	BEGIN 
		INSERT INTO [Log].[AuditLog] ( UserId, TableName, Type, Data, Created) 
		SELECT @cUserid,'UT_U',1,
		( SELECT * FROM [org].[UT_U] 
			WHERE
			[UserTypeID] = @UserTypeID AND
			[UserID] = @UserID			 FOR XML AUTO) as data,
			getdate()
	END

	Set @Err = @@Error

	RETURN @Err
END


GO
