SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[job].[prc_Job_find]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [job].[prc_Job_find] AS' 
END
GO
ALTER PROCEDURE [job].[prc_Job_find]
(
	@JobID int
)
AS
BEGIN
	SET NOCOUNT ON
	DECLARE @Err Int

	SELECT
		[JobID],
		[JobTypeID],
		[JobStatusID],
		[OwnerID],
		[UserID],
		[Name],
		[Priority],
		ISNULL([Option], 0) AS 'Option',
		[Created],
		[StartDate],
		[EndDate],
		[Description]
	FROM [job].[Job]
	WHERE
		[JobID] = @JobID

	Set @Err = @@Error

	RETURN @Err
END

GO
