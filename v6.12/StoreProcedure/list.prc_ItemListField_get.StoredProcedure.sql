SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[list].[prc_ItemListField_get]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [list].[prc_ItemListField_get] AS' 
END
GO
ALTER PROCEDURE [list].[prc_ItemListField_get]
	@ItemListID int
AS
BEGIN
	SET NOCOUNT ON;
	DECLARE @Err Int
	
    SELECT 
		[ItemListFieldID],
		[ItemListID],
		[No],
		[TableTypeID],
		[FieldType],
		[ValueType],
		[FieldName],
		[PropID],
		[QuestionID],
		[OverrideLocked],
		[ShowInUserList],
		[ShowInTableView],
		[ShowInExport],
		[Mandatory],
		[Sortable],
		[SortNo],
		[SortDirection],
		[Width],
		[Align],
		[ItemPrefix],
		[ItemSuffix],
		[HeaderCssClass],
		[ItemCssClass],
		[ColumnCssClass],
		[HeaderGroupCSSClass],
		[HeaderGroupLastItem],
		[Created]
	FROM [list].[ItemListField]
	WHERE [ItemListID] = @ItemListID
	
	Set @Err = @@Error

	RETURN @Err
  
END

GO
