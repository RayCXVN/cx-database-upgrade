SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[at].[prc_LT_A_S_upd]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [at].[prc_LT_A_S_upd] AS' 
END
GO
ALTER PROCEDURE [at].[prc_LT_A_S_upd] (
	@ASID INT,
	@LanguageID INT,
	@StatusName NVARCHAR(512),
	@StatusDescription NVARCHAR(512),
	@ReadOnlyText NVARCHAR(MAX) = '',
	@cUserid INT,
	@Log SMALLINT = 1
	)
AS
BEGIN
	SET NOCOUNT ON

	DECLARE @Err INT

	UPDATE [at].[LT_A_S]
	SET [LanguageID] = @LanguageID,
		[StatusName] = @StatusName,
		[StatusDescription] = @StatusDescription,
		[ReadOnlyText] = @ReadOnlyText
	WHERE [LanguageID] = @LanguageID
		AND [ASID] = @ASID

	IF @Log = 1
	BEGIN
		INSERT INTO [Log].[AuditLog] (
			UserId,
			TableName,
			Type,
			Data,
			Created
			)
		SELECT @cUserid,
			'LT_A_S',
			1,
			(
				SELECT *
				FROM [at].[LT_A_S]
				WHERE [LanguageID] = @LanguageID
					AND [ASID] = @ASID
				FOR XML AUTO
				) AS data,
			getdate()
	END

	SET @Err = @@Error

	RETURN @Err
END

GO
