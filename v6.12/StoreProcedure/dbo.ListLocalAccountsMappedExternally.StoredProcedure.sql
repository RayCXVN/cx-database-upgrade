SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[ListLocalAccountsMappedExternally]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [dbo].[ListLocalAccountsMappedExternally] AS' 
END
GO
ALTER PROCEDURE [dbo].[ListLocalAccountsMappedExternally]
(
	@OwnerID		int	,
	@PropertyID		int	,
	@PropertyValue	nvarchar(1000) --nvarchar(MAX) /* Begrenset denne til 1000 tegn */
)
AS
BEGIN
DECLARE @EntityStatusID INT = 0 
SELECT @EntityStatusID=EntityStatusID FROM EntityStatus WHERE CodeName='Active'
SELECT 	
	u.DepartmentID as DepartmentID,
	u.UserID as UserID,
	u.OwnerId as OwnerID,
	u.Created as CreatedDate,
	u.Deleted as Deleted,
	u.EntityStatusID as EntityStatusID,
	u.EntityStatusReasonID as EntityStatusReasonID
FROM 	
	[org].[User] u
	INNER JOIN [prop].[PropValue] pv ON pv.ItemID = u.UserID AND pv.PropertyID = @PropertyID
	INNER JOIN [prop].[Prop]p ON p.PropertyID = pv.PropertyID
	INNER JOIN [prop].[PropPage]ppage ON ppage.PropPageID = p.PropPageID AND ppage.TableTypeID = 13 /* org.User */
WHERE
	pv.Value = @PropertyValue
    AND u.EntityStatusID=@EntityStatusID /* Usikker på hva dette er, antakelig at brukeren er slettet? hilsen Geir */
    AND u.Deleted IS NULL
    AND u.[Ownerid] = @OwnerId

return @@rowcount

END

GO
