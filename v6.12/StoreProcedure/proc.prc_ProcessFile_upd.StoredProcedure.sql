SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[proc].[prc_ProcessFile_upd]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [proc].[prc_ProcessFile_upd] AS' 
END
GO


ALTER PROCEDURE [proc].[prc_ProcessFile_upd]
(
	@ProcessFileId int,
	@ProcessId int,
	@ContentType nvarchar(64),
	@Size bigint,
	@Title nvarchar(64),
	@FileName nvarchar(256),
	@Description nvarchar(128),
	@FileData varbinary(max),
	@No smallint,
	@CustomerID int = NULL,
	@cUserid int,
	@Log smallint = 1
)
AS
BEGIN
	SET NOCOUNT ON
	DECLARE @Err Int

	UPDATE [proc].[ProcessFile]
	SET
		[ProcessId] = @ProcessId,
		[ContentType] = @ContentType,
		[Size] = @Size,
		[Title] = @Title,
		[FileName] = @FileName,
		[Description] = @Description,
		[FileData] = @FileData,
		[No] = @No ,
		[CustomerID] = @CustomerID
	WHERE
		[ProcessFileId] = @ProcessFileId

	IF @Log = 1 
	BEGIN 
		INSERT INTO [Log].[AuditLog] ( UserId, TableName, Type, Data, Created) 
		SELECT @cUserid,'ProcessFile',1,
		( SELECT * FROM [proc].[ProcessFile] 
			WHERE
			[ProcessFileId] = @ProcessFileId			 FOR XML AUTO) as data,
			getdate()
	END

	Set @Err = @@Error

	RETURN @Err
END

GO
