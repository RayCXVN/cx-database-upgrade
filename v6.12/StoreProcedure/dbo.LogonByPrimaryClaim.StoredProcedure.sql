SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[LogonByPrimaryClaim]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [dbo].[LogonByPrimaryClaim] AS' 
END
GO
ALTER PROCEDURE [dbo].[LogonByPrimaryClaim]
(
	@OwnerID		int	,
	@LoginServiceID int ,
	@PropertyID		int ,
	@PropertyValue	nvarchar(1000),
	@ErrNo			int		OUTPUT
)
AS
BEGIN
	DECLARE @ret as integer, @EntityStatusID INT = 0
	SELECT @EntityStatusID = EntityStatusID FROM dbo.EntityStatus WHERE CodeName = N'Active'

	IF @PropertyID IS NULL
		BEGIN
		SELECT 	
			u.UserName,
			u.DepartmentID,
			u.UserID,
			u.OwnerId,
			u.FirstName,
			u.LastName,
			d.Name AS DepartmentName,
			u.LanguageID,
			d.CustomerID,
			u.SSN
		FROM 	
			[org].[User] u
			INNER JOIN app.LoginService_User lsu ON lsu.UserID = u.UserID AND lsu.LoginServiceID = @LoginServiceID
			INNER JOIN org.Department d ON u.DepartmentID = d.DepartmentID
		WHERE
			lsu.PrimaryClaimValue = @PropertyValue
			AND u.EntityStatusID = @EntityStatusID /* Exclude deleted users */
			AND u.Deleted IS NULL
			AND u.[Ownerid] = @OwnerId
			AND lsu.PrimaryClaimValue <>''
		ORDER BY u.Created 
		END
	ELSE
		BEGIN
		SELECT 	
			u.UserName,
			u.DepartmentID,
			u.UserID,
			u.OwnerId,
			u.FirstName,
			u.LastName,
			d.Name AS DepartmentName,
			u.LanguageID,
			d.CustomerID
		FROM 	
			[org].[User] u
			INNER JOIN [prop].[PropValue] pv ON pv.ItemID = u.UserID AND pv.PropertyID = @PropertyID
			INNER JOIN [prop].[Prop]p ON p.PropertyID = pv.PropertyID
			INNER JOIN [prop].[PropPage]ppage ON ppage.PropPageID = p.PropPageID AND ppage.TableTypeID = 13 /* org.User */
			INNER JOIN org.Department d ON u.DepartmentID = d.DepartmentID
		WHERE
			pv.Value = @PropertyValue
			AND u.EntityStatusID = @EntityStatusID /* Exclude deleted users */
			AND u.Deleted IS NULL
			AND u.[Ownerid] = @OwnerId
			
		ORDER BY u.Created  
		END  
    
SET @ret = @@ROWCOUNT
IF @ret < 1 
BEGIN
	SET @ErrNo = 1 /* Ingen brukere ble funnet */
END
ELSE
BEGIN	
	IF @ret > 1 
	BEGIN
		SET @ErrNo = 2 /* Flere brukere med samme propertyvalue ble funnet! */
	END
	ELSE
	BEGIN	
		SET @ErrNo = 0
	END
END
END

GO
