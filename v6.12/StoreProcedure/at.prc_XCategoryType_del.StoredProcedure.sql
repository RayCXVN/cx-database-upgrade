SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[at].[prc_XCategoryType_del]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [at].[prc_XCategoryType_del] AS' 
END
GO

ALTER PROCEDURE [at].[prc_XCategoryType_del] (
	@XCategoryTypeID INT
	,@cUserid INT
	,@Log SMALLINT = 1
	)
AS
BEGIN
	SET NOCOUNT ON

	DECLARE @Err INT

	IF @Log = 1
	BEGIN
		INSERT INTO [Log].[AuditLog] (
			UserId
			,TableName
			,Type
			,Data
			,Created
			)
		SELECT @cUserid
			,'CategoryType'
			,2
			,(
				SELECT *
				FROM [at].[XCategoryType]
				WHERE [XCTypeID] = @XCategoryTypeID
				FOR XML AUTO
				) AS data
			,GETDATE()
	END

	DELETE
	FROM [at].[XCategoryType]
	WHERE [XCTypeID] = @XCategoryTypeID

	SET @Err = @@Error

	RETURN @Err
END

GO
