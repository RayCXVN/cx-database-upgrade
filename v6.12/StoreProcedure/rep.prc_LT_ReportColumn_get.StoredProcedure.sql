SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[rep].[prc_LT_ReportColumn_get]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [rep].[prc_LT_ReportColumn_get] AS' 
END
GO

ALTER PROCEDURE [rep].[prc_LT_ReportColumn_get]
(
	@ReportColumnID int
)
AS
BEGIN
	SET NOCOUNT ON
	DECLARE @Err Int

	SELECT
	[LanguageID],
	[ReportColumnID],
	[Text],
	[ToolTip]
	FROM [rep].[LT_ReportColumn]
	WHERE
	[ReportColumnID] = @ReportColumnID

	Set @Err = @@Error

	RETURN @Err
END


GO
