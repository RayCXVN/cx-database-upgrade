SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[at].[prc_CalcField_upd]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [at].[prc_CalcField_upd] AS' 
END
GO
ALTER PROCEDURE  [at].[prc_CalcField_upd]
(
	@CFID int,
	@ActivityID int,
	@Name nvarchar(256),
	@Formula nvarchar(512),
	@Format nvarchar(32),
	@AllParametersMustExist bit,
	@Type int = 1,
	@cUserid int,
	@Log smallint = 1
)
AS
BEGIN
	SET NOCOUNT ON
	DECLARE @Err Int

	UPDATE [at].[CalcField]
	SET
		[ActivityID] = @ActivityID,
		[Name] = @Name,
		[Formula] = @Formula,
		[Format] = @Format,
		[AllParametersMustExist] = @AllParametersMustExist,
		[Type] =@Type
	WHERE
		[CFID] = @CFID

	IF @Log = 1 
	BEGIN 
		INSERT INTO [Log].[AuditLog] ( UserId, TableName, Type, Data, Created) 
		SELECT @cUserid,'CalcField',1,
		( SELECT * FROM [at].[CalcField] 
			WHERE
			[CFID] = @CFID			 FOR XML AUTO) as data,
			getdate()
	END

	Set @Err = @@Error

	RETURN @Err
END

GO
