SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[rep].[prc_Selection_User_upd]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [rep].[prc_Selection_User_upd] AS' 
END
GO

ALTER PROCEDURE [rep].[prc_Selection_User_upd]
(
	@SelectionID int,
	@UserID int,
	@cUserid int,
	@Log smallint = 1
)
AS
BEGIN
	SET NOCOUNT ON
	DECLARE @Err Int

	UPDATE [rep].[Selection_User]
	SET
		[SelectionID] = @SelectionID,
		[UserID] = @UserID
	WHERE
		[SelectionID] = @SelectionID AND
		[UserID] = @UserID

	IF @Log = 1 
	BEGIN 
		INSERT INTO [Log].[AuditLog] ( UserId, TableName, Type, Data, Created) 
		SELECT @cUserid,'Selection_User',1,
		( SELECT * FROM [rep].[Selection_User] 
			WHERE
			[SelectionID] = @SelectionID AND
			[UserID] = @UserID			 FOR XML AUTO) as data,
			getdate()
	END

	Set @Err = @@Error

	RETURN @Err
END


GO
