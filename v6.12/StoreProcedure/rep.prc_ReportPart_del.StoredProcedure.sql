SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[rep].[prc_ReportPart_del]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [rep].[prc_ReportPart_del] AS' 
END
GO


ALTER PROCEDURE [rep].[prc_ReportPart_del]
(
	@ReportPartID int,
	@cUserid int,
	@Log smallint = 1
)
AS
BEGIN
	SET NOCOUNT ON
	DECLARE @Err Int

	IF @Log = 1 
	BEGIN 
		INSERT INTO [Log].[AuditLog] ( UserId, TableName, Type, Data, Created) 
		SELECT @cUserid,'ReportPart',2,
		( SELECT * FROM [rep].[ReportPart] 
			WHERE
			[ReportPartID] = @ReportPartID
			 FOR XML AUTO) as data,
			getdate() 
	END 


	DELETE FROM [rep].[ReportPart]
	WHERE
		[ReportPartID] = @ReportPartID

	Set @Err = @@Error

	RETURN @Err
END


GO
