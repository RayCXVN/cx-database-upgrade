SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[proc].[prc_P_P_del]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [proc].[prc_P_P_del] AS' 
END
GO
ALTER PROCEDURE [proc].[prc_P_P_del]
(
	@ProcessID int,
	@ToProcessID int,
	@cUserid int,
	@Log smallint = 1
)
AS
BEGIN
	SET NOCOUNT ON
	DECLARE @Err Int

	IF @Log = 1 
	BEGIN 
		INSERT INTO [Log].[AuditLog] ( UserId, TableName, Type, Data, Created) 
		SELECT @cUserid,'P_P',2,
		( SELECT * FROM [proc].[P_P] 
			WHERE
			[ProcessID] = @ProcessID AND
			[ToProcessID] = @ToProcessID
			 FOR XML AUTO) as data,
			getdate() 
	END 


	DELETE FROM [proc].[P_P]
	WHERE
		[ProcessID] = @ProcessID AND
		[ToProcessID] = @ToProcessID

	Set @Err = @@Error

	RETURN @Err
END

GO
