SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[at].[prc_Access_Copy]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [at].[prc_Access_Copy] AS' 
END
GO
ALTER PROC [at].[prc_Access_Copy](
    @FromSurveyID int,
    @ToSurveyID   int)
AS
    INSERT INTO [at].[Access] ([SurveyID], [RoleID], [DepartmentID], [DepartmentTypeID], [CustomerID], [PageID], [QuestionID], [Type], [Mandatory], [ArchetypeId])
    SELECT @ToSurveyID, [RoleID], [DepartmentID], [DepartmentTypeID], [Customerid], [PageID], [QuestionID], [Type], [Mandatory], [ArchetypeId]
    FROM [at].[Access]
    WHERE [SurveyID] = @FromSurveyID;
GO
