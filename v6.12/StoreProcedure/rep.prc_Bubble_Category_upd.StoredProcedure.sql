SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[rep].[prc_Bubble_Category_upd]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [rep].[prc_Bubble_Category_upd] AS' 
END
GO

ALTER PROCEDURE [rep].[prc_Bubble_Category_upd]
(
	@BubbleID int,
	@CategoryID int,
	@AxisNo smallint,
	@cUserid int,
	@Log smallint = 1
)
AS
BEGIN
	SET NOCOUNT ON
	DECLARE @Err Int

	UPDATE [rep].[Bubble_Category]
	SET
		[BubbleID] = @BubbleID,
		[CategoryID] = @CategoryID,
		[AxisNo] = @AxisNo
	WHERE
		[BubbleID] = @BubbleID AND
		[CategoryID] = @CategoryID

	IF @Log = 1 
	BEGIN 
		INSERT INTO [Log].[AuditLog] ( UserId, TableName, Type, Data, Created) 
		SELECT @cUserid,'Bubble_Category',1,
		( SELECT * FROM [rep].[Bubble_Category] 
			WHERE
			[BubbleID] = @BubbleID AND
			[CategoryID] = @CategoryID			 FOR XML AUTO) as data,
			getdate()
	END

	Set @Err = @@Error

	RETURN @Err
END


GO
