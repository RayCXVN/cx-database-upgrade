IF NOT EXISTS (SELECT 1 FROM sys.objects WHERE name = N'prc_UGMember_ins' AND type IN ( N'P', N'PC' )  AND SCHEMA_NAME(schema_id) = 'org')
    EXEC ('CREATE PROC org.prc_UGMember_ins AS ')
GO
/*
    2018-07-03 Sarah    Created for ATAdmin
    2019-02-22 Ray      Default values for optional parameters
*/
ALTER PROCEDURE [org].[prc_UGMember_ins](
    @UGMemberID           bigint        = NULL OUTPUT,
    @UserGroupID          int,
    @UserID               int           = NULL,
    @CreatedBy            int           = NULL,
    @MemberRoleID         int           = NULL,
    @ValidFrom            datetime2(7)  = NULL,
    @ValidTo              datetime2(7)  = NULL,
    @LastUpdatedBy        int           = NULL,
    @LastSynchronized     datetime2(7)  = NULL,
    @EntityStatusID       int           = NULL,
    @EntityStatusReasonID int           = NULL,
    @CustomerID           int           = NULL,
    @PeriodID             int           = NULL,
    @ExtID                nvarchar(256) = '',
    @Deleted              datetime2(7)  = NULL,
    @DisplayName          nvarchar(max) = '',
    @ReferrerResource     nvarchar(512) = '',
    @ReferrerToken        nvarchar(512) = '',
    @ReferrerArchetypeID  int           = NULL,
    @cUserid              int,
    @Log                  smallint      = 1
) AS
BEGIN
    SET NOCOUNT ON;
    DECLARE @Err int;

    INSERT INTO [org].[UGMember] ([UserGroupID], [UserID], [Created], [CreatedBy], [MemberRoleID], [ValidFrom], [ValidTo], [LastUpdated], [LastUpdatedBy],
                                  [LastSynchronized], [EntityStatusID], [EntityStatusReasonID], [CustomerID], [PeriodID], [ExtID], [Deleted], [DisplayName],
                                  [ReferrerResource], [ReferrerToken], [ReferrerArchetypeID])
    VALUES (@UserGroupID, @UserID, GETDATE(), @CreatedBy, @MemberRoleID, @ValidFrom, @ValidTo, GETDATE(), @LastUpdatedBy,
            @LastSynchronized, @EntityStatusID, @EntityStatusReasonID, @CustomerID, @PeriodID, @ExtID, @Deleted, @DisplayName,
            @ReferrerResource, @ReferrerToken, @ReferrerArchetypeID);

    SET @Err = @@Error;
    SET @UGMemberID = SCOPE_IDENTITY();

    IF @Log = 1
    BEGIN
        INSERT INTO [Log].[AuditLog] ([UserId], [TableName], [Type], [Data], [Created])
        SELECT @cUserid, 'UGMember', 0, (SELECT * FROM [org].[UGMember] WHERE [UGMemberID] = @UGMemberID FOR XML AUTO) AS [data], GETDATE();
    END;

    RETURN @Err;
END;
GO