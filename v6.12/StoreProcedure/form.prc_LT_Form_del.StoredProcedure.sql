SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[form].[prc_LT_Form_del]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [form].[prc_LT_Form_del] AS' 
END
GO


ALTER PROCEDURE [form].[prc_LT_Form_del]
(
	@LanguageID int,
	@FormID int,
	@cUserid int,
	@Log smallint = 1
)
AS
BEGIN
	SET NOCOUNT ON
	DECLARE @Err Int

	IF @Log = 1 
	BEGIN 
		INSERT INTO [Log].[AuditLog] ( UserId, TableName, Type, Data, Created) 
		SELECT @cUserid,'LT_Form',2,
		( SELECT * FROM [form].[LT_Form] 
			WHERE
			[LanguageID] = @LanguageID AND
			[FormID] = @FormID
			 FOR XML AUTO) as data,
			getdate() 
	END 


	DELETE FROM [form].[LT_Form]
	WHERE
		[LanguageID] = @LanguageID AND
		[FormID] = @FormID

	Set @Err = @@Error

	RETURN @Err
END


GO
