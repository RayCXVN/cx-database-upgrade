SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[app].[prc_PortalPage_copy]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [app].[prc_PortalPage_copy] AS' 
END
GO
     
ALTER PROCEDURE [app].[prc_PortalPage_copy]    
(    
 @PortalPageID int,  
 @NewPortalPageID int = null output    
)    
AS    
BEGIN    
  
 SET XACT_ABORT ON    
 BEGIN TRANSACTION    
 DECLARE @Err Int,    
 @OwnerID int,    
 @No smallint,    
 @Parameter nvarchar(1024) = '',    
 @PortalPartID AS INT,  
 @NewPortalPartId AS INT  
   
 SELECT    
  @OwnerID = [OwnerID],    
  @No = [No] ,    
  @Parameter = [Parameter]  
 FROM [app].[PortalPage]     
 WHERE    
  [PortalPageID] = @PortalPageID    
    
   INSERT INTO [app].[PortalPage]    
  (    
   [OwnerID],    
   [No],    
   [Parameter]    
  )    
  VALUES    
  (    
   @OwnerID,    
   @No,    
   @Parameter    
  )    
   
  Set @NewPortalPageID = scope_identity()    
    
   --insert LT_PortalPage  
  INSERT INTO [app].[LT_PortalPage]  
  ([LanguageID]  
   ,[PortalPageID]  
      ,[Name]  
      ,[Description])  
  (SELECT [LanguageID]  
   ,@NewPortalPageID  
      ,[Name]  
      ,[Description]  
   FROM [app].[LT_PortalPage]  
   WHERE ([PortalPageID] = @PortalPageID))  
   
  -- PortalParts    
DECLARE curPortalPart CURSOR LOCAL FAST_FORWARD READ_ONLY FOR    
SELECT PortalPartID    
FROM [app].[PortalPart]  
WHERE  [PortalPageID] = @PortalPageID  
    
OPEN curPortalPart    
FETCH NEXT FROM curPortalPart INTO @PortalPartID    
    
WHILE (@@FETCH_STATUS = 0)    
BEGIN    
    INSERT INTO [app].[PortalPart]  
    ([PortalPageID]  
      ,[PortalPartTypeID]  
      ,[No]  
      ,[CategoryID]  
      ,[Settings]  
      ,[BubbleID]  
      ,[ReportPartID]  
      ,[ReportID]  
      ,[CssClass]  
      ,[InlineStyle]  
      ,[ActivityID]  
      ,[SurveyID]  
      ,[ExtID])  
     (SELECT @NewPortalPageID  
   ,[PortalPartTypeID]  
      ,[No]  
      ,[CategoryID]  
      ,[Settings]  
      ,[BubbleID]  
      ,[ReportPartID]  
      ,[ReportID]  
      ,[CssClass]  
      ,[InlineStyle]  
      ,[ActivityID]  
      ,[SurveyID]  
      ,[ExtID]  
   FROM [app].[PortalPart]  
   WHERE ([PortalPartID] = @PortalPartID))  
     
      
   SET @NewPortalPartId = SCOPE_IDENTITY()    
     
   --insert LT_PortalPart  
   INSERT INTO [app].[LT_PortalPart]  
           ( [LanguageID] ,  
             [PortalPartID] ,  
             [Name] ,  
             [Description] ,  
             [Text] ,  
             [TextAbove] ,  
             [TextBelow] ,  
             [HeaderText] ,  
             [FooterText] ,  
             [BeforeContentText] ,  
             [AfterContentText]  
           )  
   (SELECT  
   [LanguageID],  
             @NewPortalPartId ,  
             [Name] ,  
             [Description] ,  
             [Text] ,  
             [TextAbove] ,  
             [TextBelow] ,  
             [HeaderText] ,  
             [FooterText] ,  
             [BeforeContentText] ,  
             [AfterContentText]  
    FROM [app].[LT_PortalPart]  
    WHERE [PortalPartID] = @PortalPartID)  
        
    FETCH NEXT FROM curPortalPart INTO @PortalPartID    
END    
CLOSE curPortalPart    
DEALLOCATE curPortalPart    
  
     
    
    
 COMMIT TRANSACTION    
      
END    

GO
