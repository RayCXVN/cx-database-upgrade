SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[app].[prc_SitePortalPartSetting_get]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [app].[prc_SitePortalPartSetting_get] AS' 
END
GO
ALTER PROCEDURE [app].[prc_SitePortalPartSetting_get]      
 @SiteID int      
AS      
BEGIN      
 SET NOCOUNT ON;      
 DECLARE @Err Int          
 SELECT       
   [SitePortalpartSettingID]
      ,[SiteID]
      ,[No]
      ,[RenderQuestionIfDottedOrZeroForAllSelections]
      ,[RenderCategoryIfDottedOrZeroForAllSelections]
      ,[RenderReportPartRowIfDottedOrZeroForAllSelections]
      ,[ShowAverageOnFactorPortalPart]
      ,[ShowLevelLimitsOnFactorPortalPart]
      ,[ShowLevelOnFactorPortalPart]
 FROM             
  [SitePortalpartSetting]      
 WHERE        
  [SitePortalpartSetting].SiteID = @SiteID      
   ORDER BY [No]  
 Set @Err = @@Error      
      
 RETURN @Err      
END 

GO
