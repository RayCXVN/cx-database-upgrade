SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[rep].[prc_DF_O_upd]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [rep].[prc_DF_O_upd] AS' 
END
GO


ALTER PROCEDURE [rep].[prc_DF_O_upd]
(
	@DF_O_ID int,
	@OwnerID int,
	@DocumentFormatID int,
	@cUserid int,
	@Log smallint = 1
)
AS
BEGIN
	SET NOCOUNT ON
	DECLARE @Err Int

	UPDATE [rep].[DF_O]
	SET
		[OwnerID] = @OwnerID,
		[DocumentFormatID] = @DocumentFormatID
	WHERE
		[DF_O_ID] = @DF_O_ID

	IF @Log = 1 
	BEGIN 
		INSERT INTO [Log].[AuditLog] ( UserId, TableName, Type, Data, Created) 
		SELECT @cUserid,'DF_O',1,
		( SELECT * FROM [rep].[DF_O] 
			WHERE
			[DF_O_ID] = @DF_O_ID			 FOR XML AUTO) as data,
			getdate()
	END

	Set @Err = @@Error

	RETURN @Err
END



GO
