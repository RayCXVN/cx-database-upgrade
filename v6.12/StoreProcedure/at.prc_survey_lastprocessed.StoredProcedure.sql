SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[at].[prc_survey_lastprocessed]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [at].[prc_survey_lastprocessed] AS' 
END
GO
ALTER PROC [at].[prc_survey_lastprocessed]
(
  @SurveyID INT
 )
 AS
 BEGIN
   UPDATE AT.Survey SET LastProcessed = '1900-01-01'
   WHERE SurveyID = @SurveyID
 
 END
GO
