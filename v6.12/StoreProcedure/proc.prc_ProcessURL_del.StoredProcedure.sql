SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[proc].[prc_ProcessURL_del]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [proc].[prc_ProcessURL_del] AS' 
END
GO

ALTER PROCEDURE [proc].[prc_ProcessURL_del]
(
	@ProcessURLID int,
	@cUserid int,
	@Log smallint = 1
)
AS
BEGIN
	SET NOCOUNT ON
	DECLARE @Err Int

	IF @Log = 1 
	BEGIN 
		INSERT INTO [Log].[AuditLog] ( UserId, TableName, Type, Data, Created) 
		SELECT @cUserid,'ProcessURL',2,
		( SELECT * FROM [proc].[ProcessURL] 
			WHERE
			[ProcessURLID] = @ProcessURLID
			 FOR XML AUTO) as data,
			getdate() 
	END 


	DELETE FROM [proc].[ProcessURL]
	WHERE
		[ProcessURLID] = @ProcessURLID

	Set @Err = @@Error

	RETURN @Err
END

GO
