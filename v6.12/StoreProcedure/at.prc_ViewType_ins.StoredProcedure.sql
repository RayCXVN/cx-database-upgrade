SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[at].[prc_ViewType_ins]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [at].[prc_ViewType_ins] AS' 
END
GO

ALTER PROCEDURE [at].[prc_ViewType_ins]
(
	@ViewTypeID int = null output,
	@Type int,
    @No smallint,
	@cUserid int,
	@Log smallint = 1
)
AS
BEGIN
	SET NOCOUNT ON
	DECLARE @Err Int

	INSERT INTO [at].[ViewType]
	(
		[Type],
		[No]
	)
	VALUES
	(
		@Type,
		@No

	)

	Set @Err = @@Error
	Set @ViewTypeID = scope_identity()

	IF @Log = 1 
	BEGIN 
		INSERT INTO [Log].[AuditLog] ( UserId, TableName, Type, Data, Created) 
		SELECT @cUserid,'ViewType',0,
		( SELECT * FROM [at].[ViewType] 
			WHERE
			[ViewTypeID] = @ViewTypeID				 FOR XML AUTO) as data,
				getdate() 
	 END

	RETURN @Err
END


GO
