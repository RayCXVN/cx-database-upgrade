SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[prc_ObjectMapping_ins]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [dbo].[prc_ObjectMapping_ins] AS' 
END
GO


ALTER PROCEDURE [dbo].[prc_ObjectMapping_ins]
(
	@OMID int = null output,
	@OwnerID int,
	@FromTableTypeID smallint,
	@FromID int,
	@ToTableTypeID smallint,
	@ToID int,
	@RelationTypeID int,
	@cUserid int,
	@Log smallint = 1
)
AS
BEGIN
	SET NOCOUNT ON
	DECLARE @Err Int

	INSERT INTO [dbo].[ObjectMapping]
	(
		[OwnerID],
		[FromTableTypeID],
		[FromID],
		[ToTableTypeID],
		[ToID],
		[RelationTypeID]
	)
	VALUES
	(
		@OwnerID,
		@FromTableTypeID,
		@FromID,
		@ToTableTypeID,
		@ToID,
		@RelationTypeID
	)

	Set @Err = @@Error
	Set @OMID = scope_identity()

	IF @Log = 1 
	BEGIN 
		INSERT INTO [Log].[AuditLog] ( UserId, TableName, Type, Data, Created) 
		SELECT @cUserid,'ObjectMapping',0,
		( SELECT * FROM [dbo].[ObjectMapping] 
			WHERE
			[OMID] = @OMID				 FOR XML AUTO) as data,
				getdate() 
	 END

	RETURN @Err
END



GO
