SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[app].[prc_LT_MenuItem_ins]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [app].[prc_LT_MenuItem_ins] AS' 
END
GO
ALTER PROCEDURE [app].[prc_LT_MenuItem_ins]  
(  
 @LanguageID int,  
 @MenuItemID int,  
 @Name nvarchar(256)='',  
 @Description NTEXT='',  
 @ToolTip nvarchar(512)='',  
 @URL nvarchar(512)='',  
 @cUserid int,  
 @Log smallint = 1  
)  
AS  
BEGIN  
 SET NOCOUNT ON  
 DECLARE @Err Int  
  
 INSERT INTO [app].[LT_MenuItem]  
 (  
  [LanguageID],  
  [MenuItemID],  
  [Name],  
  [Description],  
  [ToolTip],  
  [URL]
 )  
 VALUES  
 (  
  @LanguageID,  
  @MenuItemID,  
  @Name,  
  @Description,  
  @ToolTip,
  @URL  
 )  
  
 Set @Err = @@Error  
  
 IF @Log = 1   
 BEGIN   
  INSERT INTO [Log].[AuditLog] ( UserId, TableName, Type, Data, Created)   
  SELECT @cUserid,'LT_MenuItem',0,  
  ( SELECT * FROM [app].[LT_MenuItem]   
   WHERE  
   [LanguageID] = @LanguageID AND  
   [MenuItemID] = @MenuItemID     FOR XML AUTO) as data,  
    getdate()   
  END  
  
 RETURN @Err  
END  
  

GO
