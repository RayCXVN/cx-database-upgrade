SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[prop].[prc_PropOption_ins]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [prop].[prc_PropOption_ins] AS' 
END
GO

ALTER PROCEDURE [prop].[prc_PropOption_ins]
(
	@PropOptionID int = null output,
	@PropertyID int,
	@Value float,
	@cUserid int,
	@Log smallint = 1
)
AS
BEGIN
	SET NOCOUNT ON
	DECLARE @Err Int

	INSERT INTO [prop].[PropOption]
	(
		[PropertyID],
		[Value]
	)
	VALUES
	(
		@PropertyID,
		@Value
	)

	Set @Err = @@Error
	Set @PropOptionID = scope_identity()

	IF @Log = 1 
	BEGIN 
		INSERT INTO [Log].[AuditLog] ( UserId, TableName, Type, Data, Created) 
		SELECT @cUserid,'PropOption',0,
		( SELECT * FROM [prop].[PropOption] 
			WHERE
			[PropOptionID] = @PropOptionID				 FOR XML AUTO) as data,
				getdate() 
	 END

	RETURN @Err
END


GO
