SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[mea].[prc_MeasureTemplate_ins]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [mea].[prc_MeasureTemplate_ins] AS' 
END
GO
ALTER PROCEDURE [mea].[prc_MeasureTemplate_ins]    
(
	@MeasureTemplateID int = null output,
	@CustomerID int = null,
	@MeasureTypeID int,
	@CreatedBy int,
	@ChangedBy int = null,
	@ChangedDate datetime = null,
	@Active bit,
	@Deleted bit,
	@Editable bit,
	@Private bit,
	@cUserid int,  
	@Log smallint = 1
)
AS    
BEGIN    
 SET NOCOUNT ON;    
 DECLARE @Err Int    

 INSERT INTO [mea].[MeasureTemplate]
 (
	[CustomerID],
	[MeasureTypeID],
	[CreatedBy],
	[CreatedDate],
	[ChangedBy],
	[ChangedDate],
	[Active],
	[Deleted ],
	[Editable],
	[Private]
 )
 VALUES
 (
	@CustomerID,
	@MeasureTypeID,
	@CreatedBy, 
	getdate(), 
	@ChangedBy, 
	@ChangedDate,
	@Active, 
	@Deleted, 
	@Editable, 
	@Private
 )
 
 Set @Err = @@Error 
 Set @MeasureTemplateID = scope_identity()
  
 IF @Log = 1   
 BEGIN   
  INSERT INTO [Log].[AuditLog] ( UserId, TableName, Type, Data, Created)   
  SELECT @cUserid,'mea.MeasureTemplate',0,  
  ( SELECT * FROM [mea].[MeasureTemplate]   
   WHERE  
   [MeasureTemplateID] = @MeasureTemplateID    FOR XML AUTO) as data,  
    getdate()   
  END  
  
 RETURN @Err  

END

GO
