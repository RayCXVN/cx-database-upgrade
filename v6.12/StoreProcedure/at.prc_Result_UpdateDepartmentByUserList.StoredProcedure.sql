SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[at].[prc_Result_UpdateDepartmentByUserList]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [at].[prc_Result_UpdateDepartmentByUserList] AS' 
END
GO
/*  
	Steve - Dec 8, 2016 - Job Fair project: merge Q and R DB, then change how to make a dynamic query
    Ray - 2017-02-09 - Remove cross database reference for Azure compatible
*/
ALTER PROCEDURE [at].[prc_Result_UpdateDepartmentByUserList] 
(
    @ListUserID     nvarchar(512),
    @OwnerID        int,
    @DepartmentID	int
)
AS
BEGIN
    DECLARE @ReportServer   nvarchar(84),
            @ReportDB       nvarchar(84),
            @cmd            nvarchar(512),
			@LinkedDB NVARCHAR(MAX) = ' '   
    CREATE TABLE #Result_temp (UserID int,res int Default (0))
    INSERT INTO #Result_temp (UserID) SELECT value FROM dbo.funcListToTableInt(@ListUserID,',')

    DECLARE cur CURSOR READ_ONLY FOR 
    SELECT DISTINCT ReportServer, ReportDB FROM at.Survey WHERE ActivityID IN (SELECT ActivityID from at.Activity WHERE OwnerID = @OwnerID)
    OPEN cur
    FETCH NEXT FROM cur INTO @ReportServer, @ReportDB
        WHILE @@FETCH_STATUS=0
        BEGIN
			IF (@ReportServer = @@servername AND @ReportDB = DB_NAME())
				SET @LinkedDB = ' '
			ELSE 
				SET @LinkedDB = '[' + @ReportServer + '].[' + @ReportDB + '].'
            SET @cmd ='UPDATE ' + @LinkedDB +'dbo.Result SET DepartmentID = '+convert(nvarchar(max), @DepartmentID)+' WHERE UserID IN(SELECT UserID FROM #Result_temp)'
            EXECUTE sp_executesql @cmd
        FETCH NEXT FROM cur INTO @ReportServer, @ReportDB
        END
    CLOSE cur
    DEALLOCATE cur
    DROP TABLE #Result_temp
END

GO
