SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[at].[prc_LT_XC_VT_upd]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [at].[prc_LT_XC_VT_upd] AS' 
END
GO

ALTER PROCEDURE [at].[prc_LT_XC_VT_upd]
(
	@LanguageID int,
	@XCVTID int,
	@Name nvarchar(256),
	@Description nvarchar(max),
	@TextAbove nvarchar(max),
	@TextBelow nvarchar(max),
	@cUserid int,
	@Log smallint = 1
)
AS
BEGIN
	SET NOCOUNT ON
	DECLARE @Err Int

	UPDATE [at].[LT_XC_VT]
	SET
		[LanguageID] = @LanguageID,
		[XCVTID] = @XCVTID,
		[Name] = @Name,
		[Description] = @Description,
		[TextAbove] = @TextAbove,
		[TextBelow] = @TextBelow
	WHERE
		[LanguageID] = @LanguageID AND
		[XCVTID] = @XCVTID

	IF @Log = 1 
	BEGIN 
		INSERT INTO [Log].[AuditLog] ( UserId, TableName, Type, Data, Created) 
		SELECT @cUserid,'LT_XC_VT',1,
		( SELECT * FROM [at].[LT_XC_VT] 
			WHERE
			[LanguageID] = @LanguageID AND
			[XCVTID] = @XCVTID			 FOR XML AUTO) as data,
			getdate()
	END

	Set @Err = @@Error

	RETURN @Err
END


GO
