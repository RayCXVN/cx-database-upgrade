SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[rep].[prc_Bubble_Status_ins]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [rep].[prc_Bubble_Status_ins] AS' 
END
GO

ALTER PROCEDURE [rep].[prc_Bubble_Status_ins]
(
	@BubbleID int,
	@SurveyID int,
	@LastProcessedDate datetime,
	@Reprocess SMALLINT=NULL,
	@cUserid int,
	@Log smallint = 1
)
AS
BEGIN
	SET NOCOUNT ON
	DECLARE @Err Int

	INSERT INTO [rep].[Bubble_Status]
	(
		[BubbleID],
		[SurveyID],
		[LastProcessedDate],
		[Reprocess]
	)
	VALUES
	(
		@BubbleID,
		@SurveyID,
		@LastProcessedDate,
		@Reprocess
	)

	Set @Err = @@Error

	IF @Log = 1 
	BEGIN 
		INSERT INTO [Log].[AuditLog] ( UserId, TableName, Type, Data, Created) 
		SELECT @cUserid,'Bubble_Status',0,
		( SELECT * FROM [rep].[Bubble_Status] 
			WHERE
			[BubbleID] = @BubbleID AND
			[SurveyID] = @SurveyID				 FOR XML AUTO) as data,
				getdate() 
	 END

	RETURN @Err
END


GO
