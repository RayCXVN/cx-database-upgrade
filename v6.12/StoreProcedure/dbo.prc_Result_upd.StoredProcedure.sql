SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[prc_Result_upd]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [dbo].[prc_Result_upd] AS' 
END
GO
/*  2016-10-10 Ray: Remove column Status from org.User, org.Department, RDB.dbo.Result
*/
ALTER PROCEDURE [dbo].[prc_Result_upd]
(
	@ResultID bigint,
	@StartDate datetime = null,
	@EndDate datetime = null,
	@RoleID int,
	@UserID int = null,
	@UserGroupID int = null,
	@SurveyID int,
	@BatchID int,
	@LanguageID int,
	@PageNo smallint,
	@DepartmentID int,
	@Anonymous smallint,
	@ShowBack bit,
	@Email varchar(100),
    @ResultKey nvarchar(128) = '',
    @LastUpdatedBy Int = 0,
    @StatusTypeID int = null,
	@ValidFrom  datetime2(7) =null,
	@ValidTo datetime2(7) =null,
	@DueDate datetime2(7) =null,
	@ParentResultID bigint =null,
	@ParentResultSurveyID int =  null,
    @CustomerID int = NULL,
	@Deleted datetime2=NULL,
	@EntityStatusID int = NULL,
	@EntityStatusReasonID int = NULL
)
As
BEGIN
	SET NOCOUNT ON
	DECLARE @Err Int
	DECLARE @OldUsername varchar(128)
	DECLARE @Count int	
	DECLARE @DBID INT;
	SET @DBID = DB_ID();

	DECLARE @DBNAME NVARCHAR(128);
    DECLARE @ActiveEntityStatusID INT = (SELECT EntityStatusID FROM EntityStatus WHERE CodeName='Active')

   SET @count = 0
	IF @ResultKey <> '' 
	BEGIN
       SELECT @Count = Count(*) FROM [Result] WHERE ResultKey = @ResultKey and Resultid <> @ResultID AND EntityStatusID = @ActiveEntityStatusID AND Deleted IS NULL
	END
	
     --SELECT @Count = Count(*) FROM [Result] WHERE UserName = @Username and Resultid <> @ResultID and @Userid <> userid

IF @Count = 0
	UPDATE [Result]
	Set
	[StartDate] = @StartDate,
	[EndDate] = @EndDate,
	[RoleID] = @RoleID,
	[UserID] = @UserID,
	[UserGroupID] = @UserGroupID,
	[SurveyID] = @SurveyID,
	[BatchID] = @BatchID,
	[LanguageID] = @LanguageID,
	[PageNo] = @PageNo,
	[DepartmentID] = @DepartmentID,
	[Anonymous] = @Anonymous,
	[ShowBack] = @ShowBack,
	[Email] = @Email,
	[ResultKey] = @ResultKey,
	[LastUpdated] = getdate(),
	[LastUpdatedBy] = @LastUpdatedBy,
	[StatusTypeID] = @StatusTypeID,
	[ValidFrom]=@ValidFrom,  
	[ValidTo]=@ValidTo, 
	[DueDate]=@DueDate, 
	[ParentResultID]=@ParentResultID,
	[ParentResultSurveyID]=@ParentResultSurveyID,
	[CustomerID] = @CustomerID ,
	[Deleted]	=	@Deleted ,
	[EntityStatusID] = @EntityStatusID ,
	[EntityStatusReasonID] = @EntityStatusReasonID
	WHERE
	[ResultID] = @ResultID AND EntityStatusID = @ActiveEntityStatusID AND Deleted IS NULL
ELSE
		RAISERROR
			(N'Resultkey taken',
			10, -- Severity.
			1, -- State.
			@DBID, -- First substitution argument.
			@DBNAME); -- Second substitution argument.

	Set @Err = @@Error

	RETURN @Err
End


GO
