SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[rep].[prc_UserActivityPageSetting]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [rep].[prc_UserActivityPageSetting] AS' 
END
GO
ALTER PROCEDURE [rep].[prc_UserActivityPageSetting]	
	@UserActivitySettingID int = null output,
	@UserID int,
	@ActivityID int,	
	@ChartTypeID int = 9,
	@ChartTypeIDAvg int = 7,		
	@ShowPage smallint = 1,	
	@PageIds nvarchar(MAX),
	@QuestionIds nvarchar(MAX),
	@Log smallint = 1,
	@QuestionItemIds nvarchar(max) = ''
AS 
BEGIN
	SET NOCOUNT ON;
	DECLARE @Err int
	
	IF (@UserID = 0 or @ActivityID = 0)
		RETURN -1
		
BEGIN TRANSACTION		
	SET @UserActivitySettingID = (SELECT TOP 1 UserActivitySettingID FROM [rep].[UserActivitySetting] WHERE UserID = @UserId AND ActivityID = @ActivityId)

	IF (@UserActivitySettingID is NULL)
		EXEC [rep].[prc_UserActivitySetting_ins] @UserActivitySettingID output, @UserID, @ActivityID, 1, 0, @ChartTypeID, @ChartTypeIDAvg, @ShowPage, 0, 0, @UserID, @Log
	ELSE
	BEGIN
		UPDATE [rep].[UserActivitySetting] 
		SET		[ChartTypeID] = @ChartTypeID
				,[ChartTypeIDAvg] = @ChartTypeIDAvg		
				,ShowPage = @ShowPage			
		WHERE [UserActivitySettingID] = @UserActivitySettingID 

		IF @Log = 1   
		BEGIN   
		  INSERT INTO [Log].[AuditLog] ( UserId, TableName, Type, Data, Created)   
		  SELECT @UserID,'UserActivitySetting',1,  
		  (SELECT * FROM [rep].[UserActivitySetting]  
		   WHERE  
		   [UserActivitySettingID] = @UserActivitySettingID     FOR XML AUTO) as data,  
			getdate()   
		END
	END

	IF @Log = 1   
	BEGIN   
	  INSERT INTO [Log].[AuditLog] ( UserId, TableName, Type, Data, Created)   
	  SELECT @UserID,'UAS_Page',2,  
	  (SELECT * FROM [rep].[UAS_Page]  
	   WHERE  
	   [UserActivitySettingID] = @UserActivitySettingID     FOR XML AUTO) as data,  
		getdate()   
	END

	--Delete old page list 		
	DELETE [rep].[UAS_Page] 
	WHERE [UserActivitySettingID] = @UserActivitySettingID

	SELECT * INTO #SplitList FROM [dbo].[StringToArray](@QuestionItemIds,';')

	select
	case 
	When (CHARINDEX('_', ParseVAlue) = 0) THEN 0
	when (CHARINDEX('_', ParseVAlue) > 0) then LEFT(ParseVAlue, CHARINDEX('_', ParseVAlue) - 1)
	ELSE 0 END AS QuestionID,

	case 
	When (CHARINDEX('_', ParseVAlue) = 0) THEN ParseVAlue
	when (CHARINDEX('_', ParseVAlue) > 0) then right(ParseVAlue, len(ParseVAlue) - CHARINDEX('_',ParseVAlue))
	ELSE 0 END AS ItemID

	INTO #PageQuestion
	FROM #SplitList

	IF (@PageIds != '') OR (@PageIds IS NOT NULL) OR (@QuestionItemIds != '') OR (@QuestionItemIds IS NOT NULL)
	BEGIN
		IF (@PageIds != '') OR (@PageIds IS NOT NULL)
		BEGIN
			INSERT INTO [rep].[UAS_Page] (UserActivitySettingID, PageID)
			SELECT @UserActivitySettingID, ParseValue FROM [dbo].[StringToArray](@PageIds,',')
			Set @Err = @@Error		
			IF @Err != 0 GOTO RollbackResult
		END

		IF (@QuestionItemIds != '') OR (@QuestionItemIds IS NOT NULL)
		BEGIN
			INSERT INTO [rep].[UAS_Page] (UserActivitySettingID, ItemID) 
			SELECT @UserActivitySettingID, ItemID FROM #PageQuestion WHERE QuestionID = 0 and ItemID > 0
			Set @Err = @@Error		
			IF @Err != 0 GOTO RollbackResult
		END		
		
		IF @Log = 1   
		BEGIN   
		  INSERT INTO [Log].[AuditLog] ( UserId, TableName, Type, Data, Created)   
		  SELECT @UserID,'UAS_Page',0,  
		  (SELECT * FROM [rep].[UAS_Page]  
		   WHERE  
		   [UserActivitySettingID] = @UserActivitySettingID     FOR XML AUTO) as data,  
			getdate()   
		END
	END
	
	IF @Log = 1   
	BEGIN   
	  INSERT INTO [Log].[AuditLog] ( UserId, TableName, Type, Data, Created)   
	  SELECT @UserID,'UAS_Question',2,  
	  (SELECT * FROM [rep].[UAS_Question]  
	   WHERE  
	   [UserActivitySettingID] = @UserActivitySettingID     FOR XML AUTO) as data,  
		getdate()   
	END
	
	-- Delete old question list
	DELETE [rep].[UAS_Question] 
	WHERE [UserActivitySettingID] = @UserActivitySettingID

	IF (@QuestionIds != '') OR (@QuestionIds IS NOT NULL) OR (@QuestionItemIds != '') OR (@QuestionItemIds IS NOT NULL)
	BEGIN
		IF (@QuestionIds != '') OR (@QuestionIds IS NOT NULL)
		BEGIN
			INSERT INTO [rep].[UAS_Question] (UserActivitySettingID, QuestionID)
			SELECT @UserActivitySettingID, ParseValue FROM [dbo].[StringToArray](@QuestionIds,',')
			Set @Err = @@Error
		
			IF @Err != 0 GOTO RollbackResult
		END

		IF (@QuestionItemIds != '') OR (@QuestionItemIds IS NOT NULL)
		BEGIN
			INSERT INTO [rep].[UAS_Question] (UserActivitySettingID, QuestionID, ItemID)
			SELECT @UserActivitySettingID,QuestionID,ItemID  FROM #PageQuestion WHERE QuestionID > 0 AND ItemID > 0
			Set @Err = @@Error
		
			IF @Err != 0 GOTO RollbackResult
		END
		
		IF @Log = 1   
		BEGIN   
		  INSERT INTO [Log].[AuditLog] ( UserId, TableName, Type, Data, Created)   
		  SELECT @UserID,'UAS_Question',0,  
		  (SELECT * FROM [rep].[UAS_Question]  
		   WHERE  
		   [UserActivitySettingID] = @UserActivitySettingID     FOR XML AUTO) as data,  
			getdate()   
		END

		DROP TABLE #SplitList;
		DROP TABLE #PageQuestion;
	END

CommitResult:
	COMMIT TRANSACTION
	RETURN @Err

RollbackResult:	
	ROLLBACK TRANSACTION
	RETURN @Err
END

GO
