SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[list].[prc_LT_ItemList_upd]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [list].[prc_LT_ItemList_upd] AS' 
END
GO

-- =============================================
ALTER PROCEDURE [list].[prc_LT_ItemList_upd]
	@LanguageID int,
	@ItemListID int,
	@Name nvarchar(256),
	@Description nvarchar(max),
	@RowTooltipText nvarchar(256),
	@EmptyListText nvarchar(max),
    @cUserid int,
    @Log smallint = 1
AS
BEGIN
	SET NOCOUNT ON;
	DECLARE @Err Int
	
    UPDATE [list].[LT_ItemList]
    SET 
		[Name] = @Name,
        [Description] = @Description,
        [RowTooltipText] = @RowTooltipText,
        [EmptyListText] = @EmptyListText
     WHERE 
		[ItemListID] = @ItemListID AND
		[LanguageID] = @LanguageID
		
	IF @Log = 1 
	BEGIN 
		INSERT INTO [Log].[AuditLog] ( UserId, TableName, Type, Data, Created) 
		SELECT @cUserid,'LT_ItemList',1,
		( SELECT * FROM [list].[LT_ItemList] 
			WHERE
				[ItemListID] = @ItemListID AND
				[LanguageID] = @LanguageID			 FOR XML AUTO) as data,
			getdate()
	END

	Set @Err = @@Error

	RETURN @Err	
END


GO
