SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[at].[prc_Survey_upd]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [at].[prc_Survey_upd] AS' 
END
GO
ALTER PROCEDURE [at].[prc_Survey_upd]
(
	@SurveyID int,
	@ActivityID int,
	@HierarchyID INT=NULL,
	@StartDate smalldatetime,
	@EndDate smalldatetime,
	@Anonymous smallint,
	@Status smallint,
	@ShowBack bit,
	@LanguageID int,
	@ButtonPlacement smallint,
	@UsePageNo bit,
	@LinkURL varchar(512),
	@FinishURL varchar(512),
	@CreateResult bit,
	@ReportDB varchar(64),
	@ReportServer varchar(64),
	@StyleSheet varchar(128),
	@Type smallint,
	@ReProcessOLAP smallint,
	@No smallint,
	@OLAPServer nvarchar(64),
	@OLAPDB nvarchar(64),
	@PeriodID int = NULL,
	@DeleteResultOnUserDelete bit = 0,
	@ProcessCategorys smallint = 0,
	@cUserid int,
	@Log smallint = 1,
	@ExtId nvarchar(256),
	@Tag nvarchar(256)
)
AS
BEGIN
	SET NOCOUNT ON
	DECLARE @Err Int
	
	IF @HierarchyID = 0
	BEGIN	
	  SET @HierarchyID = NULL
	END

	UPDATE [at].[Survey]
	SET
		[ActivityID] = @ActivityID,
		[HierarchyID] = @HierarchyID,
		[StartDate] = @StartDate,
		[EndDate] = @EndDate,
		[Anonymous] = @Anonymous,
		[Status] = @Status,
		[ShowBack] = @ShowBack,
		[LanguageID] = @LanguageID,
		[ButtonPlacement] = @ButtonPlacement,
		[UsePageNo] = @UsePageNo,
		[LinkURL] = @LinkURL,
		[FinishURL] = @FinishURL,
		[CreateResult] = @CreateResult,
		[ReportDB] = @ReportDB,
		[ReportServer] = @ReportServer,
		[StyleSheet] = @StyleSheet,
		[Type] = @Type,
		[ReProcessOLAP] = @ReProcessOLAP,
		[No] = @No,
		[OLAPServer] = @OLAPServer,
		[OLAPDB] = @OLAPDB,
		[PeriodID] = @PeriodID,
		[DeleteResultOnUserDelete] = @DeleteResultOnUserDelete,
		[ProcessCategorys] = @ProcessCategorys,
		[ExtId] = @ExtId,
		[Tag] = @Tag
	WHERE
		[SurveyID] = @SurveyID

	IF @Log = 1 
	BEGIN 
		INSERT INTO [Log].[AuditLog] ( UserId, TableName, Type, Data, Created) 
		SELECT @cUserid,'Survey',1,
		( SELECT * FROM [at].[Survey] 
			WHERE
			[SurveyID] = @SurveyID			 FOR XML AUTO) as data,
			getdate()
	END

	Set @Err = @@Error

	RETURN @Err
END

GO
