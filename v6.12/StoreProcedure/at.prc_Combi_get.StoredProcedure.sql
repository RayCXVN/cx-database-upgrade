SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[at].[prc_Combi_get]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [at].[prc_Combi_get] AS' 
END
GO

ALTER PROCEDURE [at].[prc_Combi_get]
(
	@ChoiceID int
)
AS
BEGIN
	SET NOCOUNT ON
	DECLARE @Err Int

	SELECT
	[CombiID],
	[ChoiceID],
	[QuestionID],
	ISNULL([AlternativeID], 0) AS 'AlternativeID',
	[ValueFormula]
	FROM [at].[Combi]
	WHERE
	[ChoiceID] = @ChoiceID

	Set @Err = @@Error

	RETURN @Err
END


GO
