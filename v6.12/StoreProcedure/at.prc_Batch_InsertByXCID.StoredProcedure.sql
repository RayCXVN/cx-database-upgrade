SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[at].[prc_Batch_InsertByXCID]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [at].[prc_Batch_InsertByXCID] AS' 
END
GO
ALTER PROCEDURE [at].[prc_Batch_InsertByXCID]
(   @XCID               int,
    @DepartmentIDList   varchar(max),
    @ActivePeriodID     int,
    @NumberOfPeriods    int = 0,
    @PROP_ReadOnly      int
) AS
BEGIN
    DECLARE @ActivePeriodEndDate datetime, @TT_UserType int, @TT_DepartmentType int, @RT_Standard int = 1
    DECLARE @ActiveEntityStatusID INT = (SELECT EntityStatusID FROM EntityStatus WHERE CodeName='Active')
    SELECT @ActivePeriodEndDate = [p].[Enddate] FROM [at].[Period] p WHERE [p].[PeriodID] = @ActivePeriodID
    SELECT @TT_UserType       = [TableTypeID] FROM [TableType] WHERE [Name] = 'UserType'
    SELECT @TT_DepartmentType = [TableTypeID] FROM [TableType] WHERE [Name] = 'DepartmentType'

    DECLARE @DepartmentTable TABLE ([DepartmentID] int)
    INSERT INTO @DepartmentTable ([DepartmentID]) SELECT DISTINCT [Value] FROM dbo.funcListToTableInt(@DepartmentIDList, ',')

    DECLARE @PeriodTable TABLE ([PeriodID] int, [ReadOnlyActivity] nvarchar(16))
    IF @NumberOfPeriods <= 0 SET @NumberOfPeriods = 1
    INSERT INTO @PeriodTable ([PeriodID], [ReadOnlyActivity]) SELECT TOP(@NumberOfPeriods - 1) p.[PeriodID], 'false'
                                                              FROM [at].[Period] p
                                                              WHERE p.[Enddate] <= @ActivePeriodEndDate AND p.[PeriodID] != @ActivePeriodID
                                                              ORDER BY p.[Enddate] DESC
    INSERT INTO @PeriodTable ([PeriodID], [ReadOnlyActivity]) VALUES (@ActivePeriodID, 'true')

    DECLARE @Activity TABLE ([ActivityID] int, [ReadOnly] nvarchar(16))
    INSERT INTO @Activity ([ActivityID], [ReadOnly])
    SELECT xca.[ActivityID], ISNULL(ltrim(rtrim(pv.[Value])), 'false') [ReadOnly]
    FROM [at].[XC_A] xca LEFT JOIN [prop].[PropValue] pv ON pv.[PropertyID] = @PROP_ReadOnly AND pv.[ItemID] = cast(xca.[ActivityID] as nvarchar(32))
    WHERE xca.[XCID] = @XCID

    INSERT INTO [at].[Batch] ([SurveyID], [DepartmentID], [Name], [No], [StartDate], [EndDate], [Status], [Created])
    SELECT s.[SurveyID], d.[DepartmentID], d.[Name], 0, s.[StartDate], s.[EndDate], 2, GETDATE()
    FROM [at].[Survey] s JOIN @Activity a ON s.[ActivityID] = a.[ActivityID] AND s.[Status] = 2 AND s.[ExtId] != 'Global'
    JOIN @PeriodTable p ON s.[PeriodID] = p.[PeriodID] AND ((a.[ReadOnly] = p.[ReadOnlyActivity] AND a.[ReadOnly] = 'true')
                                                            OR a.[ReadOnly] = 'false')
    CROSS JOIN @DepartmentTable dl
    JOIN [org].[Department] d ON dl.[DepartmentID] = d.[DepartmentID] AND d.EntityStatusID = @ActiveEntityStatusID AND d.Deleted IS NULL
    WHERE EXISTS (SELECT 1 FROM [at].[RoleMapping] rm JOIN [ObjectMapping] om ON s.[ActivityID] = rm.[ActivityID] AND om.[FromTableTypeID] = @TT_UserType AND rm.[UserTypeID] = om.[FromID]
                                                       AND om.[RelationTypeID] = @RT_Standard AND om.[ToTableTypeID] = @TT_DepartmentType
                           JOIN [org].[DT_D] dtd ON dtd.[DepartmentID] = d.[DepartmentID] AND dtd.[DepartmentTypeID] = om.[ToID])
      AND NOT EXISTS (SELECT 1 FROM [at].[Batch] b WHERE b.[SurveyID] = s.[SurveyID] AND b.[Status] = 2 AND b.[DepartmentID] = d.[DepartmentID])
END

GO
