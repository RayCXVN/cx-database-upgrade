SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[at].[prc_B_P_ins]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [at].[prc_B_P_ins] AS' 
END
GO
ALTER PROCEDURE [at].[prc_B_P_ins]
(
	@BulkID int,
	@PageID int
)
AS
BEGIN
	SET NOCOUNT ON
	DECLARE @Err Int

	INSERT INTO [at].[B_P]
	(
		[BulkID],
		[PageID]
	)
	VALUES
	(
		@BulkID,
		@PageID
	)

	Set @Err = @@Error


	RETURN @Err
END



GO
