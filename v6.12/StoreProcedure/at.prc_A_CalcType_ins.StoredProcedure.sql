SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[at].[prc_A_CalcType_ins]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [at].[prc_A_CalcType_ins] AS' 
END
GO


ALTER PROCEDURE [at].[prc_A_CalcType_ins]
(
	@ACALCTYPEID int = null output,
	@ReportCalcTypeID int,
	@ActivityID int,
	@No smallint,
	@DefaultSelected smallint = 0,
	@Format nvarchar(32) = '',
	@cUserid int,
	@Log smallint = 1
)
AS
BEGIN
	SET NOCOUNT ON
	DECLARE @Err Int

	INSERT INTO [at].[A_CalcType]
	(
		[ReportCalcTypeID],
		[ActivityID],
		[No],
		[DefaultSelected],
		[Format]
	)
	VALUES
	(
		@ReportCalcTypeID,
		@ActivityID,
		@No,
		@DefaultSelected,
		@Format
	)

	Set @Err = @@Error
	Set @ACALCTYPEID = scope_identity()

	IF @Log = 1 
	BEGIN 
		INSERT INTO [Log].[AuditLog] ( UserId, TableName, Type, Data, Created) 
		SELECT @cUserid,'A_CalcType',0,
		( SELECT * FROM [at].[A_CalcType] 
			WHERE
			[ACALCTYPEID] = @ACALCTYPEID				 FOR XML AUTO) as data,
				getdate() 
	 END

	RETURN @Err
END

/****** Object:  StoredProcedure [at].[prc_A_CalcType_upd]    Script Date: 11/15/2010 07:32:14 ******/
SET ANSI_NULLS ON


GO
