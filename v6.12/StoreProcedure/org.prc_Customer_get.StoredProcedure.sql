SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[org].[prc_Customer_get]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [org].[prc_Customer_get] AS' 
END
GO
ALTER PROCEDURE [org].[prc_Customer_get]  
(  
 @OwnerID int  
)  
AS  
BEGIN  
 SET NOCOUNT ON  
 DECLARE @Err Int  
  
 SELECT  
 [CustomerID],  
 [OwnerID],  
 [LanguageID],
 [Name],  
 [Status],  
 [Created],  
 [ExtID],
 [HasUserIntegration],
 [RootMenuID],
 [CodeName],
 [Logo],
 [CssVariables],
 [Favicon]
 FROM [org].[Customer]  
 WHERE  
 [OwnerID] = @OwnerID  
  
 Set @Err = @@Error  
  
 RETURN @Err  
END  

GO
