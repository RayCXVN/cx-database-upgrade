SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[at].[prc_SM_BU_upd]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [at].[prc_SM_BU_upd] AS' 
END
GO

ALTER PROCEDURE [at].[prc_SM_BU_upd]
(
	@SentMailID int,
	@BatchID int,
	@ResultID bigint,
	@cUserid int,
	@Log smallint = 1
)
AS
BEGIN
	SET NOCOUNT ON
	DECLARE @Err Int

	UPDATE [at].[SM_BU]
	SET
		[SentMailID] = @SentMailID,
		[BatchID] = @BatchID,
		[ResultID] = @ResultID
	WHERE
		[SentMailID] = @SentMailID AND
		[BatchID] = @BatchID AND
		[ResultID] = @ResultID

	IF @Log = 1 
	BEGIN 
		INSERT INTO [Log].[AuditLog] ( UserId, TableName, Type, Data, Created) 
		SELECT @cUserid,'SM_BU',1,
		( SELECT * FROM [at].[SM_BU] 
			WHERE
			[SentMailID] = @SentMailID AND
			[BatchID] = @BatchID AND
			[ResultID] = @ResultID			 FOR XML AUTO) as data,
			getdate()
	END

	Set @Err = @@Error

	RETURN @Err
END


GO
