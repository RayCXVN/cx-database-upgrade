SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[sec].[prc_AccessRule_upd]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [sec].[prc_AccessRule_upd] AS' 
END
GO
ALTER PROCEDURE [sec].[prc_AccessRule_upd]
(
	@AccessRuleID int,
	@No int,
    @Inheritance int,
    @ItemID int,
    @TableTypeID smallint, 
    @TableTypeParameters  nvarchar(512),
    @Access int,
    @Type  int,
    @AccessRuleName NVARCHAR(512) ='',
	@cUserid int,
	@Log smallint = 1
)
AS
BEGIN
	SET NOCOUNT ON
	DECLARE @Err Int
	UPDATE  [sec].[AccessRule]
	SET     [No] = @No
           ,[Inheritance] =@Inheritance
           ,[ItemID] = @ItemID
           ,[TableTypeID] = @TableTypeID
           ,[TableTypeParameters] = @TableTypeParameters
           ,[Access] = @Access
           ,[Type] =  @Type
           ,[AccessRuleName]=@AccessRuleName
     WHERE 
	 [AccessRuleID] = @AccessRuleID 
	IF @Log = 1 
	BEGIN 
		INSERT INTO [Log].[AuditLog] ( UserId, TableName, Type, Data, Created) 
		SELECT @cUserid,'AccessRule',1,
		( SELECT * FROM [sec].[AccessRule] 
			WHERE
        	[AccessRuleID] = @AccessRuleID				 FOR XML AUTO) as data,
				getdate() 
	 END
	RETURN @Err
END

GO
