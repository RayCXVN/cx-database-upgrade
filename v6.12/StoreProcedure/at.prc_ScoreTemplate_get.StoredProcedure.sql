SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[at].[prc_ScoreTemplate_get]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [at].[prc_ScoreTemplate_get] AS' 
END
GO

ALTER PROCEDURE [at].[prc_ScoreTemplate_get]
(
	@ActivityID int
)
AS
BEGIN
	SET NOCOUNT ON
	DECLARE @Err Int

	SELECT
	[ScoreTemplateID],
	ISNULL([ActivityID], 0) AS 'ActivityID',
	ISNULL([XCID], 0) AS 'XCID',
	[Type],
	[No],
	[OwnerColorID],
	[DefaultState],
	[Created]
	FROM [at].[ScoreTemplate]
	WHERE
	[ActivityID] = @ActivityID
	ORDER BY [No]

	Set @Err = @@Error

	RETURN @Err
END


GO
