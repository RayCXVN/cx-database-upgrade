SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[prc_ObjectMapping_get]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [dbo].[prc_ObjectMapping_get] AS' 
END
GO


ALTER PROCEDURE [dbo].[prc_ObjectMapping_get]
(
	@OwnerID int
)
AS
BEGIN
	SET NOCOUNT ON
	DECLARE @Err Int

	SELECT
	[OMID],
	[OwnerID],
	[FromTableTypeID],
	[FromID],
	[ToTableTypeID],
	[ToID],
	[RelationTypeID]
	FROM [dbo].[ObjectMapping]
	WHERE
	[OwnerID] = @OwnerID

	Set @Err = @@Error

	RETURN @Err
END



GO
