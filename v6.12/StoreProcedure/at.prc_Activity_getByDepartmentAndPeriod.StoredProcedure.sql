SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[at].[prc_Activity_getByDepartmentAndPeriod]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [at].[prc_Activity_getByDepartmentAndPeriod] AS' 
END
GO
/*
    Steve - Des 12 2016 - Create store procedure to improve performance
*/
ALTER PROCEDURE [at].[prc_Activity_getByDepartmentAndPeriod]
(
    @ActivityIDList         NVARCHAR(MAX) = N'',
    @DepartmentID           INT,
    @PeriodID               INT
)
AS
BEGIN
	DECLARE @TableActivityID TABLE(ActivityID INT)

	INSERT INTO @TableActivityID(ActivityID) SELECT Value FROM dbo.funcListToTableInt(@ActivityIDList, ',')
	
	SELECT a.ActivityID FROM at.Activity a JOIN at.Survey s ON a.ActivityID = s.ActivityID AND a.ActivityID IN (SELECT ActivityID FROM @TableActivityID) AND s.PeriodID = @PeriodID
	JOIN at.Batch b ON s.SurveyID = b.SurveyID AND b.DepartmentID = @DepartmentID
	WHERE s.[Status] >= 0 AND b.[Status] >= 0 AND s.StartDate <= GETDATE() AND s.EndDate >= GETDATE() AND b.StartDate <= GETDATE() AND b.EndDate >= GETDATE()
END


GO
