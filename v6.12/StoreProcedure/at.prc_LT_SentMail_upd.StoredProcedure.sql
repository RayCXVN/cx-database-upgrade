SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[at].[prc_LT_SentMail_upd]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [at].[prc_LT_SentMail_upd] AS' 
END
GO

ALTER PROCEDURE [at].[prc_LT_SentMail_upd]
(
	@LanguageID int,
	@SentMailID int,
	@Name nvarchar(256),
	@Subject nvarchar(256),
	@Body nvarchar(max),
	@cUserid int,
	@Log smallint = 1
)
AS
BEGIN
	SET NOCOUNT ON
	DECLARE @Err Int

	UPDATE [at].[LT_SentMail]
	SET
		[LanguageID] = @LanguageID,
		[SentMailID] = @SentMailID,
		[Name] = @Name,
		[Subject] = @Subject,
		[Body] = @Body
	WHERE
		[LanguageID] = @LanguageID AND
		[SentMailID] = @SentMailID

	IF @Log = 1 
	BEGIN 
		INSERT INTO [Log].[AuditLog] ( UserId, TableName, Type, Data, Created) 
		SELECT @cUserid,'LT_SentMail',1,
		( SELECT * FROM [at].[LT_SentMail] 
			WHERE
			[LanguageID] = @LanguageID AND
			[SentMailID] = @SentMailID			 FOR XML AUTO) as data,
			getdate()
	END

	Set @Err = @@Error

	RETURN @Err
END


GO
