SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[prc_ResultCountByDepartment]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [dbo].[prc_ResultCountByDepartment] AS' 
END
GO
-- EXEC [dbo].[prc_ResultCountByDepartment] 34, 14, 9, 15
ALTER PROCEDURE [dbo].[prc_ResultCountByDepartment]
@DepartmentID		int,
@usertype			int,
@usertypedefault	int,
@OwnerID			int
as
Declare @Olapdb varchar(128)
select DISTINCT  @Olapdb = OLAPDB  from at.Survey where EXISTS (select Activity.ActivityID from at.Activity where OwnerID =15)

DECLARE @HDID				int,
		@depratmentID_temp	int,
		@usetype			int ,
		@HDID_temp			int,
		@result				int=0,
		@statement			nvarchar(max),
		@Parameters			nvarchar(max) = N'@Olapdb varchar(max),@HDID_temp int,@result_ int output'
DECLARE	@result_tb table (Department int,Usetype int, Name varchar(128), resultcount int)
SELECT @HDID= HDID from org.H_D where DepartmentID = @DepartmentID
INSERT INTO @result_tb (Department,Usetype,Name )
SELECT hd.DepartmentID ,0 as Usertype,d.Name from org.H_D hd inner JOIN org.Department d on hd.DepartmentID =d.DepartmentID
WHERE  hd.ParentID =@HDID

 
IF not exists(SELECT u.UserID,ut.UserTypeID,u.UserName from org.[User] u inner JOIN org.UT_U ut ON u.UserID= ut.UserID WHERE u.DepartmentID = @DepartmentID and ut.UserTypeID = @usertype)
	Begin

	INSERT INTO @result_tb (Department,Usetype,Name ) 
	SELECT top 1 u.UserID,@usertypedefault,u.UserName from org.[User] u inner JOIN org.UT_U ut ON u.UserID= ut.UserID WHERE u.DepartmentID = @DepartmentID
	End
else
	BEGIN
		INSERT INTO @result_tb (Department,Usetype,Name ) 
		SELECT u.UserID,ut.UserTypeID,u.UserName from org.[User] u inner JOIN org.UT_U ut ON u.UserID= ut.UserID WHERE u.DepartmentID = @DepartmentID and ut.UserTypeID = @usertype
	END

DECLARE cur cursor for
SELECT Department,Usetype from @result_tb
open cur 
FETCH NEXT FROM cur  INTO @depratmentID_temp, @usetype
WHILE @@FETCH_STATUS = 0
BEGIN 
	IF (@usetype=0)
		Begin
			
			SELECT @HDID_temp= HDID from org.H_D where DepartmentID = @depratmentID_temp
			
			set @statement=
			N'	
				SELECT @result_ = count (*)from '+@Olapdb+'.dbo.Result
				where DepartmentID in
				(
				SELECT hd.DepartmentID  from org.H_D hd
				where  hd.Path LIKE ''%\'+CAST(@HDID_temp as varchar(128))+'\%'' 
				)
			'
			
			exec sp_executesql @statement, @Parameters,@Olapdb,@HDID_temp,@result_=@result output
				
			UPdate @result_tb SET resultcount =@result where Department= @depratmentID_temp
			
		End
	else
		begin
		set @statement=N'
			SELECT @result_= count (*) from '+@Olapdb+'.dbo.Result where UserID='+convert(varchar,@depratmentID_temp)+'
			'
			
			exec sp_executesql @statement,N'@Olapdb varchar(max),@depratmentID_temp int, @result_ int output', @Olapdb,@depratmentID_temp, @result_=@result output
			UPdate @result_tb SET resultcount =@result where Department= @depratmentID_temp
			
		end
	
	
  FETCH NEXT From cur INTO @depratmentID_temp, @usetype
END
CLOSE cur
DEALLOCATE cur
SELECT * FROM @result_tb
--

GO
