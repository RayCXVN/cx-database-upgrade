SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[prc_Department_get_single]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [dbo].[prc_Department_get_single] AS' 
END
GO
/*  
    2017-01-20 Johnny: Remove column Status from org.Department, EntityStatusReason
*/
ALTER PROCEDURE [dbo].[prc_Department_get_single]
(
   @DepartmentID int
)
As
BEGIN
 DECLARE @ActiveEntityStatusID INT = (SELECT EntityStatusID FROM EntityStatus WHERE CodeName='Active')

 SELECT DepartmentID, LanguageID, Name, Description, OrgNo, ISNULL(OwnerID, 0) OwnerID, Created, ExtID, Tag, Adress, PostalCode, City, Locked, EntityStatusID
 FROM department 
 WHERE departmentid = @DepartmentID AND EntityStatusID = @ActiveEntityStatusID
END


GO
