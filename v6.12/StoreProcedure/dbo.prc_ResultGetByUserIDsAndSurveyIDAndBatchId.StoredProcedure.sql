SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[prc_ResultGetByUserIDsAndSurveyIDAndBatchId]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [dbo].[prc_ResultGetByUserIDsAndSurveyIDAndBatchId] AS' 
END
GO
/*
    Sarah - Oct 31, 2016 - Hot fix: Get EntityStatus value on QDB
	Johnny - Des 07 2016 - Update store procedure for dynamic SQL 
*/
ALTER PROCEDURE [dbo].[prc_ResultGetByUserIDsAndSurveyIDAndBatchId]
	@UserIds NVARCHAR(MAX), 
	@SurveyId INT,
	@BatchId INT
AS
BEGIN
	DECLARE @ReportServer NVARCHAR(64),@ReportDB NVARCHAR(64), @ActiveEntityStatusID INT = 0
    SELECT @ActiveEntityStatusID = EntityStatusID FROM EntityStatus WHERE CodeName='Active'

	SELECT @ReportServer=ReportServer,@ReportDB=ReportDB FROM at.Survey
	WHERE SurveyID=@SurveyId
	DECLARE @sqlCommand NVARCHAR(max),@sqlParameters NVARCHAR(Max)
	DECLARE @LinkedDB NVARCHAR(MAX)
	IF @ReportDB=DB_NAME() AND @ReportServer=@@servername
		BEGIN
		SET @LinkedDB=' '
		END
	ELSE
		BEGIN
		SET @LinkedDB= '['+ @ReportServer + '].['+ @ReportDB + '].'
		END

	SET @sqlCommand='SELECT r.UserID,r.ResultID,r.ResultKey'+ 
		',r.LastUpdated,(u.FirstName + '' '' + u.LastName) as CompleteName,r.StatusTypeID FROM '+ @LinkedDB +'dbo.Result r '+
	    'LEFT JOIN org.[User] u ON r.LastUpdatedBy = u.UserID '+
	    'WHERE r.SurveyID=@p_SurveyID AND r.BatchID = @p_BatchID AND r.UserID IN ('+@UserIds+') AND r.EntityStatusID = '+CONVERT(NVARCHAR(12), @ActiveEntityStatusID)+' AND r.Deleted IS NULL '
	SET @sqlParameters = N'@p_SurveyID int,@p_BatchID int'
	EXECUTE sp_executesql @sqlCommand, @sqlParameters, @p_SurveyID = @SurveyId, @p_BatchID = @BatchId
END

GO
