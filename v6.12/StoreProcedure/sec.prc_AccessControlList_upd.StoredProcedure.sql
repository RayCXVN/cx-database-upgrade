SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[sec].[prc_AccessControlList_upd]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [sec].[prc_AccessControlList_upd] AS' 
END
GO
ALTER PROCEDURE [sec].[prc_AccessControlList_upd]
(
	@AccessControlListID int,
	@GroupAccessRuleID int,
    @AccessGroupID int,
    @No int,
    @RuleType nvarchar(512),
    @cUserid int,
	@Log smallint = 1
)
AS
BEGIN
SET NOCOUNT ON
DECLARE @Err int
UPDATE [sec].[AccessControlList]
SET [GroupAccessRuleID] = @GroupAccessRuleID,
    [AccessGroupID] = @AccessGroupID,
    [No] = @No,
    [RuleType] = @RuleType
WHERE [AccessControlListID] = @AccessControlListID
IF @Log = 1 
BEGIN
INSERT INTO [Log].[AuditLog] (UserId, TableName, Type, Data, Created)
    SELECT
        @cUserid,
        'AccessControlList',
        1,
        (SELECT
            *
        FROM [sec].[AccessControlList]
        WHERE [AccessControlListID] = @AccessControlListID
        FOR xml AUTO)
        AS data,
        GETDATE()

END
RETURN @Err
END

GO
