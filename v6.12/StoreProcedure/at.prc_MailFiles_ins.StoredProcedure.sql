SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[at].[prc_MailFiles_ins]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [at].[prc_MailFiles_ins] AS' 
END
GO

ALTER PROCEDURE [at].[prc_MailFiles_ins]
(
	@MailFileID int = null output,
	@MIMEType nvarchar(64),
	@Data varbinary(MAX),
	@Description nvarchar(max),
	@Size bigint,
	@Filename nvarchar(128)='',
	@MailID INT=NULL,
	@cUserid int,
	@Log smallint = 1
)
AS
BEGIN
	SET NOCOUNT ON
	DECLARE @Err Int

	IF @MIMEType IS NULL 
		SET @MIMEType = ''

	INSERT INTO [at].[MailFiles]
	(
		[MIMEType],
		[Data],
		[Description],
		[Size],
		[Filename],
		[MailID]
	)
	VALUES
	(
		@MIMEType,
		@Data,
		@Description,
		@Size,
		@Filename,
		@MailID
	)

	Set @Err = @@Error
	Set @MailFileID = scope_identity()

	IF @Log = 1 
	BEGIN 
		INSERT INTO [Log].[AuditLog] ( UserId, TableName, Type, Data, Created) 
		SELECT @cUserid,'MailFiles',0,
		( SELECT * FROM [at].[MailFiles] 
			WHERE
			[MailFileID] = @MailFileID				 FOR XML AUTO) as data,
				getdate() 
	 END

	RETURN @Err
END


GO
