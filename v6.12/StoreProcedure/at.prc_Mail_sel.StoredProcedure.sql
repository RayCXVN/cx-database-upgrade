SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[at].[prc_Mail_sel]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [at].[prc_Mail_sel] AS' 
END
GO


ALTER PROCEDURE [at].[prc_Mail_sel]
(
	@MailID int
)
AS
BEGIN
	SET NOCOUNT ON

	SELECT
	[SurveyID],
	[Type],
	[From],
	[Bcc],
	[StatusMail],
	[Created]
	FROM [at].[Mail]
	WHERE
	[MailID] = @MailID

END


GO
