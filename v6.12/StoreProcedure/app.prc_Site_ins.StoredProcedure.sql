SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[app].[prc_Site_ins]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [app].[prc_Site_ins] AS' 
END
GO
ALTER PROCEDURE [app].[prc_Site_ins]  
 @SiteID int = null output,  
 @OwnerID int,  
 @CodeName nvarchar(256),  
 @UseNativeLogon bit,  
 @ThemeID int,  
 @IsClosed bit,  
 @DefaultMenuID int,  
 @DefaultHierarchyID int,  
 @UseMultipleMenus bit,  
 @DefaultChartTemplate nvarchar(256),  
 @UseCustomer bit,  
 @UsePasswordRecovery bit,  
 @MaintenanceMode bit,  
 @UseRememberMe bit,
 @cUserid int,  
 @Log smallint = 1,
 @UseReturnURL bit =0,
 @UseSMS bit =0,
 @UseMemoryCache			bit	,			
 @UseDistributedCache		bit	,			
 @DistributedCacheName      nvarchar(64),
 @CacheExpiredAfter		    int,
 @MailHeaderInfoXCompany nvarchar(64)='',
 @MailHeaderInfoMessageID nvarchar(64) =''	,
 @LanguageIDWhenNotAuthenticated INT =0,
 @LogoutURL			nvarchar(max) ='',
 @TimeOutLogoutURL nvarchar(max) ='',
 @Logo nvarchar(max) = '', 
 @CssVariables nvarchar(max) = '',
 @Favicon nvarchar(max) = ''
AS  
BEGIN  
 SET NOCOUNT ON;  
 DECLARE @Err Int  

    INSERT INTO [app].[Site]  
           ([OwnerID]  
           ,[CodeName]  
           ,[UseNativeLogon]  
           ,[ThemeID]  
           ,[IsClosed]  
           ,[DefaultMenuID]  
           ,[DefaultHierarchyID]  
           ,[UseMultipleMenus]  
           ,[DefaultChartTemplate]  
           ,[UseCustomer]  
           ,[UsePasswordRecovery]
           ,[MaintenanceMode]
           ,[UseRememberMe]
           ,[UseReturnURL]
           ,[UseSMS]
           ,UseMemoryCache			
		   ,UseDistributedCache	
		   ,DistributedCacheName  
		   ,CacheExpiredAfter
		   ,MailHeaderInfoXCompany 
		   ,MailHeaderInfoMessageID
		   ,LanguageIDWhenNotAuthenticated
			,LogoutURL		
			,TimeOutLogoutURL
			,Logo
			,CssVariables
			,Favicon
           )  
     VALUES  
           (@OwnerID  
           ,@CodeName  
           ,@UseNativeLogon  
           ,@ThemeID  
           ,@IsClosed  
           ,@DefaultMenuID  
           ,@DefaultHierarchyID  
           ,@UseMultipleMenus  
           ,@DefaultChartTemplate  
           ,@UseCustomer  
           ,@UsePasswordRecovery
           ,@MaintenanceMode
           ,@UseRememberMe
           ,@UseReturnURL
           ,@UseSMS
           ,@UseMemoryCache			
		   ,@UseDistributedCache	
		   ,@DistributedCacheName  
		   ,@CacheExpiredAfter 
		   ,@MailHeaderInfoXCompany 
		   ,@MailHeaderInfoMessageID
		   ,@LanguageIDWhenNotAuthenticated
		   ,@LogoutURL		
		   ,@TimeOutLogoutURL
		   ,@Logo
		   ,@CssVariables
           ,@Favicon)  

    Set @Err = @@Error  
    Set @SiteID = scope_identity()  
 IF @Log = 1   
 BEGIN   
  INSERT INTO [Log].[AuditLog] ( UserId, TableName, Type, Data, Created)   
  SELECT @cUserid,'Site',0,  
  ( SELECT * FROM [app].[Site]  
   WHERE  
   [SiteID] = @SiteID     FOR XML AUTO) as data,  
    getdate()   
 END  

 RETURN @Err         
END 

GO
