SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[mea].[prc_GetAllMeasure_ByParentID]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [mea].[prc_GetAllMeasure_ByParentID] AS' 
END
GO
ALTER PROCEDURE [mea].[prc_GetAllMeasure_ByParentID]
    (
      @ParentId INT ,
      @IsMaster BIT = 0
    )
AS 
BEGIN
	
                WITH    N ( MeasureId, ParentId, [StatusID], Deleted, ACTIVE, CopyFrom )
                          AS ( SELECT   MeasureId ,
                                        ParentId ,
                                        StatusID ,
                                        Deleted ,
                                        Active ,
                                        CopyFrom
                               FROM     mea.Measure
                               WHERE    (MeasureId IN (	SELECT  MeasureId FROM mea.Measure WHERE  (@IsMaster = 1 AND (CopyFrom = @ParentId OR ParentId = @ParentId)) OR (@IsMaster =0 AND ParentId=@ParentId)))
							   UNION ALL
                               SELECT   np.MeasureId ,
                                        np.ParentId ,
                                        np.[StatusID] ,
                                        np.Deleted ,
                                        np.Active ,
                                        np.CopyFrom
                               FROM     mea.Measure AS np
                                        JOIN N ON N.MeasureId = np.ParentID
                             )
                    SELECT DISTINCT MeasureId ,
                            ParentId ,
                            StatusId
                    FROM    N
                    WHERE   N.Deleted = 0
                            AND N.Active = 1
                
END

GO
