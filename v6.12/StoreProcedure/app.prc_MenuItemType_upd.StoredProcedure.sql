SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[app].[prc_MenuItemType_upd]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [app].[prc_MenuItemType_upd] AS' 
END
GO

ALTER PROCEDURE [app].[prc_MenuItemType_upd]
(
	@MenuItemTypeID int,
	@Name varchar(64),
	@cUserid int,
	@Log smallint = 1
)
AS
BEGIN
	SET NOCOUNT ON
	DECLARE @Err Int

	UPDATE [app].[MenuItemType]
	SET
		[Name] = @Name
	WHERE
		[MenuItemTypeID] = @MenuItemTypeID

	IF @Log = 1 
	BEGIN 
		INSERT INTO [Log].[AuditLog] ( UserId, TableName, Type, Data, Created) 
		SELECT @cUserid,'MenuItemType',1,
		( SELECT * FROM [app].[MenuItemType] 
			WHERE
			[MenuItemTypeID] = @MenuItemTypeID			 FOR XML AUTO) as data,
			getdate()
	END

	Set @Err = @@Error

	RETURN @Err
END


GO
