SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[at].[prc_ScoreTemplate_copy]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [at].[prc_ScoreTemplate_copy] AS' 
END
GO


ALTER PROC [at].[prc_ScoreTemplate_copy]
(	
	@ScoreTemplateID	INT,
	@NewSTID	INT = NULL OUTPUT,
	@ActivityID		INT = NULL
)
AS
BEGIN
SET NOCOUNT ON
SET XACT_ABORT ON
BEGIN TRANSACTION

INSERT AT.ScoreTemplate(ActivityID,XCID, Type, NO, OwnerColorID,DefaultState)
SELECT ActivityID,XCID,Type, NO, OwnerColorID,DefaultState
FROM AT.ScoreTemplate
WHERE ScoreTemplateID = @ScoreTemplateID
SET @NewSTID = SCOPE_IDENTITY()


INSERT AT.LT_ScoreTemplate(LanguageID, ScoreTemplateID, Name, Description)
SELECT LanguageID, @NewSTID, Name, Description
FROM AT.LT_ScoreTemplate 
WHERE ScoreTemplateID = @ScoreTemplateID 


INSERT INTO AT.ST_CQ (ScoreTemplateID, CategoryID, QuestionID, MinValue, MaxValue)
SELECT @NewSTID, CategoryID, QuestionID, MinValue, MaxValue 
FROM   AT.ST_CQ  
WHERE ScoreTemplateID = @ScoreTemplateID

COMMIT TRANSACTION 
END

GO
