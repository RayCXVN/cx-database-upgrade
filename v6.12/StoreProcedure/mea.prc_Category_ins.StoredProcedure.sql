SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[mea].[prc_Category_ins]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [mea].[prc_Category_ins] AS' 
END
GO
ALTER PROCEDURE [mea].[prc_Category_ins]
 (		   
		   @CategoryId int = null output,  
		   @MeasureTypeId  int,
           @CustomerId  int = null,
           @CreatedBy int,
           @CreatedDate datetime,
           @ChangedBy int,
           @ChangedDate datetime,
           @cUserid int,  
		   @Log smallint = 1)
AS

BEGIN  
 SET NOCOUNT ON  
 DECLARE @Err Int  
  
 INSERT INTO [mea].[Category]
           ([MeasureTypeId]
           ,[CustomerId]
           ,[CreatedBy]
           ,[CreatedDate]
           ,[ChangedBy]
           ,[ChangedDate])
     VALUES
           (
           @MeasureTypeId,
           @CustomerId,
           @CreatedBy,
           @CreatedDate,
           @ChangedBy,
           @ChangedDate
           )
  
 Set @Err = @@Error  
 Set @CategoryId = scope_identity()  
  
 IF @Log = 1   
 BEGIN   
  INSERT INTO [Log].[AuditLog] ( UserId, TableName, Type, Data, Created)   
  SELECT @cUserid,'Category',0,  
  ( SELECT * FROM  [mea].[Category] 
   WHERE  
   [CategoryId] = @CategoryId     FOR XML AUTO) as data,  
    getdate()   
  END  
  
 RETURN @Err  
END


GO
