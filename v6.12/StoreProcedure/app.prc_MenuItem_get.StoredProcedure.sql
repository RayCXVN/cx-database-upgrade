SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[app].[prc_MenuItem_get]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [app].[prc_MenuItem_get] AS' 
END
GO
ALTER PROCEDURE [app].[prc_MenuItem_get]
(
	@MenuID int,
	@ParentID int = 0
)
AS
BEGIN
	SET NOCOUNT ON
	DECLARE @Err Int

	IF @ParentID is null
    Begin
      Set @ParentID = 0
	End

	SELECT
	[MenuItemID],
	[MenuID],
	ISNULL([ParentID], 0) AS 'ParentID',
	ISNULL([MenuItemTypeID], 0) AS 'MenuItemTypeID',
	ISNULL([PortalPageID], 0) AS 'PortalPageID',
	[No],
	[CssClass],
	[URL],
	[Target],
	[IsDefault],
	[Active],
	[Created],
    [NoRender]
	FROM [app].[MenuItem]
	WHERE
	[MenuID] = @MenuID AND ISNULL([ParentID],0) = @ParentID
	ORDER BY [No]

	Set @Err = @@Error

	RETURN @Err
END



GO
