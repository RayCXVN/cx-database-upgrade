SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[job].[prc_Job_upd]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [job].[prc_Job_upd] AS' 
END
GO

ALTER PROCEDURE [job].[prc_Job_upd]
(
	@JobID int,
	@JobTypeID smallint,
	@JobStatusID smallint,
	@OwnerID int,
	@UserID int,
	@Name nvarchar(256),
	@Priority smallint,
	@Option SMALLINT=NULL,
	@StartDate datetime,
	@EndDate datetime = NULL,
	@Description nvarchar(max),
	@cUserid int,
	@Log smallint = 1
)
AS
BEGIN
	SET NOCOUNT ON
	DECLARE @Err Int

	UPDATE [job].[Job]
	SET
		[JobTypeID] = @JobTypeID,
		[JobStatusID] = @JobStatusID,
		[OwnerID] = @OwnerID,
		[UserID] = @UserID,
		[Name] = @Name,
		[Priority] = @Priority,
		[Option] = @Option,
		[StartDate] = @StartDate,
		[EndDate] = @EndDate,
		[Description] = @Description
	WHERE
		[JobID] = @JobID

	IF @Log = 1 
	BEGIN 
		INSERT INTO [Log].[AuditLog] ( UserId, TableName, Type, Data, Created) 
		SELECT @cUserid,'Job',1,
		( SELECT * FROM [job].[Job] 
			WHERE
			[JobID] = @JobID			 FOR XML AUTO) as data,
			getdate()
	END

	Set @Err = @@Error

	RETURN @Err
END


GO
