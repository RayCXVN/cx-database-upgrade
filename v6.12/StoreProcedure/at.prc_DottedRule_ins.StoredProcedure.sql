SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[at].[prc_DottedRule_ins]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [at].[prc_DottedRule_ins] AS' 
END
GO

ALTER PROCEDURE [at].[prc_DottedRule_ins]
(
	@DottedRuleID int = null output,
	@ActivityID int,
	@SurveyID INT=NULL,
	@Active bit,
	@UseOnAllQuestions bit,
	@UseOnAllCategorys bit,
	@MinResult int,
	@MinResultPercentage float ,
	@FrequencyDottedType int,
	@FrequencyDottedLimit int,
	@FrequencyDotZeroValues bit,
	@FrequencyMinAlternativeCountDotted int,
	@AverageDottedType int,
	@AverageDottedLimit float,
	@OnlyDepartmentsBelow bit = 0,
	@cUserid int,
	@Log smallint = 1,
	@FrequencyMinQuestionCountDotted int =0
)
AS
BEGIN
	SET NOCOUNT ON
	DECLARE @Err Int

	INSERT INTO [at].[DottedRule]
	(
		[ActivityID],
		[SurveyID],
		[Active],
		[UseOnAllQuestions],
		[UseOnAllCategorys],
		[MinResult],
		[MinResultPercentage],
		[FrequencyDottedType],
		[FrequencyDottedLimit],
		[FrequencyDotZeroValues],
		[FrequencyMinAlternativeCountDotted],
		[AverageDottedType],
		[AverageDottedLimit],
		[OnlyDepartmentsBelow],
		[FrequencyMinQuestionCountDotted]
	)
	VALUES
	(
		@ActivityID,
		@SurveyID,
		@Active,
		@UseOnAllQuestions,
		@UseOnAllCategorys,
		@MinResult,
		@MinResultPercentage,
		@FrequencyDottedType,
		@FrequencyDottedLimit,
		@FrequencyDotZeroValues,
		@FrequencyMinAlternativeCountDotted,
		@AverageDottedType,
		@AverageDottedLimit,
		@OnlyDepartmentsBelow,
		@FrequencyMinQuestionCountDotted
	)

	Set @Err = @@Error
	Set @DottedRuleID = scope_identity()

	IF @Log = 1 
	BEGIN 
		INSERT INTO [Log].[AuditLog] ( UserId, TableName, Type, Data, Created) 
		SELECT @cUserid,'DottedRule',0,
		( SELECT * FROM [at].[DottedRule] 
			WHERE
			[DottedRuleID] = @DottedRuleID				 FOR XML AUTO) as data,
				getdate() 
	 END

	RETURN @Err
END

GO
