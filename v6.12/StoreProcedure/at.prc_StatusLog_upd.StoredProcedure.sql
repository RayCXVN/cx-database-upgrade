SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[at].[prc_StatusLog_upd]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [at].[prc_StatusLog_upd] AS' 
END
GO

ALTER PROCEDURE [at].[prc_StatusLog_upd]
(
	@StatusLogID int,
	@StatusTypeID int,
	@ResultID bigint = null,
	@CVID int = null,
	@UserID int,
	@Comment ntext,
	@cUserid int,
	@Log smallint = 1
)
AS
BEGIN
	SET NOCOUNT ON
	DECLARE @Err Int

	UPDATE [at].[StatusLog]
	SET
		[StatusTypeID] = @StatusTypeID,
		[ResultID] = @ResultID,
		[CVID] = @CVID,
		[UserID] = @UserID,
		[Comment] = @Comment
	WHERE
		[StatusLogID] = @StatusLogID

	IF @Log = 1 
	BEGIN 
		INSERT INTO [Log].[AuditLog] ( UserId, TableName, Type, Data, Created) 
		SELECT @cUserid,'StatusLog',1,
		( SELECT * FROM [at].[StatusLog] 
			WHERE
			[StatusLogID] = @StatusLogID			 FOR XML AUTO) as data,
			getdate()
	END

	Set @Err = @@Error

	RETURN @Err
END


GO
