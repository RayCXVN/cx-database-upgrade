SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[job].[prc_JobLastByHDAndTest_get]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [job].[prc_JobLastByHDAndTest_get] AS' 
END
GO
-- [job].[prc_JobLastByHDAndTest_get] 9,16959,25
ALTER PROCEDURE [job].[prc_JobLastByHDAndTest_get]
(
	@OwnerID int,
	@HDID  int,
	@JobTypeID int
)
AS
BEGIN
select j.[JobID],
	 j.[JobTypeID],
	 j.[JobStatusID],
	 j.[OwnerID],
	 j.[UserID],
	 j.[Name],
	 j.[Priority],
	 ISNULL(j.[Option], 0) AS 'Option',
	 j.[Created],
	 j.[StartDate],
	ISNULL( j.[EndDate], '1900-01-01') AS 'EndDate',
	 j.[Description],
	 jp.Value as departmentid,
	 jp2.value as testcode
	 from job.Job j
join  job.JobParameter jp on j.JobID = jp.JobID and  jp.Name = 'DepartmentID' and 
 jp.Value IN ( select cast(Departmentid as nvarchar(16)) from org.H_D where ParentID = @HDID)
 join org.Department d on d.DepartmentID = jp.Value 
 join job.JobParameter jp2 on j.JobID = jp2.JobID and  jp2.Name = 'TestCode' 
where j.JobTypeID = @JobTypeID  and j.OwnerID = @OwnerID
and j.Created =
(
     Select max(ij.created)
	 from job.Job ij
 join  job.JobParameter ijp on ij.JobID = ijp.JobID and  ijp.Name = 'DepartmentID' and 
 ijp.Value = jp.value 
 join job.JobParameter ijp2 on ij.JobID = ijp2.JobID and  ijp2.Name = 'TestCode'  and ijp2.Value = jp2.value
where j.JobTypeID = @JobTypeID  and j.OwnerID = @OwnerID
  
)
order by d.Name, jp2.value,j.created
END
GO
