SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[org].[prc_User_get]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [org].[prc_User_get] AS' 
END
GO
/*  2016-10-10 Ray: Remove column Status from org.User, org.Department, RDB.dbo.Result
*/
ALTER PROCEDURE [org].[prc_User_get]
(  
 @DepartmentID int  
)  
AS  
BEGIN  
 SET NOCOUNT ON  
 DECLARE @Err Int, @Unknown INT = 0, @EntityStatusID_Active INT = 1, @Inactive INT = 2  
 SELECT @EntityStatusID_Active = EntityStatusID FROM EntityStatus WHERE CodeName='Active'
 SELECT @Unknown  = EntityStatusID FROM EntityStatus WHERE CodeName='Unknown'
 SELECT @Inactive = EntityStatusID FROM EntityStatus WHERE CodeName='Inactive'
 
 SELECT  
 [UserID],  
 [Ownerid],  
 [DepartmentID],  
 [LanguageID],  
 ISNULL([RoleID], 0) AS 'RoleID',  
 [UserName],  
 [Password],  
 [LastName],  
 [FirstName],  
 [Email],  
 [Mobile],  
 [ExtID],  
 [SSN],  
 [Tag],  
 [Locked],  
 [ChangePassword],  
 [HashPassword],  
 [SaltPassword],  
 [OneTimePassword],  
 [OTPExpireTime],  
 [Created],  
 [CountryCode],
 [Gender],
 [DateOfBirth],
 [ForceUserLoginAgain],
 [LastUpdated],
 [LastUpdatedBy],
 [LastSynchronized],
 [ArchetypeID],
 [CustomerID],
 [Deleted],
 [EntityStatusID],
 [EntityStatusReasonID]
 FROM [org].[User]  
 WHERE  
 [DepartmentID] = @DepartmentID  
 AND [EntityStatusID] IN (@EntityStatusID_Active, @Unknown, @Inactive) 
 AND [Deleted] IS NULL

 Set @Err = @@Error  

 RETURN @Err  
END 

GO
