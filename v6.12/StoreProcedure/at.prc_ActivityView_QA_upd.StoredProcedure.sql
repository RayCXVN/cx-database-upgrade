SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[at].[prc_ActivityView_QA_upd]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [at].[prc_ActivityView_QA_upd] AS' 
END
GO

ALTER PROCEDURE [at].[prc_ActivityView_QA_upd]
(
	@ActivityViewQID int,
	@AlternativeID int,
	@No smallint,
	@cUserid int,
	@Log smallint = 1
)
AS
BEGIN
	SET NOCOUNT ON
	DECLARE @Err Int

	UPDATE [at].[ActivityView_QA]
	SET
		[ActivityViewQID] = @ActivityViewQID,
		[AlternativeID] = @AlternativeID,
		[No] = @No
	WHERE
		[ActivityViewQID] = @ActivityViewQID AND
		[AlternativeID] = @AlternativeID

	IF @Log = 1 
	BEGIN 
		INSERT INTO [Log].[AuditLog] ( UserId, TableName, Type, Data, Created) 
		SELECT @cUserid,'ActivityView_QA',1,
		( SELECT * FROM [at].[ActivityView_QA] 
			WHERE
			[ActivityViewQID] = @ActivityViewQID AND
			[AlternativeID] = @AlternativeID			 FOR XML AUTO) as data,
			getdate()
	END

	Set @Err = @@Error

	RETURN @Err
END


GO
