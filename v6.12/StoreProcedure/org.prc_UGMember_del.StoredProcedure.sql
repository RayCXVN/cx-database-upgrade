IF NOT EXISTS (SELECT 1 FROM sys.objects WHERE name = N'prc_UGMember_del' AND type IN ( N'P', N'PC' )  AND SCHEMA_NAME(schema_id) = 'org')
    EXEC ('CREATE PROC org.prc_UGMember_del AS ')
GO

/*
    2018-07-03  Sarah   -   Created for ATAdmin
*/
ALTER PROCEDURE [org].[prc_UGMember_del](
    @UGMemberID bigint,
    @cUserid    int,
    @Log        smallint = 1)
AS
BEGIN
    SET NOCOUNT ON;
    DECLARE @Err int;

    IF @Log = 1
    BEGIN
        INSERT INTO [Log].[AuditLog] ([UserId], [TableName], [Type], [Data], [Created])
        SELECT @cUserid, 'UGMember', 2, (SELECT * FROM [org].[UGMember] WHERE [UGMemberID] = @UGMemberID FOR XML AUTO) AS [data], GETDATE();
    END;

    DELETE FROM [org].[UGMember] WHERE [UGMemberID] = @UGMemberID;

    RETURN @Err;
END;