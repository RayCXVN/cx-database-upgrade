SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[fn_RandomPassword]') AND type in (N'FN', N'IF', N'TF', N'FS', N'FT'))
BEGIN
execute dbo.sp_executesql @statement = N'CREATE FUNCTION [dbo].[fn_RandomPassword]()
RETURNS varchar(6)
AS
BEGIN
	DECLARE @RandomPassword varchar(6)

	SELECT @RandomPassword = RandomPassword
	FROM view_RandomPassword6

	RETURN @RandomPassword

END' 
END

GO
