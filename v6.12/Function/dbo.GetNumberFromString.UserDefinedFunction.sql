/****** Object:  UserDefinedFunction [dbo].[GetNumberFromString]    Script Date: 5/11/2017 1:15:27 PM ******/
DROP FUNCTION [dbo].[GetNumberFromString]
GO
/****** Object:  UserDefinedFunction [dbo].[GetNumberFromString]    Script Date: 5/11/2017 1:15:27 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE FUNCTION [dbo].[GetNumberFromString]
(
   @String NVARCHAR(MAX)
)
RETURNS @Result TABLE (Number INT)
AS
BEGIN
   DECLARE @NumRange VARCHAR(50) = '%[0-9]%'
   DECLARE @Temp VARCHAR(50) = ''
   DECLARE @Index INT

   WHILE PatIndex(@NumRange, @String) >= 0
   BEGIN
        SET @Temp =  @Temp + SUBSTRING(@string, PatIndex(@NumRange, @string),1)
        SET @string = Stuff(@string, PatIndex(@NumRange, @string), 1, '')

        IF(@Index = PatIndex(@NumRange, @string) OR @Index IS NULL)
            SET @Index = PatIndex(@NumRange, @string)
        ELSE
        BEGIN
            INSERT INTO @Result(Number) VALUES (CAST(@Temp AS INT))
            SET @Temp = ''
            SET @Index = NULL
        END
    END

   RETURN 

END


GO
