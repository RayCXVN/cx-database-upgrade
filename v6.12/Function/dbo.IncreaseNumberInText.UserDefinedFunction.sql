SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[IncreaseNumberInText]') AND type in (N'FN', N'IF', N'TF', N'FS', N'FT'))
BEGIN
execute dbo.sp_executesql @statement = N'CREATE FUNCTION [dbo].[IncreaseNumberInText]
(   @SourceText     nvarchar(max),
    @MinimumDigit   int = 1,
    @MinimumValue   int = 0
) RETURNS nvarchar(max) AS
BEGIN
    DECLARE @OldText nvarchar(max) = @SourceText, @NewText nvarchar(max) = ''''
    DECLARE @NumberIndex int = 0, @NewNumber int = 0, @TextLen int = 0, @MaxLoop int = 20, @LoopCount int = 0

    SET @TextLen = LEN(@OldText)

    WHILE LEN(@NewText) < @TextLen AND @LoopCount < @MaxLoop
    BEGIN
        SET @NumberIndex = PATINDEX(''%[0-9]%'', @OldText)
        IF @NumberIndex > 0
        BEGIN
            --print ''number found''
            IF ISNUMERIC(SUBSTRING(@OldText, @NumberIndex, @MinimumDigit)) = 1
            BEGIN
                SET @NewNumber = SUBSTRING(@OldText, @NumberIndex, @MinimumDigit)
                --print ''convert year ok''
            END
            ELSE
            BEGIN
                SET @LoopCount = @LoopCount + 1
                SET @NewText = @NewText + LEFT(@OldText, @NumberIndex)
                SET @OldText = SUBSTRING(@OldText, @NumberIndex + 1, @TextLen)
                --print ''cannot convert year''
                CONTINUE
            END

            IF @NewNumber >= @MinimumValue
            BEGIN
                SET @NewText = @NewText + LEFT(@OldText, @NumberIndex - 1) + CONVERT(nvarchar(5), @NewNumber + 1)
                SET @OldText = SUBSTRING(@OldText, @NumberIndex + @MinimumDigit, @TextLen)
                --print ''valid year''
            END
            ELSE
            BEGIN
                SET @NewText = @NewText + LEFT(@OldText, @NumberIndex)
                SET @OldText = SUBSTRING(@OldText, @NumberIndex + 1, @TextLen)
                --print ''invalid year, next search''
            END
        END
        ELSE
        BEGIN
            SET @NumberIndex = 1
            SET @NewText = @NewText + SUBSTRING(@OldText, @NumberIndex, @TextLen)
            --print ''number not found''
        END

        SET @LoopCount = @LoopCount + 1
    END
    RETURN @NewText
END

' 
END

GO
