SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[GetPath]') AND type in (N'FN', N'IF', N'TF', N'FS', N'FT'))
BEGIN
execute dbo.sp_executesql @statement = N'
CREATE FUNCTION [dbo].[GetPath] 
(
	@HDID as int
)
RETURNS varchar(256)
AS
BEGIN
	-- Declare the return variable here
	DECLARE @Result varchar(200)
	Declare @ParentID as int
	Declare @sHDID as varchar(50)	

	-- Add the T-SQL statements to compute the return value here
	Select @ParentID = Parentid, @sHDID = H.[HDID] from org.H_D h (nolock) Where HDID = @HDID

  
	set @Result =  @sHDID + ''\''	
	While 	@ParentID is not null 
	begin
 	 Select @ParentID = Parentid, @sHDID = h.hdid from org.H_D h (nolock) Where HDID = @ParentID

   	 set @Result = @sHDID + ''\'' + @Result 	
	
	end


	-- Return the result of the function
	RETURN ''\'' + @Result

END
' 
END

GO
