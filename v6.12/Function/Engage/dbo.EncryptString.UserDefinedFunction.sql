SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER OFF
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[EncryptString]') AND type in (N'FN', N'IF', N'TF', N'FS', N'FT'))
BEGIN
execute dbo.sp_executesql @statement = N'CREATE FUNCTION [dbo].[EncryptString](@String [nvarchar](128))
RETURNS [nvarchar](128) WITH EXECUTE AS CALLER
AS 
EXTERNAL NAME [SQLEncrypt].[SQLEncrypt.SQLEncrypt.Encryption64].[Encrypt]' 
END

GO
