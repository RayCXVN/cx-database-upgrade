SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER OFF
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[CreatePasswordHash]') AND type in (N'FN', N'IF', N'TF', N'FS', N'FT'))
BEGIN
execute dbo.sp_executesql @statement = N'CREATE FUNCTION [dbo].[CreatePasswordHash](@String [nvarchar](512), @Salt [nvarchar](512))
RETURNS [nvarchar](128) WITH EXECUTE AS CALLER
AS 
EXTERNAL NAME [SQLEncryptHash].[Security.Pbkdf2].[CreatePasswordHash]' 
END

GO
