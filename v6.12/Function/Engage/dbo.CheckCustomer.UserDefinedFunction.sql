SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[CheckCustomer]') AND type in (N'FN', N'IF', N'TF', N'FS', N'FT'))
BEGIN
execute dbo.sp_executesql @statement = N'CREATE FUNCTION [dbo].[CheckCustomer]
(
	@DepartmentID INT, 
	@CustomerID INT
)   
RETURNS INT 
AS 
BEGIN 
	IF EXISTS (SELECT 1 FROM org.Department WHERE DepartmentID = @DepartmentID AND ISNULL(CustomerID, -1) = ISNULL(@CustomerID, -1)) 
		RETURN 1
	RETURN 0
END
' 
END

GO
