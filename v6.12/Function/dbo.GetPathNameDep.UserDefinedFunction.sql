SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[GetPathNameDep]') AND type in (N'FN', N'IF', N'TF', N'FS', N'FT'))
BEGIN
execute dbo.sp_executesql @statement = N'CREATE FUNCTION [dbo].[GetPathNameDep] 
(
	@HierarchyID as int,
	@Departmentid as int
)
RETURNS varchar(200)
AS
BEGIN
	-- Declare the return variable here
	DECLARE @Result varchar(200)
	Declare @ParentID as int
	Declare @name as varchar(50)	
    Declare @HDID as int

    select @HDID = HDID from org.H_D where @HierarchyID  = HierarchyID and departmentid = @Departmentid 

	-- Add the T-SQL statements to compute the return value here
	Select @ParentID = Parentid, @name = d.[name] from org.H_D h join org.department d on d.departmentid = h.departmentid Where HDID = @HDID

  
	set @Result =  @name	
	While 	@ParentID is not null 
	begin
 	 Select @ParentID = Parentid, @name = d.[name] from org.H_D h join org.department d on d.departmentid = h.departmentid Where HDID = @ParentID

   	 set @Result = @name + ''\'' + @Result 	
	
	end


	-- Return the result of the function
	RETURN @Result

END

' 
END

GO
