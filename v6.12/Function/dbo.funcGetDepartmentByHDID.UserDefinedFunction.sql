SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[funcGetDepartmentByHDID]') AND type in (N'FN', N'IF', N'TF', N'FS', N'FT'))
BEGIN
execute dbo.sp_executesql @statement = N'-- select * from dbo.[funcGetDepartmentByHDID] (''26641'')
CREATE FUNCTION [dbo].[funcGetDepartmentByHDID](@HDID as varchar(max))
RETURNS @DepTable table(
  DepartmentID INT
  )
AS
BEGIN
	DECLARE @EntityStatusID INT = 0
	SELECT @EntityStatusID = EntityStatusID FROM dbo.EntityStatus WHERE CodeName = N''Active''

    INSERT INTO @DepTable (DepartmentID)
    SELECT  hd.DepartmentID
    FROM	  org.[H_D] hd inner join org.[Department] d on d.DepartmentID = hd.DepartmentID and d.EntityStatusID = @EntityStatusID AND d.Deleted IS NULL
    WHERE	  hd.[path] like  ''%\'' + cast(@HDID as varchar(16)) + ''\%'' and hd.deleted = 0

    RETURN
END

' 
END

GO
