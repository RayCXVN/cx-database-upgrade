SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[IsInteger]') AND type in (N'FN', N'IF', N'TF', N'FS', N'FT'))
BEGIN
execute dbo.sp_executesql @statement = N'
create Function [dbo].[IsInteger](@Value VarChar(18))
Returns Bit
As 
Begin    
Return IsNull(
		(Select Case When CharIndex(''.'', @Value) > 0
			Then Case When Convert(int, ParseName(@Value, 1)) <> 0
				Then 0
				Else 1
				End
			Else 1                  
			End      
		Where IsNumeric(@Value + ''e0'') = 1), 0)
End
' 
END

GO
