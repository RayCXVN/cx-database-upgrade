SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[fun_H_D_All_Departments]') AND type in (N'FN', N'IF', N'TF', N'FS', N'FT'))
BEGIN
execute dbo.sp_executesql @statement = N'CREATE FUNCTION [dbo].[fun_H_D_All_Departments]
(	
	@HDID as int
)
RETURNS TABLE 
AS
RETURN 
(
	-- Add the SELECT statement with parameter references here
	WITH HDS (HDID,Depid,Level,pathstring)
   AS
  (
    --anchor
	SELECT h.HDID,h.Departmentid,0 as Level,cast(d2.name as varchar(800)) as  pathstring
    FROM org.H_D  h
     join org.department d2 on d2.departmentid = h.departmentid
    where h.HDID = @HDID
    UNION ALL
	--Recursive
    select h.HDID,h.Departmentid,Level + 1,cast(cast(d.pathstring as varchar(800)) + ''\'' + d2.name  as varchar(800))  as pathstring
	FROM org.H_D  h
	join org.department d2 on d2.departmentid = h.departmentid
    INNER JOIN HDS AS d
        ON d.HDID = h.ParentID
   
    
  )
-- Statement that executes the CTE
SELECT distinct depid,pathstring
FROM HDS 

)

' 
END

GO
