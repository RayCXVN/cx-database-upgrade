SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[SplitString]') AND type in (N'FN', N'IF', N'TF', N'FS', N'FT'))
BEGIN
execute dbo.sp_executesql @statement = N'CREATE FUNCTION [dbo].[SplitString] 
(
    -- Add the parameters for the function here
    @myString varchar(max)
    
)
RETURNS 
@ReturnTable TABLE 
(
    
    TableTypeID [varchar](50) NULL,
    ElementID varchar(50) null
)
AS
BEGIN
	Declare @iSpaces INT
	Declare @part varchar(50)
	Declare @part2 varchar(50)
	
	Select @iSpaces = charindex('','',@myString,0)
	While @iSpaces > 0
	
	BEGIN
		Select @part = substring(@myString,0,charindex('':'',@myString,0))
		Select @part2 = substring(@myString,charindex('':'',@myString,0)+1,charindex('','',@myString,0)-charindex('':'',@myString,0)-1)
			Insert Into @ReturnTable(TableTypeID,ElementID)values ( @part, @part2)
			
			Select @myString = substring(@mystring,charindex('','',@myString,0)+ len('',''),len(@myString) - charindex('' '',@myString,0))
			Select @iSpaces = charindex('','',@myString,0)
	END
	
    If len(@myString) > 0
    begin
		Select @part = substring(@myString,0,charindex('':'',@myString,0))
		Select @part2 = substring(@myString,charindex('':'',@myString,0)+1,LEN(@mystring) - charindex('':'',@myString,0))
			Insert Into @ReturnTable(TableTypeID,ElementID)values ( @part, @part2)
    end
    RETURN 
END


' 
END

GO
