SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[at].[GetLatestStatusResultIDByActivity]') AND type in (N'FN', N'IF', N'TF', N'FS', N'FT'))
BEGIN
execute dbo.sp_executesql @statement = N'/*
    Steve - Dec 8, 2016 - Job Fair project: merge Q and R DB, then change how to make a dynamic query
*/
CREATE FUNCTION [at].[GetLatestStatusResultIDByActivity]
(
    @UserID	 int,
    @ActivityID int,
    @ExcludeStatusTypeID varchar(max),
    @PriorStatusTypeID   int = 0
)
RETURNS nvarchar(max)
AS
BEGIN
	DECLARE @ResultStatus   int,
            @ReturnValue    nvarchar(max),
            @SavedStatus    int = 0,
            @SavedValue     nvarchar(max),
            @SurveyID       int,
            @ReportServer   nvarchar(max),
            @ReportDB       nvarchar(max),
            @sqlCommand     nvarchar(max) = N'''',
            @Parameters     nvarchar(max) = N''@p_SurveyID int,@p_UserID int,@p_StatusTypeID varchar(max),@p_ResultStatus int OUTPUT,@p_ReturnValue nvarchar(max) OUTPUT'',
			@ActiveEntityStatusID INT = 0, 
			@LinkedDB NVARCHAR(MAX) = '' ''

    SELECT @ActiveEntityStatusID = EntityStatusID FROM dbo.EntityStatus WHERE CodeName = ''Active''
	
	DECLARE c_Survey CURSOR SCROLL READ_ONLY FOR
	SELECT s.SurveyID, s.ReportServer, s.ReportDB FROM at.Survey s WHERE s.ActivityID = @ActivityID AND s.Status > 0
	ORDER BY s.EndDate DESC, s.StartDate DESC
	
	OPEN c_Survey
	FETCH NEXT FROM c_Survey INTO @SurveyID, @ReportServer, @ReportDB
	WHILE @@FETCH_STATUS=0
	BEGIN
	   IF (@ReportServer = @@servername AND @ReportDB = DB_NAME())
			SET @LinkedDB = '' ''
	   ELSE 
			SET @LinkedDB = ''['' + @ReportServer + ''].['' + @ReportDB + ''].''

	   -- Take the prioritized status type as highest priority
	   IF @PriorStatusTypeID > 0
	   BEGIN
		  SET @sqlCommand = N''SELECT TOP 1 @p_ResultStatus = StatusTypeID, @p_ReturnValue = (CONVERT(NVARCHAR(10),StatusTypeID) + ''''|'''' + CONVERT(NVARCHAR(32),ResultID))
						  FROM '' + @LinkedDB + ''dbo.Result
						  WHERE SurveyID = @p_SurveyID AND UserID = @p_UserID AND StatusTypeID = '' + CONVERT(VARCHAR(10),@PriorStatusTypeID)+'' AND Deleted IS NULL AND EntityStatusID = '' + CONVERT(NVARCHAR(14), @ActiveEntityStatusID) +
						  '' ORDER BY ISNULL(Created,getdate()) DESC''
		  EXECUTE sp_executesql @sqlCommand, @Parameters, @p_SurveyID = @SurveyID, @p_UserID = @UserID, @p_StatusTypeID = @ExcludeStatusTypeID, @p_ResultStatus = @ResultStatus OUTPUT, @p_ReturnValue = @ReturnValue OUTPUT
		  IF @ResultStatus = @PriorStatusTypeID BREAK
	   END
	   
	   -- Get the latest status in the survey except excluded ones
	   SET @sqlCommand = N''SELECT TOP 1 @p_ResultStatus = StatusTypeID, @p_ReturnValue = (CONVERT(NVARCHAR(10),StatusTypeID) + ''''|'''' + CONVERT(NVARCHAR(32),ResultID))
					   FROM '' + @LinkedDB + ''dbo.Result
					   WHERE SurveyID = @p_SurveyID AND UserID = @p_UserID AND StatusTypeID NOT IN (@p_StatusTypeID) AND Deleted IS NULL AND EntityStatusID = '' + CONVERT(NVARCHAR(14), @ActiveEntityStatusID) +
					   '' ORDER BY ISNULL(Created,getdate()) DESC''
	   EXECUTE sp_executesql @sqlCommand, @Parameters, @p_SurveyID = @SurveyID, @p_UserID = @UserID, @p_StatusTypeID = @ExcludeStatusTypeID, @p_ResultStatus = @ResultStatus OUTPUT, @p_ReturnValue = @ReturnValue OUTPUT
	   
	   -- If found a status
	   IF @ResultStatus IS NOT NULL AND NOT EXISTS (SELECT 1 FROM dbo.funcListToTableInt(@ExcludeStatusTypeID,'','') WHERE Value = @ResultStatus)
	   BEGIN
		  -- If having a prioritized status type then continue to search for it in other surveys
		  IF @PriorStatusTypeID > 0 AND @SavedStatus = 0
          BEGIN
			 SET @SavedStatus = @ResultStatus
             SET @SavedValue = @ReturnValue
          END
		  ELSE IF @PriorStatusTypeID = 0  -- Else return the status
			 BREAK
	   END
		  
	   FETCH NEXT FROM c_Survey INTO @SurveyID, @ReportServer, @ReportDB
	END
	
	-- If having a prioritized status type and didn''t find one but found other status then return that status
	IF @PriorStatusTypeID > 0 AND @ResultStatus != @PriorStatusTypeID AND @SavedStatus != 0
    BEGIN
        SET @ResultStatus = @SavedStatus
        SET @ReturnValue = @SavedValue
    END
	
	-- Looking for a status other than no result
     IF @ResultStatus IS NULL
     BEGIN
	  FETCH FIRST FROM c_Survey INTO @SurveyID, @ReportServer, @ReportDB
	  WHILE @@FETCH_STATUS=0
	  BEGIN
		 SET @sqlCommand = N''SELECT TOP 1 @p_ResultStatus = StatusTypeID, @p_ReturnValue = (CONVERT(NVARCHAR(10),StatusTypeID) + ''''|'''' + CONVERT(NVARCHAR(32),ResultID))
						 FROM '' + @LinkedDB + ''dbo.Result
						 WHERE SurveyID = @p_SurveyID AND UserID = @p_UserID AND StatusTypeID IN (@p_StatusTypeID) AND Deleted IS NULL AND EntityStatusID = '' + CONVERT(NVARCHAR(14), @ActiveEntityStatusID) + 
						 '' ORDER BY ResultID DESC''
		 EXECUTE sp_executesql @sqlCommand, @Parameters, @p_SurveyID = @SurveyID, @p_UserID = @UserID, @p_StatusTypeID = @ExcludeStatusTypeID, @p_ResultStatus = @ResultStatus OUTPUT, @p_ReturnValue = @ReturnValue OUTPUT
		 IF @ResultStatus IS NOT NULL AND EXISTS (SELECT 1 FROM dbo.funcListToTableInt(@ExcludeStatusTypeID,'','') WHERE Value = @ResultStatus) BREAK
		  
		 FETCH NEXT FROM c_Survey INTO @SurveyID, @ReportServer, @ReportDB
	  END
     END
	CLOSE c_Survey
	DEALLOCATE c_Survey
	
	RETURN ISNULL(@ReturnValue,''0|0'')
END
' 
END

GO
