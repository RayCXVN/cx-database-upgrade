SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[at].[GetLatestStatusByActivity]') AND type in (N'FN', N'IF', N'TF', N'FS', N'FT'))
BEGIN
execute dbo.sp_executesql @statement = N'/*
    Sarah - Oct 31, 2016 - Hot fix: Get EntityStatus value on QDB
	Johnny - Des 07 2016 - Update store procedure for dynamic SQL
*/
CREATE FUNCTION [at].[GetLatestStatusByActivity]
(
    @UserID	 int,
    @ActivityID int,
    @ExcludeStatusTypeID varchar(max),
    @PriorStatusTypeID   int = 0
)
RETURNS int
AS
BEGIN
	DECLARE @ResultStatus  int,
		   @SavedStatus   int = 0,
		   @SurveyID	   int,
		   @ReportServer  nvarchar(max),
		   @ReportDB	   nvarchar(max),
		   @sqlCommand	   nvarchar(max) = N'''',
		   @Parameters	   nvarchar(max) = N''@p_SurveyID int,@p_UserID int,@p_StatusTypeID varchar(max),@p_ResultStatus int OUTPUT'',
		   @ActiveEntityStatusID INT = 0

    SELECT @ActiveEntityStatusID = EntityStatusID FROM dbo.EntityStatus WHERE CodeName = ''Active''
	SELECT DISTINCT @ReportServer=Su.ReportServer, @ReportDB=Su.ReportDB FROM at.Survey Su
	DECLARE @LinkedDB NVARCHAR(MAX)
			
	DECLARE c_Survey CURSOR SCROLL READ_ONLY FOR
	SELECT s.SurveyID, s.ReportServer, s.ReportDB FROM at.Survey s WHERE s.ActivityID = @ActivityID AND s.Status > 0
	ORDER BY s.EndDate DESC, s.StartDate DESC

	OPEN c_Survey
	FETCH NEXT FROM c_Survey INTO @SurveyID, @ReportServer, @ReportDB
	WHILE @@FETCH_STATUS=0
	BEGIN
		IF @ReportDB=DB_NAME() AND @ReportServer=@@servername
			BEGIN
			SET @LinkedDB='' ''
			END
		ELSE
			BEGIN
			SET @LinkedDB= ''[''+ @ReportServer + ''].[''+ @ReportDB + ''].''
			END

	   -- Take the prioritized status type as highest priority
	   IF @PriorStatusTypeID > 0
	   BEGIN
		  SET @sqlCommand = N''SELECT TOP 1 @p_ResultStatus = StatusTypeID
						  FROM ''+ @LinkedDB +''dbo.Result
						  WHERE SurveyID = @p_SurveyID AND UserID = @p_UserID AND StatusTypeID = '' + CONVERT(VARCHAR(10),@PriorStatusTypeID) + '' AND Deleted IS NULL AND EntityStatusID = '' + CONVERT(NVARCHAR(14), @ActiveEntityStatusID)
		  EXECUTE sp_executesql @sqlCommand, @Parameters, @p_SurveyID = @SurveyID, @p_UserID = @UserID, @p_StatusTypeID = @ExcludeStatusTypeID, @p_ResultStatus = @ResultStatus OUTPUT
		  IF @ResultStatus = @PriorStatusTypeID BREAK
	   END
	   
	   -- Get the latest status in the survey except excluded ones
	   SET @sqlCommand = N''SELECT TOP 1 @p_ResultStatus = StatusTypeID
					   FROM ''+ @LinkedDB +''dbo.Result
					   WHERE SurveyID = @p_SurveyID AND UserID = @p_UserID AND StatusTypeID NOT IN (@p_StatusTypeID) AND Deleted IS NULL AND EntityStatusID = '' + CONVERT(NVARCHAR(14), @ActiveEntityStatusID) + 
					   '' ORDER BY ISNULL(Created,getdate()) DESC''
	   EXECUTE sp_executesql @sqlCommand, @Parameters, @p_SurveyID = @SurveyID, @p_UserID = @UserID, @p_StatusTypeID = @ExcludeStatusTypeID, @p_ResultStatus = @ResultStatus OUTPUT
	   
	   -- If found a status
	   IF @ResultStatus IS NOT NULL AND NOT EXISTS (SELECT 1 FROM dbo.funcListToTableInt(@ExcludeStatusTypeID,'','') WHERE Value = @ResultStatus)
	   BEGIN
		  -- If having a prioritized status type then continue to search for it in other surveys
		  IF @PriorStatusTypeID > 0 AND @SavedStatus = 0
			 SET @SavedStatus = @ResultStatus
		  ELSE IF @PriorStatusTypeID = 0  -- Else return the status
			 BREAK
	   END
		  
	   FETCH NEXT FROM c_Survey INTO @SurveyID, @ReportServer, @ReportDB
	END
	
	-- If having a prioritized status type and didn''t find one but found other status then return that status
	IF @PriorStatusTypeID > 0 AND @ResultStatus != @PriorStatusTypeID AND @SavedStatus != 0 SET @ResultStatus = @SavedStatus
	
	-- Looking for a status other than no result
     IF @ResultStatus IS NULL
     BEGIN
	  FETCH FIRST FROM c_Survey INTO @SurveyID, @ReportServer, @ReportDB
	  WHILE @@FETCH_STATUS=0
	  BEGIN
		 SET @sqlCommand = N''SELECT TOP 1 @p_ResultStatus = StatusTypeID
						 FROM ''+ @LinkedDB +''dbo.Result
						 WHERE SurveyID = @p_SurveyID AND UserID = @p_UserID AND StatusTypeID IN (@p_StatusTypeID) AND Deleted IS NULL AND EntityStatusID = '' + CONVERT(NVARCHAR(14), @ActiveEntityStatusID) +
						 '' ORDER BY ResultID DESC''
		 EXECUTE sp_executesql @sqlCommand, @Parameters, @p_SurveyID = @SurveyID, @p_UserID = @UserID, @p_StatusTypeID = @ExcludeStatusTypeID, @p_ResultStatus = @ResultStatus OUTPUT
		 IF @ResultStatus IS NOT NULL AND EXISTS (SELECT 1 FROM dbo.funcListToTableInt(@ExcludeStatusTypeID,'','') WHERE Value = @ResultStatus) BREAK
		  
		 FETCH NEXT FROM c_Survey INTO @SurveyID, @ReportServer, @ReportDB
	  END
     END
	CLOSE c_Survey
	DEALLOCATE c_Survey
	
	RETURN ISNULL(@ResultStatus,0)
END
' 
END

GO
