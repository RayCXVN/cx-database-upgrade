SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[StringToArray]') AND type in (N'FN', N'IF', N'TF', N'FS', N'FT'))
BEGIN
execute dbo.sp_executesql @statement = N'/*
This function split a given string by @Delimiter into table rows

2017-06-27 Ray     Fix bug when delimiter having 2+ characters and wildcard characters

SELECT * FROM [dbo].[StringToArray] (''1,2,3,4'', '','')
*/

CREATE FUNCTION [dbo].[StringToArray] (@String nvarchar(MAX), @Delimiter nvarchar(8))
RETURNS @ParseTable TABLE (ParseValue nvarchar(1024))
AS
BEGIN
    DECLARE @DelimiterLength int, @DelimiterPosition int, @SingleValue nvarchar(1024) = ''''

    SET @DelimiterLength = LEN(@Delimiter)
    IF @DelimiterLength <= 0
    BEGIN
        INSERT INTO @ParseTable ([ParseValue]) VALUES (@String)
        RETURN
    END

    IF LEN(@String) > 0
    BEGIN
        SET @String = @String + @Delimiter
        SET @DelimiterPosition = CHARINDEX(@Delimiter, @String)
        WHILE @DelimiterPosition > 0
        BEGIN
            INSERT INTO @ParseTable ([ParseValue]) SELECT LEFT(@String, @DelimiterPosition - 1)
            SET @String = SUBSTRING(@String, @DelimiterPosition + @DelimiterLength, LEN(@String))
            SET @DelimiterPosition = CHARINDEX(@Delimiter, @String)
        END
    END

    RETURN
END
' 
END

GO
