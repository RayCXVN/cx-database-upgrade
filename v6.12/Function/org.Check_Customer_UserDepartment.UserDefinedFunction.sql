SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[org].[Check_Customer_UserDepartment]') AND type in (N'FN', N'IF', N'TF', N'FS', N'FT'))
BEGIN
execute dbo.sp_executesql @statement = N'CREATE FUNCTION [org].[Check_Customer_UserDepartment] (@Temp int) RETURNS int AS BEGIN RETURN 1 END';
END
GO
/*  Check if given CustomerID match with CustomerID in given Department
    CreatedBy: Ray
*/
ALTER FUNCTION [org].[Check_Customer_UserDepartment] (
    @DepartmentID   int,
    @CustomerID     int
) RETURNS int
AS
BEGIN
    DECLARE @Matched int = 0;

    IF EXISTS (SELECT 1 FROM [org].[Department] d WHERE d.[DepartmentID] = @DepartmentID AND d.[CustomerID] = @CustomerID)
    BEGIN
        SET @Matched = 1;
    END;

    RETURN @Matched
END
GO