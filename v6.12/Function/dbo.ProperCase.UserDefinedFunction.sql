SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[ProperCase]') AND type in (N'FN', N'IF', N'TF', N'FS', N'FT'))
BEGIN
execute dbo.sp_executesql @statement = N'CREATE function [dbo].[ProperCase](@Text as nvarchar(max))
returns nvarchar(max)
as
begin
   declare @Reset bit;
   declare @Ret nvarchar(max);
   declare @i int;
   declare @c char(1);

   select @Reset = 1, @i=1, @Ret = '''';
   
   while (@i <= len(@Text))
   	select @c= substring(@Text,@i,1),
               @Ret = @Ret + case when @Reset=1 then UPPER(@c) else LOWER(@c) end,
               @Reset = case when @c like ''[a-åA-Å]'' then 0 else 1 end,
               @i = @i +1
   return @Ret
end
' 
END

GO
