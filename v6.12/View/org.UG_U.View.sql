SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.views WHERE object_id = OBJECT_ID(N'[org].[UG_U]'))
EXEC dbo.sp_executesql @statement = N'CREATE VIEW [org].[UG_U]
AS
    SELECT DISTINCT ugm.[UserGroupID], ugm.[UserID]
    FROM [org].[UGMember] ugm
    WHERE (ugm.[EntityStatusID] = 1 OR ugm.[EntityStatusID] IS NULL)
      AND ugm.[Deleted] IS NULL
      AND ugm.[UserID] > 0
      AND (ugm.[ValidFrom] IS NULL OR ugm.[ValidFrom] <= SYSDATETIME())
      AND (ugm.[ValidTo] IS NULL OR ugm.[ValidTo] >= SYSDATETIME())
' 
GO
