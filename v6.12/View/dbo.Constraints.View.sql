SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.views WHERE object_id = OBJECT_ID(N'[dbo].[Constraints]'))
EXEC dbo.sp_executesql @statement = N'CREATE VIEW [dbo].[Constraints]
as
SELECT     TOP (100) PERCENT t1.TABLE_NAME, t1.TABLE_SCHEMA, t1.COLUMN_NAME, t1.CONSTRAINT_NAME, t2.CONSTRAINT_TYPE, 
                      ISNULL(t3.UNIQUE_CONSTRAINT_NAME, '''') AS UNIQUE_CONSTRAINT_NAME, ISNULL(t7.TABLE_NAME, '''') AS PK_Table, ISNULL(t7.COLUMN_NAME, '''') AS PK_Column
FROM         INFORMATION_SCHEMA.KEY_COLUMN_USAGE AS t1 LEFT OUTER JOIN
                      INFORMATION_SCHEMA.TABLE_CONSTRAINTS AS t2 ON t1.CONSTRAINT_NAME = t2.CONSTRAINT_NAME LEFT OUTER JOIN
                      INFORMATION_SCHEMA.REFERENTIAL_CONSTRAINTS AS t3 ON t1.CONSTRAINT_NAME = t3.CONSTRAINT_NAME LEFT OUTER JOIN
                      sys.objects AS t4 ON t4.name = t1.CONSTRAINT_NAME LEFT OUTER JOIN
                      sys.foreign_key_columns AS t5 ON t5.constraint_object_id = t4.object_id LEFT OUTER JOIN
                      sys.objects AS t6 ON t6.object_id = t5.referenced_object_id LEFT OUTER JOIN
                      INFORMATION_SCHEMA.COLUMNS AS t7 ON t7.TABLE_NAME = t6.name AND t7.ORDINAL_POSITION = t5.referenced_column_id
ORDER BY t1.TABLE_NAME

' 
GO
