SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.views WHERE object_id = OBJECT_ID(N'[dbo].[view_RandomPassword6]'))
EXEC dbo.sp_executesql @statement = N'CREATE VIEW [dbo].[view_RandomPassword6] AS 
-- select * from view_RandomPassword6
SELECT
	CHAR(ROUND(65 + (RAND() * (25)),0)) -- get random upper case
	
	+
	CHAR(ROUND(65 + (RAND() * (25)),0)) -- get random upper case
	+
	CHAR(ROUND(65 + (RAND() * (25)),0)) -- get random upper case
	+
	CHAR(ROUND(65 + (RAND() * (25)),0)) -- get random upper case
	+
	CHAR(ROUND(65 + (RAND() * (25)),0)) -- get random upper case
	+
	CHAR(ROUND(65 + (RAND() * (25)),0)) -- get random upper case

	as RandomPassword' 
GO
